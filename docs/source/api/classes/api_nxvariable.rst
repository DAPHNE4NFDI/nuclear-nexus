.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`Var`
============

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   

All fittable values in Nexus are of variable type :class:`Var`.
Although all fit variables can be initialized by a *float* or *int* they are converted to a :class:`Var` object internally.

.. warning:: Please note that special care has to be taken when assigning :class:`Var` objects as described below.

.. autoclass:: nexus.Var
   :members:
   :exclude-members: thisown


.. autoclass:: nexus.FitVariables
   :members:
   :exclude-members: thisown


.. note:: When the :class:`Var` is defined in another object, you can assign a new :attr:`Var.value` like this.
          
          .. code-block::
             
             # all of the following assignments are equivalent
             site = nx.Hyperfine(magnetic_field = 33) # internally converted to a nexus.Var

             site = nx.Hyperfine(magnetic_field = nx.Var(33))

             site = nx.Hyperfine(magnetic_field = nx.Var(value = 33))

             print(site.magnetic_field)

             # assign the Var.value directly
             site.magnetic_field.value = 5

             print(site.magnetic_field)

             # assign the Var via another nexus class (here the hyperfine object).
             # stays a Var object as it is the attribute of the class 
             # RECOMMENDED OPTION
             site.magnetic_field = 7  

             print(site.magnetic_field)

          The last option is the recommended way because its easier and clearer syntax.

.. warning:: Do not try to change a :attr:`Var.value` by setting the :class:`Var` to a *float* or *int* when it is initialized as a python object directly.

             .. code-block::
              
                my_var = nx.Var(7.4)
                print(my_var)
             
                # Don't do this to assign Var.value
                my_var = 5  # here, my_var will be of type int
                print(my_var)

                my_var = nx.Var(7.4)
                # Either use 
                my_var.value = 5
                print(my_var)
                # or
                my_var >> 28
                print(my_var)
 
             To define a :class:`Var` directly is a rare case. It is useful in parameter dependent fitting across different objects.

Examples

Define a :class:`Var` and change it in various ways.

.. code-block::

  my_var = nx.Var(value = 5, min = 0, max = 10, fit = True, id = "my variable")

  my_var.FitRange(0, 7)

  print(my_var)

.. code-block::

   a = nx.Var(2, id = "my variable")

   # reassign a.value to 5
   a.value = 5

   # use of *= operator
   a *= 5  # = 25
 
   # use of / and - operators
   a = a / 3 - 1  # = 7.333333
 
   # use of +
   a + 2/3  # = 8

   print(a)

   # operator >> to set a.value to 2/3
   a >> 2/3
