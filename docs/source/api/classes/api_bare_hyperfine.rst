.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`BareHyperfine`
=======================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   

.. autoclass:: nexus.BareHyperfine
   :members:
   :exclude-members: thisown


.. autoclass:: nexus.VectorBareHyperfine
   :members:
   :exclude-members: thisown


Examples 

Define a :class:`BareHyperfine` object.

.. code-block::

   site = nx.BareHyperfine(
     weight = 1,
     isomer = -0.11,
     magnetic_field = 0,
     magnetic_theta = 0,
     magnetic_phi = 45,
     quadrupole = 0.8,
     quadrupole_alpha = 90,
     quadrupole_beta = 45,
     quadrupole_gamma = 0,
     quadrupole_asymmetry = 0.05,
     isotropic = False
     )
