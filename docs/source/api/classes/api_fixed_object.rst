.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`FixedObject`
====================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
 
.. autoclass:: nexus.FixedObject
   :members:
   :exclude-members: thisown
   :show-inheritance:
   :inherited-members:


Examples 

Define a :class:`FixedObject`.


.. code-block::

   factor = 0.876-0.965j

   mat = np.array([[0.5, 0.5j],
                   [-0.5j, 0.5]],
                   dtype=complex)

   fixed_object = nx.FixedObject(factor, mat, "my object")

   print(fixed_object)
