.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`Hyperfine`
==================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   

.. autoclass:: nexus.Hyperfine
   :members:
   :exclude-members: thisown


.. note:: A hyperfine site should not have too many distributions.
          Each distribution point number multiplies with the previously set distribution points.
          For example, having three distributions with each 30 points will result in 27000 distribution points in total for the site.
          In this case the calculations of the transitions takes quite long.
          As a rule of thumb try to stay below 1000 distribution points per :class:`Hyperfine` object.
          For 3D random distributions in the magnetic field and the quadrupole splitting set :attr:`Hyperfine.isotropic` to *True*.
          This will results in considerable faster calculations than setting two 3D distributions.


Examples 

Various ways to define a :class:`Hyperfine` object without distributions.

.. code-block::

   site = nx.Hyperfine(
     id = "user hyperfine site",
     weight = 1,
     isomer = -0.11,
     magnetic_field = 0,
     magnetic_theta = 0,
     magnetic_phi = 45,
     quadrupole = 0.8,
     quadrupole_alpha = 90,
     quadrupole_beta = 45,
     quadrupole_gamma = 0,
     quadrupole_asymmetry = 0.05,
     isotropic = False)

   site.quadrupole.value = 0

   site.magnetic_field = 33

   site.isotropic = True

   print(site)

different ways to define distributions on hyperfine parameters

.. code-block::

   gauss_dist_mag = nx.lib.distribution.Gaussian(points = 11, fwhm = 3)

   # initializer definition of magnetic field distribution
   site = nx.Hyperfine(isomer = -0.11,
                       magnetic_field = 33,
                       magnetic_field_dist = gauss_dist_mag)

.. code-block::

   site = nx.Hyperfine(isomer = -0.11,
                       magnetic_field = 33)

   # isomer distribution via set method
   gauss_dist_isomer = nx.lib.distribution.Gaussian(points = 11, fwhm = 0.2)
   
   site.SetIsomerDistribution(gauss_dist_isomer)


Random distributions have to be applied by set methods

.. code-block::

   site.SetRandomDistribution("efg", "3D", 201)

   site.SetRandomDistribution("mag", "2Dsk", 201)

