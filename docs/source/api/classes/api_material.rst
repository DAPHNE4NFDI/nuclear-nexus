.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`Material`
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   

.. autoclass:: nexus.Material
   :members:
   :exclude-members: thisown

.. autoclass:: nexus.Composition
   :members:
   :exclude-members: thisown

Examples


Various ways to define a :class:`Material`.

.. code-block::

   # non-resonant material
   Pt = nx.Material(id = "my Platinum",
                    composition = [["Pt", 1]],
                    density = 21.45
                    )

   print(Pt)

.. code-block::
   
   # material from Template
   Pt = nx.Material.Template(nx.lib.material.Pt)

.. code-block::

   # material in wt%
   steel_composition = [['Fe', 55], ['Cr', 25], ['Ni', 20]]  # in wt%

   steel_composition = nx.conversion.WtToAt(steel_composition)

   print(steel_composition)

   steel = nx.Material(id = "stainless steel",
                       composition = steel_composition,
                       density = nx.Var(value = 7.8, min = 7, max = 7.8, fit = True, id = "steel density")
                       )

   print(steel)

.. code-block::

   # resonant material
   site1 = nx.Hyperfine(magnetic_field = 33, isotropic = True)

   Fe = nx.Material(id = "iron",
                    composition = [["Fe", 1]],
                    density = 7.874,
                    isotope = nx.moessbauer.Fe57,
                    abundance = 0.02119,
                    lamb_moessbauer = 0.796
                    hyperfine_sites = [site1] )
