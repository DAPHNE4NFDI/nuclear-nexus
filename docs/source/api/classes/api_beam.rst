.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`Beam`
=============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. autoclass:: nexus.Beam
   :members:
   :exclude-members: thisown



Different ways to define a :class:`Beam`.

.. code-block::

  # initialize beam, standard is fully sigma polarized
  beam = nx.Beam()

  # set beam to pi polarization
  beam.LinearPi()

  print(beam.matrix)

  # set beam to unpolarized
  beam.Unpolarized()

  print(beam)

.. code-block::

  # set beam to sigma polarization via initialization parameters
  # Rectangular beam profile, beam with 1 mm height
  beam = nx.Beam(polarization = 1, mixing_angle = 0, profile = "r", fwhm = 1)

.. code-block::

  # set beam along pi polarization via initialization parameters
  beam = nx.Beam(polarization = 1, mixing_angle = 0, canting_angle = 90)

.. code-block::
  
  beam = nx.Beam()
  # set coherency matrix for unpolarized detection
  beam.SetCoherencyMatrix(np.array([[1,0], [0, 1]], dtype=complex))

.. code-block::
  
  beam = nx.Beam()
  # set Jones vector for fully sigma polarized beam
  beam.SetJonesVector(np.array([1,0], dtype=complex))

  print(beam.jones_vector)
  print(beam.matrix)

.. code-block::

  # circularly right polarized beam
  beam = nx.Beam(polarization = 1, mixing_angle = -90)

  print(beam.Polarization())
  print(beam.Coherence())

.. code-block::

  beam = nx.Beam()
  # linear polarization at 45 deg rotation
  beam.SetJonesVector(np.array([1, 0], dtype=complex), 45)