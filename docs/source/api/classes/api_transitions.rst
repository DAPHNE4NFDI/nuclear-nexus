.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`Transition`
===================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
  

.. autoclass:: nexus.Transition
   :members:
   :exclude-members: thisown


.. autoclass:: nexus.Transitions
   :members:
   :exclude-members: thisown

Example

.. code-block::

   site = nx.BareHyperfine(
    weight = 1,
    isomer = 0,
    magnetic_field = 33,
    magnetic_theta = nx.DegToRad(90),
    magnetic_phi = 0,
    quadrupole = 0,
    quadrupole_alpha = 0,
    quadrupole_beta = 0,
    quadrupole_gamma = 0,
    quadrupole_asymmetry = 0,
    isotropic = False
   )

   # returns a Transitions object
   nuclear_transitions = nx.HyperfineTransitions(site, nx.moessbauer.Fe57)

   # a single transition 
   print(nuclear_transitions[0])

   # energy detuning in units of Gamma
   print(nuclear_transitions[0].energy_detuning)

   # number of all transitions in the nucleus
   print(len(nuclear_transitions))

   # all transitions 
   for elem in nuclear_transitions:
     print(elem)



