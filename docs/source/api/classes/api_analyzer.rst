.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`Analyzer`
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. autoclass:: nexus.Analyzer
   :members:
   :exclude-members: thisown
   :show-inheritance:
   :inherited-members:


Different ways to define an :class:`Analyzer`.

.. code-block::

  # initialize analyzer
  analyzer = nx.Analyzer()

  # set to linear polarizer analysis along sigma direction
  analyzer.LinearSigma()

  print(analyzer.matrix)

  # redefine analyzer matrix to unpolarized detection
  analyzer.Unpolarized()

.. code-block::

  # set polarizer along sigma polarization via initialization parameters
  analyzer = nx.Analyzer(efficiency = 1, mixing_angle = 0)

.. code-block::

  # set polarizer along pi polarization via initialization parameters
  analyzer = nx.Analyzer(efficiency = 1, mixing_angle = 0, canting_angle = 90)

.. code-block::
  
  analyzer = nx.Analyzer()

  # set Jones matrix directly to unpolarized detection
  analyzer.SetJonesMatrix(np.array([[1,0], [0, 1]], dtype=complex))
