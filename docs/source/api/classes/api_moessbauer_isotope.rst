.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


:class:`MoessbauerIsotope`
==========================

.. toctree::
   :maxdepth: 3
   :caption: Contents:



.. autoclass:: nexus.MoessbauerIsotope
   :members:
   :exclude-members: thisown


Define a Moessbauer isotope. Predefined Moessbauer Isotopes can be found in the Material library :class:`Moessbauer`.

.. code-block::

  Fe57 = nx.moessbauerIsotope(
    isotope = "57-Fe",
    element = "Fe",
    mass = 56.9353933,
    
    energy = 14412.497,
    lifetime = 141.11, # input here in nanoseconds, is converted to seconds during initialization
  
    internal_conversion = 8.21,
    multipolarity = "M1",
    mixing_ratio_E2M1 = 0,
  
    spin_ground = 1/2,
    spin_excited = 3/2,
  
    gfactor_ground = 0.18121,
    gfactor_excited = -0.10348,
  
    quadrupole_ground = 0,
    quadrupole_excited = 0.187
  )

  print(Fe57.wavelength)

  copy_of_isotope = Fe57.Copy()
