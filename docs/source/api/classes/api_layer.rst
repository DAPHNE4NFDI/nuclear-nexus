.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _api-layer:


:class:`Layer`
==============

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   

.. autoclass:: nexus.Layer
   :members:
   :exclude-members: thisown



Examples


Various ways to define a :class:`Layer`.

.. code-block::

   # with a predefined material

   material_Pt = = nx.Material.Template(nx.lib.material.Pt)

   layer_Pt = nx.Layer(id = "Pt layer",
                       material = material_Pt,
                       thickness = 20,
                       roughness = 0.2,
                       thickness_fwhm = 0
                       )

   print(layer_Pt)


.. code-block::

   # material definition by the layer constructor

   layer_Pt = nx.Layer(id = "Pt layer",
                       composition = [["Pt", 1]],
                       density = 21.45,
                       thickness = 20,
                       roughness = 0.2,
                       thickness_fwhm = 0
                       )

   print(layer_Pt)
