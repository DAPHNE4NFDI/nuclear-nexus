.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _api-scattering-matrix:

Scattering Matrix
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


Funtions to calculate the forward or grazing-incidence scattering matrices.


.. autofunction:: nexus.ElectronicForwardScatteringFactor

.. autofunction:: nexus.ElectronicRefractiveIndex

.. autofunction:: nexus.ElectronicGrazingScatteringFactor

.. autofunction:: nexus.ForwardScatteringMatrix

.. autofunction:: nexus.GrazingScatteringMatrix

.. autofunction:: nexus.CriticalAngle


Examples

.. code-block::

   material = nx.Material(id = "my_material",
                          composition = [["Fe", 2], ["O", 3]],
                          density = 5.24))
   
   # k-vector along layer direction at an angle of 0.1 degree
   kz = nx.conversions.EnergyToKvectorZ(20e3, 0.1)

   scattering_factor = nx.ElectronicGrazingScatteringFactor(material, 20e3, kz)
   print(scattering_factor)


.. code-block::

   # scattering matrix at 57-Fe transition energy
   mat_Fe = nx.Material.Template(nx.lib.material.Fe_enriched)

   site1 = nx.Hyperfine(magnetic_field = 33)

   mat_Fe.hyperfinesites = [site1]

   detuning = np.linspace(-200, 200, 201)

   scattering_matrix = nx.ForwardScatteringMatrix(mat_Fe, nx.lib.moessbauer.Fe57, detuning)
   print(scattering_matrix)
