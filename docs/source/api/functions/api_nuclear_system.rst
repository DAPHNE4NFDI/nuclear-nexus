.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Nuclear transitions
===================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


Functions to calculate the nuclear transition energies and transition operators.


.. autofunction:: nexus.HamiltonianGroundState

Example

.. code-block::

   site = nx.BareHyperfine(
     magnetic_field = 33,
     magnetic_theta = nx.DegToRad(90)
   )

   # returns the ground state Hamiltonian
   hamiltonian = nx.HamiltonianGroundState(site, nx.moessbauer.Fe57)
   print(hamiltonian)


.. autofunction:: nexus.HamiltonianExcitedState

Example

.. code-block::

   site = nx.BareHyperfine(
     magnetic_field = 33,
     magnetic_theta = nx.DegToRad(90)
   )

   # returns the excited state Hamiltonian
   hamiltonian = nx.HamiltonianExcitedState(site, nx.moessbauer.Fe57)
   print(hamiltonian)

.. autofunction:: nexus.multipol_TLni

.. autofunction:: nexus.HyperfineTransitions

Example

.. code-block::

   site = nx.BareHyperfine(
     magnetic_field = 33,
     magnetic_theta = nx.DegToRad(90)
   )

   # returns a Transitions object
   nuclear_transitions = nx.HyperfineTransitions(site, nx.moessbauer.Fe57)

   # a single transition 
   print(nuclear_transitions[0])

   # energy detuning in units of Gamma
   print(nuclear_transitions[0].energy_detuning)

   # number of all transitions in the nucleus
   print(len(nuclear_transitions))

   # all transitions 
   for trans in nuclear_transitions:
     print(trans)



