.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Scattering Length
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


Functions to calculate the electronic and nuclear scattering lengths *E* and *N* and set the computation method.

.. autofunction:: nexus.GetAtomicScatteringFactorCXRO

.. autofunction:: nexus.SetAtomicScatteringFactorCXRO

.. autofunction:: nexus.ElectronicScatteringLength

.. autofunction:: nexus.ElectronicScatteringLengthTheory

.. autofunction:: nexus.ElectronicScatteringLengthCXRO

.. autofunction:: nexus.ElectronicScatteringLengthMatrix

.. autofunction:: nexus.NuclearScatteringLength

.. autofunction:: nexus.MaterialWeightedScatteringLength

.. autofunction:: nexus.MaterialWeightedScatteringLengthMatrix

.. autofunction:: nexus.MaterialWeightedNuclearScatteringLength

.. autofunction:: nexus.MaterialWeightedCoherentScatteringLength


Examples

.. code-block::

   # electronic scattering length of 20 keV photons in iron
   Fe = nx.Element(element = "Fe")

   scattering_length = nx.ElectronicScatteringLength(Fe, 20e3, cxro = True)
   print(scattering_length)


.. code-block::

   # nuclear scattering length at 57-Fe transition energy
   mat_Fe = nx.Material.Template(nx.lib.material.Fe_enriched)

   site1 = nx.Hyperfine(magnetic_field = 33)

   mat_Fe.hyperfinesites = [site1]

   detuning = np.linspace(-200, 200, 201)

   scattering_length = nx.NuclearScatteringLength(mat_Fe, nx.lib.moessbauer.Fe57, detuning)
   print(scattering_length)
