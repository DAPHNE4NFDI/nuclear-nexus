.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Fourier transform
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


General functions used for the discrete Fourier transform (DFT).

.. versionadded:: 1.0.4

.. autofunction:: nexus.SamplingFrequency

.. autofunction:: nexus.Step

.. autofunction:: nexus.Resolution


.. note:: Parceval's theorem is
  
  .. math:: \sum_{k=0}^{N-1} \left| x(orig\_step)_k \right|^2 = \frac{1}{N} * \sum_{n=0}^{N-1} \left|X(fourier\_step)_n \right|^2


.. autofunction:: nexus.NormalizationN

.. autofunction:: nexus.NormalizationSqrtN

.. autofunction:: nexus.NormalizationNIntegratedIntensity

.. autofunction:: nexus.NormalizationNIntegratedIntensityScaled

.. autofunction:: nexus.NormalizationIntegratedIntensityScaled


.. note:: For window functions see `<https://en.wikipedia.org/wiki/Window_function>`_

.. autofunction:: nexus.Welch

.. autofunction:: nexus.Sine

.. autofunction:: nexus.Hann

.. autofunction:: nexus.Hamming

.. autofunction:: nexus.Kaiser
