.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Electronic scattering
=====================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


Funtions to calculate electronic scattering cross sections for X-ray photons.

.. autofunction:: nexus.KleinNishinaCrossSection

.. autofunction:: nexus.ComptonCrossSection

.. autofunction:: nexus.ThomsonCrossSection

.. autofunction:: nexus.PhotoCrossSection

.. autofunction:: nexus.ElectronicCrossSection


Examples

.. code-block::

   Au = nx.Element("Au")

   # Klein-Nishina cross section for photons of energy E = 10 keV at gold
   cross_section = nx.KleinNishinaCrossSection(Au, 10000)
   print(cross_section)

   # Photoelectric cross section for photons of energy E = 10 keV at gold
   cross_section = nx.PhotoCrossSection(Au, 10e3)
   print(cross_section)

   # Total cross section for photons of energy E = 10 keV at gold
   cross_section = nx.ElectronicCrossSection(Au, 10e3)
   print(cross_section)
