.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Quantum mechanics
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


General functions used in quantum mechanics.

.. autofunction:: nexus.KroneckerDelta

.. autofunction:: nexus.Factorial

Example

.. code-block::

   fac = nx.Factorial(3)

   fac = nx.Factorial(3.0)


.. autofunction:: nexus.ClebshGordon

Example

.. code-block::

   clebshgordon = nx.ClebshGordon(1/2, 1, -1/2, 1, 3/2, 1/2)



.. autofunction:: nexus.ThreeJSymbol


.. autofunction:: nexus.VCoefficient


.. autofunction:: nexus.TriangleCoefficient


.. autofunction:: nexus.WCoefficient

 
.. autofunction:: nexus.SixJSymbol


.. autofunction:: nexus.SmallWignerDmatrix


Example

.. code-block::

   matrix_element = nx.SmallWignerDmatrix(1, 1, 1, nx.pi/2.5)




.. autofunction:: nexus.WignerDmatrix

Example

.. code-block::

   matrix_element = nx.WignerDmatrix(1, 1, 1, nx.pi/2.5, nx.pi/1.5, nx.pi/2)


.. autofunction:: nexus.Hermite


.. autofunction:: nexus.Laguerre
   
   
.. autofunction:: nexus.AssociatedLaguerre


.. autofunction:: nexus.Legendre


.. autofunction:: nexus.AssociatedLegendre


.. autofunction:: nexus.SphericalHarmonics


.. autofunction:: nexus.DiagonalizeHermitian

Example

.. code-block::

   site = nx.BareHyperfine(
     magnetic_field = 33,
     magnetic_theta = nx.DegToRad(90)
   )

   # returns the ground state Hamiltonian in units of linewidth
   hamiltonian = nx.HamiltonianGroundState(site, nx.moessbauer.Fe57)

   # returns the real eigenvalues and eigenvectors
   # the eigenvalues give the detuning energy in units of linewidth
   eigen_system = nx.DiagonalizeHermitian(hamiltonian)

   # list of eigenvalues
   print(eigen_system.eigenvalues)

   for vector in eigen_system.eigenvectors:
     print("Eigenvector")
     print(vector)


.. autofunction:: nexus.VecSpherHarmPolarization

Example

.. code-block::

    print("E1, L=1, lam= 1")
    VSH = nx.VecSpherHarmPolarization(1,1)
    print(VSH[0].sigma)
    print(VSH[0].pi)
    print(VSH[1].sigma)
    print(VSH[1].pi)
    print(VSH[2].sigma)
    print(VSH[2].pi)


    print("M1, L=1, lam= 0")
    VSH = nx.VecSpherHarmPolarization(1,0)
    print(VSH[0].sigma)
    print(VSH[0].pi)
    print(VSH[1].sigma)
    print(VSH[1].pi)
    print(VSH[2].sigma)
    print(VSH[2].pi)


    print("E2, L=2, lam= 1")
    VSH = nx.VecSpherHarmPolarization(2,1)
    print(VSH[0].sigma)
    print(VSH[0].pi)
    print(VSH[1].sigma)
    print(VSH[1].pi)
    print(VSH[2].sigma)
    print(VSH[2].pi)
    print(VSH[3].sigma)
    print(VSH[3].pi)
    print(VSH[4].sigma)
    print(VSH[4].pi)
