﻿.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _api-data:


:mod:`data`
===========

.. toctree::
   :maxdepth: 3
   :caption: Contents:


.. automodule:: nexus.data
    :members:


Examples

loading data

.. code-block::

   # load a time_spectrum
   # column index from 0.
   time_data, int_data = nx.data.load('data_folder\example_time_spectrum.dat', 
                                       x_index = 1,  # time data are in column 1
                                       intensity_index = 2,  # intensity data in column 2
                                       x_start = 20,  # load only data with time data larger 20
                                       x_stop = 200,  # load only data with time data smaller 200
                                       intensity_threshold = None)  # not intensity threshold

saving data

.. code-block::
   
   # save some data arrays
   num_points = 11

   angles = np.linspace(0.1, 5, num_points)

   intensity = np.random.rand(num_points)
      
   nx.data.save('example_file.txt', [angles, intensity])


folding data

.. code-block::

   def gauss(x, offset):
     return np.exp(-1/2*(x-offset)**2/0.5*2)

   arr = np.linspace(-8, 8, 100)

   dat = gauss(arr, -4) + gauss(arr, 4.6)

   folded_dat, shift_index = nx.data.Fold(dat)
