﻿.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _api-conversions:


:mod:`conversions`
==================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


.. note::  :mod:`conversions` is also available under ``nexus.``
      For example, ``nx.conversions.DegToRad(degree)`` is also available as ``nx.DegToRad(degree)``.


.. automodule:: nexus.conversions
    :members:


Examples

.. code-block::

  angle = 2100  # in microrad
  print(nx.conversions.UradToDeg(angle))

  angle = 40  # in degree
  print(nx.DegToMrad(angle))

.. code-block::

  velocity = np.linspace(-10, 10, 201)  # mm/s

  detuning = nx.conversions.VelocityToGamma(velocity, nx.moessbauer.Fe57)

.. code-block::

  comp = (['Fe', 2], ['O', 3])

  comp = nx.conversions.AtToWt(comp)
  print(comp)

  comp = nx.conversions.WtToAt(comp)
  print(comp)

.. code-block::

  comp = (['Fe', 0.4], ['O', 0.6])

  comp = nx.converions.AtToWt(comp)
  print(comp)

  comp = nx.conversions.WtToAt(comp)
  print(comp)