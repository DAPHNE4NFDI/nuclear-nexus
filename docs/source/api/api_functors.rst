.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Measurement Classes
===================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   functors/api_measurement.rst
   functors/api_fit_measurement.rst
   functors/api_amplitude_spectrum.rst
   functors/api_amplitude_time.rst
   functors/api_emission_spectrum.rst
   functors/api_electronic.rst
   functors/api_energy_spectrum.rst
   functors/api_energy_time_amplitude.rst
   functors/api_energy_time_spectrum.rst
   functors/api_field_amplitude.rst
   functors/api_field_intensity.rst
   functors/api_integrated_energy_time_spectrum.rst
   functors/api_moessbauer_spectrum.rst
   functors/api_nuclear_reflectivity.rst
   functors/api_nuclear_reflectivity_energy.rst
   functors/api_reflectivity.rst
   functors/api_reflectivity_amplitude.rst
   functors/api_time_spectrum.rst
   functors/api_transmission.rst
   functors/api_transmission_amplitude.rst
