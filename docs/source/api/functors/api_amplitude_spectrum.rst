
:class:`AmplitudeSpectrum`
==========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autoclass:: nexus.AmplitudeSpectrum
   :members:
   :exclude-members: thisown
   :show-inheritance:
   :inherited-members:
