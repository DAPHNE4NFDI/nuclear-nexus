.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Object Classes
==============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

 
   classes/api_analyzer.rst
   classes/api_beam.rst
   classes/api_bare_hyperfine.rst
   classes/api_conuss_object.rst
   classes/api_distribution.rst
   classes/api_eff_dens_model.rst
   classes/api_eigensystem.rst
   classes/api_element.rst
   classes/api_experiment.rst
   classes/api_fixed_object.rst
   classes/api_function_time.rst
   classes/api_hyperfine.rst
   classes/api_layer.rst
   classes/api_material.rst
   classes/api_moessbauer_isotope.rst
   classes/api_nxobject.rst
   classes/api_residual.rst
   classes/api_basic_sample.rst
   classes/api_forward_sample.rst
   classes/api_grazing_sample.rst
   classes/api_sample.rst
   classes/api_transitions.rst
   classes/api_nxvariable.rst
   