﻿.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Libraries
=========

.. _api-distribution:

:mod:`distribution`
-------------------

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: nexus.lib.distribution
    :members:

Example

.. code-block::

    import matplotlib.pyplot as plt

    # Gaussian distribution object from nexus.lib.distribution.
    gauss_dist = nx.lib.distribution.Gaussian(points = 101, fwhm = nx.Var(3, min = 0, max = 7, fit = True))

    # Call of the distribution function. delta and weight are set by the call.
    gauss_dist.DistributionFunction()
        
    plt.plot(gauss_dist.delta, gauss_dist.weight)
    plt.show()


.. _api-material:

:mod:`material`
---------------

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: nexus.lib.material
    :members:


.. _api-moessbauer:

:mod:`moessbauer`
-----------------

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: nexus.lib.moessbauer
    :members:

    
.. _api-energy:

:mod:`energy`
-------------

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: nexus.lib.energy
    :members:


.. _api-residual:

:mod:`residual`
---------------

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: nexus.lib.residual
    :members: