.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. image:: nexus_logo.png

Nexus
=====

The *Nuclear Elastic X-ray scattering Universal Software* (*Nexus*) is a *Python* package for simulating and fitting of

* Moessbauer spectra
* nuclear resonant scattering (NRS) data
* pure electronic X-ray reflectivities (XRR)
* nuclear X-ray reflectivities (nXRR)
* polarization dependent electronic scattering.

Its C++ implementation offers fast calculations and dedicated fit algorithms to simulate modern type of Moessbauer or nuclear resonant scattering experiments.
The object-oriented approach offers high flexibility to combine an arbitrary number of objects in the beam path, to define arbitrary samples and to calculate the complete electronic and nuclear response of the experiment.

*Nexus* has been developed in need to fit multiple measurements in parallel and to simulate, design and evaluate complex experimental setups as realized in nuclear quantum optics.
One of the big problems was that different software tools had to be used for different purposes, like fitting of NRS experiments, fitting of reflectivities, getting theoretical values from calculations, or using different theoretical models.
Often the models were not consistent, like different treatment of certain parameters (surface roughness for example) in the different software tools.
Not all experimental conditions were fittable with the available software tools.
*Nexus* is designed to unite and extend the different strengths of the other software tools in one package to consistently evaluate experiments found in Moessbauer science.

*Nexus* offers several features that are useful in the evaluation of any kind of experiments:

   * An easy to use and install *Python* package.
   * Only one software package and interface for all kinds of applications in Moessbauer science.
   * The *Python* interface makes *Nexus* totally integrable in *Python* workflows like experiment/sample design, data plotting, data evaluation, simulating, fitting, and so on.
   * Full quantum theory of the nuclear transitions including all multipole and mixed transitions known in Moessbauer isotopes.
   * Transfer matrix formalism for the photon field propagation for complete determination of the scattered radiation field.
   * High performance due to parallel C++ implementation and dedicated C++ math libraries.
   * Modern non-linear optimization algorithms for reliable and flexible fitting.
   * Combined fitting of multiple measurements and experiments.
   * Automatized parameter optimization for experiment design.
   * Large documentation, curated software.

For more information go to the :ref:`Introduction`.

There will be more features under development depending on user request:

   * Calculations of time dependent phenomena in the time and space picture [Shvydko]_.
   * Stochastic relaxation phenomena.
   * Scattering from gratings.
   * Scattering from crystals.
   * GUI.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   introduction/introduction.rst
   installation/installation.rst
   theory.rst
   scattering_geometry.rst
   tutorial/tutorial.rst
   examples/examples.rst
   api/api.rst
   news/news.rst
   software.rst
   bibliography.rst
   partners.rst
   license.rst
   legal.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
