.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _partners:


Partners
--------

Several data and software projects cover FAIR principles of research data.
FAIR stands for Findability, Accessibility, Interoperability, and Reuse.
Guidelines of FAIR principles can be found here https://www.go-fair.org/fair-principles/.


DAPHNE
******

DAPHNE is a German consortium dealing with FAIR principles.
*Nexus* is part of `DAPHNE4NFDI <https://www.daphne4nfdi.de/english/>`_ and you can find the repository also at https://codebase.helmholtz.cloud/DAPHNE4NFDI/nuclear-nexus.


PaNOSC
******

The Photon and Neutron Open Science Cloud `PaNOSC <https://www.panosc.eu/>`_ is a European project for making data FAIR.
You can find *Nexus* on the `pan-training <https://pan-training.eu/>`_ platform.


Software catalogues
*******************

*Nexus* is indexed at the `PaNdata software catalogue <https://software.pan-data.eu/>`_ and the `Helmholtz research software diretory <https://helmholtz.software/software/nuclear-nexus>`_.
