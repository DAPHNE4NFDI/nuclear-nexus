.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-beam:


Beam
====

A :class:`Beam` object is described by its polarization state and the beam profile.
The beam energy is not given here and automatically chosen by *Nexus* from the resonant isotope for nuclear scattering methods.
For pure electronic calculations the energy is specified in the :class:`Measurement`.

The coherency matrix of the beam (in version 1) is given by [Sturhahn2000]_

.. math::

    J =
    \frac{1}{2}
    \begin{pmatrix}
    1 + P \cos{\chi} \cos{2\varphi}  & P (\sin{2\varphi}\cos{\chi} -i \sin{\chi}) \\
    P (\sin{2\varphi}\cos{\chi}  + i \sin{\chi}) & 1 - P \cos{\chi} \cos{2\varphi}
    \end{pmatrix}

where :math:`P` is the polarization, :math:`\chi` is the mixing angle, and :math:`\varphi` is the canting angle (which is corrected by a factor of 2 compared to [Sturhahn2000]_ to account for the correct polarization rotation).

.. admonition:: Changed in version 2.0.0

   The mixing angle is changed by a factor of 2 in order to be consistet with the theory of optics (e.g. [Born]_), see :ref:`scattering-geometry` . 

   The three parameters :math:`P`, :math:`\chi` and :math:`\varphi` are fittable now.

The actual coherency matrix used since version 2 is given by the unpolarized coherency matrix :math:`\tfrac{1}{2} I_2` and the normalized Jones vector :math:`\boldsymbol{j}` [Gil]_

.. math::

   J = (1-P) \tfrac{1}{2} I_2 + P \boldsymbol{j} \boldsymbol{j}^{\dagger}

with

.. math::

    \boldsymbol{j} = R(\varphi)
    \begin{pmatrix}
    \cos\chi \\
    i \sin\chi  
    \end{pmatrix}
    =
    \begin{pmatrix}
    \cos\varphi & -\sin\varphi \\
    \sin\varphi & \cos\varphi 
    \end{pmatrix}
    \begin{pmatrix}
    \cos\chi \\
    i \sin\chi  
    \end{pmatrix}.

where :math:`R` is the rotation matrix.
So both the matrix and the Jones vector are set properly since version 2.
The mixing angle now corresponds to the ellipticiy angle of the ellipse of the Jones vector.
One obtains

.. math::

    J = \frac{1}{2}
        \begin{pmatrix}
        1 + P \cos{2\chi} \cos{2\varphi}  & P (\sin{2\varphi}\cos{2\chi} -i \sin{2\chi}) \\
        P (\sin{2\varphi}\cos{2\chi}  + i \sin{2\chi}) & 1 - P \cos{2\chi} \cos{2\varphi}
        \end{pmatrix}

It leads to the same matrix as used in verison 1 but the mixing angle multiplied by a factor of 2.

**The following parameters are** :class:`Var` **objects and can be fit:**
  
  * FWHM
  * polarization (since v2.0.0)
  * mixing_angle (since v2.0.0)
  * canting_angle (since v2.0.0)


Polarization properties
-----------------------

When you initialize a beam you can specify several parameters (which are similar to the parameters defined in *CONUSS*).
Those are the degree of polarization, the mixing angle between the polarization components, and the canting angle of the beam with respect to the :math:`\sigma` direction.
The definition is such that circular left polarized light (mixing angle = +45deg since version 2.0.0) carries a spin angular momentum of :math:`+\hbar`.

.. code-block::

    import nexus as nx
    import numpy as np

    # set beam along pi polarization via initialization parameters
    # those values are the default arguments
    beam = nx.Beam(polarization = 1, mixing_angle = 0, canting_angle = 90)
    
    print(beam)

The beam is described by different properties:

* the Jones vector for fully coherent beams only.
* the coherency matrix (a density matrix) for arbitrary polarizations.

When you define a Jones vector, the coherency matrix will automatically be calculated.

.. versionchanged:: 2.0.0 
   The coherency matrix will not be set anymore.

Setting a coherency matrix will not set a Jones vector, because it is not possible in general.

.. versionremoved:: 2.0.0 
   Setting the coherency matrix directly is not possible anymore.

The default arguments of ``nx.Beam()`` create a linearly polarized beam along :math:`\sigma` direction.

.. versionchanged:: 2.0.0
   The default method sets a unpolarized beam.

The initialization parameters will not create a Jones vector but only the coherency matrix of the beam.

.. versionchanged:: 2.0.0
   A Jones vector is created from input parameters.

If you want an unpolarized beam you can use the ``beam.Unpolarized()`` method instead of setting the polarization to zero.

When you want to calculate amplitude methods a valid Jones vector must be given.
There are methods to set specific polarizations, and if the beam is fully polarized, they will also create the appropriate Jones vector.
Those are 

* ``beam.LinearSigma()``
* ``beam.LinearPi()``
* ``beam.CircularLeft()``
* ``beam.CircularRight()``

You canset the Jones vector of the beam via

.. code-block::

    beam = nx.Beam()
    beam.CircularLeft()

    print(beam.matrix)

    print(beam.jones_vector)

    print("\nset new Jones vector")
    # set Jones vector for a fully polarized beam at 45 deg from the sigma direction
    # angle will rotate the refernece plane away from sigma direction
    beam.SetJonesVector(np.array([1.0+0j, 0.0]),
                        angle = 45)
  
    print(beam.jones_vector)  
    print(beam.ValidJonesVector())
    print(beam.MatrixFromJonesVector())  # only in version 2.0.0

.. code-block::

    [[ 0.5+0.j  -0. -0.5j]
     [ 0. +0.5j  0.5+0.j ]]
    [[0.70710678+0.j        ]
     [0.        +0.70710678j]]

    set new Jones vector
    [[0.70710678+0.j]
     [0.70710678+0.j]]
    True
    [[0.5+0.j 0.5+0.j]
     [0.5+0.j 0.5+0.j]]

You can also ask for the (complex) coherency, phase difference and polarization of the beam specifically

.. code-block::

    beam.CircularLeft()
    
    print(beam.Polarization())   
    
    print(beam.Coherence())
    
    print(beam.PhaseDifference())

    print(beam.ComplexCoherence())

.. code-block::

   1.0
   0.9999999999999998
   -1.5707963267948966
   (-0-0.9999999999999998j)

If one of the methods return ``nan``, one :math:`E`-vector component is zero.


Size properties
---------------

In grazing incidence geometry the beam shape and size have to be given.
The profile is either Gaussian ``g`` or rectangular ``r`` and the width parameter :attr:`fwhm` is given as the full width half maximum value (in mm) of the specified profile.
The profile and the size will determine how much of the beam actually illuminates the sample and therefore influence the reflectivity at low angles.

.. versionchanged:: 1.0.2
   Rectangular beam profile correction wrong prior to version 1.0.2

.. code-block::

    beam = nx.Beam(polarization = 1,
                   mixing_angle = 0,
                   profile = "g",   # Gaussian beam profile
                   fwhm = 0.21)     # in mm

For a beam, only the beam profiles width :attr:`fwhm` is a fittable :class:`Var` object.

In case you want to calculate a theoretical reflectivity without beam size correction, set the :attr:`fwhm` to zero.


.. seealso:: https://en.wikipedia.org/wiki/Photon_polarization

             https://en.wikipedia.org/wiki/Spin_angular_momentum_of_light

             https://en.wikipedia.org/wiki/Circular_polarization

             https://en.wikipedia.org/wiki/Elliptical_polarization

             https://en.wikipedia.org/wiki/Jones_calculus

             https://en.wikipedia.org/wiki/Density_matrix


Notebooks
---------

`beam`_ - :download:`nb_beam.ipynb`.

.. _beam: nb_beam.ipynb


Please have a look to the :ref:`sec-API` for more information.
