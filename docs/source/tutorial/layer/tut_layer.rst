.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-layer:

Layer
=====

A layer is made of a :class:`Material` and has a certain thickness.
The other two lateral dimensions are assumed to be infinite.
It also holds information on the thickness distribution via the :attr:`thickness_fwhm` in forward scattering or via the :attr:`roughness` in grazing-incidence geometry.

**The following parameters are** :class:`Var` **objects and can be fit:**

  * thickness
  * roughness
  * thickness fwhm

All values are given in nanometer.


Thickness
---------

The thickness defines the only dimension given for a :class:`Layer`.
In forward geometry the thickness is given along the beam propagation direction.
In gracing incidence geometry the thickness is assumed to be almost perpendicular to the beam propagation direction, depending on the incidence angle specified in the :class:`Sample`.


Thickness FWHM
--------------

This parameter is only used in forward scattering geometry.
The :attr:`thickness_fwhm` value gives the full width half maximum of an assumed Gaussian distribution of thickness values around :attr:`thickness`.
Contributions to the scattering intensity originating from different thicknesses are treated incoherently, see :ref:`tut-thickness-distribution`.


Roughness
---------

The roughness is only taken into account in grazing incidence geometry.
The :attr:`roughness` specifies the :math:`\sigma` value of a Gaussian distribution around the :attr:`thickness` value.
In contrast to the :attr:`thickness_fwhm`, the :attr:`roughness` is treated coherently.
How the roughness is treated is defined in the :class:`Sample` by the specified roughness model, see :ref:`tut-sample`.


Setting up a layer
------------------

.. code-block::

    import nexus as nx

    mat = nx.Material.Template(nx.lib.material.Fe2O3)

    layer = nx.Layer(id = "my iron oxide layer",
                     thickness = 1000,  # in nanometer
                     material = mat,
                     roughness = 30,
                     thickness_fwhm = 50)

    print(layer)

Here, we have defined a material first, followed by the :class:`Layer` definition.
You can define the :class:`Material` directly in the layer initialization.
Then, a new :class:`Material` object is automatically created by the :class:`Layer`.
This is helpful in setting up the experiment for non-resonant calculations and fitting.

.. code-block::

    layer = nx.Layer(id = "my iron oxide layer",
                     thickness = 1000,  # in nanometer
                     roughness = 30,
                     thickness_fwhm = 50
                     composition = [["Fe", 2], ["O", 3]],
                     density = 5.3)
    
    print(layer)

For convenience, the density and composition of a :class:`Material` can also be accessed via the :class:`Layer` directly.
This is not possible for nuclear properties of the :class:`Material`.

.. code-block::
    
    # access density via material properties
    layer.material.density = 6.1

    print(layer)

    # access density via layer directly
    layer.density = 4.9

    print(layer)

.. note::

    Changing material properties via one :class:`Layer` will change all :class:`Layers` referencing to the same material.
    Therefore, this procedure should only be done when the material was defined automatically in the :class:`Layer` initialization.
    Then, no other :class:`Layer` is using this :class:`Material`.


Layer methods
-------------

There are several methods to retrieve the scattering matrix, refractive index, critical angle and many other properties of a layer.
For example,

.. code-block::

    # refractive index of a layer
    ref_index = layer.ElectronicRefractiveIndex(energy = 14412.5)

    print(ref_index)

    # outputs (0.9999952911043145+1.4925088081386838e-07j)


.. code-block::

    # critical angle at 16 keV
    ang = layer.CriticalAngle(energy=16e3)

    print(ang * 180/np.pi)

    # outputs (0.1583868640530339)


Notebooks
---------

`layer`_ - :download:`nb_layer.ipynb`.

.. _layer: nb_layer.ipynb


Please have a look to the :ref:`sec-API` for more information.
