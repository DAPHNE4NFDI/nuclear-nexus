﻿.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-hyperfine:


Hyperfine parameters
====================

The hyperfine parameters define the energy of the Hamiltonian for the different spin states of the nucleus.
For a Moessbauer isotope one has to consider the isomer shift, the quadrupole splitting and the magnetic hyperfine field.
The quantization system for the hyperfine parameters is set to the coordinates system of the beam 

.. math:: \begin{bmatrix}
           \sigma \\
           \pi \\
           k
          \end{bmatrix}

and all angles of the spherical hyperfine parameters are referenced to this coordinate system.

**The following parameters are** :class:`Var` **objects and can be fit for :class:`Hyperfine` site:**
  
* relative weight of the site
* isomer shift
* magnetic field
* magnetic polar angle
* magnetic azimuthal angle
* quadrupole slitting
* quadrupole ZYZ extrinsic Euler angle alpha
* quadrupole ZYZ extrinsic Euler angle beta
* quadrupole ZYZ extrinsic Euler angle gamma
* quadrupole asymmetry

.. versionadded:: 1.2.0

* texture
* Lamb-Moessbauer factor
* broadening


A :class:`Hyperfine` object with all attributes defined looks like this

.. code-block::

   hyperfine = nx.Hyperfine(
      id = "example parameter",
      weight = 1,                  # relative weight of the site in the material
      isomer = -0.11,              # isomer shift in mm/s
      magnetic_field = 33,         # magnetic field in T
      magnetic_theta = 0,          # polar angle
      magnetic_phi = 45,           # azimuthal angle
      quadrupole = 0.8,            # quadrupole slitting in mm/s
      quadrupole_alpha = 90,       # ZYZ extrinsic Euler angle alpha
      quadrupole_beta = 45,        # ZYZ extrinsic Euler angle beta
      quadrupole_gamma = 0,        # ZYZ extrinsic Euler angle gamma
      quadrupole_asymmetry = 0.05, # Asymmetry parameter
      texture = 1.0,               # texture coefficient
      lamb_moessbauer = 1.0,       # site specific Lamb-Moessbauer factor
      broadening = 1.0,            # broadening parameter of the linewidth
      isomer_dist = None,
      magnetic_field_dist = None,
      magnetic_theta_dist = None,
      magnetic_phi_dist = None,
      quadrupole_dist = None,
      quadrupole_alpha_dist = None,
      quadrupole_beta_dist = None,
      quadrupole_gamma_dist = None,
      quadrupole_asymmetry_dist = None,
      isotropic = False)           # 3D distribution in mag and EFG

    print(hyperfine)

.. code-block::

    Hyperfine .id: example parameter
      .weight =               1.0
      .isomer_shift =         -0.11    	 dist points: 1
      .magnetic_field =       33.0    	 dist points: 1
      .magnetic_theta =       0.0    	 dist points: 1
      .magnetic_phi =         45.0    	 dist points: 1
      .quadrupole =           0.8    	 dist points: 1
      .quadrupole_alpha =     90.0    	 dist points: 1
      .quadrupole_beta =      45.0    	 dist points: 1
      .quadrupole_gamma =     0.0    	 dist points: 1
      .quadrupole_asymmetry = 0.05    	 dist points: 1
      .isotropic =            False    	 3D distribution in mag and efg. Random mag or efg distributions are ignored.
      random magnetic distribution: none	 dist points: 1
      random quadrupole distribution: none	 dist points: 1
      total number of distribution points: 1

In the initialization the parameters and their distributions can be set.
:class:`Distributions` are special objects, which we will cover in the next section.
Here, the basic hyperfine parameters are covered.


Weight
------

In case you specify more than one site to a material, this parameter gives the relative contribution of the hyperfine site to the material.
The values are normalized to the total relative weight of all sites.


Isomer shift
------------

The isomer shift is isotropic and no angles have to be defined.

.. seealso:: `<https://en.wikipedia.org/wiki/Isomeric_shift>`_


Magnetic hyperfine field
------------------------

For the magnetic field its field strength and two angles are given.
The polar angle :math:`\vartheta` is defined from the :math:`k` vector.
The azimuthal angle :math:`\varphi` is defined in the :math:`\sigma - \pi` plane from the :math:`\sigma` direction.

.. image:: ../../coordinates.png

For a magnetic field along :math:`k` (Faraday geometry) set the :class:`Hyperfine` parameters to

.. code-block::

   hyperfine = nx.Hyperfine(
      magnetic_field = 33,         # magnetic field in T
      magnetic_theta = 0,          # polar angle
      magnetic_phi = 0)            # azimuthal angle

For a magnetic field along :math:`\sigma` set it to

.. code-block::

   hyperfine = nx.Hyperfine(
      magnetic_field = 33,         # magnetic field in T
      magnetic_theta = 90,         # polar angle
      magnetic_phi = 0)            # azimuthal angle

and for a magnetic field along :math:`\pi` to

.. code-block::

   hyperfine = nx.Hyperfine(
      magnetic_field = 33,         # magnetic field in T
      magnetic_theta = 90,         # polar angle
      magnetic_phi = 90)           # azimuthal angle

A magnetic field can be given in the *Nexus* frame can be converted to spherical coordinates and vie versa with the functions :func:`MagVectorToSpherical` and :func:`MagSphericalToVector`.

.. versionadded:: 1.0.2

.. code-block::

    # Nexus frame (sigma, pi, k)
    # spherical (mag, polar, azimuthal)
    
    # convert a magnetic field along pi direction to spherical coordinates
    print(nx.MagVectorToSpherical(0, 33, 0))

    # convert a magnetic field along k direction to Nexus frame
    print(nx.MagSphericalToVector(33, 0, 0))

.. code-block::

   (33.0, 90.0, 90.0)

   (0.0, 0.0, 33.0)

.. seealso:: `<https://en.wikipedia.org/wiki/Hyperfine_structure>`_


Electric field gradient and quadrupole splitting
------------------------------------------------

The electric field gradient tensor is defined by the largest principal tensor component :math:`V_{zz}`, the asymmetry parameter :math:`\eta`, and three Euler angles.
In *Nexus*, instead of :math:`V_{zz}` the quadrupole splitting :math:`QS` in mm/s is given.
The quadrupole energy is

.. math:: \frac{eQ_iV_{zz}}{2} = \frac{E_{trans}}{c} QS,

where :math:`Q_i` is the quadrupole moment of either ground or excited state.

The given value of the quadrupole splitting is the value of the split state for isotopes with either ground or excited state split.
For example, for Fe-57 (:math:`I_g = 1/2` and :math:`I_e = 3/2`) only the excited state is split, while for Ir-193 (:math:`I_g = 3/2` and :math:`I_e = 1/2`) only the ground state is split.
For isotopes with split ground and excited state (e.g. Ru-99 with :math:`I_g = 5/2` and :math:`I_e = 3/2`), the value gives the quadrupole energy of the excited state (with :math:`Q_e`) and the quadrupole energy of the ground state is scaled by the ratio :math:`Q_g/Q_e`.

.. seealso:: `<https://en.wikipedia.org/wiki/Electric_field_gradient>`_

             `<https://en.wikipedia.org/wiki/Quadrupole_splitting>`_

             For the energy conversion see `<https://en.wikipedia.org/wiki/Doppler_effect>`_

The angles are the extrinsic Euler angles in ZYZ convention. 
The angles are thus defined as rotations around the internal fixed-frame coordinate system of *Nexus* and not around the EFG coordinate system (as in CONUSS).
The convention differs in the meaning of the angles :math:`\alpha` and :math:`\gamma`.
They are exchanged in the extrinsic and intrinsic convention.

.. note:: If you want to use Euler angles as in *CONUSS* just exchange the values of :math:`\alpha` and :math:`\gamma` in the definition of :class:`Hyperfine`.

The angle :math:`\alpha` is only needed in case of an asymmetry in the EFG.
The ZYZ convention is as follows

   1. rotation around :math:`\vec{k}`
   2. rotation around :math:`\vec{\pi}`, 
   3. rotation around :math:`\vec{k}`.

Without rotation, the EFG main axis :math:`V_{zz}` points along :math:`\vec{k}`.
The two components :math:`V_{xx}` and :math:`V_{yy}` point along :math:`\sigma` and :math:`\pi`, respectively.


As an example we rotate the main axis :math:`V_{zz}` to the :math:`\pi` direction.
The first rotation is arbitrary as it will rotate  :math:`V_{xx}` and :math:`V_{yy}`, which is not important without asymmetry.
The second rotation around :math:`\vec{\pi}` by 90 degree will rotate :math:`V_{zz}` to :math:`\vec{\sigma}`.
The third rotation around :math:`\vec{k}` by 90 degree will rotate it to :math:`\vec{\pi}`.

.. image:: coordinates_efg.jpg

The Euler angles are thus :math:`\alpha = 0`, :math:`\beta = \pi/2` and :math:`\gamma = \pi/2`.
And in *Nexus* we have

.. code-block::

   hyperfine = nx.Hyperfine(quadrupole = 0.8,
                            quadrupole_alpha = 0,
                            quadrupole_beta = 90,
                            quadrupole_gamma = 90)

Finding the correct Euler angles by hand can be a tedious task and is prone to error.
Therefore, *Nexus* provides conversion functions in the :mod:`euler` module.
The same results can be obtained by the functions :attr:`VectorsToAngles`.

If you only want to set the main axis :math:`V_{zz}`, use

.. code-block::

   # input vector in the nexus coordinate system [sigma,pi,k]
   alpha, beta, gamma = nx.euler.VectorsToAngles([0,1,0]) 
   
   print(alpha, beta, gamma)  # the return value of alpha is random here

In case you want to set the components :math:`V_{xx}` and :math:`V_{yy}` as well, use

.. code-block::

   # input vector in the nexus coordinate system.
   # vectors must be orthogonal, but do not have to be normalized
   Vzz = [1,0,0]
   Vxx = [0,0,1]
   Vyy = [0,1,0]
   alpha, beta, gamma = nx.euler.VectorsToAngles(Vzz, Vxx, Vyy) 
   
   print(alpha, beta, gamma)

In case you have fit some Euler angles, you can also get the EFG vectors by :attr:`AnglesToVectors`.

For more information on Euler angle convention and additional functions have a look to the :ref:`api-euler` module.

.. seealso:: `<https://en.wikipedia.org/wiki/Electric_field_gradient>`_

             `<https://en.wikipedia.org/wiki/Quadrupole_splitting>`_
             
             `<https://en.wikipedia.org/wiki/Euler_angles>`_

      
Lamb-Moessbauer factor
----------------------

.. versionadded:: 1.2.0

A site specific Lamb-Moessbauer factor :attr:`lamb_moessbauer` has been added. 
If the :class:`Material` has a Lamb Moessbauer factor defined, the material`s factor is used.
In case you want to use the site specific Lamb-Moessbauer factor, set the :attr:`lamb_moessbauer` of the Material to ``None``.
In that case each site assigned to the material must have a valid Lamb-Moessbauer factor defined.


Line broadening
---------------

.. versionadded:: 1.2.0

The factor :attr:`broadening` gives an additional line broadening.
It multiplies with the natural linewidth and describes any model-free broadening of the site.
In case you want to use a model dependent broadening use the distributions functions described in section :ref:`tut-distribution`.


Distributions
-------------

It is possible to assign distributions directly during the initialization of a Hyperfine site.
Therefore, you need to specify a distribution (see :ref:`tut-distribution`.) and assign it to "x_dist", where "x" represents any hyperfine parameter, e.g. "isomer_dist" or "quadrupole_gamma_dist".


Isotropic
---------

The :attr:`isotropic` option sets a 3D angular distribution of the site.
The site parameters (the angels between the magnetic field and the quadrupole) are fixed.


Texture
-------

.. versionadded:: 1.2.0

Since version 1.2.0 *Nexus* offers a texture coefficient :attr:`texture` as in CONUSS.
It is only used if ``.isotropic = False``.
Please be aware that the texture coefficient relates to a very special situation of hyperfine parameters.
A texture of ``1`` corresponds to site whose directional parameters are such as given.
A texture of ``0`` corresponds to a site whose :attr:`isotropic` is set to ``True``.
It means that the non-texture part models a site with magnetic field and quadrupole slitting fixed to each other and this site randomly distributed in 3D.
All values between 0 and 1 mix the two cases.

.. note:: Prior to version 1.2.0 use


Texture as in CONUSS
********************

If you are familiar with *CONUSS* you might want to set the texture of a sample.
Here is how to emulate the texture coefficient from *CONUSS*.
Let's assume a hyperfine site with a magnetic field directed along the photon k-vector. The texture should be 80% in CONUSS.
Then, in *Nexus* write

.. code-block::

   # this site corresponds to the 80% textured part with both magnetic field and efg
   # being directed along their given directions 
   site_1 = nx.Hyperfine(weight = 0.8,
                         magnetic_field = 33,
                         magnetic_theta = 0,
                         magnetic_phi = 0,
                         quadrupole = 0,
                         isotropic = False)

   # this site corresponds to the 20% non-textured part with both magnetic field and efg
   # being spherically distributed
   site_2 = nx.Hyperfine(weight = 0.2,
                         magnetic_field = 33,
                         quadrupole = 0,
                         isotropic = True)

   material.hyperfine_sites = [site_1, site_2]

When you want to fit a texture just let one of the weights be fitted and keep the other one fixed.
As the weight give the relative contribution you can easily calculate the percentage.


Notebooks
---------

`hyperfine`_ - :download:`nb_hyperfine.ipynb`.

.. _hyperfine: nb_hyperfine.ipynb


Please have a look to the :ref:`sec-API` for more information.