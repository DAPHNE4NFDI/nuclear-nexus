.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Nuclear Reflectivities
----------------------

The :class:`NuclearReflectivity` and :class:`NuclearReflectivityEnergy` methods calculate the reflected intensity in grazing incidence geometry.
The :class:`NuclearReflectivity` gives the time-integrated delayed scattered radiation, so the integrated time spectrum at each angle.
So, electronic scattering is typically not observed due to the veto set in a time spectrum.
The :class:`NuclearReflectivityEnergy` gives the energy-integrated delayed scattered radiation, so the integrated energy spectrum at each angle.
Here, electronic scattering is included. This method should be used to simulate or fit data from a synchrotron Moessbauer source (SMS).

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
The convolution with the ``resolution`` parameter is done on the angular values. So the resolution is an angular resolution (degree).

The specific parameters common for the two methods are

* ``sample``: Sample objects of the experiment whose angle is changed. Only one sample can have a changing angle.
* ``energy``: X-ray energy in eV.
* ``angles``: List of the angles for which the reflectivity is calculated in degree.
* ``intensity_data``: List of the experimental intensities. Only used for fitting.

For :class:`NuclearReflectivityEnergy` the additional parameters are

* ``detuning``: Detuning range over which the integration takes place.
* ``time_gate``: An empty or two element list.
  If a two element list is passed these two values [start, stop] are taken as a time gating of the energy spectrum calculation.
  Given in ns.
  Default is an empty list.
  The spectrum is Fourier transformed to the time domain, then the time gate is applied, and transformed back to the energy domain.
  Time gating will result in a reshaping of the spectrum depending cut components.
  Please note the for a proper Fourier transform, the detuning range large, typically larger than 1000 :math:`\Gamma`, and the step size should be smaller 0.5 :math:`\Gamma`.

For :class:`NuclearReflectivity` the additional parameters are

* ``time_start``: Start time for the integration (ns).
* ``time_stop``: Stop time for the integration (ns).
* ``time_step``: Step size for the time axis (ns).
* ``max_detuning``: Maximum value of the energy detuning (:math:`\Gamma`) used for the internal energy representation during calculations. This value is 400 :math:`\Gamma` on default. In case you expect nuclear features close or beyond this detuning increase the value. Larger values will decrease the computational speed. When you don't want to use a cut in the energy space set this value to ``0``. Then, Nexus will calculate a large detuning range determined by the time settings.


In this example we create a thin-film cavity structure and calculate both methods.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    # ------------------------- Fe layer --------------------------

    mat_Fe = nx.Material.Template(nx.lib.material.Fe_enriched)

    site = nx.Hyperfine(magnetic_field = 33,
                        magnetic_theta = 90,
                        magnetic_phi = 0)

    mat_Fe.hyperfine_sites = [site]

    lay_Fe = nx.Layer(id = "Fe",
                      material = mat_Fe,
                      thickness = 1.5,
                      roughness = 0.35)

    # ----------------------------- Pt layers -----------------------------
    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = 2,
                          roughness = 0.2)

    lay_Pt = nx.Layer(id = "Pt",
                      material = nx.Material.Template(nx.lib.material.Pt),
                      thickness = 15,
                      roughness = 0.77)

    # -------------------------- C layer ---------------------------
    lay_C = nx.Layer(id = "C",
                     material = nx.Material.Template(nx.lib.material.C),
                     thickness = 10,
                     roughness = 0.3)

    # --------------------- substrate ---------------------------------
    lay_substrate = nx.Layer(id = "Si sub",
                             material = nx.Material.Template(nx.lib.material.Si),
                             thickness = nx.inf,
                             roughness = 0.4)

    # --------------------- sample ---------------------------------
    # is defined in reflection here
    sample = nx.Sample(id = "simple layers",
                       layers = [lay_Pt_top,
                                 lay_C,
                                 lay_Fe,
                                 lay_C,
                                 lay_Pt,
                                 lay_substrate],
                        geometry = "r",
                        length = 10,
                        roughness = "a")

    beam  = nx.Beam(fwhm = 0.2)

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.lib.moessbauer.Fe57,
                        id = "my exp")

    angles = np.arange(0.01, 1, 0.01)

    nuc_ref = nx.NuclearReflectivity(experiment = exp,
                                     sample = sample,
                                     angles = angles,
                                     time_start = 10,
                                     time_stop  = 192,
                                     time_step = 0.2,
                                     max_detuning = 400,
                                     intensity_data = [],
                                     scaling = "auto",
                                     background = 0,
                                     fit_weight = 1.0,
                                     resolution = 0.001)

    plt.semilogy(angles, nuc_ref())
    plt.xlabel('angle (deg)')
    plt.ylabel('time-integrated resonant intensity ($\Gamma$)')
    plt.show()

Here you will get some warnings

.. code-block::

    -------------------------------------------------------------------------------------------
     NEXUS WARNING in NuclearReflectivity
     warning: Analytical roughness model of interface W matrix not valid! Output might be wrong!
              At angle 0.990000 and at energy 14412.497000.
              Wavevector kz * roughness.value = 0.378586 > 0.3 but should be << 1.
              Encountered in Sample.id:   - Layer.id: C  - Layer.roughness: 0.300000
    -------------------------------------------------------------------------------------------

which tell you to be careful with the calculated nuclear reflectivity as the model on the roughness is quite stressed.
You can see that the values of ``Wavevector kz * roughness.value`` are about 0.38 at an incidence angle of 0.99 degree.
The model might still be OK but its is always the users responsibility to check this.

.. image:: nuc_refl.png



The :class:`NuclearReflectivityEnergy` is similar. It is the reflectivity that one obtains when using a synchrotron Moessbauer source (SMS).

.. code-block::

    detuning = np.linspace(-200, 200, 1001)

    nuc_ref = nx.NuclearReflectivityEnergy(experiment = exp,
                                           sample = sample,
                                           angles = angles,
                                           detuning = detuning,
                                           intensity_data = [],
                                           scaling = "auto",
                                           background = 0,
                                           fit_weight = 1.0,
                                           resolution = 0.001,
                                           time_gate = [])

    plt.semilogy(angles, nuc_ref())
    plt.xlabel('angle (deg)')
    plt.ylabel('energy-integrated intensity ($\Gamma$)')
    plt.show()

.. image:: nuc_refl_energy.png
