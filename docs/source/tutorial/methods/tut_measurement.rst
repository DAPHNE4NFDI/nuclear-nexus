.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-measurement:


Measurements
============

A measurement is a certain observable of your experiment, it may be a real measureable quantity or a (comlex or real) non-measureable quantity of an experiment.
This can be, for example, a Moessbauer spectrum, a thin film reflectivity, the photon field intensity inside a sample, the complex energy-dependent scattering amplitude, and so on.

All measurement types are a :class:`Measurement` object.
Only those that calculate real and observable quantities are of type :class:`FitMeasurement`, which means that those measurements can be fit to a data set.
These two classes are base classes which means you do not have to deal with them directly, they are used for internal purposes of *Nexus* only.
However, in case you are not sure whether you can fit a measurement or not have a look to the :ref:`sec-API` of the specific measurement.
You can see a base class in the constructor documentation.
Only if it is ``nexus.clib.cnexus.FitMeasurement`` the measurement is taken by the :class:`Fit` module.
You can pass any :class:`Measurement` object to the :class:`Optimizer` module.


Measurement types
-----------------

*Nexus* provides a couple of different measurement types. The following lists show
fittable and non-fittable measurements implemented at the moment.
Electronic and nuclear scattering refer to pure prompt scattering at electrons and delayed scattering due to nuclear interactions, respectively.
For some methods you can specify which scattering contributions are taken into account.

Non-fittable measurements
*************************

Methods with pure electronic contributions:

* ``Amplitudes``: Calculates the complex electronic scattering amplitudes after each object in the experiment.
* ``Matrices``: Calculates the complex electronic scattering matrices after each object in the experiment.
* ``JonesVectors``: Calculates the complex electronic Jones vector after each object in the experiment.
* ``Intensities``: Calculates the electronic scattering intensities after each object in the experiment.
* ``FieldAmplitude``: Calculates the complex electronic field amplitude inside a sample.
* ``FieldIntensity``: Calculates the absolute electronic field intensity inside a sample.
* ``ReflectivityAmplitude``: Calculates the angular-dependent complex electronic amplitude of the experiment with a sample in reflection geometry whose incidence angle is changed.
* ``TransmissionAmplitude``: Calculates the angular-dependent complex electronic amplitude of the experiment with a sample in transmission geometry whose incidence angle is changed.

Methods with electronic and nuclear scattering:

* ``AmplitudeSpectrum``: Calculates the complex energy-dependent Jones vector of the experiment.
* ``AmplitudeTime``: Calculates the complex time-dependent Jones vector of the experiment.
* ``EnergyTimeAmplitude``: Calculates the detuning and time-dependent complex Jones vector of an experiment where one sample is energy-detuned (Doppler-detuned) with respect to all others.


Amplitude methods are of :class:`Measurement` type, return complex quantities, cannot be fit to experimental data, and are intended for theoretical calculations.
``Intensities`` and ``FieldIntensity`` return real quantities but cannot be fit.
Thickness or angular distributions cannot be applied to non-fittable measurements because they assume incoherent addition of intensities.


Fittable measurements
*********************

Methods with pure electronic contributions:

* ``Reflectivity``: Calculates the electronic intensity of an experiment with a sample in reflection geometry with the incidence angle being scanned.
* ``Transmission``: Calculates the electronic intensity of an experiment with a sample in (gracing-incidence) transmission geometry with the incidence angle being scanned.

Methods with electronic and nuclear scattering:

* ``MoessbauerSpectrum``: Calculates the velocity-dependent coherent intensity of an experiment.
* ``EmissionSpectrum``: Calculates an velocity-dependent emission intensity of an experiment.
  See comments there for the exact model.
* ``EnergySpectrum``: Calculates the detuning-dependent coherent transmission intensity of an experiment.
* ``TimeSpectrum``: Calculates the time-dependent coherent transmission intensity of an experiment.
* ``EnergyTimeSpectrum``: Calculates the detuning and time-dependent coherent intensity of an experiment where one sample is energy-detuned (Doppler-detuned) with respect to all others.
* ``IntegratedEnergyTimeSpectrum``: Calculates the time-integrated coherent intensity of an :class:`EnergyTimeSpectrum` object.
* ``NuclearReflectivity``: Calculates the pure nuclear delayed intensity of an experiment.
  One sample is in reflection geometry with the incidence angle being scanned.
* ``NuclearReflectivityEnergy``: Calculates the energy-integrated intensity of an experiment.
  One sample is in reflection geometry with the incidence angle being scanned.

These methods are of :class:`FitMeasurement` type, return real quantities and are intended to fit experimental data sets.


Parameters
**********

Every :class:`Measurement` needs at least an :class:`Experiment` or a :class:`Sample`, depending on the measurement type.
Here, all information on the experimental setup are given, needed by the measurement method.
The measurement is just a certain view onto the experiment and returns the calculated property.

A :class:`FitMeasurement` always needs an :class:`Experiment`.
If you want to fit a data set you also have to provide the measured data.
For a :class:`MoessbauerSpectrum`, for example, you have to provide the measured ``velocity`` and ``intensity_data``.
What is exactly needed depends on the :class:`Measurement` type.

A lot of additional parameters influence a real measurement.
To account for experimental conditions, a :class:`FitMeasurment` has a couple of additional parameters.

   * ``scaling``: The scaling factor to scale the theoretical curve to the measured intensity. It can be a float, :class:`nx.Var` or ``auto``.
   * ``background``: An intensity background added to the scaled theoretical curve. It can be a float, :class:`nx.Var` or ``auto``.
   * ``offset`` : An offset in the non-dependent variable to account for an experimental offset.
     Not available for Energy or Moessbauer spectra as it has the same influence as the isomer shift.

     .. versionadded:: 1.1.0

   * ``kernel_type`` (some methods): A Gaussian or Lorentzian kernel can be used to convolute the theoretical curve with the experimental resolution function.
     This is either the energy resolution of a Moessbauer source, the time resolution of an APD or the angular resolution of a reflectivity.
   * ``resolution``: FWHM of the Gaussian or Lorentzian used in the resolution kernel.
   * ``distribution_points``: In case thickness or angular distributions should be taken into account, specify a value larger than one.
     The thickness or angular distribution of each object is then approximated by the given number of points. Typically, if used, the value should be larger than 7.
   * ``fit_weight``: Relative weight of the specific measurement in multi-measurement fits.
   * ``residual``: The residual is used to weight the difference between the measured data and the theoretical curves properly.
     The standard residual depends on the measurement type you use, see :ref:`api-residual`.

**The following parameters are** :class:`Var` **objects and can be fit:**
  
  * scaling
  * background
  * resolution


.. versionadded:: 1.2.0 
   A :attr:`Velocity` function is added to obtain the velocity detuning in mm/s from the given detuning in energy dependent measurements.
   

Measurement functor
*******************

A :class:`Measurement` object is a functor.
It is quite similar to a function in the sense that it can be called but it can hold certain parameters and object states provided during initialization.
First, the object has to be initialized and after the initialization the calculation (function call) can be performed.

For example, a Moessbauer spectrum would be created and calculated like this

.. code-block::

    import nexus as nx
    import numpy as np

    # definitions for experiment would be here
    # ....
    # exp = nx.Experiment(....)

    # define a range of the calculation
    velocities = np.linspace(-10, 10, 2001)  # in mm/s

    # define the Moessbauer spectrum object
    spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                     velocity = velocities)

    # the spectrum functor is now initialized with the parameters experiment and velocity
   
    # now the spectrum can be called as it would be a function
    intensity = spectrum()
    # or equivalently by the Calculate() method
    intensity = spectrum.Calculate()
    # which is a bit more intuitive because you know what the functor actually does.

    print(intensity)


.. include:: tut_energy_spectrum.rst
.. include:: tut_moessbauer_spectrum.rst
.. include:: tut_emission_spectrum.rst
.. include:: tut_time_spectrum.rst
.. include:: tut_electronic.rst
.. include:: tut_field.rst
.. include:: tut_reflectivity.rst
.. include:: tut_nuclear_reflectivity.rst
.. include:: tut_energy_time_spectrum.rst


Notebooks
---------

`energy spectrum`_ - :download:`nb_energy_spectrum.ipynb`.

.. _energy spectrum: nb_energy_spectrum.ipynb


`energy spectrum time gate`_ - :download:`nb_energy_spectrum_time_gate.ipynb`.

.. _energy spectrum time gate: nb_energy_spectrum_time_gate.ipynb


`amplitude spectrum`_ - :download:`nb_amplitude_spectrum.ipynb`.

.. _amplitude spectrum: nb_amplitude_spectrum.ipynb


`emission spectrum`_ - :download:`nb_emission_spectrum.ipynb`.

.. _emission spectrum: nb_emission_spectrum.ipynb


`time spectrum`_ - :download:`nb_time_spectrum.ipynb`.

.. _time spectrum: nb_time_spectrum.ipynb


`amplitude time`_ - :download:`nb_amplitude_time.ipynb`.

.. _amplitude time: nb_amplitude_time.ipynb


`amplitudes`_ - :download:`nb_amplitudes.ipynb`.

.. _amplitudes: nb_amplitudes.ipynb


`intensities`_ - :download:`nb_intensities.ipynb`.

.. _intensities: nb_intensities.ipynb


`JonesVector`_ - :download:`nb_JonesVector.ipynb`.

.. _JonesVector: nb_JonesVector.ipynb


`matrices`_ - :download:`nb_matrices.ipynb`.

.. _matrices: nb_matrices.ipynb


`fields`_ - :download:`nb_fields.ipynb`.

.. _fields: nb_fields.ipynb


`reflectivity`_ - :download:`nb_reflectivity.ipynb`.

.. _reflectivity: nb_reflectivity.ipynb


`reflectivity amplitudes`_ - :download:`nb_reflectivity_amplitudes.ipynb`.

.. _reflectivity amplitudes: nb_reflectivity_amplitudes.ipynb


`nuclear reflectivity`_ - :download:`nb_nuclear_reflectivity.ipynb`.

.. _nuclear reflectivity: nb_nuclear_reflectivity.ipynb


`energy time spectrum`_ - :download:`nb_energy_time_spectrum.ipynb`.

.. _energy time spectrum: nb_energy_time_spectrum.ipynb


`energy time amplitude`_ - :download:`nb_energy_time_amplitude.ipynb`.

.. _energy time amplitude: nb_energy_time_amplitude.ipynb



Please have a look to the :ref:`sec-API` for more information.
