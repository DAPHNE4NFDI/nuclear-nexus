.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2023 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklag@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Energy Time Spectrum
--------------------

The :class:`EnergyTimeSpectrum` method calculates the time dependent intensity for different energy detunings of one sample in the experiment. 
The sample is assumed to be Doppler shifted on some kind of mechanical drive (Doppler detuning).
The momentary detuning that is specified will lead to an energetic shift of the transition on the moved sample.
Thus, an interferogram of the shifted transitions with respect to all other transitions in the experiment is calculated.

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
The specified energy resolution is not that of a source like in a Moessbauer spectrum because here a broadband excitation with a synchrotron pulse is assumed.
The energy resolution is any additional broadening term, like noise on the drive motion for example.
The time resolution is the one of the temporal detector, i.e. an APD.

The specific parameters for the :class:`EnergySpectrum` are

* ``time_length``: Length of the calculation (ns).
* ``time_step``: Step size of the calculation in (ns). This should be smaller than the experimental ``time_data`` step size.
* ``mode``:  Identifier for the calculation mode. 

             * ``i`` - interpolate mode. The scattering properties are interpolated on the detuning values (recommended fast mode).
             * ``f``- full mode. The scattering properties at different detuning values are calculated via a change of the isomer shift (slow mode).

* ``electronic``: Either ``True`` or ``False``.
  If electronic is ``True`` both electronic and nuclear scattering are taken into account.
  In case you are only interested in the nuclear delayed scattering properties set this parameter to ``False``.
  In this case pure electronic scattering will be subtracted, see :ref:`theory`.
  This will result in a pure nuclear emission spectrum.
* ``time_data``: Time axis of the intensity data. Only used for fitting.
* ``intensity_data``: 2D intensity data used for fitting.
  The 1st dimension corresponds to the detuning, the 2nd to the time.

The returned array has the shape (detuning points, time steps).

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import colors

    # ----------------- cavity in reflection geometry -------------

    # ------------------------- Fe layer --------------------------
    lay_Fe = nx.Layer(id = "Fe",
                    material = nx.Material.Template(nx.lib.material.Fe_enriched),
                    thickness = 1.5,
                    roughness = 0.3)

    site1 = nx.Hyperfine(magnetic_field = 33)

    lay_Fe.material.hyperfine_sites = [site1]

    # ----------------------------- Pt layers -----------------------------
    lay_Pt_top = nx.Layer(id = "Pt top",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 2,
                    roughness = 0.3)

    lay_Pt = nx.Layer(id = "Pt",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 15,
                    roughness = 0.3)

    # -------------------------- C ---------------------------
    lay_C = nx.Layer(id = "C",
                    material = nx.Material.Template(nx.lib.material.C),
                    thickness = 10,
                    roughness = 0.3)
                    
    # --------------------- substrate ---------------------------------
    lay_substrate = nx.Layer(id = "Si sub",
                    material = nx.Material.Template(nx.lib.material.Si),
                    thickness = nx.inf,
                    roughness = 0.3)

    sample_cavity = nx.Sample(id = "simple layers",
                              layers = [lay_Pt_top,
                                        lay_C,
                                        lay_Fe,
                                        lay_C,
                                        lay_Pt,
                                        lay_substrate],
                               geometry = "r",
                               angle = 0.148,
                               roughness = "a")

    # ------------- Stainless steel foil on a drive in forward geometry ----------------
    foil = nx.Layer(id = "StainSteel",
                    material = nx.Material.Template(nx.lib.material.SS_enriched),
                    thickness = 3000)

    site_foil = nx.Hyperfine(
        isomer = -0.09,  # with respect to iron
        quadrupole = 0.6,
        isotropic = True)

    foil.material.hyperfine_sites = [site_foil]

    sample_foil = nx.Sample(id = "simple foil ",
                            layers = [foil],
                            geometry = "f")

    # define a detuning for the sample on the drive
    sample_foil.drive_detuning = np.linspace(-200, 200, 1024)

    # -------------------------- setup experiment ------------------------
    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample_cavity, sample_foil],
                        isotope = nx.moessbauer.Fe57,
                        id = "my exp")

    energy_time_spec = nx.EnergyTimeSpectrum(experiment = exp,
                                time_length = 200.0,
                                time_step = 0.2,
                                mode = "i",
                                electronic = False)

    timescale, intensity = energy_time_spec()

    plt.imshow(intensity.T,
               cmap=plt.cm.plasma,
               norm=colors.LogNorm(),
               aspect='auto',
               origin='lower',
               extent=(min(sample_foil.drive_detuning), max(sample_foil.drive_detuning), min(timescale), max(timescale)))

    plt.colorbar(orientation='vertical', label = 'intensity ($\Gamma$/ns)')
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('time (ns)')
    plt.show()

.. image:: energy_time_spectrum.png


Energy Time Amplitude
---------------------

The :class:`EnergyTimeAmplitude` is very similar to an :class:`EnergyTimeSpectrum` but instead of intensities it provides complex amplitudes.
Amplitude methods are of :class:`Measurement` type, cannot be fit to experimental data, and are intended for theoretical calculations.
Therefore, all experimental parameters of the :class:`FitMeasurement` are not available.
Thickness or angular distributions cannot be applied because they assume incoherent addition of intensities.

The specific parameters for an amplitude spectrum are:

* ``time_length``: Length of the calculation (ns).
* ``time_step``: Step size of the calculation in (ns).
  This should be smaller than the experimental ``time_data`` step size.
* ``mode``:  Identifier for the calculation mode. 

             * ``i`` - interpolate mode. The scattering properties are interpolated on the detuning values (recommended fast mode).
             * ``f``- full mode. The scattering properties at different detuning values are calculated via a change of the isomer shift (slow mode).

* ``electronic``: Either ``True`` or ``False``.
  If electronic is ``True`` both electronic and nuclear scattering are taken into account.
  In case you are only interested in the nuclear delayed scattering properties set this parameter to ``False``.
  In this case pure electronic scattering will be subtracted, see :ref:`theory`.
  This will result in a pure nuclear emission spectrum.

To calculate amplitude properties, the beam has to have a valid Jones vector.
See, the :ref:`tut-beam` section for more details.

The :class:`EnergyTimeAmplitude` returns a 2D array of Jones vectors.
So each array has the shape (detuning points, time steps, 2) with complex entries.
The first complex entry along the third dimension corresponds to the :math:`\sigma` component, the second to the :math:`\pi` component of the scattered field.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import colors

    # ----------------- cavity in reflection geometry -------------

    # ------------------------- Fe layer --------------------------
    lay_Fe = nx.Layer(id = "Fe",
                    material = nx.Material.Template(nx.lib.material.Fe_enriched),
                    thickness = 1.5,
                    roughness = 0.3)

    site1 = nx.Hyperfine(magnetic_field = 0)

    lay_Fe.material.hyperfine_sites = [site1]

    # ----------------------------- Pt layers -----------------------------
    lay_Pt_top = nx.Layer(id = "Pt top",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 2,
                    roughness = 0.3)

    lay_Pt = nx.Layer(id = "Pt",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 15,
                    roughness = 0.3)

    # -------------------------- C ---------------------------

    lay_C = nx.Layer(id = "C",
                    material = nx.Material.Template(nx.lib.material.C),
                    thickness = 10,
                    roughness = 0.3)

    # --------------------- substrate ---------------------------------
    lay_substrate = nx.Layer(id = "Si sub",
                    material = nx.Material.Template(nx.lib.material.Si),
                    thickness = nx.inf,
                    roughness = 0.3)

    sample_cavity = nx.Sample(id = "simple layers",
                              layers = [lay_Pt_top,
                                        lay_C,
                                        lay_Fe,
                                        lay_C,
                                        lay_Pt,
                                        lay_substrate],
                               geometry = "r",
                               angle = 0.148,
                               roughness = "a")

    # ------------- Stainless steel foil in forward geometry ----------------
    foil = nx.Layer(id = "StainSteel",
                    material = nx.Material.Template(nx.lib.material.SS_enriched),
                    thickness = 3000)

    site_foil = nx.Hyperfine(
        isomer = -0.09,  # mm/s
        quadrupole = 0.6,
        isotropic = True)

    foil.material.hyperfine_sites = [site_foil]

    sample_foil = nx.Sample(id = "simple foil ",
                            layers = [foil],
                            geometry = "f")

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample_cavity, sample_foil],
                        isotope = nx.moessbauer.Fe57,
                        id = "my exp")

    # define a detuning for the sample on the drive
    sample_foil.drive_detuning = np.linspace(-200, 200, 1024)  # Gamma

    eta = nx.EnergyTimeAmplitude(experiment = exp,
                                time_length = 200.0,
                                time_step = 0.2,
                                mode = "i",
                                electronic = False)

    timescale, amp = eta()

    # calculate intensity from Jones vector
    intensity = np.square(np.abs(amp[:,:,0])) + np.square(np.abs(amp[:,:,1]))

    plt.imshow(intensity.T,
               cmap=plt.cm.plasma,
               norm=colors.LogNorm(),
               aspect='auto',
               origin = 'lower',
               extent=(min(sample_foil.drive_detuning), max(sample_foil.drive_detuning), min(timescale), max(timescale)))

    plt.colorbar(orientation='vertical', label = 'amplitude ($\sqrt{\Gamma / \mathrm{ns}}$)')
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('time (ns)')
    plt.show()


.. image:: energy_time_amplitude.png



Integrated Energy Time Spectrum
-------------------------------

The :class:`IntegratedEnergyTimeSpectrum` method calculates the time integrated intensity of a :class:`EnergyTimeSpectrum`.

Therefore, the two integration borders have to be specified in addition to the parameters of the :class:`EnergyTimeSpectrum`.

* ``integration_start``: Start time of the integration (ns).
* ``integration_stop``: Stop time of the integration (ns).

The intensity integration is done between the two values.

.. code-block::

    int_energy_time_spec = nx.IntegratedEnergyTimeSpectrum(experiment = exp,
                                                           time_length = 200.0,
                                                           time_step = 0.2,
                                                           integration_start = 51,
                                                           integration_stop = 180,
                                                           mode = "i",
                                                           electronic = False)

    intensity = int_energy_time_spec()

    plt.plot(sample_foil.drive_detuning, intensity)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('$\Delta t$-integrated intensity ($\Gamma$)')
    plt.show()

.. image:: int_energy_time_spectrum.png
