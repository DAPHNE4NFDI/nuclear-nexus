.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Reflectivity and Transmission
-----------------------------

The :class:`Reflectivity` and :class:`Transmission` methods calculate the mentioned method in grazing incidence geometry.

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
The convolution with the ``resolution`` parameter is done on the angular values. So the resolution is an angular resolution (degree).

The specific parameters for the two methods are

* ``sample``: Sample objects of the experiment whose angle is changed.
  Only one sample can have a changing angle.
* ``energy``: X-ray energy in eV.
* ``angles``: List of the angles for which the reflectivity or transmission is calculated in degree.
* ``intensity_data``: List of the experimental intensities. Only used for fitting.

In this example we create a thin-film cavity structure and calculate both methods.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    # ------------------------- Fe layer --------------------------
    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 1.5,
                      roughness = 0.35)

    # ----------------------------- Pt layers -----------------------------
    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = 2,
                          roughness = 0.2)

    lay_Pt = nx.Layer(id = "Pt",
                      material = nx.Material.Template(nx.lib.material.Pt),
                      thickness = 15,
                      roughness = 0.77)

    # -------------------------- C layer ---------------------------
    lay_C = nx.Layer(id = "C",
                     material = nx.Material.Template(nx.lib.material.C),
                     thickness = 10,
                     roughness = 0.3)

    # --------------------- substrate ---------------------------------
    lay_substrate = nx.Layer(id = "Si sub",
                             material = nx.Material.Template(nx.lib.material.Si),
                             thickness = nx.inf,
                             roughness = 0.4)

    # --------------------- sample ---------------------------------
    # is defined in reflection here
    sample = nx.Sample(id = "simple layers",
                       layers = [lay_Pt_top,
                                 lay_C,
                                 lay_Fe,
                                 lay_C,
                                 lay_Pt,
                                 lay_substrate],
                        geometry = "r",
                        length = 10,
                        roughness = "a")

    beam  = nx.Beam(fwhm = 0.2)

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        id = "my exp")

    angles = np.arange(0.001, 3, 0.001)

    reflectivity = nx.Reflectivity(experiment = exp,
                                   sample = sample,
                                   energy = nx.lib.energy.CuKalpha,  # Cu K alpha line
                                   angles = angles,
                                   resolution = 0.001)

    plt.semilogy(angles, reflectivity())
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

    # remove the infinite substrate and set to transmission geometry
    sample.layers = [lay_Pt_top,
                     lay_C,
                     lay_Fe,
                     lay_C,
                     lay_Pt]

    sample.geometry = "t"

    transmission = nx.Transmission(experiment = exp,
                                   sample = sample,
                                   energy = nx.lib.energy.CuKalpha,  # Cu K alpha line
                                   angles = angles,
                                   resolution = 0.001)

    plt.semilogy(angles, transmission())
    plt.xlabel('angle (deg)')
    plt.ylabel('transmission')
    plt.show()

.. image:: reflectivity.png

.. image:: transmission.png



Reflectivity and Transmission Amplitudes
----------------------------------------

The :class:`ReflectivityAmplitude` and :class:`TransmissionAmplitude` methods calculate the complex amplitudes in grazing incidence geometry.
Amplitude methods are of :class:`Measurement` type, cannot be fit to experimental data, and are intended for theoretical calculations.
Therefore, all experimental parameters of the :class:`FitMeasurement` are not available.

The specific parameters for the two methods are

* ``sample``: Sample objects of the experiment whose angle is changed. Only one sample can have a changing angle.
* ``energy``: X-ray energy in eV.
* ``angles``: List of the angles for which the reflectivity is calculated in degree.

In this example we create a thin-film cavity structure and calculate both methods.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    # ------------------------- Fe layer --------------------------
    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 1.5,
                      roughness = 0.35)

    # ----------------------------- Pt layers -----------------------------
    lay_Pt_top = nx.Layer(id = "Pt top",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 2,
                    roughness = 0.2)

    lay_Pt = nx.Layer(id = "Pt",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 15,
                    roughness = 0.77)

    # -------------------------- C layer ---------------------------
    lay_C = nx.Layer(id = "C",
                    material = nx.Material.Template(nx.lib.material.C),
                    thickness = 10,
                    roughness = 0.3)

    # --------------------- substrate ---------------------------------
    lay_substrate = nx.Layer(id = "Si sub",
                    material = nx.Material.Template(nx.lib.material.Si),
                    thickness = nx.inf,
                    roughness = 0.4)

    # --------------------- sample ---------------------------------
    # is defined in reflection here
    sample = nx.Sample(id = "simple layers",
                       layers = [lay_Pt_top,
                                 lay_C,
                                 lay_Fe,
                                 lay_C,
                                 lay_Pt,
                                 lay_substrate],
                        geometry = "r",
                        length = 10,
                        roughness = "a")

    beam = nx.Beam(fwhm = 0.2)

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        id = "my exp")

    angles = np.arange(0.001, 3, 0.001)

    reflectivity_amp = nx.ReflectivityAmplitude(experiment = exp,
                                                sample = sample,
                                                energy = nx.lib.energy.CuKalpha,  # Cu K alpha line
                                                angles = angles)

    ref = reflectivity_amp()

    plt.plot(angles, np.real(ref), label='real')
    plt.plot(angles, np.imag(ref), label='imag')
    plt.legend()
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity amplitude')
    plt.show()

    # remove the infinite substrate and set to transmission geometry
    sample.layers = [lay_Pt_top,
                     lay_C,
                     lay_Fe,
                     lay_C,
                     lay_Pt]

    sample.geometry = "t"

    transmission_amp = nx.TransmissionAmplitude(experiment = exp,
                                                sample = sample,
                                                energy = nx.lib.energy.CuKalpha,  # Cu K alpha line
                                                angles = angles)

    trans = transmission_amp()

    plt.plot(angles, np.real(trans), label='real')
    plt.plot(angles, np.imag(trans), label='imag')
    plt.legend()
    plt.xlabel('angle (deg)')
    plt.ylabel('transmission amplitude')
    plt.show()

.. image:: reflectivity_amplitude.png

.. image:: transmission_amplitude.png
