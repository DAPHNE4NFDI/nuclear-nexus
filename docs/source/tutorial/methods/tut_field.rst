.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Fields inside a sample
----------------------

The :class:`FieldAmplitudes` and :class:`Field Intensity` methods calculate the field inside a sample.
Both methods are of :class:`Measurement` type, cannot be fit to experimental data, and are intended for theoretical calculations.
Therefore, all experimental parameters of the :class:`FitMeasurement` are not available.


Both methods do not take an experiment but only a sample as argument.
The methods work for forward and grazing-incidence scattering.
The specific parameters for a field amplitude are:

* ``sample``: The sample for which the field should be calculated.
* ``energy``: The photon energy for the calculation in eV.
* ``points``: Number of points to calculate the field inside the sample.

The methods are not dependent on the polarization state. Only the scattering factor is calculated.

In this example we create a thin-film cavity structure.
We also add an air layer to calculate the field above the cavity.


.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    # --------------------- air --------------
    lay_air = nx.Layer(id = "air",
                       material = nx.Material.Template(nx.lib.material.air),
                       thickness = 10,
                       roughness = 0)

    # ------------------------- Fe layer --------------------------
    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 1.5,
                      roughness = 0.35)

    # ----------------------------- Pt layers -----------------------------
    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = 2,
                         roughness = 0.2)

    lay_Pt = nx.Layer(id = "Pt",
                    material = nx.Material.Template(nx.lib.material.Pt),
                    thickness = 15,
                    roughness = 0.77)

    # -------------------------- C layer ---------------------------
    lay_C = nx.Layer(id = "C",
                    material = nx.Material.Template(nx.lib.material.C),
                    thickness = 10,
                    roughness = 0.3)

    # --------------------- substrate ---------------------------------
    lay_substrate = nx.Layer(id = "Si sub",
                    material = nx.Material.Template(nx.lib.material.Si),
                    thickness = nx.inf,
                    roughness = 0.4)

    # --------------------- sample ---------------------------------
    sample = nx.Sample(id = "simple layers",
                       layers = [lay_air,
                                 lay_Pt_top,
                                 lay_C,
                                 lay_Fe,
                                 lay_C,
                                 lay_Pt,
                                 lay_substrate],
                        geometry = "r",
                        angle = 0.161, # at the sample.angle the field will be calculated
                        length = 10,
                        roughness = "a")

    beam  = nx.Beam(fwhm = 0)

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.moessbauer.Fe57,
                        id = "my exp")

    field_amps = nx.FieldAmplitude(sample = sample,
                                   energy = nx.moessbauer.Fe57.energy,
                                   points = 1001)

    depth, amp = field_amps.Calculate()

    plt.plot(depth, np.real(amp))
    plt.plot(depth, np.imag(amp))

    # add lines and labels
    axes = plt.gca()
    y_min, y_max = axes.get_ylim()
    plt.vlines(sample.Interfaces(), y_min, y_max, colors='g', linestyles='dashed')
    for id, location in zip(sample.Ids(), np.array(sample.Interfaces()) + 0.5):
        plt.text(location, y_max, id, fontsize = 10)
        y_max = y_max - 0.5

    plt.xlabel('depth (nm)')
    plt.ylabel('relative amplitude')
    plt.show()

    field_int = nx.FieldIntensity(sample = sample,
                                  energy = nx.moessbauer.Fe57.energy,
                                  points = 1001)

    depth, int = field_int()

    plt.plot(depth, int)

    # add lines and labels
    axes = plt.gca()
    y_min, y_max = axes.get_ylim()
    plt.vlines(sample.Interfaces(), y_min, y_max, colors='g', linestyles='dashed')
    for id, location in zip(sample.Ids(), np.array(sample.Interfaces()) + 0.5):
        plt.text(location, y_max, id, fontsize = 10)
        y_max = y_max - 0.5

    plt.xlabel('depth (nm)')
    plt.ylabel('relative intensity')
    plt.show()

.. image:: field_amplitude.png

.. image:: field_intensity.png
