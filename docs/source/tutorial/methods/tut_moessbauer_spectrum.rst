.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Moessbauer Spectrum
-------------------

A classical Moessbauer spectrum is calculated by the :class:`MoessbauerSpectrum` class.
It is an :class:`EnergySpectrum` object with special initialization parameters.

The Doppler velocities are given for this method instead of detuning values in units of :math:`\Gamma`.

The convolution of the experimental data is done with a Lorentzian kernel, which accounts for the single line distribution of the Moessbauer source.
It is set to the natural linewidth of one :math:`\Gamma` of the source.
Note, that standard kernel type in Nexus is Gaussian.
The convolution of a Gaussian kernel and a Lorentzian resonance will result in a Voigt profile.
To account for the Lorentzian profile of the Moessbauer source, the convolution kernel is set to a Lorentzian here.

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
The specific parameters for the :class:`MoessbauerSpectrum` are

* ``velocity``: List with velocities of the Moessbauer drive in mm/s.
* ``intensity_data``: List of intensities measured during the experiment.
  Only needed for fitting.
* ``time_gate``: An empty or two element list.
  If a two element list is passed these two values [start, stop] are taken as a time gating of the energy spectrum calculation.
  Given in ns.
  Default is an empty list.
  The spectrum is Fourier transformed to the time domain, then the time gate is applied, and transformed back to the energy domain.
  Time gating will result in a reshaping of the spectrum depending cut components.
  Please note the for a proper Fourier transform, the detuning range large, typically larger than 1000 :math:`\Gamma`, and the step size should be smaller 0.5 :math:`\Gamma`.
  This option will not results in a time gated Moessbauer spectrum.

  .. versionadded:: 1.0.1

Here is the code block of the :ref:`tut-hello-nexus` example again.

.. code-block::

   import nexus as nx
   import numpy as np
   import matplotlib.pyplot as plt

   iron = nx.Material.Template(nx.lib.material.Fe)

   layer_Fe = nx.Layer(id = "Fe layer",
                       material = iron,
                       thickness = 3000)

   site = nx.Hyperfine(magnetic_field = 33,
                       isotropic = True)

   iron.hyperfine_sites = [site]

   sample = nx.Sample(layers = [layer_Fe])

   beam = nx.Beam(polarization = 0)

   exp = nx.Experiment(beam = beam,
                       objects = [sample],
                       isotope = nx.lib.moessbauer.Fe57)

   velocities = np.linspace(-10, 10, 512)

   moessbauer_spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                               velocity = velocities)

   intensity = moessbauer_spectrum.Calculate()

   plt.plot(velocities, intensity)
   plt.show()

As written before, the :class:`MoessbauerSpecrum` is an :class:`EnergySpectrum` with special parameters.
A Moessbauer spectrum can also be calculated from the :class:`EnergySpectrum` directly with the following parameters.

.. code-block::

    detuning  = nx.VelocityToGamma(velocity, isotope)

    moessbauer_spectrum = EnergySpectrum(experiment = exp,
                                         detuning = detuning,
                                         electronic = False,
                                         intensity_data = ...,
                                         scaling = ...,
                                         background = ...,
                                         resolution = 1.0,
                                         distribution_points = ...,
                                         fit_weight = ...,
                                         kernel_type = "Lorentz",
                                         residual = ...,
                                         time_gate = [],
                                         id = "...")

    intensity = moessbauer_spectrum.Calculate()
