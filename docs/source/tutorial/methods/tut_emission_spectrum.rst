.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2023 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklag@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Emission Spectrum
-----------------

.. warning:: The theory of incoherent emission from internal conversion processes is not yet included.
  The method calculates the coherent scattering, subtracts the background and inverts the spectrum.
  Thus the full treatment of absorption of fluorescent X-rays or conversion electron electrons is not included.

Emission spectrum (from fluorescent X-rays or conversion electrons) is calculated by the :class:`EmissionSpectrum` class.
It is a derived method from the :class:`EnergySpectrum` class.

.. note:: For an emission spectrum the penetration depths of the detected entity must be accounted for.
  An X-ray fluorescence spectrum originates from several tens of :math:`\mu m` while, a conversion electron Moessbauer spectrum (CEMS) only originates from a few tens of nm region near the surface.
  However, *Nexus* calculates the photon field behind the sample and will take electronic and nuclear scattering of all layers and the photon propagation along the sample into account.
  Therefore, you should only specify layers up to the approximate penetration depth (a few tens of nm for electrons and a few tens of :math:`\mu m` for K-shell fluorescence).
  So, do not use the real sample layer structure or thickness if the accumulated length is larger then the penetration depth of the detected radiation.
  Otherwise the emission spectra might be wrong due to photon propagation effects along the sample or false contributions from not detectable layers.

  The penetration depth depends on the energy of the conversion X-rays and electrons.
  For Fe-57, the 14.4 keV conversion process proceeds via ejection of a K-shell electron with a binding energy of 7.112 keV.
  Thus, conversion electrons have an energy of 14.4 keV - 7.1 keV = 7.3 keV.
  The K-shell is filled by an electron from the L-shell and this transition emits a :math:`K \alpha` 6.4 keV X-ray.
  The 6.4 keV X-ray can leave the atom or create an Auger process leading to 5.6 keV electrons.
  Several other internal less probable processes are present.

.. seealso:: https://en.wikipedia.org/wiki/Conversion_electron_M%C3%B6ssbauer_spectroscopy
             
             https://en.wikipedia.org/wiki/Internal_conversion

             https://en.wikipedia.org/wiki/Internal_conversion_coefficient


The Doppler velocities are given for this method instead of detuning values in units of :math:`\Gamma`.

The convolution of the experimental is done with a Lorentzian kernel, which accounts for the single line distribution of the Moessbauer source.
It is set to the natural linewidth of one :math:`\Gamma` of the source.
Note, that standard kernel type in Nexus is Gaussian.
The convolution of a Gaussian kernel and a Lorentzian resonance will result in a Voigt profile.
To account for the Lorentzian profile of the Moessbauer source, the convolution kernel is set to a Lorentzian here.

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
The specific parameters for the :class:`Emissionpectrum` are

* ``velocity``: List with velocities of the Moessbauer drive in mm/s.
* ``intensity_data``: List of intensities measured during the experiment. Only needed for fitting.
* ``time_gate``: An empty or two element list.
  If a two element list is passed these two values [start, stop] are taken as a time gating during spectrum collection.
  Given in ns.
  Default is an empty list.
  Time gating will results in a reshaping of the spectrum depending on the cut temporal components.

The calculation of an emission S is very similar to the one of a Moessbauer Spectrum.
Here, a CEMS spectrum of a 10 nm film is calculated.

.. code-block::

   import nexus as nx
   import numpy as np
   import matplotlib.pyplot as plt

   iron = nx.Material.Template(nx.lib.material.Fe)

   # a 10 nm film is used for CEMS
   layer_Fe = nx.Layer(id = "Fe layer",
                       material = iron,
                       thickness = 10)

   site = nx.Hyperfine(magnetic_field = 33,
                       isotropic = True)

   iron.hyperfine_sites = [site]

   sample = nx.Sample(layers = [layer_Fe])

   beam = nx.Beam()
   beam.LinearSigma()

   exp = nx.Experiment(beam = beam,
                       objects = [sample],
                       isotope = nx.lib.moessbauer.Fe57)

   velocities = np.linspace(-10, 10, 512)

   cem_spectrum = nx.EmissionSpectrum(experiment = exp,
                                      velocity = velocities)

   intensity = cem_spectrum.Calculate()

   plt.plot(velocities, intensity)
   plt.xlabel('velocity (mm/s)')
   plt.ylabel('intensity')
   plt.show()

.. image:: cems.png
