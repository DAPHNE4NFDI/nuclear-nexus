.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Time Spectrum
-------------

The :class:`TimeSpectrum` is used to calculate the time dependent intensity of an experiment.
Actually, a quantum beat, originating from the hyperfine split lines, is measured.
It is often referred to as `time spectrum` in the NRS community.

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
The specific parameters for the :class:`TimeSpectrum` are

* ``time_length``: Sets the total length for the calculation of the :class:`TimeSpectrum` in nanoseconds.
  For fits this value should be equal or larger than the measured time length.
* ``time_step``: Sets the step size for the calculation of the :class:`TimeSpectrum` in nanoseconds.
  The time steps should be reasonably chosen when fitting. A good time step is smaller than the measured time steps.
* ``max_detuning``: Maximum value of the energy detuning (:math:`\Gamma`) used for the internal energy representation during calculations.
  This value is 400 :math:`\Gamma` on default.
  In case you expect nuclear features close or beyond this detuning increase the value.
  Larger values will decrease the computational speed.
  This option is mainly used for data fitting.
  For theoretical calculations set it to zero.
  Then, no cut in the energy domain is used and *Nexus* will take a large detuning range determined by the time settings.
  If this parameter is not zero, data beyond :attr:`max_detuning` are padded to achieve the user time settings.
  To reduce padding effects, a window function is applied during the Fourier transformation, see :attr:`fft_window`.
* ``electronic``: Either ``True`` or ``False`` (default).
  If electronic is ``True``, electronic and nuclear scattering are taken into account.
  For a time spectrum it will result in a large intensity at time zero due to the prompt electronic scattering.
  The default value is ``False`` as you are mostly interested in the delayed nuclear signal only, see :ref:`theory`.
* ``bunch_spacing``: The bunch spacing of the synchrotron in nanoseconds.
  If provided, signals from previous bunches will be added to the time spectrum to account for all contributing signals to the measured time spectrum.
  The time_length is cut to the bunch spacing if it is larger.
* ``time_data``: List of the experimental time values (nanoseconds). Only used for fitting.
* ``intensity_data``: List of the experimental intensities. Only used for fitting.
* ``fft_window``: A window function applied before the Fourier transformation.
  On default, *Nexus* automatically selects a Hann window when :attr:`max_detuning` is not ``0`` and no window when it is ``0``.
  The options are

     * ``auto`` (default) - will pick a Hann window if :attr:`max_detuning` is not zero, no window otherwise.
     * ``Hann``
     * ``Hamming``
     * ``Sine``
     * ``Welch``
     * ``Kaiser2`` Kaiser window with :math:`\alpha = 2`
     * ``Kaiser25`` Kaiser window with :math:`\alpha = 2.5`
     * ``Kaiser3`` Kaiser window with  :math:`\alpha = 3`
     * ``none``

  See `<https://en.wikipedia.org/wiki/Window_function>`_.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    iron = nx.Material.Template(nx.lib.material.Fe)

    layer_Fe = nx.Layer(id = "Fe layer",
                        material = iron,
                        thickness = 3000)

    site = nx.Hyperfine(magnetic_field = 33,
                        magnetic_theta = 90,
                        magnetic_phi = 0,
                        quadrupole = 0.6)

    site.SetRandomDistribution("efg", "3D", 401)

    iron.hyperfine_sites = [site]

    sample = nx.Sample(layers = [layer_Fe])

    beam = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.lib.moessbauer.Fe57)

    time_spectrum = nx.TimeSpectrum(experiment = exp,
                                    time_length = 200,
                                    time_step = 0.2,
                                    bunch_spacing = 192.2, # PETRA III bunch spacing
                                    id = "my time spectrum")

    timeaxis, intensity = time_spectrum.Calculate()

    plt.semilogy(timeaxis, intensity)
    plt.xlabel('time (ns)')
    plt.ylabel('intensity ($\Gamma$/ns)')
    plt.show()

.. image:: time_spectrum.png

.. note:: In case electronic is set to ``True``, the peak intensity at time zero depends on the time settings.
          It is not to real scale with the nuclear scattering.

You can also ask for the detuning grid on which the scattering matrices are calculated via 

.. code-block::

    print(np.array(time_spectrum.detuning))

When you want to know the detuning grid actually used for the FFT, set the maximum detuning also to zero.

.. code-block::

    time_spectrum.max_detuning = 0
    print(np.array(time_spectrum.detuning))

When ``max_detuning`` is not zero, the scattering matrices at largest calculated detuning are padded to this detuning grid.


Time Spectrum Amplitude
-----------------------

The :class:`AmplitudeTime` is very similar to an time spectrum but instead of intensities it provides complex amplitudes.
Amplitude methods are of :class:`Measurement` type, cannot be fit to experimental data, and are intended for theoretical calculations.
Therefore, all experimental parameters of the :class:`FitMeasurement` are not available.
Thickness or angular distributions cannot be applied because they assume incoherent addition of intensities.

The specific parameters for an amplitude time spectrum are:


* ``time_length``: Sets the total length for the calculation of the :class:`TimeSpectrum` in nanoseconds.
* ``time_step``: Sets the step size for the calculation of the :class:`TimeSpectrum` in nanoseconds.
* ``electronic``: Either ``True`` or ``False``. If electronic is ``True`` electronic and nuclear scattering are taken into account. For an amplitude time spectrum it will result in a large intensity at time zero due to the prompt electronic scattering. The default value is ``False`` as you are mostly interested in the delayed nuclear signal only, see :ref:`theory`.

To calculate amplitude properties the beam has to have a valid Jones vector defined.
See, the tutorial section :ref:`tut-beam` for more details.

The :class:`AmplitudeTime` returns an array of Jones vectors.
So each array has the shape (time_axis_points, 2) with complex entries.
The first complex entry along the second dimension corresponds to the :math:`\sigma` component, the second to the :math:`\pi` component of the scattered field.

.. code-block::

   import nexus as nx
   import numpy as np
   import matplotlib.pyplot as plt

   iron = nx.Material.Template(nx.lib.material.Fe)

   layer_Fe = nx.Layer(id = "Fe layer",
                       material = iron,
                       thickness = 3000)

   site = nx.Hyperfine(magnetic_field = 33,
                       isotropic = True)

   iron.hyperfine_sites = [site]

   sample = nx.Sample(layers = [layer_Fe])

   beam = nx.Beam()
   beam.LinearSigma()

   exp = nx.Experiment(beam = beam,
                       objects = [sample],
                       isotope = nx.lib.moessbauer.Fe57)

   detuning = np.linspace(-200, 200, 1001)

   amp_time = nx.AmplitudeTime(experiment = exp,
                               time_length = 200,
                               time_step = 0.2,
                               electronic = False,
                               id = "my amplitude time")

   time_axis, amp = amp_time.Calculate()

   amp_sigma = amp[:,0]
   amp_pi = amp[:,1]

   amp_time.Plot(sigma=True, pi=False, polar=True, unwrap=False)

.. image:: amplitude_time.png
