.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2023 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklag@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Energy Spectrum
---------------

The :class:`EnergySpectrum` method is the most general class to calculate energy dependent experiments.
To calculate Moessbauer spectra or a conversion electron Moessbauer (CEM) spectra it is recommended to use the specific implementations, which just call this method with special initialization parameters.

You can specify the standard parameters for a FitMeasurement, see :ref:`tut-measurement`.
Note, that the standard kernel type for convolutions in Nexus is Gaussian.
The convolution of a Gaussian kernel and a Lorentzian resonance will result in a Voigt profile.
Depending on your experiment this may not what you want to simulate, so you can choose a Lorentzian kernel as well.

The specific parameters for the :class:`EnergySpectrum` are

* ``detuning``: List with detuning values in units of the natural linewidth :math:`\Gamma`.
* ``intensity_data``: List of intensities measured during the experiment.
  Only needed for fitting.
* ``electronic``: Either ``True`` or ``False``.
  If electronic is ``True`` both electronic and nuclear scattering are taken into account.
  It should be used to simulate the proper transmission through the sample.
  It will result in an absorption spectrum.
  In case you are only interested in the nuclear scattering properties set this parameter to ``False``.
  In this case pure electronic scattering will be subtracted, see :ref:`theory`.
  This will result in a pure nuclear emission spectrum.
* ``time_gate``: An empty or two element list.
  If a two element list is passed these two values [start, stop] are taken as a time gating of the energy spectrum calculation.
  Given in ns.
  Default is an empty list.
  The spectrum is Fourier transformed to the time domain, then the time gate is applied, and transformed back to the energy domain.
  Time gating will result in a reshaping of the spectrum depending cut components.
  Please note the for a proper Fourier transform, the detuning range large, typically larger than 1000 :math:`\Gamma`, and the step size should be smaller 0.5 :math:`\Gamma`.

  .. versionadded:: 1.0.1

.. code-block::

   import nexus as nx
   import numpy as np
   import matplotlib.pyplot as plt

   iron = nx.Material.Template(nx.lib.material.Fe)

   layer_Fe = nx.Layer(id = "Fe layer",
                       material = iron,
                       thickness = 3000)

   site = nx.Hyperfine(magnetic_field = 33,
                       isotropic = True)

   iron.hyperfine_sites = [site]

   sample = nx.Sample(layers = [layer_Fe])

   beam = nx.Beam(polarization = 0)

   exp = nx.Experiment(beam = beam,
                       objects = [sample],
                       isotope = nx.lib.moessbauer.Fe57)

   detuning = np.linspace(-200, 200, 1001)

   energy_spectrum = nx.EnergySpectrum(experiment = exp,
                                       detuning = detuning,
                                       electronic = True,
                                       scaling = "auto",
                                       background = 0,
                                       resolution = 1,
                                       distribution_points = 1,
                                       fit_weight = 1,
                                       kernel_type = "Gauss",
                                       residual = None,
                                       id = "my energy spectrum")

   intensity = energy_spectrum.Calculate()

   plt.plot(detuning, intensity)
   plt.xlabel('detuning (Gamma)')
   plt.ylabel('rel. transmission')
   plt.show()

.. image:: energy_spectrum.png

.. note:: In case you only want to calculate the electronic contribution to a spectrum either use the methods described in the section *Electronic properties of the experiment* or just do not define a hyperfine site.



Amplitude Spectrum
------------------

The :class:`AmplitudeSpectrum` is very similar to an energy spectrum but instead of intensities it provides complex amplitudes.
Amplitude methods are of :class:`Measurement` type, cannot be fit to experimental data, and are intended for theoretical calculations.
Therefore, all experimental parameters of the :class:`FitMeasurement` are not available.
Thickness or angular distributions cannot be applied because they assume incoherent addition of intensities.

The specific parameters for an amplitude spectrum are:

* ``experiment``: Experiment for the calculation.
* ``detuning``: List with detuning values in units of the natural linewidth :math:`\Gamma`.
* ``electronic``:  Either ``True`` or ``False``.
  If electronic is ``True`` both electronic and nuclear scattering are taken into account.
  It should be used to simulate the proper transmission through the sample.
  It will result in an absorption spectrum.
  In case you are only interested in the nuclear scattering properties set this parameter to ``False``.
  In this case pure electronic scattering will be subtracted, see :ref:`theory`.
  This will result in a pure nuclear emission spectrum.
* ``time_gate``: An empty or two element list.
  If a two element list is passed these two values [start, stop] are taken as a time gating of the energy spectrum calculation.
  Given in ns.
  Default is an empty list.
  The spectrum is Fourier transformed to the time domain, then the time gate is applied, and transformed back to the energy domain.
  Time gating will result in a reshaping of the spectrum depending cut components.
  Please note the for a proper Fourier transform, the detuning range large, typically larger than 1000 :math:`\Gamma`, and the step size should be smaller 0.5 :math:`\Gamma`.

  .. versionadded:: 1.0.1

To calculate amplitude properties the beam has to have a valid Jones vector.
See :ref:`tut-beam` section for more details.

The :class:`AmplitudeSpectrum` returns an array of Jones vectors.
So each array has the shape (detuning points, 2) with complex entries.
The first complex entry along the second dimension corresponds to the :math:`\sigma` component, the second to the :math:`\pi` component of the scattered field.

.. code-block::

   import nexus as nx
   import numpy as np
   import matplotlib.pyplot as plt

   iron = nx.Material.Template(nx.lib.material.Fe)

   layer_Fe = nx.Layer(id = "Fe layer",
                       material = iron,
                       thickness = 3000)

   site = nx.Hyperfine(magnetic_field = 33,
                       isotropic = True)

   iron.hyperfine_sites = [site]

   sample = nx.Sample(layers = [layer_Fe])

   beam = nx.Beam()
   beam.LinearSigma()

   exp = nx.Experiment(beam = beam,
                       objects = [sample],
                       isotope = nx.lib.moessbauer.Fe57)

   detuning = np.linspace(-200, 200, 1001)

   amp_spectrum = nx.AmplitudeSpectrum(experiment = exp,
                                       detuning = detuning,
                                       electronic = False,
                                       id = "my anplitude spectrum")

   amp = amp_spectrum.Calculate()

   amp_sigma = amp[:,0]
   amp_pi = amp[:,1]

   plt.plot(detuning, np.real(amp_sigma), label = 'real')
   plt.plot(detuning, np.imag(amp_sigma), label = 'imag')
   plt.legend()
   plt.xlabel('detuning (Gamma)')
   plt.ylabel('amplitude')
   plt.show()

.. image:: amplitude_spectrum.png
