.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Electronic properties of the experiment
---------------------------------------

These methods calculate properties of pure electronic scattering.
Typically, electronic scattering is considered without any polarization dependence, here however, polarization analyzers (:class:`Analyzer`) or optically active elements (:class:`FixedObject`) can also be included.

All methods described here are of :class:`Measurement` type, cannot be fit to experimental data, and are intended for theoretical calculations.
Therefore, all experimental parameters of the :class:`FitMeasurement` are not available.

The only parameter you have to specify for all methods, except the :class:`experiment`, is:

* ``energy``: The photon energy for the calculation in eV.


Amplitudes
**********

The :class:`Amplitudes` method provides the complex scattering amplitude behind each object in the experiment.

The :class:`Amplitudes` method is not dependent on the polarization state.
Only the scattering factors are calculated.
For :class:`Analyzers` and :class:`FixedObjects` the provided ``scattering_factor`` is used.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    iron = nx.Material.Template(nx.lib.material.Fe)

    layer_Fe = nx.Layer(id = "Fe layer",
                        material = iron,
                        thickness = 3000)

    sample1 = nx.Sample(layers = [layer_Fe])

    layer_Pt = nx.Layer(id = "Pt layer",
                        material = nx.Material.Template(nx.lib.material.Pt),
                        thickness = 1000)

    sample2 = nx.Sample(layers=[layer_Pt])

    beam = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample1, sample2],
                        isotope = nx.lib.moessbauer.Fe57)

    electronic_amplitudes = nx.Amplitudes(experiment = exp,
                                          energy = 14400)

    print(electronic_amplitudes())

Here you will get a list with two complex numbers ``[0.73356471-0.57043914j 0.67507685+0.37599394j]`` each corresponding to the amplitude of the photon field behind each sample.
The input field in *Nexus* is assumed to be ``1+j0``.
After the first sample the field amplitude is ``0.73356471-0.57043914j`` and after the second sample it is ``0.67507685+0.37599394j``.


Matrices
********

The :class:`Matrices` method provides the accumulated complex scattering matrix up to each object in the experiment.

The :class:`Matrices` method depends on the polarization state.
For :class:`Analyzers` and :class:`FixedObjects` the scattering matrices are used, i.e. the :attr:`Analyzer.matrix` and the :attr:`FixedObject.matrix`.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    iron = nx.Material.Template(nx.lib.material.Fe)

    layer_Fe = nx.Layer(id = "Fe layer",
                        material = iron,
                        thickness = 3000)

    sample1 = nx.Sample(layers = [layer_Fe])

    layer_Pt = nx.Layer(id = "Pt layer",
                        material = nx.Material.Template(nx.lib.material.Pt),
                        thickness = 1000)

    sample2 = nx.Sample(layers=[layer_Pt])

    beam = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample1, sample2])

    scattering_matrices = nx.Matrices(experiment = exp,
                                      energy = 14400)

    print(scattering_matrices())

You will get two 2x2 matrices corresponding to the two accumulated scattering matrices up to each sample.

.. code-block::

  [[[0.73356471-0.57043914j 0.        +0.j        ]
    [0.        +0.j         0.73356471-0.57043914j]]

   [[0.67507685+0.37599394j 0.        +0.j        ]
    [0.        +0.j         0.67507685+0.37599394j]]]


Jones vectors
*************

The :class:`JonesVectors` method provides the complex Jones vector of the photon field behind each object in the experiment.

The :class:`JonesVectors` method depends on the polarization state.
A proper Jones vector has to be set in the :class:`Beam` object of the experiment.
For :class:`Analyzers` and :class:`FixedObjects` the scattering matrices are used, i.e. the :attr:`Analyzer.matrix` and the :attr:`FixedObject.matrix`.


.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    iron = nx.Material.Template(nx.lib.material.Fe)

    layer_Fe = nx.Layer(id = "Fe layer",
                        material = iron,
                        thickness = 3000)

    sample1 = nx.Sample(layers = [layer_Fe])

    layer_Pt = nx.Layer(id = "Pt layer",
                        material = nx.Material.Template(nx.lib.material.Pt),
                        thickness = 1000)

    sample2 = nx.Sample(layers=[layer_Pt])

    beam = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample1, sample2])

    jones_vectors = nx.JonesVectors(experiment = exp,
                                    energy = 14400)

    print(jones_vectors())

    # now we add an analyzer
    print("\nwith Pi analyzer")

    analyzer  = nx.Analyzer()
    analyzer.LinearPi()

    exp.objects = [sample1, analyzer, sample2]

    print(jones_vectors())

You will get complex entries corresponding to the Jones vectors behind each object.

.. code-block::

   [[0.73356471-0.57043914j 0.        +0.j        ]
    [0.67507685+0.37599394j 0.        +0.j        ]]

   with Pi analyzer
   [[0.73356471-0.57043914j 0.        +0.j        ]
    [0.        +0.j         0.        +0.j        ]
    [0.        +0.j         0.        +0.j        ]]


Intensities
***********

The :class:`Intensities` method provides the intensity of the photon field behind each object in the experiment.

The :class:`Intensities` method depends on the polarization state.
For :class:`Analyzers` and :class:`FixedObjects` the scattering matrices are used, i.e. the :attr:`Analyzer.matrix` and the :attr:`FixedObject.matrix`.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    iron = nx.Material.Template(nx.lib.material.Fe)

    layer_Fe = nx.Layer(id = "Fe layer",
                        material = iron,
                        thickness = 3000)

    sample1 = nx.Sample(layers = [layer_Fe])

    layer_Pt = nx.Layer(id = "Pt layer",
                        material = nx.Material.Template(nx.lib.material.Pt),
                        thickness = 1000)

    sample2 = nx.Sample(layers=[layer_Pt])

    beam = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample1, sample2])

    intensities = nx.Intensities(experiment = exp,
                                 energy = 14400)

    print(intensities())

You will get ``[0.863518  0.5971002]`` corresponding to the two field intensities behind each sample.
