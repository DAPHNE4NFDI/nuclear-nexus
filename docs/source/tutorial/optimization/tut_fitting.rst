.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Fitting
-------

The :class:`Fit` module fits the theoretical model parameters to experimental data sets.
In order to do so, the intensity and the corresponding free parameter (detuning, time, angle, ...) have to be specified in the :class:`FitMeasurements`.
The fit module only takes a list of :class:`FitMeasurement` objects as an argument.
As all the corresponding model parameters are defined in the :class:`Experiment` and the experimental data and conditions are defined in the :class:`FitMeasurement`, the latter is the only object needed to perform the fit.

The module will find every :class:`Var` object it depends on and automatically adds them to the fit.
Remember that every fit parameter has to be defined as a :class:`Var` whose :attr:`fit` attribute is set to ``True``.
:class:`Vars` that are set to ``.fit = True`` but do not belong to the one of the passed measurements will not be taken into account.
A :class:`FitMeasurement` object has a :attr:`scaling` which is always added to the fit even if not defined as a fit parameter.
This behavior can be changed by defining the ``scaling`` as a :class:`Var` and setting ``.fit = False``.

You should always define reasonable :attr:`min` and :attr:`max` boundaries for a fit variable.
For the gradient-based local algorithms that is often not needed to run a good fit but for global methods it is mandatory.
Otherwise the search areas can be totally unrealistic and the fit might never find a good minimum.

Go to the :ref:`tut-hello-nexus` for a basic fit example.
It used the Levenberg-Marquardt and the Differential Evolution algorithm.
We will repeat this example here but use another global fit algorithm.


Fit options
***********

All options of the fit module are accessible by the :attr:`options` attribute.
It is an instance of the :class:`OptimizerOptions` class.
To change the algorithm, for example, you can create a ``fit`` object and change the algorithm like this

.. code-block::

   import nexus as nx

   fit = nx.Fit(id = "my fit module")

   fit.options.method = "DiffEvol"
   # or
   fit.options.method = "BeeColony"
   # or
   fit.options.method = "BasinHopping"

   # or you can change the maximum number of iterations of the fit module
   fit.options.iterations = 100

There are a lot of options in the :class:`OptimizerOptions`.
Four methods have special options, those are the ``LineSearch``, ``CompassSearch``, ``DiffEvol``, and ``BasinHopping``.
In case you want to change parameter of these use

.. code-block::

   # set the inner algorithm of the Basin Hopping method
   fit.options.BasinHopping.inner = "Compass"

   # or set the number of unsuccessful iterations
   fit.options.BasinHopping.stop = 10

Please have a look at the :ref:`api-optimizer-options` for all options of both :class:`Fit` and :class:`Optimizer` module.


Global fitting
**************

Here, an example similar to the one the :ref:`tut-hello-nexus` example is treated but with the global fitting algorithm ``BasinHopping``.

.. note:: Always define reasonable boundaries on a fit variable for global optimization.
          This also holds for the :attr:`scaling` parameter of a :class:`FitMeasurement` method.
          The ``auto`` option for :attr:`scaling` or :attr:`background` will set boundaries but with a quite large range.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    velocity_experiment, intensity_experiment = nx.data.Load('example_spectrum.txt', 0, 1)

    site = nx.Hyperfine(magnetic_field = nx.Var(value = 28, min = 25, max = 35, fit = True, id = "magnetic field"),
                        isotropic = True)

    mat_Fe = nx.Material.Template(nx.lib.material.Fe)

    mat_Fe.hyperfine_sites = [site]

    layer_Fe = nx.Layer(id = "Fe",
                         material = mat_Fe,
                         thickness = 3000)

    sample = nx.Sample(layers = [layer_Fe])

    beam = nx.Beam()
    beam.Unpolarized()

    exp = nx.Experiment(beam = beam,
                         objects = [sample],
                         isotope = nx.lib.moessbauer.Fe57)

    spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                     velocity = velocity_experiment,
                                     intensity_data = intensity_experiment,
                                     scaling = "auto")

    # calculate the intensity from the assumed model
    intensity = spectrum.Calculate()

    # plot both the model and the measured data
    plt.plot(velocity_experiment, intensity_experiment)
    plt.plot(velocity_experiment, intensity)
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

.. image:: initial.png

Both the scaling and the hyperfine field are off.
Now run the fit.

.. code-block::

    # create a fit object with a list of measurements to be fit in parallel.
    fit = nx.Fit(measurements = [spectrum])

    # change to the basin hopping algorithm with inner algorithm suplex
    fit.options.method = "BasinHopping"
    fit.options.BasinHopping.inner = "Subplex"

    # run the fit
    fit.Evaluate()

    plt.plot(velocity_experiment, intensity_experiment)
    plt.plot(velocity_experiment, spectrum.result)
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()


The ``fit`` object will give the following output

.. code-block::

    Starting fit with 1 measurement data set(s) and 2 fit parameter(s).

      no. |                           id |       initial value |              min |              max
        0 |                   ES scaling |             2638.74 |                0 |           263874
        1 |               magnetic field |                  28 |               25 |               35

    Calling Pagmo solver with fit method BasinHopping

    inner algorithm: Subplex
      population: 30
      iterations: 100

      cost = 2.136686e+01

    Calling ceres solver with fit method LevMar

    Ceres Solver Report: Iterations: 1, Initial cost: 2.136686e+01, Final cost: 2.136686e+01, Termination: CONVERGENCE

    Fit performed with 2 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                   ES scaling |             3000.47 |             2638.74 |                0 |           263874
        1 |               magnetic field |             32.9955 |                  28 |               25 |               35

      total cost = 2.136686e+01

    cost for each FitMeasurement is:

      no. |                           id |                cost |                   %
        0 |                              |        2.136686e+01 |             100.000

You can observe that the global fit algorithm ``BassinHopping`` is followed by a call of the local algorithm ``LevMar``.
This is the local search following the global optimization.
In case you want to change the algorithm of the local search you can change it with the :attr:`options.local` attribute.

Let's have a look to the fit result.

.. image:: fit_result.png


In case you want to set the fit parameters back to their initial values use the :attr:`SetInitialVars()` method

.. code-block::

    fit.SetInitialVars()

    plt.plot(velocity_experiment, intensity_experiment)
    plt.plot(velocity_experiment, spectrum())
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

.. image:: set_inital_vars.png

In order to look how the residuals are distributed (now with the initial parameters again) just do

.. code-block::

    residuals = intensity_experiment - spectrum()

    plt.plot(velocity_experiment, residuals)
    plt.ylabel('residual (data - model)')
    plt.show()

.. image:: residuals.png


Residuals
*********

The residual is the difference of the experimental value and the theoretical value

.. math:: r_i = y_i- \hat{y}_i.

However, in fitting this residual has to be weighted properly in order to account for the underlying measurement statistics.

*Nexus* uses the cost function

.. math::

   cost = \frac{1}{2} \sum_i \left(\sqrt{y_i} - \sqrt{\hat{y}_i}\right)^2

This is the default residual weight function in Nexus if no :attr:`residual` of a measurement is specified (``None``) or if ``nx.lib.residual.Sqrt()`` is used, see :ref:`api-residual` section.

.. note::
   For the classes :class:`Reflectivity` and :class:`Transmission` a log norm is applied now as the standard residual :math:`r_i = \log_{10}(y_i) - \log_{10}(\hat{y}_i)`.
     
   .. versionchanged:: 1.0.3

.. admonition:: Changed in version 2.0.0
   
   Since version 2.0.0 the user can specify the cost function with an adjustable exponent.

   .. math::

      cost = \frac{1}{2} \sum_i \left(implementation \right)^{exponent}

   In this way it is possible to use a log likelihood implementation with an exponent of one.
   Use ``residual = lib.residual.LogLikelihood()`` for Poisson statistics.
   For least square problems, the exponent is 2.
   For log-likelihood estimates it is 1.


The *Python* implementations enable you to change the residual calculation to your needs.
In case you want to change the residual you can take the ones from the ``lib.residual`` library (:ref:`api-residual`).
For example, you can change the residual not to be weighted just by specifying the difference of the values

.. code-block::

    reflectivity = nx.Reflectivity(experiment = ...,
                                   sample = ...,
                                   energy = ...,
                                   angles = ...,
                                   intensity = ...,
                                   residual = nx.lib.residual.Difference()
                                   scaling = ...,
                                   ...)

It is also possible to define your own residual weighting using the :class:`Residual` class.
Let's implement a square root scaling.
This already exits as ``nx.lib.residual.Sqrt`` but it will serve as as an example.

.. code-block::

    class MySqrt(nx.Residual):
        # standard initializer
        def __init__(self, id = "MySqrt"):
            super().__init__(id)

        # residual function implementation
        # keep the inputs as they are
        def ResidualFunction(self, input_data, input_theory):
            sqrt_data = np.where(np.array(input_data) >= 0, np.sqrt(input_data), 0.0)
            sqrt_theory = np.where(np.array(input_theory) >= 0, np.sqrt(input_theory), 0.0)
            return np.subtract(sqrt_data, sqrt_theory)

     # pass your implementation
     reflectivity.residual = MySqrt()

That's it.

.. admonition:: Changed in version 2.0.0

   Since version 2.0.0 you have to specify the exponent for the residuals explicitly.
   Use

   .. code-block::

      class MySqrt(nx.Residual):
        # standard initializer
        def __init__(self, id, exponent, plot_string):
            super().__init__(id, exponent, plot_string)

        # residual function implementation
        # keep the inputs as they are
        def ResidualFunction(self, input_data, input_theory):
            sqrt_data = np.where(np.array(input_data) >= 0, np.sqrt(input_data), 0.0)
            sqrt_theory = np.where(np.array(input_theory) >= 0, np.sqrt(input_theory), 0.0)
            return np.subtract(sqrt_data, sqrt_theory)

      # pass your implementation
      reflectivity.residual = MySqrt(id = "MySqrt", exponent = 2, plot_string = r"$\sqrt{x} - \sqrt{x_t}$")

   The difference of the square roots will be squared due to exponent=2.
   Before version 2.0.0 an exponent of 2 has been assumed automatically.

In order to look for the weighted residuals of the global fit example given before you can do

.. code-block::
    
    residuals = nx.lib.residual.Sqrt().ResidualFunction(intensity_experiment, spectrum.result)

    plt.plot(velocity_experiment, residuals)
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('residual ($\sqrt{data}$ - $\sqrt{model}$)')
    plt.show()

.. image:: residuals_weighted.png

.. note:: Be careful not to run into any division by zero or any kind of similar problem in your own implementation.

Log-likelihood
##############

Since version 2.0.0 you can use a log-likelihood residual.
Note, that this residual is not working with gradient based methods of the ceres solver (``LevMar``, ``DogLeg``, ``SubDogLeg`` and ``LineSearch``).
In addition, the fit ``options.error_method`` must be ``None`` or ``Bootstrap``.

For global fit methods, set the local method to ``Subplex`` or ``Newuoa``.

In combined fitting all measurements need to have this residual, when used.

Here's an example on how to use the log-lokelihood estimate:

.. code-block::

    ...

    reflectivity = nx.Reflectivity(experiment = ...,
                                   sample = ...,
                                   energy = ...,
                                   angles = ...,
                                   intensity = ...,
                                   residual = nx.lib.residual.LogLikelihood(),
                                   scaling = ...,
                                   ...)

    ...

    fit = nx.Fit(measurements = [reflectivity])

    # do not use the LevMar algorithm
    # e.g.
    fit.options.method = "Subplex"
    # or
    fit.options.method = "Annealing"
    fit.options.local = "Newuoa"

    # do not use Gradient error analysis
    fit.options.error_method = "None"
    # or
    fit.options.error_method = "Bootstrap"

    fit.Evaluate()

    ...


Error estimation
****************

.. versionadded:: 1.1.0

*Nexus* provides tools for error analysis on the fit parameters. 
Two methods for error calculations are provided.
The first one is based on the gradient-based local search algorithms of the ceres solver.
The second method is based on a resampling strategy called bootstrapping.

Always keep in mind that reliable error analysis of fit problems is a complicated task.
The calculated errors are the estimated standard deviations of the fit parameter.


Gradient based error analysis
#############################

For this model to be valid, a normal distribution of the errors with zero mean is assumed.

The error calculation is based on the gradient approach using the Jacobian matrix :math:`J` of the fit problem.
The inverse Hessian matrix is 

.. math:: H = (J^*J)^{-1}

or in the case of a rank-deficient :math:`J` (meaning some parameters have no influence on the fit problem), :math:`H` is given by the Moore-Penrose pseudo inverse

.. math:: H = (J^*J)^{+}.

The fit problem will generate a covariance matrix :math:`C` describing the joint variability of the fit parameters.
The covariance matrix is 

.. math:: C =  f H.

with

.. math:: f = \dfrac{1}{n_d - n_p} \sum_i w_i r_i^2,

where :math:`w_i` is the weight of the residual, :math:`r_i` is the residual implementation, :math:`n_d` is the number of data points of the fit problem, and :math:`n_p` is the number of fit parameters.


.. seealso:: `<http://ceres-solver.org/nnls_covariance.html>`_


Bootstrap error analysis
########################

Bootstrapping is a random resampling technique to estimate the variability of a fit parameter from a single data set.
The newly generated data sets are fit and from all the fits the mean fit parameters, errors and the covariance matrix are determined.
A typical number of bootstrap data sets is between 50 to 100.
So, the method is computationally expensive.
The benefit is that the underlying distribution function of the data do not need to be known.

Several version exist how to implement the bootstrap algorithm.
The main difference is parametric and non-parametric bootstrapping:

* Parametric bootstrapping resamples from a known distribution, you trust the underlying model you assume.
  In *Nexus* this can be either the Poisson distribution or Gaussian distribution (whose sigma is given by the square root of the variance).
  A Poisson distribution accounts for the proper count statistics of a histogram.

* Non-parametric bootstrapping does not make any assumption on the underlying distribution function.
  The data points have to be resampled without using any model assumption on the data.
  Therefore, residual bootstrapping, wild bootstrapping, and smooth bootstrapping are used. 
  The residual bootstrap takes the first fit and randomly picks a residual (from all the residuals in a data set ) and puts it on the fit curve to generate new bootstrap data sets.
  Wild bootstrap reweights the residuals of the first fit by a Gaussian distribution with variance one to generate bootstrap data sets.
  Smooth bootstrapping uses the original data sets and applies a smoothing method (uniform or Gaussian) with smoothing parameter specified by the user.
  The default is the wild bootstrip as it relies only on the residual of the individual data points and can be apllied to heteroscedastic data sets.

.. seealso:: `<https://en.wikipedia.org/wiki/Bootstrapping_(statistics)>`_

             `<https://en.wikipedia.org/wiki/Homoscedasticity_and_heteroscedasticity>`_


Parameter errors, covariances and the correlation matrix
########################################################

The covariance measures the joint variability of fit parameters.
Both error methods will provide the covariance.
The higher the covarinace of two parameters the higher the error of these parameters will be.

.. seealso:: `<https://en.wikipedia.org/wiki/Covariance>`_

             `<https://en.wikipedia.org/wiki/Covariance_matrix>`_

The correlation matrix is determined from the Pearson correlation coefficient and measures the linear correlation between two fit parameters.
It normalizes the covariance matrix between -1 and 1.

.. seealso:: `<https://en.wikipedia.org/wiki/Correlation>`_

             `<https://en.wikipedia.org/wiki/Pearson_correlation_coefficient>`_

The diagonal elements of the covariance matrix are the variances of the fit parameters and the standard deviation is given by square root of these elements.
So the error vector of the estimated standard deviations is

.. math:: \sigma_p = \sqrt{\operatorname{diag}\left(C\right)}.

As noted these errors are estimates, especially when the provided residual is not simply weighted by unity or the variance but other functional dependencies are present like the :attr:`residual.Sqrt` or  :attr:`residual.Log10`.

Example
#######

Here is an example on how to use the error analysis

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    data = np.loadtxt('example_spectrum.txt')

    velocity_experiment = data[:,0]
    intensity_experiment = data[:,1]

    site = nx.Hyperfine(isomer = nx.Var(value = 2, min = -3, max = 3, fit = True, id = "isomer"),
                        magnetic_field = nx.Var(value = 31, min = 25, max = 35, fit = True, id = "magnetic field"),
                        magnetic_theta = nx.Var(value = 0, min = -15, max = 15, fit = True, id = "magnetic theta"),
                        quadrupole = nx.Var(value = 0.3, min = 0, max = 2, fit = True, id = "quadrupole"),
                        isotropic = True)

    mat_Fe = nx.Material.Template(nx.lib.material.Fe)

    mat_Fe.hyperfine_sites = [site]

    layer_Fe = nx.Layer(id = "Fe",
                        material = mat_Fe,
                        thickness = nx.Var(value = 2900, min = 0, max = 5000, fit = True, id = "thickness"))

    sample = nx.Sample(layers = [layer_Fe])

    beam = nx.Beam()

    beam.Unpolarized()

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.lib.moessbauer.Fe57)

    spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                     velocity = velocity_experiment,
                                     intensity_data = intensity_experiment,
                                     scaling = "auto")

    intensity = spectrum.Calculate()

    fit = nx.Fit(measurements = [spectrum])

    # default
    # standard call since version 1.1.0
    fit.options.error_method = "Gradient" 

    fit.Evaluate()

Part of the output is

.. code-block::

    Fit performed with algorithm:
    LevMar
    Error analysis:
    Gradient

    Using 6 fit parameter(s):

      no. |                           id |          fit value |   +/- std dev | initial value |          min |          max
        0 |                   ES scaling |            1682.71 |       284.377 |       2638.74 |            0 |       263874
        1 |                    ES backgr |            1276.44 |       308.362 |       257.138 |            0 |      25713.8
        2 |                    thickness |               5000 |       1326.82 |          2900 |            0 |         5000
        3 |               magnetic field |            32.9954 |     0.0095092 |            31 |           25 |           35
        4 |               magnetic theta |                  0 |             0 |             0 |          -15 |           15
        5 |                   quadrupole |        0.000658927 |    0.00259623 |           0.3 |            0 |            2

    Correlation matrix:

      no. |        0        1        2        3        4        5 
     -----|------------------------------------------------------
        0 |    1.000   -1.000   -0.998   -0.000      nan   -0.000 
        1 |   -1.000    1.000    0.999    0.000      nan    0.000 
        2 |   -0.998    0.999    1.000    0.000      nan    0.000 
        3 |   -0.000    0.000    0.000    1.000      nan    0.000 
        4 |      nan      nan      nan      nan      nan      nan 
        5 |   -0.000    0.000    0.000    0.000      nan    1.000 

    Found 1 parameters without influence on the fit model:

      no. |                           id
        4 |               magnetic theta

    It is recommended to remove these parameters from the fit or to change the start parameters.

The first three parameters show a strong covariance, meaning that their fit values strongly depend on each other.
This leads to a rather high error (+/- std dev) in these parameters.
The magnetic field and the quadrupole splitting show no correlation to the other parameters ad their errors are rather small.
The polar magnetic angle does not show any influence on the fit.
Its variance is ``nan``.
Such parameters should be removed from the fit or other start parameters should be used.

In order to get the values used for the calculations you can use 

.. code-block::

    inverseHessian = fit.inverse_hessian

    covariance_matrix = fit.covariance_matrix

    correlation_matrix = fit.correlation_matrix

    errors = np.squeeze(fit.fit_parameter_errors)

    print(errors)

.. code-block::

    [2.84377442e+02 3.08362357e+02 1.32681916e+03 9.50919595e-03
     0.00000000e+00 2.59623390e-03]

   

Scaling and background
**********************

The :attr:`scaling` option is set to ``auto`` and :attr:`background` option to ``0`` in the standard setting.
:attr:`background` can also be set to ``auto`` in case a background should be fit.
In case you want to set your own values you can change these two attributes

.. code-block::
  
    spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                     velocity = velocity_experiment,
                                     intensity_data = intensity_experiment,
                                     scaling = 10,    # constant value
                                     background = "auto")

    # or
    spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                     velocity = velocity_experiment,
                                     intensity_data = intensity_experiment,
                                     scaling = nx.Var(10, min = 0, max = 100, fit = True, id = "scaling"))

    spectrum.background = nx.Var(3, min = 0, max = 20, fit = True, id = "background")


Multi measurement fitting
*************************

One of the big advantages of *Nexus* compared to other software packages is the capability to fit multiple data sets together and even directly fit them to an underlying model.
In order to do so, you just have to provide several :class:`FitMeasurements`.
The measurements do not need to come from the same :class:`Experiment`.
You are free in passing any kind of combination of :class:`FitMeasurments` or models to the fit routines.


Multi-fit from a single experiment
**********************************

Let's assume you have several data sets from a single sample, here, an iron thin film on a silicon substrate.
The data set consists of a reflectivity and a time spectrum measured at the critical angle (0.225 deg).
One could try to fit all data sets one after another, which is often a good idea. 
You could first get the structural parameters from the reflectivities and the hyperfine parameters from the time spectrum.

Another approach is to fit all data sets together.
This can be very helpful when no consistent picture can be found with several single fits or when you have a lot of free parameters in your experiment.
Here's an example how to fit all data together

:download:`reflectivity <reflectivity.txt>`

:download:`time_spectrum <time_spectrum.txt>`

Load the data.

.. code-block::

        import nexus as nx
        import numpy as np
        import matplotlib.pyplot as plt
        from scipy.signal import argrelextrema

        angles_data, reflectivity_data = nx.data.Load("reflectivity.txt", 0, 1)

        time_data, time_spec_data = nx.data.Load("time_spectrum.txt", 0, 1)

Then set up the model and plot all together with initial guesses for some fit parameters.

.. code-block::

    beam = nx.Beam()
    beam.LinearSigma()

    # set up Fe layer on Silicon wafer
    site = nx.Hyperfine(magnetic_field = nx.Var(32, min = 30, max = 35, fit = True, id = "magnetic field"))

    site.SetRandomDistribution(target = "mag", type = "2Dsp", points = 401)

    mat_Fe = nx.Material.Template(nx.lib.material.Fe)

    mat_Fe.density = nx.Var(7.4, min = 7, max = 7.874, fit = True, id = "Fe density")

    mat_Fe.hyperfine_sites = [site]

    layer_Fe = nx.Layer(thickness = nx.Var(19.5, min = 17, max = 23, fit = True, id = "Fe thickness"),
                        roughness = nx.Var(0.5, min = 0.1, max = 1, fit = True, id = "Fe roughness"),
                        material = mat_Fe)

    substrate = nx.Layer(thickness = nx.inf,
                         roughness = nx.Var(0.1, min = 0.0, max = 1, fit = True, id = "Si roughness"),
                         material = nx.Material.Template(nx.lib.material.Si))

    sample = nx.Sample(layers = [layer_Fe, substrate],
                       geometry = "r",
                       length = 10,
                       roughness = "a")

    # experiment

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.lib.moessbauer.Fe57)

    # reflectivity

    ref = nx.Reflectivity(experiment = exp,
                          sample = sample,
                          energy = nx.lib.energy.Fe57,
                          angles = angles_data,
                          intensity_data = reflectivity_data,
                          resolution = 0.001,
                          id = "reflectivity")

    plt.semilogy(angles_data, reflectivity_data, label = "data")
    plt.semilogy(angles_data, ref(), label = "model")
    plt.legend()
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

    # time spectrum

    sample.angle = 0.225

    time_spectrum = nx.TimeSpectrum(experiment = exp,
                                    time_length = 200,
                                    time_step = 0.2,
                                    bunch_spacing = 192.2, # PETRA III bunch spacing
                                    time_data = time_data,
                                    intensity_data =  time_spec_data,
                                    id = "time spectrum")

    time_axis , intensity = time_spectrum.Calculate()

    plt.semilogy(time_data, time_spec_data, label = "data")
    plt.semilogy(time_axis, intensity, label = "model")
    plt.legend()
    plt.xlabel('time (ns)')
    plt.ylabel('intensity')
    plt.show()

.. image:: single_exp_ref_inital.png

.. image:: single_exp_time_spec_inital.png


Now, set up the multi-measurement fit.
We use a local method here because the initial guess is already quite close.
However, often global methods are a better option when the initial guess is far off.

.. code-block::

    fit = nx.Fit(measurements = [ref, time_spectrum])
    
    fit.options.method = "Newuoa"

    fit.Evaluate()

The last part of the output shows

.. code-block::

    Total cost = 2.271e+03

    cost for each FitMeasurement is:

      no. |                           id |                cost |                   %
        0 |                 reflectivity |           4.523e+01 |               1.991
        1 |                time spectrum |           2.226e+03 |              98.009

Plot the results

.. code-block::

    plt.semilogy(angles_data, reflectivity_data, label = "data")
    plt.semilogy(angles_data, ref.result, label = "model")
    plt.legend()
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

    plt.semilogy(time_data, time_spec_data, label = "data")
    plt.semilogy(time_axis, time_spectrum.result, label = "model")
    plt.legend()
    plt.xlabel('time (ns)')
    plt.ylabel('intensity')
    plt.show()

.. image:: single_exp_ref_fit.png

.. image:: single_exp_time_spec_fit.png


The last part of the output shows that the reflectivity only slightly contributes to the total :math:`cost`.
The fit of the reflectivity is not optimal due to its relatively low weight.
To improve the fit, you can change the :attr:`fit_weight`.


Multi-fit weighting
*******************

When fitting multiple measurements the problem arises that the intensities and number of points can be very different.
Therefore, the :math:`cost` of the measurements might differ a lot and a single measurement might dominate the parameter fitting.
To tune the relative contribution of each fit you can set the :attr:`fit_weight` of a :class:`Measurement`.
This parameter will be multiplied to the :math:`cost` of the individual measurement, thus changing its weight in the total :math:`cost`.

.. note:: In principle it is possible to mix different :class:`Residual` objects for different :class:`FitMeasurement` objects.
          Be careful when doing so because it can strongly influence the calculated :math:`cost` and thus the weight in fitting.

.. versionchanged 1.0.3::
   The classes :class:`Reflectivity` and :class:`Transmission` use the log norm as standard residual.
 
For the previous example we can change the fit weight of the reflectivity to improve the fit of the reflectivity

.. code-block::

    fit.SetInitialVars()

    ref.fit_weight = 1e3

    fit.Evaluate()

    plt.semilogy(angles_data, reflectivity_data, label = "data")
    plt.semilogy(angles_data, ref.result, label = "model")
    plt.legend()
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

    plt.semilogy(time_data, time_spec_data, label = "data")
    plt.semilogy(time_axis, time_spectrum.result, label = "model")
    plt.legend()
    plt.xlabel('time (ns)')
    plt.ylabel('intensity')
    plt.show()

to get 

.. code-block::

    Total cost = 1.050e+04

    cost for each FitMeasurement is:

      no. |                           id |                cost |                   %
        0 |                 reflectivity |           2.068e+03 |              19.705
        1 |                time spectrum |           8.428e+03 |              80.295

.. image:: single_exp_ref_fit_weighted.png

.. image:: single_exp_time_spec_fit_weighted.png

Now, the reflectivity is fit well.
Its :math:`cost` increased because the weighting was increased by a factor of 1000.

An even better fit is expected with a global algorithm.


Multi-fit from multiple Experiments
***********************************

A typical example is to fit several Moessbauer spectra or time spectra together when an experimental parameter has been changed.
Let's assume we have changed the temperature of an experiment which will in turn affect the magnetic hyperfine field and the Lamb-Moessbauer factor.
The quadrupole splitting and isomer shift should not change.
This is a pure academic example, the data to be fit are generated with those assumptions.
We have three different Moessbauer spectra

:download:`spectrum_A <spectrum_A.txt>`

:download:`spectrum_B <spectrum_B.txt>`

:download:`spectrum_C <spectrum_C.txt>` 

which we want to fit together.

.. image:: spectrum_A.png

.. image:: spectrum_B.png

.. image:: spectrum_C.png

In the *Nexus* language those are three different experiments where the external parameter (temperature) and the internal parameters (hyperfine field, Lamb Moessbauer factor) have been changed.
So, we need to set up three experiments in which the hyperfine field and the Lamb-Moessbauer factor can be fit separately for each temperature.

For now we just take a simple iron sample in this example, with non realistic parameters.
The hyperfine parameters are assumed to be isotropically distributed.
The experiments are defined separately here, but one could also use a function to do so, see :ref:`tut-multilayer`.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    # load data
    data_A = np.loadtxt('spectrum_A.txt')

    velocities_A = data_A[:,0]
    intensity_A = data_A[:,1]

    data_B = np.loadtxt('spectrum_B.txt')

    velocities_B = data_B[:,0]
    intensity_B = data_B[:,1]

    data_C = np.loadtxt('spectrum_C.txt')

    velocities_C = data_C[:,0]
    intensity_C = data_C[:,1]

    # setup common objects
    beam = nx.Beam()
    beam.Unpolarized()

    # define a measurement for each temperature

    # there are common fit parameters as the isomer shift and the quadrupole splitting
    # they should be the same for every measurement
    # so we define them independently of the measurement

    isomer_for_all = nx.Var(1.1, min = 0, max = 1.2, fit = True, id = "isomer")

    quadrupole_for_all = nx.Var(1.0, min = 0.5, max = 1.2, fit = True, id = "quadrupole")


    # the magnetic field and the Lamb-Moessbauer factor are sample specific and have to be defined for each measurement

    # ---- iron experiment at temperature A ----

    site_A = nx.Hyperfine(weight = 1,
                          isomer = isomer_for_all,
                          magnetic_field = nx.Var(27, min = 15, max = 40, fit = True, id = "magnetic field A"),
                          quadrupole = quadrupole_for_all,
                          isotropic = True)

    mat_iron_A = nx.Material(composition = [["Fe", 1]],
                             density = 7.8,
                             isotope = nx.lib.moessbauer.Fe57,
                             abundance = 0.02119,
                             lamb_moessbauer = nx.Var(0.81, min = 0.5, max = 1, fit = True, id = "Lamb Moessbauer A"),
                             hyperfine_sites = [site_A])

    layer_iron_A = nx.Layer(thickness = 1000,
                            material = mat_iron_A)

    sample_A = nx.Sample(layers = [layer_iron_A],
                         geometry = "f")

    experiment_A = nx.Experiment(beam = beam,
                                 objects = [sample_A],
                                 isotope = nx.lib.moessbauer.Fe57)

    moessbauer_spec_A = nx.MoessbauerSpectrum(experiment = experiment_A,
                                              velocity = velocities_A,
                                              intensity_data = intensity_A)
                                          

    # ---- iron experiment at temperature B ----

    site_B = nx.Hyperfine(weight = 1,
                          isomer = isomer_for_all,
                          magnetic_field = nx.Var(23, min = 15, max = 40, fit = True, id = "magnetic field B"),
                          quadrupole = quadrupole_for_all,
                          isotropic = True)

    mat_iron_B = nx.Material(composition = [["Fe", 1]],
                             density = 7.8,
                             isotope = nx.lib.moessbauer.Fe57,
                             abundance = 0.02119,
                             lamb_moessbauer = nx.Var(0.63, min = 0.5, max = 1, fit = True, id = "Lamb Moessbauer B"),
                             hyperfine_sites = [site_B])

    layer_iron_B = nx.Layer(thickness = 1000,
                            material = mat_iron_B)

    sample_B = nx.Sample(layers = [layer_iron_B],
                         geometry = "f")

    experiment_B = nx.Experiment(beam = beam,
                                 objects = [sample_B],
                                 isotope = nx.lib.moessbauer.Fe57)

    moessbauer_spec_B = nx.MoessbauerSpectrum(experiment = experiment_B,
                                              velocity = velocities_B,
                                              intensity_data = intensity_B)

    # ---- iron experiment at temperature C ----

    site_C = nx.Hyperfine(weight = 1,
                          isomer = isomer_for_all,
                          magnetic_field = nx.Var(8, min = 0, max = 25, fit = True, id = "magnetic field C"),
                          quadrupole = quadrupole_for_all,
                          isotropic = True)

    mat_iron_C = nx.Material(composition = [["Fe", 1]],
                             density = 7.8,
                             isotope = nx.lib.moessbauer.Fe57,
                             abundance = 0.02119,
                             lamb_moessbauer = nx.Var(0.6, min = 0.5, max = 1, fit = True, id = "Lamb Moessbauer C"),
                             hyperfine_sites = [site_C])

    layer_iron_C = nx.Layer(thickness = 1000,
                            material = mat_iron_C)

    sample_C = nx.Sample(layers = [layer_iron_C],
                         geometry = "f")

    experiment_C = nx.Experiment(beam = beam,
                                 objects = [sample_C],
                                 isotope = nx.lib.moessbauer.Fe57)

    moessbauer_spec_C = nx.MoessbauerSpectrum(experiment = experiment_C,
                                              velocity = velocities_C,
                                              intensity_data = intensity_C)

    plt.plot(velocities_A, intensity_A)
    plt.plot(velocities_A, moessbauer_spec_A())
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

    plt.plot(velocities_B, intensity_B)
    plt.plot(velocities_B, moessbauer_spec_B())
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

    plt.plot(velocities_C, intensity_C)
    plt.plot(velocities_C, moessbauer_spec_C())
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

.. image:: spec_A_before_opt.png

.. image:: spec_B_before_opt.png

.. image:: spec_C_before_opt.png

The three spectra are quite far off with the initial parameters.
The local algorithms are likely to run into problems when the parameters are that far off.
Let's try the annealing algorithm.

.. code-block::

    # set up the fit
    fit = nx.Fit(measurements = [moessbauer_spec_A, moessbauer_spec_B, moessbauer_spec_C])

    fit.options.method = "Annealing"

    fit()

The output is

.. code-block::

    Starting fit with 3 measurement data set(s) and 11 fit parameter(s).

      no. |                           id |       initial value |              min |              max
        0 |                   ES scaling |              0.9528 |                0 |            95.28
        1 |            Lamb Moessbauer A |                0.81 |              0.3 |                1
        2 |                       isomer |                   2 |                0 |                3
        3 |             magnetic field A |                  27 |                0 |               40
        4 |                   quadrupole |                 1.2 |                0 |                2
        5 |                   ES scaling |            0.952803 |                0 |          95.2803
        6 |            Lamb Moessbauer B |                0.63 |              0.3 |                1
        7 |             magnetic field B |                  23 |                0 |               40
        8 |                   ES scaling |            0.952806 |                0 |          95.2806
        9 |            Lamb Moessbauer C |                 0.4 |              0.3 |                1
       10 |             magnetic field C |                   4 |                0 |               40

    Calling Pagmo solver with fit method PagmoDiffEvol

      population: 120
      iterations: 100

      cost = 1.017731e-02

    Calling ceres solver with fit method LevMar

    Ceres Solver Report: Iterations: 16, Initial cost: 1.017731e-02, Final cost: 1.179941e-08, Termination: CONVERGENCE

    Fit performed with 11 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                   ES scaling |                   1 |              0.9528 |                0 |            95.28
        1 |            Lamb Moessbauer A |            0.795999 |                0.81 |              0.3 |                1
        2 |                       isomer |            0.999995 |                   2 |                0 |                3
        3 |             magnetic field A |                  33 |                  27 |                0 |               40
        4 |                   quadrupole |            0.799977 |                 1.2 |                0 |                2
        5 |                   ES scaling |            0.999999 |            0.952803 |                0 |          95.2803
        6 |            Lamb Moessbauer B |            0.729697 |                0.63 |              0.3 |                1
        7 |             magnetic field B |              25.002 |                  23 |                0 |               40
        8 |                   ES scaling |                   1 |            0.952806 |                0 |          95.2806
        9 |            Lamb Moessbauer C |            0.600003 |                 0.4 |              0.3 |                1
       10 |             magnetic field C |                  12 |                   4 |                0 |               40

      total cost = 1.179941e-08

    cost for each FitMeasurement is:

      no. |                           id |                cost |                   %
        0 |                              |        3.439962e-11 |               0.292
        1 |                              |           1.175e-08 |              99.579
        2 |                              |           1.527e-11 |               0.129


In the last section the fit module prints the cost of the individual measurements.
And plot the result

.. code-block::

    plt.plot(velocities_A, intensity_A)
    plt.plot(velocities_A, moessbauer_spec_A.result)
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

    plt.plot(velocities_B, intensity_B)
    plt.plot(velocities_B, moessbauer_spec_B.result)
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

    plt.plot(velocities_C, intensity_C)
    plt.plot(velocities_C, moessbauer_spec_C.result)
    plt.xlabel('velocity (mm/s)')
    plt.ylabel('intensity')
    plt.show()

.. image:: spec_A_after_opt.png

.. image:: spec_B_after_opt.png

.. image:: spec_C_after_opt.png

The fit nicely fitted all three spectra together.
Note that the isomer shift and the quadrupole splitting are only fit once.
They are the same in all three experiments.


Using *scipy* for fitting
*************************

*Nexus* covers all typical fitting scenarios to evaluate experimental data from Moessbauer science.
In case you want to fit a property that is not covered you can always use *scipy* and its fitting algorithms.
However, you are limited to the methods provided there.

Here is a basic example how to use *scipy* with *Nexus*.
First, you set up your experiment and the measurements as in any other simulation.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    from scipy.optimize import minimize
    from scipy.optimize import leastsq

    material_Fe = nx.Material.Template(nx.lib.material.Fe_enriched)

    lay_Fe = nx.Layer(id = "Fe layer",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 10,
                      roughness = 0.1)

    lay_Pt = nx.Layer(id = "Pt layer",
                      material = nx.Material.Template(nx.lib.material.Pt),
                      thickness = 15,
                      roughness = 0.1)

    sub_Si = nx.Layer(id = "Si substrate",
                      material = nx.Material.Template(nx.lib.material.Si),
                      thickness = nx.inf,
                      roughness = 0.1)

    sample = nx.Sample(id = "simple layers",
                       layers = [lay_Fe, lay_Pt, sub_Si],
                        geometry = "r",
                        length = 10,
                        roughness = "a")

    beam  = nx.Beam(fwhm = 0.2)

    exp = nx.Experiment(beam = beam,
                        objects = sample,
                        id = "my exp")

    angles = np.arange(0.001, 3, 0.001)

    reflectivity = nx.Reflectivity(experiment = exp,
                                   sample = sample,
                                   energy = nx.lib.energy.CuKalpha,  # Cu K alpha line
                                   angles = angles,
                                   resolution = 0.001)

    reference_ref = reflectivity()

    # start values for initial guess
    lay_Fe.thickness = 7.3
    lay_Pt.thickness = 18.9

    # initial guess vector
    p0 = [lay_Fe.thickness.value, lay_Pt.thickness.value]

    ref = reflectivity()

    plt.semilogy(angles, reference_ref)
    plt.semilogy(angles, ref)
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

.. image:: scipy_before_opt.png

Then, you define a target function that is to be minimized and the optimization problem.

Here's an example for the ``least_squares`` module of *scipy*.

.. code-block::

    # definition of the optimization problem
    def errorfunc(params, target):
        # redefine the Var values
        lay_Fe.thickness = params[0]
        lay_Pt.thickness = params[1]
        # here the reflectivity is called with the new parameters
        return reflectivity() - target

    res = least_squares(errorfunc, 
                        p0,
                        args=([reference_ref]),
                        method = `trf`,
                        ftol=1e-15,
                        xtol=1e-15)

    print(res.x)

    plt.semilogy(angles, reference_ref)
    plt.semilogy(angles, reflectivity())
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

.. image:: scipy_after_opt.png

and one with the ``minimze`` module of *scipy*

.. code-block::

    # start values for initial guess again
    lay_Fe.thickness = 7.3
    lay_Pt.thickness = 18.9

    # initial guess vector
    p0 = [lay_Fe.thickness.value, lay_Pt.thickness.value]

    def errorfunc(params,target):
        # redefine the Var values
        lay_Fe.thickness = params[0]
        lay_Pt.thickness = params[1]
        # here the reflectivity is called with the new parameters
        # calculate the total cost (np.sum) for the minimize module
        return np.sum(np.square(reflectivity() - target))

    res = minimize(errorfunc,
                   p0,
                   args = (reference_ref),
                   method = 'Nelder-Mead',
                   tol = 1e-6)

    print(res.x)

    plt.semilogy(angles, reference_ref)
    plt.semilogy(angles, reflectivity())
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

.. image:: scipy_after_opt_2.png

Note that *Nexus'* referencing of the variables in different objects will still work with ``scipy``.
