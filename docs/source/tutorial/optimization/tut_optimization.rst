.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


Optimizer
---------

The :class:`Optimizer` module can be used to design the experimental parameters to certain target values.
The options controlling the optimization process are the same as for the :class:`Fit` module and are controlled by the :class:`OptimizerOptions` class, see :ref:`api-optimizer-options`.
In order to perform an optimization you must define your optimizer class that is derived from the *Nexus* :class:`Optimizer` class and holds the actual implementation of the residual to be minimized or maximized.

First, the experiment and the method are defined as in any simulation or fit.
All parameters to be optimized have to be defined as :class:`Var` objects with their :attr:`fit` set to ``True``.

.. note:: When using an :class:`Optimizer` the :attr:`scaling` of a measurement is not fit.

.. warning:: In case the optimizer is not working and python crashes it's most likely that you have an error in your implementation of the ``Residual()`` function.
             As this function is called from the C++ implementation you do not get the typical *Python* error message.
             Look out for parts of your code that can give unexpected output and use ``print()`` in the Residual function to see if your implementation works.


A simple example
****************

Let's implement the trivial example mentioned at the beginning.
The resonant intensity transmitted through an iron foil should be minimized.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    # ------------------------- Fe layer --------------------------
    # the thickness is initialized to be fitted
    # set with boundaries between 0 and 10.000
    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = nx.Var(2000, 0, 1e5, True))

    site = nx.Hyperfine(magnetic_field = 33)

    lay_Fe.material.hyperfine_sites = [site]

    sample = nx.Sample(id = "simple layer",
                       layers = [lay_Fe],
                       geometry = "f")

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = sample,
                        isotope = nx.lib.moessbauer.Fe57,
                        id = "my exp")

    detuning = np.arange(-200, 200.1, 0.2)

    energy_spec = nx.EnergySpectrum(exp,
                                    detuning,
                                    electronic = True)

    intensity = energy_spec()
    
    plt.plot(detuning, intensity)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. transmission')
    plt.show()

    print(min(intensity))

.. image:: min_intensity.png


The print shows ``0.45013002160328575`` the minimum of the resonant intensity.
We want to optimize the thickness within the given boundaries such that this value is minimized.
We already know what will happen.
The thickness will increase because the intensity then further drops.
But let the optimizer do the job for us.
We need a class implementation with the implementation of the residual to be minimized.

.. code-block::

    # define your own Optimizer derived from nx.Optimizer
    class NexusOptimizer(nx.Optimizer):
        # this is just the standard initialization of the class
        # keep it like this
        def __init__(self, measurements, id):
            super().__init__(measurements, id)

        # the implementation of the residual, the value to be minimized
        def Residual(self):
            # here we need to calculate the energy spectrum and get its minimum
            intensity = energy_spec()
            # the residual is 'min(intensity) - 0' because we target the intensity minimum to be zero (minimized)
            residual = min(intensity)
            return residual

    # initialize the optimizer with the method(s) to be used in the optimizer implementation
    optimizer = NexusOptimizer(measurements = [energy_spec],
                               id = "opt id")

    # let's see what the residual returns
    print(optimizer.Residual())  # 0.45013002160328575

    # run the optimization
    optimizer.Evaluate()  # or simply call optimizer()

You get similar output as for the :class:`Fit` module.

.. code-block::

    User residual start value: 0.450130

    Starting optimizer with 1 provided measurement dependencies and 1 fit parameter(s).

      no. |                           id |       initial value |              min |              max
        0 |                              |                2000 |                0 |           100000

    Calling ceres solver with optimizer method LevMar

    Ceres Solver Report: Iterations: 16, Initial cost: 1.013085e-01, Final cost: 1.860658e-06, Termination: CONVERGENCE

    Optimizer finished with 1 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                              |              100000 |                2000 |                0 |           100000

    Optimized user residual from 0.45013 to 0.00192907

As expected the thickness of the film is optimized to the maximum boundary of 10000.
The optimizer also outputs the optimized residual value ``0.00192907``.

Let's plot the 'optimized' spectrum.

.. code-block::

    plt.plot(detuning, energy_spec.result)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. transmission')
    plt.show()

.. image:: min_intensity_optimized.png


Optimizing for critical coupling of a cavity
********************************************

In this example we will cover a typical problem encountered in cavity structures. 
The topmost layer must be optimized in its thickness to ensure critical coupling into the cavity.
Here, we will optimize critical coupling to the first waveguide mode.

We setup the cavity without knowing the correct thickness of the top layer.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.signal import find_peaks

    # ------------------------- layers --------------------------

    # the initial guess for the top layer is 3 nm here
    # set it to a Var object with reasonable bounds
    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = nx.Var(3, min = 0, max = 10, fit = True, id = "Pt top thickness"))

    lay_B4C = nx.Layer(id = "B4C",
                       material = nx.Material.Template(nx.lib.material.B4C),
                       thickness = 15)

    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 2)
    
    lay_Pt_bottom = nx.Layer(id = "Pt bottom",
                             material = nx.Material.Template(nx.lib.material.Pt),
                             thickness = 15)

    lay_substrate = nx.Layer(id = "substrate",
                             material = nx.Material.Template(nx.lib.material.Si),
                             thickness = nx.inf)

    sample = nx.Sample(id = "cavity",
                       layers = [lay_Pt_top,
                                 lay_B4C,
                                 lay_Fe,
                                 lay_B4C,
                                 lay_Pt_bottom,
                                 lay_substrate],
                       geometry = "r")

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = sample,
                        id = "my exp")

    # initialize a reflectivity object used for the optimization
    angles = np.linspace(0.01, 0.4, 1001)

    reflectivity = nx.Reflectivity(experiment = exp,
                                   sample = sample,
                                   energy = nx.lib.energy.Fe57,
                                   angles = angles)

    intensity = reflectivity()

    plt.semilogy(angles, intensity)
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

.. image:: ref_before_opt.png

Here you see the reflectivity before optimization.
The first minimum is not in the critical coupling condition as observed from the rather small decrease of the intensity.
Let's optimize.

.. code-block::

    # setup the optimizer
    class NexusOptimizer(nx.Optimizer):
        def __init__(self, measurements, id):
            super().__init__(measurements, id)

        # the definition of the residual calculation
        def Residual(self):
            # calculate the reflectivity
            intensity = reflectivity()
            
            # get the index of the first minimum
            if (len(find_peaks(-intensity)[0]) < 1):
                return 1e30  # return large punish value
            min_index = find_peaks(-intensity)[0][0]

            # optimize for the intensity at the first minimum position
            # minimize so 'intensity - 0'
            residual = intensity[min_index]
            return residual

    # pass the reflectivity object to the optimizer
    opt = NexusOptimizer(measurements = [reflectivity],
                         id = "opt id")

    # let's just use a local gradient-free algorithm here
    opt.options.method = "Subplex"
    
    # run the optimization
    opt.Evaluate()  # or simply call opt()

    plt.semilogy(angles, reflectivity.result)
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

The output is

.. code-block::

    User residual start value: 0.174367

    Starting optimizer with 1 provided measurement dependencies and 1 fit parameter(s).

      no. |                           id |       initial value |              min |              max
        0 |             Pt top thickness |                   3 |                0 |               10

    Calling NLopt solver with optimizer method Subplex

    Termination: parameter tolerance reached.
      cost = 1.310289e-02
      iterations: 50

    Optimizer finished with 1 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |             Pt top thickness |             2.00665 |                   3 |                0 |               10

    Optimized user residual from 0.174367 to 0.0262058

.. image:: ref_after_opt.png

The top layer thickness is optimized to ``2.00665`` nm.


Optimizing multiple parameters in parallel
******************************************

In order to optimize for several parameters in parallel we try to obtain a coupling such that the sum of the 1st and 3rd minimum of the cavity is minimal.

We take the same cavity as before

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.signal import argrelextrema

    # ------------------------- layers --------------------------

    # the initial guess for the top layer is 3 nm here
    # set it to a Var object with reasonable bounds
    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = nx.Var(3, min = 0, max = 10, fit = True, id = "Pt top thickness"))

    lay_B4C = nx.Layer(id = "B4C",
                       material = nx.Material.Template(nx.lib.material.B4C),
                       thickness = 15)

    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 2)
    
    lay_Pt_bottom = nx.Layer(id = "Pt bottom",
                             material = nx.Material.Template(nx.lib.material.Pt),
                             thickness = 15)

    lay_substrate = nx.Layer(id = "substrate",
                             material = nx.Material.Template(nx.lib.material.Si),
                             thickness = nx.inf)

    sample = nx.Sample(id = "cavity",
                       layers = [lay_Pt_top,
                                 lay_B4C,
                                 lay_Fe,
                                 lay_B4C,
                                 lay_Pt_bottom,
                                 lay_substrate],
                       geometry = "r")

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = sample,
                        id = "my exp")

    # initialize a reflectivity object used for the optimization
    angles = np.linspace(0.01, 0.4, 1001)

    reflectivity = nx.Reflectivity(experiment = exp,
                                   sample = sample,
                                   energy = nx.lib.energy.Fe57,
                                   angles = angles)

Now the residual has to be changed to optimize both minima

.. code-block::

    # setup the optimizer
    class NexusOptimizer(nx.Optimizer):
        def __init__(self, measurements, id):
            super().__init__(measurements, id)

        # the defienition of the residual calculation
        def Residual(self):
            # calculate the reflectivity
            intensity = reflectivity()

            # get the index of the 1st minimum
            min_index_1st = np.squeeze(argrelextrema(intensity, np.less))[0]

            # get the index of the 3rd minimum
            min_index_3rd = np.squeeze(argrelextrema(intensity, np.less))[2]

            # optimize for the intensity at both positions
            # we cannot pass several residuals to the optimizer
            # so we have to take care how to define the residual here
            # actually we have two residuals r_1 = min_1st - 0
            # and r_3  = min_3rd - 0
            # when several residuals are passed a single sum square the residuals
            # r = (min_1st - 0)^2 + (min_3rd - 0)^2
            # the squares are important if negative differences can occur
            # this passed residual is again squared by the optimizer
            # so one could pass r = \sqrt( (min_1st - 0)^2 + (min_3rd - 0)^2 )
            # but it will make no difference
            residual = intensity[min_index_1st]**2 + intensity[min_index_3rd]**2
            return residual

    # pass the reflectivity object to the optimizer
    opt = NexusOptimizer(measurements = [reflectivity],
                         id = "opt id")

    opt.options.method = "LevMar"
    
    # run the optimization
    opt.Evaluate()  # or simply call opt()

    plt.semilogy(angles, reflectivity.result)
    plt.xlabel('angle (deg)')
    plt.ylabel('reflectivity')
    plt.show()

.. image:: ref_multimode_after_opt.png

.. code-block::

    User residual start value: 0.030430

    Starting optimizer with 1 provided measurement dependencies and 1 fit parameter(s).

      no. |                           id |       initial value |              min |              max
        0 |             Pt top thickness |                   3 |                0 |               10

    Calling ceres solver with optimizer method LevMar

    Ceres Solver Report: Iterations: 12, Initial cost: 4.629876e-04, Final cost: 1.473811e-05, Termination: CONVERGENCE

    Optimizer finished with 1 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |             Pt top thickness |             2.36109 |                   3 |                0 |               10

    Optimized user residual from 0.0304298 to 0.0054292

Now, the optimized thickness is ``2.36109`` nm.

.. note:: In multi-parameter optimization square each individual residual, then take the sum as the total residual.

You might also want to add a weight to each residual. In this case the code would change to 

 .. code-block::

     residual = 0.4 * intensity[min_index_1st]**2 + 0.6 * intensity[min_index_3rd]**2

in case of a 40% to 60% weighting.


Optimization of a cavity spectrum
*********************************

In this example a whole cavity spectrum should be optimized to a certain shape.
This is a more advanced optimization problem but will serve as an example on how to optimize more than just a a few parameters.

Here we want to obtain an EIT (electromagnetically induced transparency) spectrum of a cavity with a certain shape.
The spectral shape we want to create is :download:`reference_spectrum <reference_spectrum.txt>`. The first column is the detuning and the second one the intensity.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.signal import argrelextrema

    data = np.loadtxt('reference_spectrum.txt')

    detuning_reference = data[:,0]
    intensity_reference = data[:,1]

    plt.plot(detuning_reference, intensity_reference)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. reflectivity')
    plt.show()

.. image:: cavity_spectrum_reference.png

We set up an EIT cavity with two resonant layers. Let's say we only have a rough guess of the thicknesses needed in the specific layers.
So we set up a cavity and look at the field distribution inside the cavity and of course at the spectrum

.. code-block::

    # ------------------------- layers --------------------------
    lay_Fe_top = nx.Layer(id = "Fe top",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 1)

    lay_Fe_bottom = nx.Layer(id = "Fe bottom",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 1)

    site = nx.Hyperfine(id = "no interactions")

    lay_Fe_top.material.hyperfine_sites = [site]

    lay_Fe_bottom.material.hyperfine_sites = [site]


    lay_B4C_top = nx.Layer(id = "B4C top",
                       material = nx.Material.Template(nx.lib.material.B4C),
                       thickness = nx.Var(value = 4, min = 0, max = 10, fit = True, id = "B4C top"))

    lay_B4C_mid = nx.Layer(id = "B4C mid",
                       material = nx.Material.Template(nx.lib.material.B4C),
                       thickness = nx.Var(value = 4, min = 0, max = 10, fit = True, id = "B4C mid"))

    lay_B4C_bottom = nx.Layer(id = "B4C bottom",
                       material = nx.Material.Template(nx.lib.material.B4C),
                       thickness = nx.Var(value = 16, min = 0, max = 20, fit = True, id = "B4C top"))

    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = nx.Var(3.2, min = 0, max = 10, fit = True, id = "Pt top thickness"))

    lay_Pt_bottom = nx.Layer(id = "Pt bottom",
                             material = nx.Material.Template(nx.lib.material.Pt),
                             thickness = 15)


    lay_substrate = nx.Layer(id = "substrate",
                             material = nx.Material.Template(nx.lib.material.Si),
                             thickness = nx.inf)

    cavity = nx.Sample(id = "cavity",
                       geometry = "r",
                       layers = [lay_Pt_top,
                                 lay_B4C_top,
                                 lay_Fe_top,
                                 lay_B4C_mid,
                                 lay_Fe_bottom,
                                 lay_B4C_bottom,
                                 lay_Pt_bottom,
                                 lay_substrate],
                       angle = 0.1505)

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = cavity,
                        isotope = nx.lib.moessbauer.Fe57,
                        id = "my exp")

    # initialize a reflectivity object used for the optimization
    angles = np.linspace(0.01, 0.4, 1001)

    reflectivity = nx.Reflectivity(experiment = exp,
                                   sample = cavity,
                                   energy = nx.lib.energy.Fe57,
                                   angles = angles)

    intensity = reflectivity()

    # find third minimum
    min_index = np.squeeze(argrelextrema(intensity, np.less))[2]

    # set cavity angle to angle of third minimum
    cavity.angle = angles[min_index]

    # calcualte field intensity inside the sample
    field_int = nx.FieldIntensity(sample = cavity,
                                  energy = nx.moessbauer.Fe57.energy,
                                  points = 1001)

    depth, int = field_int()

    plt.plot(depth, int)

    # add lines and labels
    axes = plt.gca()
    y_min, y_max = axes.get_ylim()
    plt.vlines(cavity.Interfaces(), y_min, y_max, colors='g', linestyles='dashed')
    for id, location in zip(cavity.Ids(), np.array(cavity.Interfaces()) + 0.5):
        plt.text(location, y_max, id, fontsize = 10)
        y_max = y_max - 0.5

    plt.xlabel('depth (nm)')
    plt.ylabel('relative intensity')
    plt.show()

.. image:: cavity_field_before_opt.png

The iron layers are not at the node and antinode positions of the field inside the cavity.
We do not expect a pronounced EIT effect in the spectrum.
Let's check.

.. code-block::

    energy_spectrum = nx.EnergySpectrum(experiment = exp,
                                        detuning = detuning_reference,
                                        electronic = True,
                                        id = "my energy spectrum")

    intensity = energy_spectrum.Calculate()

    plt.plot(detuning, intensity)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. reflectivity')
    plt.show()

.. image:: cavity_spectrum_before_opt.png

Indeed, the target spectrum looks much different. So we try to optimize.

Remember that we might need to care about certain special conditions that could crash our residual implementation.
In the following, a crash can happen if there is no third minimum in the ``argrelextrema`` function.

.. code-block::

    # setup the optimizer
    class NexusOptimizer(nx.Optimizer):
        def __init__(self, measurements, id):
            super().__init__(measurements, id)

        # the definition of the residual calculation
        def Residual(self):
            # calculate the reflectivity to find the third minimum
            ref = reflectivity()

            # if there is no third minimum due to the combination of thicknesses
            if (len(argrelextrema(ref, np.less)[0]) < 3):
                 return 1e30  # return large punish value

            # get the index of the third minimum
            min_index = np.squeeze(argrelextrema(ref, np.less))[2]

            # set cavity angle to third minimum
            cavity.angle = angles[min_index]

            # calculate energy_spectrum
            intensity = energy_spectrum()

            # calculate residual
            # the squared sum of all differences between target and calculation
            differences = intensity - intensity_reference
            residual = np.sum(np.square(differences))
            return residual

    # pass all needed measurement objects to the optimizer
    # this time we need to pass two measurements, both are needed to be optimized
    opt = NexusOptimizer(measurements = [reflectivity, energy_spectrum],
                         id = "opt id")

    opt.options.method = "LevMar"

    # run the optimization
    opt()

    plt.plot(detuning_reference, intensity_reference)
    plt.plot(detuning_reference, energy_spectrum.result)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. reflectivity')
    plt.show()

.. image:: cavity_spectrum_after_LevMar.png

The Levenberg-Marquardt algorithm didn't find a good solution, so let's try another local algorithm.

.. code-block::

    # let's try a local gradient-free algorithm
    opt.options.method = "Subplex"

    opt.SetInitialVars()

    opt()

    plt.plot(detuning_reference, intensity_reference)
    plt.plot(detuning_reference, energy_spectrum.result)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. reflectivity')
    plt.show()

.. image:: cavity_spectrum_after_Subplex.png

The Subplex algorithm did a much better job but its still not optimal. Go for a global algorithm.

.. code-block::

    # let's try a global algorithm
    opt.options.method = "PagmoDiffEvol"

    opt.SetInitialVars()

    opt()

    plt.plot(detuning_reference, intensity_reference)
    plt.plot(detuning_reference, energy_spectrum.result)
    plt.xlabel('detuning (Gamma)')
    plt.ylabel('rel. reflectivity')
    plt.show()

.. image:: cavity_spectrum_after_PagmoDiffEvol.png

Finally, a good solution is found with the parameters

.. code-block::

    User residual start value: 10.330591

    Starting optimizer with 2 provided measurement dependencies and 4 fit parameter(s).

      no. |                           id |       initial value |              min |              max
        0 |             Pt top thickness |                 3.2 |                0 |               10
        1 |                      B4C top |                   4 |                0 |               10
        2 |                      B4C mid |                   4 |                0 |               10
        3 |                      B4C top |                  16 |                0 |               20

    Calling Pagmo solver with optimizer method PagmoDiffEvol

      population: 50
      iterations: 100

      cost = 9.499292e-04

    Calling ceres solver with optimizer method LevMar

    Ceres Solver Report: Iterations: 7, Initial cost: 1.804731e-06, Final cost: 1.804731e-06, Termination: CONVERGENCE

    Optimizer finished with 4 fit parameter(s):

      no. |                           id |           fit value |       initial value |              min |              max
        0 |             Pt top thickness |             3.22195 |                 3.2 |                0 |               10
        1 |                      B4C top |             8.53259 |                   4 |                0 |               10
        2 |                      B4C mid |             5.50449 |                   4 |                0 |               10
        3 |                      B4C top |             15.0454 |                  16 |                0 |               20

    Optimized user residual from 10.3306 to 0.00189986

The fit values are exactly the ones used to create the reference spectrum. At the end we also want to see the field intensity inside the sample.

.. code-block::

    # calcualte field intensity inside the sample
    field_int = nx.FieldIntensity(sample = cavity,
                                  energy = nx.moessbauer.Fe57.energy,
                                  points = 1001)

    depth, int = field_int()

    plt.plot(depth, int)

    # add lines and labels
    axes = plt.gca()
    y_min, y_max = axes.get_ylim()
    plt.vlines(cavity.Interfaces(), y_min, y_max, colors='g', linestyles='dashed')
    for id, location in zip(cavity.Ids(), np.array(cavity.Interfaces()) + 0.5):
        plt.text(location, y_max, id, fontsize = 10)
        y_max = y_max - 0.5

    plt.xlabel('depth (nm)')
    plt.ylabel('relative intensity')
    plt.show()

.. image:: cavity_field_after_opt.png

The two resonant iron layers are now in the node and antinode of the field.
