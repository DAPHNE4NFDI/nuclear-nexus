.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.



Parameter constraints
---------------------

.. versionadded:: 1.1.0

*Nexus* offers the feature to apply constraints on fit parameters.
The fit function

.. math:: \mathrm{min} f(x,y)

is then subject to an arbitrary number of constraints :math:`g_i(x,y)` functions.
These constraints can either be of equality type :math:`g_i(x,y) = 0` or of inequality type :math:`g_i(x,y) \geq 0`.
Equality and inequality constraints can be combined

.. math::

   g_1(x,y) = 0,

   g_2(x,y) \geq 0,

   g_3(x,y) \leq 0,

   g_i(x,y) \, ...

in order to specify the optimization problem.
You should take care that all constraints are satisfiable simultaneously, which *Nexus* cannot check for you.


Equality constraints
********************

An equality constraint

.. math:: g(x,y) = 0.

is applied via a function that is passed to the :attr:`equality` attribute of a :class:`Var`.

The :attr:`fit` attribute of the :class:`Var` object must be ``False`` in order to apply an equality constraint.
In order to apply a constraint you have to create a function that specifies the constraint function and apply it to the :class:`Var` object.
The constraint function is given to the :attr:`equality` class attribute.
The functional dependence can be any (non-)linear function and can depend on several fit parameters.
Further, the constraint must be applied during the :class:`Var` initialization and it must return a float value.

.. code-block::

    import nexus as nx

    a = nx.Var(1, min=0, max=1, fit=True, id="a")

    # the sum of a and b is restricted to 1
    # The constraint g() = 0 is
    # a + b - 1 = 0
    # The constraint is applied to b
    # b = 1 - a

    # via function definition
    def sum_constraint():
        return 1 - a.value
    
    b = nx.Var(1, min=0, max=1, fit=False, equality=sum_constraint, id="b")
    
    # or via lambda function
    b = nx.Var(1, min=0, max=1, fit=False, equality=lambda: 1 - a.value, id="b")

These constraints can be used to couple fit parameters.
They also offer the possibility to use external fit parameters.
Those are :class:`Var` objects that are not assigned to other *Nexus* objects.
It offers the possibility to fit theoretical models directly to the measured data sets.


Equality constraints on Vars
############################

This example shows the basic usage of equality constraints.
Here, two hyperfine sites will be fit to this :download:`spectrum <spectrum_equality.txt>` and the magnetic fields of the two sites are coupled.

.. code-block::

    import nexus as nx
    import numpy as np

    # load the example spectrum
    detuning, intensity = nx.data.Load("spectrum_equality.txt",
                                        x_index = 0,
                                        intensity_index = 1)

    # setup fit parameters with initial guess
    # and create dependent Vars
    isomer = nx.Var(0.8, min = 0, max = 2, fit = True, id="isomer")

    mag_1 = nx.Var(value = 32, min = 25, max = 35, fit = True, id = "mag field 1")

    # we assume that the magnetic fields of the two sites have a difference of 23 T
    # mag2 = mag1 - 23
    def equality_mag2():
        return mag_1.value - 23

    # we apply a value of 14 that is used for all regular calculations.
    # for fitting, the equality constraint function equality_mag2 is applied and the value is ignored. 
    mag_2 = nx.Var(value = 14, equality = equality_mag2, id = "mag field 2")

    # assign to sites
    site1 = nx.Hyperfine(
        id = "site 1",
        weight = 0.5,
        isomer = isomer,
        magnetic_field = mag_1,
        isotropic = False)

    site2 = nx.Hyperfine(
        id = "site 2",
        weight = 0.5,
        isomer = isomer,
        magnetic_field = mag_2,
        isotropic = False)


    #setup experiment
    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 3000)

    lay_Fe.material.hyperfine_sites = [site1, site2]

    sample = nx.Sample(id = "sample",
                       layers = [lay_Fe],
                       geometry = "f")

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.moessbauer.Fe57,
                        id = "my exp")

    energy_spec = nx.EnergySpectrum(exp,
                                    detuning,
                                    intensity_data = intensity,
                                    id = "energy spec")

    energy_spec()
    energy_spec.Plot()

and obtain the initial guess of your fit.

.. image:: equality_fit_initial.png

Note that for calculations (not fitting) always the provided value for the dependent variable is used.
Equality constraints are only used for fitting.

.. code-block::

    fit = nx.Fit([energy_spec], id = "fit")
    fit()

    energy_spec.Plot()

.. image:: equality_fit.png

Note that you obtain the following lines

.. code-block::

    Fit performed with 4 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                   ES scaling |             99814.5 |             88182.9 |                0 |      8.81829e+06
        1 |                    ES backgr |             1151.94 |             7777.25 |                0 |           777725
        2 |                       isomer |             1.00012 |                 0.8 |                0 |                2
        3 |                  mag field 1 |             33.0028 |                  32 |               25 |               35

    and 1 equality constraint(s) on parameter(s).

      no. |                           id |               value
        0 |                  mag field 2 |             10.0028

where you can observe that the ``mag field 2`` is fit with the provided difference of 23 T to ``mag field 1``.


Equality constraint with parameters
###################################


Equality constraint functions normally have simple mathematical relations between :class:`Var` objects, like

.. code-block::

    a = nx.Var(5, min = 0, max = 10, fit = True, id = "a")
    b = nx.Var(3, min = 0, max = 10, fit = True, id = "b")

    def equality_constraint():
        return a.value + 2 * b.value

    c = nx.Var(0, equality = equality_constraint, id = "c")

However, equality constraints can also depend on variables like any other function.

.. code-block::

    a = nx.Var(5, min = 0, max = 10, fit = True, id = "a")
    b = nx.Var(3, min = 0, max = 10, fit = True, id = "b")

    def equality_constraint(scaling):
        return a.value + scaling * b.value

Now, the problem is that the parameter cannot easily passed to the dependent Var ``c``.
The :attr:`equality` attribute only takes functions as input.
Writing ``equality = equality_constraint(2)`` is not valid as it returns a `float` and not a `function`.
The correct way is to use a *Python lambda* function.
It will create a function reference which calls your equality constraint function with the passed arguments.

.. code-block::

    c = nx.Var(0, equality = lambda: equality_constraint(2), id = "c")

    # or

    x = 2
    c = nx.Var(0, equality = lambda: equality_constraint(x), id = "c")

Equality functions can have as many parameters as needed.


Inequality constraints
**********************

Inequality constraints

.. math:: g(x,y) \geq 0 \; \; \mathrm{or} \; \; g(x,y) \leq 0.

are applied to the :class:`Fit` or :class:`Optimizer` module via the :attr:`inequalites` attribute.
The user has to define inequality constraints as functions and pass them as a list.
For example, two inequality constraints are implemented like this

.. code-block::

    import nexus as nx

    a = nx.Var(1, min=0, max=1, fit=True, id="a")
    b = nx.Var(1, min=0, max=1, fit=True, id="b")
    c = nx.Var(2, min=0, max=4, fit=True, id="c")

    #define two inequality constraints

    # via function definition
    def ineq1():
        return a.value > b.value

    def ineq2():
        return b.value**2 + c.value**2 > 1

    fit = nx.Fit(measurements= [measurements],
                 inequalities = [ineq1, ineq2])
    
    # or via lambda functions
    fit = nx.Fit(measurements= [measurements],
                 inequalities = [lambda: a.vlaue > b.value,
                                 lambda: b.value**2 + c.value**2 > 1])


As an example we will use the same fit with two hyperfine sites as in the first examples of the equality constraints.
This time we want to ensure that the magnetic field of site 2 is larger than the one of site 1.
For local algorithms you have to ensure that the starting conditions is satisfied otherwise the fit will not run.
For global algorithms it is not necessary.
However, the ranges must be large enough for the inequality constraints to be satisfiable.

.. code-block::

    import nexus as nx

    # load example spectrum
    detuning, intensity = nx.data.Load("spectrum_equality.txt",
                                       x_index = 0,
                                       intensity_index = 1)

    #setup experiment
    isomer = nx.Var(0.8, min = 0, max = 2, fit = True, id="isomer")

    site1 = nx.Hyperfine(
        id = "site 1",
        weight = 0.5,
        isomer = isomer,
        magnetic_field = nx.Var(value = 32, min = 0, max = 35, fit = True, id = "mag field 1"),
        isotropic = False)

    site2 = nx.Hyperfine(
        id = "site 2",
        weight = 0.5,
        isomer = isomer,
        magnetic_field = nx.Var(value = 14, min = 0, max = 35, fit = True, id = "mag field 2"),
        isotropic = False)

    mat_Fe = nx.Material.Template(nx.lib.material.Fe_enriched)

    mat_Fe.hyperfine_sites = [site1, site2]

    #create sample
    lay_Fe = nx.Layer(id = "Fe",
                      material = mat_Fe,
                      thickness = 2800)

    sample = nx.Sample(id = "sample",
                       layers = [lay_Fe],
                       geometry = "f")

    beam  = nx.Beam()

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.moessbauer.Fe57,
                        id = "my exp")

    energy_spec = nx.EnergySpectrum(exp,
                                    detuning,
                                    intensity_data = intensity,
                                    id = "energy spec")
    # fit with inequality
    fit = nx.Fit([energy_spec],
                 # magnetic field of site1 must be smaller than that of site 2
                 inequalities = [lambda: site1.magnetic_field.value < site2.magnetic_field.value])

    fit.options.method = "PagmoDiffEvol"

    fit()

The fit takes the inequality constraint into account and print

.. code-block::

    Fit performed with 5 fit parameter(s):

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                   ES scaling |             99939.1 |             88182.9 |                0 |      8.81829e+06
        1 |                    ES backgr |                   0 |             7777.25 |                0 |           777725
        2 |                       isomer |             1.00017 |                 0.8 |                0 |                2
        3 |                  mag field 1 |             9.99519 |                  32 |                0 |               35
        4 |                  mag field 2 |             33.0031 |                  14 |                0 |               35

    and 0 equality constraint(s) on parameter(s):

    and 1 inequality constraint(s).

Magnetic field of site 1 is indeed smaller than that of site 2.

External fit variables
**********************

The :attr:`equality` attribute can also be used to couple :class:`Var` objects to one or various external fit parameters.
External fit parameter are all :class:`Var` objects that are not assigned to other *Nexus* object directly, like :class:`Hyperfine`, :class:`Layer`, :class:`Distribution`, :class:`Sample` and so on, and are used in an equality constraint of a :class:`Var`.
 
This external dependence is helpful in case a functional dependence on an external parameter or model should be fit to data.
To do so, one has to create a :class:`Var` object that is your free parameter and create a couple of dependent :class:`Var` objects.
The free parameter has to be registered via the :attr:`external_fit_variables` attribute of the :class:`Fit` or :class:`Optimizer`.

A simple example is the fit of two sites as shown before.
This time we do not put a constraint on the variable in depedence on another assigned :class:`Var` (``mag_field_1`` in the previous example) but use of an external fit variable on the magnetic field of the second site.

.. code-block::

    import nexus as nx
    import numpy as np

    # load example specturm
    detuning, intensity = nx.data.Load("spectrum_equality.txt",
                                        x_index = 0,
                                        intensity_index = 1)


    #setup fit parameters
    isomer = nx.Var(0.8, min = 0, max = 2, fit = True, id="isomer")

    mag_1 = nx.Var(value = 32, min = 25, max = 35, fit = True, id = "mag field 1")

    # create a Var used as external fit parameter
    # it is not assigned to a Nexus object directly
    external_fit_mag_2 = nx.Var(value = 12, min = 8, max = 15, fit = True, id = "ext mag field 2")

    # a simple functional dependence on the external fit parameter
    def external_func_mag2():
        return external_fit_mag_2.value

    # assign to sites
    site1 = nx.Hyperfine(
        id = "site 1",
        weight = 0.5,
        isomer = isomer,
        magnetic_field = mag_1,
        isotropic = False)

    site2 = nx.Hyperfine(
        id = "site 2",
        weight = 0.5,
        isomer = isomer,
        magnetic_field = nx.Var(value = 14, equality = external_func_mag2, id = "mag field 2"),
        isotropic = False)

    #create sample
    lay_Fe = nx.Layer(id = "Fe",
                      material = nx.Material.Template(nx.lib.material.Fe_enriched),
                      thickness = 3000)

    lay_Fe.material.hyperfine_sites = [site1, site2]

    sample = nx.Sample(id = "sample",
                       layers = [lay_Fe],
                       geometry = "f")

    beam  = nx.Beam()
    beam.LinearSigma()

    exp = nx.Experiment(beam = beam,
                        objects = [sample],
                        isotope = nx.moessbauer.Fe57,
                        id = "my exp")

    energy_spec = nx.EnergySpectrum(exp,
                                    detuning = detuning,
                                    intensity_data = intensity,
                                    id = "energy spec")

    energy_spec()
    energy_spec.Plot()

    # assign the external_fit_parameter to the Fit object.
    fit = nx.Fit([energy_spec],
                 external_fit_variables = [external_fit_mag_2])

    fit()

    energy_spec.Plot()


The plot of the fit is the same as before.
The important output of the fit is

.. code-block::

    Fit performed with 5 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                   ES scaling |             99815.5 |             88182.9 |                0 |      8.81829e+06
        1 |                    ES backgr |             1151.14 |             7777.25 |                0 |           777725
        2 |                       isomer |             1.00013 |                 0.8 |                0 |                2
        3 |                  mag field 1 |             33.0017 |                  32 |               25 |               35
        4 |              ext mag field 2 |             10.0033 |                  12 |                8 |               15

    and 1 equality constraint(s) on parameter(s).

      no. |                           id |               value
        0 |                  mag field 2 |             10.0033

You can see that the ``mag field 2`` is recognized as an equality constraint and the ``ext mag field 2`` is a fit variable.
Both have exactly the same value as expected.
Importantly the external fit parameter is the one that is fit.
This simple example is trivial but it shows how external fit parameters are used.


Fit to external models
######################


Equality constraints and external fit parameters can be used to fit data sets to models on any parameters, most typically hyperfine parameters.
Here, we show how various external parameters can be used to fit data sets directly to an external model.
For example, you have several measurements at different temperatures of an iron film and you want to directly fit a Bloch T :sup:`3/2` law to the data.
This is just an example for demonstration purposes.

In the *Nexus* language it is a multi-measurement fit from several experiments.
We have to setup an experiment for each measurement and connect it to a Bloch law function whose parameters should be fit.
The spectra used for the different temperatures are :download:`1000 K <spectrum_1000K.txt>`, :download:`800 K <spectrum_800K.txt>`, :download:`600 K <spectrum_600K.txt>`, :download:`400 K <spectrum_400K.txt>`, :download:`200 K <spectrum_200K.txt>`

First, we load all data

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    vel_1000, int_1000 = nx.data.Load("spectrum_1000K.txt", x_index = 0, intensity_index = 1)
    vel_800, int_800 = nx.data.Load("spectrum_800K.txt", x_index = 0, intensity_index = 1)
    vel_600, int_600 = nx.data.Load("spectrum_600K.txt", x_index = 0, intensity_index = 1)
    vel_400, int_400 = nx.data.Load("spectrum_400K.txt", x_index = 0, intensity_index = 1)
    vel_200, int_200 = nx.data.Load("spectrum_200K.txt", x_index = 0, intensity_index = 1)

    plt.plot(vel_1000, int_1000, label = "1000 K")
    plt.plot(vel_800, int_800 + 4e3, label = "800 K")
    plt.plot(vel_600, int_600 + 8e3, label = "600 K")
    plt.plot(vel_400, int_400 + 12e3, label = "400 K")
    plt.plot(vel_200, int_200 + 16e3, label = "200 K")
    plt.legend()
    plt.show()

.. image:: fit_Bloch_data.png

Then, we set up the external model we want to fit.

.. code-block::

    # Bloch T^3/2 function
    # M(T) = M(0)* (1-(T/Tc)^3/2)

    # create external fit parameters
    Tc = nx.Var(1040, min = 1000, max= 1100, fit = True, id = "Tc")
    M_0 = nx.Var(40, min = 30, max= 50, fit = True, id ="mag_0")
    exponent = nx.Var(1.2, min = 1, max= 2, fit = True,id = "exp")

    # Only the temperature T is known from the experiment and will be set.
    def Bloch(T):
        # return a magnetic field value in Tesla
        return M_0.value * (1 - (T/Tc.value)**(exponent.value))

To create such an amount of measurement spectra we use a function to create them.

.. code-block::

    # create the experiment in a for loop
    # we assume that the oterh hyperfine parameters do not change individually
    # we also want to fit the isomer shift as a common variable to all measurements
    isomer_shift = nx.Var(0.5, min = -1, max = 1, fit = True, id = "isomer all")

    # It is also possible to create additional Vars
    # either in the function to be fit individually

    # we can use the same beam object in each experient
    beam = nx.Beam()
    beam.Unpolarized()

    # because the hyperfine parameters change we have to create
    # new samples, experiments and MoessbauerSpectra for each temperature
    # also we have to pass the experimental temperature and velocity and intensity data
    def setup_specturm(temperature, velocity_data, intensity_data):
        site = nx.Hyperfine(weight=1,
          isomer = isomer_shift,
          # now assign the external function to the magnetic field of each experiment
          # given by the Bloch function and the set temperature
          magnetic_field = nx.Var(0, id="mag_at_"+str(temperature), equality = lambda: Bloch(temperature)),
          isotropic = True)

        mat_Fe = nx.Material.Template(nx.lib.material.Fe)

        mat_Fe.hyperfine_sites = [site]

        layer_Fe = nx.Layer(id = "Fe",
                            material = mat_Fe,
                            thickness = 3000)

        sample = nx.Sample(layers = [layer_Fe])

        exp = nx.Experiment(beam = beam,
                            objects = [sample],
                            isotope = nx.lib.moessbauer.Fe57)

        spectrum = nx.MoessbauerSpectrum(experiment = exp,
                                         velocity = velocity_data,
                                         intensity_data = intensity_data)

        return spectrum

and create all needed measurements

.. code-block::

    # create list to loop over
    temps = [200, 400, 600, 800, 1000]
    vels = [vel_200, vel_400, vel_600, vel_800, vel_1000]
    dats = [int_200, int_400, int_600, int_800, int_1000]
    measurements = []

    # create all spectra and append to list
    for t, v, d in zip(temps, vels, dats):
        measurements.append(setup_specturm(t,v,d))

At last we set up the fit model with the external fit variables

.. code-block::

    fit = nx.Fit(measurements = measurements,
                 id = "Bloch fit",
                 external_fit_variables = [Tc, M_0, exponent])

    fit()

to obtain the fit results. Part of the output is 

.. code-block::

    Fit performed with 14 fit parameter(s).

      no. |                           id |           fit value |       initial value |              min |              max
        0 |                   ES scaling |              100190 |               87188 |                0 |       8.7188e+06
        1 |                    ES backgr |             333.481 |             8601.76 |                0 |           860176
        2 |                   isomer all |        -5.12588e-05 |                 0.5 |               -1 |                1
        3 |                   ES scaling |              100621 |               87193 |                0 |       8.7193e+06
        4 |                    ES backgr |                   0 |             8603.46 |                0 |           860346
        5 |                   ES scaling |               99477 |               87191 |                0 |       8.7191e+06
        6 |                    ES backgr |             956.819 |             8603.12 |                0 |           860312
        7 |                   ES scaling |              100333 |               87188 |                0 |       8.7188e+06
        8 |                    ES backgr |             200.076 |             8600.74 |                0 |           860074
        9 |                   ES scaling |              100459 |               87195 |                0 |       8.7195e+06
       10 |                    ES backgr |             111.197 |             8604.11 |                0 |           860411
       11 |                           Tc |             1043.07 |                1040 |             1000 |             1100
       12 |                          M_0 |              39.126 |                  40 |               30 |               50
       13 |                     exponent |             1.49986 |                 1.2 |                1 |                2

    and 5 equality constraint(s) on parameter(s).

      no. |                           id |               value
        0 |                   mag_at_200 |             35.8402
        1 |                   mag_at_400 |             29.8333
        2 |                   mag_at_600 |             22.0552
        3 |                   mag_at_800 |             12.8448
        4 |                  mag_at_1000 |              2.3981

The magnetic fields at different temperatures are calculated from the Bloch function and are dependent parameters via the equality constraints.
The external fit parameters of the Bloch functions ``Tc``, ``M_0``, ``exponent`` are fit.

To plot the data use

.. code-block::

    offset = 0
    for t, v, d, m in zip(temps, vels, dats, measurements):
        int = m.Calculate()
        offset += max(int) * 0.05
        plt.plot(v, d + offset, label = str(t)+" K", marker='o', markersize = 2, linewidth=0)
        plt.plot(v, int + offset, color='grey')

    plt.legend()
    plt.show()

.. image:: fit_Bloch_result.png
