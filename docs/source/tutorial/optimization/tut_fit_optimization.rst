.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-optimization-overview:


Fitting and Optimization
========================

*Nexus* offers the non-linear optimization modules :class:`Fit` and :class:`Optimizer`.
Fitting theoretical models to experimental data is only one problem encountered in non-linear optimization.
In general, the goal is to find a local minimum or maximum of a problem.
In *Nexus*, this minimization procedure can also be used to design experiments to specific target values.

The :class:`Fit` module is intended to fit single or multiple data sets.
The fitting routines are not limited to the same :class:`Experiment` but arbitrary combinations of experiments, samples, measurements and so on can be fit in a consistent way.
The :class:`Fit` method can only be applied to :class:`FitMeasurement` types, like a reflectivity or a time spectrum for example.

The :class:`Optimizer` module is used to optimize an experiment to specific design parameters.
The user can define arbitrary target functions (residuals) to be minimized and which depend on :class:`Vars`.
The :class:`Optimizer` will then minimize your target function.
It is intended for theoretical calculations and can be applied to all :class:`Measurement` types.

There are different non-linear optimization algorithms implemented in *Nexus*.
The algorithms tackle your problem in different ways in order to find the best solution to your problem.
They can be divided in **local** and **global** algorithms.

* **Local** optimization should be used when you do not expect various local minima in the search area or the initial guess is already close to the global minima.
  The local methods can be further divided in **gradient-based** and **gradient free** methods.
* **Global** optimization should be used when you do not know much about your problem or you expect various local minima in the search region.

After a global optimization *Nexus* will call a local method.
This is done in order to find the local minimum around the minimum of the global method.

One of the most prominent algorithm is the *Levenberg-Marquardt* algorithm ``LevMar``, a gradient-based local algorithm. This one is typically the first one to use.
In global optimization the *Differential Evolution* algorithm is very powerful and ``PagmoDiffEvol`` is typically your first choice.
A full list of all algorithms can be found here, :ref:`api-optimizer-options`.

.. seealso:: `<https://en.wikipedia.org/wiki/Levenberg-Marquardt_algorithm>`_

             `<https://en.wikipedia.org/wiki/Differential_evolution>`_

             `<http://ceres-solver.org/nnls_tutorial.html>`_

             `<https://esa.github.io/pagmo2/overview>`_

             `<https://nlopt.readthedocs.io/en/latest/NLopt_Algorithms/>`_


The residual in data fitting
----------------------------

Both modules try to minimize a :math:`cost` function

.. math:: cost = n \sum_i L(y_i, \hat{y}_i),

where the loss :math:`L(y_i, \hat{y}_i)` specifies how different the data :math:`y_i` and the model :math:`\hat{y}_i` are.
In Nexus the loss is defined by a user defined residual function of the form

.. math:: r_i(y_i, \hat{y}_i)^a

depending on the experimental value :math:`y_i` and the theoretical value of your model :math:`\hat{y}_i = f(x_i, \beta)`, which depends on the independent variable :math:`x_i` and the parameter vector :math:`\beta` of the model.
The exponent in :math:`a` was 2 in versions 1.x.x, meaning that only least square problems were solved.
Since verion 2.0.0 the exponent :math:`a` can be set in order to solve various models.
The normalization factor :math:`n` is :math:`n = 1/2` in versions 1.x.x in order to be consistent with the cost value from the least square problems from the ceres sovler.
As the cost functions can be more genreal since version 2.0.0, it is now :math:`n = 1/N`, where :math:`N` is the number of data points.

A typical :math:`cost` function to be minimized in least square problems is

.. math:: cost = \frac{1}{N} \sum_i (y_i - \hat{y}_i)^2

This value is also the definition of the mean squared error (MSE) of the problem.

In data fitting the residual have to be weighted properly to their underlying statisitcs. 
A commonly used weighted residual in least square fitting is

.. math:: r_i(y_i, \hat{y}_i) = \frac{y_i - \hat{y}_i}{\sigma_i} = \frac{y_i - \hat{y}_i}{\sqrt{y_i}}

This will result in the :math:`cost` function

.. math:: cost = \frac{1}{N} \sum_i \frac{(y_i - \hat{y}_i)^2}{|y_i|}

It is the best unbiased estimator for uncorrelated errors for Gaussian statistics.
However, for the count statistics typically encountered in reflectivity or NRS measurements a better estimator can be found, which is an approximation of the log-likelihood function.
In **Nexus** the default residual function for fitting is 

.. math:: r_i = \left(\sqrt{y_i} - \sqrt{\hat{y}_i}\right)^2,

with the :math:`cost` function

 .. math:: cost = \frac{1}{N} \sum_i \left(\sqrt{y_i} - \sqrt{\hat{y}_i}\right)^2.

It is an approximation of a Poisson likelihood function [Thibault]_, which is better suited for Poisson statistics (low counts) as often encountered in time spectra and reflectivities.
The residual is always internally squared by the fitting and optimization routines in *Nexus* to fit a least squares problem.
Therefore, the log likelihood estimator is not directly implementable.

At larger count numbers the Poisson statistics approach a Gaussian distribution.
For Gaussian statistics, the likelihood function corresponds to the weighting with the standard deviation [Thibault]_.
Therefore, the difference between both estimators is typically small.

The residual implementation used in fitting can be changed by the :class:`Residual` class, :ref:`api-residual-class`.
For example, to use the weighting by the standard deviation use ``nx.residual.StdDev``.
More residual implementations can be found in :ref:`api-residual`.

.. admonition:: Changed in version 1.0.3

   For the classes :class:`Reflectivity` and :class:`Transmission` a log norm is applied now as the standard residual :math:`r_i = \log_{10}(y_i) - \log_{10}(\hat{y}_i)`.

.. admonition:: Changed in version 2.0.0

   Since version 2.0.0 it is possible to use a ``LogLikelihhod`` residual for certain algorithms (non-gradient methods).

   In order for Nexus to get a proper imput for different optimization modules the actual residual implementaiton is
   
   .. math:: \left(r_i(y, f)\right)^a

   where :math:`a` is an exponent that is applied to the residual function :math:`r(y,f)` internally in Nexus (This is due to the different interfaces of the used optimization algorithms).


.. seealso:: `<https://en.wikipedia.org/wiki/Loss_function>`_

             `<https://en.wikipedia.org/wiki/Goodness_of_fit>`_

             `<https://en.wikipedia.org/wiki/Weighted_least_squares>`_


The residual in optimization problems
-------------------------------------

For an optimization problem the residual is the difference between your target value and the model output.
Let's have a look at a simple example.
You want to minimize the intensity :math:`I` behind a sample that depends on various :class:`Var` objects :math:`v_i`.
The target value is :math:`0` as the intensity should be minimal and cannot be negative.
The corresponding residual is

.. math:: r = I(v_1, ..., v_i) - 0 = I(v_1, ..., v_i).

The minimization procedure will try to minimize this problem.

Especially in fitting and optimization the class referencing strategy of *Python* and *Nexus* gets very important.
Please remember the sections :ref:`introduction` and :ref:`tut-multilayer` where referencing in *Nexus* is explained.
If different objects reference the same :class:`Var` object, its :attr:`value` is used for all referencing objects.
This allows to couple a single :class:`Var` to various objects while fitting.
For multi measurement fits, referencing can get very important, for example, when the same :class:`Sample` or one of its properties has to be referenced in different :class:`Experiments` and :class:`Measurements`.


.. include:: tut_fitting.rst

.. include:: tut_constraints.rst

.. include:: tut_optimization.rst


Notebooks
---------

`fit global`_ - :download:`nb_fit_global.ipynb`.

.. _fit global: nb_fit_global.ipynb


`fit errors`_ - :download:`nb_fit_errors.ipynb`.

.. _fit errors: nb_fit_errors.ipynb


`fit multiple moessbauer`_ - :download:`nb_fit_multiple_moessbauer.ipynb`.

.. _fit multiple moessbauer: nb_fit_multiple_moessbauer.ipynb


`scipy optimize`_ - :download:`nb_scipy_optimize.ipynb`.

.. _scipy optimize: nb_scipy_optimize.ipynb


`fit equality constraint`_ - :download:`nb_fit_equality_constraint.ipynb`.

.. _fit equality constraint: nb_fit_equality_constraint.ipynb


`fit external variable Bloch`_ - :download:`nb_fit_external_variable_Bloch.ipynb`.

.. _fit external variable Bloch: nb_fit_external_variable_Bloch.ipynb


`fit inequality constraint`_ - :download:`nb_fit_inequality_constraint.ipynb`.

.. _fit inequality constraint: nb_fit_inequality_constraint.ipynb


`optimizer simple`_ - :download:`nb_optimizer_simple.ipynb`.

.. _optimizer simple: nb_optimizer_simple.ipynb


`optimizer cavity`_ - :download:`nb_optimizer_cavity.ipynb`.

.. _optimizer cavity: nb_optimizer_cavity.ipynb


`optimizer cavity multimodes`_ - :download:`nb_optimizer_cavity_multimodes.ipynb`.

.. _optimizer cavity multimodes: nb_optimizer_cavity_multimodes.ipynb


`optimizer spectrum`_ - :download:`nb_optimizer_spectrum.ipynb`.

.. _optimizer spectrum: nb_optimizer_spectrum.ipynb


Please have a look to the :ref:`sec-API` for more information.
