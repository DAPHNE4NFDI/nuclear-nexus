.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-material:


Material
========

A material is defined by its element composition and density.
It also holds information on the material specific parameters influencing nuclear resonant scattering: The Lamb-Moessbauer factor, the abundance of the isotope and the hyperfine parameters.

**The following parameters are** :class:`Var` **objects and can be fit:**
  
  * density of the material
  * abundance of the resonant isotope in the material
  * Lamb-Moessbauer factor of the resonant isotope in the material

For pure electronic calculations, only the composition and density have to be specified.

Density
-------

The density is given in g/cm^3.

Composition
-----------

The composition is given as a list of lists that define the element symbol as a string and the relative atomic weight or atomic composition.

.. code-block::

    import nexus as nx

    mat_iron_oxide = nx.Material(id = "my material",
                                 composition = [["Fe", 2], ["O", 3]],
                                 density = 5.3)

    print(mat_iron_oxide)

.. code-block::

    Material
      .id: my material
      .composition:  Fe 2.0  O 3.0
      .density (g/cm^3) Var.value = 5.3, .min = 0.0, .max = 23.0, .fit: False, .id: 
      .isotope: none
      .abundance Var.value = 0.0, .min = 0.0, .max = 1.0, .fit: False, .id: 
      .lamb_moessbauer Var.value = 0.0, .min = 0.0, .max = 1.0, .fit: False, .id: 
        derived parameters:
        .total_number_density (1/m^3) = 9.99372085016313e+28
        .average_mole_mass (g/mole) = 31.937400000000004
        .isotope_number_density (1/m^3) = 0.0
        number of hyperfine sites 0

When the composition of an alloy is given in wt% instead of at%, you have to convert the composition.
Use the :attr:`WtToAt()` function from the :mod:`conversions` module for that

.. code-block::

   permalloy_wt = (["Ni", 80], ["Fe", 20]) # or (['Ni', 0.8], ['Fe', 0.2])

   permalloy_at = nx.conversions.WtToAt([["Ni", 80], ["Fe", 20]])

   print(permalloy_at)

The output is ``(['Ni', 0.7919215353166942], ['Fe', 0.20807846468330582])```.
Similarly for Fe2O3

.. code-block::

    # Fe2O3 in relative atomic fraction
    Fe2O3_at = (['Fe', 2], ['O', 3])  # or (['Fe', 0.4], ['O', 0.6])

    Fe2O3_wt = nx.conversions.AtToWt(Fe2O3_at)

    print(Fe2O3_wt)

to get ``(['Fe', 0.6994307614270416], ['O', 0.3005692385729583])``.


Material Library
----------------

A material can also be loaded from the :mod:`lib.material` library, see :ref:`api-material`. It hosts a lot of pre-defined materials.
To do so, use the materials :attr:`Template` method.

.. code-block::

    mat_iron_oxide = nx.Material.Template(nx.lib.material.Fe2O3)


Isotope parameters
------------------

In case nuclear scattering is calculated also the isotope properties have to be given.
Those are

  * The abundance of the resonant isotope. The element must be in the composition of the material.
  * The material's Lamb Moessbauer factor
  * Hyperfine parameters that specify the hyperfine interactions

.. code-block::

    mat = nx.Material(
        id = "my material",
        composition = [["Fe", 2],["O", 3]],
        density = 5.3,
        isotope = nx.lib.moessbauer.Fe57,  # load isotope from library
        abundance = 0.95,                  # "Fe" in composition is made out of 95% of 57-Fe,
        lamb_moessbauer = 0.793,
        #hyperfine_sites = [])             # list Hyperfine objects acting on the isotope

    print(mat)

Here, no hyperfine parameters are passed.
In this case, nuclear resonant scattering is not accounted for.

.. versionadded:: 1.2.0

The Lamb-Moessbauer factor of the material overrides the site specific Lamb-Moessabuer factors of a :class:`Hyperfine` site.
In case you want to use hyperfine specific Lamb_Moessbauer factors set this parameter to ``None``.


Hyperfine interactions
----------------------

Let's define a set of hyperfine interactions, called a site,

.. code-block::

    # If no values are passed, there is no isomer shift,
    # no magnetic field and no quadrupole splitting. 
    # It will results in an unsplit line
    site = nx.Hyperfine()
    
    # apply to the material
    mat.hyperfine_sites = [site]

    print(mat)

That's it. The :class:`MoessbauerIsotope` and the :class:`Hyperfine` classes are covered in the next sections.



Notebooks
---------

`material`_ - :download:`nb_material.ipynb`.

.. _material: nb_material.ipynb


Please have a look to the :ref:`sec-API` for more information.
