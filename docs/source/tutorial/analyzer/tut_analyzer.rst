.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-analyzer:


Analyzer
========

An :class:`Analyzer` is a polarization selective device in the beam path that is described by a complex 2x2 Jones matrix.
An unpolarized detection for example is

.. math:: A= \begin{pmatrix} 1 & 0\\ 0 & 1 \end{pmatrix},

which is created by the default arguments during initialization via ``nx.Analyzer()``.

The polarization analyzer matrix of the beam (in version 1) is given by [Sturhahn2000]_

.. math::

    J =
    \frac{1}{2}
    \begin{pmatrix}
    2 - \xi (1- \cos{\chi} \cos{2\varphi})  & \xi (\sin{2\varphi}\cos{\chi} -i \sin{\chi}) \\
    \xi (\sin{2\varphi}\cos{\chi}  + i \sin{\chi}) & 2 - \xi (1 + \cos{\chi} \cos{2\varphi})
    \end{pmatrix}

where :math:`\xi` is the polarization detection efficiency, :math:`\chi` is the mixing angle, and :math:`\varphi` is the canting angle (which is corrected by a factor of 2 compared to [Sturhahn2000]_ to account for the correct polarization rotation).

.. admonition:: Changed in version 2.0.0

   The mixing angle is changed by a factor of 2 in order to be consistet with the theory of optics (e.g. [Born]_), see :ref:`scattering-geometry` . 

   The three parameters :math:`\xi`, :math:`\chi` and :math:`\varphi` are fittable now.

The actual analyzer matrix used since version 2 is given by the unpolarized coherency matrix :math:`I_2` and the normalized Jones vector :math:`\boldsymbol{j}` [Gil]_

.. math::
   
   J = (1-\xi) I_2 + \xi \boldsymbol{j} \boldsymbol{j}^{\dagger}

with

.. math::

    \boldsymbol{j} = R(\varphi)
    \begin{pmatrix}
    \cos\chi \\
    i \sin\chi  
    \end{pmatrix}
    = 
    \begin{pmatrix}
    \cos\varphi & -\sin\varphi \\
    \sin\varphi & \cos\varphi 
    \end{pmatrix}
    \begin{pmatrix}
    \cos\chi \\
    i \sin\chi  
    \end{pmatrix}.

where :math:`R` is the rotation matrix.
The mixing angle now corresponds to the ellipticiy angle of the ellipse of the Jones vector.
One obtains

.. math::
   
     J = \frac{1}{2}
         \begin{pmatrix}
         2 - \xi (1 - \cos{2\chi} \cos{2\varphi}) & \xi (\sin{2\varphi}\cos{2\chi} -i \sin{2\chi}) \\
         \xi (\sin{2\varphi}\cos{2\chi}  + i \sin{2\chi}) & 2 - \xi (1 + \cos{2\chi} \cos{2\varphi})
         \end{pmatrix}

It leads to the same matrix as used in verison 1 but the mixing angle multiplied by a factor of 2.


**The following parameters are** :class:`Var` **objects and can be fit:**
  
  * efficiency (since v2.0.0)
  * mixing_angle (since v2.0.0)
  * canting_angle (since v2.0.0)

When you set up an analyzer you can specify several parameters which are similar as in *CONUSS*.
Those are the efficiency, the mixing angle between the polarization components, and the canting angle of the beam with respect to :math:`\sigma` direction.

.. code-block::

    import nexus as nx
    import numpy as np

    # set beam along pi polarization via initialization parameters
    # those values are the default arguments
    analyzer = nx.Analyzer(efficiency = 1, mixing_angle = 0, canting_angle = 90)
    
    print(analyzer)

.. code-block::

    [[0.000000e+00+0.j 6.123234e-17+0.j]
     [6.123234e-17+0.j 1.000000e+00+0.j]]

There are methods to set specific polarization selective states.

* ``analyzer.Unpolarized()``
* ``analyzer.LinearSigma()``
* ``analyzer.LinearPi()``
* ``analyzer.CircularLeft()``
* ``analyzer.CircularRight()``

.. code-block::

    analyzer = nx.Analyzer()
    analyzer.CircularLeft()

    print(analyzer.matrix)

.. code-block::

    [[ 0.5+0.j  -0. -0.5j]
     [ 0. +0.5j  0.5+0.j ]]

or you can set the Jones matrix directly

.. code-block::

    # set the Jones matrix directly to unpolarized detection
    analyzer.SetJonesMatrix(np.array([[1,0], [0,1]], dtype=complex))

An analyzer is added to the objects list of the experiment

.. code-block::

    exp = nx.Experiment(id = "test experiment",
                        objects [sample1, analyzer, sample2],
                        isotope = nx.lib.Moessbauer.Sn119)

You can also rotate an analyzer by a certain angle

.. code-block::

    analyzer.Rotate(45)  # angle in deg

    print(analyzer)

.. code-block::

    Analyzer
      .id: 
      .matrix:
    [[0.5+0.j 0.5+0.j]
     [0.5+0.j 0.5+0.j]]


.. seealso:: `<https://en.wikipedia.org/wiki/Jones_calculus>`_ 


Notebooks
---------

`analyzer`_ - :download:`nb_analyzer.ipynb`.

.. _analyzer: nb_analyzer.ipynb


`polarization analysis`_ - :download:`nb_polarization.ipynb`.

.. _polarization analysis: nb_polarization.ipynb


Please have a look to the :ref:`sec-API` for more information.
