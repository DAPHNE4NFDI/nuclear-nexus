.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tutexperiment:


Experiment
==========

An :class:`Experiment` holds all information that are not measurement specific.
Those are the beam, all objects in the beam path and the resonant isotope for calculations of nuclear properties.
For non-resonant measurements the photon energy is given in the specific methods.

**An Experiment does not have any fittable parameters.**

.. code-block::

    import nexus as nx
    import numpy as np

    beam = nx.Beam(id="my beam")

    analyzer = nx.Analyzer(id="my analyzer")

    sample1 = nx.SimpleSample(thickness=3000,
                              composition = [["Fe", 1]],
                              density = nx.Var(7.874, min=7, max=7.874, fit=True, id = "sample density"),
                              id="my sample")

    experiment = nx.Experiment(id = "my experiment",
                               beam = beam,
                               objects = [sample1, analyzer],
                               isotope = nx.lib.moessbauer.Fe57)
    
    print(experiment)

.. code-block::

    Experiment:
      .id: my experiment
      .beam.id: my beam
      .objects:
        index: 0, type: Sample, .id: my sample
        index: 1, type: Analyzer, .id: my analyzer

To retrieve the electronic scattering factor (the electronic amplitude behind the experiment) or the scattering matrix of your experiment use

.. code-block::

    exp_scattering_factor = experiment.ElectronicAmplitude(energy = 14.4e3)

    detuning = np.linspace(-100, 100, 11)

    exp_matrix = experiment.Matrix(detunings = detuning,
                                   calc_transitions = True)

Notebooks
---------

`experiment`_ - :download:`nb_experiment.ipynb`.

.. _experiment: nb_experiment.ipynb


Please have a look to the :ref:`sec-API` for more information.
