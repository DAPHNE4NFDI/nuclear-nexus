.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _tutorial:

Tutorial
========

Welcome to the *Nexus* tutorial.
The tutorial will cover not only some basic examples but also show advanced methods to simulate and fit experimental data.
In order to follow the tutorial it is helpful to be familiar with *Python*'s data types, classes, and the modules *numpy* and *matplotlib*.

The naming conventions in *Nexus* are:

* Modules - ``lowercase``, e.g. ``nx.lib.moessbauer``.
* Classes - ``CamelCase``, e.g. ``nx.Beam``.
* Class methods and functions - ``CamelCase``, e.g. ``nx.ClebshGordon()`` or ``nx.Beam.LinearSigma()``.
* All class attributes - ``lower_case_with_underscores``, e.g. ``nx.Beam.jones_vector``.

Start with :ref:`tut-hello-nexus` tutorial for the basics and from there on proceed to the tutorials you need.
You can find jupyter notebooks with the examples at the end of each sections.

.. warning:: In case a **jupyter lab** or a **jupyter notebook** is used, be careful with data assignment in *Nexus*.
             It is recommended to **restart** the kernel and **re-run** all cells after a change of a cell or a second execution of a cell.
             This might be necessary due to the repeated command processing in **jupyter** when executing a cell and the object referencing in *Nexus*.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   hello_nexus/tut_hello_nexus.rst
   plotting/tut_plotting.rst
   beam/tut_beam.rst
   isotope/tut_isotope.rst
   material/tut_material.rst
   hyperfine/tut_hyperfine.rst
   hyperfine/tut_distributions.rst
   layer/tut_layer.rst
   sample/tut_sample.rst
   sample/tut_multilayer.rst
   sample/tut_eff_dens_model.rst
   sample/tut_thickness_distribution.rst
   analyzer/tut_analyzer.rst
   fixed_object/tut_fixedobject.rst
   conuss_object/tut_conuss_object.rst
   experiment/tut_experiment.rst
   methods/tut_measurement.rst
   time_dependence/tut_time_dependence.rst
   lib/tut_mod_lib.rst
   data/tut_data.rst
   optimization/tut_fit_optimization.rst
   quantum/tut_quantum.rst
   scattering/tut_scattering.rst
