.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-eff-dens-model:


Effective density model
=======================

In case you specify ``roughness = e`` for a sample, the effective density model is automatically created by *Nexus*.
You can also create the effective density model directly for your sample.

.. code-block::

    import nexus as nx
    import numpy as np
    import matplotlib.pyplot as plt

    lay_Pt_top = nx.Layer(id = "Pt top",
                          material = nx.Material.Template(nx.lib.material.Pt),
                          thickness = 2,
                          roughness = 2.0)

    lay_C = nx.Layer(id = "C",
                     material = nx.Material.Template(nx.lib.material.C),
                     thickness = 20,
                     roughness = 2.3)

    lay_Pt = nx.Layer(id = "Pt",
                      material = nx.Material.Template(nx.lib.material.Pt),
                      thickness = 15,
                      roughness = 0.2)

    lay_substrate = nx.Layer(id = "Si sub",
                             material = nx.Material.Template(nx.lib.material.Si),
                             thickness = nx.inf,
                             roughness = 0.2)

    sample = nx.Sample(layers = [lay_Pt_top, lay_C, lay_Pt, lay_substrate],
                       geometry = "r",
                       length = nx.inf,
                       roughness = "a")
    # here an analytical roughness is assumed, so the effective density model is not created automatically.

    # specify the effective layer thickness and
    # if nuclear scattering matrices should be calculated and mixed as well
    sample.EffectiveLayerSystem(eff_layer_thickness = 0.3, nuclear = False)

To get more information on the created effective density model in *Nexus* use the appropriate class attributes :attr:`effective_layers`, :attr:`extended_layers` and :attr:`effective_coordinates`.
The :attr:`effective_layers` and :attr:`extended_layers` attributes hold objects of the :class:`EffectiveLayer` class.

Before the effective layers are created an extended layer system has to be defined by *Nexus*.
Here, additional top and bottom layers are added to account for the additional layers that have to be created in the effective layer system.
The :attr:`effective_layers` are the sliced layers of equal size actually used by *Nexus* in calculations.
Print the extended and the effective layers

.. code-block::
    
    print("extended layers\n")
    for lay in sample.extended_layers:
        print(lay)

    print("effective layers\n")
    for lay in sample.effective_layers:
        print(lay)

Additional layers are added on top and below of the layer system.
Their thickness is automatically adjusted by the roughnesses of the top and bottom layers.
All scattering factors are not meaningful up to now because no method has been used that actually calculates these.

To gain more insight into the effective density model we can inspect how these extended layers will be weighted in the effective layers.

.. code-block::

    for lay in sample.extended_layers:
        plt.plot(sample.effective_coordinates, lay.effective_layer_weight)

    plt.xlabel('depth (nm)')
    plt.ylabel('layer weight')
    plt.show()

    # check that the sum of all layer weights is one everywhere
    sum = np.zeros(len(sample.extended_layers[0].effective_layer_weight))

    for lay in sample.extended_layers:
        sum = np.add(sum, np.array(lay.effective_layer_weight))

    print(sum)

.. image:: eff_dens_weights.png

.. note:: The total thickness of the effective layer system is larger than the one of your sample definition due to the additional layers created to account for the first and last interface properly.

Let's calculate a property of the sample with the effective layer system.

.. code-block::

    # set an angle for the field intensity calculation
    # this is the angle of the first cavity minimum
    sample.angle = 0.158

    # set the effective density roughness model
    sample.roughness = "e"

    # calculate the field intensity
    field_int = nx.FieldIntensity(sample = sample,
                                  energy = nx.moessbauer.Fe57.energy,
                                  points = 1001)

    depth, int = field_int()

    plt.plot(depth, int)
    plt.xlabel('depth (nm)')
    plt.ylabel('relative field intensity')
    plt.show()

.. image:: field_intensity.png

and print the layers again. Now they all have scattering factors assigned.

.. code-block::

    print("extended layers\n")

    for lay in sample.extended_layers:
        print(lay)

    print("effective layers\n")
    for lay in sample.effective_layers:
        print(lay)

.. note:: *Nexus* mixes the scattering matrices of the extended layers directly to obtain the effective layer scattering matrices.
          It does not create a new material with a new element composition (and hyperfine fields), which is often found in the literature on the effective density model.
          If you want to account for additional interface hyperfine effects due to roughness, create specific interface layers in your sample.


Notebooks
---------
    
`effective model`_ - :download:`nb_effective_model.ipynb`.

.. _effective model: nb_effective_model.ipynb