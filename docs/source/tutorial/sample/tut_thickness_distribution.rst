.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-thickness-distribution:


Thickness and divergence distributions
======================================

A thickness distribution of the layers in a sample or the beam divergence onto the sample have to be treated specially as the intensities have be added incoherently (with a typical partly coherent beam of a synchrotron).
A layer has the :attr:`thickness_fwhm` attribute specifying the full width half maximum of a thickness distribution around the layer thickness.
The sample has the :attr:`divergence` attribute specifying the full width half maximum of an angular distribution around the incidence angle of the beam on the sample.

.. versionadded:: 1.0.3

   A new :attr:`coherence` parameter has been added to many measurement types.
   It determines weather the summation over the thickness distributions or angular divergence is performed coherently or incoherently.

To take these distributions into account you must specify :attr:`distribution_points` to larger than one in the corresponding measurement method.

.. note:: By default *Nexus* does not use thickness or divergence distributions.

Thickness distributions are typically observed in time spectra when the hyperfine interactions are small and dynamical effects are present.
Therefore, we use an enriched stainless steel foil without any hyperfine interactions.

.. code-block::

   # import packages
   import nexus as nx
   import numpy as np
   import matplotlib.pyplot as plt

   layer_SS = nx.Layer(id = "Stainless steel",
                       material = nx.Material.Template(nx.lib.material.SS_enriched),
                       thickness = 3000,      # in nanometer
                       thickness_fwhm = 300)  # in nanometer

   site = nx.Hyperfine()

   layer_SS.material.hyperfine_sites = [site]

   sample = nx.Sample(layers = [layer_SS])

   beam  = nx.Beam()
   beam.LinearSigma()

   exp = nx.Experiment(beam = beam,
                       objects = [sample],
                       isotope = nx.moessbauer.Fe57)

   time_spectrum = nx.TimeSpectrum(experiment = exp,
                                   time_length = 200,
                                   time_step = 0.2,
                                   distribution_points = 1)  # no distribution used, the default value

   time_axis, intensity = time_spectrum.Calculate()
 
   plt.semilogy(time_axis, intensity, label='no dist')

   # now with distribution
   time_spectrum.distribution_points = 31  

   plt.semilogy(*time_spectrum.Calculate(), label='with dist')
   plt.legend()
   plt.xlabel('time (ns)')
   plt.ylabel('intensity ($\Gamma$/ns)')
   plt.show()

.. image:: thick_dist.png

The same holds for the beam divergence onto a sample.
The beam divergence is especially important while fitting time spectra in thin film cavities with strong dynamical effects.

For experiments with several samples and combined thickness distributions in forward geometry and beam divergences in gracing incidence geometry, the same distribution points number is used for all distributions in the experiment.

.. note:: This parameter must be specified for each measurement you set up.
          If you want to fit several measurements simultaneously with thickness and/or beam divergence distributions set the :attr:`distribution_points` to the same value for all measurements in the fit.


Notebooks
---------
    
`thickness distribution`_ - :download:`nb_thickness_distribution.ipynb`.

.. _thickness distribution: nb_thickness_distribution.ipynb
