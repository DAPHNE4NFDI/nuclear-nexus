.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-fixed-object:


Fixed Object
============

A :class:`FixedObject` is an object in the beam path that has fixed scattering factor and matrix at a specific energy or over a detuning range.
The advantage of a fixed object is that the scattering matrix can be set freely and optical activity can be introduced even without having a nuclear resonant material.

**A fixed object does not have any fittable variables.**

A fixed object is specified by the electronic scattering factor and the complex 2x2 scattering matrix.
Which property is used depends on the calculation method. 
All polarization dependent calculations use the matrix, non-polarization sensitive methods use the scattering factor.

.. code-block::

    import nexus as nx
    import numpy as np

    factor = 0.876-0.965j

    matrix = np.array([[0.5, 0.5j], [-0.5j, 0.5]])

    fixed_object = nx.FixedObject(factor, matrix, "my object")

    print(fixed_object)

.. code-block::

    FixedObject:
      .id: my object
      .scattering_factor: (0.876-0.965j)
      .matrix:
    [[ 0.5+0.j   0. +0.5j]
     [-0. -0.5j  0.5+0.j ]]  

The default arguments ``nx.FixedObject()`` create an object with a scattering factor of 1 and the identity matrix.

A fixed object is added to the objects list of the experiment like any other object

.. code-block::

    exp = nx.Experiment(id = "test experiment",
                        objects [sample1, fixed_object],
                        isotope = nx.lib.Moessbauer.Sn119)

Notebooks
---------

`fixed object`_ - :download:`nb_fixed_object.ipynb`.

.. _fixed object: nb_fixed_object.ipynb


Please have a look to the :ref:`sec-API` for more information.
