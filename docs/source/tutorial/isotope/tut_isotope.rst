.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-isotope:


Moessbauer isotope
==================

A :class:`MoessbauerIsotope` host all information of the Moessbauer isotope parameters needed to calculate the nuclear transitions.

.. versionadded:: 1.2.0
   Now a couple of isotope parameters are fitable.

**The following parameters are** :class:`Var` **objects and can be fit:**

  * internal_conversion
  * mixing_ratio_E2M1
  * gfactor_ground
  * gfactor_excited
  * quadrupole_ground
  * quadrupole_excited
  * interference_term

A :class:`MoessbauerIsotope` is only needed for calculations of nuclear properties.
There are a couple of predefined isotopes defined in the :mod:`lib.moessbauer` library.

.. code-block::

    import nexus as nx

    my_isotope = nx.lib.moessbauer.Ta181

    print(my_isotope)

    print(my_isotope.nuclear_cross_section) # in m^2

.. code-block::

    MoessbauerIsotope: 181-Ta
      .element = Ta
      .mass (u) = 180.94799645
      .spin_ground = 3.5
      .spin_excited = 4.5
      .energy (eV) = 6215.7
      .wavelength (m) = 1.994694e-10
      .kvector (1/m) = 3.149949e+10
      .lifetime (s) = 8.728299999999999e-06
      .gamma (eV) = 7.541124352966788e-11
      .internal_conversion = 46.0
      .multipolarity = E1 (L = 1, lambda = 1)
      .mixing_ratio_E2M1 = 0.0
      .gfactor_ground = 0.6772857
      .gfactor_excited = 1.173
      .quadrupole_ground (barn) = 3.17
      .quadrupole_excited (barn) = 3.71
      .interference_term = -0.08
      .magnetic_moment_ground (eV/T) = 7.472885542354939e-08
      .magnetic_moment_excited (eV/T) = 1.6640213949517502e-07
      .nuclear_cross_section (converted to kbarn) = 1684.1659150180583

    1.6841659150180582e-22

or you can define the Moessbauer isotope by its constructor

.. code-block::

  Ta181 = nx.MoessbauerIsotope(
    isotope = "181-Ta",
    element = "Ta",
    mass = 180.94799645,
  
    energy = 6215.7,
    lifetime = 8728.3,
  
    internal_conversion = 46,
    multipolarity = nx.Multipolarity_E1,
    mixing_ratio_E2M1 = 0,
  
    spin_ground = 7/2,
    spin_excited = 9/2,
  
    gfactor_ground = 0.6772857,
    gfactor_excited = 1.173,
  
    quadrupole_ground = 3.17,
    quadrupole_excited = 3.71,

    interference_term = -0.08
  )

  print(Ta181)

  print(Ta181.nuclear_cross_section)

The print command gives additional derived parameters of the isotope that can also be accessed as shown before.


.. seealso:: https://en.wikipedia.org/wiki/Isotope

             https://en.wikipedia.org/wiki/Selection_rule

             `<https://en.wikipedia.org/wiki/G-factor_(physics)>`_
             
             https://en.wikipedia.org/wiki/Nuclear_magnetic_moment

             https://en.wikipedia.org/wiki/Internal_conversion

             https://en.wikipedia.org/wiki/Mossbauer_spectroscopy


Notebooks
---------

`isotope`_ - :download:`nb_isotope.ipynb`.

.. _isotope: nb_isotope.ipynb


Please have a look to the :ref:`sec-API` for more information.
