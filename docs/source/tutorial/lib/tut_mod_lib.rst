.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _tut-mod-lib:


Modules and Libraries 
=====================

*Nexus* provides a couple of modules that add some basic functionalities and some libraries with predefined object definitions.

Modules
-------

* ``constants``
           Module with mathematical constants (``nx.constants.inf``, ...),  physical constants, like electron mass, speed of light, and so on, e.g. ``nx.constants.NuclearMagneton``.
           See :ref:`api-constants`.
* ``conversions``
           Module with functions to convert various properties into different units, e.g. ``nx.conversions.VelocityToGamma()`` or ``nx.conversions.WtToAt()``.
           See :ref:`api-conversions`.
* ``data``
           Module with functions to load and manipulate typical data sets, e.g. ``nx.data.load()``.
           See :ref:`api-data` and :ref:`tut-data` tutorial section.
* ``euler``
          Module with functions to work with Euler angles, e.g. ``nx.euler.TransformationMatrixToZYZEuler()``.
          See :ref:`api-euler`.
* ``theory``
          Module with functions for theoretical calculations, e.g. ``nx.theory.RecoilEnergy()``.
          See :ref:`api-theory`.

          .. versionadded:: 1.0.3
* ``tools``
          Module with some useful functions.
          See :ref:`api-tools`.

          .. versionadded:: 1.2.0

Libraries
---------

* ``lib.moessbauer`` 
         Library with predefined :class:`MoessbauerIsotopes`.
         The naming convention is `ElementMassnumber` e.g. ``nx.lib.moessbauer.Ir193``.
         See :ref:`api-moessbauer`.

* ``lib.energy``
         Library with predefined transition energies.
         For nuclear transitions the naming convention is `ElementMassnumber` e.g. ``nx.lib.energy.Eu151``. 
         For atomic transition it is `ElementTransitionname` e.g. ``nx.lib.energy.MoKalpha``.
         See :ref:`api-energy`.

* ``lib.material``
         Library with predefined :class:`Materials`.
         For compounds and alloys the naming convention is `Element1Relativeweight1...` e.g. ``nx.lib.energy.Al2O3``. 
         For isotope enriched materials with typically 95% enrichment the conventions is `Material_enriched` e.g. ``nx.lib.energy.Fe_enriched``.
         See :ref:`api-material`.

* ``lib.distribution``
        Library with predefined :class:`Distributions`.
        A variety of different distributions are included, e.g. ``nx.lib.distribution.Log()``.
        See :ref:`api-distribution`.

* ``lib.residuals``
        Library with predefined :class:`Residuals`.
        A couple of different residual implementations are included, e.g. ``nx.lib.residual.Sqrt()``.
        See :ref:`api-residual`.


Notebooks
---------

`tools`_ - :download:`nb_tools.ipynb`.

.. _tools: nb_tools.ipynb



Have a look to the :ref:`sec-API` for more information.
