.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _news:


Whats new?
==========

With *Nexus* being actively developed, there are regular new versions and releases.
You can find a list of what's new to the different versions here.

.. admonition:: API changes in version 2.0.0

   The next version - 2.0.0 - will experience changes in the API.
   Full backward compatability to version 1.X.X cannot be assured.


latest
------

API changes:
************

#. :class:`Sample` removed.
   A :class:`Sample` constructor still exists to automatically generate the new sample classes :class:`FowrardSample` and :class:`GrazingSample`.

#. new :class:`ForwardSample` class.
   Specific sample class with proper mehtods for samples in forward scattering geometry.

#. new :class:`GrazingSample` class.
   Specific sample class with proper mehtods for samples in grazing incidence scattering geometry.

#. :class:`Beam` class changed.
   Default constructor creates an unpolarized beam.
   The attributes :attr:`polarization`, :attr:`mixing_angle` and :attr:`canting_angle` have been added and are fitable.
   :attr:`SetCoherencyMatrix()` and :attr:`Rotate()` methods have been removed due to possible conflict in fits.
   mixing angle definition changed by a factor of two.
   Jones vector is set automatically during initialization.

#. :class:`Analyzer` class changed.
   The attributes :attr:`efficiency`, :attr:`mixing_angle` and :attr:`canting_angle` have been added and are fitable.
   :attr:`SetJonesMatrix()` and :attr:`Rotate()` methods have been removed due to possible conflict in fits.
   mixing angle definition changed by a factor of two.

#. :class:`Residual` class changed.
   The residual function has been changed to support an adjustable exponent.
   It enables to use other than least-square prolems, like log-likelihood estimators.
   It does not work with the ceres solver modules, which always solve least-square problems.

#. :attr:`natural_abundance` attribute added to :class:`MoessbauerIsotope`.

bug fixes
*********

#. fixed analytical roughness calculation error in grazing incidence (of version 1.2.0).
   Roughness is now calculated up to first order instead of second order.

changes
*******

#. :class:`Ln()` residual renamed to :class:`Log()`.

#. :class:`Log10()` and :class:`Log()` residuals changed. Valid data changed from ``x>1.0`` to ``x>0.0``.

#. :class:`LogLikelihood()` residual added.

#. roughness calculation of nuclear part changed from second order to first order correction.

#. New :func:`Fold_Interplotate()` function in data module. 

#. :func:`AutoFold()` is now able to fold multiple data sets the same way.


miscellaneous
*************

#. Gitlab piplines changed to miniforge3. Conda installation instruction inlcude miniforge options.
   Miniforge should be used by some users due to license restrictions on conda.


version 1.2.0
-------------

bug fixes: 

#. :class:`SimpleSample` assignment works again
#. corrected error in ``TimeSpectrum.Plot()``.
#. ``SetAtomicScatteringFactorCXRO()`` works again
#. fixed error on multiple assignments of the same :class:`Material` instance to several :class:`Layer` instances.
#. fixed bug in fits of :class:`EnergyTimeSpectra`.
#. changed 3D mag distributions to true spherical random values.
#. *numpy* version 2.0 conflict for some python versions resolved.


added:

#. included notebooks to tutorials.
#. new Examples section in documentation with juypter notebooks.
#. new tools module with CountRateEstimator() and Mask() function. The mask function restricts arrays to a certain range. Useful for plotting.
#. added GetSiteSpectra() to tools module. Get the individual spectral components of a :class:`Sample` object.
#. added AreaSites() to tools module. Get the area ratio of the individual sites of a :class:`Sample` object.
#. broadening parameter added to Hyperfine class to describe model-free independent line broadening.
#. texture coefficient added to Hyperfine class.
#. site specific Lamb-Moessbauer factor to Hyperfine class.
#. added AreaRatios() and AngleA23() to theory module to determine the area ratio A23 from a polar magnetic angle and vice versa.
#. added BeamDivergence() function to theory module.
#. added Normalization() function data module.
#. added Lorentzian functions FindPeaks(), GetLinewidths(), GetLorentzian(), GetAreas() to data module.
#. new LoadFile() function in data module.
#. a couple of Moessbauer isotope parameters are now fitable.
#. new ClearDistribution() method.
#. added 99-Ru, 67-Zn, 172-Yb and 174-Yb isotopes.
#. added residual SqrtStdDev.
#. added Racah W coefficient and 6j-symbols to quantum module
#. added some polynominal functions to quantum module (not for macOS).
#. print progress output option for nuclear reflectivity methods added. Default is ``False`` now.
#. interpolation option added to AutoFold.
#. added file output for fitting module.


changes:

#. SetRandomDistribution() method optimized. New methods for random sampling.
#. AutoFold() optimized. Was not working correctly for some data sets. Interpolation implemented. 
#. Plot() functions show the residuals used for the cost function and the bare residuals now.


version 1.1.1
-------------

bug fixes:

#. fixed error (of v 1.1.0 and 1.0.4) that only sample objects are used


version 1.1.0
-------------

bug fixes: 

#. fix shift error in FFT for AmplitudeTime method
#. correct scaling of nuclear reflectivity
#. corrected data race issue for parallel computation

added:

#. equality contraints for fitting
#. inequality constraints for fitting
#. external fit variables for fitting
#. error analysis for fit parameter via gradient method or bootstrap method
#. temporal and angular offsets can be fit
#. file output of the fit report
#. file save option to Plot() functions


version 1.0.4
-------------

bug fixes: 

#. wrong ordering of complex entries in AmplitudeTime method, issue 11
#. squared scaling for nuclear reflectivity, issue 12
#. velocity calibration offset, issue 13
#. non-deterministic behavior with parallelization, issue 15
#. wrong nuclear scattering results for thick resonant layers in reflectivity, issue 16.
   Changed check for matrix invertibility and calculation mod.

added:

#. SimpleSample class 
#. Sample motion
#. FunctionTime class
#. SimpleSample class
#. Second Order Doppler Debye Velocity functions
#. baseline correction functions to data module
#. more isotopes

updated meson build to account for pagmo 2.13


version 1.0.3
-------------

bug fixes:

#. distribution_points assignment, issue 5
#. rectangular beam profile correction, issue 9
#. on nuclear calculations on multilayers, issue 10

Standard residual of reflectivity and transmission methods set to ``residual.Log10()``

added:

#. 121-Sb, 125-Te, 45-Sc
#. theory module
#. nuclear quality factor
#. coherent and incoherent summation for thickness and angles
#. Plot functions for most methods


version 1.0.2
-------------

added:

#. CLANG support
#. magnetic field conversion functions
#. functions to find extrema
#. flip option to folding functions


version 1.0.1
-------------

added:

#. CONUSS diffraction object 
#. time window for energy methods
#. legal notes
#. automatic scaling and background option
