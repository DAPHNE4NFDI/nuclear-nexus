.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _software:


Software
========

Dependencies
------------

Nexus relies on a number of sophisticated software tools and libraries.
Those are [Cpp]_, [Python]_, [SWIG]_, [Eigen]_, [PocketFFT]_, [Ceres]_, [Pagmo]_ and [NLopt]_ and further dependencies of these packages.
Without them, *Nexus* would not have been possible.

.. [Cpp] ISO International Standard ISO/IEC 14882:2017(E) - Programming Language C++. Geneva, Switzerland: International Organization for Standardization (ISO). Available at `<https://isocpp.org/std/the-standard>`_.

.. [Python] Python Software Foundation. Python Language Reference, version 3.X. Available at `<http://www.python.org>`_.

.. [Eigen] G. Guennebaud, B. Jacob and others, Eigen 3.4.0, `<http://eigen.tuxfamily.org>`_.

.. [PocketFFT] M. Reinecke, PocketFFT for C++, `<https://gitlab.mpcdf.mpg.de/mtr/pocketfft/-/tree/cpp>`_.

.. [SWIG] David M. Beazley and SWIG developers, SWIG 4.0.2, `<http://www.swig.org/>`_.

.. [Ceres] S. Agarwal, K. Mierle and the Ceres Solver Team, Ceres Solver 2.0.0, `<https://github.com/ceres-solver/ceres-solver>`_ and `<http://ceres-solver.org/index.html>`_.

.. [NLopt] Steven G. Johnson, The NLopt nonlinear-optimization package, `<https://github.com/stevengj/nlopt>`_ and `<https://nlopt.readthedocs.io/en/latest/>`_.

.. [Pagmo] F. Biscani and D. Izzo, pagmo 2.1.8, `<https://github.com/esa/pagmo2>`_, `<https://esa.github.io/pagmo2/index.html>`_, [Biscani]_.


For further information on these software licenses have a look to their documentation.


Other evaluation tools
----------------------

A couple of other simulation and evaluation tools for Moessbauer, NRS and reflectivity measurements strongly influenced the design of *Nexus*.
Those are [CONUSS]_, [GenX]_, and [pynuss]_.

.. [CONUSS] W. Sturhahn, CONUSS 2.2.1, `<https://www.nrixs.com/conuss.html>`_.

.. [GenX] Artur Glavic, GenX 3.1.0, `<https://sourceforge.net/projects/genx/>`_.

.. [pynuss] K. Heeg and J. Evers, pynuss, `<https://www.mpi-hd.mpg.de/mpi/de/forschung/abteilungen-und-gruppen/theoretische-quantendynamik-und-quantenelektrodynamik/correlated-and-x-ray-quantum-dynamics>`_.
