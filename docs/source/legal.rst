.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _sec-legal:

.. image:: DESY.png

|

Legal Notice
=============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

Imprint
-------

`imprint <https://www.desy.de/impressum/index_ger.html>`_


Data Privacy Policy
-------------------

`data privacy policy <https://www.desy.de/data_privacy_policy/>`_


Declaration of Accessibility
----------------------------

`declaration of accessibility <https://www.desy.de/declaration_of_accessibility/index_eng.html>`_


Contact
-------

| Lars Bocklage
| Deutsches Elektronen-Synchrotron DESY
| Photon Science - Magnetism and Coherent Phenomena
| Email: lars.bocklage@desy.de
| https://photon-science.desy.de/research/research_teams/magnetism_and_coherent_phenomena/index_eng.html


| DESY: desypr@desy.de
| https://www.desy.de/index_eng.html
