.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _theory:


What *Nexus* calculates
-----------------------

Wavefield propagation
*********************

Nexus uses a scattering approach, a polarization-dependent transfer matrix formalism, to calculate the interaction of the wavefield with objects in the beam path.
For that, the beam properties and the scattering properties of all objects in the beam path have to be specified or calculated.

The beam is described by a complex 2x2 coherency matrix in the general case or a Jones vector for calculations of amplitude parameters for fully coherent beams.
The Jones vector :math:`\vec{E}` describes a fully coherent beam, while the coherency matrix :math:`J` also includes incoherent light.
The Jones vector is

.. math::

    \vec{E} = \begin{pmatrix}
               E_{\sigma} \\
               E_{\pi} \\
              \end{pmatrix}

and the coherency matrix is related to the Jones vector via :math:`J_{ij} = \braket{E_i E_j^*}`, such that

.. math::

    J =
    \begin{pmatrix}
    J_{\sigma\sigma} & J_{\sigma\pi} \\
    J_{\pi\sigma} & J_{\pi\pi}
    \end{pmatrix}
    =
    \begin{pmatrix}
    \braket{E_\sigma E_\sigma^*} & \braket{E_\sigma E_\pi^*} \\
    \braket{E_\pi E_\sigma^*} & \braket{E_\pi E_\pi^*}
    \end{pmatrix}.

Polarization properties are described along two orthogonal polarization vectors, here the linear polarization components along :math:`\sigma` and :math:`\pi` directions. 
For example, the normalized Jones vector of a fully polarized beam along :math:`\sigma` direction is given by

.. math:: E = \left( \begin{matrix} 1 \\ 0 \end{matrix} \right)

while the coherency matrix is

.. math:: J_p = \begin{pmatrix} 1 & 0\\ 0 & 0 \end{pmatrix}.

For unpolarized light the coherecny matrix is

.. math:: J_u = \frac{1}{2} \begin{pmatrix} 1 & 0\\ 0 & 1 \end{pmatrix}.

All intermediate polarization states are described by 

.. math:: J = (1-P) J_u + P J_p.

The coherency matrix is Hermitian

.. math:: J^{\dagger} = J.

The trace of the coherency matrix

.. math:: I = \operatorname{Tr}(J) = |E_{\sigma}|^2 + |E_{\pi}|^2,

corresponds to the intensity :math:`I` of beam.
*Nexus* always uses a normalized intensity input.


With an optical element :math:`M` affecting the Jones vector :math:`\vec{E}` the output vector is

.. math:: \vec{E}_{out} = M * \vec{E}_{in},

while for :math:`J` the relation is

.. math:: J_{out} = M * J_{in} * M^{\dagger}.

The output field holds all the information of the scattering properties of the objects.
In fact, the :math:`M(E)` is actually energy dependent such that the spectra can be calculated.

The matrix :math:`M` is the transfer matrix of the whole experiment and this is calculated from the parameters specified in all of the *Nexus* objects (like a sample or an analyzer).
The transfer matrix :math:`M = \prod_i m_i` can consist of many different object matrices :math:`m_i`.
For a sample, each layer has its own layer matrix :math:`L_i` and the sample matrix is :math:`m_s = \prod_i L_i`. These layers are described by a refractive index.
This refractive index is determined by the electronic and nuclear scattering cross sections which are calculated by *Nexus*.

.. seealso:: For more information on the Jones formalism and the coherency matrix, see [Born]_ chapter 10.9,
             `<https://en.wikipedia.org/wiki/Jones_calculus>`_ or
             `<https://en.wikipedia.org/wiki/Polarization_(waves)>`_ .

             For more information on the transfer matrix formalism and the cross section calculations, see [Sturhahn]_ and [Roehlsberger]_.


Nuclear response
****************

By default, the complete response of the system is calculated by *Nexus*.
The incoming photon field as well as electronic and nuclear scattering are considered.
The transmitted field :math:`\vec{E}_{out}` consist of two contributions [Smirnov]_:
electronic scattering :math:`\vec{E}_{el}` and coherent nuclear resonant scattering :math:`\vec{E}_{nuc}`

.. math:: \vec{E}_{out} = \vec{E}_{el} + \vec{E}_{nuc} = M * \vec{E}_{in} = (M_{el} + M_{nuc}) * \vec{E}_{in},

where :math:`M_{el}` is the total scattering response, :math:`M_{el}` is the pure electronic scattering response, :math:`M_{nuc}` is the nuclear response.


Sometimes one is only interested in the nuclear response, meaning that the transmitted pure electronic scattering should be excluded.
Some of the methods that calculate nuclear properties provide the parameter :attr:`electronic`.
When set to ``False`` only the nuclear response is calculated.
*Nexus* will then calculate the output field :math:`\vec{E}_{out} = M * \vec{E}_{in}` from the total response and subtract the pure electronic response :math:`M_{el} * \vec{E}_{in}`

.. math:: \vec{E}_{nuc} = (M - M_{el}) * \vec{E}_{in}.

For the intensity *Nexus* uses

.. math:: I_{nuc} = \operatorname{Tr}((M - M_{el}) * J_{in} * (M - M_{el})^{\dagger}).
