# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:#www.gnu.org/licenses/>.
 
 
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys
import sphinx_rtd_theme
import importlib.metadata

sys.path.insert(0, os.path.abspath('../../src/nexus'))

# -- Project information -----------------------------------------------------

project = 'Nexus'
copyright = '2020 - 2025, Deutsches Elektronen-Synchrotron DESY'
author = 'Lars Bocklage'

release = importlib.metadata.version('nexus')

version = "latest" #release

print("\nBuilding Nexus documentation for release {}\n".format(release))

# -- General configuration ---------------------------------------------------

extensions = ['sphinx.ext.napoleon',
              'sphinx.ext.autodoc',
              'sphinx.ext.doctest',
              'sphinx.ext.autosectionlabel',
              'sphinx_rtd_theme',
              'sphinx.ext.autosummary',
              'nbsphinx'
              ]

templates_path = ['_templates']

source_suffix = {'.rst': 'restructuredtext'}

master_doc = 'index'

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store'] # '**.ipynb_checkpoints'],# '**.ipynb']

pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

html_theme = "sphinx_rtd_theme"

html_css_files = ['custom.css']

html_static_path = ['_static']

html_show_sourcelink = False

autosectionlabel_prefix_document = True

autodoc_default_flags = ['members']

autosummary_generate = True

autoclass_content = 'both'
