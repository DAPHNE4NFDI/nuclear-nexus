.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _installation:


Installation
============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

There are various ways to get *Nexus* running.
The easiest option is to use one of the precompiled *wheels*, which are provided for some Windows *Python* versions.
Currently, no precompiled *Conda* packages are provided, but it can be used to install the various build-time and run-time dependencies to compile *Nexus*.
In case you use *Conda* or the manual compilation, you can compile *Nexus* for your specific *Python* version.
Choose one of the next three options which best fits your needs.


Option 1: *Nexus* wheels for Windows
------------------------------------

You need *Python* >= 3.7 to use the precompiled wheels.
The *Nexus* wheels can be installed directly to your python installation or be used in a virtual environment, e.g. a conda environment.
All necessary dependencies are provided by the wheels.

When you install *Nexus* on Windows for the first time, I recommend to check the **Add Python to PATH** option during installation and use a simple folder name like ``C:\PythonX`` to install in.
I recommend not to install to the ``Programfiles`` folder of Windows.
This will help to prevent issues during installation of packages.

.. seealso:: https://www.onmsft.com/how-to/how-to-download-and-install-python/

* Make sure you have *Python* installed.

* Download the *Nexus* wheel provided your *Python* version >=3.7 from the *Nexus* release page (or from the ``dist`` folder).

* In the command line, navigate to your folder with the *Nexus* distribution and run the *pip* installer with the *Nexus* wheel as argument
  ``python -m pip install nexus-1.x.x-cp3X-cp3X-win_amd64.whl``, for version 1.x.x and a *Python* 3.X build.
  In case you have several *Python* versions installed use
  ``py -3.X -m pip install nexus-1.x.x-cp3X-cp3X-win_amd64.whl``, to install *Nexus* for *Python* 3.X.

.. seealso:: In case you have multiple *Python* versions, also have a look to
             `<https://snarky.ca/why-you-should-use-python-m-pip/>`_

When you ``import nexus`` it might happen that you get a `.dll` load error.
In this case install the Microsoft Visual C++ Redistributable package, restart and try again.
Your CPU must support AVX2 instructions for the precompiled versions.


Option 2: Compiling *Nexus* in a *Conda* environment
----------------------------------------------------

This installation procedure works with Linux, Windows and macOS.
It uses *Conda* to provide all the dependencies including Python.

Linux and macOS have C++ compilers.
For Windows you have to download the MSVC compiler.

.. note:: .. versionadded:: 1.0.2

          macOs support added.
          By default the build system will choose the Apple Clang compiler, which does not support parallelization with the used C++17 standard.
          So, you will get reduced performance of *Nexus* with Clang.


On Windows: Install Visual Studio
*********************************

On Windows, you have to install Visual Studio with correct dependencies to compile for the latest Python versions.
The installation selection here is rather universal and should run for all Python version >= 3.5.

*  `Download Visual Studio <https://visualstudio.microsoft.com/de/downloads/>`_ and run installer.

* Under `Workloads` select 
   
  #. `Python development` and the optional `Python native development tools`.

  #. `Development for Universal Windows Platform` with latest `Windows 10 SDK`, `Windows 11 SDK`, `graphics debugger`, and `UWP-Tools for C++ (v142)` packages.

* Run the installation.

.. seealso:: `Windows compiler infos <https://wiki.python.org/moin/WindowsCompilers>`_


Install Conda
*************

.. admonition:: Conda license issue 

      Instead of anconda or miniconda you can also use miniforge3.
      Miniforge might be required due to license restrictions on conda for certain users.
      
      See `Anaconda terms of service <https://www.anaconda.com/blog/update-on-anacondas-terms-of-service-for-academia-and-research>`_

* `Download Conda <https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html#regular-installation>`_ and install *Miniconda* or *Anaconda* (or *Micromamba*).
  You can also use `miniforge <https://conda-forge.org/download/>`_ instead.

* On Windows it might be necessary to add the installation path, e.g. ``C:\miniconda3``, ``C:\miniconda3\condabin``, ``C:\miniconda3\Scripts`` and ``C:\miniconda3\Library\bin``, to the PATH variable of Windows. Similarly for *Anaconda* or *miniforge*.

.. seealso:: `Conda installation instructions <https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html>`_


Installing *Nexus*
******************

* `Download Nexus <https://gitlab.desy.de/fs-mcp/nuclear-nexus>`_ from the Gitlab project page.

* Open a command prompt. On Windows when *Conda* is installed for all users, you might need to run the command prompt as administrator.

* Navigate to the folder with the *Nexus* project and create a virtual environment for the *Nexus* installation.
  This is recommended as otherwise conflicts with package dependencies can occur. Type
  ``conda env create -n nexus-env --file environment.yml``.

* When you want to install the optional dependencies on *NLopt* and *Pagmo* (recommended), type
  ``conda install -n nexus-env -c conda-forge pagmo-devel nlopt``

.. note:: pagmo-devel 2.19 build 3 and 4 need boost-cpp in addition , so use ``conda install -n nexus-env -c conda-forge boost-cpp pagmo-devel nlopt`` instead.

* Use ``conda activate nexus-env`` or ``activate nexus-env`` depending on the *Conda* version.

* And install via
  ``python -m pip install .``

Always activate the *Nexus* environment in the command prompt either via ``conda activate nexus-env`` or ``activate nexus-env`` when you want to use *Nexus*.
To remove the environment use ``conda remove -n nexus-env --all``.


Option 3: Compiling *Nexus* manually
------------------------------------

You can also compile *Nexus* manually.
However, you must deal with all of its dependencies.

You need a fully C++17 compliant compiler.
The Windows wheels are created with Microsoft Visual Studio Community 2019 - Version 16 and the Python package *setuptools*.
On Windows, make sure to have the correct SDK installed.

.. seealso:: `Windows compiler infos <https://wiki.python.org/moin/WindowsCompilers>`_

Download and install CMake 3.10 or later, as well as SWIG 4.0 or later. 

In case you have the *Intel one API Base toolkit* you can also use the BLAS libraries from the *Intel MKL* in the *Ceres solver*.
Keep in mind that if you want to distribute your build every machine has to have the *oneAPI toolkit* installed.
I do not recommend to do so as it will not give any benefits.

You need the following packages or later versions of it.
Earlier versions have not been tested.

* Eigen 3.3
* glog 0.5.0
* Ceres solver 2.0.0, only this version will work.
* Nlopt 2.7.1 (optional)
* boost 1.78 (optional)
* Pagmo 2.18 (optional)

.. warning:: In case you use later versions of the Ceres solver as 2.0.0 you might get a compiler error due to changes in the ceres API.

In order to get everything running these packages have to be compiled. Please follow the instructions given here and look to the corresponding documentation.
In case you want to distribute the build, all compiled dlls have to be provided via the wheel package.

*Pagmo* requires the *Intel oneAPI TBB*.
You can download it either as stand-alone or as part of the *Intel oneAPI Base toolkit*.
Install and restart.
Actually only the ``tbb12.dll`` is needed.

It is helpful to create one folder where all *Nexus* dependencies are stored.
For example, ``..\mathlibs`` and create one folder for each of the packages, e.g. ``..\mathlibs\glog``.

For glog, all steps needed are given.
For the following packages only the CMake options are specified that have to be changed.
All other steps are similar, just change the appropriate package names.
For *Eigen* don't build the project.


Glog
****

* Create a ``glog`` folder and unzip the glog download to that folder.
* In CMake choose the ``glog\glog-master`` folder as source code folder.
* Create a folder ``glog\cmake_build`` and in CMake choose this folder as the folder where to build the binaries.
* In CMake press configure, choose x64 configure option.
* Check advanced options.
* Set ``CMAKE_INSTALL_PREFIX`` to your ``glog`` folder.
* Uncheck ``WITH_GFLAGS``.
* Press configure again, create and open project.
* In the MSVC choose ``Release`` configuration and ``x64``.
* In MSVC solution explorer right-click on ``ALL_BUILD`` and build.
* If finished right- click on ``INSTALL`` and build.
* In the ``glog`` folder you should have three new folders ``glog\bin``, ``glog\include``, and ``glog\lib``.


Eigen
*****

Create folders similar to glog and use CMake.

* Create an ``eigen`` folder and unzip the *Eigen* folder. You get a folder ``eigen-X.X.X``.
* In CMake choose this folder a source. Create a ``eigen\cmake_build`` folder and choose this folder as folder to build in.
* In case you have a BLAS library, like MKL, you can build against this lib. But there is no gain in speed for the matrix sizes used in *Nexus*.
  Therefore, *Nexus* is set up such that it is not used anyway. It just creates additional unneeded dependencies.
* Press configure again and create. Do not open the project.


Ceres
*****

Create folders similar to glog and use CMake.

* Uncheck ``GLFAGS`` if checked.
* Set ``CERES_THREADING_MODEL`` to ``NO_THREADS``.
* Set ``Eigen3_DIR`` to ``..\eigen\cmake_build``.
* Set ``glog_DIR`` to ``..\glog\cmake_build``.
* Check ``BUILD_SHARED_LIB``.
* Set ``CMAKE_INSTALL_PREFIX`` to ``ceres`` folder.
* If you want to you can build against a BLAS library like Intel MKL. However, it is not needed.

Configure, create and compile similar to *glog*.


NLopt (optional)
****************

By default *Nexus* will build with *NLopt*.
 
If you do not want to use *NLopt* you can undefine the compiler option ``-DNEXUS_USE_NLOPT`` in the ``setup_nexus.py`` file.
Also delete the NLopt dependencies in the ``setup_nexus.py`` file.

Create folders similar to glog and use CMake.

* Set ``CMAKE_INSTALL_PREFIX`` to your ``nlopt`` folder.

Configure, create and compile similar to glog.


Boost for Pagmo (optional)
**************************

By default *Nexus* will build against *Pagmo*.
If you do not want to use *Pagmo* you don't need boost at all.

*boost* is required for *Pagmo* and just needs the following few steps.

* Create a ``boost`` folder and unzip the download.
* In the ``boost X.X.X`` folder open a command line.
* In the command line input ``bootstrap``.
* In the command line input ``.\b2 --build-type=minimal stage``. This step will take some time.


Pagmo (optional)
****************

By default *Nexus* will build with *Pagmo*.
If you do not want to use *Pagmo* you can undefine the compiler option ``-DNEXUS_USE_PAGMO`` in the ``setup_nexus.py`` file.
Also delete the *Pagmo* dependencies in the ``setup_nexus.py`` file.

In case you want to build against *Pagmo*, create folders similar to glog and use CMake.

* Set the correct ``TBB_DIR``. Should be detected automatically.
* Set the correct ``boost_DIR``. Something like ``..\boost_1_78_0\stage\lib\cmake\Boost-1.78.0``, depending on your version.
* Set the correct ``boos_INCLUDE_DIR``. Something like ``..\boost_1_78_0\stage\lib``, depending on your version.
* Set ``CMAKE_INSTALL_PREFIX`` to ``pagmo`` folder.
* Check ``PAGMO_WITH_EIGEN3``.
* Check ``PAGMO_WITH_NLOPT``. (optional)
* Configure
* Set the ``Eigen_DIR`` to ``..\eigen\cmake_build``.
* Set the ``Nlopt_DIR`` to ``..\nlopt\cmake_build``.

Configure, create and compile similar to glog.


Setuptools
**********

In the *Nexus* folder is the ``setup_nexus.py`` file.
In this file change folders for the packages to the correct paths.

In case you have compiled *Eigen* or the *Ceres solver* with MKL support, the libraries have to be implemented as well.
Uncomment the needed lines for MKL support in the file and adjust to your installation.

Run the ``setup_nexus.py`` file.

.. note:: Make sure that all dlls and all required software tools are available in the ``PATH`` variable of Windows.


Installing jupyter
------------------

Jupyter is an interactive computing platform. It has two similar modules: notebook or jupyterlab.
You can use both like a digital labbook for your *Nexus* calculations.

.. seealso:: `Jupyter project <https://jupyter.org/>`_

In order to install jupyter notebook use the command

* ``python -m pip install notebook``.

For jupyter lab use

* ``python -m pip install jupyterlab``.

To enbale interactive matplotlib plots install ipympl via

* ``python -m pip install ipympl``.
