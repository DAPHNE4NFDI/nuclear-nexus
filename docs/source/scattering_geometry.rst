.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. _scattering-geometry:


Scattering geometry
===================

The scattering geometry in *Nexus* is defined with respect to the photon coordinate system

.. math:: \begin{bmatrix}
           \sigma \\
           \pi \\
           k
          \end{bmatrix}

where :math:`\sigma` and :math:`\pi` are the two independent linear polarization axes of the photon and :math:`k` is the k-vector of the photon.

Forward scattering
------------------

A forward scattering geometry corresponds to an extended sample in the plane of the polarization axes.

.. image:: forward.png
     :align: center

|

Grazing incidence scattering
----------------------------

A grazing-incidence scattering geometry corresponds to an extended sample (at an incidence angle of 0 degree) in the plane spanned by :math:`k` and an arbitrary axis in the polarization plane.
The direction of the second axis in the polarization plane is not important as gracing incidence scattering is formally treated like forward scattering in the implemented theory.
The formalism is therefore only valid at small incidence angles.
The typical scattering geometry in an experiment is shown here

.. image:: grazing.png
     :align: center

|

One could also rotate the sample around :math:`k` by any angle.

Polarization angles
-------------------

The beam and analyzer are described by the mixing angle (ellipticity) and the canting angle (rotation).
Their defintions (since version 2.0.0) are shown in the figure.

.. image:: polarization_angles.png
     :scale: 80%
     :align: center

|

Hyperfine Interactions
----------------------

The hyperfine fields are also defined with respect to the photon system. 

Magnetic hyperfine field
************************

For the magnetic field its field strength and two angles are given.
The polar angle :math:`\vartheta` is defined from the :math:`k` vector.
The azimuthal angle :math:`\varphi` is defined in the :math:`\sigma - \pi` plane from the :math:`\sigma` direction.

.. image:: coordinates.png
     :align: center



Electric field gradient
***********************

The electric field gradient tensor is defined by the largest principal tensor component :math:`V_{zz}`, the asymmetry parameter :math:`\eta`, and three Euler angles.
In *Nexus*, instead of :math:`V_{zz}` the quadrupole splitting :math:`QS` in mm/s is given. The angles are the extrinsic Euler angles in ZYZ convention. 
The angles are thus defined as rotations around the internal fixed-frame coordinate system of *Nexus*.


.. seealso:: :ref:`tut-hyperfine`.

.. note:: The definitions in *Nexus* differ from the ones in the different CONUSS modules.






