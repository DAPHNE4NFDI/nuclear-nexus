﻿.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _sec-bibliography:

Bibliography
============

The theory of nuclear resonant scattering and the calculations of scattering matrices can be found in [Sturhahn]_ and references therein.
A throughout textbook introduction and the theory of grazing incidence scattering is given in [Roehlsberger]_.


References
----------

.. [Baron96] A.Q.R. Baron et al., Phys. Rev. Lett. 77, 4808 (1996).

.. [Baron99] A.Q.R. Baron, Hyperfine Interactions 123/124, 667 (1999).

.. [Born] M. Born and E. Wolf, *Principles of Optics*, Cambridge University Press, 7th edition, 2002.

.. [Biscani] F. Biscani and D. Izzo, *A parallel global multiobjective framework for optimization: pagmo*, J. Open Source Softw. 5, 2338 (2020).

.. [CXRO] https://henke.lbl.gov/optical_constants/asf.html

.. [Drago] R.S. Drago, *Physical Methods for Chemists*, 2nd edition, Surfside Scientific Publishers, Gainesville (1992).

.. [Gil] J.J. Gil and R. Ossikovski, *Polarized Light and the Mueller Matrix Approach*, CRC Press, 2nd edition, 2022.

.. [Gonser] U. Gonser, Moessabuer Spectroscopy, Springer Berlin (1975).

.. [Hasselbach] K. M. Hasselbach and H. Spiering, *The average over a sphere*, Nuc. Instr. and Meth. 176, 537 (1980). 

.. [Henke] B. L. Henke, E. M. Gullikson, and J. C. Davis, Atomic Data and Nuclear Data Tables 54, 181 (1993).

.. [Josephson] B. D. Josephson, *Temperature-Dependent Shift of :math:`\gamma` Rays Emitted by a Solid*, Phys. Rev. Lett. 4, 341 (1960).

.. [KleinNishina] O. Klein and Y. Nishina, *Uber die Streuung von Strahlung durch freie Elektronen nach der neuen relativistischen Quantendynamik von Dirac*, Z. Phys. 52, 853 (1929).

.. [Nasu] S. Nasu and Y. Murakamdi, *Second-Order Doppler Shift of Fe :sup:`57` Moessbauer Resonance in Al-0.01% Fe :sup:`57` Alloy Solid Solution*, phys. stat. sol. (b) 46, 711 (1971).

.. [Odstrcil] M. Odstrcil et al., *Iterative least-squares solver for generalized maximum-likelihood ptychography*, Opt. Express 26, 3108 (2018).

.. [Rose] M.E. Rose, *Elementary Theory of Angular Momentum*, Dover Books on Physics (New York and London: J. Wiley and Sons), (1995).

.. [Roehlsberger] R. Roehlsberger, *Nuclear Condensed Matter Physics with Synchrotron Radiation*, Springer, Berlin, 2005.

.. [Smirnov] G.V. Smirnov, *General properties of nuclear resonant scattering*, Hyperfine Interactions 123/124, 31 (199).

.. [Smith] D.L. Smith, *Empirical Formula For Interpolation Of Tabulated Photon Photoelectric Cross Sections*, National Technical Information Service, 1972.

.. [Sturhahn] W. Sturhahn and E. Gerdau, *Evaluation of time-differential measurements of nuclear-resonance scattering of x rays*, Phys. Rev. B 49, 9285 (1994).

.. [Sturhahn2000] W. Sturhahn, *Evaluation of nuclear resonant scattering data*, Hyperfine Interactions 125, 149 (2000).

.. [Shvydko] Y.V. Shvydko, *Nuclear resonant forward scattering of x rays: Time and space picture*, Physical Review B 59, 9132 (1999).

.. [Tanaka] K. Tanaka et al., *Second‐order Doppler shift in Moessbauer spectra of Fe-B and Fe-P amorphous alloys*, J. Appl. Phys. 64, 3299 (1988).

.. [Thibault] P. Thibault and M. Guizar-Sicairos, *Maximum-likelihood refinement for coherent diffractive imaging*, New J. Phys. 14, 063004 (2012).

.. [Varshalovich] D. A. Varshalovich, A. N. Moskalev, V. K. Khersonskii, *Quantum Theory of Angular Momentum*, World Scientific, Singapore, 1988

.. [Wagner] F. E. Wagner, *Moessbauer spectroscopy with 191/193 Ir*, Hyperfine Interactions 13, 149 (1983).
