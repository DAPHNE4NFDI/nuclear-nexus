.. Nexus - the Nuclear Elastic X-ray scattering Universal Software package
   
   Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
   A research centre of the Helmholtz Association.
   All rights reserved.
   
   Author: Lars Bocklage - lars.bocklage@desy.de
   
   This file is part of Nexus.
   
   Nexus is free software; you can redistribute it and/or modify it under the terms of the
   GNU General Public License as published by the Free Software Foundation,
   either version 3 of the License, or (at your option) any later version.
   
   Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License along with Nexus.
   If not, see <https://www.gnu.org/licenses/>.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. _examples:


Examples
========

Here you can find some notebooks of evaluation examples.
The examples are created from simulations or some are measured data.
They all serve the purpose to have running evaluation script to start from.


Evaluation of a Moessbauer spectrum
-----------------------------------

The following jupyter notebooks show how to properly calibrate a spectrum from a iron foil reference and how to evaluate a Moessbauer spectrum.

The files needed are :download:`Fe_calibration <Moessbauer_Spectrum/Fe_calibration.dat>` and :download:`sample_spectrum <Moessbauer_Spectrum/sample_spectrum.dat>`. 


You should first determine the calibration values and obtain the calibrated velocity file from this notebook

`calibration`_ - :download:`Moessbauer_Spectrum/nb_velocity_calibration.ipynb`.

.. _calibration: Moessbauer_Spectrum/nb_velocity_calibration.ipynb

and then use the calibration to evaluate the specturm like this

`moessbauer_fit`_ - :download:`Moessbauer_Spectrum/nb_fit_sample.ipynb`.

.. _moessbauer_fit: Moessbauer_Spectrum/nb_fit_sample.ipynb


Evaluation of a time spectrum
-----------------------------

This notebook shows how to evaluate a basic time spectrum from a stainless steel foil.
It evaluates the data with a local search algorithm.

The file needed is :download:`foil data <Time_Spectrum/time_spectrum_stainless_steel_foil.dat>`.

`time_spectrum_fit`_ - :download:`Time_Spectrum/time_spectrum_fit.ipynb`.

.. _time_spectrum_fit: Time_Spectrum/time_spectrum_fit.ipynb


Evaluation of a reflectivity
----------------------------

This notebook shows how to evaluate a measured reflectivity of an approximately 100 nm thick SiO2 film.
It evaluates the data with a global search algorithm, which is the first choice for a reflectivity fit.

The file needed is :download:`SiO2_refl <reflectivity/SiO2_refl.txt>`.

`reflectivity_fit`_ - :download:`reflectivity/SiO2_reflectivity.ipynb`.

.. _reflectivity_fit: reflectivity/SiO2_reflectivity.ipynb
