# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Pre-defined distribution derived from the parent class :class:`Distribution`.
"""

import numpy as np

from nexus import clib

# only use distributions for single quantities, like absolute values or one angle.
# Combined angular distributions will work but not for real random angular distributions that depend on several angles
# angular 2D and 3D random distributions are implemented in hyperfine.cpp

# always use self.xxx for fit_variables

# Random distribution 
class Random(clib.Distribution):
    r"""
    Random distribution in the range of width with weight of one.

    Args:
       points (int): Number of points in the distribution
       width (float or :class:`Var`): width of the distribution in absolute values
    """
    def __init__(self, points, width):
        super().__init__(points, "Random")
        if isinstance(width, (int, float)):
            width = clib.Var(width, 0, np.inf, False, "Random width")
        self.width = width
        self.rng = np.random.default_rng()  # numpy random generator
        self.fit_variables = [width]
 
    def __setattr__(self, attr, val):
        nexusVars = ("width")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "width: {}\n".format(self.width)
        return output

    def DistributionFunction(self):
        x = np.array(self.SetRange(self.width.value))
        self.SetDelta(self.rng.random(x.size) * self.width.value - self.width.value/2)
        weight = np.ones(x.size)
        self.SetWeight(weight)


# Rectangle distribution 
class Rectangle(clib.Distribution):
    r"""
    Equally spaced distribution in the range of width with values of one.

    Args:
       points (int): Number of points in the distribution
       width (float or :class:`Var`): width of the distribution in absolute values
    """
    def __init__(self, points, width):
        super().__init__(points, "Rectangle")
        if isinstance(width, (int, float)):
            width = clib.Var(width, 0, np.inf, False, "Rect width")
        self.width = width
        self.fit_variables = [width]

    def __setattr__(self, attr, val):
        nexusVars = ("width")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "width: {}\n".format(self.width)
        return output

    def DistributionFunction(self):
        x = np.array(self.SetRange(self.width.value))
        #weight = np.where(np.abs(x) > self.width.value/2, 0*x, 1) # 0*x is needed, otherwise not broadcastable
        weight = np.ones(x.size)
        self.SetWeight(weight)


# Gaussian distribution
class Gaussian(clib.Distribution):
    r"""
    Gaussian distribution with the given FWHM.

    see `<https://en.wikipedia.org/wiki/Gaussian_function>`_

    Args:
       points (int): Number of points in the distribution.
       fwhm (float or :class:`Var`): FWHM of the Gaussian distribution.
    """
    def __init__(self, points, fwhm):
        super().__init__(points, "Gaussian")
        if isinstance(fwhm, (int, float)):
            fwhm = clib.Var(fwhm, 0, np.inf, False, "Gauss FWHM")
        self.fwhm = fwhm
        self.fit_variables = [fwhm]

    def __setattr__(self, attr, val):
        nexusVars = ("fwhm")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "FWHM: {}\n".format(self.fwhm)
        return output

    def DistributionFunction(self):
        sigma = self.fwhm.value * 0.424660900144009521361  # 1 / (1/(2 * sqrt(2* log(2))))
        if sigma == 0:
            sigma = 1e-299
        x = np.array(self.SetRange(6 * sigma))
        weight = np.exp(-0.5 * np.square(x / sigma))
        self.SetWeight(weight)

    """ Returns:
            float: The the sigma value of the Gaussian
    """
    def GetSigma(self):
        return self.fwhm.value * 0.424660900144009521361


# Positive value sonly Gaussian distribution
class PosGaussian(clib.Distribution):
    r"""
    Gaussian distribution with the given FWHM and cut to positive values only.

    Args:
       points (int): Number of points in the distribution.
       fwhm (float or :class:`Var`): FWHM of the Gaussian distribution.
       target_var (:class:`Var`): Target :class:`Var`.
    """
    def __init__(self, points, fwhm, target_var):
        super().__init__(points, "Gaussian")
        if isinstance(fwhm, (int, float)):
            fwhm = clib.Var(fwhm, 0, np.inf, False, "Gauss FWHM")
        if not isinstance(target_var, (clib.Var)):
            raise Exception("\n- NEXUS WARNING in PosGaussian distribution class - target_var must be of Var type.\n\n")
        self.target_var = target_var
        self.fwhm = fwhm
        self.fit_variables = [fwhm]

    def __setattr__(self, attr, val):
        nexusVars = ("fwhm")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "FWHM: {}\n".format(self.fwhm)
        return output

    def DistributionFunction(self):
        sigma = self.fwhm.value * 0.424660900144009521361  # 1 / (1/(2 * sqrt(2* log(2))))
        if sigma == 0:
            sigma = 1e-299
        x = np.array(self.SetRange(6 * sigma))
        weight = np.exp(-0.5 * np.square(x / sigma))
        weight = np.where(x + self.target_var.value < 0, 0, weight)           
        self.SetWeight(weight)

# Negative Gaussian distribution
class NegGaussian(clib.Distribution):
    r"""
    Gaussian distribution with the given FWHM and cut to negative values only.

    Args:
       points (int): Number of points in the distribution.
       fwhm (float or :class:`Var`): FWHM of the Gaussian distribution.
       target_var (:class:`Var`): Target :class:`Var`.
    """
    def __init__(self, points, fwhm, target_var):
        super().__init__(points, "Gaussian")
        if isinstance(fwhm, (int, float)):
            fwhm = clib.Var(fwhm, 0, np.inf, False, "Gauss FWHM")
        if not isinstance(target_var, (clib.Var)):
            raise Exception("\n- NEXUS WARNING in NegGaussian distribution class - target_var must be of Var type.\n\n")
        self.target_var = target_var
        self.fwhm = fwhm
        self.fit_variables = [fwhm]

    def __setattr__(self, attr, val):
        nexusVars = ("fwhm")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "FWHM: {}\n".format(self.fwhm)
        return output

    def DistributionFunction(self):
        sigma = self.fwhm.value * 0.424660900144009521361  # 1 / (1/(2 * sqrt(2* log(2))))
        if sigma == 0:
            sigma = 1e-299
        x = np.array(self.SetRange(6 * sigma))
        weight = np.exp(-0.5 * np.square(x / sigma))
        weight = np.where(x + self.target_var.value > 0, 0, weight)           
        self.SetWeight(weight)

# Asymetric Gaussian distribution
class AsymmetricGaussian(clib.Distribution):
    r"""
    Asymmetric Gaussian distribution with lower and higher HWHMs.

    Args:
       points (int): Number of points in the distribution.
       hwhm_low (float or :class:`Var`): HWHM of the Gaussian distribution for the lower values.
       hwhm_high (float): HWHM of the Gaussian distribution for the higher values.
    """
    def __init__(self, points, hwhm_low, hwhm_high):
        super().__init__(points, "Asymmetric Gaussian")
        if isinstance(hwhm_low, (int, float)):
            hwhm_low = clib.Var(hwhm_low, 0, np.inf, False, "AsymGauss HWHM low")
        if isinstance(hwhm_high, (int, float)):
            hwhm_high = clib.Var(hwhm_high, 0, np.inf, False, "AsymGauss HWHM high")
        self.hwhm_low = hwhm_low
        self.hwhm_high = hwhm_high
        self.fit_variables = [hwhm_low, hwhm_high]

    def __setattr__(self, attr, val):
        nexusVars = ("hwhm_low", "hwhm_high")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "HWHM_low: {}\n".format(self.hwhm_low)
        output += "HWHM_high: {}\n".format(self.hwhm_high)
        return output

    def DistributionFunction(self):
        sigma_low = 2 * self.hwhm_low.value * 0.424660900144009521361
        if sigma_low == 0:
            sigma_low = 1e-299
        sigma_high = 2 * self.hwhm_high.value * 0.424660900144009521361
        if sigma_high == 0:
            sigma_high = 1e-299
        x = np.array(self.SetRange(6 * max(sigma_low, sigma_high)))
        weight_low = np.exp(-0.5 * np.square(x / sigma_low))
        weight_high = np.exp(-0.5 * np.square(x / sigma_high))
        weight = np.where(x < 0, weight_low, weight_high)
        self.SetWeight(weight)
    
    r"""
    Calculate the FWHM from the HWHMs.

    Returns:
       float: FWHM of the asymmetric Gaussian
    """
    def GetFWHM(self):
        return self.hwhm_low.value + self.hwhm_high.value

    r"""
    Calculate the corresponding sigma value from the lower HWHM.

    Returns:
       float: Sigma value corresponding to the lower HWHM.
    """
    def GetSigmaLow(self):
       return 2* self.hwhm_low.value * 0.424660900144009521361

    r"""
    Calculate the corresponding sigma value from the higher HWHM.
        
    Returns:
       float: Sigma value corresponding to the higher HWHM.
    """
    def GetSigmaHigh(self):
       return 2 * self.hwhm_high.value * 0.424660900144009521361

    r""" Returns:
            float: Sigma value corresponding to the FWHM
    """
    def GetSigma(self):
       return GetFWHM() * 0.424660900144009521361

# Double Gaussian distribution
class DoubleGaussian(clib.Distribution):
    r"""
    Double Gaussian distribution with the given two FWHMs offset to each other and with a relative weight.

    Args:
       points (int): Number of points in the distribution.
       fwhm1 (float or :class:`Var`): FWHM of the first Gaussian distribution around the zero position.
       fwhm2 (float or :class:`Var`): FWHM of the second Gaussian distribution.
       offset2 (float or :class:`Var`): offset of the second Gaussian distributions.
       weight2 (float or :class:`Var`): relative weight of the second Gaussian distributions.
    """
    def __init__(self, points, fwhm1, fwhm2, offset2, weight2):
        super().__init__(points, "Double Gaussian")
        if isinstance(fwhm1, (int, float)):
            fwhm1 = clib.Var(fwhm1, 0, np.inf, False, "DoubGauss FWHM 1")
        if isinstance(fwhm2, (int, float)):
            fwhm2 = clib.Var(fwhm2, 0, np.inf, False, "DoubGauss FWHM 2")
        if isinstance(offset2, (int, float)):
            offset2 = clib.Var(offset2, 0, np.inf, False, "DoubGauss offset2")
        if isinstance(weight2, (int, float)):
            weight2 = clib.Var(weight2, 0, np.inf, False, "DoubGauss weight2")
        self.fwhm1 = fwhm1
        self.fwhm2 = fwhm2
        self.offset2 = offset2
        self.weight2 = weight2
        self.fit_variables = [fwhm1, fwhm2, offset2, weight2]

    def __setattr__(self, attr, val):
        nexusVars = ("fwhm1", "fwhm2", "offset2", "weight2")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "FWHM 1: {}\n".format(self.fwhm1)
        output += "FWHM 2: {}\n".format(self.fwhm2)
        output += "offset of FWHM 2: {}\n".format(self.offset2)
        output += "weight of FWHM 2: {}\n".format(self.weight2)
        return output

    def DistributionFunction(self):
        sigma1 = self.fwhm1.value * 0.424660900144009521361
        if sigma1 == 0:
            sigma1 = 1e-299
        sigma2 = self.fwhm2.value * 0.424660900144009521361
        if sigma2 == 0:
            sigma2 = 1e-299
        x = np.array(self.SetRange(3 * (max(sigma1, sigma2) + self.offset2.value)))
        x = np.add(x, self.offset2.value * 0.5)
        gauss1 = np.exp(-0.5 * np.square(x / sigma1))
        gauss2 = self.weight2.value * np.exp(-0.5 * np.square((x - self.offset2.value) / sigma2))
        weight = gauss1 + gauss2
        self.SetWeight(weight)


# Lorentzian distribution
class Lorentzian(clib.Distribution):
    r"""
    Lorentzian distributions with the given FWHM.

    see `<https://en.wikipedia.org/wiki/Cauchy_distribution>`_

    Args:
       points (int): Number of points in the distribution.
       fwhm (float or :class:`Var`): FWHM of the Lorentzian distribution.
    """
    def __init__(self, points, fwhm):
        super().__init__(points, "Lorentzian")
        if isinstance(fwhm, (int, float)):
            fwhm = clib.Var(fwhm, 0, np.inf, False, "Lorentz FWHM 1")
        self.fwhm = fwhm
        self.fit_variables = [fwhm]

    def __setattr__(self, attr, val):
        nexusVars = ("fwhm")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "FWHM: {}\n".format(self.fwhm)
        return output

    def DistributionFunction(self):
        sigma = self.fwhm.value * 0.5
        if sigma == 0:
            sigma = 1e-299
        x = np.array(self.SetRange(12 * sigma))
        weight = 1 / (1 + np.square(x / sigma))
        self.SetWeight(weight)

    r""" 
    Calculate sigma from the FWHM.

    Returns:
       float: Sigma value corresponding to the FWHM
    """
    def GetSigma(self):
        return self.fwhm.value * 0.5


# Log distribution
class Log(clib.Distribution):
    r"""
    Log distributions with the given sigma value.

    see `<https://en.wikipedia.org/wiki/Log-normal_distribution>`_

    Args:
       points (int): Number of points in the distribution.
       sigma (float or :class:`Var`): Sigma of the Log distribution.
    """
    def __init__(self, points, sigma):
        super().__init__(points, "Log")
        if isinstance(sigma, (int, float)):
            sigma = clib.Var(sigma, 0, np.inf, False, "Log Sigma")
        self.sigma = sigma
        self.fit_variables = [sigma]

    def __setattr__(self, attr, val):
        nexusVars = ("sigma")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "sigma: {}\n".format(self.sigma)
        return output

    def DistributionFunction(self):
        x = self.SetRange(12 * self.sigma.value)
        temp = np.add(x, np.max(x) + 1.0e-15)
        self.SetDelta(temp)
        weight = np.exp(-0.5 * np.square(np.log(temp) / self.sigma.value))
        self.SetWeight(weight)

    """ Returns:
            float: FWHM corresponding to sigma
    """
    def GetFWHM(self):
        return np.exp(-self.sigma.value**2 + np.sqrt(2 * self.sigma.value**2 * np.log(2))) - np.exp(-self.sigma.value**2 - np.sqrt(2 * self.sigma.value**2 * np.log(2)))


# Logistic distribution
class Logistic(clib.Distribution):
    r"""
    Logistic distributions with the given sigma value.

    see `<https://en.wikipedia.org/wiki/Logistic_distribution>`_

    Args:
       points (int): Number of points in the distribution.
       sigma (float or :class:`Var`): Sigma value of the Logistic distribution.
    """
    def __init__(self, points, sigma):
        super().__init__(points, "Logistic")
        if isinstance(sigma, (int, float)):
            sigma = clib.Var(sigma, 0, np.inf, False, "Logistic Sigma")
        self.sigma = sigma
        self.fit_variables = [sigma]

    def __setattr__(self, attr, val):
        nexusVars = ("sigma")
        if attr in nexusVars:
            if isinstance(val, (int, float)):
              setattr(getattr(self, attr), "value", val)
              return
        super().__setattr__(attr, val)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        output += "sigma: {}\n".format(self.sigma)
        return output

    def DistributionFunction(self):
        x = np.array(self.SetRange(6 * self.sigma.value))
        weight = np.exp(-x / self.sigma.value) / np.square(1 + np.exp(-x / self.sigma.value))
        self.SetWeight(weight)


# Distribution from two arrays
class Array(clib.Distribution):
    r"""
    Distributions from two arrays.

    Args:
       delta (list or ndarray): Array of :attr:`delta` values.
       weight (list or ndarray): Array of :attr:`weight` values.
    """
    def __init__(self, values, weight):
        super().__init__(len(values), "array dist")
        self.SetDelta(values)
        self.SetWeight(weight)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        return output

    def DistributionFunction(self):
        pass


# Distribution from file
# load a file with to two columns and rows > 1
# has no fit parameters
# value in hyperfine site must be set to zero if absolute values are given in file
# or 
# value in hyperfine site is set and the values are given as absolute deltax values
class File(clib.Distribution):
    r"""
    Distributions from a file. :attr:`delta` values in column 1. Relative :attr:`weight` in column 2.

    Args:
       filename (string): filename (with path) to be loaded by ``numpy.loadtxt(filename)``.
    """
    def __init__(self, filename):
        self.filename = filename
        self.data = np.loadtxt(filename)
        if len(self.data.shape) != 2:  # check array dimension
            raise Exception("\n- NEXUS WARNING in DistributionFile class - wrong data format in " + self.filename + ". No class instance initialized.\n\n")
        elif self.data.shape[0] < 1 or self.data.shape[1] != 2: # check for rows and columns
            raise Exception("\n- NEXUS WARNING in DistributionFile class - wrong data format in " + self.filename + ". No class instance initialized.\n\n")
        else:
            self.values = self.data[:, 0]
            self.weights = np.array(self.data[:,1] / np.sum(self.data[:, 1]))
            super().__init__(len(self.values), filename)
            self.SetDelta(self.values)
            self.SetWeight(self.weights)

    def __repr__(self):
        output = "Distribution id: {}\n".format(self.id)
        output += "points: {}\n".format(self.points)
        return output

    def DistributionFunction(self):
        pass
