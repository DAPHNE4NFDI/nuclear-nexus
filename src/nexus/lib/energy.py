# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Common X-ray energies of nuclear transitions and K-alpha transition lines.
"""

from nexus.lib import moessbauer

# all values given (eV)

CuKalpha = 8047.8
r"""Cu K alpha transition energy (eV)"""

FeKalpha = 6399.8
r"""Fe K alpha transition energy (eV)"""

CoKalpha = 6925.7
r"""Co K alpha transition energy (eV)"""

MoKalpha = 17450.0
r"""Mo K alpha transition energy (eV)"""

Fe57 = moessbauer.Fe57.energy
r"""Fe-57 transition energy (eV)"""

Sn119 = moessbauer.Sn119.energy
r"""Sn-119 transition energy (eV)"""

Dy161 = moessbauer.Dy161.energy
r"""Dy-161 transition energy (eV)"""

Eu151 = moessbauer.Eu151.energy
r"""Eu-151 transition energy (eV)"""

Gd157 = moessbauer.Gd157.energy
r"""Gd-157 transition energy (eV)"""

Ta181 = moessbauer.Ta181.energy
r"""Ta-181 transition energy (eV)"""

Tm169 = moessbauer.Tm169.energy
r"""Tm-169 transition energy (eV)"""

Ir193 = moessbauer.Ir193.energy
r"""Ir-193 transition energy (eV)"""

Sm149 = moessbauer.Sm149.energy
r"""Sm-149 transition energy (eV)"""

K40 = moessbauer.K40.energy
r"""K-40 transition energy (eV)"""

Ge73 = moessbauer.Ge73.energy
r"""Ge-73 transition energy (eV)"""

Sb121 = moessbauer.Sb121.energy
r"""
Sb-121 transition energy (eV)

.. versionadded:: 1.0.3
"""

Te125 = moessbauer.Te125.energy
r"""
Te-125 transition energy (eV)

.. versionadded:: 1.0.3
"""

Sc45 = moessbauer.Sc45.energy
r"""
Sc-45 transition energy (eV)

.. versionadded:: 1.0.3
"""