# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
The :mod:`material` module serves as a library for standard materials which are instances of the :class:`Material` class.
Almost all elements as well as typical alloys and compounds are covered in the library.
Literature values are used for the material composition and density.
For materials with a Moessbauer isotope, also the natural abundance and the Lamb Moessbauer factor (at room temperature, when available) are set properly.

A material can be accessed via ``nx.lib.materials.Name``.
The ``.Name`` for elements is the element symbol, e.g. for carbon its ``nx.lib.material.C`` and so on.

For isotope material often an enriched version is defined as ``Name_enriched``, e.g. ``nexus.material_lib.Fe_enriched``.

Alloys can be accessed by their name or a typical abbreviation.
For example, permalloy can be called  via ``nexus.lib.material.permalloy`` or ``nexus.lib.material.Py``.

Compounds can be accessed via their material composition.
For example, silicon nitride is referenced as ``nexus.Materials.Si3N4``.

.. note:: Defining a material with a :class:`Material` from :mod:`material` should always done by the method
          :attr:`Material.Template()`.
          
          .. code-block:: 

             my_material = nx.Material.Template(nx.lib.material.Name)
"""

from nexus import clib
from nexus.lib import moessbauer

# material is string identifier

#SINGLE ELEMENTS

Li = clib.Material(
    id = "Li",
    composition = [("Li", 1)],
    density = clib.Var(value=0.534, min = 0, max = 0.534),
    )
r"""Lithium"""

Be = clib.Material(
    id = "Be",
    composition = [("Be", 1)],
    density = clib.Var(value=1.85, min = 0, max = 1.85),
    )
r"""Beryllium"""

B = clib.Material(
    id = "B",
    composition = [("B", 1)],
    density = clib.Var(value=2.08, min = 0, max = 2.08),
    )
r"""Boron"""

C = clib.Material(
    id = "C",
    composition = [("C", 1)],
    density = clib.Var(value=2.25, min = 0, max = 2.25),
    )
r"""Carbon"""

Na = clib.Material(
    id = "Na",
    composition = [("Na", 1)],
    density = clib.Var(value=0.968, min = 0, max = 0.968),
    )
r"""Sodium"""

Mg = clib.Material(
    id = "Mg",
    composition = [("Mg", 1)],
    density = clib.Var(value=1.738, min = 0, max = 1.738),
    )
r"""Magnesium"""

Al = clib.Material(
    id = "Al",
    composition = [("Al", 1)],
    density = clib.Var(value = 2.7, min = 0, max = 2.7),
    )
r"""Aluminum"""

Si = clib.Material(
    id = "Si",
    composition = [("Si", 1)],
    density = clib.Var(value = 2.336, min = 0, max = 2.336),
    )
r"""Silicon"""

K = clib.Material(
    id = "K",
    composition = [("K", 1)],
    density = clib.Var(value = 0.89, min = 0, max = 0.89),
    isotope = moessbauer.K40,
    abundance = clib.Var(value = 0.0001171, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.0, min = 0, max = 1)
    )
r"""Potassium"""

Ca = clib.Material(
    id = "Ca",
    composition = [("Ca", 1)],
    density = clib.Var(value = 1.55, min = 0, max = 1.55),
    )
r"""Calcium"""

Sc = clib.Material(
    id = "Sc",
    composition = [("Sc", 1)],
    density = clib.Var(value = 2.985, min = 0, max = 2.985),
    isotope = moessbauer.Sc45,
    abundance = clib.Var(value = 1.0, min = 0.0, max = 1.0),
    lamb_moessbauer = clib.Var(value = 0.837, min = 0.0, max = 1.0)
    )
r"""Scandium"""

V = clib.Material(
    id = "V",
    composition = [("V", 1)],
    density = clib.Var(value = 6.11, min = 0, max = 6.11),
    )
r"""Vanadium"""

Cr = clib.Material(
    id = "Cr",
    composition = [("Cr", 1)],
    density = clib.Var(value = 7.19, min = 0, max = 7.19),
    )
r"""Chromium"""

Mn = clib.Material(
    id = "Mn",
    composition = [("Mn", 1)],
    density = clib.Var(value = 7.21, min = 0, max = 7.21),
    )
r"""Manganese"""

Fe = clib.Material(
    id = "Fe",
    composition = [("Fe", 1)],
    density = clib.Var(value = 7.874, min = 0, max = 7.874),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.02119, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.796, min = 0, max = 1)
    )
r"""Iron"""

Fe_enriched = clib.Material(
    id = "57-Fe",
    composition = [("Fe", 1)],
    density = clib.Var(value = 7.874, min = 0, max = 7.874),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.95, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.796, min = 0, max = 1)
    )
r"""Iron enriched to 95%"""

Co = clib.Material(
    id = "Co",
    composition = [("Co", 1)],
    density = clib.Var(value = 8.90, min = 0, max = 8.90),
    )
r"""Cobalt"""

Ni = clib.Material(
    id = "Ni",
    composition = [("Ni", 1)],
    density = clib.Var(value = 8.908, min = 0, max = 8.908),
    isotope = moessbauer.Ni61,
    abundance = clib.Var(value = 0.011399, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.0007719, min = 0, max = 1)
    )
r"""Nickel"""

Cu = clib.Material(
    id = "Cu",
    composition = [("Cu", 1)],
    density = clib.Var(value = 8.96, min = 0, max = 8.96),
    )
r"""Copper"""

Zn = clib.Material(
    id = "Zn",
    composition = [("Zn", 1)],
    density = clib.Var(value = 7.14, min = 0, max = 7.14),
    isotope = moessbauer.Zn67,
    abundance = clib.Var(value = 0.0404, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 5.15e-12, min = 0, max = 1)
    )
r"""Zinc"""

Ga = clib.Material(
    id = "Ga",
    composition = [("Ga", 1)],
    density = clib.Var(value = 5.91, min = 0, max = 5.91),
    )
r"""Gallium"""

Ge = clib.Material(
    id = "Ge",
    composition = [("Ge", 1)],
    density = clib.Var(value = 5.323, min = 0, max = 5.323),
    isotope = moessbauer.Ge73,
    abundance = clib.Var(value = 0.0776, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.8416, min = 0, max = 1)
    )
r"""Germanium"""

As = clib.Material(
    id = "As",
    composition = [("As", 1)],
    density = clib.Var(value = 5.727, min = 0, max = 5.727),
    )
r"""Arsenic"""

Rb = clib.Material(
    id = "Rb",
    composition = [("Rb", 1)],
    density = clib.Var(value = 1.532, min = 0, max = 1.532),
    )
r"""Rubidium"""

Sr = clib.Material(
    id = "Sr",
    composition = [("Sr", 1)],
    density = clib.Var(value = 2.64, min = 0, max = 2.64),
    )
r"""Strontium"""

Y = clib.Material(
    id = "Y",
    composition = [("Y", 1)],
    density = clib.Var(value = 4.472, min = 0, max = 4.472),
    )
r"""Yttrium"""

Zr = clib.Material(
    id = "Zr",
    composition = [("Zr", 1)],
    density = clib.Var(value = 6.52, min = 0, max = 6.52),
    )
r"""Zirconium"""

Nb = clib.Material(
    id = "Nb",
    composition = [("Nb", 1)],
    density = clib.Var(value = 8.57, min = 0, max = 8.57),
    )
r"""Niobium"""

Mo = clib.Material(
    id = "Mo",
    composition = [("Mo", 1)],
    density = clib.Var(value = 10.28, min = 0, max = 10.28),
    )
r"""Molybdenum"""

Tc = clib.Material(
    id = "Tc",
    composition = [("Tc", 1)],
    density = clib.Var(value = 11, min = 0, max = 11),
    )
r"""Technetium"""

Ru = clib.Material(
    id = "Ru",
    composition = [("Ru", 1)],
    density = clib.Var(value = 12.45, min = 0, max = 12.45),
    isotope = moessbauer.Ru99,
    abundance = clib.Var(value = 0.1276, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.033, min = 0, max = 1)
    )
r"""Ruthenium"""

Rh = clib.Material(
    id = "Rh",
    composition = [("Rh", 1)],
    density = clib.Var(value = 12.41, min = 0, max = 12.41),
    )
r"""Rhodium"""

Pd = clib.Material(
    id = "Pd",
    composition = [("Pd", 1)],
    density = clib.Var(value = 12.023, min = 0, max = 12.023),
    )
r"""Palladium"""

Ag = clib.Material(
    id = "Ag",
    composition = [("Ag", 1)],
    density = clib.Var(value = 10.49, min = 0, max = 10.49),
    )
r"""Silver"""

Cd = clib.Material(
    id = "Cd",
    composition = [("Cd", 1)],
    density = clib.Var(value = 8.65, min = 0, max = 8.65),
    )
r"""Cadmium"""

In = clib.Material(
    id = "In",
    composition = [("In", 1)],
    density = clib.Var(value = 7.31, min = 0, max = 7.31),
    )
r"""Indium"""

Sn = clib.Material(
    id = "Sn",
    composition = [("Sn", 1)],
    density = clib.Var(value = 6.99, min = 0, max = 6.99),
    isotope = moessbauer.Sn119,
    abundance = clib.Var(value = 0.0859, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.04, min = 0, max = 1)
    )
r"""Tin"""

Sn_enriched = clib.Material(
    id = "Sn resonant",
    composition = [("Sn", 1)],
    density = clib.Var(value = 6.99, min = 0, max = 6.99),
    isotope = moessbauer.Sn119,
    abundance = clib.Var(value = 0.97, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.04, min = 0, max = 1)
    )
r"""Tin enriched to 97%"""

Sb = clib.Material(
    id = "Sb",
    composition = [("Sb", 1)],
    density = clib.Var(value = 6.697, min = 0, max = 6.697),
    isotope = moessbauer.Sb121,
    abundance = clib.Var(value = 0.5725, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.057, min = 0, max = 1)
    )
r"""Antimony"""

Te = clib.Material(
    id = "Te",
    composition = [("Te", 1)],
    density = clib.Var(value = 6.24, min = 0, max = 6.24),
    isotope = moessbauer.Te125,
    abundance = clib.Var(value = 0.0699, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.0081, min = 0, max = 1)
    )
r"""Tellurium"""

I = clib.Material(
    id = "I",
    composition = [("I", 1)],
    density = clib.Var(value = 4.933, min = 0, max = 4.933),
    isotope = moessbauer.I127,
    abundance = clib.Var(value = 1, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.0, min = 0, max = 1)
    )
r"""Iodine"""

Sm = clib.Material(
    id = "Sm",
    composition = [("Sm", 1)],
    density = clib.Var(value = 7.518, min = 0, max = 7.536),
    isotope = moessbauer.Sm149,
    abundance = clib.Var(value = 0.1382, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.327, min = 0, max = 1)
    )
r"""
Samarium

versionadded: 

"""

Ta = clib.Material(
    id = "Ta",
    composition = [("Ta", 1)],
    density = clib.Var(value = 16.69, min = 0, max = 16.69),
    isotope = moessbauer.Ta181,
    abundance = clib.Var(value = 0.99988, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.966, min = 0, max = 1)
    )
r"""Tantalum"""

W = clib.Material(
    id = "W",
    composition = [("W", 1)],
    density = clib.Var(value = 19.3, min = 0, max = 19.3),
    )
r"""Tungsten"""

Ir = clib.Material(
    id = "Ir",
    composition = [("Ir", 1)],
    density = clib.Var(value = 22.56, min = 0, max = 22.56),
    isotope = moessbauer.Ir193,
    abundance = clib.Var(value = 0.627, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.038, min = 0, max = 1)
    )
r"""Iridium"""

Pt = clib.Material(
    id = "Pt",
    composition = [("Pt", 1)],
    density = clib.Var(value = 21.45, min = 0, max = 21.45),
    )
"""Platinum"""

Au = clib.Material(
    id = "Au",
    composition = [("Au", 1)],
    density = clib.Var(value = 19.3, min = 0, max = 19.3),
    )
r"""Gold"""

Dy = clib.Material(
    id = "Dy",
    composition = [("Dy", 1)],
    density = clib.Var(value = 8.6, min = 0, max = 8.6),
    isotope = moessbauer.Dy161,
    abundance = clib.Var(value = 0.18889, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.2605, min = 0, max = 1)  # Debye Temp 183 K
    )
r"""Dysprosium"""

Eu = clib.Material(
    id = "Eu",
    composition = [("Eu", 1)],
    density = clib.Var(value = 5.245, min = 0, max = 5.245),
    isotope = moessbauer.Eu151,
    abundance = clib.Var(value = 0.47816, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.087, min = 0, max = 1)   # Debye Temp 118 K
    )
r"""Europium"""


# COMPOUNDS & ALLOYS

vacuum = clib.Material(
    id = "vacuum",
    composition = [("N", 1)],
    density = clib.Var(value = 1e-299, min = 0, max = 2e-299),
    )
r"""Vacuum"""


air = clib.Material(
    id = "Air",
    composition = [("N", 1.562), ("O", 0.42), ("C", 0.0003), ("Ar", 0.0094)],
    density = clib.Var(value = 0.0012041, min = 0, max = 0.0012041),
    )
r"""Air"""

Py = clib.Material(
    id = "permalloy Ni80Fe20 wt%",
    composition = [("Ni", 79.2), ("Fe", 20.8)], # at%
    density = clib.Var(value = 8.7, min = 0, max = 8.7),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.02, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.796, min = 0, max = 1)
    )
r"""permalloy"""

permalloy = Py.Copy()

Py_enriched = clib.Material(
    id = "57-permalloy Ni80Fe20 wt%",
    composition = [("Ni", 79.2), ("Fe", 20.8)], # at%
    density = clib.Var(value = 8.7, min = 0, max = 8.7),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.95, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.76, min = 0, max = 1)
    )
r"""permalloy enriched in iron to 95%"""

permalloy_enriched = Py_enriched.Copy()

SS = clib.Material(
    id = "stainless steel Fe55Cr25Ni20 wt%",
    composition = [("Fe", 54.52), ("Cr", 26.62), ("Ni", 18.86),], # at%
    density = clib.Var(value = 7.8, min = 0, max = 7.8),
    )
r"""Stainless steel"""

stainless_steel = SS.Copy()

SS_enriched = clib.Material(
    id = "57-stainless steel Fe55Cr25Ni20 wt%",
    composition = [("Fe", 54.52), ("Cr", 26.62), ("Ni", 18.86),], # at%
    density = clib.Var(value = 7.8, min = 0, max = 7.8),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.95, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.76, min = 0, max = 1)
    )
r"""Stainless steel enriched in iron to 95%"""

stainless_steel_enriched = SS_enriched.Copy()

# CARBIDES

B4C = clib.Material(
    id = "B4C boron carbide",
    composition = [("B", 4), ("C", 1)],
    density = clib.Var(value = 2.52, min = 0, max = 2.52),
    )
r"""Boron carbide"""

SiC = clib.Material(
    id = "SiC silicon carbide",
    composition = [("Si", 1), ("C", 1)],
    density = clib.Var(value = 3.16, min = 0, max = 3.16),
    )
r"""Silicon carbide"""

WC = clib.Material(
    id = "WC tungsten carbide",
    composition = [("W", 1), ("C", 1)],
    density = clib.Var(value = 15.63, min = 0, max = 15.63),
    )
r"""Tungsten carbide"""

# OXIDES

Al2O3 = clib.Material(
    id = "Al2O3 sapphire",
    composition = [("Al", 2), ("O", 3)],
    density = clib.Var(value = 3.98, min = 0, max = 3.98),
    )
r"""Sapphire"""

sapphire = Al2O3.Copy()

MgO = clib.Material(
    id = "MgO magnesium oxide",
    composition = [("Mg", 1), ("O", 1)],
    density = clib.Var(value = 3.6, min = 0, max = 3.6),
    )
r"""Magnesium oxide"""

SiO2 = clib.Material(
    id = "SiO2 silicon dioxide",
    composition = [("Si", 1), ("O", 2)],
    density = clib.Var(value = 2.648, min = 0, max = 2.648),
    )
r"""Silicon dioxide"""

Fe2O3 = clib.Material(
    id = "Fe2O3 hematite",
    composition = [("Fe", 2), ("O", 3)],
    density = clib.Var(value = 5.25, min = 0, max = 5.25),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.02, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.793, min = 0, max = 1)
    )
"""hematite"""

hematite = Fe2O3.Copy()

Fe2O3_enriched = clib.Material(
    id = "57-Fe2O3 hematite",
    composition = [("Fe", 2), ("O", 3)],
    density = clib.Var(value = 5.25, min = 0, max = 5.25),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.95, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.793, min = 0, max = 1)
    )
r"""hematite enriched in iron to 95%"""

hematite_enriched = Fe2O3_enriched.Copy()

Fe3O4 = clib.Material(
    id = "Fe3O4 magnetite",
    composition = [("Fe", 3), ("O", 4)],
    density = clib.Var(value = 5.0, min = 0, max = 5.0),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.02, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.796, min = 0, max = 1)
    )
r"""magnetite"""

magnetite = Fe3O4.Copy()

Fe3O4_enriched = clib.Material(
    id = "Fe3O4 magnetite",
    composition = [("Fe", 3), ("O", 4)],
    density = clib.Var(value = 5.0, min = 0, max = 5.0),
    isotope = moessbauer.Fe57,
    abundance = clib.Var(value = 0.95, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.796, min = 0, max = 1)
    )
r"""magnetite enriched in iron to 95%"""

magnetite_enriched = Fe3O4_enriched.Copy()

Ta2O5 = clib.Material(
    id = "Ta2O5 tantalum pentoxide",
    composition = [("Ta", 2), ("O", 5)],
    density = clib.Var(value = 8.37, min = 0, max = 8.37),
    )
r""" Tantalum pentoxide"""

WO = clib.Material(
    id = "WO tungsten oxide",
    composition = [("W", 1), ("O", 1)],
    density = clib.Var(value = 7.16, min = 0, max = 7.16),
    )
r"""Tungsten oxide"""

SnO = clib.Material(
    id = "SnO stannous oxide",
    composition = [("Sn", 1), ("O", 1)],
    density = clib.Var(value = 6.45, min = 0, max = 6.45),
    isotope = moessbauer.Sn119,
    abundance = clib.Var(value = 0.086, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.27, min = 0, max = 1)
    )
r"""Tin oxide, stannous oxide"""

SnO_enriched = clib.Material(
    id = "SnO stannous oxide enriched",
    composition = [("Sn", 1), ("O", 1)],
    density = clib.Var(value = 6.45, min = 0, max = 6.45),
    isotope = moessbauer.Sn119,
    abundance = clib.Var(value = 0.97, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.27, min = 0, max = 1)
    )
r"""Tin oxide, stannous oxide, enriched in tin to 97%"""

SnO2 = clib.Material(
    id = "SnO2 stannic oxide",
    composition = [("Sn", 1), ("O", 2)],
    density = clib.Var(value = 6.95, min = 0, max = 6.95),
    isotope = moessbauer.Sn119,
    abundance = clib.Var(value = 0.086, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.6289, min = 0, max = 1)
    )
r"""Tin dioxide, stannic oxide,"""

SnO2_enriched = clib.Material(
    id = "SnO2 stannic oxide enriched",
    composition = [("Sn", 1), ("O", 2)],
    density = clib.Var(value = 6.95, min = 0, max = 6.95),
    isotope = moessbauer.Sn119,
    abundance = clib.Var(value = 0.97, min = 0, max = 1),
    lamb_moessbauer = clib.Var(value = 0.6289, min = 0, max = 1)
    )
r"""Tin dioxide, stannic oxide, enriched in tin to 97%"""

# NITRIDES

BN = clib.Material(
    id = "BN boron nitride",
    composition = [("B", 1), ("N", 1)],
    density = clib.Var(value = 2.1, min = 0, max = 2.1),
    )
r"""boron nitride"""

Si3N4 = clib.Material(
    id = "Si3N4 silicon nitride",
    composition = [("Si", 3), ("N", 4)],
    density = clib.Var(value = 3.17, min = 0, max = 3.17),
    )
r""" Silicon nitride"""
