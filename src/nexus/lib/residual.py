# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.


"""
Predefined residuals derived from the parent class :class:`Residual`.
"""

import numpy as np
from scipy.special import factorial

from nexus import clib


class Sqrt(clib.Residual):
    r"""
    Difference of the square root values
    
    .. math::

       residual = \sqrt{data} - \sqrt{theory}

    This an approximation of the Poisson likelihood function [Thibault]_ 
    
    .. math::

      \mathcal{L}_p \approx 2 \sum_i \left( \sqrt{data_i} - \sqrt{theory_i}) \right)^2

    and the :math:`cost` is

    .. math:: 

      cost = \frac{1}{2} \sum_i \left( \sqrt{data_i} - \sqrt{theory_i}) \right)^2

    Both only differ by a scaling factor that does not affect fitting.
    
    It is the standard residual for most :class:`FitMeasurement` types (if the residual parameter is set to ``None``).

    """
    def __init__(self, id = "Sqrt", exponent = 2, plot_string = r"$\sqrt{y} - \sqrt{\hat{y}}$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        sqrt_data = [np.sqrt(x) if x>=0.0 else 0.0 for x in input_data]
        sqrt_theory = [np.sqrt(x) if x>=0.0 else 0.0 for x in input_theory]
        return np.subtract(sqrt_data, sqrt_theory)


class StdDev(clib.Residual):
    r"""
    Difference residual weighted by the standard deviation
    
    .. math::

       residual = \frac{data - theory}{\sqrt{data}}

    For histograms the variance is the measured data value :math:`V = data` and the standard deviation is given by :math:`\sigma = \sqrt(V) = \sqrt(data)`.
    The best linear unbiased estimator for Gaussian noise is obtained by using the variance as a weight in the squared residual.
    This will result in the cost function
    
    .. math::

       cost = norm \sum_i \frac{r_i^2}{V_i} = norm \sum_i \left(\frac{data_i- theory_i}{\sqrt(data_i)}\right)^2

    This is a very typical cost function for data fitting.
    However, for Poisson statistics and low count rates a Poisson likelihood function is the better choice.

    .. seealso:: `<https://en.wikipedia.org/wiki/Weighted_least_squares>`_

    """
    def __init__(self, id = "StdDev", exponent = 2, plot_string = r"$\dfrac{y - \hat{y}}{\sqrt{x}}$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        sqrt_data = [np.sqrt(x) if x>0.0 else 1.0 for x in input_data]
        return np.subtract(input_data, input_theory) / sqrt_data


class SqrtStdDev(clib.Residual):
    r"""
    Difference of the square root values weighted by the standard deviation
    
    .. math::

       residual = \frac{\sqrt{data} - \sqrt{theory}}{\sqrt{data}}

    This gives the amplitude log-likelihood [Odstrcil]_

    .. math::

      cost = \mathcal{L}_a = \frac{1}{2} \sum_i \left(\frac{\sqrt{data_i} - \sqrt{theory_i}) }{\sqrt{data_i}}\right)^2

    .. versionadded:: 1.2.0

    """
    def __init__(self, id = "SqrtStdDev", exponent = 2, plot_string = r"$\dfrac{\sqrt{y} - \sqrt{\hat{y}}}{\sqrt{y}}$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        sqrt_data = [np.sqrt(x) if x>1.0 else 1.0 for x in input_data]
        sqrt_theory = [np.sqrt(x) if x>1.0 else 1.0 for x in input_theory]
        return np.subtract(sqrt_data, sqrt_theory) / sqrt_data


class Difference(clib.Residual):
    r"""
    Difference residual
    
    .. math::
    
       residual = data - theory

    """
    def __init__(self, id = "Difference", exponent = 2, plot_string = r"$y - \hat{y}$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        return np.subtract(input_data, input_theory)


class Norm(clib.Residual):
    r"""
    Normalized difference residual
    
    .. math::

       residual = \frac{data - theory}{\sum data}

    """
    def __init__(self, id = "Norm", exponent = 2, plot_string = r"\dfrac{y - \hat{y}}{\sum y}"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        sum = np.sum(input_data)
        if sum == 0.0:
            sum = 1.0
        return np.subtract(input_data, input_theory) / sum


class Log10(clib.Residual):
    r"""
    Difference of the logarithm of base 10 values

    .. math::

       residual = \log_{10}(data) - \log_{10}(theory)

    """
    def __init__(self, id = "Log10", exponent = 2, plot_string = r"$\log_{10}(y) - \log_{10}(\hat{y})$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        log_data = [np.log10(x) if x>0.0 else 1e-16 for x in input_data]
        log_theory = [np.log10(x) if x>0.0 else 1e-16 for x in input_theory]
        return np.subtract(log_data, log_theory)


class Log(clib.Residual):
    r"""
    Difference of the natural logarithm values

    .. math::

       residual = \log(data) - \log(theory)

    """
    def __init__(self, id = "Log", exponent = 2, plot_string = r"$\ln(y) - \ln(\hat{y})$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        ln_data = [np.log(x) if x>0.0 else 1e-16 for x in input_data]
        ln_theory = [np.log(x) if x>0.0 else 1e-16 for x in input_theory]
        return np.subtract(ln_data, ln_theory)


class LogLikelihood(clib.Residual):
    r"""
    The residual is given by [Odstrcil]_

    .. math::

       residual = - \left( data * ln(theory) - theory \right)

    and gives the Poisson log-likelihood 

    .. math::

       \mathcal{L}_p = -\sum_i data_i * log(theory_i) - theory_i

    Note, that this residual is not working with gradient based methods of the ceres solver (``LevMar``, ``DogLeg``, ``SubDogLeg`` and ``LineSearch``).
    In addition, the fit ``options.error_method`` must be ``None`` or ``Bootstrap``.

    For global fit methods, set the local method to ``Subplex`` or ``Newuoa``.

    In combined fitting all measurements need to have this residual, when used.

    .. versionadded:: 2.0.0

    """
    def __init__(self, id = "LogLikelihood", exponent = 1, plot_string = r"$-(y\, \log(\hat{y}) - \hat{y})$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        log_theory = np.where(np.array(input_theory) > 0, np.log(input_theory), 1e-16)
        return -np.subtract(np.array(input_data) * log_theory, input_theory)
