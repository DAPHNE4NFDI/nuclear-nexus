# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Predefined Moessbauer isotopes.
Instances of ``nexus.MoessbauerIsotope`` class.
"""

from nexus import clib, cnexus

#none definition of an isotope
none = clib.MoessbauerIsotope(
  isotope = "none",
  element = "none",
  mass = 0.0,
  
  energy = 0.0,
  lifetime = 0.0,
  
  internal_conversion = 0.0,
  multipolarity = cnexus.Multipolarity_E1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 0,
  spin_excited = 0,
  
  gfactor_ground = 0.0,
  gfactor_excited = 0.0,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = 0.0,

  interference_term = 0.0,

  natural_abundance = 0.0
  )
r"""
none
"""



# K-40
K40 = clib.MoessbauerIsotope(
  isotope = "40-K",
  element = "K",
  mass = 39.96399848,
  
  energy = 29834,
  lifetime = 5.96e-9,
  
  internal_conversion = 6.6,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 4,
  spin_excited = 3,
  
  gfactor_ground = -0.3245,
  gfactor_excited = -0.4,
  
  quadrupole_ground = -0.061,
  quadrupole_excited = 1.0e-200,  # unkwonwn

  interference_term = 0.0,

  natural_abundance = 1.17e-4
  )
r"""
K-40

.. warning:: Quadrupole of excited state unknown. Set to 1e-200. Do not calculate with quadrupole interaction.
"""


# Sc-45
Sc45 = clib.MoessbauerIsotope(
  isotope = "45-Sc",
  element = "Sc",
  mass = 44.95591276,
  
  energy = 12389.59,
  lifetime = 460e-3,
  
  internal_conversion = 632,
  multipolarity = cnexus.Multipolarity_M2,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 7/2,
  spin_excited = 3/2,
  
  gfactor_ground = 1.359,
  gfactor_excited = 0.245333,
  
  quadrupole_ground = -0.22,
  quadrupole_excited = 0.318,

  interference_term = 0.0,

  natural_abundance = 1.0
  )
r"""
Sc-45

.. versionadded:: 1.0.3
"""



# Fe-57
Fe57 = clib.MoessbauerIsotope(
  isotope = "57-Fe",
  element = "Fe",
  mass = 56.9353933,
  
  energy = 14412.497,
  lifetime = 141.11e-9,
  
  internal_conversion = 8.56, #8.21 in Pynuss and CONUSS, 8.56 in Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 1/2,
  spin_excited = 3/2,
  
  gfactor_ground = 0.18121,
  gfactor_excited = -0.10348,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = 0.187,

  interference_term = 0.0,

  natural_abundance = 0.02119
  )
r"""
Fe-57

.. versionchanged:: 1.2.0 internal conversion changed from 8.21 to 8.56

"""


# Ni-61
Ni61 = clib.MoessbauerIsotope(
  isotope = "61-Ni",
  element = "Ni",
  mass = 60.931056,
  
  energy = 67408.0,
  lifetime = 7.6e-9,
  
  internal_conversion = 0.139,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 3/2,
  spin_excited = 5/2,
  
  gfactor_ground = 0.5,
  gfactor_excited = 0.192,
  
  quadrupole_ground = 0.162,
  quadrupole_excited = -0.2,

  interference_term = 0.0,

  natural_abundance = 0.011399
  )
r"""
Ni-61

.. versionadded:: 1.0.4
"""


# Zn-67
Zn67 = clib.MoessbauerIsotope(
  isotope = "67-Zn",
  element = "Zn",
  mass = 66.927128,

  energy = 93312,
  lifetime = 1.309e-5,

  internal_conversion = 0.873,
  multipolarity = cnexus.Multipolarity_E2,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 5/2,
  spin_excited = 1/2,
  
  gfactor_ground = 0.8756/2.5,
  gfactor_excited = 0.587/0.5,
  
  quadrupole_ground = 0.15,
  quadrupole_excited = 0.0,

  interference_term = 0.0,

  natural_abundance = 0.0404
  )
r"""
Zn-67

.. versionadded:: 1.2.0

"""



# Ge-73 - 13.2 keV
Ge73 = clib.MoessbauerIsotope(
  isotope = "73-Ge",
  element = "Ge",
  mass = 72.9234589,
  
  energy = 13275,
  lifetime = 4.26e-6,
  
  internal_conversion = 1095,
  multipolarity = cnexus.Multipolarity_E2,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 9/2,
  spin_excited = 5/2,
  
  gfactor_ground = -0.19544444444444445,
  gfactor_excited = -0.03764 ,
  
  quadrupole_ground = -0.173,
  quadrupole_excited = -0.4,

  interference_term = 0.0,

  natural_abundance = 0.0776
  )
r"""
Ge-73
13.2 keV
"""


# Kr-83
Kr83 = clib.MoessbauerIsotope(
  isotope = "83-Kr",
  element = "Kr",
  mass = 82.914136,
  
  energy = 9396.0,
  lifetime = 212.08e-9,
  
  internal_conversion = 17.9,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 9/2,
  spin_excited = 7/2,
  
  gfactor_ground = -0.2157,
  gfactor_excited = -0.2694,
  
  quadrupole_ground = 0.253,
  quadrupole_excited = 0.495,

  interference_term = 0.0,

  natural_abundance = 0.115
  )
r"""
Kr-83

.. versionadded:: 1.0.4
"""


# Ru-99
Ru99 = clib.MoessbauerIsotope(
  isotope = "99-Ru",
  element = "Ru",
  mass = 98.9059393,
  
  energy = 89680,
  lifetime = 29.58e-9,
  
  internal_conversion = 1.498,
  multipolarity = cnexus.Multipolarity_M1E2,
  mixing_ratio_E2M1 = -1.65, # delta^2 = -2.72, see Terence C. Gibb et al., J. Chem. Soc., Dalton Trans., 1973, 1253-1258
  
  spin_ground = 5/2,
  spin_excited = 3/2,
  
  gfactor_ground = -0.641/2.5,
  gfactor_excited = -0.284/1.5,
  
  quadrupole_ground = 0.079,
  quadrupole_excited = 0.231,

  interference_term = 0.0,

  natural_abundance = 0.1276
  )
r"""
Ru-99

.. versionadded:: 1.2.0
"""


# Sn-119
Sn119 = clib.MoessbauerIsotope(
  isotope = "119-Sn",
  element = "Sn",
  mass = 118.9033115,
  
  energy = 23879.5,
  lifetime = 25.3e-9,
  
  internal_conversion = 5.22,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 1/2,
  spin_excited = 3/2,
  
  gfactor_ground = -1.04728/0.5,
  gfactor_excited = 0.633/1.5,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = -0.094,

  interference_term = 0.0,

  natural_abundance = 0.0859
  )
r"""
Sn-119
"""



# Sb-121
Sb121 = clib.MoessbauerIsotope(
  isotope = "121-Sb",
  element = "Sb",
  mass = 120.9038157,
  
  energy = 37133,
  lifetime = 4.99e-9,
  
  internal_conversion = 11.11,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 5/2,
  spin_excited = 7/2,
  
  gfactor_ground = 1.34536,
  gfactor_excited = 0.71942857142,
  
  quadrupole_ground = -0.36,
  quadrupole_excited = -0.48,

  interference_term = 0.0,

  natural_abundance = 0.5721
  )
r"""
Sb-121

.. versionadded:: 1.0.3
"""



# Te-125
Te125 = clib.MoessbauerIsotope(
  isotope = "125-Te",
  element = "Te",
  mass = 124.9044307,
  
  energy = 35491.9,
  lifetime = 2.14e-9,
  
  internal_conversion = 14.0,
  multipolarity = cnexus.Multipolarity_M1E2,
  mixing_ratio_E2M1 = 0.06016,   # check sign
  
  spin_ground = 1/2,
  spin_excited = 3/2,
  
  gfactor_ground = -1.777,
  gfactor_excited = 0.4033333333333,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = -0.31,

  interference_term = 0.0,

  natural_abundance = 0.0707
  )
r"""
Te-125

.. versionadded:: 1.0.3

Please check the sign of the mixing ratio
"""


# I-127
I127 = clib.MoessbauerIsotope(
  isotope = "127-I",
  element = "I",
  mass = 126.904472681,
  
  energy = 57606.0,
  lifetime = 2.76e-9,
  
  internal_conversion = 3.77,
  multipolarity = cnexus.Multipolarity_M1E2,
  mixing_ratio_E2M1 = -0.083,
  
  spin_ground = 5/2,
  spin_excited = 7/2,
  
  gfactor_ground = 1.125,
  gfactor_excited = 0.726,
  
  quadrupole_ground = -0.79,
  quadrupole_excited = -0.71,

  interference_term = 0.0,

  natural_abundance = 1.0
  )
r"""
I-127

.. versionadded:: 1.0.4
"""



# Sm-149
Sm149 = clib.MoessbauerIsotope(
  isotope = "149-Sm",
  element = "Sm",
  mass = 148.9171847,
  
  energy = 22494,
  lifetime = 10.27e-9,
  
  internal_conversion = 29.4,
  multipolarity = cnexus.Multipolarity_M1E2,
  mixing_ratio_E2M1 = 0.075,  # Inoyatov et al., Eur. Phys. J. A (2011) 47, 64 , could not find it there anymore # has to be checked in sign!!!!!
  
  spin_ground = 7/2,
  spin_excited = 5/2,
  
  gfactor_ground = -0.192,
  gfactor_excited = -0.2496,
  
  quadrupole_ground = 0.075,
  quadrupole_excited = 1.01,

  interference_term = 0.0,

  natural_abundance = 0.1382
  )
r"""
Sm-149
please check mixing_ratio_E2M1
"""



# Eu-151
Eu151 = clib.MoessbauerIsotope(
  isotope = "151-Eu",
  element = "Eu",
  mass = 150.91985026,
  
  energy = 21541.418,
  lifetime = 13.85e-9,
  
  internal_conversion = 28.6,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 5/2,
  spin_excited = 7/2,
  
  gfactor_ground = 1.388,
  gfactor_excited = 0.7402,
  
  quadrupole_ground = 0.903,
  quadrupole_excited = 1.28,

  interference_term = 0.0,

  natural_abundance = 0.4781
  )
r"""
Eu-151
"""



# Gd-157 - 63.9 keV
Gd157 = clib.MoessbauerIsotope(
  isotope = "157-Gd",
  element = "Gd",
  mass = 156.92396011,
  
  energy = 63917,
  lifetime = 663.64e-9,
  
  internal_conversion = 0.971,
  multipolarity = cnexus.Multipolarity_E1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 3/2,
  spin_excited = 5/2,
  
  gfactor_ground = -0.3398,
  gfactor_excited = -0.464,
  
  quadrupole_ground = 1.38,
  quadrupole_excited = 2.46,

  interference_term = 0.0,

  natural_abundance = 0.1565
  )
r"""
Gd-157
63.9 keV
"""



# Dy-161
Dy161 = clib.MoessbauerIsotope(
  isotope = "161-Dy",
  element = "Dy",
  mass = 160.9269334,
  
  energy = 25655,
  lifetime = 40.684e-9,
  
  internal_conversion = 2.35,
  multipolarity = cnexus.Multipolarity_E1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 5/2,
  spin_excited = 5/2,
  
  gfactor_ground = -0.192,
  gfactor_excited = 0.237,
  
  quadrupole_ground = 2.507,
  quadrupole_excited = 2.506,

  interference_term = 0.0,

  natural_abundance = 0.18889
  )
r"""
Dy-161
25.6 keV
"""



# Tm-169
Tm169 = clib.MoessbauerIsotope(
  isotope = "169-Tm",
  element = "Tm",
  mass = 168.93421429,
  
  energy = 8410,
  lifetime = 5.785e-9,
  
  internal_conversion = 297,
  multipolarity = cnexus.Multipolarity_M1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 1/2,
  spin_excited = 3/2,
  
  gfactor_ground = -0.464,
  gfactor_excited = 0.347,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = -1.3,

  interference_term = 0.0,

  natural_abundance = 1.0
  )
r"""
Tm-169
"""


# Yb-172
Yb172 = clib.MoessbauerIsotope(
  isotope = "172-Yb",
  element = "Yb",
  mass = 171.9363815,
  
  energy = 78742.7,
  lifetime = 2.38e-9,
  
  internal_conversion = 8.4,
  multipolarity = cnexus.Multipolarity_E2,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 0,
  spin_excited = 2,
  
  gfactor_ground = 0,
  gfactor_excited = 0.669/2,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = 2.16,

  interference_term = 0.0,

  natural_abundance = 0.21686
)
r"""
Yb-172

.. versionadded:: 1.2.0

"""


# Yb-174
Yb174 = clib.MoessbauerIsotope(
  isotope = "174-Yb",
  element = "Yb",
  mass = 173.9388621,
  
  energy = 76471,
  lifetime = 2.58e-9,
  
  internal_conversion = 9.43,
  multipolarity = cnexus.Multipolarity_E2,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 0,
  spin_excited = 2,
  
  gfactor_ground = 0,
  gfactor_excited = 0.676/2,
  
  quadrupole_ground = 0.0,
  quadrupole_excited = 2.12,

  interference_term = 0.0,

  natural_abundance = 0.32025
)
r"""
Yb-174

.. versionadded:: 1.2.0

"""


# Ta-181
Ta181 = clib.MoessbauerIsotope(
  isotope = "181-Ta",
  element = "Ta",
  mass = 180.94799645,
  
  energy = 6215.7,
  lifetime = 8728.3e-9,
  
  internal_conversion = 46,
  multipolarity = cnexus.Multipolarity_E1,
  mixing_ratio_E2M1 = 0.0,
  
  spin_ground = 7/2,
  spin_excited = 9/2,
  
  gfactor_ground = 0.6772857,
  gfactor_excited = 1.173,
  
  quadrupole_ground = 3.17,
  quadrupole_excited = 3.71,

  interference_term = -0.08,  # Tramell and Hannon, Phys.Rev. 180, 337 (1969)

  natural_abundance = 0.9998799
  )
r"""
Ta-181
"""



# Ir-193
Ir193 = clib.MoessbauerIsotope(
  isotope = "193-Ir",
  element = "Ir",
  mass = 192.9629273,
  
  energy = 73041,
  lifetime = 8.598e-9, # Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation: 8.79ns
  
  internal_conversion = 6.24,
  multipolarity = cnexus.Multipolarity_M1E2,
  mixing_ratio_E2M1 = 0.557, #-0.557,
  
  spin_ground = 3/2,
  spin_excited = 1/2,
  
  gfactor_ground = 0.10607,
  gfactor_excited = 1.009,
  
  quadrupole_ground = 0.78,
  quadrupole_excited = 0.0,

  interference_term = 0.0,

  natural_abundance = 0.627
  )
r"""
Ir-193
"""
