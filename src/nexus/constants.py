# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Module with constants available to the user in Nexus.
"""

import numpy as np


inf = np.inf
r"""
Definition of infinity, same as ``np.inf``
"""

Inf = np.inf
INF = np.inf


nan = np.nan
r"""
Definition of nan, same as ``np.nan``
"""

NaN = np.nan
NAN = np.nan


pi = np.pi
r"""
Definition of pi, same as ``np.pi``
"""

Pi = np.pi
PI = np.pi


SpeedOfLight = 299792458.0
r"""
speed of light in m/s
"""

PlanckConstant = 4.135667696e-15
r"""Planck constant in eV/Hz"""

mu0 = 4*np.pi*1e-7
r"""
vacuum magnetic permeability in H/m

.. versionadded:: 1.0.3
"""

e0 = 1/(mu0 * SpeedOfLight**2)
r"""
vacuum permittivity in F/m

.. versionadded:: 1.0.3
"""

H = 4.135667696e-15
r"""
Planck constant in eV/Hz

.. versionadded:: 1.0.3
"""

HBar = 6.582119569e-16
r"""
reduced Planck constant in eVs
"""

BohrMagneton = 5.7883818060e-5
r"""
Bohr magneton in eV/T

.. versionadded:: 1.0.3
"""

NuclearMagneton = 3.1524512550e-8
r"""
nuclear magneton in eV/T
"""

ElementaryCharge = 1.602176634e-19
r"""
elementary charge in coulombs

.. versionadded:: 1.0.3
"""

ElectronRadius = 2.8179403227e-15
r"""
classical electron radius in meter.
"""

ElectronMass = 510998.95
r"""
electron mass in eV/c\ :sup:`2`.
"""

ProtonMass = 938272088.16
r"""
proton mass in eV/c\ :sup:`2`.

.. versionadded:: 1.0.3
"""
NeutronMass = 939565420.52
r"""
neutron mass in eV/c\ :sup:`2`.

.. versionadded:: 1.0.3
"""

Na = 6.02214076e23
r"""
Avogadro constant, per mole.
"""

Boltzmann = 8.617333262e-5
r"""
Boltzmann constant in eV/K.
"""

ElectronG = -2.00231930436256
r"""
g factor of the electron.

.. versionadded:: 1.0.3
"""
