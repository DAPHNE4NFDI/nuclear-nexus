# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
r"""
Conversion functions for the internal nexus coordinate system to Euler angles in ZYZ convention.
The module is helpful to convert certain EFG directions to the proper Euler angles.

Nexus works with extrinsic ZYZ convention for the Euler angles.
All rotations take place around the fixed frame coordinate system of Nexus.
In extrinsic ZYZ convention the first angle alpha, is only needed to set the asymmetry, in contrast to CONUSS where it is gamma.
This reflects the fact, that the meaning of alpha and gamma are inverse for extrinsic and intrinsic ZYZ convention.
If you want to change to intrinsic ZYZ (like in CONUSS) interchange alpha and gamma angles.

The rotation matrix R rotates the vector v in the coordinate system to the new position :math:`w = R*v`.

R = R :sub:`x` * R :sub:`y` * R :sub:`z`
is a rotation matrix that may be used to represent a composition of extrinsic rotations about axes z, y, x, (in that order)
or a composition of intrinsic rotations about axes x-y′-z″ (in that order).

Conventions for ZYZ Euler angles by rotation matrices are:

* extrinsic R :sub:`ZYZ` = R :sub:`Z` (:math:`\\alpha`) * R :sub:`Y` (:math:`\\beta`) * R :sub:`Z` (:math:`\gamma`) 
  all rotations around the fixed frame system in the order z, y, z

* intrinsic R :sub:`Z''Y'Z` = R :sub:`Z''` (:math:`\gamma`) * R :sub:`Y'` (:math:`\\beta`) * R :sub:`Z` (:math:`\\alpha`)     
  all rotations about the rotated body system in the order Z , Y', Z''.
 
Note the changed order of angles :math:`\\alpha` and :math:`\gamma`.

The transformation matrix T describes the transformation from the EFG system (XYZ, EFG) to the fixed frame system (xyz, [:math:`\sigma`, :math:`\pi`, k]).
And thus is equal to R :sub:`ZYZ`.

    v :sub:`xyz` = R :sub:`ZYZ` * v :sub:`XYZ`
          = R :sub:`Z` (:math:`\\alpha`) * R :sub:`Y` (:math:`\\beta`) * R :sub:`Z` (:math:`\gamma`) * v :sub:`XYZ`

For the transformation from the fixed frame system to the body system, the matrix relation is

    v :sub:`XYZ` = (R :sub:`ZYZ` ) :sup:`T` * v :sub:`xyz`
          = R :sub:`Z` (:math:`\gamma`) :sup:`T` * R :sub:`Y` (:math:`\\beta`) :sup:`T` * R :sub:`Z` (:math:`\\alpha`) :sup:`T` * v :sub:`xyz`

.. seealso:: `<https://en.wikipedia.org/wiki/Euler_angles>`_
"""

import math
import numpy as np


# here we use array assignment (row, column), so np.array([[row1], [row2], ....])

def RotationX(angle):
  r"""
  Returns the rotation matrix for a rotation by angle around the X axis of the internal coordinate system.
  
  Args:
     angle (float): rotation angle.

  Returns:
     ndarray: 3x3 rotation matrix.
  """

  M = np.array([[1, 0, 0], [0, math.cos(angle), math.sin(angle)], [0, -math.sin(angle), math.cos(angle)]])

  return M


def RotationY(angle):
  r"""
  Returns the rotation matrix for a rotation by angle around the Y axis of the internal coordinate system.

  Args:
     angle (float): rotation angle.

  Returns:
     ndarray: 3x3 rotation matrix.
  """

  M = np.array([[math.cos(angle), 0, -math.sin(angle)], [0, 1, 0], [math.sin(angle), 0, math.cos(angle)]])

  return M
  

def RotationZ(angle):
  r"""
  Returns the rotation matrix for a rotation by angle around the Z axis of the internal coordinate system.

  Args:
     angle (float): rotation angle.

  Returns:
     ndarray: 3x3 rotation matrix.
  """ 

  M = np.array([[math.cos(angle), math.sin(angle), 0], [-math.sin(angle), math.cos(angle), 0], [0, 0, 1]])

  return M


def ZYZEulerToTransformationMatrixExtrinsic(alpha, beta, gamma):
  r"""
  Returns the 3x3 transformation matrix from extrinsic Euler angles of the EFG to rotate to internal coordinate system.
  Euler angles in extrinsic ZYZ convention.

  Args:
     alpha (float): Euler angle alpha
     beta (float): Euler angle beta
     gamma (float): Euler angle gamma

  Returns:
     ndarray: 3x3 transformations matrix.
  """

  T = np.matmul(RotationY(beta), RotationZ(gamma))
  T = np.matmul(RotationZ(alpha), T)

  return T


def ZYZEulerToTransformationMatrixIntrinsic(alpha, beta, gamma):
  r"""
  Returns the 3x3 transformation matrix from intrinsic Euler angles of the EFG to rotate to internal coordinate system.
  Euler angles in intrinsic ZYZ convention.
  
  Args:
     alpha (float): Euler angle alpha
     beta (float): Euler angle beta
     gamma (float): Euler angle gamma

  Returns:
     ndarray: 3x3 transformations matrix.
  """

  T = np.matmul(RotationY(beta), RotationZ(alpha))
  T = np.matmul(RotationZ(gamma), T)

  return T


#same as ZYZEulerToTransitionMatrixExtrinsic
def ZYZEulerToTransformationMatrix(alpha, beta, gamma):
    r"""
    Returns the 3x3 transformation matrix from extrinsic Euler angles of the EFG to rotate to internal coordinate system.
    Euler angles in extrinsic ZYZ convention. Same as :attr:`ZYZEulerToTransformationMatrixExtrinsic(alpha, beta, gamma)`.

    Args:
       alpha (float): Euler angle alpha
       beta (float): Euler angle beta
       gamma (float): Euler angle gamma

    Returns:
        ndarray: 3x3 transformations matrix.
    """

    t00 = math.cos(alpha) * math.cos(beta) * math.cos(gamma) - math.sin(alpha) * math.sin(gamma)
    t10 = math.cos(alpha) * math.sin(gamma) + math.cos(beta) * math.cos(gamma) * math.sin(alpha)
    t20 = -math.cos(gamma) * math.sin(beta)

    t01 = -math.cos(gamma) * math.sin(alpha) - math.cos(alpha) * math.cos(beta) * math.sin(gamma)
    t11 = math.cos(alpha) * math.cos(gamma) - math.cos(beta) * math.sin(alpha) * math.sin(gamma)
    t21 = math.sin(beta) * math.sin(gamma)
    
    t02 = math.cos(alpha) * math.sin(beta)
    t12 = math.sin(alpha) * math.sin(beta)  
    t22 = math.cos(beta)

    T = np.array([[t00 , t10, t20], [t01, t11, t21], [t02, t12, t22]])

    return T


def TransformationMatrixToZYZEuler(T):
  r"""
  Returns the Euler angles (rad) from a 3x3 transformation matrix between two coordinate systems.
  Calculates the Euler angles with respect to the internal coordinate system [x,y,z] = [sigma,pi,k].

  Args:
      T (ndarray): 3x3 numpy array.

  Returns:
        float, float, float: alpha, beta, gamma
  """

  if T.shape != (3,3):
    raise NameError('Matrix must be 3x3')

  if T[2, 2] == 1:
    alpha = 0
    beta = 0
    gamma = math.acos(T[0, 0])
  elif T[2, 2] == -1:
    alpha = 0
    beta = math.pi
    gamma = -math.acos(T[0, 0])
  else:
    alpha = math.atan2(T[1,2], T[0,2])
    #beta = math.atan2(math.sqrt(T[0,2] * T[0,2] + T[1,2] * T[1,2]), T[2,2]) # can also be math.sqrt(1 - T[2,2]*T[2,2])
    beta = math.atan2(math.sqrt(1 - T[2,2] * T[2,2]), T[2,2])
    gamma = math.atan2(T[2,1], -T[2,0])

  return alpha, beta, gamma


def TransformationMatrixToEFGvectors(T):
  r"""
  Returns the column vectors from the transformation matrix T.
  Input the transformation matrix of the EFG components from ``ZYZEulerToTransformationMatrix`` to obtain Vxx, Vyy, Vzz.
      
  Args:
      T (ndarray): 3x3 transformation matrix.

  Returns:
        ndarray, ndarray, ndarray: Vxx, Vyy, Vzz (row vectors)
  """  

  if T.shape != (3,3):
    raise NameError('Matrix must be 3x3')

  return T[0], T[1], T[2]


def AnglesToVectors(alpha, beta, gamma):
  r"""
  Calculates the EFG vectors corresponding to the Euler angles in extrinsic ZYZ convention.
  Input the extrinsic ZYZ Euler angles to obtain Vxx, Vyy, Vzz.
      
  Returns:
      ndarray, ndarray, ndarray: Vxx, Vyy, Vzz (row vectors)
  """ 

  T = ZYZEulerToTransformationMatrix(alpha, beta, gamma)

  return T[0], T[1], T[2]


def EFGvectorToZYZEuler(Vzz):
  if len(Vzz) != 3:
    raise Exception("\n- NEXUS WARNING in EFGvectorToZYZEuler - Vector has to have 3 components.\n\n")
    return math.nan, math.nan, math.nan

  Vzz /= np.linalg.norm(Vzz)
  Vyy = np.random.randn(3)
  Vyy -= Vyy.dot(Vzz) * Vzz # make orthogonal
  Vyy /= np.linalg.norm(Vyy)
  Vxx = np.cross(Vzz, Vyy)
  efg_transformation_matrix = np.array([Vxx, Vyy, Vzz])

  return TransformationMatrixToZYZEuler(efg_transformation_matrix)


def EFGvectorsToZYZEuler(Vxx,Vyy,Vzz):
  if len(Vxx) != 3 or len(Vyy) != 3 or len(Vzz) != 3 :
    raise Exception("\n- NEXUS WARNING in EFGvectorsToZYZEuler - Vectors have to have 3 components.\n\n")
    return math.nan, math.nan, math.nan

  Vxx /= np.linalg.norm(Vxx)
  Vyy /= np.linalg.norm(Vyy)
  Vzz /= np.linalg.norm(Vzz)
  abs = np.abs(np.dot(np.cross(Vxx, Vyy), Vzz))
  allowed_error = 1e-4
  if (np.dot(Vxx, Vyy) > allowed_error or np.dot(Vxx, Vzz) > allowed_error or np.dot(Vyy, Vzz) > allowed_error or abs > 1+allowed_error or abs < 1-allowed_error):
    raise Exception("\n- NEXUS WARNING in EFGvectorsToZYZEuler - EFG vectors are not orthogonal.\n\n")
    return math.nan, math.nan, math.nan
  efg_transformation_matrix = np.array([Vxx, Vyy, Vzz])

  return TransformationMatrixToZYZEuler(efg_transformation_matrix)


def VectorsToAngles(Vzz, Vxx = [], Vyy = []):
  r"""
  Calculates the Euler angles corresponding to the EFG vectors in thr internal coordinate system.
  Input 3 orthogonal vectors of the EFG components Vxx, Vyy, and Vzz in the internal coordinate system or just the main component Vzz.
  The vectors will be normalized, so only their directions is important.

  e.g. Vxx = [0,0,1], Vyy = [-1,-1,0], Vzz = [1,-1,0] in the [sigma,pi,k] coordinate system.

  .. note:: In case only the main axis Vyy is given, the returned alpha value is random.

  Args:
      Vxx (ndarray): Vxx vector in the reference frame.
      Vyy (ndarray): Vyy vector in the reference frame.
      Vzz (ndarray): Vzz vector in the reference frame.

  Returns:
      float, float, float: alpha, beta, gamma
  """
  if not Vxx and not Vyy:
    return EFGvectorToZYZEuler(Vzz)
  else:
    return EFGvectorsToZYZEuler(Vxx,Vyy,Vzz)
    