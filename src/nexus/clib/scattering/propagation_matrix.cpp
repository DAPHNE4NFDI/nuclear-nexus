// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "propagation_matrix.h"

#include <complex>
#include <algorithm>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


namespace electronic {

  namespace forward {

    Complex PropagationFactor(const Complex f, const double kvector)
    {
      return kvector + f;
    }

  }  // namespace forward

  namespace grazing {

    Eigen::Matrix2cd PropagationMatrix(const Complex f, const double kvector_z)
    {
      Eigen::Matrix2cd F_matrix = Eigen::Matrix2cd::Zero();

      F_matrix(0, 0) = f + kvector_z;
      F_matrix(0, 1) = f;
      F_matrix(1, 0) = -f;
      F_matrix(1, 1) = -f - kvector_z;

      return F_matrix;
    }

  }  // namespace grazing

}  // namespace electronic


namespace nuclear {

  namespace forward {

    Eigen::Matrix2cd PropagationMatrix(const Eigen::Matrix2cd& scattering_matrix, const double kvector)
    {
      const Eigen::Matrix2cd k_matrix = kvector * Eigen::Matrix2cd::Identity();

      return k_matrix + scattering_matrix;
    }

    PropagationMatrix2 PropagationMatrix(const ScatteringMatrix2& scattering_matrix, const double kvector)
    {
      const Eigen::Matrix2cd k_matrix = kvector * Eigen::Matrix2cd::Identity();

      PropagationMatrix2 F_matrix(scattering_matrix.size());

      std::transform(NEXUS_EXECUTION_POLICY scattering_matrix.begin(), scattering_matrix.end(), F_matrix.begin(),
        [&k_matrix](const Eigen::Matrix2cd& scat_mat)
        {
          return k_matrix + scat_mat;
        }
      );

      return F_matrix;
    }

  }  // namespace forward


  namespace grazing {

    Eigen::Matrix4cd PropagationMatrix(const Eigen::Matrix4cd& scattering_matrix, const double kvector_z)
    {
      Eigen::Matrix4cd kz_matrix = kvector_z * Eigen::Matrix4cd::Identity();

      return kz_matrix + scattering_matrix;
    }

    PropagationMatrix4 PropagationMatrix(const ScatteringMatrix2& scattering_matrix, const double kvector_z)
    {
      const Eigen::Matrix2cd kz_matrix = kvector_z * Eigen::Matrix2cd::Identity();

      PropagationMatrix4 propagation_matrix(scattering_matrix.size());

      std::transform(NEXUS_EXECUTION_POLICY scattering_matrix.begin(), scattering_matrix.end(), propagation_matrix.begin(),
        [&kz_matrix](const Eigen::Matrix2cd& matrix)
        {
          Eigen::Matrix4cd F_matrix;

          F_matrix.topLeftCorner<2, 2>() = matrix + kz_matrix;
          F_matrix.topRightCorner<2, 2>() = matrix;
          F_matrix.bottomLeftCorner<2, 2>() = -matrix;
          F_matrix.bottomRightCorner<2, 2>() = -matrix - kz_matrix;

          return F_matrix;
        }
      );

      return propagation_matrix;
    }

  }  // namespace grazing

}  // namespace nuclear


/*
Eigen::MatrixXcd F_matrixX(const Eigen::MatrixXcd& matrix, const double kvector){
  Eigen::Matrix4cd kmatrix = Eigen::MatrixXcd::Identity();
  kmatrix *= kvector;
  return kmatrix + matrix;
}

PropagationMatrixX F_matrixX(const ScatteringMatrix& scatteringmatrix, const double kvector){
  PropagationMatrixX F_matrix {};
  
  Eigen::MatrixXcd kmatrix = Eigen::MatrixXcd::Identity(F_matrix.rows(), F_matrix.cols());
  kmatrix *= kvector;
  
  return F_matrix;
}
*/
