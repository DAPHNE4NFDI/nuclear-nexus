// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Map for the photoelectric effect coefficients of the elements up to Z=86 ("Rn").
// from "D. L. Smith, ANL-7796, March 1971, EMPIRICAL FORMULA FOR INTERPOLATION OF TABULATED PHOTON PHOTOELECTRIC CROSS SECTIONS". 
// The first map coefficient is the atom string identifier.
// The second map coefficient is a vector of a double vector.
// The inner vector gives the energy up to which the following coefficients are valid.
// The coefficients are p1 to p5 for formula in Smith paper.
// The first entry starts from 1 keV to entry [keV], then value to next first entry and so on.
//
// Example for Helium:
// {"He",{{1.0e1, 6.014, -3.284, -2.586e-02, 1.660e-03, 0},
//        {4.0e1, 6.014, -3.380, 0, 0, 0}}},
// The coefficients p1 to p5 are 6.014, -3.284, -2.586e-02, 1.660e-03, 0 for an energy range from 1keV to 10 keV.
// The coefficients p1 to p5 are 6.014, -3.380, 0, 0, 0 for an energy range from 10keV to 40 keV.
// The coefficients p1 to p5 are 0, 0, 0, 0, 0 for an energy range above 40 keV and the cross section is zero.


#ifndef NEXUS_PHOTOEFFECTMAPS_H_
#define NEXUS_PHOTOEFFECTMAPS_H_

#include <string>
#include <vector>
#include <map>


namespace photoeffect_maps {

  extern const std::map<const std::string, const std::vector<std::vector<double>>> photoeffect_coefficients_map;

}  // namespace photoeffect_maps

#endif // NEXUS_PHOTOEFFECTMAPS_H_
