// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "layer_matrix.h"

#include <complex>
#include <algorithm>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include <unsupported/Eigen/MatrixFunctions>

#include "../nexus_definitions.h"
#include "../utilities/python_print.h"
#include "../math/matrix.h"


using namespace std::complex_literals;


namespace electronic {
  
  namespace forward {

    Complex TransmissionFactor(const Complex propagation_factor, const double thickness)
    {
      return exp(1.0i * thickness * propagation_factor);
    }

  }  // namespace forward
  
  namespace grazing {

    Eigen::Matrix2cd LayerMatrix(const Eigen::Matrix2cd& propagation_matrix, const double thickness)
    {
      return (1.0i * thickness * propagation_matrix).exp();
    }

    // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. 4.35
    Eigen::Matrix2cd LayerMatrixBottom(const Complex scattering_factor, const double kvector_z)
    {
      const Complex beta = sqrt(1.0 + 2.0 * scattering_factor / kvector_z);

      Eigen::Matrix2cd layer_matrix;

      layer_matrix(0, 0) = 0.0;
      layer_matrix(0, 1) = 0.0;
      layer_matrix(1, 0) = (beta - 1.0) / (2.0 * beta);
      layer_matrix(1, 1) = (beta + 1.0) / (2.0 * beta);

      return layer_matrix;
    }

    // Exponential of Roughness Matrix for electronic scattering
    // after Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. 4.66
    // W matrix after Roehlsberger, HI 1999, Eq. C.2
    Eigen::Matrix2cd RoughnessLayerMatrix(const Complex scattering_factor_1, const Complex scattering_factor_2,
      const double kvector_z, const double sigma)
    {
      const Complex beta_1 = sqrt(1.0 + 2.0 * scattering_factor_1 / kvector_z);
      const Complex beta_2 = sqrt(1.0 + 2.0 * scattering_factor_2 / kvector_z);

      const Complex c_factor = 0.5 * (pow(beta_1, 2) - pow(beta_2, 2)) / (pow(beta_1, 2) + pow(beta_2, 2))
        * (1.0 - exp(-pow(kvector_z, 2) * (pow(beta_1, 2) + pow(beta_2, 2)) * pow(sigma, 2)));

      Eigen::Matrix2cd W_matrix = Eigen::Matrix2cd::Zero();

      W_matrix(0, 1) = c_factor;
      W_matrix(1, 0) = c_factor;

      return W_matrix.exp();
    }

  }  // namespace grazing
}  // namespace electronic


namespace nuclear {
  /*
  // The layer matrix is extended by the boundary entries to padding size
  LayerMatrix2 PaddingLayerMatrix2(const LayerMatrix2& layer_matrix, const int grid_size, const int padding_size)
  {
    LayerMatrix2 layer_matrix_padded{};

    const int padding_difference = padding_size - grid_size;

    layer_matrix_padded.insert(layer_matrix_padded.begin(), static_cast<size_t>(round(padding_difference / 2)), layer_matrix.front());

    layer_matrix_padded.insert(layer_matrix_padded.end(), layer_matrix.begin(), layer_matrix.end());

    layer_matrix_padded.insert(layer_matrix_padded.end(), static_cast<size_t>(round(padding_difference / 2)), layer_matrix.back());

    return layer_matrix_padded;
  }

  LayerMatrix2 MultiplyLayerMatrix2(const LayerMatrix2& layer_matrix_1, const LayerMatrix2& layer_matrix_2)
  {
    LayerMatrix2 result(layer_matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY layer_matrix_1.begin(), layer_matrix_1.end(), layer_matrix_2.begin(), result.begin(),
      [](const Eigen::Matrix2cd& lay_mat_1, const Eigen::Matrix2cd& lay_mat_2)
      {
        return lay_mat_1 * lay_mat_2;
      }
    );

    return result;
  }

  LayerMatrix4 MultiplyLayerMatrix4(const LayerMatrix4& layer_matrix_1, const LayerMatrix4& layer_matrix_2)
  {
    LayerMatrix4 result(layer_matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY layer_matrix_1.begin(), layer_matrix_1.end(), layer_matrix_2.begin(), result.begin(),
      [](const Eigen::Matrix4cd& lay_mat_1, const Eigen::Matrix4cd& lay_mat_2)
      {
        return lay_mat_1 * lay_mat_2;
      }
    );

    return result;
  }

  LayerMatrix2 AddLayerMatrices(const LayerMatrix2& layer_matrix_1, const LayerMatrix2& layer_matrix_2)
  {
    LayerMatrix2 result(layer_matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY layer_matrix_1.begin(), layer_matrix_1.end(), layer_matrix_2.begin(), result.begin(),
      [](const Eigen::Matrix2cd& mat_1, const Eigen::Matrix2cd& mat_2)
      {
        return mat_1 + mat_2;
      }
    );

    return result;
  }

  LayerMatrix2 AddLayerMatrices(const LayerMatrix2& layer_matrix_1, const Eigen::Matrix2cd& matrix_2)
  {
    ScatteringMatrix2 result(layer_matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY layer_matrix_1.begin(), layer_matrix_1.end(), result.begin(),
      [&matrix_2](const Eigen::Matrix2cd& mat_1)
      {
        return mat_1 + matrix_2;
      }
    );

    return result;
  }

  LayerMatrix2 SubtractLayerMatrices(const LayerMatrix2& layer_matrix_1, const LayerMatrix2& layer_matrix_2)
  {
    LayerMatrix2 result(layer_matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY layer_matrix_1.begin(), layer_matrix_1.end(), layer_matrix_2.begin(), result.begin(),
      [](const Eigen::Matrix2cd& mat_1, const Eigen::Matrix2cd& mat_2)
      {
        return mat_1 - mat_2;
      }
    );

    return result;
  }

  LayerMatrix2 SubtractLayerMatrices(const LayerMatrix2& layer_matrix_1, const Eigen::Matrix2cd& matrix_2)
  {
    ScatteringMatrix2 result(layer_matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY layer_matrix_1.begin(), layer_matrix_1.end(), result.begin(),
      [&matrix_2](const Eigen::Matrix2cd& mat_1)
      {
        return mat_1 - matrix_2;
      }
    );

    return result;
  }

  template <class T>
  LayerMatrix2 ScaleLayerMatrix(const LayerMatrix2& layer_matrix, const T factor)
  {
    LayerMatrix2 result(layer_matrix.size());

    std::transform(layer_matrix.begin(), layer_matrix.end(), result.begin(),
      [factor](const Eigen::Matrix2cd& lay_mat)
      {
        return factor * lay_mat;
      }
    );

    return result;
  }

  template LayerMatrix2 ScaleLayerMatrix<double>(const LayerMatrix2& layer_matrix, const double factor);

  template LayerMatrix2 ScaleLayerMatrix<Complex>(const LayerMatrix2& layer_matrix, const Complex factor);


  template <class T>
  LayerMatrix2 MultiplyVectorLayerMatrix2(const LayerMatrix2& layer_matrix, const std::vector<T>& vector)
  {
    LayerMatrix2 result(layer_matrix.size());

    std::transform(layer_matrix.begin(), layer_matrix.end(), vector.begin(), result.begin(),
      [](const Eigen::Matrix2cd& lay_mat, T vector_element)
      {
        return vector_element * lay_mat;
      }
    );

    return result;
  }

  template LayerMatrix2 MultiplyVectorLayerMatrix2<double>(const LayerMatrix2& layer_matrix, const std::vector<double>& vector);

  template LayerMatrix2 MultiplyVectorLayerMatrix2<Complex>(const LayerMatrix2& layer_matrix, const std::vector<Complex>& vector);


  template <class T>
  LayerMatrix2 DivideVectorLayerMatrix2(const LayerMatrix2& layer_matrix, const std::vector<T>& vector)
  {
    LayerMatrix2 result(layer_matrix.size());

    std::transform(layer_matrix.begin(), layer_matrix.end(), vector.begin(), result.begin(),
      [](const Eigen::Matrix2cd& lay_mat, T vector_element)
      {
        return lay_mat / vector_element;
      }
    );

    return result;
  }

  template LayerMatrix2 DivideVectorLayerMatrix2<double>(const LayerMatrix2& layer_matrix, const std::vector<double>& vector);

  template LayerMatrix2 DivideVectorLayerMatrix2<Complex>(const LayerMatrix2& layer_matrix, const std::vector<Complex>& vector);
*/

  namespace forward {

    Eigen::Matrix2cd TransmissionMatrix(const Eigen::Matrix2cd& propagation_matrix, const double thickness)
    {
      return (1.0i * thickness * propagation_matrix).exp();
    }

    LayerMatrix2 TransmissionMatrix(const PropagationMatrix2& propagation_matrix, const double thickness)
    {
      LayerMatrix2 layer_matrix(propagation_matrix.size());

      std::transform(NEXUS_EXECUTION_POLICY propagation_matrix.begin(), propagation_matrix.end(), layer_matrix.begin(),
        [thickness](const Eigen::Matrix2cd& matrix)
        {
          return (1.0i * thickness * matrix).exp();
        }
      );

      return layer_matrix;
    }

  }  // namespace forward

  namespace grazing {

    Eigen::Matrix4cd LayerMatrix(const Eigen::Matrix4cd& propagation_matrix, const double thickness)
    {
      return (1.0i * thickness * propagation_matrix).exp();
    }

    LayerMatrix4 LayerMatrix(const PropagationMatrix4& propagation_matrix, const double thickness)
    {
      LayerMatrix4 layer_matrix(propagation_matrix.size());

      std::transform(NEXUS_EXECUTION_POLICY propagation_matrix.begin(), propagation_matrix.end(), layer_matrix.begin(),
        [thickness](const Eigen::Matrix4cd& matrix)
        {
          return (1.0i * thickness * matrix).exp();
        }
      );

      return layer_matrix;
    }

    // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. A.33
    // This is not the tensor or Kronecker product.
    Eigen::Matrix4cd RalfsDirectProduct(const Eigen::Matrix2cd& matrix_1, const Eigen::Matrix2cd& matrix_2)
    {
      Eigen::Matrix4cd result = Eigen::Matrix4cd::Zero();

      result(0, 0) = matrix_1(0, 0);
      result(0, 2) = matrix_1(0, 1);

      result(1, 1) = matrix_2(0, 0);
      result(1, 3) = matrix_2(0, 1);

      result(2, 0) = matrix_1(1, 0);
      result(2, 2) = matrix_1(1, 1);

      result(3, 1) = matrix_2(1, 0);
      result(3, 3) = matrix_2(1, 1);

      return result;
    }

    // Infinte layers need special treatment.
    // Find diagonal matrix, analytical is faster for diagonal scattering matrix.
    // This is the standard case, like Si wafer as infinite layer.
    // For anisotropic resonant scattering, complex EigenSovler is faster. But unlikely the case.
    // g_matrix for scattering matrix f
    // Roehlsberger, HI 1999, Eq. 4.17, only valid for anisotropic scattering.
    // For isotropic scattering, the matrix is already diagonal.
    Eigen::Matrix4cd LayerMatrixBottomOneMatrix(const Eigen::Matrix2cd& scattering_matrix, const double kvector_z)
    {
      // set values for diagonal scattering_matrix
      Complex eigenvalue_1 = scattering_matrix(0, 0);
      Complex eigenvalue_2 = scattering_matrix(1, 1);

      Eigen::Matrix2cd g_matrix = Eigen::Matrix2cd::Identity();

      Eigen::Matrix2cd g_matrix_inverse = Eigen::Matrix2cd::Identity();

      // if anisotropic scattering_matrix, almost never the case, because substrate is typically non-resonant.
      if (scattering_matrix(1, 0).real() > 1.0e-30 ||
          scattering_matrix(1, 0).imag() > 1.0e-30)
      {
        const Complex sum = scattering_matrix(0, 0) + scattering_matrix(1, 1);

        const Complex difference = scattering_matrix(0, 0) - scattering_matrix(1, 1);

        const Complex product = scattering_matrix(0, 1) * scattering_matrix(1, 0);

        eigenvalue_1 = 0.5 * sum + 0.5 * sqrt(pow(difference, 2) + 4.0 * product);
        eigenvalue_2 = 0.5 * sum - 0.5 * sqrt(pow(difference, 2) + 4.0 * product);

        g_matrix(0, 0) = 1.0;
        g_matrix(0, 1) = 1.0;
        g_matrix(1, 0) = (eigenvalue_1 - scattering_matrix(0, 0)) / scattering_matrix(0, 1);
        g_matrix(1, 1) = (eigenvalue_2 - scattering_matrix(0, 0)) / scattering_matrix(0, 1);

        Eigen::Matrix2cd g_matrix_inverse = g_matrix.completeOrthogonalDecomposition().solve(Eigen::Matrix2cd::Identity());
      }

      Eigen::Matrix4cd G_matrix = Eigen::Matrix4cd::Zero();

      G_matrix.topLeftCorner<2,2>() = g_matrix;
      G_matrix.bottomRightCorner<2,2>() = g_matrix;

      Eigen::Matrix4cd G_matrix_inverse = Eigen::Matrix4cd::Zero();

      G_matrix_inverse.topLeftCorner<2,2>() = g_matrix_inverse;
      G_matrix_inverse.bottomRightCorner<2,2>() = g_matrix_inverse;

      // layer matrix of bottom layer
      // RalfsDirectProdcut is faster then explicit matrices
      const Complex beta_1 = sqrt(1.0 + 2.0 * eigenvalue_1 / kvector_z);
      const Complex beta_2 = sqrt(1.0 + 2.0 * eigenvalue_2 / kvector_z);

      // calculate matrix exponential of block part matrix F_1 for first eigenvalue
      // bottom layer, simplified Eq. (4.35)
      Eigen::Matrix2cd F_matrix_1 = Eigen::Matrix2cd::Zero();

      F_matrix_1(1, 0) = (beta_1 - 1.0) / (2.0 * beta_1);
      F_matrix_1(1, 1) = (beta_1 + 1.0) / (2.0 * beta_1);

      // calculate matrix exponential of block part matrix F_2 for second eigenvalue
      // bottom layer, simplified Eq. (4.35)
      Eigen::Matrix2cd F_matrix_2 = Eigen::Matrix2cd::Zero();

      F_matrix_2(1, 0) = (beta_2 - 1.0) / (2.0 * beta_2);
      F_matrix_2(1, 1) = (beta_2 + 1.0) / (2.0 * beta_2);

      // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq A.33
      const Eigen::Matrix4cd F_exponential_eigenpol = RalfsDirectProduct(F_matrix_1, F_matrix_2);

      // apply diagonalization transformation
      const Eigen::Matrix4cd matrix_bottom = G_matrix * F_exponential_eigenpol * G_matrix_inverse;

      return matrix_bottom;
    }

    LayerMatrix4 LayerMatrixBottom(const ScatteringMatrix2& scattering_matrix, const double kvector_z)
    {
      LayerMatrix4 layer_matrix_bottom(scattering_matrix.size());

      std::transform(NEXUS_EXECUTION_POLICY scattering_matrix.begin(), scattering_matrix.end(), layer_matrix_bottom.begin(),
        [kvector_z](const Eigen::Matrix2cd& matrix)
        {
          return LayerMatrixBottomOneMatrix(matrix, kvector_z);
        }
      );

      return layer_matrix_bottom;
    }

    inline Eigen::Matrix4cd MatrixCommutatorOrder2(const Eigen::Matrix4cd& F_matrix_1, const Eigen::Matrix4cd& F_matrix_2)
    {
      const Eigen::Matrix4cd commutator_2 = (F_matrix_1 * F_matrix_2) - (F_matrix_2 * F_matrix_1);

      return commutator_2;
    }

    /*
    // inline Eigen::Matrix4cd MatrixCommutatorOrder4(const Eigen::Matrix4cd& F_matrix_1, const Eigen::Matrix4cd& F_matrix_2)
    // problems with referencing
    inline Eigen::Matrix4cd MatrixCommutatorOrder4(const Eigen::Matrix4cd F_matrix_1, const Eigen::Matrix4cd F_matrix_2)
    {
      const Eigen::Matrix4cd m1 = MatrixCommutatorOrder2(F_matrix_1, F_matrix_2);

      const Eigen::Matrix4cd m2 = MatrixCommutatorOrder2(m1, F_matrix_1);

      const Eigen::Matrix4cd commutator_4 = MatrixCommutatorOrder2(m2, F_matrix_2);
      
      return commutator_4;
    }
    */

    // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq 4.69 W
    // up to second order, n=2
    // F1 and F2 are matrixes of left and right layer matrices, respectively: exp(F1)*exp(F2) -> exp(F1)*exp(W)*exp(F2).
    // So F1, F2 means that matrix of layer 2 is above layer 1 in the layer system
    //
    // changed to first order (n=1) due to compiler problems (MSVC) with second order in version 1.2.0
    //
    Eigen::Matrix4cd RoughnessMatrix(const Eigen::Matrix4cd& F_matrix_1, const Eigen::Matrix4cd& F_matrix_2, const double sigma)
    {
      const double sigma_squared = pow(sigma, 2);

      //const Eigen::Matrix4cd mco4 = MatrixCommutatorOrder4(F_matrix_1, F_matrix_2);

      const Eigen::Matrix4cd W_matrix = sigma_squared / 2.0 * MatrixCommutatorOrder2(F_matrix_1, F_matrix_2);
                                       // + 0.5 * pow(sigma_squared / 2.0, 2) * mco4;

      return W_matrix;
    }

    // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, exponential of Eq 4.69 (exp(W))
    // up to second order, n=2
    // F1 and F2 are matrixes of left and right layer matrices, respectively: exp(F1)*exp(F2) -> exp(F1)*exp(W)*exp(F2).
    // So F1, F2 means that matrix of layer 2 is above layer 1 in the layer system
    LayerMatrix4 RoughnessLayerMatrix(const PropagationMatrix4& F_matrix_1, const PropagationMatrix4& F_matrix_2, const double sigma)
    {
      LayerMatrix4 roughness_matrix(F_matrix_1.size());

      std::transform(NEXUS_EXECUTION_POLICY F_matrix_1.begin(), F_matrix_1.end(), F_matrix_2.begin(), roughness_matrix.begin(),
        [sigma](const Eigen::Matrix4cd& F_mat_1, const Eigen::Matrix4cd& F_mat_2)
        {
          const Eigen::Matrix4cd W_matrix = RoughnessMatrix(F_mat_1, F_mat_2, sigma);
        
          return W_matrix.exp();
        });

      return roughness_matrix;
    }

  }  // namespace grazing
}  // namespace nuclear


/*
Eigen::MatrixXcd exponential_diagx(const Eigen::MatrixXcd &matrix)
{
  Eigen::ComplexEigenSolver<Eigen::MatrixXcd> eigensolver(matrix);
  Eigen::VectorXcd D = eigensolver.eigenvalues().array().exp();
  return eigensolver.eigenvectors() * D.asDiagonal() * eigensolver.eigenvectors().inverse();
}


Eigen::MatrixXcd layermatrixX(const Eigen::MatrixXcd& matrix, const double z) {
  return (1i * z * matrix).exp();
}
*/
