// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the scattering matrix/factor f (forward and grazing incidence) and the refractive index (forward scattering) of a Material object
// and
// the scattering lengths M already weighted by the material composition.
// see R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, page 69 for dynamic theory.
// 
// f and M are complex scalar for non-resonant scattering and calculations are all done at a specific energy.
// 
// ScatteringMatrix types f and M store the energy dependence for nuclear scattering.
// All matrices can be calculated at a specific energy, which returns EigenMatrix2cd, or on a detuning grid.
// This is used together with an isotope to get the transition energy.
// Around its transition energy the grid will be calculated.
// 
// Note, that for the scattering length N matrix the Lamb Moessbauer factor is needed, which is not isotope specific but a material property.
// The detuning grid is given in terms of transition gamma of the used isotope.
// e.g. -10, -9, ... ,-1, 0, 1, ..., 9, 10 Gammas
// This is chosen here for easier user input for nuclear transitions.
//
// Scattering matrices of type 2x2, include:
// forward scattering matrix f
// coherent/atomic scattering length M
// nuclear scattering length N
// electronic scattering length E
//
// To calculate nuclear scattering, calculate the material transitions via CalcTransitions() before calling these functions.


#ifndef NEXUS_SCATTERING_MATRIX_H_
#define NEXUS_SCATTERING_MATRIX_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/element.h"
#include "../classes/material.h"
#include "../classes/moessbauer.h"


// electronic scalar functions
// electronic functions directly calculate scattering factor (matrix) f
namespace electronic {

  /**
  Electronic scattering length for a material.
  Calculation mode is defined by the function :func:`SetAtomicScatteringFactorCXRO`.

  Args:
     material (:class:`Material`): Material for the calculation.
     energy (float): Energy for the calculation in eV.

  Returns:
     complex: Electronic scattering length (1/m^2).
  */
  Complex MaterialWeightedScatteringLength(const Material* const material, const double energy);

  /**
  Electronic scattering length for a material in the base of :math:`\sigma` and :math:`\pi` polarization.
  Calculation mode is defined by the function :func:`SetAtomicScatteringFactorCXRO`.
  See :func:`ElectronicScatteringLength`.

  Args:
     material (:class:`Material`): Material for the calculation.
     energy (float): Energy for the calculation in eV.

  Returns:
     array: 2x2 matrix electronic scattering length (1/m^2).
  */
  Eigen::Matrix2cd MaterialWeightedScatteringLengthMatrix(const Material* const material, const double energy);

  /**
  Electronic scattering length for a material in the base of :math:`\sigma` and :math:`\pi` polarization for a detuning range.
  Calculation mode is defined by the function :func:`SetAtomicScatteringFactorCXRO`.
  See :func:`ElectronicScatteringLength`.

  Args:
     material (:class:`Material`): Material for the calculation.
     isotope (:class:MoessbauerIsotope): The :class:`MoessbauerIsotope` for the calcuaiton.
     detuning (list or ndarray): Detuning values around the isotope transition energy.

  Returns:
     array: Array of 2x2 matrices of the electronic scattering length (1/m^2).
  */
  ScatteringMatrix2 MaterialWeightedScatteringLengthMatrix(const Material* const material, const MoessbauerIsotope* const isotope,
    const std::vector<double>& detuning);

  namespace forward {

    /**
    Pure electronic forward scattering factor.
    Scalar version of Eq. (4.4), [Roehlsberger]_.

    Args:
       material (:class:`Material`): Material for the calculation.
       energy (float): Photon energy (eV).

    Returns:
       complex: Electronic forward scattering factor.
    */
    Complex ScatteringFactor(const Material* const material, const double energy);

    /**
    Pure electronic refractive index in forward direction.
    Scalar version of Eq. (4.5), [Roehlsberger]_.

    Args:
       material (:class:`Material`): Material for the X-ray interaction.
       energy (float): Photon energy (eV).

    Returns:
       complex: Electronic forward scattering factor.
    */
    Complex RefractiveIndex(const Material* const material, const double energy);

  }  // namespace forward

  namespace grazing {

    /**
    Pure electronic scattering factor in grazing incidence geometry.
    Scalar version of Eq. (4.25), [Roehlsberger]_.

    Args:
       material (:class:`Material`): Material for the X-ray interaction.
       energy (float): Photon energy (eV).
       kvector_z (float): k-vector component along layer direction.

    Returns:
       complex: Electronic scattering factor.
    */
    Complex ScatteringFactor(const Material* const material, const double energy, const double kvector_z);

    /**
    Calculates the critical angle of the material, see Eq. (4.22), [Roehlsberger]_.

    .. versionadded:: 1.0.3

    Args:
       material (:class:`Material`): Material for the X-ray interaction.
       energy (float): Photon energy (eV).

    Returns:
       float: Critical angle (rad).
    */
    double CriticalAngle(const Material* const material, const double energy);

  }  // namespace grazing
}  // namespace electronic


namespace nuclear {

  //Math functions for scattering matrix types
  /*
  ScatteringMatrix2 AddScatteringMatrices(const ScatteringMatrix2& input1, const ScatteringMatrix2& input2);

  ScatteringMatrix2 AddScatteringMatrices(const ScatteringMatrix2& input1, const Eigen::Matrix2cd& input2);

  ScatteringMatrix2 SubtractScatteringMatrices(const ScatteringMatrix2& input1, const ScatteringMatrix2& input2);

  ScatteringMatrix2 SubtractScatteringMatrices(const ScatteringMatrix2& input1, const Eigen::Matrix2cd& input2);

  ScatteringMatrix2 ScaleScatteringMatrix(const ScatteringMatrix2& scattering_matrix, const double factor);
  */

  /**
  Nuclear scattering length in the base of :math:`\sigma` and :math:`\pi` polarization already weighted by the isotope density.
  
  Args:
     material (:class:`Material`): Material for the X-ray interaction.
     isotope (:class:`MoessbauerIsotope`): The :class:`MoessbauerIsotope` for which the nuclear scattering length should be calculated.
     detuning (list or ndarray): Detuning values around the isotope transition energy.
     calc_transition (bool): Specifies if the nuclear transitions of the material are calculated before the nuclear scattering length is determined.

  Returns:
     array: Array of 2x2 matrices of the nuclear scattering length (1/m^2).
  */
  ScatteringMatrix2 MaterialWeightedNuclearScatteringLength(Material* const material, const MoessbauerIsotope* const isotope,
    const std::vector<double>& detuning_grid, const bool calc_transitions);
  
  /**
  Coherent (electronic + nuclear) scattering length in the base of :math:`\sigma` and :math:`\pi` polarization already weighted by the material properties.
  
  Args:
     material (:class:`Material`): Material for the X-ray interaction.
     isotope (:class:`MoessbauerIsotope`): The :class:`MoessbauerIsotope` for which the nuclear scattering length should be calculated.
     detuning (list or ndarray): Detuning values around the isotope transition energy.
     calc_transition (bool): Specifies if the nuclear transitions of the material are calculated before the nuclear scattering length is determined.

  Returns:
     array: Array of 2x2 matrices of the nuclear scattering length (1/m^2).
  */
  ScatteringMatrix2 MaterialWeightedCoherentScatteringLength(Material* const material, const MoessbauerIsotope* const isotope,
    const std::vector<double>& detuning_grid, const bool calc_transitions);

  namespace forward {

    /**
    Forward scattering matrix. Eq. (4.4), [Roehlsberger]_.

    Args:
       material (:class:`Material`): Material for the X-ray interaction.
       isotope (:class:`MoessbauerIsotope`): The :class:`MoessbauerIsotope` for which the nuclear scattering length should be calculated.
       detuning (list or ndarray): Detuning values around the isotope transition energy.
       calc_transition (bool): Specifies if the nuclear transitions of the material are calculated before the scattering matrix is determined.

    Returns:
       list: List of 2x2 matrices of the forward scattering matrix (1/m).
    */
    ScatteringMatrix2 ScatteringMatrix(
      Material* const material,
      const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning, 
      const bool calc_transitions = true
    );

  } // namespace forward

  namespace grazing {

    /**
    Grazing-incidence scattering matrix. Eq. (4.25), [Roehlsberger]_.

    Args:
       material (:class:`Material`): Material for the X-ray interaction.
       isotope (:class:`MoessbauerIsotope`): The :class:`MoessbauerIsotope` for which the nuclear scattering length should be calculated.
       detuning (list or ndarray): Detuning values around the isotope transition energy.
       kvector_z (float): k-vector component along layer direction.
       calc_transition (bool): Specifies if the nuclear transitions of the material are calculated before the scattering matrix is determined.

    Returns:
       list: List of 2x2 matrices of the grazing-incidence scattering matrix (1/m).
    */
    ScatteringMatrix2 ScatteringMatrix(
      Material* const material,
      const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning,
      const double kvector_z,
      const bool calc_transitions = true
    );

  }  // namespace grazing

}  // namespace nuclear

#endif // NEXUS_SCATTERING_MATRIX_H_
