// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementations of the scattering length of pure electronic and nuclear scattering lengths E and N of an Element or Material.
// E and N are calculated from Sturhahn PRB Eq. 16.
// Sturhahn PRB Eq. 16 is divided by 2pi to be consistent with Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
// Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. 4.11.
//
// The coherent/atomic scattering length M is calculated by scattering_matrix.h.


#ifndef NEXUS_SCATTERING_LENGTH_H_
#define NEXUS_SCATTERING_LENGTH_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/element.h"
#include "../classes/material.h"
#include "../classes/moessbauer.h"


namespace scattering_length {

  /*
   Variable that determines the calculation of the atomic scattering factors.
   true: use CXRO scattering factors
   false: use cross section calculations
   */
  extern bool atomic_scattering_factors_cxro;

  /**
  Get the calculation method of the atomic scattering factors.
  If ``False`` the scattering factors are calculated from Klein-Nishina cross sections and tabulated photo-effect cross sections.
  If ``True`` the tabulated scattering factors from the Center of X-ray Optics are used.

  Returns:
    bool: atomic_scattering_factor_cxro.
  */
  bool GetAtomicScatteringFactorCXRO();

  /**
  Set the calculation method of the atomic scattering factors.

  Args:
    cxro (bool): Determines the calculation of the atomic scattering factors.
      If ``False`` the scattering factors are calculated from Klein-Nishina scattering and tabulated photo-effect cross sections.
      If ``True`` the tabulated scattering factors from the Center of X-ray Optics are used.
  */
  void SetAtomicScatteringFactorCXRO(const bool cxro);

  namespace electronic {

    /**
    Calculates the electronic scattering length as given in Eq.(4.11) [Roehlsberger]_.
    Note that the definition differs by a factor of :math:`1/(2\pi)` from the one given in Eq. (4, 16) [Sturhahn]_.
    The Debye-Waller factor is one here, because only forward and grazing incidence scattering are considered.
    An additional relativistic correction is applied to the real part, after Eq. (78) [Henke]_.

    Args:
      element (:class:`Element`): Element for which the scattering length is calculated.
      energy (float): X-ray energy (eV).
      cxro (bool): Determines the calculation of the atomic scattering factors.
        If ``False`` the scattering factors are calculated from Klein-Nishina cross sections and tabulated photo-effect cross sections (as in CONUSS).
        If ``True`` the tabulated scattering factors from the Center of X-ray Optics are used (as in GenX).

    Returns:
      complex: Electronic scattering length (m).
    */
    Complex ScatteringLength(const Element& element, const double energy, const bool cxro);

    /**
    Calculates the electronic scattering length as defined in Eq.(4.11) [Roehlsberger]_.
    Note that the definition differs by a factor of :math:`1/(2\pi)` from the one given in Eq. (4, 16) [Sturhahn]_.
    Klein Nishina cross sections are calculated after [KleinNishina]_.
    Photoeffect cross sections are calculated after [Smith]_.
    The Debye-Waller factor is one here, because only forward and grazing incidence scattering are considered.
    An additional relativistic correction is applied to the real part, after Eq. (78) [Henke]_.

    Args:
       element (:class:`Element`): Element for which the scattering length is calculated.
       energy (float): X-ray energy (eV).

    Returns:
       complex: Electronic scattering length (m).
    */
    Complex ScatteringLengthTheory(const Element& element, const double energy);

    /**
    Calculates the electronic scattering length as defined in Eq.(4.11) [Roehlsberger]_ but with the atomic scattering factors from the CXRO database [Henke]_.
    Note that the definition differs by a factor of :math:`1/(2\pi)` from the one given in Eq. (4, 16) [Sturhahn]_.
    The complex correction to the refractive index in [Roehlsberger]_ and [Henke]_ are defined differently.
    The factor :math:`E_i` from [Roehlsberger]_ is :math:`E_i = -r_e (f_1 - i f_2)`.
    The Debye-Waller factor is one here, because only forward and grazing incidence scattering are considered.
    An additional relativistic correction is applied to the real part, after Eq. (78) [Henke]_.

    .. seealso::  `<https://henke.lbl.gov/optical_constants/asf.html>`_

    Args:
      element (:class:`Element`): Element for which the scattering length is calculated.
      energy (float): X-ray energy (eV).

    Returns:
      complex: Electronic scattering length (m).
    */
    Complex ScatteringLengthCXRO(const Element& element, const double energy);

    /**
    Electronic scattering length in the base of :math:`\sigma` and :math:`\pi` polarization, Eq. (4.11), [Roehlsberger]_.
    Note that the definition differs by a factor of :math:`1/(2\pi)` from the one given in Eq. (4, 16) [Sturhahn]_. The Debye-Waller factor is one here, because only forward and grazing incidence scattering are considered.
    An additional relativistic correction is applied to the real part, after Eq. (78) [Henke]_.


    Args:
      element (:class:`Element`): Element for the calculation.
      energy (float): X-ray energy (eV).
      cxro (bool): Determines the calculation of the atomic scattering factors.
        If ``False`` the scattering factors are calculated from Klein-Nishina cross sections and tabulated photo-effect cross sections (as in CONUSS).
        If ``True`` the tabulated scattering factors from the Center of X-ray Optics are used (as in GenX).

    Returns:
      ndarray: 2x2 matrix of the electronic scattering length (m).
    */
    Eigen::Matrix2cd ScatteringLengthMatrix(const Element& element, const double energy, const bool cxro);

    /**
    Electronic scattering length in the base of :math:`\sigma` and :math:`\pi` polarization, Eq. (4.11), [Roehlsberger]_.
    Note that the definition differs by a factor of :math:`1/(2\pi)` from the one given in Eq. (4, 16) [Sturhahn]_.
    The Debye Waller factor is one here, because only forward and grazing incidence scattering are considered.
    An additional relativistic correction is applied to the real part, after Eq. (78) [Henke]_.
    The electronic scattering length is calculated around the transition energy of :class:`MoessbauerIsotope`.

    Args:
      element (:class:`Element`): Element for the calculation.
      isotope (:class:`MoessbauerIsotope`): The :class:`MoessbauerIsotope` for which the electronic scattering length should be calculated.
      detuning (list or ndarray): Detuning values around the isotope transition energy.
      cxro (bool): Determines the calculation of the atomic scattering factors.
        If ``False`` the scattering factors are calculated from Klein-Nishina cross sections and tabulated photo-effect cross sections (as in CONUSS).
        If ``True`` the tabulated scattering factors from the Center of X-ray Optics are used (as in GenX).

    Returns:
      array: Array of 2x2 matrices of the electronic scattering length (m).
    */
    ScatteringMatrix2 ScatteringLengthMatrix(
      const Element* const element,
      const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning,
      const bool cxro
    );

  } // namespace electronic

  namespace nuclear {






    /**
    Nuclear scattering length in the base of :math:`\sigma` and :math:`\pi` polarization. Eq. (11), [Sturhahn]_.
    Note that the definition differs by a factor of :math:`1/(2\pi)` from the one given in Eq. (11, 16) [Sturhahn]_.
    The nuclear scattering length is calculated around the transition energy of :class:`MoessbauerIsotope`.

    Args:
       material (:class:`Material`): Material for the X-ray interaction.
       isotope (:class:`MoessbauerIsotope`): The :class:`MoessbauerIsotope` for which the nuclear scattering length should be calculated.
       detuning (list or ndarray): Detuning values around the isotope transition energy.
       calc_transition (bool): Specifies if the nuclear transitions of the material are calculated before the nuclear scattering length is determined.

    Returns:
       array: Array of 2x2 matrices of the nuclear scattering length (m).
    */
    ScatteringMatrix2 ScatteringLength(
      Material* const material,
      const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning,
      const bool calc_transitions = true
    );

  } // namespace nuclear

}  // namespace scattering_length

#endif // NEXUS_SCATTERING_LENGTH_H_
