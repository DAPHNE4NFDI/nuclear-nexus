// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "scattering_length.h"

#include <cmath>
#include <complex>
#include <algorithm>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../math/constants.h"
#include "../math/conversions.h"
#include "../nexus_definitions.h"
#include "../scattering/electronic_scattering.h"
#include "../scattering/atomic_scattering_factors.h"


using namespace std::complex_literals;


namespace scattering_length {

  bool atomic_scattering_factors_cxro = true;

  bool GetAtomicScatteringFactorCXRO()
  {
    return atomic_scattering_factors_cxro;
  }

  void SetAtomicScatteringFactorCXRO(const bool cxro)
  {
    atomic_scattering_factors_cxro = cxro;
  }

  namespace electronic {

    // energy in eV, scattering factor in meter
    Complex ScatteringLength(const Element& element, const double energy, const bool cxro)
    {
      // fallback - out of photoeffect cross section region
      if (energy < 1000.0)
        return ScatteringLengthCXRO(element, energy);

      // fallback - out of CXRO database
      if (energy >= 30000.0)
        return ScatteringLengthTheory(element, energy);

      Complex electronic_scattering_factor = Complex(0.0, 0.0);

      if (cxro == true)
      {
        electronic_scattering_factor = ScatteringLengthCXRO(element, energy);
      }
      else
      {
        electronic_scattering_factor = ScatteringLengthTheory(element, energy);
      }

      return electronic_scattering_factor;
    }

    // energy in eV, scattering factor in meter
    // atomic form factor (k0-k), is simply element.Z because momentum transfer q is 0 in forward and grazing.
    // double atomic form factor = element.Z;
    // definition dependent x = (2*kPi), in Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, 2pi is not used here
    // return x * DebyeWaller * (-kElectronRadius * atomic form factor(k0-k) + 1i * kvector / (4 * kPi) * ElectronicCrossSection(element));
    // relativistic correction of f1, see
    // Low-Energy X-ray Interaction Coefficients: Photoabsorption, Scattering, and Reflection, 
    // B. L. Henke, E. M. Gullikson, and J. C. Davis
    // https://henke.lbl.gov/optical_constants/asf.html
    // readme in downloadable Gzipped archive
    // only applied to f1, so real part of scattering
    Complex ScatteringLengthTheory(const Element& element, const double energy)
    {
      const double kvector = conversions::EnergyToKvector(energy);

      const double corrected_atomic_number = element.atomic_number - pow(element.atomic_number / 82.5, 2.37);

      const Complex electronic_scattering_factor = -constants::kElectronRadius * corrected_atomic_number
        + 1.0i * kvector / (4.0 * constants::kPi) * electronic_scattering::ElectronicCrossSection(element, energy);

      return electronic_scattering_factor;
    }

    // energy in eV, scattering factor in meter
    // atomic scattering factors from CXRO database
    // see https://henke.lbl.gov/optical_constants/asf.html
    Complex ScatteringLengthCXRO(const Element& element, const double energy)
    {
      const auto map_iterator = atomic_scattering_factors::atomic_scattering_factors_map.find(element.element);

      const atomic_scattering_factors::ScatteringFactorArray* factors_array = map_iterator->second;

      double energy_left = (*factors_array)[0][0];
      double f1_left = (*factors_array)[0][1];
      double f2_left = (*factors_array)[0][2];

      double energy_right = (*factors_array)[0][0];
      double f1_right = (*factors_array)[0][1];
      double f2_right = (*factors_array)[0][2];

      for (int i = 0; i < 1000; ++i)
      {
        energy_right = (*factors_array)[i][0];

        f1_right = (*factors_array)[i][1];
        f2_right = (*factors_array)[i][2];

        if (energy_right > energy)
        {
          break;
        }
        else
        {
          energy_left = energy_right;

          f1_left = f1_right;
          f2_left = f2_right;
        }
      }

      double gradient = (f1_right - f1_left) / (energy_right - energy_left);

      const double f1 = f1_left + gradient * (energy - energy_left);

      gradient = (f2_right - f2_left) / (energy_right - energy_left);

      const double f2 = f2_left + gradient * (energy - energy_left);

      // definition of refractive index
      // Ralf and Sturhahn use n = 1 - delta + i beta
      // Eric M. Gullikson uses n = 1 - delta - i beta
      // so the sign must be switched in f2
      return -constants::kElectronRadius * (f1 - 1.0i * f2);
    }


    Eigen::Matrix2cd ScatteringLengthMatrix(const Element& element, const double energy, const bool cxro)
    {
      const Complex scattering_factor = ScatteringLength(element, energy, cxro);

      Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Zero();

      matrix(0, 0) = scattering_factor;

      matrix(1, 1) = scattering_factor;

      return matrix;
    }


    // electronic scattering length matrix for element
    // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. 4.11
    // detuning around an isotopes resonance energy in units of transition gamma
    ScatteringMatrix2 ScatteringLengthMatrix(const Element* const element, const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning_grid, const bool cxro)
    {
      const double energy = isotope->energy;

      const double gamma_to_eV = isotope->gamma;

      ScatteringMatrix2 matrix{};

      for (const double detuning_gamma : detuning_grid)
        matrix.push_back(ScatteringLengthMatrix(*element, energy + detuning_gamma * gamma_to_eV, cxro));

      return matrix;
    }

  } // namespace electronic

  namespace nuclear {

    // Sturhahn PRB Eq. 16 nuclear part, divided by 2pi, to be consistent with Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, and electronic scattering
    // transistion.energy_detuning and detuning_gamma in units of gamma
    // the actual Eq. 7 from Sturhahn PRB is
    // double gamma_to_eV = material.isotope.gamma;
    // divisor = 2.0 / material.isotope.gamma * ((material.isotope.energy + transition.energy_detuning * gamma_to_eV - (material.isotope.energy + detuning_gamma * gamma_to_eV)) - 1i;
    // which can be reformulated to 
    // divisor = 2.0 * (transition.energy_detuning - detuning_gamma) - 1i;
    Eigen::Matrix2cd SumTransitions(const transitions::Transitions& hyperfine_transitions, const double detuning_gamma)
    {
      Eigen::Matrix2cd N_matrix = Eigen::Matrix2cd::Zero();

      // do not parallelize as the mutex not working here
      // the NuclearScatteringLength function is parallel anyways
      std::for_each(hyperfine_transitions.begin(), hyperfine_transitions.end(),
        [&](const transitions::Transition& transition)
        {
          const Complex divisor = 2.0 * (transition.energy_detuning - detuning_gamma) - 1.0i * transition.broadening;

          N_matrix += transition.weight * transition.lamb_moessbauer * transition.transition_polarisation_matrix / divisor;
        }
      );

      return N_matrix;
    }


    // most time consuming function in NEXUS
    ScatteringMatrix2 ScatteringLengthTransitions(
      const MoessbauerIsotope* const isotope,
      transitions::Transitions hyperfine_transitions,
      const double lamb_moessbauer,
      const std::vector<double>& detuning_grid)
    {
      ScatteringMatrix2 nuclear_scattering_matrix(detuning_grid.size(), Eigen::Matrix2cd::Zero());

      if (isotope->isotope.compare("none") != 0)
      {
        const Complex factor = isotope->kvector / (4.0 * constants::kPi) * isotope->nuclear_cross_section
            * (1.0 + 1.0i * isotope->interference_term->value);

        std::transform(NEXUS_EXECUTION_POLICY detuning_grid.begin(), detuning_grid.end(), nuclear_scattering_matrix.begin(),
          [&](const double& detuning_gamma)
          {
            return factor * SumTransitions(hyperfine_transitions, detuning_gamma);
          }
        );
      }

      return nuclear_scattering_matrix;
    }


    // material N matrix
    // only valid for one isotope transition in the material, i.e. the isotope of the experiment
    ScatteringMatrix2 ScatteringLength(Material* const material, const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning_grid, const bool calc_transitions)
    {
      if (calc_transitions)
        material->CalcTransitions();

      ScatteringMatrix2 nuclear_scattering_matrix(detuning_grid.size(), Eigen::Matrix2cd::Zero());
      
      if (material->isotope->isotope.compare(isotope->isotope) == 0 ||
          isotope->isotope.compare("none") != 0)
      {
        nuclear_scattering_matrix = ScatteringLengthTransitions(material->isotope, material->hyperfine_transitions, material->lamb_moessbauer->value, detuning_grid);
      }

      return nuclear_scattering_matrix;
    }

  } // namespace nuclear

}  // namespace scattering_length
