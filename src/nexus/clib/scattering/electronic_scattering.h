// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of pure electronic scattering cross sections.
// energy in eV
// cross sections in m^2
// 
// Klein-Nishina formula, taken from original work of Klein and Nishina (1929).
// Klein-Nishina scattering covers Thomson and Compton scattering.
// ElectronicCrossSection is Klein Nishina and Photo effect cross sections.


#ifndef NEXUS_ELECTRONICSCATTERING_H_
#define NEXUS_ELECTRONICSCATTERING_H_

#include <complex>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/element.h"
#include "../classes/material.h"


namespace electronic_scattering {

  /**
  Calculates the Klein-Nishina cross section.
  After [KleinNishina]_.
  Describes Thomson scattering of low energy photons and Compton scattering of high energy photons.

  Also see `<https://en.wikipedia.org/wiki/Klein-Nishina_formula>`_.

  Args:
     element (:class:`Element`): Element for which the cross section is calculated.
     energy (float): X-ray energy (eV).

  Returns:
     float: Klein-Nishina cross section (m :sup:`2`).
  */
  double KleinNishinaCrossSection(const Element& element, const double energy);

  /**
  Alias for the Klein-Nishina cross section.
  
  Also see `<https://en.wikipedia.org/wiki/Klein-Nishina_formula>`_.

  Args:
     element (:class:`Element`): Element for which the cross section is calculated.
     energy (float): X-ray energy (eV).

  Returns:
     float: Klein-Nishina cross section (m :sup:`2`).
  */
  double ComptonCrossSection(const Element& element, const double energy);

  /**
  Alias for the Klein-Nishina cross section.

  Also see `<https://en.wikipedia.org/wiki/Klein-Nishina_formula>`_.

  Args:
     element (:class:`Element`): Element for which the cross section is calculated.
     energy (float): X-ray energy (eV).

  Returns:
     float: Klein-Nishina cross section (m :sup:`2`).
  */
  double ThomsonCrossSection(const Element& element, const double energy);

  /**
  Calculates the photoelectric cross section.
  After [Smith]_.
  Valid in the range from Z = 1 to 100 and :math:`E_{\gamma}` = 1 keV to 100 MeV.

  Args:
     element (:class:`Element`): Element for which the cross section is calculated.
     energy (float): X-ray energy (eV).

  Returns:
     float: Photo effect cross section (m :sup:`2`).
  */
  double PhotoCrossSection(const Element& element, const double energy);

  /**
  Calculates the total electronic absorption scattering cross section for X-ray photons
  of energy :math:`E_{\gamma}` = 1 keV to 100 MeV and element up to Z = 86.
  Returns the sum of the Klein-Nishma cross section and the photoelectric cross section.
  Pair production at high energies is not included.
  If you are interested in pair production cross sections see Maximon and Borsellino-Ghizzetti equations.

  Args:
     element (:class:`Element`): Element for which the cross section is calculated.
     energy (float): X-ray energy (eV).

  Returns:
     float: Total absorption scattering cross section (m :sup:`2`).
  */
  double ElectronicCrossSection(const Element& element, const double energy);

}  // namespace electronic_scattering

#endif // ELECTRONICSCATTERING_H_
