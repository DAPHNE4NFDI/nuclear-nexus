// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module scattering_length

%{
    #define SWIG_FILE_WITH_INIT
    #include "scattering_length.h"
%}

%rename(ElectronicScatteringLength) scattering_length::electronic::ScatteringLength;

%rename(ElectronicScatteringLengthTheory) scattering_length::electronic::ScatteringLengthTheory;

%rename(ElectronicScatteringLengthCXRO) scattering_length::electronic::ScatteringLengthCXRO;

%rename(ElectronicScatteringLengthMatrix) scattering_length::electronic::ScatteringLengthMatrix;

%rename(NuclearScatteringLength) scattering_length::nuclear::ScatteringLength;

%include "scattering_length.h"
