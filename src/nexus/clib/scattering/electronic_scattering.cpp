// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "electronic_scattering.h"

#include <complex>
#include <cmath>

#include "../nexus_definitions.h"
#include "../math/constants.h"
#include "../math/conversions.h"
#include "../scattering/photoeffect_maps.h"


namespace electronic_scattering {

  using namespace std::complex_literals;

  double KleinNishinaCrossSection(const Element& element, const double energy)
  {
    const double x = energy / (constants::kElectronMass * 1.0e6); // MeV to eV

    const double cross_section = 2.0 * constants::kPi * pow(constants::kElectronRadius, 2) * element.atomic_number *
      ((1.0 + x) / (x * x) * (2.0 * (1.0 + x) / (1.0 + 2.0 * x) - log(1.0 + 2.0 * x) / x)
       + log(1.0 + 2.0 * x) / (2.0 * x) - (1.0 + 3.0 * x) / ((1.0 + 2.0 * x) * (1.0 + 2.0 * x))
      );

    return cross_section;
  }


  double ComptonCrossSection(const Element& element, const double energy)
  {
    return KleinNishinaCrossSection(element, energy);
  }


  double ThomsonCrossSection(const Element& element, const double energy)
  {
    return KleinNishinaCrossSection(element, energy);
  }


  double PhotoCrossSection(const Element& element, const double energy)
  {
    const auto map_iterator = photoeffect_maps::photoeffect_coefficients_map.find(element.element);

    const double energy_keV = energy / 1000.0;

    double sigma = 0.0;
  
    double p1 = 0.0, p2 = 0.0, p3 = 0.0, p4 = 0.0, p5 = 0.0;

    double energy_max = 0.0;

    // find coefficients for the energy interval in which the parameter energy is.
    for (const auto& inner_vector : map_iterator->second)
    {
      energy_max = inner_vector[0];
    
      if (energy_max > energy_keV)
      {
        p1 = inner_vector[1];
        p2 = inner_vector[2];
        p3 = inner_vector[3];
        p4 = inner_vector[4];
        p5 = inner_vector[5];
      
        sigma = 1.0e-28 * exp(p1 + (p2 + p3 * energy_keV + p4 * pow(energy_keV, 2) + p5 * pow(energy_keV, 3)) * log(energy_keV));
        
        break;
      }
    }

    return sigma;
  }


  double ElectronicCrossSection(const Element& element, const double energy)
  {
    return KleinNishinaCrossSection(element, energy) + PhotoCrossSection(element, energy);
  }

}  // namespace electronicscattering
