// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of a transmission and layer factors and matrices.
// A transmission or layer matrix is the matrix exponential
// T = exp(i*F*z) or L = exp(i*F*z)
// with (factors or matrices)
// propagation matrix F
// transmission matrix T in forward direction
// layer matrix L in grazing incidence geometry


#ifndef NEXUS_LAYER_MATRIX_H_
#define NEXUS_LAYER_MATRIX_H_

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../scattering/propagation_matrix.h"


namespace electronic {

  namespace forward {
    /*
    Pure electronic transmission factor T.
    Scalar electronic version of Eq. (4.16),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Complex TransmissionFactor(const Complex propagation_factor, const double thickness);

  }  // namespace forward


  namespace grazing {
    /*
    Pure electronic layer matrix L.
    2x2 matrix, electronic version of Eq. (4.23),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix2cd LayerMatrix(const Eigen::Matrix2cd& propagation_matrix, const double thickness);

    /*
    Pure electronic layer matrix L of an infinite layer.
    2x2 matrix, electronic version of Eq. (4.35),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix2cd LayerMatrixBottom(const Complex scattering_factor, const double kvector_z);

    /*
    Pure electronic exponential of roughness matrix W.
    2x2 matrix, electronic version of Eq. (4.66),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    W matrix after R. Roehlsberger, Hyperfine Interactions 1999, Eq. (C.2).
    */
    Eigen::Matrix2cd RoughnessLayerMatrix(
      const Complex scattering_factor_1,
      const Complex scattering_factor_2,
      const double kvector_z,
      const double sigma);

  }  // namespace grazing

}  // namespace electronic


namespace nuclear {

  /*
  Math functions for a LayerMatrix type.

  LayerMatrix2 PaddingLayerMatrix2(const LayerMatrix2& layer_matrix, const int grid_size, const int padding_size);

  LayerMatrix2 MultiplyLayerMatrix2(const LayerMatrix2& layer_matrix_1, const LayerMatrix2& layer_matrix_2);

  LayerMatrix4 MultiplyLayerMatrix4(const LayerMatrix4& layer_matrix_1, const LayerMatrix4& layer_matrix_2);

  LayerMatrix2 AddLayerMatrices(const LayerMatrix2& layer_matrix_1, const LayerMatrix2& layer_matrix_2);

  LayerMatrix2 AddLayerMatrices(const LayerMatrix2& layer_matrix_1, const Eigen::Matrix2cd& matrix_2);

  LayerMatrix2 SubtractLayerMatrices(const LayerMatrix2& layer_matrix_1, const LayerMatrix2& layer_matrix_2);

  LayerMatrix2 SubtractLayerMatrices(const LayerMatrix2& layer_matrix_1, const Eigen::Matrix2cd& matrix_2);

  template <class T>
  LayerMatrix2 ScaleLayerMatrix(const LayerMatrix2& layer_matrix, const T factor);

  template <class T>
  LayerMatrix2 MultiplyVectorLayerMatrix2(const LayerMatrix2& layer_matrix, const std::vector<T>& vector);

  template <class T>
  LayerMatrix2 DivideVectorLayerMatrix2(const LayerMatrix2& layer_matrix_1, const std::vector<T>& vector);
  */

  namespace forward {

    /*
    Transmission matrix T.
    2x2 matrix, Eq. (4.16),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix2cd TransmissionMatrix(const Eigen::Matrix2cd& propagation_matrix, const double thickness);

    LayerMatrix2 TransmissionMatrix(const PropagationMatrix2& propagation_matrix, const double thickness);

  }  //namespace forward

  namespace grazing {

    /*
    Layer matrix L.
    4x4 matrix, Eq. (4.23),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix4cd LayerMatrix(const Eigen::Matrix4cd& propagation_matrix, const double thickness);

    LayerMatrix4 LayerMatrix(const PropagationMatrix4& propagation_matrix, const double thickness);

    /*
    Layer matrix L of an infinite layer.
    4x4 matrix, Eq. (4.35),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, section A.3
    R. Roehlsberger, Hyperfine Interactions 1999, section 4.4
    special optimized implementation, that's why the ScatteringMatrix is passed.
    */
    LayerMatrix4 LayerMatrixBottom(const ScatteringMatrix2& scattering_matrix, const double kvector_z);

    /*
    Exponential of roughness matrix W.
    4x4 matrix, Eq. (4.66)
    implemented as Eq. (4.69), up to second order, n = 2.
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    LayerMatrix4 RoughnessLayerMatrix(const PropagationMatrix4& F_matrix_1, const PropagationMatrix4& F_matrix_2, const double sigma);

  }  // namespace grazing

}  // namespace nuclear

//Eigen::MatrixXcd LayerMatrixX(const Eigen::MatrixXcd& matrix, const double z);

#endif // NEXUS_LAYER_MATRIX_H_
