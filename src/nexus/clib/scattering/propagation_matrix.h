// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the propagation matrix/factor
// F = k + f
// where
// k is the photon wave vector 
// f is the scattering matrix
// see R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, page 69 for dynamic theory.


#ifndef NEXUS_PROPAGATION_MATRIX_H_
#define NEXUS_PROPAGATION_MATRIX_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../scattering/scattering_matrix.h"


namespace electronic {

  namespace forward {

    /*
    Pure electronic propagation factor F.
    Scalar electronic version of Eq. (4.6),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Complex PropagationFactor(const Complex f, const double kvector);

  }  // namespace forward

  namespace grazing {

    /*
    Pure electronic propagation factor F in grazing geometry.
    2x2 matrix version of Eq. (4.24),
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix2cd PropagationMatrix(const Complex f, const double kvector_z);

  }  // namespace grazing 
}  // namespace electronic


namespace nuclear {

  namespace forward {
    /*
    Propagation matrix F, Eq. (4.6).
    2x2 matrix
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix2cd PropagationMatrix(const Eigen::Matrix2cd& scattering_matrix, const double kvector);

    PropagationMatrix2 PropagationMatrix(const ScatteringMatrix2& scattering_matrix, const double kvector);

  }  // namespace forward

  namespace grazing {
    /*
    Propagation matrix F, Eq. (4.24).
    4x4 matrix
    R. Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation.
    */
    Eigen::Matrix4cd PropagationMatrix(const Eigen::Matrix4cd& scattering_matrix, const double kvector_z);

    PropagationMatrix4 PropagationMatrix(const ScatteringMatrix2& scattering_matrix, const double kvector_z);

  }  // namespace grazing
}  // namespace nuclear


// higher order scattering, use for diffraction
//typedef std::vector<Eigen::MatrixXcd> PropagationMatrixX;
// 
//Eigen::MatrixXcd F_matrixX(const Eigen::MatrixXcd& matrix);
//PropagationMatrixX F_matrixX(ScatteringMatrix scatteringmatrix, const double kvector);

#endif // NEXUS_PROPAGATION_MATRIX_H_
