// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "scattering_matrix.h"

#include <cmath>
#include <complex>
#include <algorithm>

#include "../math/constants.h"
#include "../math/conversions.h"
#include "../nexus_definitions.h"
#include "../scattering/scattering_length.h"
#include "../math/matrix.h"

using namespace std::complex_literals;


namespace electronic {

  /*
  This is the sum over all coherent forward scattering matrices weighted by the number density, Eq. (4.4).
  Electronic scalar case only.
  */
  Complex MaterialWeightedScatteringLength(const Material* const material, const double energy)
  {
    Complex material_scattering_factor(0.0, 0.0);

    for (const Element& element : material->elements)
      material_scattering_factor += element.number_density * scattering_length::electronic::ScatteringLength(element, energy, scattering_length::atomic_scattering_factors_cxro);

    return material_scattering_factor;
  }

  Eigen::Matrix2cd MaterialWeightedScatteringLengthMatrix(const Material* const material, const double energy)
  {
    Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Zero();

    matrix(0, 0) = MaterialWeightedScatteringLength(material, energy);

    matrix(1, 1) = matrix(0, 0);

    return matrix;
  }

  // material E matrix, already weighted by elements number_density in material
  // Sturhahn PRB Eq. 4, factor of 2*pi not taken, see also comment ElectronicScatteringFactor
  // Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. 4.11
  ScatteringMatrix2 MaterialWeightedScatteringLengthMatrix(const Material* const material, const MoessbauerIsotope* const isotope,
    const std::vector<double>& detuning_grid)
  {
    const double energy = isotope->energy;

    const double gamma_to_eV = isotope->gamma;

    ScatteringMatrix2 matrix{};

    for (const double& detuning_gamma : detuning_grid)
      matrix.push_back(MaterialWeightedScatteringLengthMatrix(material, energy + detuning_gamma * gamma_to_eV));

    return matrix;
  }

  namespace forward {

    // scalar forward scattering matrix f, Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq 4.4
    Complex ScatteringFactor(const Material* const material, const double energy)
    {
      return 2.0 * constants::kPi / conversions::EnergyToKvector(energy) * MaterialWeightedScatteringLength(material, energy);
    }

    Complex RefractiveIndex(const Material* const material, const double energy)
    {
      return 1.0 + ScatteringFactor(material, energy) / conversions::EnergyToKvector(energy);
    }

  }  // namespace forward

  namespace grazing {

    // complex scalar f, Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, Eq. 4.25
    Complex ScatteringFactor(const Material* const material, const double energy, const double kvector_z)
    {
      return 2.0 * constants::kPi / kvector_z * MaterialWeightedScatteringLength(material, energy);
    }

    double CriticalAngle(const Material* const material, const double energy)
    {
      return sqrt(2.0 * abs(real(forward::ScatteringFactor(material, energy) / conversions::EnergyToKvector(energy) )) );
    }

  }  // namespace grazing

}  // namespace electronic


namespace nuclear {

  ScatteringMatrix2 MaterialWeightedNuclearScatteringLength(Material* const material, const MoessbauerIsotope* const isotope,
    const std::vector<double>& detuning_grid, const bool calc_transitions)
  {   
    ScatteringMatrix2 nuclear_scattering_matrix = matrix::Scale(
      scattering_length::nuclear::ScatteringLength(material, isotope, detuning_grid, calc_transitions), material->isotope_number_density);

    return nuclear_scattering_matrix;
  }

  // material M matrix
  // already weighted by abundance and number density
  ScatteringMatrix2 MaterialWeightedCoherentScatteringLength(Material* const material, const MoessbauerIsotope* const isotope,
    const std::vector<double>& detuning_grid, const bool calc_transitions)
  {
    const ScatteringMatrix2 E = electronic::MaterialWeightedScatteringLengthMatrix(material, isotope, detuning_grid);

    const ScatteringMatrix2 N = MaterialWeightedNuclearScatteringLength(material, isotope, detuning_grid, calc_transitions);
    
    return matrix::Add(N, E);
  }

  namespace forward {

    // f matrix = 2pi/kvec * M
    ScatteringMatrix2 ScatteringMatrix(Material* const material, const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning_grid, const bool calc_transitions)
    {
      const double pre_factor = 2.0 * constants::kPi / isotope->kvector;

      ScatteringMatrix2 f_matrix = matrix::Scale(
        MaterialWeightedCoherentScatteringLength(material, isotope, detuning_grid, calc_transitions),
        pre_factor
      );

      return f_matrix;
    }

  }  // namespace forward

  namespace grazing {

    // f matrix = 2pi/kvec_z * M
    ScatteringMatrix2 ScatteringMatrix(Material* const material, const MoessbauerIsotope* const isotope,
      const std::vector<double>& detuning_grid, const double kvector_z, const bool calc_transitions)
    {
      const double pre_factor = 2.0 * constants::kPi / kvector_z;

      ScatteringMatrix2 f_matrix = matrix::Scale(
        MaterialWeightedCoherentScatteringLength(material, isotope, detuning_grid, calc_transitions),
        pre_factor
      );

      return f_matrix;
    }

  }  // namespace grazing

}  // namespace nuclear
