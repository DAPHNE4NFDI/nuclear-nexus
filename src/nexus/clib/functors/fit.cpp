// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.



#include "fit.h"

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>
#include <random>

#include <Eigen/Dense>

#ifdef NEXUS_USE_PAGMO
#include <pagmo/pagmo.hpp>
#endif

#include "../utilities/errors.h"
#include "../utilities/progress.h"
#include "../utilities/python_print.h"
#include "../utilities/file_output.h"

#include "../math/constants.h"
#include "../math/fourier.h"
#include "../math/intensity.h"

#include "../classes/nxvariable.h"



// performs the fit
void Fit::operator() ()
{
  if (PopulateFitHandler() == false)
    return;

  // initial check for FitOptions and the GlobalFitHandler
  if (options->InitialCheck() == false)
  {
    errors::ErrorMessage("Fit", "Error in Options. No fit performed.");

    return;
  }

  //check that valid intensity data are provided
  for (FitMeasurement* measurement : measurements)
  {
    if (measurement->data_size <= 0)
    {
      const auto it = std::find(measurements.begin(), measurements.end(), measurement);

      const auto num = std::distance(measurements.begin(), it);

      std::string output = "Measurement at index ";
      output.append(std::to_string(num));
      output.append(" has no valid intensity data");

      errors::WarningMessage("Fit::operator()", output);
    }
  }

  if (!Initialize(options->global_fit))
    return;

  fit_parameter_pointers = GetFitParameterPointers();

  num_fit_parameters = fit_parameter_pointers.size();
  
  initial_parameters = GetInitialParameters();

  lower_bounds = GetLowerBounds();
  
  upper_bounds = GetUpperBounds();
  
  ids = GetIds();

  fit_equalities = GetFitEqualities();

  // write special array needed for NLopt
  nlopt_fit_parameters = new double[initial_parameters.size()];
  
  for (size_t i = 0; i < initial_parameters.size(); ++i)
    nlopt_fit_parameters[i] = initial_parameters[i];

  python_print::output(StartOutput().str());

  // set the appropriate fit pointers for the different modules (ceres, nlopt, pagmo)
  // the fit_parameter_pointers in the fit_measurement are pointing to the actual data in the model
  // for Nlopt an additional pointer to the parameter array passed to NLopt is needed.
  for (FitMeasurement* fit_measurement : measurements)
  {
    fit_measurement->fit_parameter_pointers = fit_parameter_pointers; // ceres and nlopt

    fit_measurement->nlopt_fit_parameters_ptr = nlopt_fit_parameters;  // nlopt

    fit_measurement->fit_equalities = fit_equalities;  // ceres, nlopt and pagmo

    fit_measurement->inequality = inequality; // ceres, nlopt, pagmo
  }

  ResetErrorData();

  RunFit(options->output);

  RunErrorAnalysis(options->error_method);
  
  for (FitMeasurement* measurement : measurements)
  {
    measurement->SetFitEqualities();

    measurement->experiment->Update();
  }

  total_sum_user_residuals = 0.0;

  total_cost = 0.0;

  // recalculate for best parameters
  for (FitMeasurement* fit_measurement : measurements)
  {
    fit_measurement->sum_user_residuals = fit_measurement->Residuals(); // Residuals call the () operator.

    total_sum_user_residuals += fit_measurement->sum_user_residuals;

    total_cost += fit_measurement->Cost();
  }

  const std::stringstream ss = EndOutput();
  
  python_print::output(ss.str());

  if (options->file_output)
    file_output::write_file("fit_id_" + id + ".txt", ss.str(), true);

  if (non_dependent_variables.size() > 0)
    errors::WarningMessage("Fit", "Fit model does not depend on " + std::to_string(non_dependent_variables.size()) + " fit parameters.");

  delete[] nlopt_fit_parameters;
}


void Fit::ResetErrorData()
{
  inverse_hessian = Eigen::MatrixXd::Zero(num_fit_parameters, num_fit_parameters);

  non_dependent_variables.clear();
  
  correlation_matrix = Eigen::MatrixXd::Zero(num_fit_parameters, num_fit_parameters);

  covariance_matrix = Eigen::MatrixXd::Zero(num_fit_parameters, num_fit_parameters);

  fit_parameter_errors.resize(0);
}


// Scans the whole fit environment for all passed Vars and adds them to the fit problem
bool Fit::PopulateFitHandler()
{
  bool ret = true;

  ClearFitVariables();

  for (FitMeasurement* meas : measurements)
    meas->PopulateFitHandler(this);

  for (Var* const var : external_fit_variables)
    PrepareFitVariable(var);

  if (GetFitVariables().size() == 0 && external_fit_variables.size() == 0)
  {
    errors::WarningMessage("Fit::operator()", "No fit parameter found. No fit performed.");

    ret = false;
  }

  return ret;
}


std::stringstream Fit::StartOutput() const
{
  std::stringstream ss("");

  ss << "\nRun Fit instance with id: " << id << " \n";

  ss << "\nStarting fit with " << measurements.size() << " measurement data set(s) and " << fit_parameter_pointers.size() << " fit parameter(s):\n\n";

  ss << "  no. |                           id |       initial value |              min |              max\n";

  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << ids[i];
    ss << " | " << std::setw(19) << initial_parameters[i];
    ss << " | " << std::setw(16) << lower_bounds[i];
    ss << " | " << std::setw(16) << upper_bounds[i];
    ss << "\n";
  }

  ss << "\nUsing " << fit_equalities.size() << " equality constraint(s) on parameter(s):\n";

  if (fit_equalities.size() > 0)
    ss << "\n  no. |                           id |               value\n";

  for (size_t i = 0; i < fit_equalities.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << fit_equalities[i]->id;
    ss << " | " << std::setw(19) << fit_equalities[i]->equality->Function();
    ss << "\n";
  }

  if (inequality == nullptr)
  {
    ss << "\nUsing 0 inequality constraint(s).\n";
  }
  else
  {
    ss << "\nUsing " << inequality->NumFunction() << " inequality constraint(s).\n";
  }

  return ss;
}


std::stringstream Fit::EndOutput() const
{
  std::stringstream ss("");

  ss << "\nFit performed with algorithm:\n";

  ss << options->method << std::endl;

  if (std::find(options->pagmo_methods.begin(), options->pagmo_methods.end(), options->method) != options->pagmo_methods.end())
  {
    ss << "Local algorithm:\n";

    ss << options->local << std::endl;
  }

  if (options->error_method == "Gradient" || options->error_method == "Bootstrap")
  {
    ss << "Error analysis:\n" << options->error_method << std::endl;
    
    if (options->error_method == "Bootstrap")
      ss << "Bootstrap method:\n" << options->Bootstrap->method << std::endl;
  }

  ss << "\nUsing " << fit_parameter_pointers.size() << " fit parameter(s):\n";

  if (fit_parameter_errors.size() > 0)
  {
    ss << "\n  no. |                           id |          fit value |   +/- std dev | initial value |          min |          max\n";

    for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    {
      ss << " " << std::setw(4) << i;
      ss << " | " << std::setw(28) << ids[i];
      ss << " | " << std::setw(18) << *fit_parameter_pointers[i];
      ss << " | " << std::setw(13) << fit_parameter_errors[i];
      ss << " | " << std::setw(13) << initial_parameters[i];
      ss << " | " << std::setw(12) << lower_bounds[i];
      ss << " | " << std::setw(12) << upper_bounds[i];
      ss << "\n";
    }
  }
  else
  {
    ss << "\n  no. |                           id |           fit value |       initial value |              min |              max\n";
    
    for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    {
      ss << " " << std::setw(4) << i;
      ss << " | " << std::setw(28) << ids[i];
      ss << " | " << std::setw(19) << *fit_parameter_pointers[i];
      ss << " | " << std::setw(19) << initial_parameters[i];
      ss << " | " << std::setw(16) << lower_bounds[i];
      ss << " | " << std::setw(16) << upper_bounds[i];
      ss << "\n";
    }
  }

  if (fit_parameter_errors.size() > 0)
  {
    ss << "\nCorrelation matrix:\n\n";

    ss << "  no. | ";

    for (size_t i = 0; i < num_fit_parameters; i++)
      ss << " " << std::setw(7) << i << " ";

    ss << "\n -----|";

    for (size_t i = 0; i < num_fit_parameters; i++)
      ss << "---------";

    ss << std::endl;

    for (size_t i = 0; i < num_fit_parameters; i++)
    {
      ss << " " << std::setw(4) << i << " | ";

      for (size_t j = 0; j < num_fit_parameters; j++)
        ss << " " << std::setw(7) << std::fixed << std::setprecision(3) << correlation_matrix(i, j) << " ";

      ss << std::endl;
    }
  }

  if (non_dependent_variables.size() > 0)
  {
    ss << "\nFound " << non_dependent_variables.size() << " parameters without influence on the fit model:\n\n";

    ss << "  no. |                           id\n";

    for (size_t i = 0; i < non_dependent_variables.size(); i++)
    {
      ss << " " << std::setw(4) << non_dependent_variables[i];
      ss << " | " << std::setw(28) << ids[non_dependent_variables[i]];
      ss << std::endl;
    }

    ss << "\nIt is recommended to remove these parameters from the fit or to change the start parameters.\n";
  }

  ss << "\nUsing " << fit_equalities.size() << " equality constraint(s) on parameter(s):\n";

  if (fit_equalities.size() > 0)
    ss << "\n  no. |                           id |               value\n";

  for (size_t i = 0; i < fit_equalities.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << fit_equalities[i]->id;
    ss << " | " << std::setw(19) << fit_equalities[i]->value;
    ss << "\n";
  }

  if (inequality == nullptr)
    ss << "\nand 0 inequality constraint(s).\n";
  else
    ss << "\nand " << inequality->NumFunction() << " inequality constraint(s).\n";

  if (total_sum_user_residuals > 1.0e298)
  {
    ss << "\nInequality constraint not satisfied.\n";
  }
  else
  {
    //ss << "\nTotal cost = " << std::scientific << total_sum_user_residuals * 0.5 << "\n";
    ss << "\nTotal cost = " << std::scientific << total_cost << "\n";
  }

  ss << "\ncost for each FitMeasurement is:\n";

  ss << "\n  no. |                           id |                cost |                   %\n";

  int i = 0;

  for (FitMeasurement* fit_measurement : measurements)
  {
    ss << " " << std::setw(4) << i++;
    ss << " | " << std::setw(28) << fit_measurement->id;
    //ss << " | " << std::setw(19) << std::scientific << 0.5 * fit_measurement->sum_user_residuals;
    ss << " | " << std::setw(19) << std::scientific << fit_measurement->Cost();
    ss << " | " << std::setw(19) << std::fixed << std::setprecision(3) << fit_measurement->sum_user_residuals / total_sum_user_residuals * 100;
    ss << "\n";
  }

  ss << "\nFit instance with id \"" << id << "\" finished.\n";

  return ss;
}


// perform the fit
void Fit::RunFit(const bool output)
{
  CallMethod(options->method, options->fit_module, output);

  if (options->global_fit)
    CallMethod(options->local, options->fit_module_local, output);
}



void Fit::CallMethod(const std::string method, const OptimizerModule fit_module, const bool output)
{
  if (fit_module == OptimizerModule::Ceres)
  {
    if (output)
      python_print::output("\nCalling ceres solver with fit method " + method + "\n");

    CeresFit(method, false, output);
  }
  else if (fit_module == OptimizerModule::NLopt)
  {
    if (output)
      python_print::output("\nCalling NLopt solver with fit method " + method + "\n");

    NLoptFit(method, output);
  }
  else if (fit_module == OptimizerModule::Pagmo)
  {
    if (output)
      python_print::output("\nCalling Pagmo solver with fit method " + method + "\n");

    PagmoFit(method, output);
  }
  else
  {
    errors::WarningMessage("Fit", "Fit method not found in any of the available solver modules.");
  }
}



// fit routines

void Fit::CeresFit(const std::string method, const bool error, const bool output)
{
  ceres::Problem ceres_problem;

  ceres::NumericDiffOptions ceres_numeric_cost_options;

  ceres::Solver::Options ceres_solver_options;

  ceres::Solver::Summary ceres_summary;

  SetCeresOptions(method, ceres_solver_options, ceres_numeric_cost_options);

  if (!output)
    ceres_solver_options.minimizer_progress_to_stdout = false;

  // create cost function for each observable
  for (FitMeasurement* fit_measurement : measurements)
  {
    ceres::DynamicNumericDiffCostFunction<FitMeasurement, ceres::CENTRAL>* cost_function =
      new ceres::DynamicNumericDiffCostFunction<FitMeasurement, ceres::CENTRAL>(
        fit_measurement, ceres::DO_NOT_TAKE_OWNERSHIP, ceres_numeric_cost_options);

    // add a parameter for each entry in vectors of ceres fit parameter pointers
    for (size_t i = 0; i < num_fit_parameters; ++i)
      cost_function->AddParameterBlock(1);

    cost_function->SetNumResiduals(static_cast<int>(fit_measurement->data_size));

    // ceres::internal::ResidualBlock* ceres_block =
    ceres_problem.AddResidualBlock(cost_function, NULL, fit_parameter_pointers);
  }

  // set boundaries for ceres solver
  // keep boundary index at zero, as there is only one ParameterBlock added for each fit parameter to the cost_function
  for (size_t i = 0; i < num_fit_parameters; ++i)
  {
    ceres_problem.SetParameterLowerBound(fit_parameter_pointers[i], 0, lower_bounds[i]);
    ceres_problem.SetParameterUpperBound(fit_parameter_pointers[i], 0, upper_bounds[i]);
  }

  ceres::Solve(ceres_solver_options, &ceres_problem, &ceres_summary);

  if (output)
  {
    if (options->report == "Brief")
    {
      python_print::output(ceres_summary.BriefReport());
    }
    else if (options->report == "Full")
    {
      python_print::output(ceres_summary.FullReport());
    }

    std::cout << std::endl;
  }

  if(error)
    CeresErrors(ceres_problem);
}



// The NLopt lib only takes a reference to a double vector, while ceres takes a pointer to a vector of pointers.
// So, we have to pass the initial values as an array of doubles. 
// In the cost functions we have to use this array to set the new fit_parameters, similar to is done with Ceres with pointers.
// However, NLopt internally copies the parameter vector in the cpp interface. use the c functions directly!
// And in some NLopt functions, it seems that data are copied again, so the trick with the pointers is not working for some algorithms.
// Or at least I do not see why certain methods do not work at all, like BOBYA.

#ifdef NEXUS_USE_NLOPT
static int nlopt_iteration = 0;
static bool nlopt_output = true;
#endif

// function to calculate the squared total residual of the fit method
#ifdef NEXUS_USE_NLOPT
double NLoptFitModel(unsigned n, const double* params, double* grad, void* fit_class)
{
  const Fit* const fit_object = static_cast<Fit*>(fit_class);

  double total_user_residual = 0.0;

  for (FitMeasurement* fit_measurement : fit_object->measurements)
  {
    fit_measurement->SetNLoptFitParameterPointers();

    total_user_residual += fit_measurement->Residuals();
  }

  ++nlopt_iteration;

  if (nlopt_output)
    if ((nlopt_iteration-1) % 10 == 0)
      std::cout <<  std::setw(6) << nlopt_iteration << std::endl;

  return total_user_residual;
}
#endif

#ifdef NEXUS_USE_NLOPT
void Fit::NLoptFit(const std::string method, const bool output)
{
  nlopt_output = output;

  // SOLVER OPTIONS
  nlopt_algorithm algorithm = NLOPT_LN_SBPLX;

  if (method == "Newuoa")
  {
    algorithm = NLOPT_LN_NEWUOA_BOUND;
  }
  else if (method == "Subplex")
  {
    algorithm = NLOPT_LN_SBPLX;
  }

  // create the optimizer object with specific method
  nlopt_opt opt = nlopt_create(algorithm, static_cast<unsigned int>(initial_parameters.size()));

  SetNloptOptions(opt);

  // set boundaries
  nlopt_set_lower_bounds(opt, lower_bounds.data());

  nlopt_set_upper_bounds(opt, upper_bounds.data());

  // set the solver to minimize the objective function
  nlopt_set_min_objective(opt, NLoptFitModel, this);

  nlopt_iteration = 0;

  std::cout << "  iteration:" << std::endl;

  nlopt_result ret = nlopt_optimize(opt, nlopt_fit_parameters, &cost);

  // definition of cost is 1/2 * sum over squared residuals in nexus
  cost *= 0.5;

  // needed after output from optimizer 
  std::cout << std::endl;

  const int nlopt_iterations = nlopt_get_numevals(opt);

  nlopt_destroy(opt);

  // create solver output  
  if (ret < 0)
    errors::WarningMessage("Fit", "Error in module NLopt with method " + method + ". Error ret = " + std::to_string(ret));

  if (ret == NLOPT_STOPVAL_REACHED)
  {
    python_print::output("Termination: stop value reached.");
  }
  else if (ret == NLOPT_FTOL_REACHED)
  {
    python_print::output("Termination: function tolerance reached.");
  }
  else if (ret == NLOPT_XTOL_REACHED)
  {
    python_print::output("Termination: parameter tolerance reached.");
  }
  else if (ret == NLOPT_XTOL_REACHED)
  {
    python_print::output("Termination: maximum iterations reached.");
  }
  else if (ret == NLOPT_MAXTIME_REACHED)
  {
    python_print::output("Termination: maximum time reached.");
  }
  
  std::stringstream ss;
  ss << "\n  cost = " << std::scientific << cost;
  python_print::output(ss.str());
  python_print::output("  iterations: " + std::to_string(nlopt_iterations));

  // Set best parameters
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = nlopt_fit_parameters[i];
}
#else
void Fit::NLoptFit(const std::string method, const bool output)
{
  errors::WarningMessage("Fit", "Nexus was compiled without NLopt support.");
}
#endif


// Pagmo
// pagmo::vector_double is just pagmos internal name for std::vector<double>

#ifdef NEXUS_USE_PAGMO
class PagmoFunctor {
public:
  // The default constructor has to exist OR all values have to be passed with default values
  PagmoFunctor(Fit* const fit = nullptr) : fit(fit) {};

  // Implementation of the objective function.
  // fitness defined in pagmo.
  pagmo::vector_double fitness(const pagmo::vector_double& inputs) const
  {
    double total_user_residual = 0.0;

    for (FitMeasurement* fit_measurement : fit->measurements)
    {
      fit_measurement->SetPagmoFitParameterPointers(inputs);

      total_user_residual += fit_measurement->Residuals();
    }

    return { total_user_residual };
  }

  // Implementation of the box bounds.
  // get_bounds name defined in pagmo.
  // no set method necessary as the fit instance can directly be used.
  std::pair<pagmo::vector_double, pagmo::vector_double> get_bounds() const
  {
    return { fit->lower_bounds, fit->upper_bounds};
  }

private:

  Fit* fit = nullptr;
};
#endif

#ifdef NEXUS_USE_PAGMO
void Fit::PagmoFit(const std::string method, const bool output)
{
  pagmo::problem prob{ PagmoFunctor(this) };

  pagmo::pop_size_t population = options->population;

  if (options->population == 0)
    population = 10 * (initial_parameters.size() + 1);

  unsigned int iterations = options->iterations;

  if (options->iterations == 0)
    iterations = 100;

  pagmo::algorithm algo;

  if (method == "BeeColony")
  {
    algo = pagmo::algorithm(pagmo::bee_colony(iterations));
  }
  else if (method == "CMA-ES")
  {
    algo = pagmo::algorithm(pagmo::cmaes(iterations, -1, -1, -1, -1, 0.5, options->function_tolerance, options->parameter_tolerance, false, true));
  }
  else if (method == "CompassSearch")
  {
    population = 1;

    if (options->iterations == 0)
      iterations = 500;

    algo = pagmo::algorithm(pagmo::compass_search(iterations, options->CompassSearch->start_range,
      options->CompassSearch->stop_range, options->CompassSearch->reduction_coeff));
  }
  else if (method == "DiffEvol")
  {
    algo = pagmo::algorithm(pagmo::de(iterations, options->DiffEvol->F, options->DiffEvol->CR,
      options->DiffEvol->variant, options->function_tolerance, options->parameter_tolerance));
  }
  else if (method == "PagmoDiffEvol")
  {
    algo = pagmo::algorithm(pagmo::de1220(iterations, pagmo::de1220_statics<void>::allowed_variants,
      options->DiffEvol->adaptive_variant, options->function_tolerance, options->parameter_tolerance));
  }
  else if (method == "AntColony")
  {
    if (population < 64)
      population = 64;

    algo = pagmo::algorithm(pagmo::gaco(iterations));
  }
  else if (method == "GreyWolf")
  {
    algo = pagmo::algorithm(pagmo::gwo(iterations));
  }
  else if (method == "SParticleSwarm")
  {
    algo = pagmo::algorithm(pagmo::nspso(iterations));
  }
  else if (method == "ParticleSwarm")
  {
    algo = pagmo::algorithm(pagmo::pso(iterations));
  }
  else if (method == "ParticleSwarmGen")
  {
    algo = pagmo::algorithm(pagmo::pso_gen(iterations));
  }
  else if (method == "AdaptiveDiffEvol")
  {
    algo = pagmo::algorithm(pagmo::sade(iterations, options->DiffEvol->variant,
      options->DiffEvol->adaptive_variant, options->function_tolerance, options->parameter_tolerance));
  }
  else if (method == "SimpleEvol")
  {
    if (options->population == 0)
      population = 20 * (initial_parameters.size() + 1);

    if (options->iterations == 0)
      iterations = 500;

    algo = pagmo::algorithm(pagmo::sea(iterations));
  }
  else if (method == "Genetic")
  {
    algo = pagmo::algorithm(pagmo::sga(iterations));
  }
  else if (method == "Annealing")
  {
    algo = pagmo::algorithm(pagmo::simulated_annealing());
  }
  else if (method == "NaturalEvol")
  {
    algo = pagmo::algorithm(pagmo::xnes(iterations, -1, -1, -1, -1, options->function_tolerance, options->parameter_tolerance, false, true));
  }
  else if (method == "BasinHopping")
  {
    if (options->BasinHopping->inner == "Subplex")
    {
      algo = pagmo::algorithm(pagmo::mbh(pagmo::nlopt("sbplx"), options->BasinHopping->stop,
        options->BasinHopping->perturbation, pagmo::random_device::next()));
    }
    else if (options->BasinHopping->inner == "Compass")
    {
      algo = pagmo::algorithm(pagmo::mbh(pagmo::compass_search(iterations, options->CompassSearch->start_range,
        options->CompassSearch->stop_range, options->CompassSearch->reduction_coeff),
        options->BasinHopping->stop, options->BasinHopping->perturbation, pagmo::random_device::next()));
    }
    else
    {
      python_print::output("Fit: Basin Hopping inner algorithm not recognized. Using Compass Search.");
      
      options->BasinHopping->inner = "Compass";

      algo = pagmo::algorithm(pagmo::mbh(pagmo::compass_search(iterations, options->CompassSearch->start_range,
        options->CompassSearch->stop_range, options->CompassSearch->reduction_coeff),
        options->BasinHopping->stop, options->BasinHopping->perturbation, pagmo::random_device::next()));
    }
  }

  // output every iteration or quiet
  algo.set_verbosity(output);

  // print start parameters
  if (method == "BasinHopping")
    python_print::output("inner algorithm: " + options->BasinHopping->inner);
  
  python_print::output("  population: " + std::to_string(population));
  python_print::output("  iterations: " + std::to_string(iterations));

  // Instantiate a population
  pagmo::population pop{ prob, population };

  // Evolve the population
  pop = algo.evolve(pop);

  // calculate cost comparable to ceres solver, see definition of ceres cost with factor 1/2
  cost = prob.fitness(pop.champion_x())[0] * 0.5;

  std::stringstream ss;
  ss << "\n  cost = " << std::scientific << cost;
  python_print::output(ss.str());

  // Set best parameters
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = pop.champion_x()[i];
}
#else
void Fit::PagmoFit(const std::string method, const bool output)
{
  errors::WarningMessage("Fit", "Nexus was compiled without Pagmo support.");
}
#endif


// Error calculation
// The covariance in the ceres solver is C(x) = (J*(x)J(x))^(-1).
// This corresponds to the inverse Hessian that Scipy.optimize.leastsq returns.
// 
// The covariance matrix is obtained from the inverse Hessian by scaling.
// The scaling factor is
// f = residual_weight * SUM_i(residual_i^2) / (number of data points - number of fit parameters)
// covariance = f * inverse Hessian
// The diagonal elements are the variances of the fit parameters.
// 
// The correlation matrix is calculated via the Pearson correlation coefficient.
// Can be calculated from the inverse Hessian or from the covariance matrix.
// correlation_ij = C_ij/sqrt(C_ii * C_jj)
// or
// correlation_ij = covar_ij/sqrt(covar_ii * covar_jj)
//
// The estimated standard deviation errors are obtained from the diagonal elements
// std_error_ii = sqrt(covar_ii)
//
void Fit::RunErrorAnalysis(const std::string error_method_)
{
  // error calculation
  if (error_method_ == "Gradient")
  {
    python_print::output("\nGradient error analysis.\n");

    CeresFit("LevMar", true, false);
  }
  else if (error_method_ == "Bootstrap")
  {

    python_print::output("\nBootstrap error analysis with " + std::to_string(options->Bootstrap->iterations) + " iterations. Please wait.\n");

    python_print::output("Bootstrap method: " + options->Bootstrap->method + "\n");

    CallBootstrap();
  }
  else
  {
    python_print::output("\nNo error analysis performed.\n");
  }

}


void Fit::EvaluateCovariance(const Eigen::MatrixXd& cov_matrix)
{
  // correlation matrix
  for (size_t i = 0; i < num_fit_parameters; ++i)
  {
    for (size_t j = 0; j < num_fit_parameters; ++j)
    {
      correlation_matrix(i, j) = cov_matrix(i, j) / sqrt(cov_matrix(i, i) * cov_matrix(j, j));

      if (std::isnan(correlation_matrix(i, j)))
      {
        correlation_matrix(i, j) = std::numeric_limits<double>::quiet_NaN();

        if (i == j)
          non_dependent_variables.push_back(static_cast<int>(i));
      }
    }
  }

  // estimated standard deviation of the fit parameters
  // sqrt of diagonal elements of covariance matrix
  fit_parameter_errors = covariance_matrix.diagonal().cwiseSqrt();
}

void Fit::CeresErrors(ceres::Problem& ceres_problem)
{
  ceres::Covariance::Options covariance_options;

  covariance_options.num_threads = 1;
  covariance_options.sparse_linear_algebra_library_type = ceres::EIGEN_SPARSE;
  covariance_options.algorithm_type = ceres::DENSE_SVD; //SPARSE_QR not working for rank deficient matrices(encountered in fit scenarios)
  //covariance_options.min_reciprocal_condition_number = 1.0e-14;
  covariance_options.null_space_rank = -1; // must be positive to compute pseudo inverse - but works this way as well.

  ceres::Covariance covariance(covariance_options);

  std::vector<std::pair<const double*, const double*> > covariance_blocks;

  for (size_t i = 0; i < num_fit_parameters; ++i)
    for (size_t j = 0; j < num_fit_parameters; ++j)
      covariance_blocks.push_back(std::make_pair(fit_parameter_pointers[i], fit_parameter_pointers[j]));

  bool covariance_success = covariance.Compute(covariance_blocks, &ceres_problem);

  non_dependent_variables.resize(0);

  if (!covariance_success)
  {
    errors::ErrorMessage("Fit", "Covariance calculation not valid. Check model for non-dependent variables.");
  }
  else
  {
    // inverse Hessian
    for (int i = 0; i < num_fit_parameters; ++i)
      for (int j = 0; j < num_fit_parameters; ++j)
        covariance.GetCovarianceBlock(fit_parameter_pointers[i], fit_parameter_pointers[j], &inverse_hessian(i, j));

    // covariance matrix
    // The inverse Hessian must be scaled by a factor
    // f = residual weight * SUM_i(residual_i^2) / (number of data points - number of fit parameters);
    // The residual must be the used implementation from python.
    double squared_sum_of_residuals = 0.0;

    size_t num_of_data_points = 0;

    for (FitMeasurement* fit_measurement : measurements)
    {
      num_of_data_points += fit_measurement->data_size;// fit_residuals.size();

      squared_sum_of_residuals += fit_measurement->fit_weight * std::inner_product(fit_measurement->fit_residuals.begin(),
        fit_measurement->fit_residuals.end(), fit_measurement->fit_residuals.begin(), 0.0);
    }

    double covar_scaling_factor = 0.0;

    if (num_of_data_points > num_fit_parameters)
    {
      covar_scaling_factor = squared_sum_of_residuals / (num_of_data_points - num_fit_parameters);
    }
    else
    {
      errors::ErrorMessage("Fit", "Number of data points provided to Fit module is equal or less to fit parameters.");
    }

    covariance_matrix = covar_scaling_factor * inverse_hessian;

    EvaluateCovariance(covariance_matrix);
  }
}


// Various bootstraping methods
void Fit::CallBootstrap()
{
  if (options->InitialCheck() == false)
  {
    errors::ErrorMessage("Fit", "Error in Options. No error analysis performed.");

    return;
  }

  ResetErrorData();

  // store fit parameters
  const std::vector<double> fit_parameters = GetFitParameters();

  // setup random engine
  std::default_random_engine generator;

  // Gaussian distribution for wild method
  std::normal_distribution<double> distribution_Gaussian(0.0, 1.0);

  std::uniform_real_distribution<double> distribution_real(-1.0, 1.0);

  // bootstrap data set
  boostrap_fit_parameters = Eigen::MatrixXd::Zero(options->Bootstrap->iterations, num_fit_parameters);

  for (FitMeasurement* fit_measurement : measurements)
  {
    fit_measurement->bootstrap_fit_result = fit_measurement->result;

    fit_measurement->bootstrap_residuals = fit_measurement->bare_residuals;
  }

  // generate n bootstrap data sets
  // fit each data set with start values
  for (unsigned int i = 0; i < options->Bootstrap->iterations; i++)
  {
    // cycle through all measurement and resample data
    for (FitMeasurement* fit_measurement : measurements)
    {
      // generator for the new index to draw the residuals for "Residuals"
      std::uniform_int_distribution<size_t> distribution(0, fit_measurement->bootstrap_residuals.size() - 1);

      // generate new intensity_data with added random normal noise
      for (size_t j = 0; j < fit_measurement->intensity_data.size(); j++)
      {
        if (options->Bootstrap->method == "Poisson")
        {
          std::poisson_distribution<int> distribution_Poisson(fit_measurement->intensity_data[j]);

          fit_measurement->intensity_data[j] = distribution_Poisson(generator);
        }
        else if (options->Bootstrap->method == "Gaussian")
        {
          // weight the variance of the values
          std::normal_distribution<double> distribution_Gaussian(fit_measurement->input_intensity_data[j], sqrt(fit_measurement->input_intensity_data[j]));

          fit_measurement->intensity_data[j] = distribution_Gaussian(generator);
        }
        else if (options->Bootstrap->method == "Residuals")
        {
          const size_t random_index = distribution(generator);

          fit_measurement->intensity_data[j] = fit_measurement->bootstrap_fit_result[j] + fit_measurement->bootstrap_residuals[random_index];
        }
        else if (options->Bootstrap->method == "Wild")
        {
          fit_measurement->intensity_data[j] = fit_measurement->bootstrap_fit_result[j] + distribution_Gaussian(generator) * fit_measurement->bootstrap_residuals[j];
        }
        else if (options->Bootstrap->method == "Smoothed")
        {
          fit_measurement->intensity_data[j] = fit_measurement->input_intensity_data[j] + fit_measurement->input_intensity_data[j] * options->Bootstrap->smoothing_parameter * distribution_Gaussian(generator);
        }
        else if (options->Bootstrap->method == "Smoothed_Uniform")
        {
          fit_measurement->intensity_data[j] = fit_measurement->input_intensity_data[j] + fit_measurement->input_intensity_data[j] * options->Bootstrap->smoothing_parameter * distribution_real(generator);
        }

        if (fit_measurement->intensity_data[j] < 0.0)
          fit_measurement->intensity_data[j] = 0.0;
      }
    }

    SetInitialVars();

    RunFit(false);

    progress::PrintProgress(static_cast<double>(i) / static_cast<double>(options->Bootstrap->iterations));

    // store new bootstrap data
    const std::vector<double> fit_pars = GetFitParameters();

    for (size_t j = 0; j < num_fit_parameters; j++)
      boostrap_fit_parameters(i, j) = fit_pars[j];
  }

  std::cout << std::endl;
  
  // calculate mean of fit parameters
  const Eigen::RowVectorXd mean_fit_parameters = boostrap_fit_parameters.colwise().mean();

  // calculate covariance
  // see https://en.wikipedia.org/wiki/Sample_mean_and_covariance#Sample_covariance
  // Definition of sample covariance
  const Eigen::MatrixXd mat = boostrap_fit_parameters - Eigen::VectorXd::Ones(options->Bootstrap->iterations) * mean_fit_parameters;

  covariance_matrix = (mat.transpose() * mat) / (mat.rows() - 1.0);

  EvaluateCovariance(covariance_matrix);

  // restore best fit parameters and input data
  std::vector<double> mean_fit_params{};

  for (size_t i = 0; i < static_cast<size_t>(mean_fit_parameters.size()); i++)
    mean_fit_params.push_back(mean_fit_parameters(i));

  SetFitParameters(mean_fit_params);

  for (FitMeasurement* fit_measurement : measurements)
    fit_measurement->intensity_data = fit_measurement->input_intensity_data;
}
