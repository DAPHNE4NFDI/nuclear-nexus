// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "nuclear_reflectivity.h"

#include <iostream>
#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include "../utilities/errors.h"
#include "../utilities/progress.h"

#include "../math/constants.h"
#include "../math/fourier.h"
#include "../math/intensity.h"
#include "../math/integrate.h"
#include "../math/interpolate.h"
#include "../math/convolution.h"

#include "../functors/energy_spectrum.h"
#include "../functors/time_spectrum.h"


NuclearReflectivityEnergy::NuclearReflectivityEnergy(
  Experiment* const experiment_,
  GrazingSample* const sample_,
  const std::vector<double>& angles_,
  const std::vector<double>& detuning_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const offset_,
  Var* const resolution_,
  const double fit_weight_,
  Residual* const residual_,
  const std::vector<double> time_gate_,
  const bool coherence_,
  const bool print_progress_,
  const std::string id_
) :
  FitMeasurement(experiment_,
    detuning_,
    intensity_data_,
    1,
    scaling_,
    background_,
    offset_,
    intensity_data_.size(),
    fit_weight_,
    resolution_,
    nullptr,
    "Gauss",
    residual_,
    coherence_,
    id_),
  sample(sample_),
  angles(angles_),
  time_gate(time_gate_),
  print_progress(print_progress_)
{
  if (intensity_data.size() > 0 &&
      angles.size() != intensity_data.size())
  {
    errors::WarningMessage("NuclearReflectivityEnergy", "angles and intensity_data not of same size.");
  }

  if (fit_weight < 0.0)
    errors::WarningMessage("NuclearReflectivityEnergy", "fit_weight must be larger than zero.");

  if (!(time_gate.size() == 2 || time_gate.size() == 0))
    errors::WarningMessage("NuclearReflectivityEnergy", "time_gate size must be 0 or 2.");

  CheckIsotope("NuclearReflectivityEnergy");

  CheckForSampleInExperiment("NuclearReflectivityEnergy", sample);
};

std::vector<double> NuclearReflectivityEnergy::operator() (const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  if (*std::min_element(angles.begin(), angles.end()) <= 0.0)
    errors::WarningMessage("NuclearReflectivityEnergy::operator()", "All angles must be larger than zero");

  if (!(sample->geometry == "r"))
    errors::WarningMessage("NuclearReflectivityEnergy::operator()", "Only working in reflection geometry (\"r\").");

  // update and calculate transitions only once
  // here we use the experiment matrix once to be sure all transitions are calculated
  // not repeated in energy spectrum functor because only the angles change
  if (update_and_calc_transitions)
  {
    Update();

    LayerMatrix2 experiment_matrix = experiment->Matrix(detuning, true);
  }

  if (check_W_matrix)
  {
    CheckWmatrixValidityExperiment("NuclearReflectivityEnergy", experiment->isotope->energy);

    CheckWmatrixValiditySample("NuclearReflectivityEnergy", sample, experiment->isotope->energy, *std::max_element(angles.begin(), angles.end()));
  }

  // The incidence angle of the sample might be fit, so make sure that it is not fit here
  const double angle_storage_value = sample->angle->value;

  const bool angle_storage_fit = sample->angle->fit;

  sample->angle->fit = false;

  Var* const internal_scaling = new Var(1.0, 0.0, INFINITY, false, "", nullptr);
  Var* const internal_background = new Var(0.0, 0.0, INFINITY, false, "", nullptr);

  // EnergySpectrum calculates the spectrum of the whole experiment
  const EnergySpectrum energy_spec = EnergySpectrum(experiment, detuning, true, std::vector<double>(0), internal_scaling, internal_background,
    resolution, 1, 1.0, "Gauss", residual, time_gate, coherence, "EnergySpecForNuclearReflectivity_75326");
  
  const double detuning_step = detuning.at(1) - detuning.at(0);

  std::vector<double> total_reflectivity(angles.size());

  std::transform(angles.begin(), angles.end(), total_reflectivity.begin(), [&](const double ang)
    {
      sample->angle->value = ang + offset->value;
      
      const std::vector<double> energy_spectrum = energy_spec(false, false);  // do not update (material hyperfine) nor calculate transitions
      
      const double nuclear_intensity = integrate::IntegrateTrapez(detuning_step, energy_spectrum, 0, 0); // detuning step in Gamma, energy_spectrum no units --> Gamma

      if (print_progress)
        progress::PrintProgress(ang / angles.back());

      return nuclear_intensity * BeamProfileCorrection(ang + offset->value , sample->length, experiment->beam->fwhm->value, experiment->beam->profile);  
    }
  );

  if (resolution->value > 0.0)
  {
    resolution_kernel = convolution::GaussianKernel(angles, resolution->value, true);

    total_reflectivity = convolution::SameConvolution(total_reflectivity, resolution_kernel);
  }

  std::for_each(total_reflectivity.begin(), total_reflectivity.end(), [&](double& intens)
    {
      intens = background->value + intens * scaling->value;
    }
  );

  sample->angle->value = angle_storage_value;

  sample->angle->fit = angle_storage_fit;

  delete internal_scaling;
  delete internal_background;

  result = total_reflectivity;

  return total_reflectivity;
}

bool NuclearReflectivityEnergy::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  const std::vector<double> intensity_fit = operator()(true, false);

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_fit);

  return true;
}


double NuclearReflectivityEnergy::Residuals() const
{
  const std::vector<double> intensity_fit = operator()(true, false);

  return WeightedUserResidual(intensity_data, intensity_fit);
}


NuclearReflectivity::NuclearReflectivity(
  Experiment* const experiment_,
  GrazingSample* const sample_,
  const std::vector<double>& angles_,
  const double time_start_,
  const double time_stop_,
  const double time_step_,
  const double max_detuning_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const offset_,
  Var* const resolution_,
  const double fit_weight_,
  const double bunch_spacing_,
  Residual* const residual_,
  const bool coherence_,
  const bool print_progress_,
  const std::string id_
) :
  FitMeasurement(experiment_,
    std::vector<double>(0),
    intensity_data_,
    1,
    scaling_,
    background_,
    offset_,
    intensity_data_.size(),
    fit_weight_,
    resolution_,
    nullptr,
    "Gauss",
    residual_,
    coherence_,
    id_),
  sample(sample_),
  angles(angles_),
  time_start(time_start_),
  time_stop(time_stop_),
  time_step(time_step_),
  max_detuning(max_detuning_),
  bunch_spacing(bunch_spacing_),
  print_progress(print_progress_)
{
  if (intensity_data.size() > 0 &&
      angles.size() != intensity_data.size())
    errors::WarningMessage("NuclearReflectivity", "angles and intensity_data not of same size.");

  if (fit_weight < 0.0)
    errors::WarningMessage("NuclearReflectivity", "fit_weight must be larger than zero.");

  CheckIsotope("NuclearReflectivity");

  CheckForSampleInExperiment("NuclearReflectivity", sample);
};


std::vector<double> NuclearReflectivity::operator() (const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  if (*std::min_element(angles.begin(), angles.end()) <= 0.0)
    errors::WarningMessage("NuclearReflectivity::operator()", "All angles must be larger than zero");

  if (!(sample->geometry == "r"))
    errors::WarningMessage("NuclearReflectivity::operator()", "Only working in reflection geometry (\"r\").");

  // update and calculate transitions only once
  // here we use the experiment matrix once to be sure all transitions are calculated
  // not repeated in time spectrum functor because only the angles change
  if (update_and_calc_transitions)
  {
    Update();

    LayerMatrix2 experiment_matrix = experiment->Matrix(detuning, true);
  }

  if (check_W_matrix)
  {
    CheckWmatrixValidityExperiment("NuclearReflectivity", experiment->isotope->energy);

    CheckWmatrixValiditySample("NuclearReflectivity", sample, experiment->isotope->energy, *std::max_element(angles.begin(), angles.end()));
  }

  // The incidence angle of the sample might be fit, so make sure that it is not fit here
  const double angle_storage_value = sample->angle->value;

  const bool angle_storage_fit = sample->angle->fit;

  sample->angle->fit = false;
  /// here we somehow have to erase the var from the fithandler!
  /// in both nuclear ref methods!
  /// really necessary? if the angle is set anyway over an array.

  time = PopulateTime(time_stop, time_step);

  Var* const internal_scaling = new Var(1.0, 0.0, INFINITY, false, "", nullptr);
  Var* const internal_background = new Var(0.0, 0.0, INFINITY, false, "", nullptr);
  Var* const internal_time_offset = new Var(0.0, 0.0, INFINITY, false, "", nullptr);

  // TimeSpectrum calculates the spectrum of the whole experiment
  const TimeSpectrum time_spec = TimeSpectrum(experiment, time_stop, time_step, max_detuning, false,
    std::vector<double>(0), std::vector<double>(0), internal_scaling, internal_background, internal_time_offset, resolution, 1,
    1.0, bunch_spacing, residual, "auto", coherence, "TimeSpecForNuclearReflectivity_75326");

  std::vector<double> nuclear_reflectivity(angles.size());

  std::transform(angles.begin(), angles.end(), nuclear_reflectivity.begin(), [&](const double ang)
    {
      sample->angle->value = ang + offset->value;

      const std::vector<double> time_spectrum = time_spec(false, false);  // neither update (material hyperfine) nor calculate transitions

      const std::vector<double>::iterator start_time_iterator = std::lower_bound(time.begin(), time.end(), time_start);

      const std::ptrdiff_t distance = std::distance(time.begin(), start_time_iterator);

      const double nuclear_intensity = integrate::IntegrateTrapez(time_step, time_spectrum, distance, 0);  // time step in ns, time_spectrum in Gamma/ns --> Gamma

      if (print_progress)
        progress::PrintProgress((ang) / angles.back());

      return nuclear_intensity * BeamProfileCorrection(ang + offset->value, sample->length, experiment->beam->fwhm->value, experiment->beam->profile);
    }
  );

  fflush(stdout);

  if (resolution->value > 0.0)
  {
    resolution_kernel = convolution::GaussianKernel(angles, resolution->value, true);

    nuclear_reflectivity = convolution::SameConvolution(nuclear_reflectivity, resolution_kernel);
  }

  std::for_each(nuclear_reflectivity.begin(), nuclear_reflectivity.end(), [&](double& intens)
    {
      intens = background->value + intens * scaling->value;
    }
  );

  sample->angle->value = angle_storage_value;

  sample->angle->fit = angle_storage_fit;

  delete internal_scaling;
  delete internal_background;

  result = nuclear_reflectivity;

  return nuclear_reflectivity;
}


bool NuclearReflectivity::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  const std::vector<double> intensity_fit = operator()(true, false);

  bootstrap_fit_result = intensity_fit;

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_fit);

  return true;
}


double NuclearReflectivity::Residuals() const
{
  const std::vector<double> intensity_fit = operator()(true, false);

  bootstrap_fit_result = intensity_fit;

  return WeightedUserResidual(intensity_data, intensity_fit);
}
