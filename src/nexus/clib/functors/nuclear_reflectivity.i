// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module nuclear_reflectivity

%{
    #define SWIG_FILE_WITH_INIT
    #include "nuclear_reflectivity.h"
%}


%feature("shadow") NuclearReflectivity::NuclearReflectivity %{
def __init__(self,
             experiment,
             sample,
             angles,
             time_start,
             time_stop,
             time_step,
             max_detuning = 400.0,
             intensity_data = [],
             scaling = "auto",
             background = 0.0,
             offset = 0.0,
             resolution = internal_angular_resolution,
             fit_weight = 1.0,
             bunch_spacing = np.inf,
             residual = Sqrt(),
             coherence = False,
             print_progress = False,
             id = ""):

    intensity_data = np.array(intensity_data).astype(float)

    self._experiment = experiment

    self._sample = sample

    if max_detuning == 0.0:
       max_detuning = int(max_detuning)

    if isinstance(angles, (int, float)):
        angles = [angles]

    if scaling == "auto" and len(intensity_data) > 0:
      scaling = np.amax(intensity_data) - np.amin(intensity_data)
      scaling = Var(scaling, 0.0, 100.0*scaling, True, "NucRefl scaling")
    elif scaling == "auto" and len(intensity_data) == 0:
      scaling = Var(1.0, 0.0, np.inf, True, "NucRefl scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 0.0, np.inf, True, "NucRefl scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and len(intensity_data) > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100.0*background, True, "NucRefl backgr")
    elif background == "auto" and len(intensity_data) == 0:
      background = Var(0.0, 0.0, np.inf, False, "NucRefl backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "NucRefl backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(offset, (int, float)):
      offset = Var(offset, -1.0, 1.0, False, "NucRefl angular offset")

    if isinstance(resolution, (int, float)):
      resolution = Var(resolution, 0.0, 10.0, False, "NucRefl resolution")

    self._scaling = scaling

    self._background = background

    self._offset = offset

    self._resolution = resolution

    self._residual = residual

    _cnexus.NuclearReflectivity_swiginit(self, _cnexus.new_NuclearReflectivity(experiment,
                                                                               sample,
                                                                               angles,
                                                                               time_start,
                                                                               time_stop,
                                                                               time_step,
                                                                               max_detuning,
                                                                               intensity_data,
                                                                               scaling,
                                                                               background,
                                                                               offset,
                                                                               resolution,
                                                                               fit_weight,
                                                                               bunch_spacing,
                                                                               residual,
                                                                               coherence,
                                                                               print_progress,
                                                                               id))
%}

%feature("shadow") NuclearReflectivity::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of intensity values.
    """
    return np.array(_cnexus.NuclearReflectivity___call__(self))
%}


%feature("shadow") NuclearReflectivityEnergy::NuclearReflectivityEnergy %{
def __init__(self,
             experiment,
             sample,
             angles,
             detuning,
             intensity_data = [],
             scaling = "auto",
             background = 0.0,
             offset = 0.0,
             resolution = internal_angular_resolution,
             fit_weight = 1.0,
             residual = Sqrt(),
             time_gate = [],
             coherence = False,
             print_progress = False,
             id = ""):

    intensity_data = np.array(intensity_data).astype(float)
    
    self._experiment = experiment

    self._sample = sample

    if isinstance(angles, (int, float)):
        angles = [angles]

    if isinstance(detuning, (int, float)):
        detuning = [detuning]

    if scaling == "auto" and len(intensity_data) > 0:
      scaling = np.amax(intensity_data) - np.amin(intensity_data)
      scaling = Var(scaling, 0.0, 100.0*scaling, True, "NucReflEnergy scaling")
    elif scaling == "auto" and len(intensity_data) == 0:
      scaling = Var(1.0, 0.0, np.inf, True, "NucReflEnergy scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 0.0, np.inf, True, "NucReflEnergy scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and len(intensity_data) > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100.0*background, True, "NucReflEnergy backgr")
    elif background == "auto" and len(intensity_data) == 0:
      background = Var(0.0, 0.0, np.inf, False, "NucReflEnergy backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "NucReflEnergy backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(offset, (int, float)):
      offset = Var(offset, -1.0, 1.0, False, "NucReflEnergy angular offset")

    if isinstance(resolution, (int, float)):
      resolution = Var(resolution, 0.0, 10.0, False, "NucReflEnergy resolution")

    self._scaling = scaling

    self._background = background

    self._offset = offset 

    self._resolution = resolution

    self._residual = residual

    _cnexus.NuclearReflectivityEnergy_swiginit(self, _cnexus.new_NuclearReflectivityEnergy(experiment,
                                                                                           sample,
                                                                                           angles,
                                                                                           detuning,
                                                                                           intensity_data,
                                                                                           scaling,
                                                                                           background,
                                                                                           offset,
                                                                                           resolution,
                                                                                           fit_weight,
                                                                                           residual,
                                                                                           time_gate,
                                                                                           coherence,
                                                                                           print_progress,
                                                                                           id))
%}

%feature("shadow") NuclearReflectivityEnergy::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of intensity values.
    """
    return np.array(_cnexus.NuclearReflectivityEnergy___call__(self))
%}



%include "nuclear_reflectivity.h"



%extend NuclearReflectivity {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_sample", "_scaling", "_background", "_offset", "_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("NuclearReflectivity does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr == "sample":
      self._sample = sample

    if attr in ("scaling", "background", "offset", "resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if isinstance(val, (list, tuple)):
        val = DoubleVector(val)
        super().__setattr__(attr, val)
        return

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "offset":
      self._offset = val

    if attr == "resolution":
      self._resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)

  def Plot(self,
           data = True,
           residuals = True,
           datacolor='black', 
           theorycolor='r',
           theorywidth=2,
           datalinestyle='none',
           datamarker='+',
           datamarkersize=2,
           datafillstyle='full',
           legend=True,
           errors= False,
           errorcap=2,
           name=None):
    r"""
    Plot the :class:`NuclearReflectivity`.

    .. versionadded:: 1.0.3

    .. versionchanged:: 1.2.0

    Args:
      data (bool): Select if :attr:`intensity_data` should be plot.
      residuals (bool): Select if :attr:`residuals` from fit should be plot.
      datacolor (string): See *matplotlib* documentation.
      theorycolor (string): See *matplotlib* documentation.
      theorywidth (float): See *matplotlib* documentation.
      datalinestyle (string): See *matplotlib* documentation.
      datamarker (string): See *matplotlib* documentation.
      datamarkersize (float): See *matplotlib* documentation.
      datafillstyle (string): See *matplotlib* documentation.
      legend (bool): Select if legend should be plot when model and data are plot.
      errors (bool): Select if error bars should be plot.
      errorcap (float): See *matplotlib* documentation (``capsize``).
      name(string): If given, the plot is saved under this name.
         
        .. versionadded:: 1.1.0
    """
    
    title_string = r'Nuclear Reflectivity: '

    x_axis = self.angles
    x_label = r'angle (deg)'

    if self.scaling.value == 1.0:
      y_label=r'time-integrated resonant intensity ($\Gamma$)'
    else:
      y_label=r'counts'

    self.FitMeasruementPlot(x_axis,
                            x_label,
                            y_label,
                            title_string,
                            x_axis_data=None,
                            log=True,
                            data=data,
                            residuals=residuals,
                            datacolor=datacolor, 
                            theorycolor=theorycolor,
                            theorywidth=theorywidth,
                            datalinestyle=datalinestyle,
                            datamarker=datamarker,
                            datamarkersize=datamarkersize,
                            datafillstyle=datafillstyle,
                            legend=legend,
                            errors=errors,
                            errorcap=errorcap,
                            name=name)

  }
}

%extend NuclearReflectivityEnergy {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_sample", "_scaling", "_background", "_offset", "_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("NuclearReflectivityEnergy does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr == "sample":
      self._sample = sample

    if attr in ("scaling", "background", "offset", "resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return
    
    if isinstance(val, (list, tuple)):
      val = DoubleVector(val)

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "offset":
      self._offset = val

    if attr == "resolution":
      self._resolution = val

    if attr == "residual":
      self._residual = val
    
    super().__setattr__(attr, val)


  def Plot(self,
           data = True,
           residuals = True,
           datacolor='black', 
           theorycolor='r',
           theorywidth=2,
           datalinestyle='none',
           datamarker='+',
           datamarkersize=2,
           datafillstyle='full',
           legend=True,
           errors= False,
           errorcap=2,
           name=None):
    r"""
    Plot the :class:`NuclearReflectivityEnergy`.

    .. versionadded:: 1.0.3

    .. versionchanged:: 1.2.0

    Args:
      data (bool): Select if :attr:`intensity_data` should be plot.
      residuals (bool): Select if :attr:`residuals` from fit should be plot.
      datacolor (string): See *matplotlib* documentation.
      theorycolor (string): See *matplotlib* documentation.
      theorywidth (float): See *matplotlib* documentation.
      datalinestyle (string): See *matplotlib* documentation.
      datamarker (string): See *matplotlib* documentation.
      datamarkersize (float): See *matplotlib* documentation.
      datafillstyle (string): See *matplotlib* documentation.
      legend (bool): Select if legend should be plot when model and data are plot.
      errors (bool): Select if error bars should be plot.
      errorcap (float): See *matplotlib* documentation (``capsize``).
      name(string): If given, the plot is saved under this name.
         
        .. versionadded:: 1.1.0
    """
    title_string = r'Nuclear Reflectivity Energy: '

    x_axis = self.angles
    x_label = r'angle (deg)'

    if self.scaling.value == 1.0:
      y_label=r'energy-integrated resonant intensity ($\Gamma$)'
    else:
      y_label=r'counts'

    self.FitMeasruementPlot(x_axis,
                            x_label,
                            y_label,
                            title_string,
                            x_axis_data=None,
                            log=True,
                            data=data,
                            residuals=residuals,
                            datacolor=datacolor, 
                            theorycolor=theorycolor,
                            theorywidth=theorywidth,
                            datalinestyle=datalinestyle,
                            datamarker=datamarker,
                            datamarkersize=datamarkersize,
                            datafillstyle=datafillstyle,
                            legend=legend,
                            errors=errors,
                            errorcap=errorcap,
                            name=name)

  }
}
