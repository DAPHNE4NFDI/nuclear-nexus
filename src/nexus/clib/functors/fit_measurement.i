// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module fit_measurement

%{
    #define SWIG_FILE_WITH_INIT
    #include "fit_measurement.h"
%}


%feature("autodoc", "List of :class:`FitMeasurement` objects.") std::vector<FitMeasurement*>;

%template(FitMeasurementVector) std::vector<FitMeasurement*>;



%include "fit_measurement.h"



%extend FitMeasurement {
  %pythoncode{

  def __setattr__(self, attr, val):

    if attr == "intensity_data":
        self.input_intensity_data = val

    super().__setattr__(attr, val)


  def fit_output(self):
    output = "  .data_size =  {}\n".format(self.data_size)
    output = "  .distribution_points =  {}\n".format(self.distribution_points)
    output += "  .scaling = {}\n".format(self.scaling)
    output += "  .background =  {}\n".format(self.background)
    output += "  .fit_weight =  {}\n".format(self.fit_weight)
    output += "  .kernel_type =  {}\n".format(self.kernel_type)
    return output


  def FitMeasruementPlot(self,
                         x_axis,
                         x_label,
                         y_label,
                         title_string,
                         x_axis_data=None,
                         log=False,
                         data=True,
                         residuals=True,
                         datacolor='black', 
                         theorycolor='r',
                         theorywidth=2,
                         datalinestyle='none',
                         datamarker='+',
                         datamarkersize=2,
                         datafillstyle='full',
                         legend=True,
                         errors= False,
                         errorcap=2,
                         name=None):
    
      if (len(self.result) == 0):
        raise Exception("NEXUS error: No .result found. Use .Calculate() before calling the result.")

      if x_axis_data == None:
        x_axis_data = x_axis
    
      if residuals == True and len(self.fit_residuals) > 0:
        fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, height_ratios=[4, 1, 1], figsize=(7.0, 7.0))

        str_cost = r'$cost = \dfrac{1}{n} \sum_i r_i(y_i, \hat{y}_i)^a$'

        str_resfunc = r'$r(y, \hat{y}) = $' + self.residual.plot_string

        fig.text(0.4,0.91, str_cost + '  |  ' + str_resfunc + '  |  a = ' + str(self.residual.exponent),
                  fontsize=9)

        cost_title = r'  $cost$ = {:.2f}'.format(self.Cost())
          
        if(self.Cost() < 0.1):
          cost_title = r'  $cost$ = {:.2e}'.format(self.Cost())

        mse_title = r'  MSE = {:.2f}'.format(self.MeanSquaredError())

        ax1.set_title(cost_title + '\n' + mse_title, 
                      loc='left')

        fonstsize_residual = 10

        ax2.plot(x_axis_data, self.fit_residuals, label='data', color=datacolor, marker=datamarker,
                  ms=datamarkersize, fillstyle=datafillstyle, linestyle=datalinestyle)

        ax2.set(ylabel = '$r_i(y_i, \hat{y}_i)$')

        ax2.set_title('residual function', fontsize=fonstsize_residual)

        ax3.plot(x_axis_data, self.bare_residuals, label='data', color=datacolor, marker=datamarker,
                  ms=datamarkersize, fillstyle=datafillstyle, linestyle=datalinestyle)

        ax3.set(xlabel=x_label)
        ax3.set(ylabel = '$y - \hat{y}$')

        ax3.set_title('residuals', fontsize=fonstsize_residual)
      else:
        fig, ax1 = plt.subplots()

        ax1.set(xlabel=x_label)   
      
      ax1.set(ylabel=y_label)
    
      if data == True and len(self.intensity_data) > 0:
        if errors==True:
          ax1.errorbar(x_axis_data, self.intensity_data, yerr=np.sqrt(self.intensity_data), label='data', 
                       color=datacolor, marker='o', ms=datamarkersize, fillstyle=datafillstyle, 
                       linestyle=datalinestyle, ecolor=datacolor, capsize=errorcap)
        else:
          ax1.plot(x_axis_data, self.intensity_data, label='data', color=datacolor, marker=datamarker,
                   ms=datamarkersize, fillstyle=datafillstyle, linestyle=datalinestyle)

      if len(x_axis_data) == len(self.result):
        ax1.plot(x_axis_data, self.result, label='model', color=theorycolor, linewidth=theorywidth, zorder=10)
      else:
        ax1.plot(x_axis, self.result, label='model', color=theorycolor, linewidth=theorywidth, zorder=10)

      if log == True:
        ax1.set_yscale('log')
          
        data = np.append(np.array(self.intensity_data), np.array(self.result))
        
        if min(data) <= 0.0:
          ax1.set_ylim(0.5, max(data)*2.0)
        else:
          ax1.set_ylim(min(data)/2.0, max(data)*2.0)
    
      fig.suptitle(title_string + self.id)
      
      if legend == True:
          ax1.legend()
        
      if name != None:
        plt.savefig(name)
    
      plt.show()
  }
}
