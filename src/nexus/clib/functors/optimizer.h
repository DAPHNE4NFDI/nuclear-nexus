// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Optimizer class.
// Takes Measurement types as argument.
// Fit functors only depends on general Measurement properties.


#ifndef NEXUS_OPTIMIZER_H_
#define NEXUS_OPTIMIZER_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"
#include "../classes/optimizer_handler.h"
#include "../classes/optimizer_options.h"
#include "../classes/inequality.h"

#include "../functors/fit.h"


/**
Constructor for the abstract :class:`Optimizer` class.
The optimizer is used to minimize a certain residual value.
The actual implementation has to be provided by the user in python the following way.

.. code-block::

    # A new python class is derived from the nexus Optimizer class.
    class DerivedOptimizerClass(nx.Optimizer):
        def __init__(self, measurements, id):
            super().__init__(measurements, id)

        # implementation of the residual
        def Residual(self):
            # here goes your code which calculates the residual to be minimized
            # it must depend on measurement class(es) object with fittable Var(s)
            ...
            residual = ...
            # the return value must be of type float
            return residual            

    # Set up a optimization scenario 

    # There must exist nx.Var object(s) that should be optimized
    optimization_parameter = nx.Var(..., fit = True)
    ...

    # There are measurement class object(s) defined which depend on the optimization parameter(s)
    measurement_type = nx.MeasurementClassType(...)

    # initialize a derived optimizer class object
    # pass a list with measurement objects on which the residual implementation Residual() depends
    my_optimizer = DerivedOptimizerClass(measurements = [measurement_type, ...], id = "optimizer id")

    # calculate the residual value if you want to check your code and print
    print(my_optimizer.Residual())
   
    # run the optimizer to optimize all nx.Var object(s) on which the residual depends
    my_optimizer() # or my_optimizer.Evaluate()

You can also pass the negative of the value to maximize. For more information have a look to the tutorial.

Args:
   measurements (list): List of :class:`Measurement` objects used in the Optimizer.
   id (string): User identifier.
   external_fit_variables (list): List of :class:`Var` objects.
     These Vars are not directly assigned to a *Nexus* object but can be related to an equality constraint on a Var.
     *Nexus* is not able to detect those automatically, so they have to provided manually.

     .. versionadded:: 1.1.0

   inequalities (:class:`Inequality`): An :class:`Inequality` object.
     All inequalities constraints are handled by this object.

     .. versionadded:: 1.1.0

Attributes:
   measurements (list): List of :class:`Measurement` objects used in the Optimizer.
   id (string): User identifier.
   external_fit_variables (list): List of :class:`Var` objects.
     These Vars are not directly assigned to a *Nexus* object but can be related to an equality constraint on a Var.
     *Nexus* is not able to detect those automatically, so they have to provided manually.

     .. versionadded:: 1.1.0

   inequalities (:class:`Inequality`): An :class:`Inequality` object.
     All inequalities constraints are handled by this object.
   
     .. versionadded:: 1.1.0

   options (:class:`OptimizerOptions`): Options that specify how the optimization is performed.
   start_residual (float): The residual value at the beginning of optimization.

     .. versionadded:: 1.1.0

   residual (float): The residual value to be optimized.
   initial_parameters (list): List with initial values of the fit parameters.
   lower_bounds (list): List with min values of the fit parameters.
   upper_bounds (list): List with max values of the fit parameters.
   ids (list): List with ids of the fit parameters.
*/
class Optimizer : public OptimizerHandler {
public:
  Optimizer(
    const std::vector<Measurement*> measurements,
    const std::string id,
    const std::vector<Var*> external_fit_variables,
    Inequality* const inequality
  ) :
    OptimizerHandler(id, external_fit_variables, inequality),
    measurements(measurements)
  {}

  virtual ~Optimizer() {};

  /**
  Call of the residual implementation function.
   
  Returns:
     float: The residual value.
  */
  virtual double Residual() const = 0;

  /*
  Evaluates the residual as implemented by the user.
  Also callable via operator ``Evaluate()``.
  */
  void operator() ();

  bool operator() (double const* const* parameters, double* residuals) const;

  #ifdef NEXUS_USE_NLOPT
  double NloptResiduals();
  #endif

  std::vector<Measurement*> measurements{};

  double start_residual = 0.0;

  mutable double residual = 0.0;

private:

  bool PopulateFitHandler();

  std::stringstream StartOutput() const;

  std::stringstream EndOutput() const;

  void CallMethod(const std::string method, const OptimizerModule fit_module);

  void CeresFit(const std::string method);

  void NLoptFit(const std::string method);

  void PagmoFit(const std::string method);
};


#endif // NEXUS_OPTIMIZER_H_
