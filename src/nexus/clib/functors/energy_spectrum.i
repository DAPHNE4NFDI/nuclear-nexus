// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module energy_spectrum

%{
    #define SWIG_FILE_WITH_INIT
    #include "energy_spectrum.h"
%}



%feature("shadow") AmplitudeSpectrum::AmplitudeSpectrum %{
def __init__(self,
             experiment,
             detuning,
             electronic = True,
             time_gate = [],
             id = ""):

    self._experiment = experiment

    _cnexus.AmplitudeSpectrum_swiginit(self, _cnexus.new_AmplitudeSpectrum(experiment, detuning, electronic, id, time_gate))
%}


%feature("shadow") AmplitudeSpectrum::operator() %{
def __call__(self):
    """
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of Jones vectors.
    """
    return np.squeeze(_cnexus.AmplitudeSpectrum___call__(self))
%}


%feature("shadow") EnergySpectrum::EnergySpectrum %{
def __init__(self,
             experiment,
             detuning,
             electronic = True,
             intensity_data = [],
             scaling = "auto",
             background = "auto",
             resolution = 0.0,
             distribution_points = 1,
             fit_weight = 1.0,
             kernel_type = "Gauss",
             residual = Sqrt(),
             time_gate = [],
             coherence = False,
             id = ""):

    intensity_data = np.array(intensity_data).astype(float)

    self._experiment = experiment

    if scaling == "auto" and len(intensity_data) > 0:
      scaling = np.amax(intensity_data)
      scaling = Var(scaling, 1.0e-16, 100.0*scaling, True, "ES scaling")
    elif scaling == "auto" and len(intensity_data) == 0:
      scaling = Var(1.0, 1.0e-16, np.inf, True, "ES scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 1.0e-16, np.inf, True, "ES scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and len(intensity_data) > 0:
      background = np.mean(intensity_data) / 10.0
      background = Var(background, 0.0, 100.0*background, True, "ES backgr")
    elif background == "auto" and len(intensity_data) == 0:
      background = Var(0.0, 0.0, np.inf, False, "ES backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "ES backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(resolution, (int, float)):
      resolution = Var(resolution, 0.0, 10.0, False, "ES resolution")

    self._scaling = scaling

    self._background = background

    self._resolution = resolution

    self._residual = residual

    _cnexus.EnergySpectrum_swiginit(self, _cnexus.new_EnergySpectrum(experiment,
                                                                     detuning,
                                                                     electronic,
                                                                     intensity_data,
                                                                     scaling,
                                                                     background,
                                                                     resolution,
                                                                     distribution_points,
                                                                     fit_weight,
                                                                     kernel_type,
                                                                     residual,
                                                                     time_gate,
                                                                     coherence,
                                                                     id))
%}

%feature("shadow") EnergySpectrum::operator() %{
def __call__(self):
    """
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
       ndarray: Array of float values.
    """
    return np.array(_cnexus.EnergySpectrum___call__(self))
%}


%feature("shadow") EmissionSpectrum::EmissionSpectrum %{
def __init__(self,
             experiment,
             velocity,
             electronic = True,
             intensity_data = [],
             scaling = "auto",
             background = "auto",
             resolution = 1.0,
             distribution_points = 1,
             fit_weight = 1.0,
             kernel_type = "Lorentz",
             residual = Sqrt(),
             time_gate = [],
             coherence = False,
             id = ""):

    self._experiment = experiment

    if scaling == "auto" and len(intensity_data) > 0:
      scaling = np.amax(intensity_data) - np.amin(intensity_data)
      scaling = Var(scaling, 0.0, 100.0*scaling, True, "ES scaling")
    elif scaling == "auto" and len(intensity_data) == 0:
      scaling = Var(1.0, 0.0, np.inf, True, "ES scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 0.0, np.inf, True, "ES scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and len(intensity_data) > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100.0*background, True, "ES backgr")
    elif background == "auto" and len(intensity_data) == 0:
      background = Var(0.0, 0.0, np.inf, False, "ES backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "ES backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(resolution, (int, float)):
      resolution = Var(resolution, 0.0, 10.0, False, "ES resolution")

    self._scaling = scaling

    self._background = background

    self._resolution = resolution

    self._residual = residual

    velocity = np.array(velocity)

    detuning = velocity * 1.0e-3 / 299792458.0 * experiment.isotope.energy / experiment.isotope.gamma

    intensity_data = np.array(intensity_data).astype(float)

    _cnexus.EmissionSpectrum_swiginit(self, _cnexus.new_EmissionSpectrum(experiment,
                                                                         detuning,
                                                                         electronic,
                                                                         intensity_data,
                                                                         scaling,
                                                                         background,
                                                                         resolution,
                                                                         distribution_points,
                                                                         fit_weight,
                                                                         kernel_type,
                                                                         residual,
                                                                         time_gate,
                                                                         coherence,
                                                                         id))
%}

%feature("shadow") EmissionSpectrum::operator() %{
def __call__(self):
    """
    Calculates the Emission class method.
    Also callable via operator ``()``.

    Returns:
       ndarray: Array of float values.
    """
    return np.array(_cnexus.EmissionSpectrum___call__(self))
%}



%include "energy_spectrum.h"



%extend AmplitudeSpectrum {
  %pythoncode{

  Calculate = __call__
  
  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment") and not hasattr(self, attr):
      raise Exception("AmplitudeSpectrum does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if isinstance(val, (list, tuple)):
        val = DoubleVector(val)

    super().__setattr__(attr, val)

  def __repr__(self):
    output = "AmplitudeSpectrum\n"
    output += self.experiment_output()
    output += self.object_output()
    output += "  .detuning min / max / steps: {} / {} / {}\n".format(min(self.detuning), max(self.detuning), len(self.detuning))
    return output


  def Plot(self, sigma = True, pi = True, polar = False, unwrap = True, name=None):
    r"""
    Plot the :class:`AmplitudeSpectrum`.

    .. versionadded:: 1.0.3

    Args:
        sigma (bool): Select if :math:`\sigma` component should be plot.
        pi (bool): Select if :math:`\pi` component should be plot.
        phase (bool): If ``True`` absolute and phase are plotted instead of real and imaginary parts.
        unwrap (bool): Select if phase should be unwrapped.
        name(string): If given, the plot is saved under this name.
           
           .. versionadded:: 1.1.0
    """
    result = self.__call__()
    result_sigma = result[:,0]
    result_pi = result[:,1]

    if polar == False:
        result_sigma_real = np.real(result_sigma)
        result_sigma_imag = np.imag(result_sigma)

        result_pi_real = np.real(result_pi)
        result_pi_imag = np.imag(result_pi)

        real_label = 'real'
        imag_label = 'imag'

    else:
        result_sigma_real = np.abs(result_sigma)
        if unwrap == True:
            result_sigma_imag = np.unwrap(np.angle(result_sigma))
        else:
            result_sigma_imag = np.angle(result_sigma)

        result_pi_real = np.abs(result_pi)
        if unwrap == True:
            result_pi_imag = np.unwrap(np.angle(result_pi))
        else:
            result_pi_imag = np.angle(result_pi)

        real_label = 'abs'
        imag_label = 'phase'

    if sigma == True and pi == True:
        fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(6.4, 2*4.8))
        fig.suptitle('Amplitude Spectrum: ' + self.id)

        lns1 = ax1.plot(self.detuning, result_sigma_real, label = real_label)
        ax1.set_ylabel(real_label)

        ax1_r = ax1.twinx()
        lns2 = ax1_r.plot(self.detuning, result_sigma_imag, label = imag_label, color = 'darkorange')
        ax1_r.set_ylabel(imag_label)

        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs)

        ax1.set_title(r'$\sigma$ polarization')


        lns3 = ax2.plot(self.detuning, result_pi_real, label = real_label)
        ax2.set_xlabel(r'detuning ($\Gamma$)')
        ax2.set_ylabel(real_label)

        ax2_r = ax2.twinx()
        lns4 = ax2_r.plot(self.detuning, result_pi_imag, label = imag_label, color = 'darkorange')
        ax2_r.set_ylabel(imag_label)

        lns = lns3 + lns4
        labs = [l.get_label() for l in lns]
        ax2.legend(lns, labs)

        ax2.set_title(r'$\pi$ polarization')

    elif sigma == True:
        fig, ax1 = plt.subplots()
        fig.suptitle('Amplitude Spectrum: ' + self.id)

        lns1 = ax1.plot(self.detuning, result_sigma_real, label = real_label)
        ax1.set_xlabel(r'detuning ($\Gamma$)')
        ax1.set_ylabel(real_label)

        ax1_r = ax1.twinx()
        lns2 = ax1_r.plot(self.detuning, result_sigma_imag, label = imag_label, color = 'darkorange')
        ax1_r.set_ylabel(imag_label)

        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs)
        ax1.set_title(r'$\sigma$ polarization')

    elif pi == True:
        fig, ax1 = plt.subplots(1)
        fig.suptitle('Amplitude Spectrum: ' + self.id)

        lns1 = ax1.plot(self.detuning, result_pi_real, label = real_label)
        ax1.set_xlabel(r'detuning ($\Gamma$)')
        ax1.set_ylabel(real_label)

        ax1_r = ax1.twinx()
        lns2 = ax1_r.plot(self.detuning, result_pi_imag, label = imag_label, color = 'darkorange')
        ax1_r.set_ylabel(imag_label)

        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs)
        ax1.set_title(r'$\pi$ polarization')
       
    if name != None:
        plt.savefig(name)

    plt.show()
  }
}


%extend EnergySpectrum {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_scaling", "_background", "_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("EnergySpectrum does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr in ("scaling", "background", "resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if isinstance(val, (list, tuple)):
      val = DoubleVector(val)

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "resolution":
      self._resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)


  def __repr__(self):
    output = "EnergySpectrum\n"
    output += self.experiment_output()
    output += self.object_output()
    output += "  .detuning   min / max / points: {} / {} / {}\n".format(min(self.detuning), max(self.detuning), len(self.detuning))
    output += "  .intensity_data   points: {}\n".format(len(self.intensity_data))
    output += "  .scaling =  {}\n".format(self.scaling)
    output += "  .background =  {}\n".format(self.background)
    output += "  .resolution =  {}\n".format(self.resolution)
    output += "  .distribution_points =  {}\n".format(self.distribution_points)
    output += "  .fit_weight =  {}\n".format(self.fit_weight)
    output += "  .kernel_type =  {}\n".format(self.kernel_type)
    return output


  def Plot(self,
           data = True,
           residuals = True,
           velocity = False,
           datacolor='black', 
           theorycolor='r',
           theorywidth=2,
           datalinestyle='none',
           datamarker='+',
           datamarkersize=2,
           datafillstyle='full',
           legend=True,
           errors= False,
           errorcap=2,
           name=None):
    r"""
    Plot the :class:`EnergySpectrum`.

    .. versionadded:: 1.0.3

    .. versionchanged:: 1.2.0

    Args:
      data (bool): True if :attr:`intensity_data` should be plot.
      residuals (bool): True if :attr:`residuals` from fit should be plot.
      velocity (bool): Plot velocity instead of detuning.
      datacolor (string): See *matplotlib* documentation.
      theorycolor (string): See *matplotlib* documentation.
      theorywidth (float): See *matplotlib* documentation.
      datalinestyle (string): See *matplotlib* documentation.
      datamarker (string): See *matplotlib* documentation.
      datamarkersize (float): See *matplotlib* documentation.
      datafillstyle (string): See *matplotlib* documentation.
      legend (bool): Select if legend should be plot when model and data are plot.
      errors (bool): Select if error bars should be plot.
      errorcap (float): See *matplotlib* documentation (``capsize``).
      name(string): If given, the plot is saved under this name.
         
        .. versionadded:: 1.1.0
    """
 
    title_string = r'Energy Spectrum: '

    x_axis = self.detuning
    x_label = r'detuning ($\Gamma$)'

    if velocity == True:
      title_string = r'Moessbauer Spectrum: '

      x_axis = self.Velocity()
      x_label = r'velocity (mm/s)'

    if max(self.result) < 2:
      y_label=r'transmission'
    else:
      y_label=r'counts'

    self.FitMeasruementPlot(x_axis,
                            x_label,
                            y_label,
                            title_string,
                            x_axis_data=None,
                            log=False,
                            data=data,
                            residuals=residuals,
                            datacolor=datacolor, 
                            theorycolor=theorycolor,
                            theorywidth=theorywidth,
                            datalinestyle=datalinestyle,
                            datamarker=datamarker,
                            datamarkersize=datamarkersize,
                            datafillstyle=datafillstyle,
                            legend=legend,
                            errors=errors,
                            errorcap=errorcap,
                            name=name)

  }
}


%extend EmissionSpectrum {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_scaling", "_background", "_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("EmissionSpectrum does not have attribute {}".format(attr))

    if attr in ("scaling", "background", "resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if isinstance(val, (list, tuple)):
      val = DoubleVector(val)
    
    if attr == "experiment":
      self._experiment = experiment

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "resolution":
      self._resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)

  def __repr__(self):
    output = "EmissionSpectrum\n"
    output += self.experiment_output()
    output += self.object_output()
    output += "  .detuning   min / max / points: {} / {} / {}\n".format(min(self.detuning), max(self.detuning), len(self.detuning))
    output += "  .intensity_data   points: {}\n".format(len(self.intensity_data))
    output += "  .scaling =  {}\n".format(self.scaling)
    output += "  .background =  {}\n".format(self.background)
    output += "  .resolution =  {}\n".format(self.resolution)
    output += "  .distribution_points =  {}\n".format(self.distribution_points)
    output += "  .fit_weight =  {}\n".format(self.fit_weight)
    output += "  .kernel_type =  {}\n".format(self.kernel_type)
    return output


  def Plot(self,
           data = True,
           residuals = True,
           velocity = False,
           datacolor='black', 
           theorycolor='r',
           theorywidth=2,
           datalinestyle='none',
           datamarker='+',
           datamarkersize=2,
           datafillstyle='full',
           legend=True,
           errors= False,
           errorcap=2,
           name=None):
    r"""
    Plot the :class:`EmissionSpectrum`.

    .. versionadded:: 1.0.3

    .. versionchnged:: 1.2.0

    Args:
      data (bool): Select if :attr:`intensity_data` should be plot.
      residuals (bool): Select if :attr:`residuals` from fit should be plot.
      velocity (bool): Plot velocity instead of detuning.
      datacolor (string): See *matplotlib* documentation.
      theorycolor (string): See *matplotlib* documentation.
      theorywidth (float): See *matplotlib* documentation.
      datalinestyle (string): See *matplotlib* documentation.
      datamarker (string): See *matplotlib* documentation.
      datamarkersize (float): See *matplotlib* documentation.
      datafillstyle (string): See *matplotlib* documentation.
      legend (bool): Select if legend should be plot when model and data are plot.
      errors (bool): Select if error bars should be plot.
      errorcap (float): See *matplotlib* documentation (``capsize``).
      name(string): If given, the plot is saved under this name.
           
        .. versionadded:: 1.1.0

    """
    
    title_string = r'Emission Spectrum: '

    x_axis = self.detuning
    x_label = r'detuning ($\Gamma$)'

    if velocity == True:
      x_axis = self.Velocity()
      x_label = r'velocity (mm/s)'

    if max(self.result) < 2:
      y_label=r'transmission'
    else:
      y_label=r'counts'

    self.FitMeasruementPlot(x_axis,
                            x_label,
                            y_label,
                            title_string,
                            x_axis_data=None,
                            log=False,
                            data=data,
                            residuals=residuals,
                            datacolor=datacolor, 
                            theorycolor=theorycolor,
                            theorywidth=theorywidth,
                            datalinestyle=datalinestyle,
                            datamarker=datamarker,
                            datamarkersize=datamarkersize,
                            datafillstyle=datafillstyle,
                            legend=legend,
                            errors=errors,
                            errorcap=errorcap,
                            name=name)
  }
}

%pythoncode{

  def MoessbauerSpectrum(experiment,
                         velocity,
                         electronic = True,
                         intensity_data = [],
                         scaling = "auto",
                         background = "auto",
                         resolution = 1.0, # this is one Gamma.
                         distribution_points = 1,
                         fit_weight = 1.0,
                         kernel_type = "Lorentz",
                         residual = Sqrt(),
                         time_gate = [],
                         coherence = False,
                         id = ""
                         ):
      r"""
      Alias for :class:`EnergySpectrum`.
      Instead of a detuning in units of the line width, a velocity range is given [mm/s].
      It is internally converted to units of the resonant line width.
      
      The energy resolution is set to the natural linewidth of :math:`1 \Gamma`.
      The :attr:`kernel_type` is ``"Lorentz"``. Electronic scattering is included.
      
      Note, that standard kernel type in nexus is Gaussian.
      The convolution of a Gaussian kernel and a Lorentzian resonance will result in a Voigt profile.
      To account for the Lorentzian profile of the Moessbauer source, the convolution kernel is set to a Lorentzian here.
      """
      velocity_in = np.array(velocity)
      detuning = velocity_in * 1.0e-3 / 299792458.0 * experiment.isotope.energy / experiment.isotope.gamma

      return EnergySpectrum(experiment,
                            detuning,
                            electronic,
                            intensity_data,
                            scaling,
                            background,
                            resolution,
                            distribution_points,
                            fit_weight,
                            kernel_type,
                            residual,
                            time_gate,
                            coherence,
                            id)
}
