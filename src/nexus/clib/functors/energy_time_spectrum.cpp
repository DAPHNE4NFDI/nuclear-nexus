// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// For information on the Fourier scaling see time_spectrum.h and time_spectrum.cpp


#include "energy_time_spectrum.h"

#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/fourier.h"
#include "../math/intensity.h"
#include "../math/integrate.h"
#include "../math/interpolate.h"
#include "../math/convolution.h"
#include "../math/matrix.h"
#include "../functors/electronic.h"


EnergyTimeAmplitude::EnergyTimeAmplitude(
  Experiment* const experiment_,
  const double time_length_,
  const double time_step_,
  const std::string mode_,
  const bool electronic_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  time_length(time_length_),
  time_step(time_step_),
  mode(mode_),
  electronic(electronic_)
{
  // Check that exactly one sample has a drive_detuning
  drive_sample = CheckAndGetValidDriveSample();

  CheckIsotope("EnergyTimeAmplitude");

  CheckGrazingAngleExperiment("EnergyTimeAmplitude");
}


BasicSample* EnergyTimeAmplitude::CheckAndGetValidDriveSample() const
{
  int drive_index = -1;  // negative value -> no drive detuning given

  for (size_t i = 0; i < experiment->objects.size(); ++i)
  {
    if (BasicSample* const sample = dynamic_cast<BasicSample*>(experiment->objects[i]))
    {
      if (static_cast<int>(sample->drive_detuning.size()) > 0 &&
          drive_index < 0)
      {
        drive_index = static_cast<int>(i);
      }
      else if (sample->drive_detuning.size() > 0)
      {
        errors::WarningMessage("EnergyTimeAmplitude::operator()", "More than one Sample with drive_detuning found.");
      }
    }
  }

  if (drive_index < 0)
    errors::WarningMessage("EnergyTimeAmplitude::operator()", "No drive_detuning found.");

  return static_cast<BasicSample*>(experiment->objects[static_cast<unsigned int>(drive_index)]);
}


// This function is used to get the data storage to Eigen::MatrixXcd
// Then no new typemap in SWIG is needed
Eigen::MatrixXcd EnergyTimeAmplitude::Convert(const intensity::AmplitudeVector& input) const
{
  // number rows of time step
  Eigen::MatrixXcd amplitude(input.size(), 2);

  int j = 0;

  for (const Eigen::Vector2cd& in : input)
  {
    amplitude.row(j) = in;

    ++j;
  }

  return amplitude;
}


// Calculate total system response
intensity::AmplitudeVector EnergyTimeAmplitude::TimeResponse(const double fourier_norm, const double time_length_response,
  const double time_step_response, const bool electronic_response) const
{
  ExperimentMatrix2 experiment_matrix(detuning.size(), Eigen::Matrix2cd::Identity());

  for (NxObject* const obj : experiment->objects)
    experiment_matrix = matrix::Multiply(obj->object_matrix, experiment_matrix);

  if (electronic_response == false)
    experiment_matrix = SubtractElectronic(experiment_matrix);

  const ExperimentMatrix2 fourier_matrix = fourier::FourierTransform(experiment_matrix, fourier::FourierScaling::SqrtN, "none");

  intensity::AmplitudeVector time_spectrum_amplitude = intensity::Amplitude(fourier_matrix, experiment->beam);

  for (Eigen::Vector2cd& amp : time_spectrum_amplitude)
    amp *= fourier_norm;

  time = PopulateTime(time_length_response, time_step_response);

  time_spectrum_amplitude.resize(time.size());

  return time_spectrum_amplitude;
}


std::vector<Eigen::MatrixXcd> EnergyTimeAmplitude::AmplitudeInterpolated(BasicSample* drive_sample, const double fourier_norm) const
{
  std::vector<Eigen::MatrixXcd> amplitude{};

  // find maximum abs(values) of detuning and drive_detuning
  const double max_detuning_value = abs(*std::max_element(detuning.begin(), detuning.end(),
    [](const double& a, const double& b)
    {
      return  abs(a) < abs(b);
    })
  );

  const double max_drive_detuning_value = abs(*std::max_element(drive_sample->drive_detuning.begin(), drive_sample->drive_detuning.end(),
    [](const double& a, const double& b)
    {
      return  abs(a) < abs(b);
    })
  );

  // find the needed step size of the extended detuning 
  const double detuning_step = detuning.at(1) - detuning.at(0);

  // find number of points for the extended detuning
  const size_t extended_detuning_points = static_cast<size_t>(round(2 * (max_detuning_value + max_drive_detuning_value) / detuning_step + 1));

  std::vector<double> extended_detuning(extended_detuning_points);

  for (size_t i = 0; i < extended_detuning_points; ++i)
    extended_detuning[i] = -max_detuning_value - max_drive_detuning_value + i * detuning_step;

  // calculate the drive_sample_matrix on the extended detuning
  const ObjectMatrix2 drive_sample_matrix = drive_sample->ObjectMatrix(experiment->isotope, extended_detuning, false);

  // the object matrix size might be zero up to here, so resize to detuning size
  drive_sample->object_matrix.resize(detuning.size());
  
  for (const double drive_det : drive_sample->drive_detuning)
  {
    // Interpolate complex layer matrix linearly
    std::transform(NEXUS_EXECUTION_POLICY detuning.begin(), detuning.end(), drive_sample->object_matrix.begin(),
      [&](const double det)
      {
        // -drive_det shifts the spectrum to the correct direction.
        // interpolate on the extended grid
        return interpolate::InterpolateLinear<Eigen::Matrix2cd>(extended_detuning, drive_sample_matrix, det - drive_det, false);
      }
    );

    intensity::AmplitudeVector time_spectrum_amplitude = TimeResponse(fourier_norm, time_length, time_step, electronic);

    // convert std::vector<Eigen::Vector2cd> to Eigen::MatrixXcd
    amplitude.push_back(Convert(time_spectrum_amplitude));
  }

  return amplitude;
}


std::vector<Eigen::MatrixXcd> EnergyTimeAmplitude::AmplitudeFull(BasicSample* drive_sample, const double fourier_norm) const
{
  std::vector<Eigen::MatrixXcd> amplitude{};

  // store isomer shifts of each Hyperfine site of drive_sample / all layers / all materials
  std::vector<double> isomer_values;

  for (Layer* layer : drive_sample->layers)
  {
    for (Hyperfine* hyperfine_site : layer->material->hyperfine_sites)
      isomer_values.push_back(hyperfine_site->isomer->value);
  }

  // calculate time spectrum for each drive detuning
  for (const double drive_det : drive_sample->drive_detuning)
  {
    // set isomer shifts to detuned value and update to get all dependencies correct
    size_t i = 0;

    // shift isomer shift in all sample hyperfine sites
    for (Layer* layer : drive_sample->layers)
    {
      for (Hyperfine* hyperfine_site : layer->material->hyperfine_sites)
      {
        hyperfine_site->isomer->value = isomer_values[i] + drive_det / 1.0e-3 * constants::kSpeedOfLight /
          experiment->isotope->energy * experiment->isotope->gamma;

        ++i;
      }
    }

    drive_sample->Update();

    // calculate the sample_matrix of the drive_sample
    drive_sample->object_matrix = drive_sample->ObjectMatrix(experiment->isotope, detuning, true);

    intensity::AmplitudeVector time_spectrum_amplitude = TimeResponse(fourier_norm, time_length, time_step, electronic);

    // rewrite std::vector<Eigen::Vector2cd> to Eigen::MatrixXcd
    amplitude.push_back(Convert(time_spectrum_amplitude));
  }

  //set all isomer values to original data
  size_t i = 0;

  for (Layer* layer : drive_sample->layers)
  {
    for (Hyperfine* hyperfine_site : layer->material->hyperfine_sites)
    {
      hyperfine_site->isomer->value = isomer_values[i];

      ++i;
    }
  }

  return amplitude;
}


std::vector<Eigen::MatrixXcd> EnergyTimeAmplitude::operator() (const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  drive_sample = CheckAndGetValidDriveSample();

  if (update_and_calc_transitions)
    Update();

  if (check_W_matrix)
    CheckWmatrixValidityExperiment("EnergyTimeSpectrum", experiment->isotope->energy);

  // Create equidistant detuning grid for FFT.
  // Check parameters to ensure a detuning step size smaller than one Gamma.

  // the number of detuning points needed for a proper time spectrum
  const size_t number_detuning_points = NumberDetuningPoints(time_length, time_step, experiment->isotope->lifetime);

  // detuning might be cut in size due to max_detuning parameter
  detuning = PopulateDetuning(number_detuning_points, time_step, 0, experiment->isotope->lifetime);

  const double detuning_step = detuning.at(1) - detuning.front();

  const double fourier_norm = sqrt(fourier::NormalizationIntegratedIntensityScaled(detuning_step, time_step));

  // calculate all objects once in advance
//  std::for_each(NEXUS_EXECUTION_POLICY experiment->objects.begin(), experiment->objects.end(),
  std::for_each(experiment->objects.begin(), experiment->objects.end(),
    [&](NxObject* obj)
    {
      obj->object_matrix = obj->ObjectMatrix(experiment->isotope, detuning, update_and_calc_transitions);
    }
  );

  std::vector<Eigen::MatrixXcd> amplitude{};

  if (mode == "i")
  {
    amplitude = AmplitudeInterpolated(drive_sample, fourier_norm);
  }
  else if (mode == "f")
  {
    amplitude = AmplitudeFull(drive_sample, fourier_norm);
  }
  else
  {
    errors::WarningMessage("EnergyTimeAmplitude::operator()", "Calculation mode not recognized.");
  }

  result = amplitude;
  
  return amplitude;
}



EnergyTimeSpectrum::EnergyTimeSpectrum(
  Experiment* const experiment_,
  const double time_length_,
  const double time_step_,
  const std::string mode_,
  const bool electronic_,
  const std::vector<double>& time_data_,
  const std::vector<std::vector<double>>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const energy_resolution_,
  Var* const time_resolution_,
  const unsigned int distribution_points_,
  const double fit_weight_,
  const double bunch_spacing_,
  Residual* const residual_,
  const std::string id_
) :
  FitMeasurement(
    experiment_,
    std::vector<double>(0), // detuning
    std::vector<double>(0), // intensity  --> recast to std::vector<double> later
    distribution_points_,
    scaling_,
    background_,
    nullptr,
    intensity_data_.size() * intensity_data_.front().size(),
    fit_weight_,
    energy_resolution_,
    time_resolution_,
    "Gauss",
    residual_,
    false,
    id_
  ),
  time_length(time_length_),
  time_step(time_step_),
  mode(mode_),
  electronic(electronic_),
  time_data(time_data_),
  intensity_data(intensity_data_),
  bunch_spacing(bunch_spacing_)
{
  if (time_length >= bunch_spacing_)
    time_length = bunch_spacing_ - time_step;

  // check that exactly one sample has a drive_detuning
  drive_sample = CheckAndGetValidDriveSample();
  
  if (intensity_data.size() > 0 &&
      time_data.size() != intensity_data.front().size() &&
      drive_sample->drive_detuning.size() != intensity_data.size())
  {
    errors::WarningMessage("EnergyTimeSpectrum", "time_data and / or detuning_data do not match intensity_data dimensions");
  }

  if (fit_weight < 0.0)
    errors::WarningMessage("EnergyTimeSpectrum", "fit_weight must be >= 0.");

  if (distribution_points < 1)
    errors::WarningMessage("EnergyTimeSpectrum", "distribution points must be > 0.");

  CheckIsotope("EnergyTimeSpectrum");

  CheckGrazingAngleExperiment("EnergyTimeSpectrum");
};


BasicSample* EnergyTimeSpectrum::CheckAndGetValidDriveSample() const
{
  int drive_index = -1; // negative value -> no drive detuning given

  for (size_t i = 0; i < experiment->objects.size(); ++i)
  {
    if (BasicSample* const sample = dynamic_cast<BasicSample*>(experiment->objects[i]))
    {
      if (static_cast<int>(sample->drive_detuning.size()) > 0 &&
          drive_index < 0)
      {
        drive_index = static_cast<int>(i);
      }
      else if (sample->drive_detuning.size() > 0)
      {
        errors::WarningMessage("EnergyTimeSpectrum::operator()", "More than one Sample with drive_detuning found.");
      }
    }
  }

  if (drive_index < 0)
    errors::WarningMessage("EnergyTimeSpectrum::operator()", "No drive_detuning found.");

  return static_cast<BasicSample*>(experiment->objects[static_cast<unsigned int>(drive_index)]);
}


// calculate total system response and add to intensity
std::vector<double> EnergyTimeSpectrum::TimeResponse(const double fourier_norm,
  const double time_length_response, const double time_step_response, const bool electronic_response) const
{
  ExperimentMatrix2 experiment_matrix(detuning.size(), Eigen::Matrix2cd::Identity());

  for (NxObject* const obj : experiment->objects)
    experiment_matrix = matrix::Multiply(obj->object_matrix, experiment_matrix);

  if (electronic_response == false)
    experiment_matrix = SubtractElectronic(experiment_matrix);

  const ExperimentMatrix2 fourier_matrix = fourier::FourierTransform(experiment_matrix, fourier::FourierScaling::SqrtN, "none");

  std::vector<double> time_spectrum_intensity = intensity::Intensity(fourier_matrix, experiment->beam);
 
  for (double& intens : time_spectrum_intensity)
    intens *= fourier_norm;

  time_spectrum_intensity = BunchSpacingCorrection(time_spectrum_intensity, bunch_spacing, time_step_response);

  time = PopulateTime(time_length_response, time_step_response);
  
  time_spectrum_intensity.resize(time.size());

  return time_spectrum_intensity;
}


std::vector<std::vector<double>> EnergyTimeSpectrum::IntensityInterpolated(BasicSample* drive_sample, const double fourier_norm) const
{
  std::vector<std::vector<double>> intensity{};

  // find maximum abs(values) of detuning and drive_detuning
  const double max_detuning_value = abs(*std::max_element(detuning.begin(), detuning.end(),
    [](const double& a, const double& b)
    {
      return  abs(a) < abs(b);
    }
  ));

  const double max_drive_detuning_value = abs(*std::max_element(drive_sample->drive_detuning.begin(), drive_sample->drive_detuning.end(),
    [](const double& a, const double& b)
    {
      return  abs(a) < abs(b);
    }
  ));

  //find the needed step size of the extended detuning 
  const double detuning_step = detuning.at(1) - detuning.at(0);

  //find number of points for the extended detuning
  const size_t extended_detuning_points = static_cast<size_t>(round(2 * (max_detuning_value + max_drive_detuning_value) / detuning_step + 1));

  std::vector<double> extended_detuning(extended_detuning_points);

  for (size_t i = 0; i < extended_detuning_points; ++i)
    extended_detuning[i] = -max_detuning_value - max_drive_detuning_value + i * detuning_step;

  // calculate the drive_sample_matrix on the extended detuning
  const ObjectMatrix2 drive_sample_matrix = drive_sample->ObjectMatrix(experiment->isotope, extended_detuning, false);

  // the object matrix size might be zero up to here, so resize to detuning size
  drive_sample->object_matrix.resize(detuning.size());

  for (const double drive_det : drive_sample->drive_detuning)
  {
    // Interpolate complex layer matrix linearly
    //std::transform(NEXUS_EXECUTION_POLICY detuning.begin(), detuning.end(), drive_sample->object_matrix.begin(), [&](const double det)
    std::transform(detuning.begin(), detuning.end(), drive_sample->object_matrix.begin(), [&](const double det)
      {
        // -drive_det shifts the spectrum to the correct direction.
        // interpolate on the extended grid
        return interpolate::InterpolateLinear<Eigen::Matrix2cd>(extended_detuning, drive_sample_matrix, det - drive_det, false);
      }
    );

    std::vector<double> time_spectrum_intensity = TimeResponse(fourier_norm, time_length, time_step, electronic);

    intensity.push_back(time_spectrum_intensity);
  }
  
  return intensity;
}


std::vector<std::vector<double>> EnergyTimeSpectrum::IntensityFull(BasicSample* drive_sample, const double fourier_norm) const
{
  std::vector<std::vector<double>> intensity{};

  // store isomer shifts of each Hyperfine site of drive_sample / all layers / all materials
  std::vector<double> isomer_values;

  for (Layer* layer : drive_sample->layers)
  {
    for (Hyperfine* hyperfine_site : layer->material->hyperfine_sites)
      isomer_values.push_back(hyperfine_site->isomer->value);
  }

  // calculate time spectrum for each drive detuning
  for (const double drive_det : drive_sample->drive_detuning)
  {
    // set isomer shifts to detuned value and update to get all dependencies correct
    size_t i = 0;

    // shift isomer shift in all sample hyperfine sites
    for (Layer* layer : drive_sample->layers)
    {
      for (Hyperfine* hyperfine_site : layer->material->hyperfine_sites)
      {
        hyperfine_site->isomer->value = isomer_values[i] + drive_det / 1.0e-3 * constants::kSpeedOfLight /
          experiment->isotope->energy * experiment->isotope->gamma;

        ++i;
      }
    }

    drive_sample->Update();

    // calculate the sample_matrix of the drive_sample
    drive_sample->object_matrix = drive_sample->ObjectMatrix(experiment->isotope, detuning, true);

    std::vector<double> time_spectrum_intensity = TimeResponse(fourier_norm, time_length, time_step, electronic);

    intensity.push_back(time_spectrum_intensity);
  }

  //set all isomer values to original data
  size_t i = 0;

  for (Layer* layer : drive_sample->layers)
  {
    for (Hyperfine* hyperfine_site : layer->material->hyperfine_sites)
    {
      hyperfine_site->isomer->value = isomer_values[i];

      ++i;
    }
  }

  return intensity;
}


std::vector<std::vector<double>> EnergyTimeSpectrum::operator() (const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  drive_sample = CheckAndGetValidDriveSample();

  if (update_and_calc_transitions)
    Update();

  if (check_W_matrix)
    CheckWmatrixValidityExperiment("EnergyTimeSpectrum", experiment->isotope->energy);
    
  // the number of detuning points needed for a proper time spectrum
  const size_t number_detuning_points = NumberDetuningPoints(time_length, time_step, experiment->isotope->lifetime);

  // detuning might be cut in size due to max_detuning parameter
  detuning = PopulateDetuning(number_detuning_points, time_step, 0, experiment->isotope->lifetime);

  const double detuning_step = detuning.at(1) - detuning.front();

  const double fourier_norm = fourier::NormalizationIntegratedIntensityScaled(detuning_step, time_step);

  // calculate only non-changing objects once in advance (analyzer, fixed object conuss_object, ...)
  std::for_each(experiment->objects.begin(), experiment->objects.end(),
    [&](NxObject* obj)
    {
      //if (obj->object_id != NxObjectId::Sample)
      if (obj->object_id != NxObjectId::ForwardSample && obj->object_id != NxObjectId::GrazingSample)
        obj->object_matrix = obj->ObjectMatrix(experiment->isotope, detuning, update_and_calc_transitions);
    }
  );

  experiment->ResetDistributionIndex();

  experiment->ResetDivergenceIndex();

  experiment->StoreForwardThicknesses();

  experiment->StoreGrazingAngles();

  const size_t total_distribution_combinations = experiment->PopulateThicknessDistribution(distribution_points);

  const size_t total_divergence_combinations = experiment->PopulateDivergenceDistribution(distribution_points);

  std::vector<std::vector<double>> intensity{};

  std::vector<std::vector<double>> total_intensity{};

  // cycle through all divergence combinations
  for (size_t j = 0; j < total_divergence_combinations; ++j)
  {
    const double weight_divergence = experiment->SetAngleAndGetWeight();

    // recalculate all objects in grazing geometry for angular change
    // this is not optimal but otherwise quite complex to implement
    std::for_each(experiment->objects.begin(), experiment->objects.end(),
      [&](NxObject* obj)
      {
        if (GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
          sample->object_matrix = sample->ObjectMatrix(experiment->isotope, detuning, update_and_calc_transitions);
      }
    );

    // cycle through all forward geometry thickness combinations
    for (size_t i = 0; i < total_distribution_combinations; ++i)
    {
      const double weight = weight_divergence * experiment->SetForwardThicknessAndGetWeight();

      // recalculate all objects in forward geometry for thickness change
      // this is not optimal but otherwise quite complex to implement
      std::for_each(experiment->objects.begin(), experiment->objects.end(),
        [&](NxObject* obj)
        {
          if (ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
            sample->object_matrix = sample->ObjectMatrix(experiment->isotope, detuning, update_and_calc_transitions);
        }
      );

      if (mode == "i")
      {
        intensity = IntensityInterpolated(drive_sample, fourier_norm);
      }
      else if (mode == "f")
      {
        intensity = IntensityFull(drive_sample, fourier_norm);
      }
      else
      {
        errors::WarningMessage("EnergyTimeSpectrum::operator()", "Calculation mode not recognized.");
      }

      if (i == 0 &&
          j == 0)
      {
        total_intensity.assign(intensity.size(), std::vector<double>(intensity.front().size(), 0.0));
      }

      const size_t outer_size = total_intensity.size();

      const size_t inner_size = total_intensity.front().size();

      // incoherent sum of intensities
      for (size_t m = 0; m < outer_size; ++m)
      {
        for (size_t n = 0; n < inner_size; ++n)
          total_intensity[m][n] += weight * intensity[m][n];
      }

      experiment->IncreaseDistributionIndex(distribution_points);
    }

    experiment->ResetDistributionIndex();

    experiment->IncreaseDivergenceIndex(distribution_points);
  }

  experiment->RecallForwardThicknesses();

  experiment->RecallGrazingAngles();

  experiment->ResetDistributionIndex();

  experiment->ResetDivergenceIndex();

  if (resolution->value > 0.0 ||
      resolution_2->value > 0.0)
  {
    resolution_kernel = convolution::GaussianKernel2D(detuning, resolution->value, time, resolution_2->value, true);
  
    total_intensity = convolution::SameConvolution2D(total_intensity, resolution_kernel);
  }

  std::for_each(NEXUS_EXECUTION_POLICY total_intensity.begin(), total_intensity.end(),
    [&](std::vector<double>& intens_vec)
    {
      std::for_each(intens_vec.begin(), intens_vec.end(), [&](double& intens)
        {
          intens = background->value + intens * scaling->value;
        }
      );
    }
  );

  result = total_intensity;

  return total_intensity;
}


bool EnergyTimeSpectrum::operator() (double const* const* parameters, double* residuals) const
{
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = *parameters[i];

  const std::vector<std::vector<double>> intensity_fit = operator()(true, false);

  const std::vector<std::vector<double>> intensity_interpolated = InterpolateTimeaxis(time_data, intensity_data, time, intensity_fit);

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_interpolated);

  return true;
}

double EnergyTimeSpectrum::Residuals() const
{
  const std::vector<std::vector<double>> intensity_fit = operator()(true, false);

  const std::vector<std::vector<double>> intensity_interpolated = InterpolateTimeaxis(time_data, intensity_data, time, intensity_fit);

  return WeightedUserResidual(intensity_data, intensity_interpolated);
}


IntegratedEnergyTimeSpectrum::IntegratedEnergyTimeSpectrum(
  Experiment* const experiment_,
  const double time_length_,
  const double time_step_,
  const double integration_start_,
  const double integration_stop_,
  const std::string mode_,
  const bool electronic_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const energy_resolution_,
  Var* const time_resolution_,
  const unsigned int distribution_points_,
  const double fit_weight_,
  const double bunch_spacing_,
  Residual* const residual_,
  const std::string id_
) :
  FitMeasurement(experiment_,
    std::vector<double>(0), // detuning
    std::vector<double>(0), // intensity
    distribution_points_,
    scaling_,
    background_,
    nullptr,
    intensity_data_.size(),
    fit_weight_,
    energy_resolution_,
    time_resolution_,
    "Gauss",
    residual_,
    false,
    id_
  ),
  time_length(time_length_),
  time_step(time_step_),
  integration_start(integration_start_),
  integration_stop(integration_stop_),
  mode(mode_),
  electronic(electronic_),
  intensity_data(intensity_data_),
  bunch_spacing(bunch_spacing_)
{
  if (time_length >= bunch_spacing_)
    time_length = bunch_spacing_ - time_step;

  drive_sample = CheckAndGetValidDriveSample();

  if (intensity_data_.size() > 0 &&
      drive_sample->drive_detuning.size() != intensity_data_.size())
  {
    errors::WarningMessage("IntegratedEnergyTimeSpectrum", "drive detuning and intensity_data not of same size.");
  }

  if (fit_weight < 0.0)
    errors::WarningMessage("IntegratedEnergyTimeSpectrum", "fit_weight must be >= 0.");

  if (distribution_points < 1)
    errors::WarningMessage("IntegratedEnergyTimeSpectrum", "distribution points must be > 0.");

  CheckIsotope("IntegratedEnergyTimeSpectrum");

  CheckGrazingAngleExperiment("IntegratedEnergyTimeSpectrum");
};


BasicSample* IntegratedEnergyTimeSpectrum::CheckAndGetValidDriveSample() const
{
  int drive_index = -1; // negative value -> no drive detuning given

  for (size_t i = 0; i < experiment->objects.size(); ++i)
  {
    if (BasicSample* const sample = dynamic_cast<BasicSample*>(experiment->objects[i]))
    {
      if (sample->drive_detuning.size() > 0 &&
          drive_index < 0)
      {
        drive_index = static_cast<int>(i);
      }
      else if (sample->drive_detuning.size() > 0)
      {
        errors::WarningMessage("IntegratedEnergyTimeSpectrum::operator()", "More than one Sample with drive_detuning found.");
      }
    }
  }

  if (drive_index < 0)
    errors::WarningMessage("IntegratedEnergyTimeSpectrum::operator()", "No drive_detuning found.");

  return static_cast<BasicSample*>(experiment->objects[static_cast<size_t>(drive_index)]);
}


std::vector<double> IntegratedEnergyTimeSpectrum::operator() (const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  const EnergyTimeSpectrum energy_time_spec = EnergyTimeSpectrum(experiment, time_length, time_step, mode, electronic, std::vector<double>(1), std::vector<std::vector<double>>(1, std::vector<double>(1)),
    scaling, background, resolution, resolution_2, distribution_points, fit_weight, bunch_spacing, residual, "");

  const std::vector<std::vector<double>> energy_spectrum = energy_time_spec(update_and_calc_transitions, check_W_matrix);

  time = energy_time_spec.time;

  std::vector<double> integrated_intensity(energy_spectrum.size(), 0.0);

  std::transform(energy_spectrum.begin(), energy_spectrum.end(), integrated_intensity.begin(),
    [&](std::vector<double> time_axis)
    {
      const std::vector<double>::iterator start_time_iterator = std::lower_bound(time.begin(), time.end(), integration_start);

      const std::ptrdiff_t start_time_distance = std::distance(time.begin(), start_time_iterator);

      const std::vector<double>::iterator stop_time_iterator = std::lower_bound(time.begin(), time.end(), integration_stop);

      const std::ptrdiff_t stop_time_distance = std::distance(time.begin(), stop_time_iterator);

      return integrate::IntegrateTrapez(time_step, time_axis, start_time_distance, stop_time_distance);
    }
  );

  result = integrated_intensity;

  return integrated_intensity;
}


bool IntegratedEnergyTimeSpectrum::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  const std::vector<double> intensity_fit = operator()(true, false);

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_fit);

  return true;
}


double IntegratedEnergyTimeSpectrum::Residuals() const
{
  const std::vector<double> intensity_fit = operator()(true, false);

  return WeightedUserResidual(intensity_data, intensity_fit);
}
