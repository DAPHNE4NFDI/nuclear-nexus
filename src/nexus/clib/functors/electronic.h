// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Functor definitons for calculations of pure electronic scattering.
// for calculations of amplitudes, coherency matrices, field amplitudes, etc. of the experiment


#ifndef NEXUS_ELECTRONIC_H_
#define NEXUS_ELECTRONIC_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"


// only used for reflectivity and transmission functors
struct Amplitude : public Measurement {
  Amplitude(
    Experiment* const experiment,
    const double energy,
    const std::string id = ""
  );

  Complex operator() (const bool update = false) const;  // called by other methods, normally no update needed

  double energy = 0.0;
};


// only used for electronic subtraction
struct AmplitudeMatrix : public Measurement {
  AmplitudeMatrix(
    Experiment* const experiment,
    const double energy,
    const std::string id = ""
  );

  Eigen::Matrix2cd operator() (const bool update = false) const;  // called by other methods, normally no update needed

  double energy = 0.0;
};


/**
Constructor for the :class:`Amplitudes` class.
Calculates the complex electronic field amplitudes after each object of the experiment.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).
   result (list): List of complex scattering amplitudes.
*/
struct Amplitudes : public Measurement {
  Amplitudes(
    Experiment* const experiment,
    const double energy,
    const std::string id = ""
  );

  std::vector<Complex> operator() () const;

  mutable std::vector<Complex> result{};

  double energy = 0.0;
};


/**
Constructor for the :class:`Matrices` class.
Calculates the complex electronic scattering matrix after each object of the experiment.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).
   result (list): List of complex scattering amplitudes.
*/
struct Matrices : public Measurement {
  Matrices(
    Experiment* const experiment,
    const double energy,
    const std::string id = ""
  );

  std::vector<Eigen::Matrix2cd> operator() () const;

  mutable std::vector<Eigen::Matrix2cd> result{};

  double energy = 0.0;
};


/**
Constructor for the :class:`JonesVectors` class.
Calculates the complex electronic Jones vector after each object of the experiment.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).
   result (list): List of complex scattering amplitudes.
*/
struct JonesVectors : public Measurement {
  JonesVectors(
    Experiment* const experiment,
    const double energy,
    const std::string id = ""
  );

  std::vector<Eigen::Vector2cd> operator() () const;

  mutable std::vector<Eigen::Vector2cd> result{};

  double energy = 0.0;
};


/**
Constructor for the :class:`Matrices` class.
Calculates the intensities after each object from the polarization dependent scattering matrix of the experiment. 

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   energy (float): X-ray energy (eV).
   result (list): List of complex scattering amplitudes.
*/
struct Intensities : public Measurement {
  Intensities(
    Experiment* const experiment,
    const double energy,
    const std::string id = ""
  );

  std::vector<double> operator() () const;

  mutable std::vector<double> result{};

  double energy = 0.0;
};


/**
Constructor for the :class:`FieldAmplitude` class.
Calculates the depth-dependent field amplitude inside a sample.

Args:
   id (string): User identifier.
   sample (:class:`BasicSample`): Sample for the calculation. In case that the experiment has several objects only the field amplitude of the given sample is determined.
   energy (float): X-ray energy (eV).
   points (int): Number of calculation points.

Attributes:
   id (string): User identifier.
   sample (:class:`BasicSample`): Sample for the calculation.  In case that the experiment has several objects only the field amplitude of the given sample is determined.
   energy (float): X-ray energy (eV).
   result (list): List of complex scattering amplitudes.
   depth (list): List of the depth values for the calculation.
   num_points (int): Number of calculation points.
*/
struct FieldAmplitude : public Measurement {
  FieldAmplitude(
    BasicSample* const sample,
    const double energy,
    const unsigned int points = 101,
    const std::string id = ""
  );

  std::vector<Complex> operator() () const;

  mutable std::vector<Complex> result{};

  mutable std::vector<double> depth{};

  BasicSample* sample = nullptr;

  double energy = 0.0;

  unsigned int num_points = 0;
};


/**
Constructor for the :class:`FieldIntensity` class.
Calculates the depth-dependent field intensity inside a sample.

Args:
   id (string): User identifier.
   sample (:class:`BasicSample`): Sample for the calculation. In case that the experiment has several objects only the field amplitude of the given sample is determined.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`BasicSample`.

   energy (float): X-ray energy (eV).
   points (int): Number of calculation points.

Attributes:
   id (string): User identifier.
   sample (:class:`BasicSample`): Sample for the calculation. In case that the experiment has several objects only the field amplitude of the given sample is determined.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`BasicSample`.

   energy (float): X-ray energy (eV).
   result (list): List of field intensities
   depth (list): List of the depth values for the calculation.
   num_points (int): Number of calculation points.
*/
struct FieldIntensity : public Measurement {
  FieldIntensity(
    BasicSample* const sample,
    const double energy,
    const unsigned int points = 101,
    const std::string id = ""
  );

  std::vector<double> operator() () const;

  mutable std::vector<double> result{};

  mutable std::vector<double> depth{};

  BasicSample* sample = nullptr;

  double energy = 0.0;

  unsigned int num_points = 0;
};

#endif // NEXUS_ELECTRONIC_H_
