// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "electronic_transmission.h"

#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>

#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/convolution.h"
#include "../functors/electronic.h"


TransmissionAmplitude::TransmissionAmplitude(
  Experiment* const experiment_,
  GrazingSample* const sample_,
  const double energy_,
  const std::vector<double>& angles_,
  const std::string id_
) : 
  Measurement(experiment_, id_),
  sample(sample_),
  energy(energy_),
  angles(angles_)
{
  CheckForSampleInExperiment("TransmissionAmplitude", sample_);
}

std::vector<Complex> TransmissionAmplitude::operator()() const
{
  Update();

  std::vector<Complex> transmission(angles.size());

  // store actual sample angle
  const double sample_angle = sample->angle->value;

  if (*std::min_element(angles.begin(), angles.end()) <= 0.0)
    errors::WarningMessage("TransmissionAmplitude::operator()", "All angles must be larger than zero.");

  Amplitude electronic_amplitude_functor(experiment, energy);

  std::transform(angles.begin(), angles.end(), transmission.begin(),
    [&](const double angle)
    {
      sample->angle->value = angle;

      return electronic_amplitude_functor(false);
    }
  );

  // set sample_angle back to original value
  sample->angle->value = sample_angle;

  result = transmission;

  return transmission;
}


Transmission::Transmission(
  Experiment* const experiment_,
  GrazingSample* const sample_,
  const double energy_, 
  const std::vector<double>& angles_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const offset_,
  Var* const resolution_,
  const double fit_weight_,
  Residual* const residual_,
  const std::string id_
) :
  FitMeasurement(experiment_,
    std::vector<double>(0),
    intensity_data_,
    1,
    scaling_,
    background_,
    offset_,
    intensity_data_.size(),
    fit_weight_,
    resolution_,
    nullptr,
    "Gauss",
    residual_,
    false,
    id_),
  sample(sample_),
  energy(energy_),
  angles(angles_)
{
  if (intensity_data.size() > 0 &&
      angles.size() != intensity_data.size())
  {
    errors::WarningMessage("Transmission", "angles and intensity_data not of same size");
  }

  if (fit_weight < 0.0)
    errors::WarningMessage("Transmission", "fit_weight must be >= 0.");

  CheckForSampleInExperiment("Transmission", sample);
}


std::vector<double> Transmission::operator()() const
{
  Update();

  std::vector<double> transmission(angles.size());

  // store actual sample angle
  const double sample_angle = sample->angle->value;

  if (*std::min_element(angles.begin(), angles.end()) <= 0.0)
    errors::WarningMessage("Transmission::operator()", "All angles must be larger than zero.");

  Amplitude electronic_amplitude_functor(experiment, energy);

  std::transform(angles.begin(), angles.end(), transmission.begin(), [&](const double angle)
    {
      sample->angle->value = angle + offset->value;

      return std::norm(electronic_amplitude_functor(false))
              * BeamProfileCorrection(angle + offset->value, sample->length, experiment->beam->fwhm->value, experiment->beam->profile);
    }
  );

  if (resolution->value > 0.0)
  {
    resolution_kernel = convolution::GaussianKernel(angles, resolution->value, true);

    transmission = convolution::SameConvolution(transmission, resolution_kernel);
  }

  std::for_each(transmission.begin(), transmission.end(),
    [&](double& trans)
    {
      trans = background->value + trans * scaling->value;
    }
  );

  // set sample_angle back to original value
  sample->angle->value = sample_angle;

  result = transmission;

  return transmission;
}


bool Transmission::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  const std::vector<double> intensity_fit = operator()();

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_fit);

  return true;
}

double Transmission::Residuals() const
{
  const std::vector<double> intensity_fit = operator()();

  return WeightedUserResidual(intensity_data, intensity_fit);
}
