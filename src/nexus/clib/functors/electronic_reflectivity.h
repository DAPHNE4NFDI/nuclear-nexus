// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the pure electronic reflectivity (amplitude and intensity) functor.


#ifndef NEXUS_ELECTRONIC_REFLECTIVITY_H_
#define NEXUS_ELECTRONIC_REFLECTIVITY_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"
#include "../functors/fit_measurement.h"


/**
Constructor for the :class:`ReflectivityAmplitude` class.
Calculates the amplitude of the experiment for one sample whose incidence angle is changed in reflection geometry.
No beam profile correction is applied.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample of the :class:`Experiment` whose angle is changed.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

        energy (float): X-ray energy in eV.
   angles (list): List of the angles.

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample of the :class:`Experiment` whose angle is changed.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

        energy (float): X-ray energy in eV.
   angles (list): List of the angles.
   result (list): List of intensity values.
*/
struct ReflectivityAmplitude : public Measurement {
  ReflectivityAmplitude(
    Experiment* const experiment,
    GrazingSample* const sample,
    const double energy,
    const std::vector<double>& angles,
    const std::string id
  );

  std::vector<Complex> operator() () const;

  mutable std::vector<Complex> result{};

  GrazingSample* sample = nullptr;

  double energy = 0.0;
  
  std::vector<double> angles{};
};


/**
Constructor for the :class:`Reflectivity` class.
Calculates the intensity of the experiment for one sample whose incidence angle is changed in reflection geometry.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample of the :class:`Experiment` whose angle is changed.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

   energy (float): X-ray energy (eV).
   angles (list): List of the angles (degree).
   intensity_data (list or ndarray): Intensity data for fitting.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``0``.
   offset (float or :class:`Var`): Angular offset of the model. Default is ``0``.

     .. versionadded:: 1.1.0

   resolution (float or :class:`Var`): Resolution value for convolution (degree).
   fit_weight (float): Relative weight for the const function in multi-measurement fitting.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample of the :class:`Experiment` whose angle is changed.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

   energy (float): X-ray energy (eV).
   angles (list): List of the angles (degree).
   result (list): List of intensity values.
   intensity_data (list or ndarray): Intensity data for fitting.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   offset (float or :class:`Var`): Angular offset of the model.

     .. versionadded:: 1.1.0

   resolution (:class:`Var`): Resolution value for convolution (degree).
   resolution_kernel (list): Weight of the Gaussian resolution same step size as detuning.
   fit_weight (float): Relative weight for the const function in multi-measurement fitting.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
*/
struct Reflectivity : public FitMeasurement {
  Reflectivity(
    Experiment* const experiment,
    GrazingSample* const sample,
    const double energy,
    const std::vector<double>& angles,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const offset,
    Var* const resolution,
    const double fit_weight,
    Residual* const residual,
    const std::string id
  );

  std::vector<double> operator() () const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;

  GrazingSample* sample = nullptr;

  double energy = 0.0;
  
  std::vector<double> angles{};

  mutable std::vector<double> resolution_kernel{};
};

#endif // NEXUS_ELECTRONIC_REFLECTIVITY_H_
