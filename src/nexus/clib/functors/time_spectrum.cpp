// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Normalization of the FFT with 1/sqrt(N) ensures Parseval's theorem for the DFT (see also Rayleigh's energy theorem and Plancherel theorem). 
// 
//     Sum_k |X(E)_k|^2 = 1/N * Sum_n |X_n(t)|^2
// 
// The Fourier transform is given by
// 
//     X(t) = F(x(E)) = Int x(E) * exp(-i*2pi*E/hbar*t) dE
// 
// where x(E) is the energy dependent scattering matrix entry [unitless].
// The unit of the Foruier transform X(t) is [eV] or [Gamma].
// But for the 
// 
// First the DFT coefficients are determined and scaled to Parseval's theorem (1/sqrt(N)).
// 
// X(k) = Sum_n^(N-1) x(n) exp(-i*2pi*kn/N)
// 
// The implied unit of the coefficient x(n) is that of x(E) [unitless].
// The implied unit of X(E) is then the same as the implied unit of x(n) [unitless].
// See J. Ahrens et al., "Tutorial on Scaling of the Discrete Fourier Transform and the Implied Physical Units of the Spectra of Time - Discrete Signals",
// Audio Engineering Society, Convention e - Brief 56.
// 
// The 1/sqrt(N) will change the amplitude/intensity when the FFT points or time_step change.
// To get around one can use the energy-integrated intensity scaling
// 
//     delta_E * Sum_k |X(E)_k|^2 = delta_E / N * Sum_n |X_n(t)|^2
//  
// and in the code (the Fourier coefficients are already scaled)
// 
//     delta_E * Sum_k |X(E)_k|^2 = delta_E * Sum_n |1/sqrt(N) * X_n(t)|^2
// 
// which still obeys Parseval's theorem.
// It has the units [eV or Gamma].
// 
// This scaling is now altered to account for the changing sampling rate (time_step), for which each bin represents a changing Fourier band
// 
//    energy_step / time_step * Sum_k^(N-1) |x(energy_step)_k|^2 = energy_step / time_step * Sum_n^(N-1) |1/sqrt(N) * x(fourier_step)_n|^2
//
// in units [eV/s] or [Gamma/s].
// This property is independent on the time settings.
// It is the energy-integrated intensity per time.


#include "time_spectrum.h"

#include <iostream>
#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include "../utilities/errors.h"
#include "../math/constants.h"

#include "../math/intensity.h"
#include "../math/interpolate.h"
#include "../math/convolution.h"
#include "../math/matrix.h"
#include "../functors/electronic.h"


// valid FFt window options
const std::vector<std::string> fft_window_list
{
  "none",
  "auto",
  "Hann",
  "Hamming",
  "Sine",
  "Welch",
  "Kaiser2",
  "Kaiser25",
  "Kaiser3"
};


AmplitudeTime::AmplitudeTime(
  Experiment* const experiment_,
  const double tlength_,
  const double tstep_,
  const bool electronic_,
  const std::string fft_window_,
  const std::string id_)
  : Measurement(experiment_, id_),
  time_length(tlength_),
  time_step(tstep_),
  electronic(electronic_),
  fft_window(fft_window_)
{
  CheckIsotope("AmplitudeTime");

  CheckGrazingAngleExperiment("AmplitudeTime");

  if (std::find(fft_window_list.begin(), fft_window_list.end(), fft_window) == fft_window_list.end())
  {
    errors::WarningMessage("AmplitudeTime", "fft_window " + fft_window + " not found. Set to auto.");
    fft_window = "auto";
  }

  if (fft_window == "auto")
    fft_window = "none";
};

intensity::AmplitudeVector AmplitudeTime::AmplitudeTimeDetuning(const ExperimentMatrix2& matrix) const
{
  const double detuning_step = detuning.at(1) - detuning.front();

  const ExperimentMatrix2 fourier_matrix = fourier::FourierTransform(matrix, fourier::FourierScaling::SqrtN, fft_window);   // implied unit of [unitless]

  intensity::AmplitudeVector amplitude_vector = intensity::Amplitude(fourier_matrix, experiment->beam);

  const double fourier_norm = sqrt(fourier::NormalizationIntegratedIntensityScaled(detuning_step, time_step));   // unit of [sqrt(Gamma/ns)], time_step is in ns.

  for (Eigen::Vector2cd& amplitude : amplitude_vector)
    amplitude *= fourier_norm;    // unit of [sqrt(Gamma/ns)]

  return amplitude_vector;
}

intensity::AmplitudeVector AmplitudeTime::operator()(const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  if (update_and_calc_transitions)
    Update();

  if (check_W_matrix)
    CheckWmatrixValidityExperiment("AmplitudeTime", experiment->isotope->energy);

  // the number of detuning points needed for a proper time spectrum
  const size_t number_detuning_points = NumberDetuningPoints(time_length, time_step, experiment->isotope->lifetime);

  // detuning might be cut in size due to max_detuning
  detuning = PopulateDetuning(number_detuning_points, time_step, 0, experiment->isotope->lifetime);

  ExperimentMatrix2 experiment_matrix = experiment->Matrix(detuning, update_and_calc_transitions);

  if (electronic == false)
    experiment_matrix = SubtractElectronic(experiment_matrix);

  intensity::AmplitudeVector amplitude = AmplitudeTimeDetuning(experiment_matrix);

  time = PopulateTime(time_length, time_step);

  amplitude.resize(time.size());

  result = amplitude;

  return amplitude;
}


// Time Spectrum
TimeSpectrum::TimeSpectrum(
  Experiment* const experiment_,
  const double time_length_,
  const double time_step_,
  const double max_detuning_,
  const bool electronic_,
  const std::vector<double>& time_data_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const offset_,
  Var* const resolution_,
  const unsigned int distribution_points_,
  const double fit_weight_,
  const double bunch_spacing_,
  Residual* const residual_,
  const std::string fft_window_,
  const bool coherence_,
  const std::string id_
) :
  FitMeasurement(experiment_,
    std::vector<double>(0),
    intensity_data_,
    distribution_points_,
    scaling_,
    background_,
    offset_,
    intensity_data_.size(),
    fit_weight_,
    resolution_,
    nullptr,
    "Gauss",
    residual_,
    coherence_,
    id_),
  time_length(time_length_),
  time_step(time_step_),
  max_detuning(max_detuning_),
  electronic(electronic_),
  time_data(time_data_),
  //intensity_data(intensity_data_),
  bunch_spacing(bunch_spacing_),
  fft_window(fft_window_)
{
  if (time_length >= bunch_spacing_)
    time_length = bunch_spacing_ - time_step;

  if (intensity_data_.size() > 0 &&
      time_data_.size() != intensity_data_.size())
  {
    errors::WarningMessage("TimeSpectrumDetuning", "time_data and intensity_data not of same size.");
  }

  if (fit_weight_ < 0.0)
    errors::WarningMessage("TimeSpectrum", "fit_weight must be >= 0.");

  if (distribution_points_ < 1)
    errors::WarningMessage("TimeSpectrum", "distribution points is" + std::to_string(distribution_points_) + " but must be > 0.");

  if (std::find(fft_window_list.begin(), fft_window_list.end(), fft_window) == fft_window_list.end())
  {
    errors::WarningMessage("TimeSpectrum", "fft_window " + fft_window + " not found. Set to auto.");
    fft_window = "auto";
  }

  if (fft_window == "auto" &&
      max_detuning != 0.0)
  {
    fft_window = "Hann";
  }
  else if (fft_window == "auto" &&
           max_detuning == 0.0)
  {
    fft_window = "none";
  }

  CheckIsotope("TimeSpectrum");

  if (id_ != "TimeSpecForNuclearReflectivity_75326")  // identifier for nuclear reflectivity
    CheckGrazingAngleExperiment("TimeSpectrum");
};

ExperimentMatrix2 TimeSpectrum::TimeSpectrumMatrix(const size_t num_detuning, const bool electronic_, const bool calc_transitions) const
{
  ExperimentMatrix2 experiment_matrix = experiment->Matrix(detuning, calc_transitions);

  if (electronic_ == false)
    experiment_matrix = SubtractElectronic(experiment_matrix);

  if (num_detuning > detuning.size())
    experiment_matrix = matrix::Padding(experiment_matrix, static_cast<int>(detuning.size()), static_cast<int>(num_detuning));

  return experiment_matrix;
}

std::vector<double> TimeSpectrum::TimeSpectrumDetuning(const LayerMatrix2& matrix) const
{
  const double detuning_step = detuning.at(1) - detuning.front();

  const ExperimentMatrix2 fourier_matrix = fourier::FourierTransform(matrix, fourier::FourierScaling::SqrtN, fft_window);  // implied unit [unitless]

  std::vector<double> intensity = intensity::Intensity(fourier_matrix, experiment->beam);

  const double fourier_norm_intensity = fourier::NormalizationIntegratedIntensityScaled(detuning_step, time_step);  // units of [Gamma/ns], time_step in [ns].

   for (double& intens : intensity)
     intens *= fourier_norm_intensity;  // units of [Gamma/ns]

  return intensity;
}

intensity::AmplitudeVector TimeSpectrum::AmplitudeTimeDetuning(const LayerMatrix2& matrix) const
{
  const double detuning_step = detuning.at(1) - detuning.front();

  const ExperimentMatrix2 fourier_matrix = fourier::FourierTransform(matrix, fourier::FourierScaling::SqrtN, fft_window);   // implied unit of [unitless]

  intensity::AmplitudeVector amplitude_vector = intensity::Amplitude(fourier_matrix, experiment->beam);

  const double fourier_norm = sqrt(fourier::NormalizationIntegratedIntensityScaled(detuning_step, time_step));   // unit of [sqrt(Gamma/ns)], time_step is in ns.

  for (Eigen::Vector2cd& amplitude : amplitude_vector)
    amplitude *= fourier_norm;    // unit of [sqrt(Gamma/ns)]

  return amplitude_vector;
}


std::vector<double> TimeSpectrum::operator()(const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  if (distribution_points < 1)
    errors::WarningMessage("TimeSpectrum", "distribution points is" + std::to_string(distribution_points) + " but must be > 0.");

  if (update_and_calc_transitions)
    Update();

  if (check_W_matrix)
    CheckWmatrixValidityExperiment("TimeSpectrum", experiment->isotope->energy);

  if (coherence == true && !(experiment->beam->ValidJonesVector()))
    errors::ErrorMessage("TimeSpectrum::operator()", "No valid Jones vector set in Beam.");

  // the number of detuning points needed for a proper time spectrum
  const size_t number_detuning_points = NumberDetuningPoints(time_length, time_step, experiment->isotope->lifetime);

  // detuning might be cut in size due to max_detuning
  detuning = PopulateDetuning(number_detuning_points, time_step, max_detuning, experiment->isotope->lifetime);

  intensity::AmplitudeVector total_amplitude{};

  std::vector<double> total_intensity{};

  experiment->ResetDistributionIndex();

  experiment->ResetDivergenceIndex();

  experiment->StoreForwardThicknesses();

  experiment->StoreGrazingAngles();

  const size_t total_distribution_combinations = experiment->PopulateThicknessDistribution(distribution_points);

  const size_t total_divergence_combinations = experiment->PopulateDivergenceDistribution(distribution_points);

  for (size_t j = 0; j < total_divergence_combinations; ++j)
  {
    const double weight_divergence = experiment->SetAngleAndGetWeight();

    for (size_t i = 0; i < total_distribution_combinations; ++i)
    {
      const double weight = weight_divergence * experiment->SetForwardThicknessAndGetWeight();

      const ExperimentMatrix2 experiment_matrix = TimeSpectrumMatrix(number_detuning_points, electronic, update_and_calc_transitions);

      if (coherence == false)
      {
        std::vector<double> intensity = TimeSpectrumDetuning(experiment_matrix);

        if (i == 0 &&
            j == 0)
        {
          total_intensity.resize(intensity.size(), 0.0);
        }

        // incoherent sum of intensities
        std::transform(total_intensity.begin(), total_intensity.end(), intensity.begin(), total_intensity.begin(),
          [weight](double& total_intens, const double& intens)
          {
            return total_intens + weight * intens;
          }
        );
      }
      else if (coherence == true)
      {
        intensity::AmplitudeVector amplitude = AmplitudeTimeDetuning(experiment_matrix);

        if (i == 0 &&
          j == 0)
        {
          total_amplitude.resize(amplitude.size(), Eigen::Vector2cd::Zero());
        }

        // coherent sum of amplitudes
        std::transform(NEXUS_EXECUTION_POLICY total_amplitude.begin(), total_amplitude.end(), amplitude.begin(), total_amplitude.begin(),
          [weight](Eigen::Vector2cd& total_ampl, const Eigen::Vector2cd& ampl)
          {
            return total_ampl + weight * ampl;
          }
        );
      }

      experiment->IncreaseDistributionIndex(distribution_points);
    }

    experiment->ResetDistributionIndex();

    experiment->IncreaseDivergenceIndex(distribution_points);
  }

  if (coherence == true)
  {
    total_intensity = intensity::Intensity(total_amplitude);
  }

  experiment->ResetDistributionIndex();

  experiment->ResetDivergenceIndex();

  experiment->RecallForwardThicknesses();

  experiment->RecallGrazingAngles();

  total_intensity = BunchSpacingCorrection(total_intensity, bunch_spacing, time_step);

  time = PopulateTime(time_length, time_step, offset->value);

  total_intensity.resize(time.size());

  if (resolution->value > 0.0)
  {
    resolution_kernel = convolution::GaussianKernel(time, resolution->value);

    total_intensity = convolution::SameConvolution(total_intensity, resolution_kernel);
  }

  std::for_each(total_intensity.begin(), total_intensity.end(), [&](double& intens)
    {
      intens = background->value + intens * scaling->value;
    }
  );

  result = total_intensity;

  return total_intensity;
}


bool TimeSpectrum::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  const std::vector<double> intensity_fit = operator()(true, false);

  const std::vector<double> intensity_interpolated = Interpolate(time_data, intensity_data, time, intensity_fit);

  result = intensity_interpolated;

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_interpolated);

  return true;
}


double TimeSpectrum::Residuals() const
{
  const std::vector<double> intensity_fit = operator()(true, false);

  const std::vector<double> intensity_interpolated = Interpolate(time_data, intensity_data, time, intensity_fit);

  result = intensity_interpolated;

  return WeightedUserResidual(intensity_data, intensity_interpolated);
}
