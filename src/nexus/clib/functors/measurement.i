// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module measurement

%{
    #define SWIG_FILE_WITH_INIT
    #include "measurement.h"
%}


%feature("autodoc", "List of :class:`Measurement` objects.") std::vector<Measurement*>;

%template(MeasurementVector) std::vector<Measurement*>;



%include "measurement.h"



%extend Measurement {
  %pythoncode{

  def experiment_output(self):
    output = "  .experiment.id: {}\n".format(self.experiment.id)
    output += "  .experiment.isotope: {}\n".format(self.experiment.isotope.isotope)
    return output

  def object_output(self):
    output = "  .objects:\n"
    for obj in self.experiment.objects:
      output += "    .id: {}  - type: ".format(obj.id)
      if obj.object_id == NxObjectId_Sample:
        output += "Sample"
      elif obj.object_id == NxObjectId_Analyzer:
        output += "Analyzer"
      elif obj.object_id == NxObjectId_FixedObject:
        output += "FixedObject"
      output += "\n"
    return output

  }
}
