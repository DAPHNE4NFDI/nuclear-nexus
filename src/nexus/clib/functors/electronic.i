// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module electronic

%{
    #define SWIG_FILE_WITH_INIT
    #include "electronic.h"
%}


%feature("shadow") Amplitudes::Amplitudes %{
def __init__(self, experiment, energy, id = ""):
    _cnexus.Amplitudes_swiginit(self, _cnexus.new_Amplitudes(experiment, energy, id))
%}

%feature("shadow") Matrices::Matrices %{
def __init__(self, experiment, energy, id = ""):
    _cnexus.Matrices_swiginit(self, _cnexus.new_Matrices(experiment, energy, id))
%}

%feature("shadow") JonesVectors::JonesVectors %{
def __init__(self, experiment, energy, id = ""):
    _cnexus.JonesVectors_swiginit(self, _cnexus.new_JonesVectors(experiment, energy, id))
%}

%feature("shadow") Intensities::Intensities %{
def __init__(self, experiment, energy, id = ""):
    _cnexus.Intensities_swiginit(self, _cnexus.new_Intensities(experiment, energy, id))
%}

%feature("shadow") FieldAmplitude::FieldAmplitude %{
def __init__(self, sample, energy, points = 101, id = ""):
    _cnexus.FieldAmplitude_swiginit(self, _cnexus.new_FieldAmplitude(sample, energy, points, id))
%}


%feature("shadow") FieldIntensity::FieldIntensity %{
def __init__(self, sample, energy, points = 101, id = ""):
    _cnexus.FieldIntensity_swiginit(self, _cnexus.new_FieldIntensity(sample, energy, points, id))
%}


%feature("shadow") Amplitudes::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of complex values.
    """
    return np.array(_cnexus.Amplitudes___call__(self))
%}


%feature("shadow") Matrices::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of complex 2x2 matrices.
    """
    return np.squeeze(_cnexus.Matrices___call__(self))
%}


%feature("shadow") JonesVectors::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of complex Jones vectors.
    """
    return np.squeeze(_cnexus.JonesVectors___call__(self))
%}


%feature("shadow") Intensities::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of intensities.
    """
    return np.array(_cnexus.Intensities___call__(self))
%}


%feature("shadow") FieldAmplitude::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray, ndarray: Array of depth values, array of amplitude values.
    """
    output = _cnexus.FieldAmplitude___call__(self)
    return np.array(self.depth), np.array(output)

def Plot(self):
    r"""
    Plot the field amplitude in a sample.
    """
    depth, amp = self.__call__()

    plt.plot(depth, np.real(amp))
    plt.plot(depth, np.imag(amp))

    axes = plt.gca()
    y_min, y_max = axes.get_ylim()
    plt.vlines(self.sample.Interfaces(), y_min, y_max, colors='g', linestyles='dashed')
    s = -1
    for id, location in zip(self.sample.Ids(), np.array(self.sample.Interfaces()) + 0.5):
        plt.text(location, y_max, id, fontsize = 10)
        y_max = y_max + s * 0.5
        s *= -1

    plt.xlabel('depth (nm)')
    plt.ylabel('relative amplitude')
    plt.show()
%}


%feature("shadow") FieldIntensity::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray, ndarray: Array of depth values, array of intensity values.
    """
    output = _cnexus.FieldIntensity___call__(self)
    return np.array(self.depth), np.array(output)

def Plot(self):
    r"""
    Plot the field intensity in a sample.
    """
    depth, int = self.__call__()

    plt.plot(depth, int)

    axes = plt.gca()
    y_min, y_max = axes.get_ylim()
    plt.vlines(self.sample.Interfaces(), y_min, y_max, colors='g', linestyles='dashed')
    s = -1
    for id, location in zip(self.sample.Ids(), np.array(self.sample.Interfaces()) + 0.5):
        plt.text(location, y_max, id, fontsize = 10)
        y_max = y_max + s * 0.5
        s *= -1

    plt.xlabel('depth (nm)')
    plt.ylabel('relative intensity')

    plt.show()
%}



%include "electronic.h"



%extend Amplitudes {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this") and not hasattr(self, attr):
      raise Exception("Amplitudes does not have attribute {}".format(attr))

    super().__setattr__(attr, val)

  }
}


%extend FieldAmplitude {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this") and not hasattr(self, attr):
      raise Exception("FieldAmplitude does not have attribute {}".format(attr))

    super().__setattr__(attr, val)

  }
}

%extend FieldIntensity {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this") and not hasattr(self, attr):
      raise Exception("FieldIntensity does not have attribute {}".format(attr))

    super().__setattr__(attr, val)

  }
}