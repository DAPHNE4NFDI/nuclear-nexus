// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module energy_time_spectrum

%{
    #define SWIG_FILE_WITH_INIT
    #include "energy_time_spectrum.h"
%}


%feature("shadow") EnergyTimeAmplitude::EnergyTimeAmplitude %{
def __init__(self,
             experiment,
             time_length = 200,
             time_step = 0.2,
             mode = "i",
             electronic = False,
             id = ""):

    self._experiment = experiment

    _cnexus.EnergyTimeAmplitude_swiginit(self, _cnexus.new_EnergyTimeAmplitude(experiment, time_length, time_step, mode, electronic, id))
%}


%feature("shadow") EnergyTimeAmplitude::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: 2D array of Jones vectors.
    """
    output = np.array(_cnexus.EnergyTimeAmplitude___call__(self))
    return np.array(self.time), output
%}


%feature("shadow") EnergyTimeSpectrum::EnergyTimeSpectrum %{
def __init__(self,
             experiment,
             time_length = 200,
             time_step = 0.2,
             mode = "i",
             electronic = False,
             time_data = [],
             intensity_data = [[]],
             scaling = "auto",
             background = 0.0,
             energy_resolution = 0.0,
             time_resolution = internal_time_resolution,
             distribution_points = 1,
             fit_weight = 1.0,
             bunch_spacing = np.inf,
             residual = Sqrt(),
             id = ""):

    self._experiment = experiment

    intensity_data = np.array(intensity_data)

    if scaling == "auto" and intensity_data.size > 0:
      scaling = np.amax(intensity_data)
      scaling = Var(scaling, 0.0, 100*scaling, True, "ETS scaling")
    elif scaling == "auto" and intensity_data.size == 0:
      scaling = Var(1.0, 0.0, np.inf, True, "ETS scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 0.0, np.inf, True, "ETS scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and intensity_data.size > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100*background, True, "ETS backgr")
    elif background == "auto" and intensity_data.size == 0:
      background = Var(0.0, 0.0, np.inf, False, "ETS backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "ETS backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(energy_resolution, (int, float)):
      energy_resolution = Var(energy_resolution, 0.0, 10, False, "energy resolution")

    if isinstance(time_resolution, (int, float)):
      time_resolution = Var(time_resolution, 0.0, 10, False, "time resolution")

    self._scaling = scaling

    self._background = background

    self._energy_resolution = energy_resolution

    self._time_resolution = time_resolution

    self._residual = residual

    _cnexus.EnergyTimeSpectrum_swiginit(self, _cnexus.new_EnergyTimeSpectrum(experiment, time_length, time_step, mode, electronic, 
      time_data, intensity_data, scaling, background, energy_resolution, time_resolution, distribution_points, fit_weight, bunch_spacing, residual, id))
%}


%feature("shadow") EnergyTimeSpectrum::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: 2D array of intensities.
    """
    output = np.array(_cnexus.EnergyTimeSpectrum___call__(self))
    return np.array(self.time), output
%}


%feature("shadow") IntegratedEnergyTimeSpectrum::IntegratedEnergyTimeSpectrum %{
def __init__(self,
             experiment,
             time_length = 200,
             time_step = 0.2,
             integration_start = 0,
             integration_stop = 200,
             mode = "i",
             electronic = False,
             intensity_data = [],
             scaling = "auto",
             background = 0.0,
             energy_resolution = 0.0,
             time_resolution = internal_time_resolution,
             distribution = 1,
             fit_weight = 1.0,
             bunch_spacing = np.inf,
             residual = Sqrt(),
             id = ""):

    self._experiment = experiment

    intensity_data = np.array(intensity_data)

    if scaling == "auto" and intensity_data.size > 0:
      scaling = np.amax(intensity_data)
      scaling = Var(scaling, 0.0, 100*scaling, True, "IETS scaling")
    elif scaling == "auto" and intensity_data.size == 0:
      scaling = Var(1.0, 0.0, np.inf, True, "IETS scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 0.0, np.inf, True, "IETS scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and intensity_data.size > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100*background, True, "IETS backgr")
    elif background == "auto" and intensity_data.size == 0:
      background = Var(0.0, 0.0, np.inf, False, "IETS backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "IETS backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(energy_resolution, (int, float)):
      energy_resolution = Var(energy_resolution, 0.0, 10, False, "energy resolution")

    if isinstance(time_resolution, (int, float)):
      time_resolution = Var(time_resolution, 0.0, 10, False, "time resolution")

    self._scaling = scaling

    self._background = background

    self._energy_resolution = energy_resolution

    self._time_resolution = time_resolution

    self._residual = residual

    _cnexus.IntegratedEnergyTimeSpectrum_swiginit(self, _cnexus.new_IntegratedEnergyTimeSpectrum(experiment, time_length, time_step, integration_start, integration_stop,
       mode, electronic, intensity_data, scaling, background, energy_resolution, time_resolution, distribution, fit_weight, bunch_spacing, residual, id))
%}


%feature("shadow") IntegratedEnergyTimeSpectrum::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: array of intensities.
    """
    return np.array(_cnexus.IntegratedEnergyTimeSpectrum___call__(self))
%}



%include "energy_time_spectrum.h"



%extend EnergyTimeAmplitude {
  %pythoncode{

  Calculate = __call__
  
  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment") and not hasattr(self, attr):
      raise Exception("EnergyTimeAmplitude does not have attribute {}".format(attr))
    
    if attr == "experiment":
      self._experiment = experiment

    super().__setattr__(attr, val)

  }
}


%extend EnergyTimeSpectrum {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_scaling", "_background", "_energy_resolution", "_time_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("EnergyTimeSpectrum does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr in ("scaling", "background", "energy_resolution", "time_resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "energy_resolution":
      self._energy_resolution = val

    if attr == "time_resolution":
      self._time_resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)


  def Plot(self):
    r"""
    Plot the :class:`EnergyTimeSpectrum`.

    .. versionadded:: 1.0.3

    """
    if (len(self.result) == 0):
      raise Exception("NEXUS error: No .result found. Use .Calculate() before calling the result.")

    plt.imshow(np.array(self.result).T,
               cmap=plt.cm.plasma,
               norm=clr.LogNorm(),
               aspect='auto',
               origin='lower',
               extent=(min(self.drive_sample.drive_detuning), max(self.drive_sample.drive_detuning), min(self.time), max(self.time)))

    plt.colorbar(orientation='vertical', label = r'intensity ($\Gamma$/ns)')
    plt.xlabel(r'detuning ($\Gamma$)')
    plt.ylabel('time (ns)')
    plt.show()

  }
}


%extend IntegratedEnergyTimeSpectrum {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_scaling", "_background", "_energy_resolution", "_time_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("IntegratedEnergyTimeSpectrum does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr in ("scaling", "background", "energy_resolution", "time_resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "energy_resolution":
      self._energy_resolution = val

    if attr == "time_resolution":
      self._time_resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)


  def Plot(self):
    r"""
    Plot the :class:`IntegratedEnergyTimeSpectrum`.

    .. versionadded:: 1.0.3

    """
    plt.plot(self.drive_sample.drive_detuning, self.result)
    plt.xlabel(r'detuning ($\Gamma$)')
    plt.ylabel(r'$\Delta t$-integrated intensity ($\Gamma$)')
    plt.show()

  }
}
