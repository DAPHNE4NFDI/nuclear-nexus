// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "fit_measurement.h"

#include <vector>
#include <algorithm>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../nexus_definitions.h"
#include "../math/interpolate.h"



FitMeasurement::FitMeasurement(
  Experiment* const experiment_,
  const std::vector<double>& detuning_,
  const std::vector<double>& intensity_data_,
  const unsigned int distribution_points_,
  Var* const scaling_,
  Var* const background_,
  Var* const offset_,
  const size_t data_size_,
  const double fit_weight_,
  Var* const resolution_,
  Var* const resolution_2_,
  const std::string kernel_type_,
  Residual* residual_,
  const bool coherence_,
  const std::string id_
) :
  Measurement(experiment_, detuning_, id_),
  intensity_data(intensity_data_),
  scaling(scaling_),
  background(background_),
  offset(offset_),
  data_size(data_size_),
  fit_weight(fit_weight_),
  distribution_points(distribution_points_),
  resolution(resolution_),
  resolution_2(resolution_2_),
  kernel_type(kernel_type_),
  residual(residual_),
  coherence(coherence_)
{
  input_intensity_data = intensity_data;
}


void FitMeasurement::SetFitEqualities() const
{
  for (Var* const var : fit_equalities)
    var->value = var->equality->Function();
}


void FitMeasurement::SetCeresFitParameterPointers(double const* const* parameters) const
{
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = *parameters[i];

  SetFitEqualities();
}


void FitMeasurement::SetNLoptFitParameterPointers()
{
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = nlopt_fit_parameters_ptr[i];

  SetFitEqualities();
}


void FitMeasurement::SetPagmoFitParameterPointers(const std::vector<double> inputs)
{
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = inputs[i];

  SetFitEqualities();
}


void FitMeasurement::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(scaling);

  fit_handler->PrepareFitVariable(background);

  if (offset != nullptr)
    fit_handler->PrepareFitVariable(offset);

  fit_handler->PrepareFitVariable(resolution);

  if (resolution_2 != nullptr)
    fit_handler->PrepareFitVariable(resolution_2);

  experiment->PopulateFitHandler(fit_handler);
}


double FitMeasurement::Cost() const
{
  double cost = 0.0;

  for (const double& res : fit_residuals)
    cost += pow(res, residual->exponent);
  
  return cost / fit_residuals.size();
}


double FitMeasurement::MeanSquaredError() const
{
  double mse = 0.0;

  for (const double& res : bare_residuals)
    mse += pow(res, 2);

  return mse / bare_residuals.size();
}


std::vector<double> FitMeasurement::GetBareResiduals(
  const std::vector<double>& intensity_data,
  const std::vector<double>& intensity_theory) const
{
  std::vector<double> residuals(intensity_data.size(), 0.0);

  std::transform(intensity_data.begin(), intensity_data.end(), intensity_theory.begin(), residuals.begin(), std::minus<double>());

  return residuals;
}


std::vector<double> FitMeasurement::GetBareResiduals(
  const std::vector<std::vector<double>>& intensity_data,
  const std::vector<std::vector<double>>& intensity_theory) const
{
  const size_t outer_size = intensity_data.size();

  const size_t inner_size = intensity_data.front().size();

  std::vector<double> all_residuals(inner_size * outer_size, 0.0);

  std::vector<double> inner_residual_vector(inner_size, 0.0);

  size_t k = 0;

  for (size_t i = 0; i < outer_size; ++i)
  {
    std::transform(intensity_data[i].begin(), intensity_data[i].end(), intensity_theory[i].begin(), inner_residual_vector.begin(), std::minus<double>());

    for (size_t j = 0; j < inner_size; ++j)
    {
      all_residuals[k] = inner_residual_vector[j];

      ++k;
    }
  }

  return all_residuals;
}


/*
Each FitMeasurement set its own residuals in the ceres solver or its squared residual in the NLopt and Pagmo method.

The residual is the difference between a measured value of the dependent variable y and the value predicted by the model f

r_i = y_i - f_i

The sum of all squared residuals is to be minimized

S = SUM_I r_i^2

The cost function is S/2.

A weighted fit is given by

S = SUM_i w_i * r_i^2

Normally the weight_i should be inverse to the variance of the residual_i, so 

w_i = 1/(sigma_i^2)

also see https://en.wikipedia.org/wiki/Weighted_least_squares

However, we define a weighted residual that approximates a Poisson likelihood function via

r_i = sqrt(y_i) - sqrt(f_i)

and 

S = SUM r_i^2  = SUM (sqrt(y_i) - sqrt(f_i))^2

see P. Thibault and M. Guizar-Sicairos, "Maximum-likelihood refinement for coherent diffractive imaging", New J. Phys. 14, 063004 (2012)

The user can specify an additional fit_weight (fw) of a FitMeasurement, which has to be weighted accordingly.
This is just a weighting value given by the user and is not correlated to counting statistics.

In the ceres sovler the residuals are given so the sqrt(fw_i) is used to weight the residual properly.

in the Nlopt and Pagmo solver the squared residuals are weighted by fw_i.
*/
void FitMeasurement::SetCeresWeightedResiduals(double* residuals, const std::vector<double>& intensity_data, const std::vector<double>& intensity_theory) const
{
  bare_residuals = GetBareResiduals(intensity_data, intensity_theory);

  fit_residuals.resize(intensity_data.size());

  fit_residuals = residual->ResidualFunction(intensity_data, intensity_theory);

  for (size_t i = 0; i < intensity_data.size(); ++i)
    residuals[i] = sqrt(fit_weight) * fit_residuals[i];

  if (inequality != nullptr)
    if (inequality->Function() == false)
      residuals[0] = 2.0e299;
}


void FitMeasurement::SetCeresWeightedResiduals(double* residuals, const std::vector<std::vector<double>>& intensity_data,
  const std::vector<std::vector<double>>& intensity_theory) const 
{
  bare_residuals = GetBareResiduals(intensity_data, intensity_theory);

  const size_t outer_size = intensity_data.size(); // energy size

  const size_t inner_size = intensity_data.front().size();  // time size

  fit_residuals.resize(intensity_data.size() * intensity_data.front().size());

  std::vector<double> residual_vector{};

  size_t k = 0;

  for (size_t i = 0; i < outer_size; ++i)
  {
    residual_vector = residual->ResidualFunction(intensity_data[i], intensity_theory[i]);
  
    for (size_t j = 0; j < inner_size; ++j)
    {
      residuals[k] = sqrt(fit_weight) * residual_vector[j];
      
      fit_residuals[k] = residuals[k];

      ++k;
    }
  }

  if (inequality != nullptr)
    if (inequality->Function() == false)
      residuals[0] = 2.0e299;
}


double FitMeasurement::WeightedUserResidual(const std::vector<double>& intensity_data, const std::vector<double>& intensity_theory) const
{
  bare_residuals = GetBareResiduals(intensity_data, intensity_theory);

  fit_residuals.resize(intensity_data.size());

  fit_residuals = residual->ResidualFunction(intensity_data, intensity_theory);

  double user_residual = 0.0;

  for (size_t i = 0; i < intensity_data.size(); ++i)
    user_residual += pow(fit_residuals[i], residual->exponent);

  if (inequality != nullptr)
    if (inequality->Function() == false)
      user_residual = 2.0e299;

  return fit_weight * user_residual;
}


double FitMeasurement::WeightedUserResidual(const std::vector<std::vector<double>>& intensity_data,
  const std::vector<std::vector<double>>& intensity_theory) const
{
  bare_residuals = GetBareResiduals(intensity_data, intensity_theory);

  const size_t outer_size = intensity_data.size();

  const size_t inner_size = intensity_data.front().size();

  fit_residuals.resize(intensity_data.size() * intensity_data.front().size());

  std::vector<double> residual_vector{};

  double user_residual = 0.0;

  size_t k = 0;

  for (size_t i = 0; i < outer_size; ++i)
  {
    residual_vector = residual->ResidualFunction(intensity_data[i], intensity_theory[i]);

    for (size_t j = 0; j < inner_size; ++j)
    {
      user_residual += pow(residual_vector[j], residual->exponent);

      fit_residuals[k] = residual_vector[j];

      ++k;
    }
  }

  if (inequality != nullptr)
    if (inequality->Function() == false)
       user_residual = 2.0e299;

  return fit_weight * user_residual;
}


std::vector<double> FitMeasurement::Interpolate(
  const std::vector<double>& data_axis, 
  const std::vector<double>& intensity_data,
  const std::vector<double>& theory_axis, 
  const std::vector<double>& intensity_theory) const
{
  std::vector<double> intensity_interpolated(intensity_data.size());

  std::transform(NEXUS_EXECUTION_POLICY data_axis.begin(), data_axis.end(), intensity_interpolated.begin(),
    [&](const double data_axis_elem)
    {
      return interpolate::InterpolateLinear<double>(theory_axis, intensity_theory, data_axis_elem, false);
    }
  );

  return intensity_interpolated;
}


std::vector<std::vector<double>> FitMeasurement::InterpolateTimeaxis(
  const std::vector<double>& time_axis,
  const std::vector<std::vector<double>>& intensity_data,
  const std::vector<double>& theory_axis,
  const std::vector<std::vector<double>>& intensity_theory) const
{
  const size_t outer_size = intensity_data.size();

  const size_t inner_size = intensity_data.front().size();

  std::vector<std::vector<double>> intensity_interpolated(outer_size, std::vector<double>(inner_size, 0.0));

  for (size_t i = 0; i < outer_size; ++i)
  {
    std::transform(NEXUS_EXECUTION_POLICY time_axis.begin(), time_axis.end(), intensity_interpolated[i].begin(), [&](const double t_data)
      {
        return interpolate::InterpolateLinear<double>(theory_axis, intensity_theory[i], t_data, false);
      }
    );
  }

  return intensity_interpolated;
}


std::vector<double> FitMeasurement::BunchSpacingCorrection(const std::vector<double> input_intensity,
  const double bunch_spacing, const double time_step) const
{
  // create a time axis to find correct indices for cutting
  std::vector<double> time_axis(input_intensity.size(), 0.0);

  for (size_t i = 0; i < input_intensity.size(); ++i)
    time_axis[i] = i * time_step;

  const auto cut_point_itr = std::lower_bound(time_axis.begin(), time_axis.end(), bunch_spacing);

  const auto num_points = std::distance(time_axis.begin(), cut_point_itr);

  const size_t num_vectors = input_intensity.size() / num_points;  // int division is round to zero

  std::vector<double> intensity = input_intensity;

  // last cut is not used due to FFT modifications at late times. So adding makes sense only if at least three elements are present.
  if (num_vectors > 2)
  {
    size_t begin = 0;

    size_t end = 0;

    intensity.assign(static_cast<size_t>(num_points), 0.0);

    // discard last time slice due to FFT effects
    for (size_t i = 0; i < num_vectors - 1; ++i)
    {
      end += num_points;

      std::transform(intensity.begin(), intensity.end(), input_intensity.begin() + static_cast<long long>(begin),
        intensity.begin(), std::plus<double>());

      begin = end;
    }
  }
 
  return intensity;
}
