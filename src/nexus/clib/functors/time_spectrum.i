// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module time_spectrum

%{
    #define SWIG_FILE_WITH_INIT
    #include "time_spectrum.h"
%}


%feature("shadow") AmplitudeTime::AmplitudeTime %{
def __init__(self,
             experiment,
             time_length = 200,
             time_step = 0.2,
             electronic = False,
             fft_window = "auto",
             id = ""):
    self._experiment = experiment

    _cnexus.AmplitudeTime_swiginit(self, _cnexus.new_AmplitudeTime(experiment, time_length, time_step, electronic, fft_window, id))
%}


%feature("shadow") AmplitudeTime::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Time array of float values.
        ndarray: Amplitude array of Jones vectors.
    """
    output = np.squeeze(_cnexus.AmplitudeTime___call__(self))
    return np.array(self.time), output
%}


%feature("shadow") TimeSpectrum::TimeSpectrum %{
def __init__(self,
             experiment,
             time_length = 200.0,
             time_step = 0.2,
             max_detuning = 400.0,
             electronic = False,
             time_data = [],
             intensity_data = [],
             scaling = "auto",
             background = 0.0,
             offset = 0.0,
             resolution = internal_time_resolution,
             distribution_points = 1,
             fit_weight = 1.0,
             bunch_spacing = np.inf,
             residual = Sqrt(),
             fft_window = "auto",
             coherence = False,
             id = ""):

    time_data = np.array(time_data).astype(float)

    intensity_data = np.array(intensity_data).astype(float)

    self._experiment = experiment

    if max_detuning == 0.0:
       max_detuning = int(max_detuning)

    if scaling == "auto" and len(intensity_data) > 0:
      scaling = np.amax(intensity_data) - np.amin(intensity_data)
      scaling = Var(100.0*scaling, 1.0e-16, 1.0e6*scaling, True, "TS scaling")
    elif scaling == "auto" and len(intensity_data) == 0:
      scaling = Var(1.0, 1.0e-16, np.inf, True, "TS scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 1.0e-16, np.inf, True, "TS scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and len(intensity_data) > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100.0*background, True, "TS backgr")
    elif background == "auto" and len(intensity_data) == 0:
      background = Var(0.0, 0.0, np.inf, False, "TS backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "TS backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(resolution, (int, float)):
      resolution = Var(resolution, 0.0, 10.0, False, "TS resolution")

    if isinstance(offset, (int, float)):
      offset = Var(offset, -2.0, 2.0, False, "TS time offset")

    self._scaling = scaling

    self._background = background

    self._offset = offset
    
    self._resolution = resolution

    self._residual = residual

    _cnexus.TimeSpectrum_swiginit(self, _cnexus.new_TimeSpectrum(experiment,
                                                                 time_length,
                                                                 time_step,
                                                                 max_detuning,
                                                                 electronic,
                                                                 time_data,
                                                                 intensity_data,
                                                                 scaling,
                                                                 background,
                                                                 offset,
                                                                 resolution,
                                                                 distribution_points,
                                                                 fit_weight,
                                                                 bunch_spacing,
                                                                 residual,
                                                                 fft_window,
                                                                 coherence,
                                                                 id))
%}


%feature("shadow") TimeSpectrum::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Time array of float values.
        ndarray: Intensity array of float values.
    """
    output = np.array(_cnexus.TimeSpectrum___call__(self))
    return np.array(self.time), output
%}



%include "time_spectrum.h"



%extend AmplitudeTime {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment") and not hasattr(self, attr):
      raise Exception("AmplitudeTime does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    super().__setattr__(attr, val)


  def Plot(self, sigma = True, pi = True, polar = False, unwrap = True, name=None):
      r"""
      Plot the :class:`AmplitudeTime`.

      . versionadded:: 1.0.3

      Args:
          sigma (bool): Select if :math:`\sigma` component should be plot.
          pi (bool): Select if :math:`\pi` component should be plot.
          phase (bool): If ``True`` absolute and phase are plotted instead of real and imaginary parts.
          unwrap (bool): Select if phase should be unwrapped.
          name(string): If given, the plot is saved under this name.
           
            .. versionadded:: 1.1.0
      """
      time, result = self.__call__()
      result_sigma = result[:,0]
      result_pi = result[:,1]

      if polar == False:
          result_sigma_real = np.real(result_sigma)
          result_sigma_imag = np.imag(result_sigma)

          result_pi_real = np.real(result_pi)
          result_pi_imag = np.imag(result_pi)

          real_label = r'real ($\sqrt{\Gamma / \mathrm{ns}}$)'
          imag_label = r'imag ($\sqrt{\Gamma / \mathrm{ns}}$)'

      else:
          result_sigma_real = np.abs(result_sigma)
          if unwrap == True:
              result_sigma_imag = np.unwrap(np.angle(result_sigma))
          else:
              result_sigma_imag = np.angle(result_sigma)

          result_pi_real = np.abs(result_pi)
          if unwrap == True:
              result_pi_imag = np.unwrap(np.angle(result_pi))
          else:
              result_pi_imag = np.angle(result_pi)

          real_label = r'abs ($\sqrt{\Gamma / \mathrm{ns}}$)'
          imag_label = 'phase (rad)'

      if sigma == True and pi == True:
          fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True, figsize=(6.4, 2*4.8))
          fig.suptitle('Amplitude Time: ' + self.id)
          fig.tight_layout(pad=2.0)

          ax1.set_title(r'$\sigma$ polarization')

          ax1.plot(time, result_sigma_real, )
          ax1.set_ylabel(real_label)

          ax2.plot(time, result_sigma_imag, color = 'darkorange')
          ax2.set_ylabel(imag_label)

          ax3.set_title(r'$\pi$ polarization')

          ax3.plot(time, result_pi_real)
          ax3.set_ylabel(real_label)

          ax4.plot(time, result_pi_imag, color = 'darkorange')
          ax4.set_xlabel('time (ns)')
          ax4.set_ylabel(imag_label)
          
      elif sigma == True:
          fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(6.4, 4.8))
          fig.suptitle('Amplitude Time: ' + self.id)

          ax1.set_title(r'$\sigma$ polarization')

          ax1.plot(time, result_sigma_real, )
          ax1.set_ylabel(real_label)

          ax2.plot(time, result_sigma_imag, color = 'darkorange')
          ax2.set_ylabel(imag_label)

          ax2.set_xlabel('time (ns)')

      elif pi == True:
          fig, (ax3, ax4) = plt.subplots(2, sharex=True, figsize=(6.4, 4.8))
          fig.suptitle('Amplitude Time: ' + self.id)

          ax3.set_title(r'$\pi$ polarization')

          ax3.plot(time, result_pi_real)
          ax3.set_ylabel(real_label)

          ax4.plot(time, result_pi_imag, color = 'darkorange')
          ax4.set_xlabel('time (ns)')
          ax4.set_ylabel(imag_label)

      if name != None:
          plt.savefig(name)

      plt.show()

  }
}

%extend TimeSpectrum {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_scaling", "_background", "_offset", "_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("TimeSpectrum does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr in ("scaling", "background", "offset", "resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "offset":
      self._offset = val

    if attr == "resolution":
      self._resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)


  def Plot(self,
           data = True,
           residuals = True,
           datacolor='black', 
           theorycolor='r',
           theorywidth=2,
           datalinestyle='none',
           datamarker='+',
           datamarkersize=2,
           datafillstyle='full',
           legend=True,
           errors= False,
           errorcap=2,
           name=None):
    r"""
    Plot the :class:`TimeSpectrum`.

    .. versionadded:: 1.0.3

    .. versionchanged:: 1.2.0

    Args:
      data (bool): Select if :attr:`intensity_data` should be plot.
      residuals (bool): Select if :attr:`residuals` from fit should be plot.
      datacolor (string): See *matplotlib* documentation.
      theorycolor (string): See *matplotlib* documentation.
      theorywidth (float): See *matplotlib* documentation.
      datalinestyle (string): See *matplotlib* documentation.
      datamarker (string): See *matplotlib* documentation.
      datamarkersize (float): See *matplotlib* documentation.
      datafillstyle (string): See *matplotlib* documentation.
      legend (bool): Select if legend should be plot when model and data are plot.
      errors (bool): Select if error bars should be plot.
      errorcap (float): See *matplotlib* documentation (``capsize``).
      name(string): If given, the plot is saved under this name.
         
        .. versionadded:: 1.2.0
    """

    title_string = r'Time Spectrum: '

    x_axis = self.time
    x_label = r'time (ns)'

    x_axis_data = self.time_data

    if self.scaling.value == 1.0:
      y_label=r'intensity ($\Gamma / \mathrm{ns}$)'
    else:
      y_label=r'counts'

    self.FitMeasruementPlot(x_axis,
                            x_label,
                            y_label,
                            title_string,
                            x_axis_data=x_axis_data,
                            log=True,
                            data=data,
                            residuals=residuals,
                            datacolor=datacolor, 
                            theorycolor=theorycolor,
                            theorywidth=theorywidth,
                            datalinestyle=datalinestyle,
                            datamarker=datamarker,
                            datamarkersize=datamarkersize,
                            datafillstyle=datafillstyle,
                            legend=legend,
                            errors=errors,
                            errorcap=errorcap,
                            name=name)

  }
}
