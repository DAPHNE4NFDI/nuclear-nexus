// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Measurement class.
// Base class for all functors and the FitMeasurement class.
// Measurements and FitMeasurements are passed to the Optimizer and the Fit modules respectively.
// 
// The measurement functors use class down casting from NxObjects.
// The NxObjectId makes this downcasting safe.
//
// for Sphinx:
// Class inheritance does not work in Sphinx due to easy Python labeling here.
// Put the needed attributes from the base class Measurement to the derived class Sphinx documentation manually for clearer API interface.


#ifndef NEXUS_MEASUREMENT_H_
#define NEXUS_MEASUREMENT_H_

#include <iostream>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/distribution.h"
#include "../classes/experiment.h"
#include "../classes/basic_sample.h"
#include "../classes/forward_sample.h"
#include "../classes/sample.h"


/**
Base class for all measurement class objects. Do not initialize this method, use the derived classes.

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   detuning (list): Detuning values for the calculation.
   velocity (list): Velocity detuning for the calculation in units mm/s.
   time (list): Time values for the calculation. Only used for time dependent measurements.
   distribution_points (int): Number of distribution_points for objects in forward geometry
*/
struct Measurement {
  Measurement(
    Experiment* const experiment,
    const std::string id = ""
  ) : 
    experiment(experiment),
    id(id)
  {};

  Measurement(
    Experiment* const experiment,
    const std::vector<double> detuning,
    const std::string id = ""
  ) : 
    experiment(experiment), 
    detuning(detuning), 
    id(id)
  {};

  void Update() const;

  void PopulateOptimizerHandler(OptimizerHandler* optimizer_handler);

  /**
  Returns the velocity detuning of the measurement in units of mm/s.

  Returns:
     ndarray: Velocity detuning (mm/s). 
  */
  std::vector<double> Velocity() const;

  Experiment* experiment = nullptr;

  mutable std::vector<double> detuning{};

  std::string id = "";

  mutable std::vector<double> time{};

protected:

  // consistency check functions

  void CheckIsotope(const std::string module_name) const;

  void CheckForSampleInExperiment(const std::string module_name, const BasicSample* const sample) const;

  void CheckGrazingAngleSample(const std::string module_name, const GrazingSample* const sample) const;

  void CheckGrazingAngleExperiment(const std::string module_name) const;

  void CheckWmatrixValiditySample(const std::string module_name, const GrazingSample* const grazing_sample, const double energy, double angle = 0.0) const;

  void CheckWmatrixValidityExperiment(const std::string module_name, const double energy) const;

  // larger output of module
  void Print(std::string module_name, std::vector<std::string> string_vector) const;

  // general functions for calculations
  std::vector<double> PopulateDepth(const size_t num_points, const double thickness) const;

  std::vector<double> PopulateTime(const double timelength, const double timestep, const double offset = 0.0) const;

  size_t NumberDetuningPoints(const double timelength, const double timestep, const double isotope_lifetime) const;

  std::vector<double> PopulateDetuning(const size_t num_detuning_points, const double time_step,
    const double max_detuning, const double isotope_lifetime) const;

  double BeamProfileCorrection(const double angle, const double sample_length, double beam_width, const std::string profile) const;

  ExperimentMatrix2 AddElectronic(const ExperimentMatrix2 matrix) const;

  ExperimentMatrix2 SubtractElectronic(const ExperimentMatrix2 matrix) const;

  ExperimentMatrix2 TimeGating(const ExperimentMatrix2 matrix, const std::vector<double> time_gate) const;
};

#endif // NEXUS_MEASUREMENT_H_
