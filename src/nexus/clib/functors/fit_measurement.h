// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the abstract class FitMeasurement.
// Derived from Measurement class.
// It is used for the functor interface to class Fit.
// The virtual bool operator(double const* const* parameters, double* residuals) is used by the ceres solver for fitting.
// The virtual double Residuals() is used to return the squared sum of all residuals for the Pagmo and Nlopt solvers.
// Only this one has to be defined in the abstract class.
// 
// operator() (double const* const* parameters, double* residuals) and Residuals() both call the 
// operator()(const bool update_and_calc_transitions, const bool check_W_matrix)
// the return of this operator is meeasurement method specific and therefore not defined as a virtual function here.
// 
// for Sphinx:
// Class inheritance does not work in Sphinx due to easy Python labeling here.
// Put the needed attributes from the base class Measurement to the derived class Sphinx documentation manually for clearer API interface.


#ifndef NEXUS_FIT_MEASUREMENT_H_
#define NEXUS_FIT_MEASUREMENT_H_

#include <iostream>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/residual.h"
#include "../classes/optimizer_handler.h"
#include "../classes/inequality.h"
#include "../functors/measurement.h"


/**
Abstract class for all fittable measurement class objects. Do not initialize this method, use the derived classes.

Args:
   id (string): String identifier.
   experiment(:class:`Experiment`): Experiment.
   detuning (list): Detuning values for nuclear calculation.
   time (list): Time values for nuclear calculation which depend on time.
   distribution_points (int): Number of distribution_points for objects in forward geometry.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   offset (:class:`Var`): Offset in the x axis.

     .. versionadded:: 1.1.0

   fit_weight (float): Relative weight for the cost function in multi measurement fitting. Default is 1.
   resolution (:class:`Var`): FWHM of the convolution kernel.
   resolution_2 (:class:`Var`): FWHM of the convolution kernel along the second axis in 2D data sets.
   kernel_type (string): The kernel used for the convolution, either ``Gauss`` or ``Lorentz``.

Attributes:
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   offset (:class:`Var`): Offset in the x axis.

     .. versionadded:: 1.1.0

   fit_weight (float): Relative weight for the cost function in multi measurement fitting. Default is 1.
   resolution (:class:`Var`): FWHM of the convolution kernel.
   resolution_2 (:class:`Var`): FWHM of the convolution kernel along the second axis in 2D data sets.
   kernel_type (string): The kernel used for the convolution, either "Gauss" or "Lorentz".
   squared_residuals (float): Sum of the squared residuals. Calculated during fitting.

     .. versionremoved:: 2.0.0

   sum_user_residuals (float): Sum of the residuals provided by the user (with exponent used). Calculated during fitting.

     .. versionadded:: 2.0.0

   fit_residuals (array): The residuals from the :class:`Residual` function used during fitting fit. Calculated during fitting.

     .. versionadded:: 1.0.3

   bare_residuals (array): The `difference residuals (:math:`x_i - y_i`) from the fit. Calculated during fitting.

     .. versionadded:: 1.1.0

*/
struct FitMeasurement : public Measurement {
  FitMeasurement(
    Experiment* const experiment,
    const std::vector<double>& detuning,
    const std::vector<double>& intensity_data,
    const unsigned int distribution_points,
    Var* const scaling,
    Var* const background,
    Var* const offset,
    const size_t data_size,
    const double fit_weight,
    Var* const resolution,
    Var* const resolution_2,
    const std::string kernel_type = "Gauss",
    Residual* residual = nullptr,
    const bool coherence = false,
    const std::string id = ""
  );

  virtual ~FitMeasurement() {};

  // calculates the model via operator()(const bool update_and_calc_transitions, const bool check_W_matrix)
  // and sets all residuals in the ceres solver
  virtual bool operator() (double const* const* parameters, double* residuals) const = 0;  

  // calculates the model via the operator(const bool update_and_calc_transitions, const bool check_W_matrix)
  // and returns the weighted squared sum of the measurements residual
  // used in Pagmo and Nlopt modules
  virtual double Residuals() const = 0;

  void SetFitEqualities() const;

  void SetCeresFitParameterPointers(double const* const* parameters) const;

  void SetNLoptFitParameterPointers();

  void SetPagmoFitParameterPointers(const std::vector<double> inputs);

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  /**
  Get the cost function of the measurement

  .. math:: cost = \frac{1}{n} \sum_i r(y_i, \hat{y}_i)

  .. versionadded:: 2.0.0

  Returns:
     float: cost function value.
  */
  double Cost() const;

  /**
  Get the mean squared error (MSE) of the measurement

  .. math:: cost = \frac{1}{n} \sum_i (y_i - \hat{y}_i)^2

  .. versionadded:: 2.0.0

  Returns:
     float: MSE value.
  */
  double MeanSquaredError() const;

  // store input_data for bootstrap method
  std::vector<double> input_intensity_data{};

  std::vector<double> intensity_data{};

  mutable Var* scaling = nullptr;

  mutable Var* background = nullptr;

  mutable Var* offset = nullptr;

  size_t data_size = 0;

  double fit_weight = 1.0;

  unsigned int distribution_points = 1; // for thickness and divergence distributions

  mutable Var* resolution = nullptr; // FWHM in units of gamma

  mutable Var* resolution_2 = nullptr; // FWHM in units of ns

  std::string kernel_type = "Gauss";

  std::vector<double*> fit_parameter_pointers{};

  double* nlopt_fit_parameters_ptr = nullptr;

  mutable std::vector<Var*> fit_equalities{};

  mutable Inequality* inequality = nullptr;

  mutable double sum_user_residuals = std::numeric_limits<double>::quiet_NaN();

  Residual* residual = nullptr;

  bool coherence = false;

  mutable std::vector<double> result{};

  // this is the bare residual r = x_i - y_i
  mutable std::vector<double> bare_residuals{};

  // this is the residual implementation from the user in pyhton
  // without the exponent as such neede for ceres solver
  mutable std::vector<double> fit_residuals{};

  // store vectors for the bootstrap method
  mutable std::vector<double> bootstrap_fit_result{};

  mutable std::vector<double> bootstrap_residuals{};

protected:

  std::vector<double> GetBareResiduals(
    const std::vector<double>& intensity_data,
    const std::vector<double>& intensity_theory) const;

  std::vector<double> GetBareResiduals(
    const std::vector<std::vector<double>>& intensity_data,
    const std::vector<std::vector<double>>& intensity_theory) const;
    
  void SetCeresWeightedResiduals(
    double* residuals,
    const std::vector<double>& intensity_data,
    const std::vector<double>& intensity_theory) const;

  void SetCeresWeightedResiduals(
    double* residuals,
    const std::vector<std::vector<double>>& intensity_data,
    const std::vector<std::vector<double>>& intensity_theory) const;

  double WeightedUserResidual(
    const std::vector<double>& intensity_data,
    const std::vector<double>& intensity_theory) const;

  double WeightedUserResidual(
    const std::vector<std::vector<double>>& intensity_data,
    const std::vector<std::vector<double>>& intensity_theory) const;

  std::vector<double> Interpolate(
    const std::vector<double>& data_axis,
    const std::vector<double>& intensity_data,
    const std::vector<double>& theory_axis,
    const std::vector<double>& intensity_theory) const;

  std::vector<std::vector<double>> InterpolateTimeaxis(
    const std::vector<double>& data_axis,
    const std::vector<std::vector<double>>& intensity_data,
    const std::vector<double>& theory_axis,
    const std::vector<std::vector<double>>& intensity_theory) const;

  std::vector<double> BunchSpacingCorrection(
    const std::vector<double> input_intensity,
    const double bunch_spacing,
    const double time_step) const;
};

#endif // NEXUS_MEASUREMENT_H_
