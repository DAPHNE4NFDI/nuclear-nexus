// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the time spectrum (amplitude and intensity) functor.


#ifndef NEXUS_TIME_SPECTRUM_H_
#define NEXUS_TIME_SPECTRUM_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../math/intensity.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"
#include "../functors/fit_measurement.h"
#include "../math/fourier.h"


/**
Constructor for the :class:`AmplitudeTime` class.
Class to calculate the time-dependent scattering amplitudes of the experiment.
Only possible for a properly set Jones Vector of the :class:`Beam`.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   time_length (float): Length of the calculation (nanoseconds).
   time_step (float): Step size of the calculation in (nanoseconds).
   electronic (bool): If *True* electronic scattering is also included.
   fft_window (string): A window function that is applied during the Fourier transformation from energy to time.
     Options are

     * ``auto`` (default) - no window, equal to ``none``.
     * ``Hann``
     * ``Hamming``
     * ``Sine``
     * ``Welch``
     * ``Kaiser2`` - :math:`\alpha = 2`
     * ``Kaiser25`` - :math:`\alpha = 2.5`
     * ``Kaiser3`` - :math:`\alpha = 3`
     * ``none``

     .. versionadded:: 1.0.4

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   time_length (float): Length of the calculation (nanoseconds).
   time_step (float): Step size of the calculation (nanoseconds).
   electronic (bool): If *True* electronic scattering is also included.
   fft_window (string): A window function that is applied during the Fourier transformation from energy to time.
     Options are

     * ``auto`` (default) - no window, equal to ``none``.
     * ``Hann``
     * ``Hamming``
     * ``Sine``
     * ``Welch``
     * ``Kaiser2`` - :math:`\alpha = 2`
     * ``Kaiser25`` - :math:`\alpha = 2.5`
     * ``Kaiser3`` - :math:`\alpha = 3`
     * ``none``
     
     .. versionadded:: 1.0.4

   result (list): List of Jones vectors in units of (:math:`\sqrt(\Gamma / ns)`.
*/
struct AmplitudeTime : public Measurement {
  AmplitudeTime(
    Experiment* const experiment,
    const double time_length = 200.0,
    const double time_step = 0.2,
    const bool electronic = false,
    const std::string = "auto",
    const std::string id = ""
  );

  intensity::AmplitudeVector operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  mutable intensity::AmplitudeVector result{};

  double time_length = 0.0;

  double time_step = 0.0;

  bool electronic = false;

  std::string fft_window = "none";

private:

  // calculates the scattering matrices in the energy representation
  // performs the FFT
  // calculates the amplitude of the time spectrum  
  intensity::AmplitudeVector AmplitudeTimeDetuning(const ExperimentMatrix2& matrix) const;
};



/**
Constructor for the :class:`TimeSpectrum` class.
Class to calculate the time-dependent intensity of the experiment.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment
   time_length (float): Length of the calculation (nanoseconds).
   time_step (float): Step size of the calculation (nanoseconds).
   max_detuning (float): Maximum value of the energy detuning (Gamma).
     Set this value to speed up the calculation.
     If set to zero no maximum is applied.
     Also see :attr:`fft_window`.
   electronic (bool): If *True* electronic scattering is included.
   time_data (list or ndarray): Time data for fitting (nanoseconds).
   intensity_data (list or ndarray): Intensity data for fitting.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``0``.
   offset (float or :class:`Var`): Temporal offset of the model. Default is ``0``.

     .. versionadded:: 1.1.0

   resolution (float or :class:`Var`): Resolution value for convolution (nanoseconds). Corresponds to the APD resolution.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions.
      Default is 1.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   bunch_spacing (float): Spacing of subsequent X-ray bunches in time (nanoseconds).
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   fft_window (string): A window function that is applied during the Fourier transformation from energy to time.
     This should be used when the :attr:`max_detuning` is not zero.
     Options are 
     
     * ``auto`` (default) - will pick a Hann window if :attr:`max_detuning` is not zero, no window otherwise.
     * ``Hann``
     * ``Hamming``
     * ``Sine``
     * ``Welch``
     * ``Kaiser2`` - :math:`\alpha = 2`
     * ``Kaiser25`` - :math:`\alpha = 2.5`
     * ``Kaiser3`` - :math:`\alpha = 3`
     * ``none``
   
   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment
   time_length (float): Length of the calculation (nanoseconds).
   time_step (float): Step size of the calculation (nanoseconds).
   max_detuning (float): Maximum value of the energy detuning (Gamma).
     Set this value to speed up the calculation.
     If set to zero no maximum is applied.
     Also see :attr:`fft_window`.
   electronic (bool): If *True* electronic scattering is included.
   time_data (list): Time data for fitting (nanoseconds).
   intensity_data (list): Intensity data for fitting.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   offset (float or :class:`Var`): Temporal offset of the model.

     .. versionadded:: 1.1.0

   resolution (:class:`Var`): Resolution value for convolution (nanoseconds). Corresponds to the APD resolution.
   resolution_kernel (list): weight of the Gaussian resolution same step size as detuning.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   bunch_spacing (float): Spacing of subsequent X-ray bunches in time (nanoseconds).
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   fft_window (string): A window function that is applied during the Fourier transformation from energy to time.
     This should be used when the :attr:`max_detuning` is not zero.
     Options are
     
     * ``auto`` (default) - will pick a Hann window if :attr:`max_detuning`  is not zero, no window otherwise.
     * ``Hann``
     * ``Hamming``
     * ``Sine``
     * ``Welch``
     * ``Kaiser2`` - :math:`\alpha = 2`
     * ``Kaiser25`` - :math:`\alpha = 2.5`
     * ``Kaiser3`` - :math:`\alpha = 3`
     * ``none``
     
   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

   result (list): List of intensity values. It is the energy-integrated intensity per time in units of (:math:`\Gamma /ns`).
*/
struct TimeSpectrum : public FitMeasurement {
  TimeSpectrum(
    Experiment* const experiment,
    const double time_length,
    const double time_step,
    const double max_detuning,
    const bool electronic,
    const std::vector<double>& time_data,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const offset,
    Var* const resolution,
    const unsigned int distribution_points,
    const double fit_weight,
    const double bunch_spacing,
    Residual* const residual,
    const std::string fft_window,
    const bool coherence,
    const std::string id
  );

  std::vector<double> operator()(const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;

  double time_length = 0.0;

  double time_step = 0.0;

  double max_detuning = 0.0;

  bool electronic = false;

  std::vector<double> time_data{};

  double bunch_spacing = INFINITY;

  mutable std::vector<double> resolution_kernel{};

  std::string fft_window = "auto";

private:

  LayerMatrix2 TimeSpectrumMatrix(const size_t num_detuning, const bool electronic_det, const bool calc_transitions) const;

  // performs the FFT
  // calculates the intensity of the time spectrum  
  std::vector<double> TimeSpectrumDetuning(const LayerMatrix2& experiment_matrix) const;

  intensity::AmplitudeVector AmplitudeTimeDetuning(const LayerMatrix2& matrix) const;
};


#endif // NEXUS_TIME_SPECTRUM_H_
