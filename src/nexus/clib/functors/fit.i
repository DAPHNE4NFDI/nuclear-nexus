// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module fit

%{
    #define SWIG_FILE_WITH_INIT
    #include "fit.h"
%}


%feature("shadow") Fit::Fit %{
def __init__(self, measurements, id = "", external_fit_variables = [], inequalities = None):

    if isinstance(measurements, (FitMeasurement)):
      measurements = [measurements]

    # the python class object must reference to the python list with objects pointers
    # in order to maintain securely in the python domain 
    self._measurements = measurements

    self._external_fit_variables = external_fit_variables

    class InequalityFit(Inequality):
        def __init__(self, id, inequalities_list):
            super().__init__(id)
            self.inequalities_list = inequalities_list

        def Function(self):
            ret = True
       
            for ineq in self.inequalities_list:
                ret = ineq() * ret
       
            return bool(ret)

        def NumFunction(self):
            return len(self.inequalities_list)

    self._inequalities_list = inequalities

    self._inequalities = None
      
    if isinstance(inequalities, (list, tuple)):
        for ineq in inequalities:
            if not callable(ineq):
                raise Exception("inequalities must be a list of functions. Use equality=[func1, func2, ...]")

        self._inequalities = InequalityFit(id+" - inequalities", inequalities)

    _cnexus.Fit_swiginit(self, _cnexus.new_Fit(measurements, id, external_fit_variables, self._inequalities))
%}


%feature("shadow") Fit::operator() %{
def __call__(self):
    r"""
    Evaluates the fit.
    Also callable via operator ``Evaluate()``.
    """
    return _cnexus.Fit___call__(self)
%}



%include "fit.h"



%extend Fit {
  %pythoncode{

  Evaluate = __call__
  r"""
  Evaluates the fit.
  Also callable via operator ``()``.
  """

  def __setattr__(self, attr, val):
    if attr not in ("this", "_measurements", "_external_fit_variables", "_inequalities", "_inequalities_list") and not hasattr(self, attr):
      raise Exception("Fit does not have attribute {}".format(attr))

    if attr == "measurements":
      if isinstance(val, (FitMeasurement)):
        val = [val]
        self._measurements = val  # ref to python list
        val = FitMeasurementVector(val)
      elif isinstance(val, (list, tuple)):
        self._measurements = val  # ref to python list
        val = FitMeasurementVector(val)

    if attr == "external_fit_variables":
      if isinstance(val, (Var)):
        val = [val]
        self._external_fit_variables = val  # ref to python list
        val = FitVariables(val)
      elif isinstance(val, (list, tuple)):
        self._external_fit_variables = val  # ref to python list
        val = FitVariables(val)

    super().__setattr__(attr, val)

  def __repr__(self):
    output = "Fit .id: {}\n".format(self.id)
    output += "  .measurements:\n"

    i = 0
    for meas in self.measurements:
      output += "    index: {}, .id: {}, .experiment.id: {}\n".format(i, meas.id, meas.experiment.id)
      i += 1

    output += "  .external_fit_variables:\n"

    i = 0
    for var in self.external_fit_variables:
      output += "    index: {}, .id: {}\n".format(i, var.id)
      i += 1
    
    return output

  }
}
