// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the total (electronic and nuclear) reflectivity (intensity only) functors.
// Intensity integration over energy or time.


#ifndef NEXUS_NUCLEAR_REFLECTIVITY_H_
#define NEXUS_NUCLEAR_REFLECTIVITY_H_

#include <iostream>
#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"
#include "../functors/fit_measurement.h"

/**
Constructor for the :class:`NuclearReflectivityEnergy` class.
Class to calculate the angular-dependent and energy-integrated total scattering intensity of the experiment while for one sample the gracing-incidence angle is changed.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample whose angle is changed.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

   angles (list): List of the angles.
   detuning (list or ndarray): Detuning values for the calculation.
   intensity_data (list or ndarray): Intensity data for fitting.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``0``.
   offset (float or :class:`Var`): Angular offset of the model. Default is ``0``.

     .. versionadded:: 1.1.0

   resolution (float or :class:`Var`): Resolution value for convolution.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.

     .. versionadded:: 1.0.1
   
   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

   print_progrees (bool):  Determines if the progress bar is printed.
     Default is ``False``.

     .. versionadded:: 1.2.0

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample whose angle is changed.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

   angles (list): List of the angles.
   detuning (list or ndarray): Detuning values for the calculation.
   result (list): List of angle-dependent energy-integrated intensity values in units of (:math:`\Gamma`).
   intensity_data (list): Intensity data for fitting.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   offset (float or :class:`Var`): Angular offset of the model. Default is ``0``.

     .. versionadded:: 1.1.0

   resolution (:class:`Var`): Resolution value for convolution.
   resolution_kernel (list): Weight of the Gaussian resolution same step size as detuning.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.

     .. versionadded:: 1.0.1

   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

   print_progrees (bool):  Determines if the progress bar is printed.
     Default is ``False``.

     .. versionadded:: 1.2.0

*/
struct NuclearReflectivityEnergy : public FitMeasurement {
  NuclearReflectivityEnergy(
    Experiment* const experiment,
    GrazingSample* const sample,
    const std::vector<double>& angles,
    const std::vector<double>& detuning,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const offset,
    Var* const resolution,
    const double fit_weight,
    Residual* const residual,
    const std::vector<double> time_gate,
    const bool coherence,
    const bool print_progress,
    const std::string id
  );

  std::vector<double> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;

  GrazingSample* sample = nullptr;

  std::vector<double> angles{};

  mutable std::vector<double> resolution_kernel{};

  std::vector<double> time_gate{};

  bool print_progress = false;
};


/**
Constructor for the :class:`NuclearReflectivityEnergy` class.
Class to calculate the angular dependent and time-integrated resonant scattering intensity of the experiment while for one sample the gracing-incidence angle is changed.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample of the :class:`Experiment` for the calculation.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

   angles (list): List of the angles.
   time_start (float): Start time for the integration (ns).
   time_stop (float): Stop time for the integration (ns).
   time_step (float): Step size for the time axis (ns).
   max_detuning (float): Maximum value of the energy detuning in units of the resonant linewidth.
     Set this value to speed up the calculation.
     If zero no maximum is applied.
     The FFT window is always ``auto``.
   intensity_data (list or ndarray): Intensity data for fitting.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``0``.
   offset (float or :class:`Var`): Angular offset of the model. Defualt is ``0``.

     .. versionadded:: 1.1.0

   resolution (float or :class:`Var`): Resolution value for convolution.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   bunch_spacing (float): Spacing of subsequent X-ray bunches in time (nanoseconds).
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

   print_progrees (bool):  Determines if the progress bar is printed.
     Default is ``False``.

     .. versionadded:: 1.2.0

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   sample (:class:`GrazingSample`): Sample of the :class:`Experiment` for the calculation.

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`GrazingSample`.

   angles (list): List of the angles.
   time_start (float): Start time for the integration (ns).
   time_stop (float): Stop time for the integration (ns).
   time_step (float): Step size for the time axis (ns).
   max_detuning (float): Maximum value of the energy detuning in units of the resonant linewidth.
     Set this value to speed up the calculation.
     If zero no maximum is applied.
     The FFT window is always ``auto``.
   result (list): List of angle-dependent time-integrated intensity values in units of (:math:`\Gamma`).
   intensity_data (list): Intensity data for fitting.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   offset (float or :class:`Var`): Angular offset of the model. Defualt is ``0``.

     .. versionadded:: 1.1.0

   resolution (:class:`Var`): Resolution value for convolution. Default is 0.001 deg.
   resolution_kernel (list): weight of the Gaussian resolution same step size as detuning.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   bunch_spacing (float): Spacing of subsequent X-ray bunches in time (nanoseconds).
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

   print_progrees (bool):  Determines if the progress bar is printed.
     Default is ``False``.

     .. versionadded:: 1.2.0

*/
struct NuclearReflectivity : public FitMeasurement {
  NuclearReflectivity(
    Experiment* const experiment,
    GrazingSample* const sample,
    const std::vector<double>& angles,
    const double time_start,
    const double time_stop,
    const double time_step,
    const double max_detuning,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const offset,
    Var* const resolution,
    const double fit_weight,
    const double bunch_spacing,
    Residual* const residual,
    const bool coherence,
    const bool print_progress,
    const std::string id
  );

  std::vector<double> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;

  GrazingSample* sample = nullptr;

  std::vector<double> angles{};

  double time_start = 0.0;

  double time_stop = 0.0;

  double time_step = 0.0;

  double max_detuning = 0.0;

  double bunch_spacing = INFINITY;

  bool print_progress = false;

  mutable std::vector<double> resolution_kernel{};
};

#endif // NEXUS_NUCLEAR_REFLECTIVITY_H_
