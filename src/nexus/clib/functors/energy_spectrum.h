// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the energy spectrum (amplitude and intensity) functor.


#ifndef NEXUS_ENERGY_SPECTRUM_H_
#define NEXUS_ENERGY_SPECTRUM_H_

#include <iostream>
#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../math/intensity.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"
#include "../functors/fit_measurement.h"


/**
Constructor for the :class:`AmplitudeSpectrum` class.
Class to calculate the energy-dependent scattering amplitudes of the experiment. Only possible for a properly set Jones Vector of the :class:`Beam`.

Args:
   id (string): User identifier.
   experiment (:class:`Experiment`): experiment
   detuning (list or ndarray): detuning values for the calculation (Gamma).
   electronic (bool): If *True* electronic scattering is included.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating during spectrum collection.
     Given in ns.
     Default is an empty list.

     .. versionadded:: 1.0.1

Attributes:
   id (string): User identifier.
   experiment (:class:`Experiment`): experiment
   detuning (list): detuning values for the calculation (Gamma).
   electronic (bool): If *True* electronic scattering is also included.
   result (list): List of detuning dependent complex amplitude values.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.
     
     .. versionadded:: 1.0.1
*/
struct AmplitudeSpectrum : public Measurement {
  AmplitudeSpectrum(
    Experiment* const experiment,
    const std::vector<double>& detuning,
    const bool electronic,
    const std::string id,
    const std::vector<double> time_gate
  );

  intensity::AmplitudeVector operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  mutable intensity::AmplitudeVector result{};

  bool electronic = true;

  std::vector<double> time_gate{};
};


/**
Constructor for the :class:`EnergySpectrum` class.
Class to calculate the energy-dependent intensity of the experiment.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   detuning (list or ndarray): Detuning values for the calculation (Gamma).
   electronic (bool): If *True* electronic scattering is included.
   intensity_data (list or ndarray): Intensity data for fitting.
     Default is an empty list.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting.
     Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting.
     Default is ``0``.
   resolution (float or :class:`Var`): Resolution value for convolution (Gamma).
     Default is 0.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
     Both distributions assume incoherent summation over the weighted contributions.
     Default is 1.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
     Default is 1.
   kernel_type (string): Type of the resolution kernel. Is used for the convolution of the calculated energy spectrum with the resolution kernel.
      Can be *Gauss* or *Lorentz*.
      Default is *Gauss*.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.
     
     .. versionadded:: 1.0.1

   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   detuning (list or ndarray): Detuning values for the calculation (Gamma).
   electronic (bool): If *True* electronic scattering is included.
   result (list): List of detuning dependent intensity values.
   intensity_data (list): Intensity data for fitting.
   data_size (int): Number of data points of experimental intensity data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   resolution (:class:`Var`): Resolution value for convolution.
   resolution_kernel (list): The used kernel, same step size as detuning.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   kernel_type (string): Type of the resolution kernel. Is used for the convolution of the calculated energy spectrum with the resolution kernel.
      Can be *Gauss* or *Lorentz*.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.
     
     .. versionadded:: 1.0.1

   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

*/
struct EnergySpectrum : public FitMeasurement {
  EnergySpectrum(
    Experiment* const experiment,
    const std::vector<double>& detuning,
    const bool electronic,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const resolution,
    const unsigned int distribution_points,
    const double fit_weight,
    const std::string kernel_type,
    Residual* const residual,
    const std::vector<double> time_gate,
    const bool coherence,
    const std::string id
  );

  std::vector<double> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;
  
  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;
  
  bool electronic = true;

  mutable std::vector<double> resolution_kernel{};

  std::vector<double> time_gate{};
};


/**
Constructor for the :class:`EmissionSpectrum` class.
Class to calculate the energy-dependent emission intensity of the experiment.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   detuning (list or ndarray): Detuning values for the calculation (Gamma).
   electronic (bool): If *True* electronic scattering is included.
   intensity_data (list or ndarray): Intensity data for fitting. Default is an empty list.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``auto``.
   resolution (float or :class:`Var`): Resolution value for convolution (Gamma). Default is 1.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions. Default is 1.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting. Default is 1.
   kernel_type (string): Type of the resolution kernel. Is used for the convolution of the calculated energy spectrum with the resolution kernel.
      Can be *Gauss* or *Lorentz*. Default is *Lorentz*.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.
     
     .. versionadded:: 1.0.1

   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment for the calculation.
   detuning (list or ndarray): Detuning values for the calculation (Gamma).
   electronic (bool): If *True* electronic scattering is included.
   result (list): List of detuning dependent intensity values.
   intensity_data (list): Intensity data for fitting.
   data_size (int): Number of data points of experimental intensity data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   resolution (:class:`Var`): Resolution value for convolution.
   resolution_kernel (list): The used kernel, same step size as detuning.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   kernel_type (string): Type of the resolution kernel. Is used for the convolution of the calculated energy spectrum with the resolution kernel.
      Can be *Gauss* or *Lorentz*.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   time_gate (list):  An empty or two element list.
     If a two element list is passed these two values [start, stop] are taken as a time gating.
     The spectrum is Fourier transformed, the time gate is applied, and time response is transformed back to a spectrum.
     This does not give a time gated Moessbauer spectrum.
     Given in ns.
     
     .. versionadded:: 1.0.1

   coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
     Default is ``False``.

     .. versionadded:: 1.0.3

*/
struct EmissionSpectrum : public EnergySpectrum {
  EmissionSpectrum(
    Experiment* const experiment,
    const std::vector<double>& detuning,
    const bool electronic,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const resolution,
    const unsigned int distribution_points,
    const double fit_weight,
    const std::string kernel_type,
    Residual* const residual,
    const std::vector<double> time_gate,
    const bool coherence,
    const std::string id
  );

  std::vector<double> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;
};

#endif // NEXUS_ENERGY_SPECTRUM_H_
