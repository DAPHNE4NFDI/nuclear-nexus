// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the energy time spectrum (amplitude and intensity) functor.
// It is an array of time spectra at different energetic detunings of a sample with respect to all other element in the experiment.

// bool coherence not implemented up to now. Requires more changes in code here.
// Unlikely to be used for EnergyTimeSpectra.

#ifndef NEXUS_ENERGY_TIME_SPECTRUM_H_
#define NEXUS_ENERGY_TIME_SPECTRUM_H_

#include <string>
#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../math/intensity.h"
#include "../classes/experiment.h"
#include "../functors/measurement.h"
#include "../functors/fit_measurement.h"


/**
Constructor for the :class:`EnergyTimeAmplitude` class.
Class to calculate the energy and time-dependent Jones vector of the experiment.
One sample has to have a drive detuning.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   time_length (float): Length of the calculation in nanoseconds.
   time_step (float): Step size of the calculation in nanoseconds.
   mode (string): Identifier for the calculation mode.

      * ``i`` Interpolate detuning values. Fast mode.
      * ``f`` Full calculation via change of the isomer shift. Slow mode.

   electronic (bool): If ``True`` electronic scattering is also included.

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment
   time_length (float): Length of the calculation in nanoseconds.
   time_step (float): Step size of the calculation in nanoseconds.
   electronic (bool): If *True* electronic scattering is also included.
   mode (string): Identifier for the calculation mode.

      * ``i``: interpolate detuning values, fast mode.
      * ``f``: full calculation via change of the isomer shift.

    result (array): Array of Jones vectors in units of (:math:`\sqrt(\Gamma/ns)`).
    drive_sample (:class:`BasicSample`): :class:`BasicSample` object with drive detuning.

      .. versionadded:: 1.0.3

      .. versionchanged:: 2.0.0

         changed from old :class:`Sample` class to :class:`BasicSample`.
*/
struct EnergyTimeAmplitude : public Measurement {
  EnergyTimeAmplitude(
    Experiment* const experiment,
    const double time_length,
    const double time_step,
    const std::string mode,
    const bool electronic,
    const std::string id
  );

  // std:vector<std::vector<Eigen::Vector2cd>> leads to a swig memory leak, no destructor found
  // instead of creating a new typemap for the double vector the return is std::vector<Eigen::MatrixXcd>
  std::vector<Eigen::MatrixXcd> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  mutable std::vector<Eigen::MatrixXcd> result{};

  double time_length = 0.0;

  double time_step = 0.0;

  std::string mode = "";

  bool electronic = false;

  mutable BasicSample* drive_sample = nullptr;

private:

  BasicSample* CheckAndGetValidDriveSample() const;

  intensity::AmplitudeVector TimeResponse(const double fourier_norm, const double time_length_response,
    const double time_step_response, const bool electronic_response) const;

  std::vector<Eigen::MatrixXcd> AmplitudeInterpolated(BasicSample* drive_sample, const double fourier_norm) const;

  std::vector<Eigen::MatrixXcd> AmplitudeFull(BasicSample* drive_sample, const double fourier_norm) const;

  Eigen::MatrixXcd Convert(const intensity::AmplitudeVector& input) const;
};



/**
Constructor for the :class:`EnergyTimeSpectrum` class.
Class to calculate the energy and time-dependent intensity of the experiment.
One sample has to have a drive detuning.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   time_length (float): Length of the calculation in nanoseconds.
   time_step (float): Step size of the calculation in nanoseconds.
   mode (string): Identifier for the calculation mode.
   
      * ``i`` Interpolate detuning values. Fast mode.
      * ``f`` Full calculation via change of the isomer shift. Slow mode.
   
   electronic (bool): If ``True`` electronic scattering is also included.
   time_data (list or ndarray): Time data for fitting.
   intensity_data (ndarray): 2D intensity data for fitting. 1st dimension detuning, 2nd dimension time.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``0``.
   energy_resolution (float or :class:`Var`): Energy resolution value for convolution.
   time_resolution (float or :class:`Var`): Time resolution value for convolution.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions. Default is 1.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   bunch_spacing (float): Spacing of subsequent X-ray bunches in time (nanoseconds).
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment
   time_length (float): Length of the calculation in nanoseconds.
   time_step (float): Step size of the calculation in nanoseconds.
   electronic (bool): If *True* electronic scattering is also included.
   mode (string): Identifier for the calculation mode.

      * ``i``: interpolate detuning values, fast mode.
      * ``f``: full calculation via change of the isomer shift.
   
   time_data (list): Time data for fitting.
   intensity_data (array): 2D intensity data for fitting. 1st dimension detuning, 2nd dimension time.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Intensity background for fitting.
   energy_resolution (:class:`Var`): Energy resolution value used for the convolution.
      The energy resolution is not the energy resolution of the drive sample (e.g. an analyzer foil).
      This intrinsic resolution is given by the energy spectrum of drive sample.
      The energy resolution specified here is an additional energy resolution due to the experimental conditions, like an in proper drive motion itself.
   time_resolution (:class:`Var`): Time resolution value for convolution.
   resolution_kernel (array): Weight of 2D Gaussian resolution. Same step size as detuning.
   distribution_points (int): Number of points for thickness distributions in forward geometry and angular divergence in grazing geometry.
      Both distributions assume incoherent summation over the weighted contributions.
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   bunch_spacing (float): Spacing of subsequent X-ray bunches in time (nanoseconds).
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   result (ndarray): Array of intensity values in units of (:math:`\Gamma/ns`).
   drive_sample (:class:`BasicSample`): Sample with drive detuning.

     .. versionadded:: 1.0.3

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`BasicSample`.
*/
//coherence (bool):  Determines if the summation over the thickness and divergence distributions is performed coherently (``True``) or incoherently (``False``).
//Default is ``False``.
//
//..versionadded:: 1.0.3
//
struct EnergyTimeSpectrum : public FitMeasurement {
  EnergyTimeSpectrum(
    Experiment* const experiment,
    const double time_length,
    const double time_step,
    const std::string mode,
    const bool electronic,
    const std::vector<double>& time_data,
    const std::vector<std::vector<double>>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const energy_resolution,
    Var* const time_resolution,
    const unsigned int distribution_points,
    const double fit_weight,
    const double bunch_spacing,
    Residual* const residual,
    const std::string id
  );

  std::vector<std::vector<double>> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;

  mutable std::vector<std::vector<double>> result{};

  double time_length = 0.0;

  double time_step = 0.0;

  std::string mode = "";

  bool electronic = false;

  std::vector<double> time_data{};

  std::vector<std::vector<double>> intensity_data{};  // the inner vector is the time axis, outer vector is drive detuning axis

  mutable std::vector<std::vector<double>> resolution_kernel{};

  double bunch_spacing = INFINITY;

  Var* energy_resolution = resolution;

  Var* time_resolution = resolution_2;

  mutable BasicSample* drive_sample = nullptr;

private:

  BasicSample* CheckAndGetValidDriveSample() const;

  std::vector<double> TimeResponse(const double fourier_norm, const double time_length_response,
    const double time_step_response, const bool electronic_response) const;

  std::vector<std::vector<double>> IntensityInterpolated(BasicSample* drive_sample, const double fourier_norm) const;

  std::vector<std::vector<double>> IntensityFull(BasicSample* drive_sample, const double fourier_norm) const;
};


/**
Constructor for the :class:`IntegratedEnergyTimeSpectrum` class.
Class to calculate the energy and time-integrated [``integration_start``, ``integration_stop``] resonant intensity of the experiment.
One sample has to have a drive detuning.

Args:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment.
   time_length (float): Length of the calculation (nanoseconds).
   time_step (float): Step size of the calculation (nanoseconds).
   integration_start (float): Start time of the integration (nanosecond).
   integration_stop (float): Stop time of the integration (nanosecond).
   mode (string): Identifier for the calculation mode.

      * ``i`` - Interpolate detuning values. Fast mode.
      * ``f`` - Full calculation via change of the isomer shift. Slow mode.

   electronic (bool): If ``True`` electronic scattering is also included.
   time_data (list or ndarray): Time data for fitting.
   intensity_data (list or ndarray): Time integrated intensity data for fitting.
   scaling (float or :class:`Var` or string): Intensity scaling factor for fitting. Default is ``auto``.
   background (float or :class:`Var` or string): Background for fitting. Default is ``0``.
   energy_resolution (float or :class:`Var`): Energy resolution value for convolution.
      The energy resolution is not the energy resolution of the drive sample (e.g. an analyzer foil).
      This intrinsic resolution is given by the energy spectrum of drive sample.
      The energy resolution specified here is an additional energy resolution due to the experimental conditions, like an in proper drive motion itself.
   time_resolution (float or :class:`Var`): Time resolution value for convolution.
   distribution_points (int): Number of points for roughness distributions in forward geometry
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.

Attributes:
   id (string): User identifier.
   experiment(:class:`Experiment`): Experiment
   time_length (float): Length of the calculation (nanoseconds).
   time_step (float): Step size of the calculation (nanoseconds).
   integration_start (float): Start time of the integration (nanosecond).
   integration_stop (float): Stop time of the integration (nanosecond).
   electronic (bool): If *True* electronic scattering is also included.
   mode (string): Identifier for the calculation mode.

      * ``i`` - Interpolate detuning values. Fast mode.
      * ``f`` - Full calculation via change of the isomer shift. Slow mode.

   time_data (list): Time data for fitting.
   intensity_data (list or ndarray): Time integrated intensity data for fitting.
   data_size (int): Number of data points of experimental data.
   scaling (:class:`Var`): Intensity scaling factor for fitting.
   background (:class:`Var`): Background for fitting.
   energy_resolution (:class:`Var`): Energy resolution value for convolution.
      The energy resolution is not the energy resolution of the drive sample (e.g. an analyzer foil).
      This intrinsic resolution is given by the energy spectrum of drive sample.
      The energy resolution specified here is an additional energy resolution due to the experimental conditions, like an in proper drive motion itself.
   time_resolution (:class:`Var`): Time resolution value for convolution.
   resolution_kernel (array): Weight of 2D Gaussian resolution. Same step size as detuning.
   distribution_points (int): Number of points for roughness distributions in forward geometry
   fit_weight (float): Relative weight for the cost function in multi measurement fitting.
   residual (:class:`Residual`): Implementation of the residual calculation used for fitting.
   result (ndarray): List of intensity values in units of (:math:`\Gamma`).
   drive_sample (:class:`BasicSample`): Sample object with drive detuning.

     .. versionadded:: 1.0.3

     .. versionchanged:: 2.0.0

        changed from old :class:`Sample` class to :class:`BasicSample`.
*/
//coherence(bool) : Determines if the summation over the thicknessand divergence distributions is performed coherently(``True``) or incoherently(``False``).
//  Default is ``False``.
//
//  ..versionadded:: 1.0.3
//
struct IntegratedEnergyTimeSpectrum : public FitMeasurement {
  IntegratedEnergyTimeSpectrum(
    Experiment* const experiment,
    const double time_length,
    const double time_step,
    const double integration_start,
    const double integration_stop,
    const std::string mode,
    const bool electronic,
    const std::vector<double>& intensity_data,
    Var* const scaling,
    Var* const background,
    Var* const energy_resolution,
    Var* const time_resolution,
    const unsigned int distribution_points,
    const double fit_weight,
    const double bunch_spacing,
    Residual* const residual,
    const std::string id
  );

  std::vector<double> operator() (const bool update_and_calc_transitions = true, const bool check_W_matrix = true) const;

  bool operator() (double const* const* parameters, double* residuals) const;

  double Residuals() const;

  mutable std::vector<double> result{};

  double time_length = 0.0;
  
  double time_step = 0.0;

  double integration_start = 0.0;
  
  double integration_stop = 0.0;

  std::string mode = "";
  
  bool electronic = false;

  std::vector<double> intensity_data{}; // the inner vector is the time axis, outer vector is drive detuning axis

  double bunch_spacing = INFINITY;

  mutable std::vector<std::vector<double>> resolution_kernel{};

  mutable BasicSample* drive_sample = nullptr;

private:
  BasicSample* CheckAndGetValidDriveSample() const;
};


#endif // NEXUS_ENERGY_TIME_SPECTRUM_H_
