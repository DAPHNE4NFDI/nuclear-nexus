// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module electronic_transmission

%{
    #define SWIG_FILE_WITH_INIT
    #include "electronic_transmission.h"
%}


%feature("shadow") TransmissionAmplitude::TransmissionAmplitude %{
def __init__(self, experiment, sample, energy, angles, id = ""):

    self._experiment = experiment

    self._sample = sample

    if isinstance(angles, (int, float)):
        angles = [angles]

    _cnexus.TransmissionAmplitude_swiginit(self, _cnexus.new_TransmissionAmplitude(experiment, sample, energy, angles, id))
%}


%feature("shadow") TransmissionAmplitude::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of complex values.
    """
    return np.array(_cnexus.TransmissionAmplitude___call__(self))
%}


%feature("shadow") Transmission::Transmission %{
def __init__(self,
             experiment,
             sample,
             energy,
             angles,
             intensity_data = [],
             scaling = "auto",
             background = 0.0,
             offset = 0.0,
             resolution = internal_angular_resolution,
             fit_weight = 1.0,
             residual = Log10(),
             id = ""):

    intensity_data = np.array(intensity_data).astype(float)

    self._experiment = experiment

    self._sample = sample

    if isinstance(angles, (int, float)):
        angles = [angles]

    if scaling == "auto" and len(intensity_data) > 0:
      scaling = np.amax(intensity_data)
      scaling = Var(scaling, 0.0, 100.0*scaling, True, "Trans scaling")
    elif scaling == "auto" and len(intensity_data) == 0:
      scaling = Var(1.0, 0.0, np.inf, True, "Trans scaling")
    elif isinstance(scaling, (int, float)):
      scaling = Var(scaling, 0.0, np.inf, True, "Trans scaling")
    elif not isinstance(scaling, (Var)):
      raise Exception("scaling has wrong format")

    if background == "auto" and len(intensity_data) > 0:
      background = np.amin(intensity_data)
      background = Var(background, 0.0, 100.0*background, True, "Trans backgr")
    elif background == "auto" and len(intensity_data) == 0:
      background = Var(0.0, 0.0, np.inf, False, "Trans backgr")
    elif isinstance(background, (int, float)):
      background = Var(background, 0.0, np.inf, False, "Trans backgr")
    elif not isinstance(background, (Var)):
      raise Exception("background has wrong format")

    if isinstance(offset, (int, float)):
      offset = Var(offset, -1.0, 1.0, False, "Trans angular offset")

    if isinstance(resolution, (int, float)):
      resolution = Var(resolution, 0.0, 10.0, False, "Trans resolution")

    self._scaling = scaling

    self._background = background

    self._resolution = resolution

    self._offset = offset

    self._residual = residual

    _cnexus.Transmission_swiginit(self, _cnexus.new_Transmission(experiment,
                                                                 sample,
                                                                 energy,
                                                                 angles,
                                                                 intensity_data,
                                                                 scaling,
                                                                 background,
                                                                 offset,
                                                                 resolution,
                                                                 fit_weight,
                                                                 residual,
                                                                 id))
%}


%feature("shadow") Transmission::operator() %{
def __call__(self):
    r"""
    Calculates the class method.
    Also callable via operator ``()``.

    Returns:
        ndarray: Array of float values.
    """
    return np.array(_cnexus.Transmission___call__(self))
%}



%include "electronic_transmission.h"



%extend TransmissionAmplitude {
  %pythoncode{
    
  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_sample") and not hasattr(self, attr):
      raise Exception("TransmissionAmplitude does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr == "sample":
      self._sample = sample

    super().__setattr__(attr, val)

  }
}

%extend Transmission {
  %pythoncode{

  Calculate = __call__

  def __setattr__(self, attr, val):
    if attr not in ("this", "_experiment", "_sample", "_scaling", "_background", "_offset", "_resolution", "_residual") and not hasattr(self, attr):
      raise Exception("Transmission does not have attribute {}".format(attr))

    if attr == "experiment":
      self._experiment = experiment

    if attr == "sample":
      self._sample = sample

    if attr in ("scaling", "background", "offset", "resolution"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if isinstance(val, (list, tuple)):
        val = DoubleVector(val)
        super().__setattr__(attr, val)
        return

    if attr == "scaling":
      self._scaling = val

    if attr == "background":
      self._background = val

    if attr == "offset":
      self._offset = val

    if attr == "resolution":
      self._resolution = val

    if attr == "residual":
      self._residual = val

    super().__setattr__(attr, val)


  def Plot(self,
           data = True,
           residuals = True,
           datacolor='black', 
           theorycolor='r',
           theorywidth=2,
           datalinestyle='none',
           datamarker='+',
           datamarkersize=2,
           datafillstyle='full',
           legend=True,
           errors= False,
           errorcap=2,
           name=None):
    r"""
    Plot the :class:`Transmission`.

    .. versionadded:: 1.0.3

    .. versionchanged:: 1.2.0

    Args:
      data (bool): Select if :attr:`intensity_data` should be plot.
      residuals (bool): Select if :attr:`residuals` from fit should be plot.
      datacolor (string): See *matplotlib* documentation.
      theorycolor (string): See *matplotlib* documentation.
      theorywidth (float): See *matplotlib* documentation.
      datalinestyle (string): See *matplotlib* documentation.
      datamarker (string): See *matplotlib* documentation.
      datamarkersize (float): See *matplotlib* documentation.
      datafillstyle (string): See *matplotlib* documentation.
      legend (bool): Select if legend should be plot when model and data are plot.
      errors (bool): Select if error bars should be plot.
      errorcap (float): See *matplotlib* documentation (``capsize``).
      name(string): If given, the plot is saved under this name.
         
        .. versionadded:: 1.1.0
    """
    title_string = r'Transmission: '

    x_axis = self.angles
    x_label = r'angle (deg)'

    if self.scaling.value == 1.0:
      y_label=r'intensity'
    else:
      y_label=r'counts'

    self.FitMeasruementPlot(x_axis,
                            x_label,
                            y_label,
                            title_string,
                            x_axis_data=None,
                            log=True,
                            data=data,
                            residuals=residuals,
                            datacolor=datacolor, 
                            theorycolor=theorycolor,
                            theorywidth=theorywidth,
                            datalinestyle=datalinestyle,
                            datamarker=datamarker,
                            datamarkersize=datamarkersize,
                            datafillstyle=datafillstyle,
                            legend=legend,
                            errors=errors,
                            errorcap=errorcap,
                            name=name)

  }
}
