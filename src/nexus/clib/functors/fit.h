// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Fit module.
// Fit functors only depends on general fit measurement properties.
// No need to include the specific functor methods.


#ifndef NEXUS_FIT_H_
#define NEXUS_FIT_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"

#include "../classes/experiment.h"
#include "../classes/optimizer_handler.h"
#include "../classes/optimizer_options.h"
#include "../classes/inequality.h"

#include "../functors/measurement.h"
#include "../functors/fit_measurement.h"


/**
Constructor for :class:`Fit` class.
Performs a fit of a measured data set to the given theoretical model.
Local and global optimization algorithms are available.
An arbitrary number of measurements can be fit simultaneously.

Args:
   measurements (list): List of :class:`FitMeasurement` objects that should be fit simultaneously.
   id (string): User identifier.
   external_fit_variables (list): List of :class:`Var` objects.
     These Vars are not directly assigned to a *Nexus* object but can be related to an equality constraint on a Var.
     *Nexus* is not able to detect those automatically, so they have to provided manually.

     .. versionadded:: 1.1.0

   inequalities (:class:`Inequality`): An :class:`Inequality` object.
     All inequalities constraints are handled by this object.

     .. versionadded:: 1.1.0

Attributes:
   measurements (list): List of :class:`FitMeasurement` objects that should be fit simultaneously.
   id (string): User identifier.
   fit_variables (list): List of :class:`Var` objects with the fit variables.
   external_fit_variables (list): List of :class:`Var` objects with external fit varibales.
     These Vars are not directly assigned to a *Nexus* object but can be related to an equality constraint on a Var.
     *Nexus* is not able to detect those automatically, so they have to provided manually.

     .. versionadded:: 1.1.0

   inequalities (:class:`Inequality`): An :class:`Inequality` object.
     All inequalities constraints are handled by this object.

     .. versionadded:: 1.1.0

   options (:class:`OptimizerOptions`): Options that specify how the fit is performed.
   initial_parameters (list): List with initial values of the fit parameters.
   lower_bounds (list): List with min values of the fit parameters.
   upper_bounds (list): List with max values of the fit parameters.
   ids (list): List with ids of the fit parameters.
   total_squared_residuals (float): Total sum of squared user residuals of all data provided to the :class:`Fit` module.

     .. versionadded:: 1.1.0

   non_dependent_variables (array): Indices of parameters on which is the fit model does not depend on.
     Obtained from number of zero diagonal element of the covariance matrix (``nan`` values of the correlation matrix).

     .. versionadded:: 1.1.0

   inverse_hessian (ndarray): The inverse Hessian matrix of the fit parameters calculated from the ceres solver :math:`(J*(x)J(x))^{-1}` with :math:`J` being the Jacobi matrix of the fit problem.

     .. versionadded:: 1.1.0

   covariance_matrix (ndarray): The covariance matrix :math:`C` of the fit parameters.

     .. versionadded:: 1.1.0

   correlation_matrix (ndarray): The correlation matrix between the fit parameters.
     Calculated via the Pearson correlation coefficient :math:`C_{ij} / \sqrt{C_{ii}  C_{jj} }` from the covariance matrix.

     .. versionadded:: 1.1.0
     
   fit_parameter_errors (array): Array with the standard deviation errors of the fit parameters.

     .. versionadded:: 1.1.0

   boostrap_fit_parameters (array): 2D Array of all fit parameters over the number of bootstrap iterations.

     .. versionadded:: 1.1.0

*/
class Fit : public OptimizerHandler {
public:
  Fit(
    const std::vector<FitMeasurement*> measurements,
    const std::string id,
    const std::vector<Var*> external_fit_variables,
    Inequality* const inequality
  ) :
    OptimizerHandler(id, external_fit_variables, inequality),
    measurements(measurements)
  {}

  /*
  Evaluates the fit.
  Also callable via operator ``Evaluate()``.
  */
  void operator() ();

  std::vector<FitMeasurement*> measurements{};

  Eigen::MatrixXd boostrap_fit_parameters{};

  void CallBootstrap();

private:

  bool PopulateFitHandler();

  std::stringstream StartOutput() const;

  std::stringstream EndOutput() const;

  void ResetErrorData();

  void RunFit(const bool output);

  void RunErrorAnalysis(const std::string error_method_);

  void CallMethod(const std::string method, const OptimizerModule fit_module, const bool output);

  void CeresFit(const std::string method, const bool error, const bool output);

  void NLoptFit(const std::string method, const bool output);

  void PagmoFit(const std::string method, const bool output);

  void EvaluateCovariance(const Eigen::MatrixXd& cov_matrix);

  void CeresErrors(ceres::Problem& ceres_problem);
};

#endif // NEXUS_FIT_H_
