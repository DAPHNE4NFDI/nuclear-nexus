// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.



#include "electronic.h"

#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/intensity.h"


Amplitude::Amplitude(
  Experiment* const experiment_,
  const double energy_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  energy(energy_)
{}


Complex Amplitude::operator() (const bool update) const
{
  if (update)
    Update();

  Complex electronic_amplitude(1.0, 0.0);

  for(NxObject* const obj : experiment->objects)
    electronic_amplitude *= obj->ElectronicAmplitude(energy);

  return electronic_amplitude;
}


AmplitudeMatrix::AmplitudeMatrix(
  Experiment* const experiment_,
  const double energy_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  energy(energy_)
{}


Eigen::Matrix2cd AmplitudeMatrix::operator() (const bool update) const
{
  if (update)
    Update();

  Eigen::Matrix2cd electronic_amplitude_matrix = Eigen::Matrix2cd::Identity();

  for (NxObject* const obj : experiment->objects)
    electronic_amplitude_matrix *= obj->ElectronicAmplitudeMatrix(energy);

  return electronic_amplitude_matrix;
}


Amplitudes::Amplitudes(
  Experiment* const experiment_,
  const double energy_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  energy(energy_)
{
  CheckGrazingAngleExperiment("Amplitudes");
}

std::vector<Complex> Amplitudes::operator() () const
{
  Update();

  CheckWmatrixValidityExperiment("Amplitudes", energy);

  Complex electronic_amplitude(1.0, 0.0);

  std::vector<Complex> electronic_amplitudes(experiment->objects.size());

  std::transform(experiment->objects.begin(), experiment->objects.end(), electronic_amplitudes.begin(),
    [&](NxObject* const obj)
    {
      electronic_amplitude *= obj->ElectronicAmplitude(energy);

      return electronic_amplitude;
    }
  );

  result = electronic_amplitudes;

  return electronic_amplitudes;
}


Matrices::Matrices(
  Experiment* const experiment_,
  const double energy_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  energy(energy_)
{
  CheckGrazingAngleExperiment("Matrices");
}


std::vector<Eigen::Matrix2cd> Matrices::operator() () const
{
  Update();

  CheckWmatrixValidityExperiment("Matrices", energy);

  Eigen::Matrix2cd electronic_amplitude_matrix = Eigen::Matrix2cd::Identity();

  std::vector<Eigen::Matrix2cd> electronic_amplitude_matrices(experiment->objects.size());

  std::transform(experiment->objects.begin(), experiment->objects.end(), electronic_amplitude_matrices.begin(),
    [&](NxObject* const obj)
    {
      electronic_amplitude_matrix *= obj->ElectronicAmplitudeMatrix(energy);

      return electronic_amplitude_matrix;
    }
  );

  result = electronic_amplitude_matrices;

  return electronic_amplitude_matrices;
}


JonesVectors::JonesVectors(
  Experiment* const experiment_,
  const double energy_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  energy(energy_)
{
  CheckGrazingAngleExperiment("JonesVector");
}

std::vector<Eigen::Vector2cd> JonesVectors::operator() () const
{
  Update();

  CheckWmatrixValidityExperiment("JonesVector", energy);

  Eigen::Matrix2cd electronic_amplitude_matrix = Eigen::Matrix2cd::Identity();

  std::vector<Eigen::Vector2cd> electronic_amplitude_vectors(experiment->objects.size());

  std::transform(experiment->objects.begin(), experiment->objects.end(), electronic_amplitude_vectors.begin(),
    [&](NxObject* const obj)
    {
      electronic_amplitude_matrix *= obj->ElectronicAmplitudeMatrix(energy);

      return intensity::Amplitude(electronic_amplitude_matrix, experiment->beam);
    }
  );

  result = electronic_amplitude_vectors;

  return electronic_amplitude_vectors;
}


Intensities::Intensities(
  Experiment* const experiment_,
  const double energy_,
  const std::string id_
) :
  Measurement(experiment_, id_),
  energy(energy_)
{
  CheckGrazingAngleExperiment("Intensities");
}


std::vector<double> Intensities::operator() () const
{
  Update();

  CheckWmatrixValidityExperiment("Intensities", energy);

  Eigen::Matrix2cd electronic_amplitude_matrix = Eigen::Matrix2cd::Identity();

  std::vector<double> electronic_intensities(experiment->objects.size());

  std::transform(experiment->objects.begin(), experiment->objects.end(), electronic_intensities.begin(),
    [&](NxObject* const obj)
    {
      electronic_amplitude_matrix *= obj->ElectronicAmplitudeMatrix(energy);

      return intensity::Intensity(electronic_amplitude_matrix, experiment->beam);
    }
  );

  result = electronic_intensities;

  return electronic_intensities;
}


FieldAmplitude::FieldAmplitude(
  BasicSample* const sample_,
  const double energy_,
  const unsigned int num_points_,
  const std::string id_
) :
  Measurement(nullptr, id_),
  sample(sample_),
  energy(energy_),
  num_points(num_points_)
{
  if (GrazingSample* const grazing_sample = dynamic_cast<GrazingSample*>(sample_))
    CheckGrazingAngleSample("FieldAmplitude", grazing_sample);
}


std::vector<Complex> FieldAmplitude::operator() () const
{
  sample->Update();

  depth = PopulateDepth(num_points, sample->TotalThickness());

  if (GrazingSample* const grazing_sample = dynamic_cast<GrazingSample*>(sample))
  {
    CheckWmatrixValiditySample("FieldAmplitude", grazing_sample, energy);

    if (grazing_sample->roughness == "e")
      depth = PopulateDepth(num_points, grazing_sample->TotalThicknessEffective());
  }

  const std::vector<Complex> field_amplitude = sample->ElectronicFieldAmplitude(energy, num_points);

  result = field_amplitude;

  return field_amplitude;
}


FieldIntensity::FieldIntensity(
  BasicSample* const sample_,
  const double energy_,
  const unsigned int num_points_,
  const std::string id_
) :
  Measurement(nullptr, id_),
  sample(sample_),
  energy(energy_),
  num_points(num_points_)
{
  if (GrazingSample* const grazing_sample = dynamic_cast<GrazingSample*>(sample_))
    CheckGrazingAngleSample("FieldIntensity", grazing_sample);
}


std::vector<double> FieldIntensity::operator() () const
{
  sample->Update();

  depth = PopulateDepth(num_points, sample->TotalThickness());

  if (GrazingSample* const grazing_sample = dynamic_cast<GrazingSample*>(sample))
  {
    CheckWmatrixValiditySample("FieldIntensity", grazing_sample, energy);

    if (grazing_sample->roughness == "e")
      depth = PopulateDepth(num_points, grazing_sample->TotalThicknessEffective());
  }

  const std::vector<double> field_intensity = sample->ElectronicFieldIntensity(energy, num_points);

  result = field_intensity;

  return field_intensity;
}
