// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "optimizer.h"

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <utility>

#include <Eigen/Dense>

#ifdef NEXUS_USE_PAGMO
#include <pagmo/pagmo.hpp>
#endif

#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/fourier.h"
#include "../math/intensity.h"
#include "../classes/nxvariable.h"
#include "../utilities/python_print.h"
#include "../utilities/file_output.h"


void Optimizer::operator() ()
{
  if (PopulateFitHandler() == false)
    return;

  // initial check for FitOptions and the GlobalFitHandler
  options->InitialCheck();

  if (!Initialize(options->global_fit))
    return;

  start_residual = Residual();

  fit_parameter_pointers = GetFitParameterPointers();

  initial_parameters = GetInitialParameters();

  lower_bounds = GetLowerBounds();

  upper_bounds = GetUpperBounds();

  ids = GetIds();

  fit_equalities = GetFitEqualities();

  // write special array needed for NLopt
  nlopt_fit_parameters = new double[initial_parameters.size()];

  for (size_t i = 0; i < initial_parameters.size(); ++i)
    nlopt_fit_parameters[i] = initial_parameters.at(i);

  python_print::output(StartOutput().str());

  CallMethod(options->method, options->fit_module);

  if (std::find(options->pagmo_methods.begin(), options->pagmo_methods.end(), options->method) != options->pagmo_methods.end())
    CallMethod(options->local, options->fit_module_local);

  for (Measurement* measurement : measurements)
  {
    for(Var* const var : fit_equalities)
      var->value = var->equality->Function();

    measurement->experiment->Update();
  }

  const std::stringstream ss = EndOutput();

  python_print::output(ss.str() + "\n");

  if (options->file_output)
    file_output::write_file("optimizer_id_" + id + ".txt", ss.str(), true);

  delete[] nlopt_fit_parameters;
}


bool Optimizer::operator() (double const* const* parameters, double* residuals) const
{
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = *parameters[i];

  for (Var* const var : fit_equalities)
    var->value = var->equality->Function();

  residuals[0] = Residual();

  if (inequality != nullptr)
    if (inequality->Function() == false)
      residuals[0] = 2.0e299;

  return true;
}


// Scans the whole fit environment for all passed Vars and adds them to the fit problem
bool Optimizer::PopulateFitHandler()
{
  bool ret = true;

  ClearFitVariables();

  for (Measurement* meas : measurements)
    meas->PopulateOptimizerHandler(this);

  for (Var* const var : external_fit_variables)
    PrepareFitVariable(var);

  if (GetFitVariables().size() == 0 && external_fit_variables.size() == 0)
  {
    errors::WarningMessage("Optimizer::operator()", "No optimization parameter found. No optimization performed.");

    ret = false;
  }

  return ret;
}


std::stringstream Optimizer::StartOutput() const
{
  std::stringstream ss("");

  ss << "\nRun Optimizer instance with id: " << id << " \n";

  ss << "\nStarting optimizer with " << measurements.size() << " provided measurement dependencies and " << std::to_string(fit_parameter_pointers.size()) << " fit parameter(s):\n\n";

  ss << "  no. |                           id |       initial value |              min |              max\n";

  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << ids[i];
    ss << " | " << std::setw(19) << initial_parameters[i];
    ss << " | " << std::setw(16) << lower_bounds[i];
    ss << " | " << std::setw(16) << upper_bounds[i];
    ss << "\n";
  }

  ss << "\nUsing " << fit_equalities.size() << " equality constraint(s) on parameter(s):\n";

  if (fit_equalities.size() > 0)
    ss << "\n  no. |                           id |               value\n";

  for (size_t i = 0; i < fit_equalities.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << fit_equalities[i]->id;
    ss << " | " << std::setw(19) << fit_equalities[i]->equality->Function();
    ss << "\n";
  }

  if (inequality == nullptr)
    ss << "\nUsing 0 inequality constraint(s).\n";
  else
    ss << "\nUsing " << inequality->NumFunction() << " inequality constraint(s).\n";

  ss << "\nResidual start value: " << start_residual << "\n";

  return ss;
}


std::stringstream Optimizer::EndOutput() const
{
  std::stringstream ss("");

  ss << "\nOptimizer finished with " << fit_parameter_pointers.size() << " fit parameter(s):\n";

  ss << "\n  no. |                           id |           fit value |       initial value |              min |              max\n";

  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << ids[i];
    ss << " | " << std::setw(19) << *fit_parameter_pointers[i];
    ss << " | " << std::setw(19) << initial_parameters[i];
    ss << " | " << std::setw(16) << lower_bounds[i];
    ss << " | " << std::setw(16) << upper_bounds[i];
    ss << "\n";
  }

  ss << "\nand " << fit_equalities.size() << " equality constraint(s) on parameter(s):\n";

  if (fit_equalities.size() > 0)
    ss << "\n  no. |                           id |               value\n";

  for (size_t i = 0; i < fit_equalities.size(); ++i)
  {
    ss << " " << std::setw(4) << i;
    ss << " | " << std::setw(28) << fit_equalities[i]->id;
    ss << " | " << std::setw(19) << fit_equalities[i]->value;
    ss << "\n";
  }

  if (inequality == nullptr)
  {
    ss << "\nand 0 inequality constraint(s).\n";
  }
  else
  {
    ss << "\nand " << inequality->NumFunction() << " inequality constraint(s).\n";
  }

  double sqr_residual = pow(Residual(), 2);

  if (inequality != nullptr)
    if (inequality->Function() == false)
      sqr_residual = 2.0e299;

  if (sqr_residual > 1.0e298)
  {
    ss << "\nInequality constraint not satisfied.\n";
  }
  else
  {
    ss << "\nOptimized residual from " << start_residual << " to " << Residual() << "\n";
  }

  ss << "\nOptimizer instance finished. id:" << id << "\n";

  return ss;
}


void Optimizer::CallMethod(const std::string method, const OptimizerModule fit_module)
{
  if (fit_module == OptimizerModule::Ceres)
  {
    python_print::output("\nCalling ceres solver with fit method " + method + "\n");

    CeresFit(method);
  }
  else if (fit_module == OptimizerModule::NLopt)
  {
    python_print::output("\nCalling NLopt solver with fit method " + method + "\n");

    NLoptFit(method);
  }
  else if (fit_module == OptimizerModule::Pagmo)
  {
    python_print::output("\nCalling Pagmo solver with fit method " + method + "\n");

    PagmoFit(method);
  }
  else
  {
    errors::WarningMessage("Fit", "Fit method not found in any of the available solver modules.");
  }
}


void Optimizer::CeresFit(const std::string method)
{
  ceres::Problem ceres_problem;

  ceres::NumericDiffOptions ceres_numeric_cost_options;

  ceres::Solver::Options ceres_solver_options;

  ceres::Solver::Summary ceres_summary;

  SetCeresOptions(method, ceres_solver_options, ceres_numeric_cost_options);

  // here the Optimizer object is passed
  // it has the target function Optimizer::operator()
  ceres::DynamicNumericDiffCostFunction<Optimizer, ceres::CENTRAL>* cost_function =
    new ceres::DynamicNumericDiffCostFunction<Optimizer, ceres::CENTRAL>(
      this, ceres::DO_NOT_TAKE_OWNERSHIP, ceres_numeric_cost_options);

  // add a parameter for each entry in vectors of ceres fit parameter pointers
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    cost_function->AddParameterBlock(1);

  cost_function->SetNumResiduals(1);

  ceres_problem.AddResidualBlock(cost_function, NULL, fit_parameter_pointers);

  // set boundaries for ceres solver
  // keep boundary index at zero, as there is only one ParameterBlock added for each fit parameter to the cost_function
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
  {
    ceres_problem.SetParameterLowerBound(fit_parameter_pointers[i], 0, lower_bounds[i]);
    ceres_problem.SetParameterUpperBound(fit_parameter_pointers[i], 0, upper_bounds[i]);
  }

  std::cout << "Start solving\n";

  ceres::Solve(ceres_solver_options, &ceres_problem, &ceres_summary);

  std::cout << "Solving done\n";

  if (options->report == "Brief")
  {
    python_print::output(ceres_summary.BriefReport());
  }
  else if (options->report == "Full")
  {
    python_print::output(ceres_summary.FullReport());
  }
}


// The NLopt lib only takes a reference to a double vector, while ceres takes a pointr to a vector of pointers.
// So, we have to pass the initial values as an array of doubles. 
// In the cost functions we have to use this array to set the new fit_parameters, similar to is done with Ceres with pointers.
// However, NLopt internally copies the parameter vector in the cpp interface. use the c functions directly!
// And in some NLopt functions, it seems that data are copied again, so the trick with the pointers is not working for some algorithms.
// Or at least I do not see why certain methods do not work at all, like BOBYA.

#ifdef NEXUS_USE_NLOPT
static int nlopt_iteration_optimizer;
#endif

// function to calculate the squared total residual of the fit method
#ifdef NEXUS_USE_NLOPT
double NLoptFitModelOptimizer(unsigned n, const double* params, double* grad, void* opt_class)
{
  Optimizer* const optimizer_object = static_cast<Optimizer*>(opt_class);

  ++nlopt_iteration_optimizer;

  if ((nlopt_iteration_optimizer - 1) % 10 == 0)
    std::cout << std::setw(6) << nlopt_iteration_optimizer << std::endl;

  double residual = optimizer_object->NloptResiduals();

  return residual;
}
#endif

#ifdef NEXUS_USE_NLOPT
double Optimizer::NloptResiduals()
{
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = nlopt_fit_parameters[i];

  for (Var* const var : fit_equalities)
    var->value = var->equality->Function();

  double sqr_residual = pow(Residual(), 2);

  if (inequality != nullptr)
    if (inequality->Function() == false)
      sqr_residual = 2.0e299;

  //return pow(Residual(), 2);
  return sqr_residual;
}
#endif

#ifdef NEXUS_USE_NLOPT
void Optimizer::NLoptFit(const std::string method)
{
  // SOLVER OPTIONS
  nlopt_algorithm algorithm = NLOPT_LN_SBPLX;

  if (method == "Newuoa")
  {
    algorithm = NLOPT_LN_NEWUOA_BOUND;
  }
  else if (method == "Subplex")
  {
    algorithm = NLOPT_LN_SBPLX;
  }

  // create the optimizer object with specific method
  nlopt_opt opt = nlopt_create(algorithm, static_cast<unsigned int>(initial_parameters.size()));

  SetNloptOptions(opt);

  // set boundaries
  nlopt_set_lower_bounds(opt, lower_bounds.data());

  nlopt_set_upper_bounds(opt, upper_bounds.data());

  // set the solver to minimize the objective function
  nlopt_set_min_objective(opt, NLoptFitModelOptimizer, this);

  nlopt_iteration_optimizer = 0;

  std::cout << "  iteration" << std::endl;

  nlopt_result ret = nlopt_optimize(opt, nlopt_fit_parameters, &cost);

  // definition of cost is 1/2 * sum over squared residuals
  cost *= 0.5;

  // needed after output from optimizer 
  std::cout << std::endl;

  const int nlopt_iterations = nlopt_get_numevals(opt);

  nlopt_destroy(opt);

  // create solver output  
  if (ret < 0)
    errors::WarningMessage("Fit", "Error in module NLopt with method " + method + ". Error ret = " + std::to_string(ret));

  if (ret == NLOPT_STOPVAL_REACHED)
  {
    python_print::output("Termination: stop value reached.");
  }
  else if (ret == NLOPT_FTOL_REACHED)
  {
    python_print::output("Termination: function tolerance reached.");
  }
  else if (ret == NLOPT_XTOL_REACHED)
  {
    python_print::output("Termination: parameter tolerance reached.");
  }
  else if (ret == NLOPT_XTOL_REACHED)
  {
    python_print::output("Termination: maximum iterations reached.");
  }
  else if (ret == NLOPT_MAXTIME_REACHED)
  {
    python_print::output("Termination: maximum time reached.");
  }

  std::stringstream ss;

  ss << "\n  cost = " << std::scientific << cost;
  python_print::output(ss.str());
  python_print::output("  iterations: " + std::to_string(nlopt_iterations));

  // Set best parameters
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = nlopt_fit_parameters[i];
}
#else
void Optimizer::NLoptFit(const std::string method)
{
  errors::WarningMessage("Fit", "Nexus was compiled without NLopt support.");
}
#endif


// Pagmo

// pagmo::vector_double is just the internal name for std::vector<double>

#ifdef NEXUS_USE_PAGMO
class PagmoFunctorOptimizer {
public:
  // The default constructor has to exist OR all values have to be passed with default values
  PagmoFunctorOptimizer(Optimizer* const opt = nullptr)
    : opt(opt)
  {};

  // Implementation of the objective function.
  // fitness defined in pagmo.
  pagmo::vector_double fitness(const pagmo::vector_double& inputs) const
  {
    for (size_t i = 0; i < opt->fit_parameter_pointers.size(); ++i)
      *(opt->fit_parameter_pointers[i]) = inputs[i];

    for (Var* const var : opt->fit_equalities)
      var->value = var->equality->Function();

    double sqr_residual = pow(opt->Residual(), 2);

    if (opt->inequality != nullptr)
      if (opt->inequality->Function() == false)
        sqr_residual = 2.0e299;

    return { sqr_residual };
  }

  // Implementation of the box bounds.
  // get_bounds defined in pagmo.
  // no set method necessary as the fit instance can directly be used.
  std::pair<pagmo::vector_double, pagmo::vector_double> get_bounds() const
  {
    return { opt->lower_bounds, opt->upper_bounds };
  }

private:

  Optimizer* opt = nullptr;
};
#endif


#ifdef NEXUS_USE_PAGMO
void Optimizer::PagmoFit(const std::string method)
{
  pagmo::problem prob{ PagmoFunctorOptimizer(this) };

  pagmo::pop_size_t population = options->population;

  if (options->population == 0)
    population = 10 * (initial_parameters.size() + 1);

  unsigned int iterations = options->iterations;

  if (options->iterations == 0)
    iterations = 100;

  pagmo::algorithm algo;

  if (method == "BeeColony")
  {
    algo = pagmo::algorithm(pagmo::bee_colony(iterations));
  }
  else if (method == "CMA-ES")
  {
    algo = pagmo::algorithm(pagmo::cmaes(iterations, -1, -1, -1, -1, 0.5, options->function_tolerance, options->parameter_tolerance, false, true));
  }
  else if (method == "CompassSearch")
  {
    population = 1;

    if (options->iterations == 0)
      iterations = 500;

    algo = pagmo::algorithm(pagmo::compass_search(iterations, options->CompassSearch->start_range,
      options->CompassSearch->stop_range, options->CompassSearch->reduction_coeff));
  }
  else if (method == "DiffEvol")
  {
    algo = pagmo::algorithm(pagmo::de(iterations, options->DiffEvol->F, options->DiffEvol->CR,
      options->DiffEvol->variant, options->function_tolerance, options->parameter_tolerance));
  }
  else if (method == "PagmoDiffEvol")
  {
    algo = pagmo::algorithm(pagmo::de1220(iterations, pagmo::de1220_statics<void>::allowed_variants,
      options->DiffEvol->adaptive_variant, options->function_tolerance, options->parameter_tolerance));
  }
  else if (method == "AntColony")
  {
    if (population < 64)
      population = 64;

    algo = pagmo::algorithm(pagmo::gaco(iterations));
  }
  else if (method == "GreyWolf")
  {
    algo = pagmo::algorithm(pagmo::gwo(iterations));
  }
  else if (method == "SParticleSwarm")
  {
    algo = pagmo::algorithm(pagmo::nspso(iterations));
  }
  else if (method == "ParticleSwarm")
  {
    algo = pagmo::algorithm(pagmo::pso(iterations));
  }
  else if (method == "ParticleSwarmGen")
  {
    algo = pagmo::algorithm(pagmo::pso_gen(iterations));
  }
  else if (method == "AdaptiveDiffEvol")
  {
    algo = pagmo::algorithm(pagmo::sade(iterations, options->DiffEvol->variant,
      options->DiffEvol->adaptive_variant, options->function_tolerance, options->parameter_tolerance));
  }
  else if (method == "SimpleEvol")
  {
    if (options->population == 0)
      population = 20 * (initial_parameters.size() + 1);

    if (options->iterations == 0)
      iterations = 500;

    algo = pagmo::algorithm(pagmo::sea(iterations));
  }
  else if (method == "Genetic")
  {
    algo = pagmo::algorithm(pagmo::sga(iterations));
  }
  else if (method == "Annealing")
  {
    algo = pagmo::algorithm(pagmo::simulated_annealing());
  }
  else if (method == "NaturalEvol")
  {
    algo = pagmo::algorithm(pagmo::xnes(iterations, -1, -1, -1, -1, options->function_tolerance, options->parameter_tolerance, false, true));
  }
  else if (method == "BasinHopping")
  {
    if (options->BasinHopping->inner == "Subplex")
    {
      algo = pagmo::algorithm(pagmo::mbh(pagmo::nlopt("sbplx"), options->BasinHopping->stop,
        options->BasinHopping->perturbation, pagmo::random_device::next()));
    }
    else if (options->BasinHopping->inner == "Compass")
    {
      algo = pagmo::algorithm(pagmo::mbh(pagmo::compass_search(iterations, options->CompassSearch->start_range,
        options->CompassSearch->stop_range, options->CompassSearch->reduction_coeff),
        options->BasinHopping->stop, options->BasinHopping->perturbation, pagmo::random_device::next()));
    }
    else
    {
      python_print::output("Fit: Basin Hopping inner algorithm not recognized. Using Compass Search.");

      options->BasinHopping->inner = "Compass";

      algo = pagmo::algorithm(pagmo::mbh(pagmo::compass_search(iterations, options->CompassSearch->start_range,
        options->CompassSearch->stop_range, options->CompassSearch->reduction_coeff),
        options->BasinHopping->stop, options->BasinHopping->perturbation, pagmo::random_device::next()));
    }
  }

  // output every iteration or quiet
  algo.set_verbosity(options->output);

  // print start parameters
  if (method == "BasinHopping")
    python_print::output("  inner algorithm: " + options->BasinHopping->inner);

  python_print::output("  population: " + std::to_string(population));
  python_print::output("  iterations: " + std::to_string(iterations));

  // Instantiate a population
  pagmo::population pop{ prob, population };

  // Evolve the population
  pop = algo.evolve(pop);

  // calculate cost, comparable to ceres solver, see definition of cost with factor 1/2
  //const auto cost = prob.fitness(pop.champion_x())[0] / 2.0;
  cost = prob.fitness(pop.champion_x())[0] / 2.0;

  std::stringstream ss;
  ss << "\n  cost = " << std::scientific << cost;
  python_print::output(ss.str());

  // Set best parameters
  for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
    *fit_parameter_pointers[i] = pop.champion_x()[i];
}
#else
void Optimizer::PagmoFit(const std::string method)
{
  errors::WarningMessage("Fit", "Nexus was compiled without Pagmo support.");
}
#endif
