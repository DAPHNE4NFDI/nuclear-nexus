// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "measurement.h"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <numeric>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../nexus_definitions.h"
#include "../math/constants.h"
#include "../math/conversions.h"
#include "../math/fourier.h"
#include "../math/matrix.h"
#include "../utilities/errors.h"
#include "../functors/electronic.h"



void Measurement::Update() const
{
  experiment->Update();
}


void Measurement::PopulateOptimizerHandler(OptimizerHandler* optimizer_handler)
{
  experiment->PopulateFitHandler(optimizer_handler);
}


void Measurement::CheckIsotope(const std::string module_name) const
{
  if (experiment->isotope->isotope == "none")
    errors::WarningMessage(module_name, "Resonant method called but no isotope is defined in experiment");

  for (NxObject* const obj : experiment->objects)
  {
    if (const BasicSample* const sample = dynamic_cast<BasicSample*>(obj))
    {
      for (const auto lay : sample->layers)
      {
        if (lay->material->isotope->isotope == experiment->isotope->isotope &&
            lay->material->hyperfine_sites.size() == 0)
        {
          errors::WarningMessage(module_name, "No hyperfine site defined in Layer id: " + lay->id + "\n"
            + "          with resonant material id: " + lay->material->id);
        }
      }
    }
  }
}



void Measurement::CheckForSampleInExperiment(const std::string module_name, const BasicSample* const sample) const
{
  bool sample_found = false;

  for (const NxObject* const obj : experiment->objects)
  {
    if (obj == sample)
      sample_found = true;
  }

  if (!sample_found)
    errors::WarningMessage(module_name, ".sample not found in .experiment. Use a sample from the .objects list.");
}



void Measurement::CheckGrazingAngleSample(const std::string module_name, const GrazingSample* const sample) const
{
  if (sample->angle->value <= 0)
    errors::WarningMessage(module_name, "Sample.angle.value <= 0 in grazing geometry at sample.id " + sample->id);
}


void Measurement::CheckGrazingAngleExperiment(const std::string module_name) const
{
  for (NxObject* const obj : experiment->objects)
  {
    if (const GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
    {
      if (sample->angle->value <= 0)
        errors::WarningMessage(module_name, "Sample.angle.value <= 0 in grazing geometry at sample.id " + sample->id);
    }
  }
}


void Measurement::CheckWmatrixValiditySample(const std::string module_name, const GrazingSample* const grazing_sample, const double energy, double angle) const
{
  if (grazing_sample->roughness == "a")
  {
    if (angle == 0.0)
      angle = grazing_sample->angle->value;

    for (const Layer* const lay : grazing_sample->layers)
    {
      const double value = lay->KzSigma(energy, angle);
      
      if (value > 0.3)
      {
        std::string str = "";
        str += "Analytical roughness model of interface W matrix not valid! Output might be wrong!\n";
        str += "          At angle " + std::to_string(angle) + " and at energy " + std::to_string(energy) + ".\n";
        str += "          Wavevector kz * roughness.value = " + std::to_string(value) + " > 0.3 but should be << 1.\n";
        str += "          Encountered in Sample.id: " + grazing_sample->id + "  - Layer.id: " + lay->id + "  - Layer.roughness: " + std::to_string(lay->roughness->value);

        errors::WarningMessage(module_name, str);
      }

    }
  }
}


void Measurement::CheckWmatrixValidityExperiment(const std::string module_name, const double energy) const
{
  for (NxObject* const obj : experiment->objects)
  {
    if (const GrazingSample* const grazing_sample = dynamic_cast<GrazingSample*>(obj))
      CheckWmatrixValiditySample(module_name, grazing_sample, energy);
  }
}


void Measurement::Print(std::string module_name, std::vector<std::string> string_vector) const
{
  python_print::output("Module " + module_name + " message:");
  
  for (const std::string& str : string_vector)
    python_print::output(str);
}


std::vector<double> Measurement::PopulateDepth(const size_t num_points, const double thickness) const
{
  std::vector<double> depth_vector(num_points);

  const double thickness_step = thickness / (num_points - 1.0);

  for (size_t i = 0; i < num_points; ++i)
    depth_vector[i] = i * thickness_step;
 
  return depth_vector;
}


std::vector<double> Measurement::PopulateTime(const double timelength, const double timestep, const double offset) const
{
  const size_t num_timesteps = static_cast<size_t>(round(timelength / timestep) + 1);

  std::vector<double> time_vector(num_timesteps);

  for (size_t i = 0; i < num_timesteps; ++i)
    time_vector[i] = i * timestep + offset;

  return time_vector;
}


// Determine number of detuning points needed in energy space to represent the desired time setup.
// size optimization for FFT, fastest with powers of two, 2^n
size_t Measurement::NumberDetuningPoints(const double time_length, const double time_step, const double isotope_lifetime) const
{
  // the factor 11 is used to get a proper maximum step size in the detuning, which is smaller than 1 Gamma.
  const double minimum_time_length = isotope_lifetime * 1.0e9 * 11.0;  // time length in ns, life time in s to ns

  double FFT_time_length = time_length;

  if (FFT_time_length < minimum_time_length)
    FFT_time_length = minimum_time_length;

  const int num_points = static_cast<int>(round(FFT_time_length / time_step) + 1);

  const size_t num_detuning = static_cast<size_t>(pow(2, static_cast<int>(ceil(log2(num_points)))));  // round to get next full 2^n

  return num_detuning;
}


// Determine the detuning grid
// 
// The sampling frequency/energy for the FFT is given by the time step delta_t
// f_s = 1 / delta_t
// --> omega_s = 2pi/ delta_t
// --> E_s = 2pi * hbar / delta_t
// 
// in units of Gamma
// gamma = hbar / lifetime;  // gamma in eV, lifetime in s
// -->
// G_s = E_s / gamma
//     = 2pi * hbar / (delta_t * gamma)
//     = 2pi * hbar / (delta_t * hbar / isotope.lifetime)
//     = 2pi * isotope.lifetime / delta_t
//     = 2pi * isotope.lifetime * f_max
// 
// G_s is the detuning range for the specific time_step in units of Gamma.
// The detuning is then setup between -G_s/2 and G_s/2.
//
// The number of points used is determined from NumberDetuningPoints().
// 
// The frequency/energy step is
// delta_f = 1 / (N * delta_t)
// 
// delta_omega = 2pi * delta_f
// delta_E = 2pi * hbar * delta_f
// delta_Gamma = 2pi * hbar / gamma * delta_f  // E in units of Gamma
//             = 2pi * hbar / (hbar / isotope.lifetime) * delta_f
//             = 2pi * isotope.lifetime * delta_f
//
std::vector<double> Measurement::PopulateDetuning(const size_t num_detuning_points, const double time_step,
  const double max_detuning, const double isotope_lifetime) const
{
  const double detuning_sampling = 2.0 * constants::kPi * isotope_lifetime * fourier::SamplingFrequency(time_step * 1.0e-9);  // in units of Gamma

  const double detuning_step = 2.0 * constants::kPi * isotope_lifetime * fourier::Step(num_detuning_points, time_step * 1.0e-9);

  std::vector<double> detuning_vector{};

  if (max_detuning > 0.0)
  {
    for (size_t i = 0; i < num_detuning_points; ++i)
    {
      if (abs(-detuning_sampling / 2.0 + i * detuning_step) <= max_detuning)
        detuning_vector.push_back(-detuning_sampling / 2.0 + i * detuning_step);
    }
  }
  else
  {
    for (size_t i = 0; i < num_detuning_points; ++i)
      detuning_vector.push_back(-detuning_sampling / 2.0 + i * detuning_step);
  }

  return detuning_vector;
}


// Beam profiles from GenX
// sample_length in GenX is divided by 2 on input
double Measurement::BeamProfileCorrection(const double angle, const double sample_length,
  double beam_fwhm, const std::string beam_profile) const
{
  double correction = 1.0;

  const double half_sample_length = sample_length / 2.0;

  if (beam_fwhm <= 0.0)
    beam_fwhm = 1.0e-299;

  // Gaussian beam
  if (beam_profile == "g")
  {
    const double sigma = beam_fwhm / constants::kSigmaToFHWM;

    correction = erf(half_sample_length / constants::kSqrt2 / sigma * sin(angle * constants::kDegToRad));
  }
  // rectangular beam
  else if (beam_profile == "r")
  {
    const double a = half_sample_length * sin(angle * constants::kDegToRad) / (beam_fwhm / 2.0);

    if (a < 1.0 &&
        a >= 0.0)
    {
      correction *= a;
    }
  }

  return correction;
}


/*
Function to add the electronic scattering to the total scattering
*/
ExperimentMatrix2 Measurement::AddElectronic(const ExperimentMatrix2 matrix) const
{
  const AmplitudeMatrix electronic_matrix_functor(experiment, experiment->isotope->energy);

  const Eigen::Matrix2cd electronic_matrix = electronic_matrix_functor(false);

  return matrix::Add(matrix, electronic_matrix);
}

/*
Function to subtract the electronic scattering from the total scattering
in case only the nuclear part should be calculated
*/
ExperimentMatrix2 Measurement::SubtractElectronic(const ExperimentMatrix2 matrix) const
{
  const AmplitudeMatrix electronic_matrix_functor(experiment, experiment->isotope->energy);

  const Eigen::Matrix2cd electronic_matrix = electronic_matrix_functor(false);

  return matrix::Subtract(matrix, electronic_matrix);
}

/*
Function to perform a Fourier transform, set certain amplitudes outside a time window to zero, and transform back

Keep the electronic background.
*/
ExperimentMatrix2 Measurement::TimeGating(const ExperimentMatrix2 matrix, const std::vector<double> time_gate) const
{
  const size_t number_points = matrix.size();

  LayerMatrix2 fourier_matrix = fourier::FourierTransform(matrix, fourier::FourierScaling::none, "none");

  const double detuning_step = detuning.at(1) - detuning.front();

  // see Measurement::PopulateDetuning for scaling
  const double frequency_step = detuning_step / (2.0 * constants::kPi * experiment->isotope->lifetime);

  const double delta_t = fourier::Step(number_points, frequency_step);

  time.resize(number_points);

  for (size_t i = 0; i < number_points; ++i)
    time[i] = i * delta_t;

  const auto start_time_iterator = std::lower_bound(time.begin(), time.end(), time_gate.front() * 1.0e-9);  // to ns

  const auto distance_start = std::distance(time.begin(), start_time_iterator);

  for (auto it = fourier_matrix.begin(); it != fourier_matrix.begin() + distance_start; ++it)
    *it = Eigen::Matrix2cd::Zero();

  const auto stop_time_iterator = std::lower_bound(time.begin(), time.end(), time_gate.at(1) * 1.0e-9);  // to ns

  const auto distance_stop = std::distance(time.begin(), stop_time_iterator);

  for (auto it = fourier_matrix.begin() + distance_stop; it != fourier_matrix.end(); ++it)
    *it = Eigen::Matrix2cd::Zero();

  // transform back
  return fourier::FourierTransform(fourier_matrix, fourier::FourierScaling::N, "none", true);
}


std::vector<double> Measurement::Velocity() const
{
  std::vector<double> velocity(detuning.size(), 0.0);

  std::transform(NEXUS_EXECUTION_POLICY detuning.begin(), detuning.end(), velocity.begin(),
    [&](const double& det)
    {
      return conversions::GammaToVelocity(det, experiment->isotope->energy, experiment->isotope->gamma);
    }
  );

  return velocity;
}