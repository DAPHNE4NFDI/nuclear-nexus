// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.



#include "energy_spectrum.h"

#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/fourier.h"
#include "../math/convolution.h"
#include "../functors/electronic.h"


AmplitudeSpectrum::AmplitudeSpectrum(
  Experiment* const experiment_,
  const std::vector<double>& detuning_,
  const bool electronic_,
  const std::string id_,
  const std::vector<double> time_gate_
) :
  Measurement(experiment_, detuning_, id_),
  electronic(electronic_),
  time_gate(time_gate_)
{
  CheckIsotope("AmplitudeSpectrum");
 
  CheckGrazingAngleExperiment("AmplitudeSpectrum");
};


intensity::AmplitudeVector AmplitudeSpectrum::operator() (const bool update_and_calc_transitions, const bool check_W_matrix) const
{  
  if (update_and_calc_transitions)
    Update();

  if (check_W_matrix)
    CheckWmatrixValidityExperiment("AmplitudeSpectrum", experiment->isotope->energy);

  if (!(experiment->beam->ValidJonesVector()))
  {
    errors::ErrorMessage("AmplitudeSpectrum::operator()", "No valid Jones vector set in Beam.");
  }
 
  ExperimentMatrix2 experiment_matrix = experiment->Matrix(detuning, update_and_calc_transitions);

  if (electronic == false)
    experiment_matrix = SubtractElectronic(experiment_matrix);

  if (time_gate.size() == 2)
    experiment_matrix = TimeGating(experiment_matrix, time_gate);
  
  result = intensity::Amplitude(experiment_matrix, experiment->beam);

  return result;
}


EnergySpectrum::EnergySpectrum(
  Experiment* const experiment_,
  const std::vector<double>& detuning_,
  const bool electronic_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const resolution_,
  const unsigned int distribution_points_,
  const double fit_weight_,
  const std::string kernel_type_,
  Residual* const residual_,
  const std::vector<double> time_gate_,
  const bool coherence_,
  const std::string id_
) : 
  FitMeasurement(experiment_,
    detuning_,
    intensity_data_,
    distribution_points_,
    scaling_,
    background_,
    nullptr,
    intensity_data_.size(),
    fit_weight_,
    resolution_,
    nullptr,
    kernel_type_,
    residual_,
    coherence_,
    id_),
  electronic(electronic_),
  time_gate(time_gate_)
{
  if (intensity_data_.size() > 0 &&
      detuning_.size() != intensity_data_.size())
  {
    errors::WarningMessage("EnergySpectrum", "detuning and intensity_data not of same size.");
  }

  if (fit_weight_ < 0.0)
    errors::WarningMessage("EnergySpectrum", "fit_weight must be >= 0.");

  if (distribution_points_ < 1)
    errors::WarningMessage("EnergySpectrum", "distribution points must be > 0.");

  if (!(time_gate.size() == 2 || time_gate.size() == 0))
    errors::WarningMessage("EnergySpectrum", "time_gate size must be 0 or 2.");

  CheckIsotope("EnergySpectrum");
  
  if (id_ != "EnergySpecForNuclearReflectivity_75326") // identifier for nuclear reflectivity energy
    CheckGrazingAngleExperiment("EnergySpectrum");
};


// Non-optimized implementation for thickness distributions.
// Simply recalculate everything for each thickness combination.
// Transitions are only calculated once.
std::vector<double> EnergySpectrum::operator()(const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  if (distribution_points < 1)
    errors::WarningMessage("EnergySpectrum", "distribution points is" + std::to_string(distribution_points) + " but must be > 0.");

  if (update_and_calc_transitions)
    Update();

  if (check_W_matrix)
    CheckWmatrixValidityExperiment("EnergySpectrum", experiment->isotope->energy);

  if (coherence == true && !(experiment->beam->ValidJonesVector()))
    errors::ErrorMessage("EnergySpectrum::operator()", "No valid Jones vector set in Beam.");

  intensity::AmplitudeVector total_amplitude(detuning.size(), Eigen::Vector2cd::Zero());

  std::vector<double> total_intensity(detuning.size(), 0.0);

  experiment->ResetDistributionIndex();

  experiment->ResetDivergenceIndex();

  experiment->StoreForwardThicknesses();

  experiment->StoreGrazingAngles();

  const size_t total_distribution_combinations = experiment->PopulateThicknessDistribution(distribution_points);

  const size_t total_divergence_combinations = experiment->PopulateDivergenceDistribution(distribution_points);

  // incoherent summation over thickness and divergence distribution
  for (size_t j = 0; j < total_divergence_combinations; ++j)
  {
    const double weight_divergence = experiment->SetAngleAndGetWeight();

    for (size_t i = 0; i < total_distribution_combinations; ++i)
    {
      const double weight = weight_divergence * experiment->SetForwardThicknessAndGetWeight();

      ExperimentMatrix2 experiment_matrix = experiment->Matrix(detuning, update_and_calc_transitions);

      if (electronic == false)
        experiment_matrix = SubtractElectronic(experiment_matrix);

      if (time_gate.size() == 2)
        experiment_matrix = TimeGating(experiment_matrix, time_gate);

      if (coherence == false)
      {
        const std::vector<double> intensity = intensity::Intensity(experiment_matrix, experiment->beam);

        // incoherent sum of intensity
        std::transform(NEXUS_EXECUTION_POLICY total_intensity.begin(), total_intensity.end(), intensity.begin(), total_intensity.begin(),
          [weight](double& total_intens, const double& intens)
          {
            return total_intens + weight * intens;
          }
        );
      }
      else if (coherence == true)
      {
        intensity::AmplitudeVector amplitude = intensity::Amplitude(experiment_matrix, experiment->beam);

        // coherent sum of amplitudes
        std::transform(NEXUS_EXECUTION_POLICY total_amplitude.begin(), total_amplitude.end(), amplitude.begin(), total_amplitude.begin(),
          [weight](Eigen::Vector2cd& total_ampl, const Eigen::Vector2cd& ampl)
          {
            return total_ampl + weight * ampl;
          }
        );
      }

      experiment->IncreaseDistributionIndex(distribution_points);
    }

    experiment->ResetDistributionIndex();

    experiment->IncreaseDivergenceIndex(distribution_points);
  }

  if (coherence == true)
  {
    total_intensity = intensity::Intensity(total_amplitude);
  }

  experiment->ResetDistributionIndex();

  experiment->ResetDivergenceIndex();

  experiment->RecallForwardThicknesses();

  experiment->RecallGrazingAngles();

  if (resolution->value > 0.0)
  {
    if (kernel_type == "Lorentz")
    {
      resolution_kernel = convolution::LorentzianKernel(detuning, resolution->value);
    }
    else
    {
      resolution_kernel = convolution::GaussianKernel(detuning, resolution->value);
    }

    total_intensity = convolution::SameConvolution(total_intensity, resolution_kernel);
  }

  std::for_each(total_intensity.begin(), total_intensity.end(),
    [&](double& intens)
    {
      intens = background->value + scaling->value * intens;
    }
  );

  result = total_intensity;

  return total_intensity;
}

bool EnergySpectrum::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  const std::vector<double> intensity_fit = operator()(true, false);

  bootstrap_fit_result = intensity_fit;

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_fit);

  return true;
}

double EnergySpectrum::Residuals() const
{
  const std::vector<double> intensity_fit = operator()(true, false);

  bootstrap_fit_result = intensity_fit;
 
  return WeightedUserResidual(intensity_data, intensity_fit);
}


EmissionSpectrum::EmissionSpectrum(
  Experiment* const experiment_,
  const std::vector<double>& detuning_,
  const bool electronic_,
  const std::vector<double>& intensity_data_,
  Var* const scaling_,
  Var* const background_,
  Var* const resolution_,
  const unsigned int distribution_points_,
  const double fit_weight_,
  const std::string kernel_type_,
  Residual* const residual_,
  const std::vector<double> time_gate_,
  const bool coherence_,
  const std::string id_
) :
  EnergySpectrum(experiment_,
    detuning_,
    electronic_,
    intensity_data_,
    scaling_,
    background_,
    resolution_,
    distribution_points_,
    fit_weight_,
    kernel_type_,
    residual_,
    time_gate_,
    coherence_,
    id_)
{
  if (intensity_data_.size() > 0 &&
    detuning_.size() != intensity_data_.size())
  {
    errors::WarningMessage("EmissionSpectrum", "detuning and intensity_data not of same size.");
  }

  if (fit_weight_ < 0.0)
    errors::WarningMessage("EmissionSpectrum", "fit_weight must be >= 0.");

  if (distribution_points_ < 1)
    errors::WarningMessage("EmissionSpectrum", "distribution points must be > 0.");

  CheckIsotope("EmissionSpectrum");
};


std::vector<double> EmissionSpectrum::operator()(const bool update_and_calc_transitions, const bool check_W_matrix) const
{
  std::vector<double> total_intensity = EnergySpectrum::operator()(update_and_calc_transitions, check_W_matrix);

  // revert scaling of EnergySpectrum
  std::for_each(total_intensity.begin(), total_intensity.end(),
    [&](double& intens)
    {
      intens =  (intens - background->value) / scaling->value;
    }
  );

  // calculate electronic background for the fit
  const AmplitudeMatrix electronic_matrix_functor(experiment, experiment->isotope->energy);

  const double background_intensity = intensity::Intensity(electronic_matrix_functor(false), experiment->beam);

  for (double& element : total_intensity)
    element = -scaling->value * (element - background_intensity) + background->value;

  result = total_intensity;

  return total_intensity;
}

bool EmissionSpectrum::operator() (double const* const* parameters, double* residuals) const
{
  SetCeresFitParameterPointers(parameters);

  std::vector<double> intensity_fit = operator()(true, false);

  SetCeresWeightedResiduals(residuals, intensity_data, intensity_fit);

  return true;
}

double EmissionSpectrum::Residuals() const
{
  const std::vector<double> intensity_fit = operator()(true, false);

  return WeightedUserResidual(intensity_data, intensity_fit);
}
