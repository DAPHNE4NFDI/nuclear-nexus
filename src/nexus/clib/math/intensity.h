// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Calculations of the amplitudes and intensities in the Jones matrix formalism.
// Amplitudes are calculated from the Jones vector.
// Intensities are calculated from the coherency matrix.
// 
// The intensity after Wolfgang HI 125, Eq. 8 or Roehlsberger book Eq. 4.78 do not give the correct result
// for arbitrary sample and analyzer combinations in the experiment.
// 
// For more details see H. Egstrom, Appl. Optics 30, 1730 (1991) or A. Aiello et al., Phys. Rev. A 76, 032323 (2007)
// From the Jones formalism we have for the arbitrary objects M_i and Jones vector E 
// that the transmitted light is E_out = M_i * ... * M_j * E_in = M * E_in
// 
// For arbitrary light given by the coherency matrix J = E * adj(E)
// the transmitted coherency matrix is J_out = M * J_in * adj(M)
// So for arbitrary object order we get
// J_out = M_i * ... * M_j * J_in * adj(M_j) * ... * adj(M_i)


#ifndef NEXUS_INTENSITY_H_
#define NEXUS_INTENSITY_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/beam.h"
#include "../classes/analyzer.h"
#include "../scattering/layer_matrix.h"


namespace intensity {

  std::vector<double> Intensity(const std::vector<Complex>& ampltiude);

  typedef std::vector<Eigen::Vector2cd> AmplitudeVector;

  Eigen::Vector2cd Amplitude(const Eigen::Matrix2cd& layer_matrix, const Beam* const beam);

  AmplitudeVector Amplitude(const ExperimentMatrix2& layer_matrix, const Beam* const beam);

  double Intensity(const Eigen::Matrix2cd& layer_matrix, const Beam* const beam);

  std::vector<double> Intensity(const ExperimentMatrix2& layer_matrix, const Beam* const beam);

  std::vector<double> Intensity(const AmplitudeVector amplitude);

}  // namespace intensity


#endif // NEXUS_INTENSITY_H_
