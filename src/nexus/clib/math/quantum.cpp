// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "quantum.h"

#include <cmath>
#include <algorithm>

#include "../nexus_definitions.h"
#include "../math/constants.h"
#include "../utilities/errors.h"

namespace quantum {

  using namespace std::complex_literals;


  bool KroneckerDelta(const int i, const int j)
  {
    bool d = 0;

    if (i == j)
      d = 1;

    return d;
  }


  // Factorial(n), n >= 0, non-zero integer
  // int Factorial(const int n)
  unsigned long long Factorial(const int n)
  {
    if (n < 0 || n >= 20)
      return std::numeric_limits<unsigned long long>::quiet_NaN();

    constexpr short kCacheSize = 16;

    static constexpr unsigned long long factorial_cache[kCacheSize] = { 1, 1, 2, 6, 24, 120, 720, 5040,
                                                                       40320, 362880, 3628800, 39916800,
                                                                       479001600, 6227020800,
                                                                       87178291200, 1307674368000 };

    unsigned long long f = 1;

    if (n < kCacheSize)
    {
      f = factorial_cache[n];
    }
    else
    {
      f = factorial_cache[kCacheSize - 1];

      for (int i = kCacheSize; i <= n; i++)
        f *= i;
    }

    return f;
  }

  // alias for Factorial with double input and proper conversion
  unsigned long long Factorial(const double n)
  {
    const int n_int = static_cast<int>(round(n));

    return Factorial(n_int);
  }


  // Racah's formula Eq. (3.19), [Rose]_.
  double ClebshGordon(const double j1, const double j2, const double m1, const double m2, const double J, const double M)
  {
    const double epsilon = 0.0000001;

    //check that J satisfies the triangular condition |j1 - j2| <= J <= j1 + j2
    if (fabs(j1 - j2) - J > epsilon)
    {
      return 0.0;
    }
    else if (J - (j1 + j2) > epsilon)
    {
      return 0.0;
    }

    //check that m's are restricted to j's range
    else if (fabs(m1) - j1 > epsilon)
    {
      return 0.0;
    }
    else if (fabs(m2) - j2 > epsilon)
    {
      return 0.0;
    }
    else if (fabs(M) - J > epsilon)
    {
      return 0.0;
    }
    // Kronecker delta M, m1+m2
    else if (fabs(m1 + m2 - M) > epsilon)
    {
      return 0.0;
    }

    // summation over k
    // sum runs over all integers k such that none of the Factorial arguments is negative
    // so find min and max of k sum values
    /*
    j1 + j2 - J - k >= 0, so j1 + j2 + J >= k
    j1 - m1 - k >= 0, so j1 - m1 >= k
    j2 + m2 - k >= 0, so j2  +m2 >= k

    J - j2 + m1 + k >= 0, so k >= -(J - j2 + m1)
    J - j1 - m2 + k >= 0, so k >= -(J - j1 - m2)
    and k >= 0
    --->
    kmin = max(-(J - j2 + m1), -(J - j1 - m2), 0)
    kmax = min(j1 + j2 - J, j1 - m1, j2 + m2)
    */
    const int lowerbound[] = { static_cast<int>(round(-(J - j2 + m1))), static_cast<int>(round(-(J - j1 - m2))), 0 };
    const int upperbound[] = { static_cast<int>(round(j1 + j2 - J)), static_cast<int>(round(j1 - m1)), static_cast<int>(round(j2 + m2)) };

    const int k_min = *std::max_element(lowerbound, lowerbound + 3);
    const int k_max = *std::min_element(upperbound, upperbound + 3);

    double clebsh_gordon = 0.0;

    for (int k = k_min; k <= k_max; k++)
    {
      clebsh_gordon += pow(-1, k) / static_cast<double>(Factorial(k) * Factorial(j1 + j2 - J - k) * Factorial(j1 - m1 - k)
        * Factorial(j2 + m2 - k) * Factorial(J - j2 + m1 + k) * Factorial(J - j1 - m2 + k));
    }

    clebsh_gordon *= sqrt((2 * J + 1)
      * Factorial(j1 + j2 - J) * Factorial(J + j1 - j2) * Factorial(J - j1 + j2) / Factorial(j1 + j2 + J + 1)
      * Factorial(j1 + m1) * Factorial(j1 - m1) * Factorial(j2 + m2) * Factorial(j2 - m2) * Factorial(J + M) * Factorial(J - M));

    return clebsh_gordon;
  }


  double ThreeJSymbol(const double j1, const double j2, const double j3, const double m1, const double m2, const double m3)
  {
    return pow(-1, j1 - j2 - m3) / sqrt(2 * j3 + 1) * ClebshGordon(j1, j2, m1, m2, j3, -1 * m3);
  }


  // Eq. (3.20), [Rose]_ and Racah Theory of Complex Spectra
  double VCoefficient(const double j1, const double j2, const double j3, const double m1, const double m2, const double m3)
  {
    return pow(-1, -j3 + m3) / sqrt(2 * j3 + 1) * ClebshGordon(j1, j2, m1, m2, j3, -1 * m3);
  }


  // Eq. (6.8), [Rose]_.
  double TriangleCoefficient(const double a, const double b, const double c)
  {
    const double u = static_cast<double>(Factorial(a + b - c));
    const double v = static_cast<double>(Factorial(a - b + c));
    const double w = static_cast<double>(Factorial(-a + b + c));
    const double z = static_cast<double>(Factorial(a + b + c + 1));

    double tc = 0.0;

    if (z > 0)
      tc = sqrt(u * v * w / z);

    return tc;
  }


  // after [Rose]_.
  double WCoefficient(const double a, const double b, const double c, const double d, const double e, const double f)
  {
    const double a_1 = a + b + e;
    const double a_2 = c + d + e;
    const double a_3 = a + c + f;
    const double a_4 = b + d + f;
    const double b_1 = a + b + c + d;
    const double b_2 = a + d + e + f;
    const double b_3 = b + c + e + f;

    const int lowerbound[] = { static_cast<int>(round(a_1)), static_cast<int>(round(a_2)), static_cast<int>(round(a_3)), static_cast<int>(round(a_4)) };
    const int upperbound[] = { static_cast<int>(round(b_1)), static_cast<int>(round(b_2)), static_cast<int>(round(b_3)) };

    const int z_min = *std::max_element(lowerbound, lowerbound + 3);
    const int z_max = *std::min_element(upperbound, upperbound + 3);

    double w_coeff = 0.0;

    for (int z = z_min; z <= z_max; z++)
    {
      w_coeff += pow(-1, z + b_1) * Factorial(z + 1) /
        static_cast<double>(Factorial(z - a_1) * Factorial(z - a_2) * Factorial(z - a_3) * Factorial(z - a_4) * Factorial(b_1 - z) * Factorial(b_2 - z) * Factorial(b_3 - z));
    }

    constexpr auto TC = TriangleCoefficient;

    w_coeff *= TC(a, b, e) * TC(c, d, e) * TC(a, c, f) * TC(b, d, f);

    return w_coeff;
  }

  // After Racah
  double SixJSymbol(const double a, const double b, const double c, const double d, const double e, const double f)
  {
    return pow(1, a + b + d + e) * WCoefficient(a, b, e, d, c, f);
  }


  // Eq. (4.13), [Rose]_.
  double SmallWignerDmatrix(const double j, const double m1, const double m2, const double beta)
  {
    if (fabs(m1) > j ||
      fabs(m2) > j ||
      j <= 0)
    {
      return 0.0;
    }

    // The sum over s is over such values that the Factorials are non-negative
    // consitions for s: smin = max(0, m1-m2) and smax = min(j+)
    const int lowerbound[] = { 0, static_cast<int>(round(m2 - m1)) };
    const int upperbound[] = { static_cast<int>(round(j + m2)), static_cast<int>(round(j - m1)) };

    const int s_min = *std::max_element(lowerbound, lowerbound + 2);
    const int s_max = *std::min_element(upperbound, upperbound + 2);

    double d = 0.0;

    for (int s = s_min; s <= s_max; s++)
    {
      //d += pow(-1, m1-m2+s) ...// this is for +sin(beta/2) version of formula, not used here
      d += pow(-1, s) * pow(cos(beta / 2), 2 * j + m2 - m1 - 2.0 * s) * pow(-sin(beta / 2), m1 - m2 + 2.0 * s)
        / static_cast<double>(Factorial(s) * Factorial(j - m1 - s) * Factorial(j + m2 - s) * Factorial(m1 - m2 + s));
    }

    d *= sqrt(Factorial(j + m2) * Factorial(j - m2) * Factorial(j + m1) * Factorial(j - m1));

    return d;
  }


  // Eq. (4.12), [Rose]_.
  Complex WignerDmatrix(const double j, const double m1, const double m2, const double alpha, const double beta, const double gamma)
  {
    return exp(-1.0i * m1 * alpha) * SmallWignerDmatrix(j, m1, m2, beta) * exp(-1.0i * m2 * gamma);
  }


  // polynomials - for clang they return NaN

  double Laguerre(const int n, const double x)
  {
#ifndef NEXUS_WITH_CLANG
    if (n < 0)
      return std::numeric_limits<double>::quiet_NaN();

    return std::laguerre(n, x);
#else
    errors::WarningMessage("Quantum", "Function not supported with Apple clang compiler.");

    return std::numeric_limits<double>::quiet_NaN();
#endif
  }


  double AssociatedLaguerre(const int n, const int k, const double x)
  {
#ifndef NEXUS_WITH_CLANG
    if (n < 0 || k < 0)
      return std::numeric_limits<double>::quiet_NaN();

    return std::assoc_laguerre(n, k, x);
#else
    errors::WarningMessage("Quantum", "Function not supported with Apple clang compiler.");

    return std::numeric_limits<double>::quiet_NaN();
#endif
  }


  double Legendre(const int n, const double x)
  {
#ifndef NEXUS_WITH_CLANG
    if (n < 0)
      return std::numeric_limits<double>::quiet_NaN();

    return std::legendre(n, x);
#else
  errors::WarningMessage("Quantum", "Function not supported with Apple clang compiler.");

  return std::numeric_limits<double>::quiet_NaN();
#endif
  }


  double AssociatedLegendre(const int l, const int m, const double x, const bool csp)
  {
#ifndef NEXUS_WITH_CLANG
    if (l < 0 || m < 0)
      return std::numeric_limits<double>::quiet_NaN();
    
    double cs_phase = 1.0;

    if (csp)
      cs_phase = pow(-1, m);

    return cs_phase * std::assoc_legendre(l, m, x);
#else
    errors::WarningMessage("Quantum", "Function not supported with Apple clang compiler.");

    return std::numeric_limits<double>::quiet_NaN();
#endif
  }


  double Hermite(const int n, const double x)
  {
#ifndef NEXUS_WITH_CLANG
    if (n < 0)
      return std::numeric_limits<double>::quiet_NaN();

    return std::hermite(n, x);
#else
    errors::WarningMessage("Quantum", "Function not supported with Apple clang compiler.");

    return std::numeric_limits<double>::quiet_NaN();
#endif
  }


  Complex SphericalHarmonics(const int l, const int m, const double theta, const double phi, const bool csp, const bool racah_norm)
  {
#ifndef NEXUS_WITH_CLANG
    if (l < 0 || m < 0)
      return std::numeric_limits<double>::quiet_NaN();

    double norm = sqrt( Factorial(l - m) / static_cast<double>(Factorial(l + m)) );

    if (!racah_norm)
      norm *= sqrt( (2.0 * l + 1.0) / (4.0 * constants::kPi) );

    return norm * AssociatedLegendre(l, m, cos(theta), csp) * exp(1.0i * static_cast<double>(m) * phi);
#else
  errors::WarningMessage("Quantum", "Function not supported with Apple clang compiler.");

  return std::numeric_limits<double>::quiet_NaN();
#endif
  }


  VecSpherHarm VecSpherHarmPolarization(const int L, const int lambda, const coordinates::Euler euler)
  {
    VecSpherHarm YLM {};

    VecSpherHarmPolBase YLMcomponents;

    const double scaling = 0.25 * sqrt((2.0 * L + 1.0) / constants::kPi);

    for (int M = -L; M <= L; M++)
    {
      const Complex D1 = WignerDmatrix(L, 1, M, euler.alpha, euler.beta, euler.gamma);
      const Complex D2 = WignerDmatrix(L, -1, M, euler.alpha, euler.beta, euler.gamma);

      YLMcomponents.sigma = -scaling * (D1 - pow(-1, lambda + 1) * D2);
      YLMcomponents.pi = -1.0i * scaling * (D1 - pow(-1, lambda + 2) * D2);

      YLM.push_back(YLMcomponents);
    }

    return YLM;
  }

}   // namespace quantum
