// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of a general templated interpolation function.
// header only file.


#ifndef NEXUS_INTERPOLATE_H_
#define NEXUS_INTERPOLATE_H_

#include <vector>

#include "../nexus_definitions.h"


namespace interpolate {

  // input x_data and y_data, return the linear interpolated value of y_data value at x_value
  // x_data and y_data must be of same length
  template <typename T>
  inline T InterpolateLinear(const std::vector<double>& x_data, const std::vector<T>& y_data,
    const double x_value, const bool extrapolate)
  {
    size_t size = x_data.size();

    size_t i = 0;

    if (x_value >= x_data[size - 2])
    {
      i = size - 2;
    }
    else
    {
      while (x_value > x_data[i + 1])
        i++;
    }

    const double x_left = x_data[i];

    const double x_right = x_data[i + 1];

    T y_left = y_data[i];

    T y_right = y_data[i + 1];

    if (!extrapolate)
    {
      if (x_value < x_left)
        y_right = y_left;

      if (x_value > x_right)
        y_left = y_right;
    }

    const T gradient = (y_right - y_left) / (x_right - x_left);

    const T result = y_left + gradient * (x_value - x_left);

    return result;
  }

}  // namespace interpolate

#endif // NEXUS_INTERPOLATE_H_
