// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module transitions

%{
    #define SWIG_FILE_WITH_INIT
    #include "transitions.h"
%}


%feature("autodoc", "List of :class:`Transition` objects.") std::vector<transitions::Transition>;

%template(Transitions) std::vector<transitions::Transition>;

%template(NuclearCurrents) std::vector<transitions::NuclearCurrent>;



%include "transitions.h"



%extend transitions::Transition{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr != "this" and not hasattr(self, attr):
      raise Exception("Transition has no attribute {}".format(attr))

    super().__setattr__(attr, val) 

  def __repr__(self):
    output = "\nweight = {}\n".format(self.weight)
    output += " .energy_detuning = {}\n".format(self.energy_detuning)
    output += " .transition_polarisation_matrix:\n{}\n".format(self.transition_polarisation_matrix)
    output += " .lamb_moessbauer = {}\n".format(self.lamb_moessbauer)
    output += " .broadening = {}\n".format(self.broadening)
    return output

  }
}

%extend transitions::NuclearCurrent{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr != "this" and not hasattr(self, attr):
      raise Exception("NuclearCurrent has no attribute {}".format(attr))

    super().__setattr__(attr, val) 

  def __repr__(self):
    output = "\nweight = {}\n    ".format(self.weight)
    output += "energy_detuning = {}\n".format(self.energy_detuning)
    output += "nuclear_current: \n{}\n".format(self.nuclear_current)
    output += " .lamb_moessbauer = {}\n".format(self.lamb_moessbauer)
    return output
  
  }
}
