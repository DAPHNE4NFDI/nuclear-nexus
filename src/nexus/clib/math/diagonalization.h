// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of an Eigen-system (eigenvalues and eigenvectors) and the diagonalization function for Hermitian matrices.
// Implementation with Eigen.


#ifndef NEXUS_DIAGONALIZATION_H_
#define NEXUS_DIAGONALIZATION_H_

#include <Eigen/Dense>

#include "../nexus_definitions.h"


namespace diagonalization {

  /**
  Constructor for the :class:`Eigensystem` class. Stores the eigenvalues and the eigenvectors.

  Attributes:
     eigenvalues (ndarray): List of eigenvalues.
     eigenvectors (ndarray): List of eigenvectors.
  */
  struct Eigensystem {
    Eigen::VectorXd eigenvalues;

    Eigen::MatrixXcd eigenvectors;
  };

  /**
  Diagonalizes a hermitian matrix. Used to diagonalize the nuclear Hamiltonian. See Eq. (14) [Sturhahn]_.

  Args:
     matrix (ndarray): Hermitian matrix to diagonalize.

  Returns:
     :class:`Eigensystem`: Eigenvalues and eigenvectors of the matrix.
  */
  Eigensystem DiagonalizeHermitian(const Eigen::MatrixXcd& matrix);

}  // namespace diagonalization

#endif // NEXUS_DIAGONALIZATION_H_
