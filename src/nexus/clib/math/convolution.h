// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Convolution implementations and their kernels with specific optimizations.


#ifndef NEXUS_CONVOLUTION_H_
#define NEXUS_CONVOLUTION_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"


namespace convolution {

  // kernel definitions
  std::vector<double> GaussianKernel(const std::vector<double>& grid, const double fwhm, const bool truncate = true);

  std::vector<double> LorentzianKernel(const std::vector<double>& grid, const double fwhm, const bool truncate = true);

  std::vector<std::vector<double>> GaussianKernel2D(const std::vector<double>& grid_x, double fwhm_x,
    const std::vector<double>& grid_y, double fwhm_y, const bool truncate = true);

  // convolution functions
  std::vector<double> SameConvolution(std::vector<double> data, std::vector<double> kernel);

  std::vector<std::vector<double>> SameConvolution2D(std::vector<std::vector<double>> data, std::vector<std::vector<double>> kernel);

  // somethings wrong in the FFT, the position of the objects is shifted strangely in one direction.
  // Optimized SameConvolution is faster for typical kernel sizes anyway of experiments.
  // So I didn't checked the error any further.
  // std::vector<std::vector<double>> SameConvolutionFFT2D(const std::vector<std::vector<double>>& data,
  //  const std::vector<std::vector<double>>& kernel);
  
}  // namespace convolution

#endif // NEXUS_CONVOLUTION_H_
