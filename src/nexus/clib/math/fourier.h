// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Module for the Fourier transform implementation
// uses Pocket FFT, header only version
// 
// see e.g.
// L. Tan and J. Jiang, "Digital Signal Processing: Fundamentals and Applications", Academic Press, Amsterdam, The Netherlands, 2nd edition, 2013
// G.Heinzel et al., "Spectrum and spectral density estimation by the Discrete Fourier transform(DFT), including a comprehensive list of window functions and some new at - top windows."
// J. Ahrens et al., "Tutorial on Scaling of the Discrete Fourier Transform and the Implied Physical Units of the Spectra of Time - Discrete Signals",
//     Audio Engineering Society, Convention e - Brief 56.
// 
// The normalization of the DFT/FFT is not unique.
// Several definitions exist.
// 
// Parseval's Theorem states: 
// 
//     Int |x(t)|^2 dt= 1/2pi Int |X(omega)|^2 domega = Int |X(2pi*f)|^2 df
// 
// and for the DFT:
// 
//     Sum_(k=0)^(N-1) |x(t)_k|^2 = 1/N * Sum_(n=0)^(N-1) |X(f)_n|^2
// 
// normalization in PocketFFT with FourierScaling::SqrtN ensures Parseval's theorem.


#ifndef NEXUS_FOURIER_H_
#define NEXUS_FOURIER_H_

#include <cmath>
#include <numeric>

#include "../nexus_definitions.h"
#include "../math/constants.h"


namespace fourier {

  /**
  The sampling frequency :math:`f_s` for the DFT is given by the inverse of the step size in the base space.

  .. math:: f_s = \frac{1}{orig\_step}

  Args:
    orig_step (float): original step size in the base space.
    
  Returns:
    float: sampling frequency for the DFT.
  */
  inline double SamplingFrequency(const double orig_step)
  {
    return 1.0 / orig_step;
  }

  /**
  The frequency step :math:`\Delta f` for the DFT is given by

  .. math:: \Delta f = \frac{f_s}{N} = \frac{1}{N * orig\_step}

  Args:
    num_points (int): number of points of the original base space.
    orig_step (float): original step size in the base space.

  Returns:
    float: step size for the Fourier transform.
  */
  inline double Step(const size_t num_points, const double orig_step)
  {
    return  1.0 / (num_points * orig_step);
  }

  /**
  The resolution of the DFT is the smallest visible frequency.
  Same as the frequency step.

  .. math:: \Delta f = \frac{f_s}{N} = \frac{1}{N * orig\_step}.

  Args:
    num_points (int): number of points of the original base space.
    sampling_frequency (float): sampling frequency of the Fourier transform.

  Returns:
    float: Resolution of the DFT.
  */
  inline double Resolution(const size_t num_points, const double sampling_frequency)
  {
    return sampling_frequency / num_points;
  }

  // Normalization factors
  /**
  Normalization factor :math:`\frac{1}{N}`.
  Used for Intensities to ensures Parseval's theorem.

  Args:
    num_points (int): number of points of the original base space.

  Returns:
    float: Normalization factor.
  */
  inline double NormalizationN(const int num_points)
  {
    return 1.0 / num_points;
  }

  /**
  Normalization factor :math:`\frac{1}{\sqrt{N}}`.
  Used for amplitudes to ensures Parseval's theorem.

  Args:
    num_points (int): number of points of the original base space.

  Returns:
    float: Normalization factor.
  */
  inline double NormalizationSqrtN(const int number_points)
  {
    return 1.0 / sqrt(number_points);
  }

  // Normalization for original-space-integrated intensity
  // normalization obeys Parseval's theorem
  // 
  //    orig_step * Sum Sum_k^(N-1) |x(orig_step)_k|^2 = orig_step/N * Sum_n^(N-1) |x(fourier_step)_n|^2
  //
  /**
  Normalization for original-space-integrated intensity :math:`\frac{orig\_step}{N}`.
  Obeys Parseval's theorem.
  Relates the Fourier components via

  .. math:: orig\_step * \sum_{k=0}^{N-1} \left| x(orig\_step)_k \right|^2 = \frac{orig\_step}{N} * \sum_{n=0}^{N-1} \left| X(fourier\_step)_n \right|^2

  Args:
    num_points (int): number of points of the original base space.
    orig_step (float): original step size in the base space.

  Returns:
    float: Normalization factor.
  */
  inline double NormalizationNIntegratedIntensity(const int number_points, const double orig_step)
  {
    return orig_step / number_points;
  }

  // Normalization for original-space-integrated intensity, scaled by Fourier step
  // 
  //    orig_step / N / fourier_step * Sum_n^(N-1) |x(fourier_step)_n|^2
  // 
  // This normalization factor is scaled by the Fourier step in order to compensate for the changing sampling rate (with changing N or time_step),
  // for which each bin represents a changing Fourier band.
  // similar to the scaling of a power density spectrum.
  /**
  Normalization for original-space-integrated intensity scaled by Fourier step size :math:`\frac{step\_size}{N * fourier\_step}`.
  This normalization factor is scaled by the Fourier step in order to compensate for the changing sampling rate (with changing N or time_step), for which each bin represents a changing Fourier band.
  Similar to the scaling of a power density spectrum.

  .. math:: \frac{orig\_step}{fourier\_step} * \sum_{k=0}^{N-1} \left| x(orig\_step)_k \right|^2 = \frac{orig\_step}{N * fourier\_step} * \sum_{n=0}^{N-1} \left|X(fourier\_step)_n \right|^2

  Args:
    num_points (int): number of points of the original base space.
    orig_step (float): original step size in the base space.
    fourier_step (float): step size in the Fourier space.

  Returns:
    float: Normalization factor.
  */
  inline double NormalizationNIntegratedIntensityScaled(const int number_points, const double orig_step, const double fourier_step)
  {
    return orig_step / (number_points * fourier_step);
  }

  // Normalization for original-space-integrated intensity, scaled by Fourier step
  // assumes that the Fourier coefficients are already normed by 1/sqrt(N)
  // normalization does not obey Parseval's theorem
  // 
  //    orig_step / fourier_step * Sum_n^(N-1) |1/sqrt(N) * x(fourier_step)_n|^2
  // 
  // This normalization factor is scaled by the Fourier step in order to compensate for the changing sampling rate (with changing N or time_step),
  // for which each bin represents a changing Fourier band.
  // similar to the scaling of a power density spectrum.
  /**
  Normalization for original-space-integrated intensity scaled by Fourier step size :math:`\frac{orig\_step}{fourier\_step}`.
  Assumes that the Fourier coefficients are already normalized by :math:`\frac{1}{\sqrt{N}}`.
  Normalization does not obey Parseval's theorem.
  This normalization factor is scaled by the Fourier step in order to compensate for the changing sampling rate (with changing N or time_step), for which each bin represents a changing Fourier band.
  Similar to the scaling of a power density spectrum.

  .. math:: \frac{orig\_step}{fourier\_step} * \sum_{k=0}^{N-1} \left| x(orig\_step)_k \right|^2 = \frac{orig\_step}{fourier\_step} * \sum_{n=0}^{N-1} \left| \frac{1}{\sqrt{N}} X(fourier\_step)_n \right|^2

  .. versionadded:: 1.0.4

  Args:
    orig_step (float): original step size in the base space.
    fourier_step (float): step size in the Fourier space.

  Returns:
    float: Normalization factor.
  */
  inline double NormalizationIntegratedIntensityScaled(const double orig_step, const double fourier_step)
  {
    return orig_step / fourier_step;
  }


  // Fourier transform scaling for PocketFFT
  // none: no scaling, y(k)
  // N: 1/N scaling, y_(k) = 1/N * y(k)
  // SqrtN: 1/sqrt(N) scaling, y_(k) = 1/sqrt(N) * y(k)
  enum class FourierScaling { none, N, SqrtN };

  // Fourier transform via PockettFFT
  // y(k) = Sum_(n=0)^(N-1) x(n) * exp(-i*2pi* nk/N)
  // k = 0, ..., N-1
  // 
  // norm defines the amplitude normalization
  // window defines the Window function
  //
  // windowing the padded function slightly improves the undesired peak t time zero when max_detunning is low.
  // 
  // Windowing before padding:
  // At larger max_detunings the errors due to padding are small
  // At lower max_detunig the padding creates a peak, while windowing underestimates the intensity at low times.
  //
  // If direction is backward the scaling should be changed to N.
  std::vector<Eigen::Matrix2cd> FourierTransform(const std::vector<Eigen::Matrix2cd>& layer_matrix, const FourierScaling norm, const std::string window, const bool backward = false);

  /**
  Creates a Welch window.

  .. versionadded:: 1.0.4

  Args:
    num_points (int): number of points.
  
  Returns:
    list: Welch window.
  */
  std::vector<double> Welch(const size_t num_points);

  /**
  Creates a Sine window.

  .. versionadded:: 1.0.4

  Args:
    num_points (int): number of points.
  
  Returns:
    list: Sine window.
  */
  std::vector<double> Sine(const size_t num_points);

  /**
  Creates a Hann window.

  .. versionadded:: 1.0.4

  Args:
    num_points (int): number of points.
  
  Returns:
    list: Hann window.
  */
  std::vector<double> Hann(const size_t num_points);

  /**
  Creates a Hamming window.

  .. versionadded:: 1.0.4

  Args:
    num_points (int): number of points.
  
  Returns:
    list: Hamming window.
  */
  std::vector<double> Hamming(const size_t num_points);

  /**
  Creates a Kaiser window.

  .. versionadded:: 1.0.4

  Args:
    num_points (int): number of points.
    alpha (float): alpha value.
  
  Returns:
    list: Kaiser window.
  */
  std::vector<double> Kaiser(const size_t num_points, const double alpha);

}  // namespace fourier

#endif // NEXUS_FOURIER_H_
