// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the nuclear Hamiltonian in matrix form after W. Sturhahn, PRB 49, 9285 (1994), Eq. (12).
// The quantization axis is along wave vector of X-rays k
// k is along z, sigma is along x, pi is along y in the laboratory coordinate system [x,y,z] = [sigma, pi, k].
// This is chosen so one does not have to change the axis of the magnetic field and EFG with respect to the internal coordinate system additionally.
// Especially, in distributions this gets nasty. Keep the structure clear.
//
// functions return energy in units of the natural isotopes linewidth (isotope.gamma).


#ifndef NEXUS_HAMILTONIAN_H_
#define NEXUS_HAMILTONIAN_H_

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/bare_hyperfine.h"
#include "../classes/moessbauer.h"


namespace hamiltonian {

  /**
  Calculates the matrix Hamiltonian :math:`H` of the ground state in the basis of the nuclear spin operator, after Eq. (12) [Sturhahn]_.
  With a ground state spin of :math:`J` one obtains a square matrix of dimension :math:`2J+1`.
  The matrix elements :math:`H_{m',m}` are in the range from :math:`m = -J,...,J`.
  In general, the matrix must be diagonalized in order to find the energy detuning (eigenvalues) in units of the linewidth (:math:`\Gamma`).

  Args:
     hyperfine (:class:`BareHyperfine`): Hyperfine parameters acting on the nucleus.
     isotope (:class:`MoessbauerIsotope`): Moessbauer isotope on which the hyperfine parameters act.

  Returns:
     ndarray: Complex matrix Hamiltonian. Energy in units of the linewidth (:math:`\Gamma`). 
  */
  Eigen::MatrixXcd HamiltonianGroundState(const BareHyperfine& hyperfine, const MoessbauerIsotope* const isotope);

  /**
  Calculates the matrix Hamiltonian :math:`H` of the excited state in the basis of the nuclear spin operator, after Eq. (12) [Sturhahn]_.
  With an excited state spin of :math:`J` one obtains a square matrix of dimension :math:`2J+1`.
  The matrix elements :math:`H_{m',m}` are in the range from :math:`m = -J,...,J`.
  In general, the matrix must be diagonalized in order to find the energy detuning (eigenvalues) in units of the linewidth (:math:`\Gamma`).

  Args:
     hyperfine (:class:`BareHyperfine`): Hyperfine parameters acting on the nucleus.
     isotope (:class:`MoessbauerIsotope`): Moessbauer isotope on which the hyperfine parameters act.

  Returns:
     ndarray: Complex matrix Hamiltonian. Energy in units of the linewidth (:math:`\Gamma`).
  */
  Eigen::MatrixXcd HamiltonianExcitedState(const BareHyperfine& hyperfine, const MoessbauerIsotope* const isotope);

}  // namspace hamiltonian

#endif // NEXUS_HAMILTONIAN_H_
