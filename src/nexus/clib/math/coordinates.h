// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Various coordinate systems and coordinate transformations.
// Random numbers and optimized average functions in the coordinate systems


#ifndef NEXUS_COORDINATES_H_
#define NEXUS_COORDINATES_H_

#include "../nexus_definitions.h"
#include "../math/constants.h"


namespace coordinates {

  struct Polar;

  struct Spherical;

  struct Cartesian2D {
    Cartesian2D(
      const double x = 0.0,
      const double y = 0.0
    ) :
      x(x),
      y(y)
    {}

    Cartesian2D operator+(const Cartesian2D& a);

    Cartesian2D operator-(const Cartesian2D& a);

    Cartesian2D operator*(const double& a);

    Cartesian2D operator/(const double& a);

    Polar ToPolar() const;

    void Normalize();

    Eigen::Vector2d ToEigen() const;

    Cartesian2D Mirror(const Cartesian2D& mirror) const;

    double x = 0.0;
    double y = 0.0;
  };

  struct Cartesian3D {
    Cartesian3D(
      const double x = 0.0,
      const double y = 0.0,
      const double z = 0.0
    ) :
      x(x),
      y(y),
      z(z)
    {}

    Cartesian3D operator+(const Cartesian3D& a);

    Cartesian3D operator-(const Cartesian3D& a);

    Cartesian3D operator*(const double& a);

    Cartesian3D operator/(const double& a);

    Spherical ToSpherical() const;

    void Normalize();

    Eigen::Vector3d ToEigen() const;

    Cartesian3D Mirror(const Cartesian3D& mirror) const;

    double x = 0.0;
    double y = 0.0;
    double z = 0.0;
  };


  // Polar
  // angles in rad
  struct Polar {
    Polar(
      const double r = 0.0,
      const double phi = 0.0
    ) :
      r(r),
      phi(phi)
    {}

    Cartesian2D ToCartesian() const;

    void Normalize();

    Eigen::Vector2d ToEigen() const;

    double r = 0.0;
    double phi = 0.0;
  };


  // Spherical
  // angles in rad
  struct Spherical {
    Spherical(
      const double r = 0.0,
      const double theta = 0.0,
      const double phi = 0.0
    ) :
      r(r),
      theta(theta),
      phi(phi)
    {};

    Cartesian3D ToCartesian() const;
    
    void Normalize();

    Eigen::Vector3d ToEigen() const;

    double r = 0.0;
    double theta = 0.0;
    double phi = 0.0;
  };

  /**
  Constructor for :class:`Eulerangles` class.
  Euler angles in ZYZ convention.

  Args:
    alpha (float): :math:`\alpha` in rad.
    beta (float): :math:`\beta` in rad.
    gamma (float): :math:`\gamma` in rad.

  Attributes:
    alpha (float): :math:`\alpha` in rad.
    beta (float): :math:`\beta` in rad.
    gamma (float): :math:`\gamma` in rad.
  */
  struct Euler {
    Euler(
      const double alpha = 0.0,   // angles in rad
      const double beta = 0.0,
      const double gamma = 0.0
    ) :
      alpha(alpha),
      beta(beta),
      gamma(gamma)
    {};


    /*
    Euler angles from rotation in z - y - z notation from a rotation matrix
    Eigen provides a module for Euler angles but for arbitrary rotation combinations
    here only the Z-Y-Z convention is need as defined in M.E. Rose, page 50
    vector(0,1,2) = vector(alpha, beta, gamma)
    matrix is a 3x3 rotation matrix
    */
    void FromMatrix(const Eigen::Matrix3d& matrix);
 
    void FromVectors(const Eigen::Vector3d& Vxx, const Eigen::Vector3d& Vyy, const Eigen::Vector3d& Vzz);

    void FromVector(const Cartesian3D& Vzz);

    Eigen::Matrix3d ToMatrix() const;

    double alpha = 0.0;
    double beta = 0.0;
    double gamma = 0.0;
  };


  // permute and mirror
  std::vector<Cartesian2D> PermuteAndMirror(const Cartesian2D cartesian);
  
  std::vector<Cartesian3D> PermuteAndMirror(const Cartesian3D cartesian);


  // random vectors in standard coordinates

  std::vector<Cartesian2D> RandomCartesian2D(const int points);

  std::vector<Cartesian3D> RandomCartesian3D(const int points);

  std::vector<Polar> RandomPolar(const int points);

  std::vector<Spherical> RandomSpherical(const int points);


  // random permuted and mirrored vectors in standard coordinates
  // functions return n = 8 * points for 2D
  // and n = 48 * points for 3D
  std::vector<Cartesian2D> RandomPMCartesian2D(const int points);

  std::vector<Cartesian3D> RandomPMCartesian3D(const int points);

  std::vector<Polar> RandomPMPolar(const int points);

  std::vector<Spherical> RandomPMSpherical(const int points);


  // random orthonormal matrix from random seed vector
  // for EFG matrix Vzz = cartesian_vector
  // get all Vxx, Vyy, Vzz orientations from matrix columns
  std::vector<Eigen::Matrix3d> RandomOrthonormalMatrix(const std::vector<Cartesian3D> cartesian_vector);


  // average over circle
  // 2 permutations * 4 mirrors = 8 vectors
  std::vector<Cartesian2D> AverageCircleCartesian();

  std::vector<Polar> FixedCirclePolar();

  std::vector<Polar> AverageCirclePolar();

  // average over sphere
  // functions return a number of vectors that best represent an average over a sphere
  // see K. M. Hasselbach and H. Spiering, Nuc. Instr. and Meth. 176, 537 (1980), "The average over a sphere"
  // return 48 vectors for order=1, up to 1008 for order=6
  // 6 permutations * 8 mirrors = 48 vectors for each basis vector

  std::vector<coordinates::Cartesian3D> AverageSphereCartesian(const int order);
  
  std::vector<coordinates::Spherical> AverageSphereSpherical(const int order);

}  // namespace coordinates

#endif // NEXUS_COORDINATES_H_
