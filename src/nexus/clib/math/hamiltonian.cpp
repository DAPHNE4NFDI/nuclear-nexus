// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "hamiltonian.h"

#include <iostream>
#include <complex>

#include "../nexus_definitions.h"
#include "../math/quantum.h"


namespace hamiltonian {

  Complex magnetic_matrix_element(
    const BareHyperfine& hyperfine,
    const double magnetic_energy,
    const double J,
    const double m1,
    const double m2,
    const double clebsh_gordon_state)
  {
    const Complex matrix_element = -magnetic_energy * pow(-1, m1 - m2) * quantum::ClebshGordon(J, 1, m1, m2 - m1, J, m2)
      / clebsh_gordon_state * quantum::WignerDmatrix(1, 0, m1 - m2, 0, hyperfine.magnetic_theta, hyperfine.magnetic_phi);

    return matrix_element;
  }

  Complex quadrupole_matrix_element(const BareHyperfine& hyperfine, const double electric_energy,
    const double J, const double m1, const double m2, const double clebsh_gordon_state)
  {
    const Complex Wigner20 = quantum::WignerDmatrix(2, 0, m1 - m2, hyperfine.quadrupole_alpha,
      hyperfine.quadrupole_beta, hyperfine.quadrupole_gamma);
   
    const Complex Wigner22 = quantum::WignerDmatrix(2, 2, m1 - m2, hyperfine.quadrupole_alpha,
      hyperfine.quadrupole_beta, hyperfine.quadrupole_gamma);
    
    const Complex Wigner2m2 = quantum::WignerDmatrix(2, -2, m1 - m2, hyperfine.quadrupole_alpha,
      hyperfine.quadrupole_beta, hyperfine.quadrupole_gamma);
    
    const Complex matrix_element = electric_energy * pow(-1, m1 - m2) * quantum::ClebshGordon(J, 2, m1, m2 - m1, J, m2)
      / (2.0 * clebsh_gordon_state) * (Wigner20 + hyperfine.quadrupole_asymmetry / constants::kSqrt6 * (Wigner22 + Wigner2m2));

    return matrix_element;
  }

  Eigen::MatrixXcd HamiltonianGroundState(const BareHyperfine& hyperfine, const MoessbauerIsotope* const isotope)
  {
    const double eV2gamma = 1.0 / isotope->gamma;

    // setup magnetic interaction
    double magnetic_energy = 0.0;

    // no splitting for I = 0
    // number of states 2 * Ig + 1 > 1
    // Clebsh-Gordon normalization factor is zero for I = 0
    if (isotope->number_ground_states > 1)
      magnetic_energy = isotope->magnetic_moment_ground * hyperfine.magnetic_field * eV2gamma;


    // setup quadrupole interaction
    double quadrupole_energy = 0.0;

    // splitting only for I > 1/2
    // number of states 2*Ig+1 > 2
    // Clebsh Gordon normalization factor is zero for I = 0 or 1/2
    if (isotope->number_ground_states > 2)
    {
      // Here, the splitting (hyperfine.quadrupole) is given in mm/s.
      quadrupole_energy = hyperfine.quadrupole * 1.0e-3 / constants::kSpeedOfLight * isotope->energy * eV2gamma; // convert mm/s to m/s, delta_E=delta_velocity/c*E_0
      
      // In Moessbauer experiments only the ratio of the quadrupole moments is measured.
      // The quadrupole splitting has thus to be scaled correctly by the quadrupole ratio Qg/Qe. Here the ground state is scaled.
      // If one of the states is not split, the EFG in this state is zero and the splitting is given only by the split state with I > 1/2.
      // Thus check if quadrupole splitting has to be scaled.
      if (isotope->number_excited_states > 2)
        quadrupole_energy *= isotope->quadrupole_ground->value / isotope->quadrupole_excited->value;
    }

    Eigen::MatrixXcd hamiltonian_matrix = Eigen::MatrixXcd::Zero(isotope->number_ground_states, isotope->number_ground_states);

    // cycle through states - m1, m2 combinations and fill Hamiltonian
    const double J = isotope->spin_ground;
 
    for (int i = 0; i < isotope->number_ground_states; ++i)
    {
      const double m1 = i - J;
     
      // cycle through half of the combinations and use complex conjugate to fill the upper right part of the matrix
      for (int k = 0; k < i + 1; ++k)
      {
        const double m2 = k - J;
        
        if (magnetic_energy != 0)
        {
          hamiltonian_matrix(i, k) += magnetic_matrix_element(hyperfine, magnetic_energy,
            J, m1, m2, isotope->clebsch_gordon_Ig_magnetic);
        }
        
        if (quadrupole_energy != 0)
        {
          hamiltonian_matrix(i, k) += quadrupole_matrix_element(hyperfine, quadrupole_energy,
            J, m1, m2, isotope->clebsch_gordon_Ig_quadrupole);
        }
        
        if (i != k)
          hamiltonian_matrix(k, i) = std::conj(hamiltonian_matrix(i, k));
      }
    }

    return hamiltonian_matrix;
  }

  Eigen::MatrixXcd HamiltonianExcitedState(const BareHyperfine& hyperfine, const MoessbauerIsotope* const isotope)
  {
    const double eV2gamma = 1.0 / isotope->gamma;

    // apply isomer shift to all diagonal elements in matrix initialization
    const double isomer_shift = hyperfine.isomer * 1.0e-3 / constants::kSpeedOfLight * isotope->energy * eV2gamma;
    
    Eigen::MatrixXcd hamiltonian_matrix = Eigen::MatrixXcd::Identity(isotope->number_excited_states, isotope->number_excited_states);
   
    hamiltonian_matrix *= isomer_shift;


    // setup magnetic interaction
    double magnetic_energy = 0.0;

    // no splitting for I = 0
    if (isotope->number_excited_states > 1)
      magnetic_energy = isotope->magnetic_moment_excited * hyperfine.magnetic_field * eV2gamma;


    // setup quadrupole interaction
    double quadrupole_energy = 0.0;

    // splitting only for I > 1/2
    if (isotope->number_excited_states > 2)
      quadrupole_energy = hyperfine.quadrupole * 1.0e-3 / constants::kSpeedOfLight * isotope->energy * eV2gamma; // convert mm/s to m/s, delta_E=delta_velocity/c*E_0


    const double J = isotope->spin_excited;

    // cycle through states - m1, m2 combinations and fill Hamiltonian
    for (int i = 0; i < isotope->number_excited_states; ++i)
    {
      const double m1 = i - J;

      // cycle through half of the combinations and use complex conjugate to fill the upper right part of the matrix
      for (int k = 0; k < i + 1; ++k)
      {
        const double m2 = k - J;

        if (magnetic_energy != 0)
        {
          hamiltonian_matrix(i, k) += magnetic_matrix_element(hyperfine, magnetic_energy,
            J, m1, m2, isotope->clebsch_gordon_Ie_magnetic);
        }

        if (quadrupole_energy != 0)
        {
          hamiltonian_matrix(i, k) += quadrupole_matrix_element(hyperfine, quadrupole_energy,
            J, m1, m2, isotope->clebsch_gordon_Ie_quadrupole);
        }

        if (i != k)
          hamiltonian_matrix(k, i) = std::conj(hamiltonian_matrix(i, k));
      }
    }

    return hamiltonian_matrix;
  }

}  // namspace hamiltonian
