// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "convolution.h"

#include <algorithm>
#include <numeric>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../math/constants.h"


namespace convolution {

  std::vector<double> GaussianKernel(const std::vector<double>& grid, const double fwhm, const bool truncate)
  {
    std::vector<double> kernel(grid.size());

    // important to floor, to not shift the convolution
    const double center = grid.at(static_cast<size_t>(floor(grid.size() / 2)));

    const double sigma = fwhm / constants::kSigmaToFHWM;

    std::transform(NEXUS_EXECUTION_POLICY grid.begin(), grid.end(), kernel.begin(),
      [center, sigma](double grid_value)
      {
        return exp(-0.5 * pow((grid_value - center) / sigma, 2));
      }
    );

    if (truncate)
    {
      // maximum of Gaussian before norm is one, take only values larger 3 sigma
      auto new_end = std::remove_if(kernel.begin(), kernel.end(),
        [&](const double element)
        {
          return (element < constants::kGaussianThreeSigma);
        }
      );

      kernel.erase(new_end, kernel.end());
    }

    const double norm = std::accumulate(kernel.begin(), kernel.end(), 0.0);

    std::for_each(kernel.begin(), kernel.end(),
      [norm](double& element)
      { 
        element /= norm;
      }
    );

    return kernel;
  }


  std::vector<double> LorentzianKernel(const std::vector<double>& grid, const double fwhm, const bool truncate)
  {
    std::vector<double> kernel(grid.size());

    // important to floor, to not shift the convolution
    const double center = grid.at(static_cast<size_t>(floor(grid.size() / 2)));

    const double gam = fwhm / 2.0;

    std::transform(NEXUS_EXECUTION_POLICY grid.begin(), grid.end(), kernel.begin(),
      [center, gam](double grid_value)
      {
        return 1.0 / (1.0 + pow((grid_value - center) / gam, 2));
      }
    );

    if (truncate)
    {
      // maximum of Gaussian before norm is one, take only values larger 30 sigma
      auto new_end = std::remove_if(kernel.begin(), kernel.end(), [&](const double element)
        {
          return (element < constants::kLorentzianThirtySigma);
        }
      );

      kernel.erase(new_end, kernel.end());
    }

    const double norm = std::accumulate(kernel.begin(), kernel.end(), 0.0);

    std::for_each(kernel.begin(), kernel.end(),
      [norm](double& element)
      {
        element /= norm;
      }
    );

    return kernel;
  }


  std::vector<std::vector<double>> GaussianKernel2D(const std::vector<double>& grid_x, double fwhm_x,
    const std::vector<double>& grid_y, double fwhm_y, const bool truncate)
  {
    if (fwhm_x == 0.0)
      fwhm_x = 1.0e-200;

    if (fwhm_y == 0.0)
      fwhm_y = 1.0e-200;

    std::vector<std::vector<double>> kernel(grid_x.size(), std::vector<double> (grid_y.size()));

    // important to floor, to not shift the convolution
    const double center_x = grid_x.at(static_cast<size_t>(floor(grid_x.size() / 2)));

    const double center_y = grid_y.at(static_cast<size_t>(floor(grid_y.size() / 2)));

    const double sigma_x = fwhm_x / constants::kSigmaToFHWM;
    
    const double sigma_y = fwhm_y / constants::kSigmaToFHWM;

    std::transform(NEXUS_EXECUTION_POLICY grid_x.begin(), grid_x.end(), kernel.begin(),
      [&](double grid_value_x)
      {
        std::vector<double> kernel_vector(grid_y.size());

        std::transform(grid_y.begin(), grid_y.end(), kernel_vector.begin(), [&](double grid_value_y)
          {
            return exp(-0.5 * pow((grid_value_x - center_x) / sigma_x, 2) - 0.5 * pow((grid_value_y - center_y) / sigma_y, 2));
          }
        );
      
        return kernel_vector;
      }
    );

    if (truncate)
    {
      // maximum of Gaussian before norm is one, take only values larger 3 sigma for each direction
      // outer vector
      const auto new_end = std::remove_if(kernel.begin(), kernel.end(),
        [&](const std::vector<double>& kernel_vector)
        {
          const double max_value = *std::max_element(kernel_vector.begin(), kernel_vector.end());

          return (max_value < constants::kGaussianThreeSigma);
        }
      );

      kernel.erase(new_end, kernel.end());

      //inner vectors, reverse order to delete proper index
      for (int j = static_cast<int>(kernel.front().size()) - 1; j >= 0; --j)
      {
        // create a new vector with y direction values
        std::vector<double> vector_y(kernel.size());

        for (int i = 0; i < static_cast<int>(kernel.size()); ++i)
          vector_y[static_cast<size_t>(i)] = kernel[static_cast<size_t>(i)][static_cast<size_t>(j)];

        const double max_value = *std::max_element(vector_y.begin(), vector_y.end());

        if (max_value < constants::kGaussianThreeSigma)
        {
          for (std::vector<double>& kernel_vector : kernel)
            kernel_vector.erase(kernel_vector.begin() + j);
        }
      }
    }

    double norm = 0.0;

    std::for_each(kernel.begin(), kernel.end(),
      [&](const std::vector<double>& kernel_vector)
      {
        norm += std::accumulate(kernel_vector.begin(), kernel_vector.end(), 0.0);
      }
    );

    std::for_each(NEXUS_EXECUTION_POLICY kernel.begin(), kernel.end(),
      [&](std::vector<double>& kernel_vector)
      {     
        for (double& element : kernel_vector)
          element /= norm;
      }
    );

    return kernel;
  }

  // Same convolution, Will give the same size as data.
  // It is based on valid convolution with padding.
  // Data and kernel must have the same distance for each point
  // So the grid must be the same for non-equidistant data
  // also see https://stackoverflow.com/questions/24518989/how-to-perform-1-dimensional-valid-convolution
  std::vector<double> SameConvolution(std::vector<double> data, std::vector<double> kernel)
  {
    if (kernel.size() % 2 == 0) // is even
      kernel.pop_back();

    const size_t kernel_size = kernel.size();

    // value padding of data matrix
    const size_t padding_size = (kernel_size - 1) / 2;

    data.insert(data.begin(), padding_size, data.front());

    data.insert(data.end(), padding_size, data.back());

    const size_t data_size = data.size();  // padded data size

    const size_t output_size = data_size - kernel_size + 1;  // original size of data

    std::vector<double> output(output_size);

    std::vector<int> index(output_size);

    std::iota(index.begin(), index.end(), 0);

    std::transform(NEXUS_EXECUTION_POLICY index.begin(), index.end(), output.begin(),
      [&](const int i)
      {
        double output_point = 0.0;

        for (int j = static_cast<int>(kernel_size) - 1, k = i; j >= 0; --j, ++k)
          output_point += kernel[static_cast<size_t>(j)] * data[static_cast<size_t>(k)];

        return output_point;
      }
    );

    return output;
  }


  // parallel in both directions, similar speed as parallel in one direction
  std::vector<std::vector<double>> SameConvolution2D(std::vector<std::vector<double>> data, std::vector<std::vector<double>> kernel)
  {
    // set correct dimensions for same convolution
    if (kernel.size() % 2 == 0) // is even
      kernel.pop_back();

    const size_t kernel_size_x = kernel.size();

    if (kernel.front().size() % 2 == 0) // is even
    {
      for (std::vector<double> &kernel_y : kernel)
        kernel_y.pop_back();
    }

    const size_t kernel_size_y = kernel.front().size();

    // value padding of data matrix
    const size_t x_padding_size = (kernel_size_x - 1) / 2;

    data.insert(data.begin(), x_padding_size, data.front());
    data.insert(data.end(), x_padding_size, data.back());

    const size_t y_padding_size = (kernel_size_y - 1) / 2;

    for (std::vector<double>& data_y : data)
    {
      data_y.insert(data_y.begin(), y_padding_size, data_y.front());
      data_y.insert(data_y.end(), y_padding_size, data_y.back());
    }

    const size_t padded_data_size_x = data.size(); // padded data size here
    const size_t padded_data_size_y = data.front().size();

    const size_t output_size_x = padded_data_size_x - kernel_size_x + 1;  //= data.size() - back to original data size for same convolution
    const size_t output_size_y = padded_data_size_y - kernel_size_y + 1;  //= data.front().size()

    std::vector<std::vector<double>> output(output_size_x, std::vector<double>(output_size_y, 0.0));

    std::vector<int> index_x(output_size_x);
    std::iota(index_x.begin(), index_x.end(), 0);

    std::vector<int> index_y(output_size_y);
    std::iota(index_y.begin(), index_y.end(), 0);

    std::transform(NEXUS_EXECUTION_POLICY index_x.begin(), index_x.end(), output.begin(),
      [&](const int i)
      {
        std::vector<double> output_vector(output_size_y);
      
        std::transform(NEXUS_EXECUTION_POLICY index_y.begin(), index_y.end(), output_vector.begin(),
          [&](const int j)
          {
            double output_point = 0.0;

            for (int k = static_cast<int>(kernel_size_x) - 1, a = i; k >= 0; --k, ++a)
            {
              for (int l = static_cast<int>(kernel_size_y) - 1, b = j; l >= 0; --l, ++b)
                output_point += kernel[static_cast<size_t>(k)][static_cast<size_t>(l)] * data[static_cast<size_t>(a)][static_cast<size_t>(b)];
            }

            return output_point;
          }
        );

        return output_vector;
      }
    );

    return output;
  }

  /*
  // constants for 2D Pocket FFT
  const int num_threads = std::thread::hardware_concurrency();
  // order of FFTs, increasing non-negative values
  const pocketfft::shape_t axes = { 0, 1 };

  std::vector<std::vector<double>> SameConvolutionFFT2D(const std::vector<std::vector<double>>& data,
    const std::vector<std::vector<double>>& kernel)
  {
    // real values are mirrored in FFT. So mirror the FFT multiplication result by complex conjugation

    const size_t n0 = data.size();
    const size_t n1 = data.front().size();
    // padding in size of kernel has to be added to avoid periodic effects, so double the size
    const size_t n = 2*n0 * 2*n1;

    // create vector out of vector<vector<>>
    std::vector<Complex> data_vector(n, Complex(0.0, 0.0));
    std::vector<Complex> kernel_vector(n, Complex(0.0, 0.0));

    std::vector<Complex> data_vector_fft = data_vector;
    std::vector<Complex> kernel_vector_fft = kernel_vector;

    // write vector<vector> to vector which is already zero padded
    for (size_t i = 0; i < 2*n0; ++i)
      for (size_t j = 0; j < 2*n1; ++j) {
        if (i < n0 && j < n1) {
          data_vector[i * 2*n1 + j] = Complex(data[i][j], 0.0);
          kernel_vector[i * 2*n1 + j] = Complex(kernel[i][j], 0.0);
        }
      }

    // shape of axes
    const pocketfft::shape_t shape{ 2 * n0, 2 * n1 };

    // stride along each axis in size of data type
    pocketfft::stride_t stride(shape.size());
    stride.at(1) = sizeof(Complex);
    stride.at(0) = shape.at(1) * sizeof(Complex);

    pocketfft::c2c(shape, stride, stride, axes, pocketfft::FORWARD, data_vector.data(), data_vector_fft.data(), 1., num_threads);
    pocketfft::c2c(shape, stride, stride, axes, pocketfft::FORWARD, kernel_vector.data(), kernel_vector_fft.data(), 1., num_threads);

    // point wise multipy of FFT arrays, store to data_fftw
    // complex conjugate of result, to mirror to correct position in reverse FFT
    std::transform(NEXUS_EXECUTION_POLICY data_vector_fft.begin(), data_vector_fft.end(), kernel_vector_fft.begin(), data_vector_fft.begin(),
      [](const Complex& dat, const Complex& ker)
      {
        return std::conj(dat * ker);
      }
    );

    pocketfft::c2c(shape, stride, stride, axes, pocketfft::BACKWARD, data_vector_fft.data(), data_vector.data(), 1./n, num_threads);

    // convert array to vector of vector
    std::vector<std::vector<double>> output(n0, std::vector<double>(n1, 0.0));

    for (size_t i = 0; i < 2 * n0; ++i)
      for (size_t j = 0; j < 2 * n1; ++j)
        if (i < n0 && j < n1)
          output[i][j] = data_vector[i * 2*n1 + j].real();  // /n: scaling factor for backward FFT

    return output;

  }
  */ 

}  // namespace convolution
