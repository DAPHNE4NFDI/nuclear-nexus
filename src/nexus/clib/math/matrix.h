// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Module for the Eigen::Matrix calculations in a std::vector
//
// template functions
// definitiosn in matrix.cpp



#ifndef NEXUS_MATRIX_H_
#define NEXUS_MATRIX_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"


namespace matrix {

  // angle in deg
  Eigen::Matrix2d RotationMatrix(const double angle);

  // angle in deg
  Eigen::Vector2cd VectorRotation(const Eigen::Vector2cd& vector, const double angle);

  // angle in deg
  Eigen::Matrix2cd MatrixRotation(const Eigen::Matrix2cd& matrix, const double angle);


  // scaling
  template <typename M , typename T>
  std::vector<M> Scale(const std::vector<M>& matrix, const T factor);


  // addition
  template <typename M>
  std::vector<M> Add(
    const std::vector<M>& matrix_1,
    const std::vector<M>& matrix_2);

  
  template <typename M>
  std::vector<M> Add(
    const std::vector<M>& matrix_11,
    const M& matrix_2);


  template <typename M>
  std::vector<M> Subtract(
    const std::vector<M>& matrix_1,
    const std::vector<M>& matrix_2);

  template <typename M>
  std::vector<M> Subtract(
    const std::vector<M>& matrix_1,
    const M& matrix_2);


  template <typename M>
  std::vector<M> Multiply(
    const std::vector<M>& matrix_1,
    const std::vector<M>& matrix_2);
  

  template <typename M, typename T>
  std::vector<M> MultiplyVector(
    const std::vector<M>& matrix,
    const std::vector<T>& vector);

  template <typename M, typename T>
  std::vector<M> DivideVector(
    const std::vector<M>& matrix,
    const std::vector<T>& vector);


  // padding
  template <typename M>
  std::vector<M> Padding(
    const std::vector<M>& matrix,
    const int grid_size,
    const int padding_size);

}  // namespace matrix

#endif // NEXUS_MATRIX_H_
