// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "transitions.h"

#include <complex>
#include <cmath>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../math/constants.h"
#include "../math/quantum.h"
#include "../math/diagonalization.h"
#include "../math/hamiltonian.h"


namespace transitions {

  // the isotropic distribution calculation (distribution of site) is adopted from CONUSS source code.
  // The scaling of transition_vector(2) is different to CONUSS due to different definitions of functions.

  // multipole weighted TLni components. Sturhahn PRB Eq. (7).
  // Here the delta_(L, lamda) * TLni == delta_(L', lamda') * TL'ni is calculated.
  // The third component is used to store the isotropic distribution component.
  Eigen::Vector3cd multipol_TLni(const Eigen::VectorXcd& eigenvec_ground, const Eigen::VectorXcd& eigenvec_excited,
    const MoessbauerIsotope* const isotope, bool multipolarity2)
  {
    // assign parameters of multipolarity
    int L = isotope->L;
    int number_L = isotope->number_L;
    
    double multipol_mixing_coefficient = isotope->mixing_coefficient_1;
    quantum::VecSpherHarm YLM = isotope->YLM1;
  
    if (multipolarity2 == true)
    {
      L = isotope->L2;
      number_L = isotope->number_L2;
    
      YLM = isotope->YLM2;
      multipol_mixing_coefficient = isotope->mixing_coefficient_2;
    }

    // cycle through delta_m (M), this cycles through the 2*L+1 configurations of the vector spherical harmonics YLM
    // the angular momentum of the transition type, e.g. M1, gives allowed delta_m values -L to L (E1 or M1: L=1,   E2 or  M2: L=2)
    Eigen::Vector3cd transition_vector = Eigen::Vector3cd::Zero();

    for (int index_delta_m = 0; index_delta_m < number_L; ++index_delta_m)
    {
      const int delta_m = index_delta_m - L;

      Complex quantum_weight(0.0, 0.0);  // given by the sums of the Clebsh-Gordon coefficients and the eigenvector projection of the spin eigenvectors in the diagonal base.

      // sum over ground states of second line in Sturhahn PRB Eq. 7
      for (int index_m_g = 0; index_m_g < isotope->number_ground_states; ++index_m_g)
      {
        const double m_ground = index_m_g - isotope->spin_ground;

        const double m_excited = m_ground + delta_m;

        // check that the excited spin value is a valid configuration, needed for Dy and Ir.
        // For 57-Fe, 119-Sn, etc., the index is always valid
        if (m_excited >= -isotope->spin_excited &&
            m_excited <= isotope->spin_excited)
        {
          const int index_m_e = static_cast<int>(round(m_excited + isotope->spin_excited));

          quantum_weight += std::conj(eigenvec_excited(index_m_e)) * eigenvec_ground(index_m_g)
            * quantum::ClebshGordon(isotope->spin_ground, L, m_ground, delta_m, isotope->spin_excited, m_excited);
        }

      }

      transition_vector(0) += quantum_weight * std::conj(YLM[static_cast<size_t>(index_delta_m)].sigma);
      transition_vector(1) += quantum_weight * std::conj(YLM[static_cast<size_t>(index_delta_m)].pi);

      // component for 3D isotropic distribution
      // Eq A.5 Sturhahn thesis factor S_ni, no scaling
      transition_vector(2) += quantum_weight * std::conj(quantum_weight);
    }

    transition_vector(0) *= sqrt(8.0 * constants::kPi / isotope->number_excited_states) * multipol_mixing_coefficient;
    transition_vector(1) *= sqrt(8.0 * constants::kPi / isotope->number_excited_states) * multipol_mixing_coefficient;

    // component for 3D isotropic distribution
    // Eq A.5 Sturhahn thesis factor S_ni - scaling factor (without 1+alpha)
    transition_vector(2) *= pow(multipol_mixing_coefficient, 2) / isotope->number_excited_states; 

    return transition_vector;
  }


  // here we calculate in the new diagonal basis of the hamiltonian_matrix.
  // It has 2*I+1 eigenvalues and eigenvectors.
  // The eigenvectors are the columns of the eigencomposition matrix from diagonalize (Eigensystem.eigenvectors).
  // The column vectors contain the components of the eigenvector in the basis formed by 
  // the eigenstates |m> of the spin projection operator.
  // So we have to take the proper Clebsh-Gordon coefficient of the spin projection on |m>. 
  // See Roehlsberger, Nuclear Condensed Matter Physics with Synchrotron Radiation, appendix and Sturhahn PRB Eq. (7), second line
  // Note, the sum over the multipolarity mixing coefficients delta_L,lambda and delta_L',lambda'
  // can put into the sum over all states n. Then this will give two individual sums over:
  // SUM_l,lambda   delta_L,lambda *TLni   x   SUM_l,lambda   delta_L',lambda' *TL'ni
  Transitions HyperfineTransitions(const BareHyperfine& hyper, const MoessbauerIsotope* const isotope)
  {
    Transition trans {};
    Transitions transitions {};

    trans.weight = hyper.weight;

    trans.lamb_moessbauer = hyper.lamb_moessbauer;

    trans.broadening = hyper.broadening;

    Eigen::MatrixXcd hamiltonian_matrix = hamiltonian::HamiltonianGroundState(hyper, isotope);

    const diagonalization::Eigensystem eigensystem_ground = diagonalization::DiagonalizeHermitian(hamiltonian_matrix);
  
    hamiltonian_matrix = hamiltonian::HamiltonianExcitedState(hyper, isotope);

    const diagonalization::Eigensystem eigensystem_excited = diagonalization::DiagonalizeHermitian(hamiltonian_matrix);

    // Sturhahn PRB Eq. (7), energies and polarisation matrices of each transition are calculated.
    // cycle through ground and excited state values, e.g. 57-Fe -1/2, +1/2 and -3/2, -1/2, 1/2, 3/2
    // actually we cycle through the number of eigenvalues, but our Hamiltonian has as many eigenvalues as the matrix size, which is 2*I+1.
    
    Eigen::Vector3cd transition_vector = Eigen::Vector3cd::Zero();

    Eigen::Vector2cd reduced_transition_vector = Eigen::Vector2cd::Zero();
    
    for (int i_ground = 0; i_ground < isotope->number_ground_states; ++i_ground)
    {
      for (int i_excited = 0; i_excited < isotope->number_excited_states; ++i_excited)
      {
        // here the transition energy is given as the difference to the actual unsplit transition
        // because the eigensystem give the change with respect to the unsplit resonance.
        trans.energy_detuning = eigensystem_excited.eigenvalues(i_excited) - eigensystem_ground.eigenvalues(i_ground);

        // Sum in TLni over M = delta_m and m = m_g and the multipolarity weights
        // first multipolarity e.g. E1, M1, E2 or M2
        transition_vector = multipol_TLni(
          eigensystem_ground.eigenvectors.col(i_ground),
          eigensystem_excited.eigenvectors.col(i_excited),
          isotope,
          false);

        // second multipolarity E2 if M1E2 transition
        if (isotope->multipolarity == Multipolarity::M1E2)
        {
          transition_vector += multipol_TLni(
            eigensystem_ground.eigenvectors.col(i_ground),
            eigensystem_excited.eigenvectors.col(i_excited),
            isotope,
            true);
        }

        // isotropic contribution
        trans.transition_polarisation_matrix = transition_vector(2) * Eigen::Matrix2cd::Identity();
        
        // site and/or texture contribution
        if (hyper.isotropic != true)
        {
          reduced_transition_vector(0) = transition_vector(0);
          reduced_transition_vector(1) = transition_vector(1);

          trans.transition_polarisation_matrix *= (1.0 - hyper.texture);

          // outer product
          trans.transition_polarisation_matrix += hyper.texture
            * reduced_transition_vector.conjugate() * reduced_transition_vector.transpose();
        }

        // eliminate rounding error, if whole matrix is zero, the error is typically ~ 1e-33
        // for real or imaginary parts of entries that are zero the error is typically ~1e-17 to 1e-16
        if (trans.transition_polarisation_matrix.isZero(1e-30) == false)
          transitions.push_back(trans);

      }  // end cycle excited states

    }  // end cycle ground states

    return transitions;
  }


  // CHECK nuclear currents for isotropic, texture and scaling!!!
  // not yet implemented
  // some optimizations of TnLi not yet done here.

  // Sturhahn thesis Eq. 1.23, first line, single multipole term (L, lambda)
  // only scaling differs compared to multipol_TLni
  Eigen::Vector2cd nuclear_current_multipol(const Eigen::VectorXcd& eigenvec_ground, const Eigen::VectorXcd& eigenvec_excited,
    const MoessbauerIsotope* const isotope, bool multipolarity2)
  {
    // assign parameters of multipolarity
    int L = isotope->L;
    int number_L = isotope->number_L;
  
    double multipol_mixing_coefficient = isotope->mixing_coefficient_1;

    quantum::VecSpherHarm YLM = isotope->YLM1;
    
    if (multipolarity2 == true)
    {
      L = isotope->L2;
      number_L = isotope->number_L2;
      
      YLM = isotope->YLM2;
      multipol_mixing_coefficient = isotope->mixing_coefficient_2;
    }

    // cycle through delta_m (M), this cycles through the 2*L+1 configurations of vector spherical harmonics YLM
    // the angular momentum of the transition type, e.g. M1, gives allowed delta_m values -L to L (E1 or M1: L=1,   E2: L=2)
    Eigen::Vector2cd transition_current = Eigen::Vector2cd::Zero();
    
    for (int index_delta_m = 0; index_delta_m < number_L; ++index_delta_m)
    {
      const int delta_m = index_delta_m - L;

      // quantum_weight is given by the sums of the Clebsh-Gordon coeffs and the eigenvector projection of the spin eigenvectors in the diagonal base.
      Complex quantum_weight(0.0, 0.0);
      
      // sum over ground states of second line in Sturhahn PRB Eq. 7
      for (int index_m_g = 0; index_m_g < isotope->number_ground_states; ++index_m_g)
      {
        const double m_ground = index_m_g - isotope->spin_ground;

        const double m_excited = m_ground + delta_m;

        // check that the excited spin value is a valid configuration, needed for Dy and Ir. For 57-Fe, 119-Sn, etc., the index is always valid
        if (m_excited >= -isotope->spin_excited &&
            m_excited <= isotope->spin_excited)
        {
          const int index_m_e = static_cast<int>(round(m_excited + isotope->spin_excited));

          quantum_weight += std::conj(eigenvec_excited(index_m_e)) * eigenvec_ground(index_m_g)
            * quantum::ClebshGordon(isotope->spin_ground, L, m_ground, delta_m, isotope->spin_excited, m_excited);
        }
      }

      transition_current(0) += quantum_weight * std::conj(YLM[static_cast<size_t>(index_delta_m)].sigma);
      transition_current(1) += quantum_weight * std::conj(YLM[static_cast<size_t>(index_delta_m)].pi);

      //transition_current(2) += quantum_weight * std::conj(quantum_weight); // component for 3D isotropic distribution
    }

    transition_current *= sqrt(4.0 * constants::kPi * constants::kPi * constants::kSpeedOfLight * isotope->gamma /
      constants::khbar / isotope->kvector) * multipol_mixing_coefficient;

    return transition_current;
  }

  // nuclear current <i|J|j> from Sturhahn PRB and Thesis (Eq.1.23)
  // similar to HyperfineTransitions and TLni, check constants!!!!
  // be sure to calculate both multipolarities, but not over all states
  Eigen::Vector2cd nuclear_current(const Eigen::VectorXcd& eigenvec_ground, const Eigen::VectorXcd& eigenvec_excited,
    const MoessbauerIsotope* const isotope)
  {
    Eigen::Vector2cd transition_current = nuclear_current_multipol(eigenvec_ground, eigenvec_excited, isotope, false);

    if (isotope->multipolarity == Multipolarity::M1E2)
      transition_current += nuclear_current_multipol(eigenvec_ground, eigenvec_excited, isotope, true);

    return transition_current;
  }


  NuclearCurrents HyperfineNuclearCurrents(const BareHyperfine& hyper, const MoessbauerIsotope* const isotope)
  {
    NuclearCurrent current {};
    NuclearCurrents nuclear_currents {};

    current.weight = hyper.weight;
    
    current.lamb_moessbauer = hyper.lamb_moessbauer;
    
    current.broadening = hyper.broadening;

    Eigen::MatrixXcd hamiltonian_matrix = hamiltonian::HamiltonianGroundState(hyper, isotope);

    const diagonalization::Eigensystem eigensystem_ground = diagonalization::DiagonalizeHermitian(hamiltonian_matrix);
  
    hamiltonian_matrix = hamiltonian::HamiltonianExcitedState(hyper, isotope);

    const diagonalization::Eigensystem eigensystem_excited = diagonalization::DiagonalizeHermitian(hamiltonian_matrix);

    // cycle through ground and excited state values, e.g. 57-Fe -1/2, +1/2 and -3/2, -1/2, 1/2, 3/2
    // actually we cycle through the number of eigenvalues, but our Hamiltonian has as many eigenvalues as the matrix size, which is 2*I+1.
    for (int i_ground = 0; i_ground < isotope->number_ground_states; ++i_ground)
    {
      for (int i_excited = 0; i_excited < isotope->number_excited_states; ++i_excited)
      {
        // here the transition energy is given as the difference to the actual unsplit transition because the eigensystem give the change with respect to the unsplit resonance.
        current.energy_detuning = eigensystem_excited.eigenvalues(i_excited) - eigensystem_ground.eigenvalues(i_ground);

        current.nuclear_current = nuclear_current(eigensystem_ground.eigenvectors.col(i_ground),
          eigensystem_excited.eigenvectors.col(i_excited), isotope);

        if (current.nuclear_current.isZero(1e-30) == false)
          nuclear_currents.push_back(current);

      } // end cycle excited states
    } // end cycle ground states
    
    return nuclear_currents;
  }

}  // namespace transitions
