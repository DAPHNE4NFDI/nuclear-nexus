// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "coordinates.h"

#include <iostream>
#include <map>
#include <random>


namespace coordinates {

  struct Polar;

  struct Spherical;

  // Cartesian 2D

  Cartesian2D Cartesian2D::operator+(const Cartesian2D& a) {
    Cartesian2D result;

    result.x = this->x + a.x;
    result.y = this->y + a.y;

    return result;
  }

  Cartesian2D Cartesian2D::operator-(const Cartesian2D& a) {
    Cartesian2D result;

    result.x = this->x - a.x;
    result.y = this->y - a.y;

    return result;
  }

  Cartesian2D Cartesian2D::operator*(const double& a) {
    Cartesian2D result;

    result.x = a * this->x;
    result.y = a * this->y;

    return result;
  }

  Cartesian2D Cartesian2D::operator/(const double& a) {
    Cartesian2D result;

    result.x = a / this->x;
    result.y = a / this->y;

    return result;
  }

  Polar Cartesian2D::ToPolar() const
  {
    Polar polar{};

    polar.r = sqrt(pow(x, 2) + pow(y, 2));
    polar.phi = atan2(y, x);

    return polar;
  }

  void Cartesian2D::Normalize()
  {
    const double norm = sqrt(pow(x, 2) + pow(y, 2));

    x /= norm;
    y /= norm;
  }

  Eigen::Vector2d Cartesian2D::ToEigen() const
  {
    Eigen::Vector2d vector{ x, y };

    return vector;
  }

  Cartesian2D Cartesian2D::Mirror(const Cartesian2D& mirror) const
  {
    Cartesian2D result{};

    result.x = this->x * mirror.x;
    result.y = this->y * mirror.y;

    return result;
  }


  // Cartesian 3D

  Cartesian3D Cartesian3D::operator+(const Cartesian3D& a) {
    Cartesian3D result;

    result.x = this->x + a.x;
    result.y = this->y + a.y;
    result.z = this->z + a.z;

    return result;
  }

  Cartesian3D Cartesian3D::operator-(const Cartesian3D& a) {
    Cartesian3D result;

    result.x = this->x - a.x;
    result.y = this->y - a.y;
    result.z = this->z - a.z;

    return result;
  }

  Cartesian3D Cartesian3D::operator*(const double& a) {
    Cartesian3D result;

    result.x = a * this->x;
    result.y = a * this->y;
    result.z = a * this->z;

    return result;
  }

  Cartesian3D Cartesian3D::operator/(const double& a) {
    Cartesian3D result;

    result.x = a / this->x;
    result.y = a / this->y;
    result.z = a / this->z;

    return result;
  }

  Spherical Cartesian3D::ToSpherical() const
  {
    Spherical spherical{};

    spherical.r = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    spherical.theta = atan2(sqrt(pow(x, 2) + pow(y, 2)), z);
    spherical.phi = atan2(y, x);

    return spherical;
  }

  void Cartesian3D::Normalize()
  {
    const double norm = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));

    x /= norm;
    y /= norm;
    z /= norm;
  }

  Eigen::Vector3d Cartesian3D::ToEigen() const
  {
    Eigen::Vector3d vector{ x, y, z };

    return vector;
  }

  Cartesian3D Cartesian3D::Mirror(const Cartesian3D& mirror) const
  {
    Cartesian3D result{};

    result.x = this->x * mirror.x;
    result.y = this->y * mirror.y;
    result.z = this->z * mirror.z;

    return result;
  }


  // Polar

  Cartesian2D Polar::ToCartesian() const
  {
    Cartesian2D cartesian{};

    cartesian.x = r * cos(phi);
    cartesian.y = r * sin(phi);

    return cartesian;
  }

  Eigen::Vector2d Polar::ToEigen() const
  {
    Eigen::Vector2d vector{ r, phi };

    return vector;
  }

  void Polar::Normalize()
  {
    r = 1.0;
  }

  // Spherical

  Cartesian3D Spherical::ToCartesian() const
  {
    Cartesian3D cartesian{};

    cartesian.x = r * sin(theta) * cos(phi);
    cartesian.y = r * sin(theta) * sin(phi);
    cartesian.z = r * cos(theta);

    return cartesian;
  }

  void Spherical::Normalize()
  {
    r = 1.0;
  }

  Eigen::Vector3d Spherical::ToEigen() const 
  {
    Eigen::Vector3d vector{ r, theta, phi };

    return vector;
  }


  // Euler
  
  void Euler::FromMatrix(const Eigen::Matrix3d& matrix)
  {
    if (matrix(2,2) == 1)
    {
      beta = 0.0;
      alpha = 0.0;
      gamma = acos(matrix(0,0));
    }
    else if (matrix(2,2) == -1)
    {
      alpha = 0.0;
      beta = constants::kPi;
      gamma = -acos(matrix(0,0));
    }
    else
    {
      alpha = atan2(matrix(1,2), matrix(0,2));
      beta = atan2(sqrt(1.0 - matrix(2,2) * matrix(2,2)), matrix(2,2));
      gamma = atan2(matrix(2, 1), -matrix(2, 0));
    }
  }


  // Vectors must be orthogonal
  void Euler::FromVectors(const Eigen::Vector3d& Vxx, const Eigen::Vector3d& Vyy, const Eigen::Vector3d& Vzz)
  {
    Eigen::Matrix3d matrix;

    matrix.col(0) = Vxx;
    matrix.col(1) = Vyy;
    matrix.col(2) = Vzz;

    FromMatrix(matrix);
  }


  void Euler::FromVector(const Cartesian3D& Vzz)
  {
    alpha = 0.0;
    beta = atan2(sqrt(1.0 - Vzz.z * Vzz.z), Vzz.z);
    gamma = atan2(Vzz.y, -Vzz.x);
  }

  // Return matrix column vectors are the Vxx, Vyy, Vzz vectors
  Eigen::Matrix3d Euler::ToMatrix() const
  {
    Eigen::Matrix3d matrix;

    matrix(0,0) = cos(alpha) * cos(beta) * cos(gamma) - sin(alpha) * sin(gamma);
    matrix(1,0) = cos(alpha) * sin(gamma) + cos(beta) * cos(gamma) * sin(alpha);
    matrix(2,0) = -cos(gamma) * sin(beta);

    matrix(0,1) = -cos(gamma) * sin(alpha) - cos(alpha) * cos(beta) * sin(gamma);
    matrix(1,1) = cos(alpha) * cos(gamma) - cos(beta) * sin(alpha) * sin(gamma);
    matrix(2,1) = sin(beta) * sin(gamma);

    matrix(0,2) = cos(alpha) * sin(beta);
    matrix(1,2) = sin(alpha) * sin(beta);
    matrix(2,2) = cos(beta);

    return matrix;
  }


  // all 2D and 3D mirror combinations
  // four 2D
  static const std::vector<Cartesian2D> mirrors2D{
    {1,1},           // not mirrored
    {-1,1}, {1,-1},  // single mirror
    {-1,-1}          // all mirrored
  };

  // eight 3D
  static const std::vector<Cartesian3D> mirrors3D{
    {1,1,1},                          // not mirrored
    {-1,1,1}, {1,-1,1}, {1,1,-1},     // single mirror
    {-1,-1,1}, {-1,1,-1}, {1,-1,-1},  // pairwise mirror 
    {-1,-1,-1}                        // all mirrored
  };


  // permutations and mirrors
  std::vector<Cartesian2D> PermuteAndMirror(const Cartesian2D cartesian)
  {
    const double x = cartesian.x;
    const double y = cartesian.y;

    // all two 2D permutations
    const std::vector<Cartesian2D> permutations{
      {x,y}, {y,x}
    };

    std::vector<Cartesian2D> cartesian_vector{};

    Cartesian2D cart{};

    for (const Cartesian2D& permutation : permutations)
    {
      for (const Cartesian2D& mirror : mirrors2D)
      {
        cart = permutation.Mirror(mirror);

        cartesian_vector.push_back(cart);
      }
    }

    return cartesian_vector;
  }

  std::vector<Cartesian3D> PermuteAndMirror(const Cartesian3D cartesian)
  {
    const double x = cartesian.x;
    const double y = cartesian.y;
    const double z = cartesian.z;

    // all six 3D permutations
    const std::vector<Cartesian3D> permutations{
      {x,y,z}, {z,x,y}, {y,z,x}, {y,x,z}, {z,y,x}, {x,z,y}
    };

    std::vector<Cartesian3D> cartesian_vector{};

    Cartesian3D cart{};

    for (const Cartesian3D& permutation : permutations)
    {
      for (const Cartesian3D& mirror : mirrors3D)
      {
        cart = permutation.Mirror(mirror);

        cartesian_vector.push_back(cart);
      }
    }

    return cartesian_vector;
  }


  // random vectors

  std::vector<Cartesian2D> RandomCartesian2D(const int points)
  {
    std::random_device random_device;
    std::mt19937 random_engine(random_device());

    std::uniform_real_distribution<double> distribution_m11(-1.0, 1.0);

    std::vector<Cartesian2D> cartesian_vector{};
  
    Cartesian2D cartesian{};

    for (int i = 0; i < points; ++i)
    {
      cartesian = { distribution_m11(random_engine), distribution_m11(random_engine) };

      cartesian.Normalize();

      cartesian_vector.push_back(cartesian);
    }

    return cartesian_vector;
  }

  std::vector<Cartesian3D> RandomCartesian3D(const int points)
  {
    std::random_device random_device;
    std::mt19937 random_engine(random_device());

    std::uniform_real_distribution<double> distribution_m11(-1.0, 1.0);

    std::vector<Cartesian3D> cartesian_vector{};

    Cartesian3D cartesian{};

    for (int i = 0; i < points; ++i)
    {
      cartesian = { distribution_m11(random_engine), distribution_m11(random_engine), distribution_m11(random_engine) };

      cartesian.Normalize();

      cartesian_vector.push_back(cartesian);
    }

    return cartesian_vector;
  }

  // 0 to 2Pi random distribution in phi
  std::vector<Polar> RandomPolar(const int points)
  {
    std::random_device random_device;
    std::mt19937 random_engine(random_device());

    std::uniform_real_distribution<double> distribution_phi(0.0, constants::k2Pi);

    std::vector<Polar> polar_vector{};

    Polar polar{};

    for (int i = 0; i < points; ++i)
    {
      polar = {1.0, distribution_phi(random_engine) };

      polar_vector.push_back(polar);
    }

    return polar_vector;
  }

  // 0 to Pi random distribution in theta
  // 0 to 2Pi random distribution in phi
  std::vector<Spherical> RandomSpherical(const int points)
  {
    std::random_device random_device;
    std::mt19937 random_engine(random_device());

    std::uniform_real_distribution<double> distribution_phi(0.0, constants::k2Pi);

    // for 3D distributions, theta must be such that the area elements of the random values are same in spherical coordinates
    // so random values from u = -1 to 1, then calculate arccos(u)
    // arccos[-1;1] gives [0;Pi]
    std::uniform_real_distribution<double> distribution_u(-1.0, 1.0);  

    std::vector<Spherical> spherical_vector{};
    
    Spherical spherical{};

    for (int i = 0; i < points; ++i)
    {
      spherical = { 1.0, acos(distribution_u(random_engine)), distribution_phi(random_engine) };

      spherical_vector.push_back(spherical);
    }

    return spherical_vector;
  }


  // Permuted and mirrored (PM) random vectors

  std::vector<Cartesian2D> RandomPMCartesian2D(const int points)
  {
    std::vector<Cartesian2D> random_vector = RandomCartesian2D(points);

    std::vector<Cartesian2D> random_pm_vector{};

    for (const Cartesian2D& cart : random_vector)
    {
      const std::vector<Cartesian2D> pm_vector = PermuteAndMirror(cart);

      random_pm_vector.insert(random_pm_vector.begin(), pm_vector.begin(), pm_vector.end());
    }

    return random_pm_vector;
  }

  std::vector<Cartesian3D> RandomPMCartesian3D(const int points)
  {
    std::vector<Cartesian3D> random_vector = RandomCartesian3D(points);

    std::vector<Cartesian3D> random_pm_vector{};

    for (const Cartesian3D& cart : random_vector)
    {
      const std::vector<Cartesian3D> pm_vector = PermuteAndMirror(cart);

      random_pm_vector.insert(random_pm_vector.begin(), pm_vector.begin(), pm_vector.end());
    }

    return random_pm_vector;
  }

  std::vector<Polar> RandomPMPolar(const int points)
  {
    std::vector<Polar> polar_vector{};

    std::vector<Cartesian2D> random_vector = RandomPMCartesian2D(points);

    for (const Cartesian2D& cartesian : random_vector)
    {
      polar_vector.push_back(cartesian.ToPolar());
    }

    return polar_vector;
  }

  std::vector<Spherical> RandomPMSpherical(const int points)
  {
    {
      std::vector<Spherical> spherical_vector{};

      std::vector<Cartesian3D> random_vector = RandomPMCartesian3D(points);

      for (const Cartesian3D& cartesian : random_vector)
      {
        spherical_vector.push_back(cartesian.ToSpherical());
      }

      return spherical_vector;
    }
  }


  // random orthonormal matrix

  std::vector<Eigen::Matrix3d> RandomOrthonormalMatrix(const std::vector<Cartesian3D> cartesian_vector)
  {
    std::random_device random_device;
    std::mt19937 random_engine(random_device());

    // -1 t 1
    std::uniform_real_distribution<double> distribution_m11(-1.0, 1.0);

    Eigen::Vector3d Vxx = Eigen::Vector3d::Zero();
    Eigen::Vector3d Vyy = Eigen::Vector3d::Zero();
    Eigen::Vector3d Vzz = Eigen::Vector3d::Zero();

    std::vector<Eigen::Matrix3d> matrix_vector{};

    for (const Cartesian3D& cartesian : cartesian_vector)
    {
      Vzz = cartesian.ToEigen();
      Vzz.normalize();

      // create second random vector
      while (Vyy.norm() == 0.0)
      {
        Vyy(0) = distribution_m11(random_engine);
        Vyy(1) = distribution_m11(random_engine);
        Vyy(2) = distribution_m11(random_engine);
      }

      // ensure orthonormality
      Vyy = Vyy.cross(Vzz);
      Vyy.normalize();

      // determine Vxx via cross product
      Vxx = Vzz.cross(Vyy);
      Vxx.normalize();

      Eigen::Matrix3d matrix;

      matrix.col(0) = Vxx;
      matrix.col(1) = Vyy;
      matrix.col(2) = Vzz;

      matrix_vector.push_back(matrix);
    }

    return matrix_vector;
  }



  // average over ... functions

  std::vector<Cartesian2D> AverageCircleCartesian()
  {
    std::vector<Cartesian2D> cartesian_vector{};

    Cartesian2D cartesian{};

    const Polar polar{ 1.0, 22.5 * constants::kDegToRad }; // 45 good - makes no sense with 8 points, 30 good, 22.5 good

    cartesian_vector = PermuteAndMirror(polar.ToCartesian());

    return cartesian_vector;
  }

  std::vector<Polar> AverageCirclePolar()
  {
    std::vector<Polar> polar_vector{};

    std::vector<Cartesian2D> cartesian_vector = AverageCircleCartesian();

    for (const Cartesian2D& cartesian : cartesian_vector)
    {
      polar_vector.push_back(cartesian.ToPolar());
    }

    return polar_vector;
  }

  std::vector<Polar> FixedCirclePolar()
  {
    const std::vector<Polar> fixed_polar {
      {1.0, 0.0},
      {1.0, 0.5 * constants::kPi},
      {1.0, constants::kPi},
      {1.0, 1.5 * constants::kPi}
    };
    
    return fixed_polar;
  }


  // average over sphere functions after Hasselbach ans Spierig

  // n is order parameter
  // pair(n, i < n), pair(theta (deg), phi (deg))
  static const std::map<const std::pair<int, int>, const std::array<double, 2>> vectors_angles{
    // n = 1
    {{1, 1}, {29.9747, 32.2544}},
    // n = 2
    {{2, 1}, {45.7717, 24.4677}},
    {{2, 2}, {20.9819, 14.2693}},
    // n = 3
    {{3, 1}, {27.3195, 12.6726}},
    {{3, 2}, {17.0563, 16.4135}},
    {{3, 3}, {39.6707, 41.1853}},
    // n = 4
    {{4, 1}, {36.0683, 9.6711}},
    {{4, 2}, {30.9724, 26.6363}},
    {{4, 3}, {14.8170, 29.2641}},
    {{4, 4}, {44.2746, 35.0827}},
    // n = 5
    {{5, 1}, {30.4882, 8.9865}},
    {{5, 2}, {13.0347, 8.5162}},
    {{5, 3}, {28.3668, 42.9986}},
    {{5, 4}, {45.3428, 36.7445}},
    {{5, 5}, {41.4175, 15.7099}},
    // n = 6
    {{6, 1}, {29.9420, 10.9780}},
    {{6, 2}, {12.1007, 26.9384}},
    {{6, 3}, {35.0195, 36.0697}},
    {{6, 4}, {47.1329, 35.3578}},
    {{6, 5}, {43.7872, 12.3579}},
    {{6, 6}, {24.9733, 25.0513}}
  };

  std::vector<Cartesian3D> AverageSphereCartesian(const int order)
  {
    std::vector<Cartesian3D> cartesian_vector{};

    Cartesian3D cartesian{};

    // sum over order n
    for (int n = 1; n <= order; ++n)
    {
      for (int i = 1; i <= n; ++i) // i < n
      {
        // find i-th n-order vector
        const auto map_iterator = vectors_angles.find(std::pair<int, int>(n, i));

        const Spherical spherical{
          1.0,
          map_iterator->second[0] * constants::kDegToRad,
          map_iterator->second[1] * constants::kDegToRad
        };

        const std::vector<Cartesian3D> cart_n_i_vector = PermuteAndMirror(spherical.ToCartesian());

        cartesian_vector.insert(cartesian_vector.end(), cart_n_i_vector.begin(), cart_n_i_vector.end());
      }
    }

    return cartesian_vector;
  }


  std::vector<Spherical> AverageSphereSpherical(const int order)
  {
    std::vector<Spherical> spherical_vector{};

    std::vector<Cartesian3D> cartesian_vector = AverageSphereCartesian(order);

    for (const Cartesian3D& cartesian : cartesian_vector)
    {
      spherical_vector.push_back(cartesian.ToSpherical());
    }

    return spherical_vector;
  }

}
