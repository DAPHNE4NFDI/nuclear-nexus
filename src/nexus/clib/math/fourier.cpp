// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "fourier.h"

#include <iostream>
#include <complex>
#include <cmath>
#include <functional>
#include <algorithm>

#include <Eigen/Dense>
#include "../external/pocketfft_hdronly.h"


namespace fourier {
  // constant values for 1D PocketFFT

  // stride along each axis in size of data type, here only along the first axis
  const pocketfft::stride_t stride(1, sizeof(Complex));

  // axis order of FFTs, increasing non-negative values (0,1,2,...)
  const pocketfft::shape_t axes{0};

  // via PocketFFT, performs slightly better than FFTW here
  std::vector<Eigen::Matrix2cd> FourierTransform(const std::vector<Eigen::Matrix2cd>& layer_matrix, const FourierScaling norm, const std::string window, const bool backward)
  {
    const size_t num_points = layer_matrix.size();

    // create single vectors out of vector of matrices
    std::vector<Complex> f_in_00(num_points);
    std::vector<Complex> f_out_00(num_points);
    std::vector<Complex> f_in_01(num_points);
    std::vector<Complex> f_out_01(num_points);
    std::vector<Complex> f_in_10(num_points);
    std::vector<Complex> f_out_10(num_points);
    std::vector<Complex> f_in_11(num_points);
    std::vector<Complex> f_out_11(num_points);

    for (size_t i = 0; i < num_points; ++i)
    {
      f_in_00[i] = layer_matrix[i](0, 0);
      f_in_01[i] = layer_matrix[i](0, 1);
      f_in_10[i] = layer_matrix[i](1, 0);
      f_in_11[i] = layer_matrix[i](1, 1);
    }

    // apply window function
    std::vector<double> window_func{};

    if (window == "Welch")
    {
      window_func = Welch(num_points);
    }
    else if (window == "Sine")
    {
      window_func = Sine(num_points);
    }
    else if (window == "Hann")
    {
      window_func = Hann(num_points);
    }
    else if (window == "Hamming")
    {
      window_func = Hamming(num_points);
    }
    else if (window == "Kaiser2")
    {
      window_func = Kaiser(num_points, 2.0);
    }
    else if (window == "Kaiser25")
    {
      window_func = Kaiser(num_points, 2.5);
    }
    else if (window == "Kaiser3")
    {
      window_func = Kaiser(num_points, 3.0);
    }

    if (window_func.size() != 0)
    {
      std::transform(f_in_00.begin(), f_in_00.end(), window_func.begin(), f_in_00.begin(), std::multiplies());
      std::transform(f_in_01.begin(), f_in_01.end(), window_func.begin(), f_in_01.begin(), std::multiplies());
      std::transform(f_in_10.begin(), f_in_10.end(), window_func.begin(), f_in_10.begin(), std::multiplies());
      std::transform(f_in_11.begin(), f_in_11.end(), window_func.begin(), f_in_11.begin(), std::multiplies());
    }

    // apply FFT shift for forward transform
    int shift_point = static_cast<int>(floor(layer_matrix.size() / 2));

    if (backward == false)
    {
      std::rotate(f_in_00.begin(), f_in_00.begin() + shift_point, f_in_00.end());
      std::rotate(f_in_01.begin(), f_in_01.begin() + shift_point, f_in_01.end());
      std::rotate(f_in_10.begin(), f_in_10.begin() + shift_point, f_in_10.end());
      std::rotate(f_in_11.begin(), f_in_11.begin() + shift_point, f_in_11.end());
    }

    // shape of axes, here only number of points in one dimensions
    const pocketfft::shape_t shape{num_points};

    // normalization of amplitudes
    double norm_factor = 1.0;

    if (norm == FourierScaling::N)
    {
      norm_factor = 1.0 / num_points;
    }
    else if (norm == FourierScaling::SqrtN)
    {
      norm_factor = 1.0 / sqrt(num_points);
    }

    bool direction = pocketfft::FORWARD;

    if (backward == true)
      direction = pocketfft::BACKWARD;

    pocketfft::c2c(shape, stride, stride, axes, direction, f_in_00.data(), f_out_00.data(), norm_factor, 0);
    pocketfft::c2c(shape, stride, stride, axes, direction, f_in_01.data(), f_out_01.data(), norm_factor, 0);
    pocketfft::c2c(shape, stride, stride, axes, direction, f_in_10.data(), f_out_10.data(), norm_factor, 0);
    pocketfft::c2c(shape, stride, stride, axes, direction, f_in_11.data(), f_out_11.data(), norm_factor, 0);

    // apply inverse FFT shift for backward transform
    if (backward == true)
    {
      std::rotate(f_out_00.rbegin(), f_out_00.rbegin() + shift_point, f_out_00.rend());
      std::rotate(f_out_01.rbegin(), f_out_01.rbegin() + shift_point, f_out_01.rend());
      std::rotate(f_out_10.rbegin(), f_out_10.rbegin() + shift_point, f_out_10.rend());
      std::rotate(f_out_11.rbegin(), f_out_11.rbegin() + shift_point, f_out_11.rend());
    }

    // output matrix
    std::vector<Eigen::Matrix2cd> fourier_matrix(num_points);

    Eigen::Matrix2cd eigen_matrix;

    for (size_t i = 0; i < num_points; ++i)
    {
      eigen_matrix(0, 0) = f_out_00[i];
      eigen_matrix(0, 1) = f_out_01[i];
      eigen_matrix(1, 0) = f_out_10[i];
      eigen_matrix(1, 1) = f_out_11[i];

      fourier_matrix[i] = eigen_matrix;
    }

    return fourier_matrix;
  }

  // Window functions

  std::vector<double> Welch(const size_t num_points)
  {
    std::vector<double> window(num_points);

    const double N_half = num_points / 2.0;

    for (size_t i = 0; i < num_points; ++i)
      window[i] = 1.0 - pow((i - N_half) / N_half, 2);

    return window;
  }

  std::vector<double> Sine(const size_t num_points)
  {
    std::vector<double> window(num_points);

    for (size_t i = 0; i < num_points; ++i)
      window[i] = sin(constants::kPi * i / num_points);

    return window;
  }

  std::vector<double> Hann(const size_t num_points)
  {
    std::vector<double> window(num_points);

    for (size_t i = 0; i < num_points; ++i)
      window[i] = pow(sin(constants::kPi * i / num_points), 2);

    return window;
  }

  std::vector<double> Hamming(const size_t num_points)
  {
    std::vector<double> window(num_points);

    for (size_t i = 0; i < num_points; ++i)
      window[i] = 0.53836 - 0.46164 * cos(2.0 * constants::kPi * i / num_points);

    return window;
  }

  // regular modified Bessel function implementation
  // only needed for Clang to replace std::cyl_bessel_i
  double modified_bessel_I_0(const double x)
  {
    double factor = 1.0;
    
    double sum = 0.0;
    
    for (int k = 0; k < 6; factor *= ++k)
      sum += pow(x / 2.0, 2 * k) / pow(factor, 2);

    return sum;
  }

  std::vector<double> Kaiser(const size_t num_points, const double alpha)
  {
    std::vector<double> window(num_points);

    //const double divisor = std::cyl_bessel_i(0, constants::kPi * alpha);
    const double divisor = modified_bessel_I_0(constants::kPi * alpha);

    double argument = 0.0;

    for (size_t i = 0; i < num_points; ++i)
    {
      argument = constants::kPi * alpha * sqrt(1.0 - pow((2.0 * i / num_points) - 1.0, 2));

      //window[i] = std::cyl_bessel_i(0, argument) / divisor;
      window[i] = modified_bessel_I_0(argument) / divisor;
    }

    return window;
  }

}  // namespace fourier
