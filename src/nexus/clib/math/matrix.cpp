// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "matrix.h"

#include <complex>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../math/constants.h"


namespace matrix {

  // angle in deg
  // counterclockwise rotation
  Eigen::Matrix2d RotationMatrix(const double angle)
  {
    Eigen::Matrix2d rotation = Eigen::Matrix2d::Zero();

    rotation(0, 0) = cos(angle * constants::kDegToRad);
    rotation(0, 1) = -sin(angle * constants::kDegToRad);
    rotation(1, 0) = sin(angle * constants::kDegToRad);
    rotation(1, 1) = cos(angle * constants::kDegToRad);

    return rotation;
  }

  // V' = R(a) * V
  Eigen::Vector2cd VectorRotation(const Eigen::Vector2cd& vector, const double angle)
  {
    return RotationMatrix(angle) * vector;
  }

  // M' = R(a) * M * R(a)^-1 = R(a) * M * R(-a) = R(a) * M * R(a)^T
  Eigen::Matrix2cd MatrixRotation(const Eigen::Matrix2cd& matrix, const double angle)
  {
    return RotationMatrix(angle) * matrix * RotationMatrix(-angle);
  }
 

  template <typename M, typename T>
  std::vector<M> Scale(const std::vector<M>& matrix, const T factor)
  {
    std::vector<M> result(matrix.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix.begin(), matrix.end(), result.begin(),
      [factor](const M& mat)
      {
        return factor * mat;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> Scale<Eigen::Matrix2cd, double>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const double factor);

  template std::vector<Eigen::Matrix2cd> Scale<Eigen::Matrix2cd, Complex>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const Complex factor);



  template <typename M>
  std::vector<M> Add(
    const std::vector<M>& matrix_1,
    const std::vector<M>& matrix_2)
  {
    std::vector<M> result(matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix_1.begin(), matrix_1.end(), matrix_2.begin(), result.begin(),
      [](const M& mat_1, const M& mat_2)
      {
        return mat_1 + mat_2;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> Add<Eigen::Matrix2cd>(
    const std::vector<Eigen::Matrix2cd>& matrix_1,
    const std::vector<Eigen::Matrix2cd>& matrix_2);
  


  template <typename M>
  std::vector<M> Add(
    const std::vector<M>& matrix_1,
    const M& matrix_2)
  {
    std::vector<M> result(matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix_1.begin(), matrix_1.end(), result.begin(),
      [&matrix_2](const M& mat_1)
      {
        return mat_1 + matrix_2;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> Add<Eigen::Matrix2cd>(
    const std::vector<Eigen::Matrix2cd>& matrix_1,
    const Eigen::Matrix2cd& matrix_2);
  


  template <typename M>
  std::vector<M> Subtract(
    const std::vector<M>& matrix_1,
    const std::vector<M>& matrix_2)
  {
    std::vector<M> result(matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix_1.begin(), matrix_1.end(), matrix_2.begin(), result.begin(),
      [](const M& mat_1, const M& mat_2)
      {
        return mat_1 - mat_2;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> Subtract<Eigen::Matrix2cd>(
    const std::vector<Eigen::Matrix2cd>& matrix_1,
    const std::vector<Eigen::Matrix2cd>& matrix_2);



  template <typename M>
  std::vector<M> Subtract(
    const std::vector<M>& matrix_1,
    const M& matrix_2)
  {
    std::vector<M> result(matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix_1.begin(), matrix_1.end(), result.begin(),
      [&matrix_2](const M& mat_1)
      {
        return mat_1 - matrix_2;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> Subtract<Eigen::Matrix2cd>(
    const std::vector<Eigen::Matrix2cd>& matrix_1,
    const Eigen::Matrix2cd& matrix_2);

  
  
  template <typename M>
  std::vector<M> Multiply(
    const std::vector<M>& matrix_1,
    const std::vector<M>& matrix_2)
  {
    std::vector<M> result(matrix_1.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix_1.begin(), matrix_1.end(), matrix_2.begin(), result.begin(),
      [](const M& mat_1, const M& mat_2)
      {
        return mat_1 * mat_2;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> Multiply<Eigen::Matrix2cd>(
    const std::vector<Eigen::Matrix2cd>& matrix_1,
    const std::vector<Eigen::Matrix2cd>& matrix_2);
  
  template std::vector<Eigen::Matrix4cd> Multiply<Eigen::Matrix4cd>(
    const std::vector<Eigen::Matrix4cd>& matrix_1,
    const std::vector<Eigen::Matrix4cd>& matrix_2);
  

  

  template <typename M, typename T>
  std::vector<M> MultiplyVector(
    const std::vector<M>& matrix,
    const std::vector<T>& vector)
  {
    std::vector<M> result(matrix.size());

    std::transform(matrix.begin(), matrix.end(), vector.begin(), result.begin(),
      [](const M& mat, const T& vector_element)
      {
        return vector_element * mat;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> MultiplyVector<Eigen::Matrix2cd, double>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const std::vector<double>& vector);

  template std::vector<Eigen::Matrix2cd> MultiplyVector<Eigen::Matrix2cd, Complex>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const std::vector<Complex>& vector);




  template <typename M, typename T>
  std::vector<M> DivideVector(
    const std::vector<M>& matrix,
    const std::vector<T>& vector)
  {
    std::vector<M> result(matrix.size());

    std::transform(matrix.begin(), matrix.end(), vector.begin(), result.begin(),
      [](const M& mat, const T& vector_element)
      {
        return mat / vector_element;
      }
    );

    return result;
  }

  template std::vector<Eigen::Matrix2cd> DivideVector<Eigen::Matrix2cd,double>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const std::vector<double>& vector);

  template std::vector<Eigen::Matrix2cd> DivideVector<Eigen::Matrix2cd,Complex>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const std::vector<Complex>& vector);




  // The matrix is extended by the boundary entries to padding size

  template <typename M>
  std::vector<M> Padding(
    const std::vector<M>& matrix,
    const int grid_size,
    const int padding_size)
  {
    std::vector<M> matrix_padded{};

    const int padding_difference = padding_size - grid_size;

    matrix_padded.insert(matrix_padded.begin(), static_cast<size_t>(round(padding_difference / 2)), matrix.front());

    matrix_padded.insert(matrix_padded.end(), matrix.begin(), matrix.end());

    matrix_padded.insert(matrix_padded.end(), static_cast<size_t>(round(padding_difference / 2)), matrix.back());

    return matrix_padded;
  }

  template std::vector<Eigen::Matrix2cd> Padding<Eigen::Matrix2cd>(
    const std::vector<Eigen::Matrix2cd>& matrix,
    const int grid_size,
    const int padding_size);

}  // namespace matrix
