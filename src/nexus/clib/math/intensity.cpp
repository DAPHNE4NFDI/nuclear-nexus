// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "intensity.h"

#include <iostream>
#include <cmath>
#include <algorithm>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../nexus_definitions.h"
#include "../utilities/errors.h"


namespace intensity {

  std::vector<double> Intensity(const std::vector<Complex>& ampltiude)
  {
    std::vector<double> intensity(ampltiude.size());

    std::transform(NEXUS_EXECUTION_POLICY ampltiude.begin(), ampltiude.end(), intensity.begin(),
      [](const Complex& ampl)
      {
        return std::norm(ampl);
      }
    );

    return intensity;
  }

  // Amplitudes from Jones vector. Only defined for fully coherent beams
  // intensity can be calculated via real(amplitude.adjoint() * amplitude)
  Eigen::Vector2cd Amplitude(const Eigen::Matrix2cd& matrix, const Beam* const beam)
  {
    if (isnan(beam->jones_vector(0).real()) ||
        isnan(beam->jones_vector(1).real()))
    {
      errors::WarningMessage("Amplitude", "No valid Jones vector set in Beam.");
    }

    return matrix * beam->jones_vector;
  }

  AmplitudeVector Amplitude(const ExperimentMatrix2& matrix, const Beam* const beam)
  {
    AmplitudeVector amplitude(matrix.size());

    if (isnan(beam->jones_vector(0).real()) ||
        isnan(beam->jones_vector(1).real()))
    {
      errors::WarningMessage("Amplitude", "No valid Jones vector set in Beam.");
    }

    std::transform(NEXUS_EXECUTION_POLICY matrix.begin(), matrix.end(), amplitude.begin(), [&](const Eigen::Matrix2cd& mat)
      {
        return mat * beam->jones_vector;
      }
    );

    return amplitude;
  }

  // Intensity from coherency matrix
  double Intensity(const Eigen::Matrix2cd& matrix, const Beam* const beam)
  {
    return (matrix * beam->matrix * matrix.adjoint()).trace().real();
  }

  std::vector<double> Intensity(const ExperimentMatrix2& matrix, const Beam* const beam)
  {
    std::vector<double> intensity(matrix.size());

    std::transform(NEXUS_EXECUTION_POLICY matrix.begin(), matrix.end(), intensity.begin(), [&](const Eigen::Matrix2cd& mat)
      {
        return (mat * beam->matrix * mat.adjoint()).trace().real();
      }
    );

    return intensity;
  }

  std::vector<double> Intensity(const AmplitudeVector amplitude)
  {
    std::vector<double> intensity(amplitude.size());

    std::transform(NEXUS_EXECUTION_POLICY amplitude.begin(), amplitude.end(), intensity.begin(), [&](const Eigen::Vector2cd& amp)
      {
        return amp.squaredNorm();
      }
    );

    return intensity;
  }

}  // namespace intensity
