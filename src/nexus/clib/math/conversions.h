// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Conversions of physical properties to equivalent properties.
// Header only file.


#ifndef NEXUS_CONVERSIONS_H_
#define NEXUS_CONVERSIONS_H_

#include "../nexus_definitions.h"
#include "../math/constants.h"


namespace conversions {

  inline double EnergyToKvector(const double energy)
  {
    const double kvector = energy / (constants::khbar * constants::kSpeedOfLight);  // 1/m
    
    return kvector;
  }

  inline double KvectorToEnergy(const double kvector)
  {
    const double energy = constants::khbar * constants::kSpeedOfLight * kvector;  // eV
    
    return energy;
  }

  inline double EnergyToWavelength(const double energy)
  {
    const double wavelength = constants::kPlanckConstant * constants::kSpeedOfLight / energy;  // m

    return wavelength;
  }

  inline double WavelengthToEnergy(const double wavelength)
  {
    const double energy = constants::kPlanckConstant * constants::kSpeedOfLight / wavelength;  // eV
    
    return energy;
  }

  inline double EnergyToKvectorZ(const double energy, const double angle)
  {
    const double kvector_z = energy / (constants::khbar * constants::kSpeedOfLight) * sin(angle * constants::kDegToRad);  // 1/m
    
    return kvector_z;
  }


  // energy in eV
  // isotope_energy in eV
  // isotope_gamma in eV
  inline double EnergyToVelocity(const double energy, const double isotope_energy, const double isotope_gamma)
  {
    const double velocity = energy / isotope_energy * constants::kSpeedOfLight / 1.0e-3;  // mm/s

    return velocity;
  }

  // energy in Gamma
  // isotope_energy in eV
  // isotope_gamma in eV
  inline double GammaToVelocity(const double energy, const double isotope_energy, const double isotope_gamma)
  {
    const double velocity = energy / isotope_energy * isotope_gamma * constants::kSpeedOfLight / 1.0e-3;  // mm/s

    return velocity;
  }

}  // namespace conversions

#endif // NEXUS_CONVERSIONS_H_
