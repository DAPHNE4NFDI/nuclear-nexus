// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementations of classes and functions to calculate properties of the nuclear transitions.
// A Transitions object holds all transitions of a BareHyperfine site (energy, polarization matrix, weight).
// A NuclearCurrents object holds the properties of all nuclear currents of a BareHyperfine site (energy, polarization dependent current vector, weight)
//
// for Transitions
// weight: of the specific transition depending on site and distribution
// energy: energy detuning from the actual isotope transition in units of Gamma
// transition_polarisation_matrix: product of [TLni]u* and [TLni]v
//     already summed over all multipole moments and weighted by vector spherical harmonics.


#ifndef NEXUS_TRANSITIONS_H_
#define NEXUS_TRANSITIONS_H_

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/bare_hyperfine.h"
#include "../classes/moessbauer.h"


namespace transitions {

  /**
  The :class:`Transition` class stores properties of a single transition under the influence of hyperfine interactions.

  Attributes:
     weight (float): Relative weight of the transition in a material. Only used for material with multiple site definitions and distributions.
     energy_detuning (float): Energy detuning form the unsplit nuclear transition energy in units of linewidth (:math:`\Gamma`).
     transition_polarisation_matrix (ndarray): Polarization dependent 2x2 complex transition matrix, already weighted by the multipole mixing coefficients.
        The calculated polarization dependent transition matrix for the specific transition is the nominator of the last term in Eq. (7) [Sturhahn]_

        .. math::
           \mathbf{T}_{\mu, \nu} = \left[\sum_{L,\lambda} \delta_{L,\lambda} T_{Lni}^{\lambda}\right]_{\mu}^* \left[\sum_{L,\lambda} \delta_{L',\lambda'}^* T_{L'ni}^{\lambda'}\right]_{\nu}
    
     lamb_moessbauer (float): Lamb-Moessbauer factor of the transition.

       .. versionadded:: 1.2.0

  */
  struct Transition {
    double weight = 0.0;
    double energy_detuning = 0.0;
    Eigen::Matrix2cd transition_polarisation_matrix = Eigen::Matrix2cd::Zero();
    double lamb_moessbauer = 1.0;
    double broadening = 1.0;
  };

  struct NuclearCurrent {
    double weight = 0.0;
    double energy_detuning = 0.0;
    Eigen::Vector2cd nuclear_current = Eigen::Vector2cd::Zero();
    double lamb_moessbauer = 1.0;
    double broadening = 1.0;
  };

  typedef std::vector<Transition> Transitions;

  typedef std::vector<NuclearCurrent> NuclearCurrents;

  /**
  Calculates the factor :math:`\delta_{L,\lambda} T_{Lni}^{(\lambda)}` of Eq. (7) in [Sturhahn]_.
  :math:`I_i` and :math:`I_n` are spins of the ground and excited state, respectively.
  :math:`L` is the angular momentum from the multipolarity (E1 or M1 :math:`L=1`, E2 :math:`L=2`).
  :math:`\lambda` indicates electronic (1) or magnetic (0) scattering.
  The sum over :math:`M` (which is :math:`\Delta m`) is the multipole selection rule and
  runs over all possible changes in angular momentum from :math:`M = -L, ..., L`.
  The sum over :math:`m` runs over all possible ground state spin quantum numbers :math:`-I_i, ..., I_i``.
  The eigenvector components of the ground and excited state, given in bra-ket notation,
  are summed up for each ground state :math:`m` and all possible multipole transition :math:`M`. 
  
  Args:
     eigenvec_ground (ndarray): Complex eigenvector of the ground state corresponding to a magnetic quantum number :math:`m_g`.
     eigenvec_excited (ndarray): Complex eigenvector of the excited state corresponding to a magnetic quantum number :math:`m_e`.
     isotope (:class:`MoessbauerIsotope`): isotope for the calculation.
     multipolarity2 (bool): Specifies if the first or second multipole component should be calculated, e.g. for a M1E2 transition. 
        If ``False`` the first multipole component of the transition is calculated, e.g. M1.
        If ``True`` the second one is calculated, e.g. E2.
    
  Returns:
     ndarray: Returns a 3 dimensional vector. The first two entries are the vector components (sigma and pi) corresponding to the vector spherical harmonics :math:`Y_{LM}^{\lambda}`. The third component is the corresponding factor for pure isotropic scattering.
  */
  Eigen::Vector3cd multipol_TLni(const Eigen::VectorXcd& eigenvec_ground, const Eigen::VectorXcd& eigenvec_excited,
    const MoessbauerIsotope* const isotope, bool multipolarity2);

  /**
  Calculate all hyperfine-split transitions in a nucleus for a given set of hyperfine parameters.
  The model is described in [Sturhahn]_.
  The eigenvalues and eigenvectors are calculated by diagonalization of the nuclear Hamiltonian, Eq. (12), and the energy detuning is found by Eq. (14).
  The calculated polarization dependent transition matrix of the returned :class:`Transition` objects is the one of the single transition and is the nominator of the last term in Eq. (7)

  .. math::
    \mathbf{T}_{\mu, \nu} = \left[\sum_{L,\lambda} \delta_{L,\lambda} T_{Lni}^{\lambda}\right]_{\mu}^* \left[\sum_{L,\lambda} \delta_{L',\lambda'}^* T_{L'ni}^{\lambda'}\right]_{\nu}
    
  already weighted by the multipole mixing coefficients. The sum over all transitions :math:`n` is not calculated here.
  The angles of the hyperfine parameters are defined from an arbitrary quantization axis of the nucleus.
  For calculations of photon interactions with the nuclear system, this quantization axis is set along the photon wave vector :math:`k` in nexus.

  Args:
     hyper (:class:`BareHyperfine`): Hyperfine parameters acting on the nucleus.
     isotope (:class:`MoessbauerIsotope`): Moessbauer isotope on which the hyperfine parameters act.
  
  Returns:
     :class:`Transitions`: List of all possible transitions in the nucleus.
  */
  Transitions HyperfineTransitions(const BareHyperfine& hyperfine, const MoessbauerIsotope* const isotope);

  NuclearCurrents HyperfineNuclearCurrents(const BareHyperfine& hyperfine, const MoessbauerIsotope* const isotope);

}  // namespace transitions

#endif // NEXUS_TRANSITIONS_H_
