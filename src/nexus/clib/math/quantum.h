// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementations of general function and classes used in quantum mechanical calculations.


#ifndef NEXUS_QUANTUM_H_
#define NEXUS_QUANTUM_H_

#include <vector>
#include <complex>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../math/coordinates.h"


namespace quantum {

  // polarization is a along two orthonormal vectors
  // e.g. (1,0,0) and (0,1,0) in the system of (e1_vec, e2_vec, k_vec), here (sigma, pi, k)
  struct VecSpherHarmPolBase
  {
    Complex sigma = Complex(0.0, 0.0);
    Complex pi = Complex(0.0, 0.0);
  };

  // contains the 2*L+1 components of the vector spherical harmonics, where L is photon angular momentum
  // so we have Y_(L,-L) to Y_(L,L) components
  // for M1 transition (L=1, lambda = 0) vector contains: Y^0_(1,-1)    Y^0_(1,0)    Y^0_(1,1)
  // for E2 transition (L=2, lambda = 1): Y^1_(2,-2)    Y^1_(2,-1)     Y^1_(2,0)     Y^1_(2,1)     Y^1_(2,2)
  typedef std::vector<VecSpherHarmPolBase> VecSpherHarm;


  /**
  Evaluates the Kronecker delta function

  .. math:: \delta_{ij} = \begin{cases}
                           0 &\text{if } i \neq j,   \\
                           1 &\text{if } i=j.
                          \end{cases}

  .. versionadded:: 1.2.0

  .. seealso:: `<https://de.wikipedia.org/wiki/Kronecker-Delta>`_

  Args:
     i (int): Integer number i.
     j (int): Integer number j.

  Returns:
     bool: Kronecker delta.
  */

  bool KroneckerDelta(const int i, const int j);


  /**
  Calculates the factorial :math:`n!` for n <= 20.

  .. seealso:: `<https://en.wikipedia.org/wiki/Factorial>`_

  Args:
     n (int): Integer number n.

  Returns:
     int: Factorial of n.
  */
  unsigned long long Factorial(const int n);


  /**
  Calculates the factorial :math:`n!` for n <= 20.

  .. seealso:: `<https://en.wikipedia.org/wiki/Factorial>`_

  Args:
     n (float): Integer number. The input is rounded and converted to an integer.

  Returns:
     int: Factorial of n.
  */
  unsigned long long Factorial(const double n);


  /**
  Calculates the Clebsh-Gordon coefficient to the given angular momentum quantum numbers via Racah's formula Eq. (3.19), [Rose]_.
  The input follows the convention:

  .. math::  <j_1, m_1, j_2, m_2 | J, M> = C(j_1, j_2, J; m_1, m_2, M) =  C(j_1, j_2, J; m_1, M-m_1),

  where C(...) is the notation of [Rose]_ for the Clebsh-Gordon coefficients.

  .. seealso:: `<https://en.wikipedia.org/wiki/Clebsch-Gordan_coefficients>`_

  Args:
     j1 (float): j1
     j2 (float): j2
     m1 (float): m1
     m2 (float): m2
     J (float): J
     M (float): M

  Returns:
     float: Clebsh-Gordon coefficient.
  */
  double ClebshGordon(const double j1, const double j2, const double m1, const double m2, const double J, const double M);


  /**
  Calculates the 3j-symbol via

  .. math:: \begin{Bmatrix} j_1 & j_2 & j_3 \\ m_1 & m_2 & m_3 \end{Bmatrix} = \frac{(-1)^{j_1 -j_2 - m_3}}{\sqrt{2 j_3 + 1}} <j_1, j_2, m_1, m_2 | j_3, -m_3>.

  .. seealso:: `<https://de.wikipedia.org/wiki/3j-Symbol>`_

  Args:
     j1 (float): j1
     j2 (float): j2
     j3 (float): j3
     m1 (float): m1
     m2 (float): m2
     m3 (float): m3

  Returns:
     float: ThreeJSymbol.
  */
  double ThreeJSymbol(const double j1, const double j2, const double j3, const double m1, const double m2, const double m3);


  /**
  Calculates the Racah V-coefficient after Eq. (3.20), [Rose]_.

  .. math:: V(j_1, j_2, j_3, m_1, m_2, m_3) = (-1)^{-j_3 + m_3} \sqrt{2j_3+1} <j_1, j_2, m_1, m_2 | j_3, -m_3>.

  .. seealso:: `<https://de.wikipedia.org/wiki/3j-Symbol>`_

  Args:
     j1 (float): j1
     j2 (float): j2
     j3 (float): j3
     m1 (float): m1
     m2 (float): m2
     m3 (float): m3

  Returns:
     float: V-coefficient.
  */
  double VCoefficient(const double j1, const double j2, const double j3, const double m1, const double m2, const double m3);


  /**
  Calculates the triangle coefficient after Eq. (6.8), [Rose]_.

  .. math:: \Delta (abc) = \sqrt{\frac{(a+b-c)! (a-b+c)! (-a+b+c)!}{(a+b+c+1)!}}.

  .. versionadded:: 1.2.0

  Args:
     a (float): a
     b (float): b
     c (float): c

  Returns:
     float: triangle coefficient :math:`\Delta`.
  */
  double TriangleCoefficient(const double a, const double b, const double c);


  /**
  Calculates the Racah W coefficient :math:`W(abcd ; ef)` after [Rose]_.

  .. versionadded:: 1.2.0

  .. seealso:: `<https://en.wikipedia.org/wiki/Racah_W-coefficient>`_

  Args:
     a (float): a
     b (float): b
     c (float): c
     d (float): d
     e (float): e
     f (float): f

  Returns:
     float: Racah W coefficient :math:`W(abcd ; ef)`.
  */
  double WCoefficient(const double a, const double b, const double c, const double d, const double e, const double f);


  /**
  Calculates the 6j-symbol after Eq. (9.11), [Varshalovich]_.
  
  .. math:: \begin{Bmatrix}
      a & b & c \\
      d & e & f
      \end{Bmatrix}
      = (-1)^{a+b+d+e} W(abed;cf).

  .. versionadded:: 1.2.0

  .. seealso:: `<https://en.wikipedia.org/wiki/6-j_symbol>`_

  Args:
     a (float): a
     b (float): b
     c (float): c
     d (float): d
     e (float): e
     f (float): f

  Returns:
     float: 6j-symbol.
  */
  double SixJSymbol(const double a, const double b, const double c, const double d, const double e, const double f);


  /**
  Calculates the small Wigner D matrix elements :math:`d^j_{m',m}(\beta) = d^j_{m1,m2}(\beta)` after Eq. (4.13), [Rose]_.

  .. seealso:: `<https://en.wikipedia.org/wiki/Wigner_D-matrix>`_

  Args:
     j (float): j
     m1 (float): m1
     m2 (float): m2
     beta (float): :math:`\beta` angle (rad).

  Returns:
     float: Small Wigner D matrix element.
  */
  double SmallWignerDmatrix(const double j, const double m1, const double m2, const double beta);


  // Wigner D matrix elements
  // notation D^j_m',m (alpha, beta, gamma) = D^j_m1,m2 (alpha, beta, gamma)

  /**
  Calculates the Wigner D matrix elements :math:`D^j_{m',m} (\alpha, \beta, \gamma) = D^j_{m1,m2} (\alpha, \beta, \gamma)` after Eq. (4.12), [Rose]_.

  .. seealso:: `<https://en.wikipedia.org/wiki/Wigner_D-matrix>`_

  Args:
     j (float): j
     m1 (float): m1
     m2 (float): m2
     alpha (float): :math:`\alpha` angle (rad).
     beta (float): :math:`\beta` angle (rad).
     gamma (float): :math:`\gamma` angle (rad).

  Returns:
     complex: Wigner D matrix element.
  */
  Complex WignerDmatrix(const double j, const double m1, const double m2, const double alpha, const double beta, const double gamma);


  // Polynominals - for clang they return NaN

  /**
  Computes the physicist's Hermite polynomials of the degree n and argument x.

  .. math:: P_l^m(x) = (-1)^n e^{x^2} \frac{d^n}{dx^n} e^{-x^2}.

  .. versionadded:: 1.2.0

  .. note:: Not available for macOS.

  .. seealso:: `<https://en.wikipedia.org/wiki/Hermite_polynomials>`_

  Args:
     n (int): degree :math:`n \geq 0`.
     x (float): argument x.

  Returns:
     double: physicist's Hermite polynomials.
  */
  double Hermite(const int n, const double x);


  /**
  Computes the Laguerre polynomials of the degree n and argument x.

  .. math:: L_n(x) = \frac{e^x}{n!} \frac{d^n}{dx^n} \left(e^{-x} x^n \right).
  
  .. versionadded:: 1.2.0

  .. note:: Not available for macOS.

  .. seealso:: `<https://en.wikipedia.org/wiki/Laguerre_polynomials>`_

  Args:
     n (int): degree :math:`n \geq 0`.
     x (float): argument x.

  Returns:
     double: Laguerre polynomials.
  */
  double Laguerre(const int n, double x);


  /**
  Computes the associated Laguerre polynomials of the degree n, order k and argument x.

  .. math:: L_n^k(x) = (-1)^k \frac{d^k}{dx^k} L_{n+k}(x).
 
  .. versionadded:: 1.2.0

  .. note:: Not available for macOS.

  .. seealso:: `<https://en.wikipedia.org/wiki/Laguerre_polynomials>`_

  Args:
     n (int): degree :math:`n \geq 0`.
     k (int): order :math:`k \geq 0`.
     x (float): argument x.

  Returns:
     double: associated Laguerre polynomials.
  */
  double AssociatedLaguerre(const int n, const int k, const double x);


  /**
  Computes the unassociated Legendre polynomials of the degree n and argument x.

  .. math:: P_n(x) = \frac{1}{2^n n!} \frac{d^n}{dx^n} \left(x^2-1 \right)^n.

  .. versionadded:: 1.2.0

  .. note:: Not available for macOS.

  .. seealso:: `<https://en.wikipedia.org/wiki/Legendre_polynomials>`_

  Args:
     n (int): degree :math:`n \geq 0`.
     x (float): argument x.

  Returns:
     double: unassociated Legendre polynomials.
  */
  double Legendre(const int n, const double x);


  /**
  Computes the associated Legendre polynomials of the degree l, order m, and argument x.
  The Condon–Shortley phase can be included.

  .. math:: P_\ell^m(x) = \left(1- x^2 \right)^{m/2} \frac{d^m}{dx^m} \left(P_\ell(x) \right).

  If the Condon–Shortley phase is included a factor :math:`(-1)^m` is multiplied to :math:`P_\ell^m(x)`.
 
  .. versionadded:: 1.2.0

  .. note:: Not available for macOS.

  .. seealso:: `<https://en.wikipedia.org/wiki/Associated_Legendre_polynomials>`_

  Args:
     l (int): degree :math:`\ell \geq 0`.
     m (int): order :math:`m \geq 0`. 
     x (float): argument x.
     csp (bool): If ``True`` the Condon-Shortley phase is included.

  Returns:
     double: associated Legendre polynomials.
  */
  double AssociatedLegendre(const int l, const int m, const double x, const bool csp);


  /**
  Computes the spherical harmonic of the degree l, order m, and arguments theta and phi.
  The Condon–Shortley phase can be included.
  The normalization factor can be specified.

  .. math:: Y_\ell^m(\theta,\varphi) = N P_\ell^m(\cos{\theta}) e^{i m \varphi}.

  where :math:`P_\ell^m(x)` are the associated Legendre polynominals (without the Condon-Shortly phase) and the normalization factor is
  
  .. math:: N = \sqrt{\frac{2 \ell + 1}{4 \pi} \frac{(\ell-m)!}{(\ell+m)!}}

  If the Condon–Shortley phase is included a factor :math:`(-1)^m` is multiplied to :math:`Y_\ell^m(x)`.

  If the Racah norm is used, the normalization is :math:`N = \sqrt{\frac{(\ell-m)!}{(\ell+m)!}}`.

  .. versionadded:: 1.2.0

  .. note:: Not available for macOS.

  .. seealso:: `<https://en.wikipedia.org/wiki/Spherical_harmonics>`_

  Args:
     l (int): degree :math:`\ell \geq 0`.
     m (int): order :math:`m \geq 0`.
     theta (float): argument :math:`\theta`.
     phi (float): argument :math:`\varphi`.
     csp (bool): If ``True`` the Condon-Shortley phase is included.
     Racah_norm (bool): If ``True`` the Racah norm is used.

  Returns:
     Complex: spherical harmonics.
  */
  Complex SphericalHarmonics(
    const int l,
    const int m,
    const double theta,
    const double phi,
    const bool csp,
    const bool racah_norm
  );

  // Vector Spherical Harmonics (VSH) in the polarization base of vector 1 and 2
  // after W. Sturhahn PRB 49, 9285 (1994), Eq. (15)
  // calculates the VSH for a specific transition of type L.lambda (E1, M1, E2, ....)
  // L = Photon Angular momentum, dipole L = 1, quadrupole L = 2,...
  // for magnetic transition M: lambda = 0       for electronic transition E: lambda = 1
  // parity of photon is P=(-1)^(L+lambda+1)
  // gives a vector of 2*L+1 elements of the polarization base 1 and 2  
  // overloaded function, as for standard k vectors (0,0,1) the Euler angles are all zero in our standard coordinate system along (e1,e2,k)

  /*
  Calculates the vector spherical harmonics in the polarization base of :math:`\sigma` and :math:`\pi`.
  See Eq. (15), [Sturhahn]_.

  Args:
     L (int): photon angular momentum of the transition, dipole L = 1, quadrupole L = 2, ...
     lambda (int): transition type, magnetic transition lambda = 0, electronic transition lambda = 1

  Returns:
     ndarray: first dimension has size of 2*L+1, each entry has a .simga and .pi attribute.
  */
  //VecSpherHarm VecSpherHarmPolarization(const int L, const int lambda);

  /**
  Calculates the vector spherical harmonics in the polarization base of :math:`\sigma` and :math:`\pi` for given Euler angles.
  See Eq. (15), [Sturhahn]_.

  .. seealso:: `<https://en.wikipedia.org/wiki/Vector_spherical_harmonics>`_

  Args:
     L (int): photon angular momentum of the transition, dipole L = 1, quadrupole L = 2, ...
     lambda (int): transition type, magnetic transition lambda = 0, electronic transition lambda = 1
     euler (:class:`Euler`): Euler angles. Default is 0 for all angles.

  Returns:
     ndarray: first dimension has size of 2*L+1, each entry has a .simga and .pi attribute.
  */
  VecSpherHarm VecSpherHarmPolarization(const int L, const int lambda,
    const coordinates::Euler euler = coordinates::Euler(0.0, 0.0, 0.0));

}  // namespace quantum

#endif // NEXUS_QUANTUM_H_
