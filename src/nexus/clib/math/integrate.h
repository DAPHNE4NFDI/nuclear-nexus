// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of a general templated integration function.
// header only file.


#ifndef NEXUS_INTEGRATE_H_
#define NEXUS_INTEGRATE_H_

#include <vector>

#include "../nexus_definitions.h"


namespace integrate {

  /*
  Implementation of the trapezoidal rule for numerical integration from a to b
  
  I = step * [1/2 * f(a) + 1/2 * f(b) + Sum_i=1^n-1 f(a + i * h) ]

  std::accumulate adds the start and end points fully.
  Subtract half of them again.
  */
  template <typename T>
  inline T IntegrateTrapez(const T step_size, const std::vector<T>& data, const std::ptrdiff_t start_offset, const std::ptrdiff_t stop_offset)
  {
    T sum = 0.0;

    if (stop_offset == 0)
    {
      sum = std::accumulate(data.begin() + start_offset, data.end(), 0.0);
      
      sum -= *(data.begin() + start_offset) / 2.0;

      sum -= data.back() / 2.0;
    }
    else
    {
       sum = std::accumulate(data.begin() + start_offset, data.begin() + stop_offset, 0.0);

       sum -= *(data.begin() + start_offset) / 2.0;

       sum -= *(data.begin() + stop_offset) / 2.0;
    }

    return step_size * sum;
  }

}  // namespace integrate

#endif // NEXUS_INTEGRATE_H_
