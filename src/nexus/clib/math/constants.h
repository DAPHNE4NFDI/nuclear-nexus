// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of physical and mathematical constants.
// Header only file.


#ifndef NEXUS_CONSTANTS_H_
#define NEXUS_CONSTANTS_H_

#include "../nexus_definitions.h"


namespace constants {

	/* numbers */
  constexpr const double kPi = 3.141592653589793238463;

  constexpr const double k2Pi = 6.283185307179586476925;

  constexpr const double kSqrt2 = 1.414213562373095048801;

  constexpr const double kSqrt6 = 2.449489742783178098197;

  /* unit conversions */
  constexpr const double kDegToRad = kPi / 180.0;

  /* pyhsical constants */
  constexpr const double kSpeedOfLight = 299792458.0; // m/s

  // constexpr const double kElementaryCharge = 1.602176634e-19; // C

  // constexpr const double ke = kElementaryCharge;

  // constexpr const double kVacuumPermittivity = 8.8541878188e-12; // As/Vm

  // constexpr const double kVacuumPermeability = 1.25663706127e-6; // N/A^2

  constexpr const double kPlanckConstant = 4.135667696e-15; //  eV/Hz

  constexpr const double kh = kPlanckConstant;

  constexpr const double kReducedPlanckConstant = 6.582119569e-16; // eV*s

  constexpr const double khbar = kReducedPlanckConstant;

  // constexpr const double kBohrMagneton = 5.7883818060e-5; // eV/T

  constexpr const double kNuclearMagneton = 3.15245125417e-8; // eV/T

  // constexpr const double kBohrRadius = 5.29177210544e-11; // m

  // constexpr const double kFineStrucutreConstant = 0.0072973525643;
  
  // constexpr const double kInverseFineStrucutreConstant = 137.035999177;

  // constexpr const double kRydbergConstant = 10973731.568157; //1/m

  constexpr const double kAvogadroConstant = 6.02214076e23; // Avogadro constant, per mole

  constexpr const double kNa = kAvogadroConstant;

  // constexpr const double kDalton = 931494103.72; // eV/c^2

  // constexpr const double ku = kDalton;

  // constexpr const double kMolarMassConstant = 1.00000000105e-3; //  kg/mol

  /* electron */
  constexpr const double kElectronRadius = 2.8179403227e-15; // meter

  constexpr const double kElectronMass = 0.51099895069; // MeV/c^2

  //constexpr const double kElectronGfactor = -2.00231930436092;

  /* proton */
  //constexpr const double kProtonMass = 938.2720894; // MeV/c^2

  //constexpr const double kProtonGfactor = 5.5856946893;

  /* neutron */
  //constexpr const double kNeutronMass = 939.56542052; // MeV//c^2

  //constexpr const double kNeutronGfactor = -3.82608552;

  /* statistical constants */
  constexpr const double kSigmaToFHWM = 2.354820045030949382023;  // for Gaussian: FWHM = 2 * sqrt(2* ln(2)) * sigma

  // value corresponds to roughly 99.7 % of all intensity
  // value for 3 sigma is exp(-0.5 * pow((3.0 * sigma_x) / sigma_x, 2)) = 
  constexpr const double kGaussianThreeSigma = 0.011108996538242306;

  // value corresponds to roughly 97.9 % of all intensity
  constexpr const double kLorentzianThirtySigma = 0.001109877913429523;

} //end namespace constants

#endif // NEXUS_CONSTANTS_H_
