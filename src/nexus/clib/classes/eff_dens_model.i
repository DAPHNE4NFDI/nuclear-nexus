// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module effdensmodel

%{
    #define SWIG_FILE_WITH_INIT
    #include "eff_dens_model.h"
%}


%feature("autodoc", "List of :class:`EffectiveLayer` objects.") std::vector<EffectiveLayer>;

%template(VectorEffectiveLayer) std::vector<EffectiveLayer>;


%include "eff_dens_model.h"


%extend EffectiveLayer{
  %pythoncode{

  def __repr__(self):
    output = "Effective density model layer:\n"
    output += "  .thickness: {}\n".format(self.thickness)
    output += "  .roughness: {}\n".format(self.roughness)
    output += "  .grazing_scattering_factor: {}\n".format(self.grazing_scattering_factor)
    return output

  }
}
