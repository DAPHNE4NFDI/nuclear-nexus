// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Sample class.
// A sample consists of several Layer objects and holds information on its lateral dimension and its scattering geometry.


#ifndef NEXUS_FORWARD_SAMLPE_H_
#define NEXUS_FORWARD_SAMLPE_H_

#include <string>
#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/layer.h"
#include "../classes/basic_sample.h"
#include "../classes/function_time.h"



/**
Constructor for :class:`ForwardSample`.

.. versionadded:: 2.0.0

Args:
   layers (list): List of Layer in order of beam propagation.
   drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
   function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
     Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
   id (string): User identifier.

Attributes:
   layers (list): List of Layer in order of beam propagation.
   drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
   function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
     Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
   id (string): User identifier.
*/
class ForwardSample : public BasicSample {
public:
  ForwardSample(
    const std::vector<Layer*> layers,
    const std::vector<double> drive_detuning, // Gamma
    FunctionTime* const function_time,  // return phase factor
    const std::string id = ""
  );

  //NxObject function definition
  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  /**
  Returns the effective thickness :math:`t_{eff} = \sigma \sum_i \rho_{i} f^{LM}_i t_i` of the sample in forward geometry, where
  :math:`\sigma` is the nuclear cross section,
  :math:`\rho_{i}` is the number density of the resonant nuclei in the i-th layer,
  :math:`f^{LM}_i` is the Lamb Moessbauer factor of the i-th layer,
  :math:`t_i` is the thickness of the layer i-th layer.

  Returns:
     double: Effective thickness.
  */
  double EffectiveThickness() const;

  /**
  Calculates the electronic amplitude behind the sample.

  Args:
     energy (float): X-ray energy (eV)

  Returns:
     complex: Electronic amplitude.
  */
  Complex ElectronicAmplitude(const double energy); // definition of virtual function of NxOject

  /**
  Calculates the relative electronic amplitude matrix behind the sample.

  Args:
     energy (float): X-ray energy (eV)

  Returns:
     complex: Electronic amplitude.
  */
  Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy); // definition of virtual function ofNxOject

  /**
  Calculates the electronic X-ray field amplitude in the sample at a given thickness value.

  Args:
     energy (float): X-ray energy (eV).
     thickness (float): Thickness value (nm).

  Returns:
     complex: Electronic field amplitude in the sample at the thickness value.
  */
  Complex ElectronicAmplitudeThickness(const double energy, const double thickness);

  /**
  Calculates the electronic X-ray field amplitude in the sample.

  Args:
     energy (float): X-ray energy (eV).
     num_points (int): Number of points of the output.

  Returns:
     complex: Electronic field amplitude in the sample.
  */
  std::vector<Complex> ElectronicFieldAmplitude(const double energy, const size_t num_points);

  /**
  Calculates the electronic X-ray field intensity (transmission factor) behind the sample.

  Args:
     energy (float): X-ray energy (eV)

  Returns:
     float: Electronic intensity (transmission factor).
  */
  double ElectronicIntensity(const double energy);

  /**
  Calculates the electronic X-ray field intensity (transmission factor) in the sample at a given thickness value.

  Args:
     energy (float): X-ray energy (eV).
     thickness (float): Thickness value (nm).

  Returns:
     float: Electronic field intensity in the sample at the thickness value.
  */
  double ElectronicIntensityThickness(const double energy, const double thickness);

  /**
  Calculates the electronic X-ray field intensity in the sample.

  Args:
     energy (float): X-ray energy (eV).
     num_points (int): Number of points of the output.

  Returns:
     float: Electronic field intensity.
  */
  std::vector<double> ElectronicFieldIntensity(const double energy, const size_t num_points);

  /* nuclear */
  
  /**
  Calculates the sample matrix - the product of all layer matrices.

  Args:
     isotope (:class:`MoessbauerIsotope`): Moessbauer isotope.
     detuning (list or ndarray): Detuning values of the calculation.
     calc_transitions (bool): Specifies if the nuclear transitions should be updated or not. Set this value to *True*.

  Returns:
     list: List of complex 2x2 matrices.
  */
  ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning, const bool calc_transitions);

  /* thickness distribution */

  size_t PopulateThicknessDistribution(const size_t distribution_points);

  void IncreaseDistributionIndex(const size_t distribution_points);

  void ResetDistributionIndex();

private:

  void ScatteringMatrices(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid, const bool calc_transitions);

  void PropagationMatrices(const MoessbauerIsotope* const isotope);

  void LayerMatrices();
};


#endif // NEXUS_FORWARD_SAMLPE_H_
