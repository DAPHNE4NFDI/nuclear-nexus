// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module fixed_object

%{
    #define SWIG_FILE_WITH_INIT
    #include "fixed_object.h"
%}

%feature("shadow") FixedObject::FixedObject %{
  def __init__(self, factor = 1+0j, matrix = np.array([[0,0], [0,0]], dtype=complex), id = ""):
    _cnexus.FixedObject_swiginit(self, _cnexus.new_FixedObject(factor, matrix, id))
%}



%include "fixed_object.h"



%extend FixedObject{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this") and not hasattr(self, attr):
      raise Exception("FixedObject has no attribute {}".format(attr))

    super().__setattr__(attr, val)

  def __repr__(self):
    output = "FixedObject:\n"
    output += "  .id: {}\n".format(self.id)
    output += "  .scattering_factor: {}\n".format(self.scattering_factor)
    output += "  .matrix:\n{}\n".format(self.matrix)
    return output

  }
}
