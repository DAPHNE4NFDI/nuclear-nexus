// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "beam.h"

#include <cmath>
#include <limits>

#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/matrix.h"


using namespace std::complex_literals;


Beam::Beam(
  Var* const polarization_,
  Var* const mixing_angle_,
  Var* const canting_angle_,
  const std::string profile_,
  Var* const fwhm_,
  const std::string id_
) : 
  polarization(polarization_),
  mixing_angle(mixing_angle_),
  canting_angle(canting_angle_),
  profile(profile_),
  fwhm(fwhm_),
  id(id_)
{
  if (!(profile_ == "g" ||
        profile_ == "r" ||
        profile_ == "n"))
  {
    errors::WarningMessage("Beam", ".profile not recognized");
  }

  Update();
}


void Beam::Update()
{
  const Complex x = cos(mixing_angle->value * constants::kDegToRad);
  const Complex y = 1.0i * sin(mixing_angle->value * constants::kDegToRad);

  jones_vector = matrix::RotationMatrix(canting_angle->value) * Eigen::Vector2cd(x,y);

  matrix = 0.5 * (1.0 - polarization->value) * Eigen::Matrix2cd::Identity()
           + polarization->value * jones_vector * jones_vector.adjoint();
};


void Beam::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(polarization);
    
  fit_handler->PrepareFitVariable(mixing_angle);
    
  fit_handler->PrepareFitVariable(canting_angle);

  fit_handler->PrepareFitVariable(fwhm);
}

void Beam::Unpolarized()
{
  polarization->value = 0.0;

  mixing_angle->value = 0.0;

  canting_angle->value = 0.0;

  Update();
}


void Beam::LinearSigma()
{
  polarization->value = 1.0;

  mixing_angle->value = 0.0;

  canting_angle->value = 0.0;

  Update();
}


void Beam::LinearPi()
{
  polarization->value = 1.0;

  mixing_angle->value = 0.0;

  canting_angle->value = 90.0;

  Update();
}


void Beam::CircularLeft()
{
  polarization->value = 1.0;

  mixing_angle->value = 45.0;

  canting_angle->value = 0.0;

  Update();
}


void Beam::CircularRight()
{
  polarization->value = 1.0;

  mixing_angle->value = -45.0;

  canting_angle->value = 0.0;

  Update();
}


void Beam::SetJonesVector(const Eigen::Vector2cd& jones_vector_, const double angle)
{
  jones_vector = jones_vector_;
  
  jones_vector.normalize();

  jones_vector = matrix::RotationMatrix(angle) * jones_vector;
}


void Beam::SetCoherencyMatrix(const Eigen::Matrix2cd& matrix_)
{
  /*
  matrix = matrix_;

  // set undefined Jones vector
  jones_vector(0) = Complex(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
  jones_vector(1) = Complex(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
  */

  errors::ErrorMessage("Beam", "removed in verison 2.0.0");
}


double Beam::Polarization() const
{
  return sqrt(1.0 - 4.0 * matrix.determinant().real() / pow(matrix.trace().real(), 2));
}


Complex Beam::ComplexCoherence() const
{
  Complex coherence(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());

  if (std::abs(sqrt(matrix(0, 0)) * sqrt(matrix(1, 1))) > 0.0)
    coherence = matrix(0, 1) / (sqrt(matrix(0, 0)) * sqrt(matrix(1, 1)));

  return coherence;
}


double Beam::Coherence() const
{
  double coherence = std::numeric_limits<double>::quiet_NaN();

  if (std::abs(sqrt(matrix(0, 0)) * sqrt(matrix(1, 1))) > 0.0)
    coherence = std::abs(matrix(0,1) / (sqrt(matrix(0, 0)) * sqrt(matrix(1, 1)) ));

  return coherence;
}


double Beam::PhaseDifference() const
{
  double phase_difference = std::numeric_limits<double>::quiet_NaN();
  
  if (std::abs(sqrt(matrix(0, 0)) * sqrt(matrix(1, 1))) > 0.0)
  {
    const Complex phase = matrix(0,1) / (sqrt(matrix(0,0)) * sqrt(matrix(1,1)));
  
    phase_difference = std::arg(phase);
  }

  return phase_difference;
}


bool Beam::ValidJonesVector() const
{
  bool valid = true;

  if (isnan(jones_vector(0).real()) ||
      isnan(jones_vector(0).imag()) ||
      isnan(jones_vector(1).real()) ||
      isnan(jones_vector(1).imag()))
  {
    valid = false;
  }

  return valid;
}


Eigen::Matrix2cd Beam::MatrixFromJonesVector() const
{
  // outer product --> Jij = <Ei * Ej^*>
  return jones_vector * jones_vector.adjoint();
}


// rotate the Jones vector and the coherency matrix
void Beam::Rotate(const double angle)
{
  /*
  jones_vector = matrix::VectorRotation(jones_vector, angle);

  matrix = matrix::MatrixRotation(matrix, angle);
  */
  errors::ErrorMessage("Beam", "removed in verison 2.0.0");
}
