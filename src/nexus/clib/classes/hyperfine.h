// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Hyperfine class.
// This class does not only hold the bare hyperfine parameters but also the corresponding distributions.
// units of the inputs are
// weight (fraction, 0 to 1)
// isomer shift (mm/s)
// magnetic field (T)
// magnetic theta, phi (deg)
// quadrupole (mm/s)
// quadrupole alpha, beta, gamma (deg), these angles are given in extrinsic ZYZ convention! When you want to use intrinsic ZYZ change alpha and gamma.
// quadrupole asymmetry (dimensionless 0 to 1)
// isotropic (true or false)
//
// The distribution class pointers point to Distribution class objects defined in Python.


#ifndef NEXUS_HYPERFINE_H_
#define NEXUS_HYPERFINE_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"
#include "../classes/optimizer_handler.h"
#include "../classes/distribution.h"
#include "../classes/bare_hyperfine.h"


/**
Defines a set of hyperfine parameters acting on a Moessbauer isotope.
Parameters can have distributions of type :class:`Distribution`.
Special random 2D and 3D distributions are available.

Args:
    id (string): User identifier.
    weight (float or :class:`Var`): Relative weight of the site, > 0.
    isomer (float or :class:`Var`): Isomer shift (mm/s).
    magnetic_field (float or :class:`Var`): Magnetic hyperfine field amplitude (Tesla). 
    magnetic_theta (float or :class:`Var`): Polar angle of the magnetic field with respect to the beam propagation direction - photon :math:`k` vector (degree).
    magnetic_phi (float or :class:`Var`): Azimuthal angle of the magnetic field with respect to the :math:`sigma` direction of the beam (degree).
    quadrupole (float or :class:`Var`): Quadrupole splitting (:math:`QS`) value (mm/s).
      It is :math: QS = \frac{c}{E_{trans}} \frac{eQ_iV_{zz}}{2}`, with :math:`Q_i` being the quadrupole moment of ground or excited state.
      For isotopes with only one quadrupole split state, the given value represents the value of the split state.
      For isotopes with split ground and excited state, it represents the quadrupole splitting of the excited state.
      The ground state quadrupole energy is then scaled by the ratio of the quadrupole moments :math:`Q_g/Q_e`.
    quadrupole_alpha (float or :class:`Var`): Euler angle of the electric field gradient :math:`\alpha` in extrinsic ZYZ convention (degree).
    quadrupole_beta (float or :class:`Var`): Euler angle of the electric field gradient :math:`\beta` in extrinsic ZYZ convention (degree).
    quadrupole_gamma (float or :class:`Var`): Euler angle of the electric field gradient :math:`\gamma` in extrinsic ZYZ convention (degree).
    quadrupole_asymmetry (float or :class:`Var`): Asymmetry parameter of the electric field gradient, 0 to 1.
    texture (float or :class:`Var`): Texture coefficient between 0 and 1.
      It assumes the directional parameters of the site with a ratio of ``texture`` and a 3D isotropic distribution of the parameters with the ration ``1 - texture``.
      If isotropic is ``True`` it is not taken into account.
      Setting texture to ``0`` is the same as to set isotropic to ``True``.

      .. versionadded:: 1.2.0

    lamb_moessbauer (float or :class:`Var`): Lamb-Moessbauer factor of the hyperfine site.
      Only used when the :attr:`lamb_moessbauer` of the material is ``None``.

      .. versionadded:: 1.2.0

    broadening (float or :class:`Var`): Optional inhomogeneous broadening parameter.
      It multiplies with the natural linewidth.

      .. versionadded:: 1.2.0

    isomer_dist (:class:`Distribution`): Distribution to the isomer shift (mm/s).
    magnetic_field_dist (:class:`Distribution`): Distribution to the magnetic hyperfine field amplitude (Tesla).
    magnetic_theta_dist (:class:`Distribution`): Distribution to the polar angle of the magnetic field with respect to the beam propagation direction - photon :math:`k` vector (degree).
    magnetic_phi_dist (:class:`Distribution`): Distribution to the azimuthal angle of the magnetic field with respect to the :math:`sigma` direction of the beam (degree).
    quadrupole_dist (:class:`Distribution`): Distribution to the quadrupole splitting amplitude (mm/s).
    quadrupole_alpha_dist (:class:`Distribution`): Distribution to the Euler angle of the electric field gradient :math:`\alpha` in extrinsic ZYZ convention (degree).
    quadrupole_beta_dist (:class:`Distribution`): Distribution to the Euler angle of the electric field gradient :math:`\beta` in extrinsic ZYZ convention (degree).
    quadrupole_gamma_dist (:class:`Distribution`): Distribution to the Euler angle of the electric field gradient :math:`\gamma` in extrinsic ZYZ convention (degree).
    quadrupole_asymmetry_dist (:class:`Distribution`): Distribution to the asymmetry parameter of the electric field gradient, 0 to 1.
    isotropic (bool): If *True* the hyperfine site is set to 3D angular distribution.
      The site parameters (the angels between the magnetic field and the quadrupole) are fixed.
  
      .. versionchanged:: 1.2.0

         Prior to version 1.2.0, it overrides all other distributions on angular values applied to the Hyperfine object.
         Since version 1.2.0 the random distributions are kept.

Attributes:
    id (string): User identifier.
    weight (:class:`Var`): Relative weight of the site, > 0.
    isomer (:class:`Var`): Isomer shift (mm/s).
    magnetic_field (:class:`Var`): Magnetic hyperfine field amplitude (Tesla).
    magnetic_theta (:class:`Var`): Polar angle of the magnetic field with respect to the beam propagation direction - photon :math:`k` vector (degree).
    magnetic_phi (:class:`Var`): Azimuthal angle of the magnetic field with respect to the :math:`sigma` direction of the beam (degree).
    quadrupole (float or :class:`Var`): Quadrupole splitting (:math:`QS`) value (mm/s).
      It is :math: QS = \frac{c}{E_{trans}} \frac{eQ_iV_{zz}}{2}`, with :math:`Q_i` being the quadrupole moment of ground or excited state.
      For isotopes with only one quadrupole split state, the given value represents the value of the split state.
      For isotopes with split ground and excited state, it represents the quadrupole splitting of the excited state.
      The ground state quadrupole energy is then scaled by the ratio of the quadrupole moments :math:`Q_g/Q_e`.
    quadrupole_alpha (:class:`Var`): Euler angle of the electric field gradient :math:`\alpha` in extrinsic ZYZ convention (degree).
    quadrupole_beta (:class:`Var`): Euler angle of the electric field gradient :math:`\beta` in extrinsic ZYZ convention (degree).
    quadrupole_gamma (:class:`Var`): Euler angle of the electric field gradient :math:`\gamma` in extrinsic ZYZ convention (degree).
    quadrupole_asymmetry (:class:`Var`): Asymmetry parameter of the electric field gradient, 0 to 1.
    texture (:class:`Var`): Texture coefficient between 0 and 1.
      It assumes the directional parameters of the site with a ratio of ``texture`` and a 3D isotropic distribution of the parameters with the ration ``1 - texture``.
      If isotropic is ``True`` it is not taken into account.
      Setting texture to ``0`` is the same as to set isotropic to ``True``.

      .. versionadded:: 1.2.0

    lamb_moessbauer (:class:`Var`): Lamb-Moessbauer factor of the hyperfine site.
      Only used when the :attr:`lamb_moessbauer` of the material is ``None``.

      .. versionadded:: 1.2.0

    broadening (float or :class:`Var`): Optional inhomogeneous broadening parameter.
      It multiplies with the natural linewidth.

      .. versionadded:: 1.2.0

    isomer_dist (:class:`Distribution`): Distribution to the isomer shift (mm/s).
    magnetic_field_dist (:class:`Distribution`): Distribution to the magnetic hyperfine field amplitude (Tesla).
    magnetic_theta_dist (:class:`Distribution`): Distribution to the polar angle of the magnetic field with respect to the beam propagation direction - photon :math:`k` vector (degree).
    magnetic_phi_dist (:class:`Distribution`): Distribution to the azimuthal angle of the magnetic field with respect to the :math:`sigma` direction of the beam (degree).
    quadrupole_dist (:class:`Distribution`): Distribution to the quadrupole splitting amplitude (mm/s).
    quadrupole_alpha_dist (:class:`Distribution`): Distribution to the Euler angle of the electric field gradient :math:`\alpha` in extrinsic ZYZ convention (degree).
    quadrupole_beta_dist (:class:`Distribution`): Distribution to the Euler angle of the electric field gradient :math:`\beta` in extrinsic ZYZ convention (degree).
    quadrupole_gamma_dist (:class:`Distribution`): Distribution to the Euler angle of the electric field gradient :math:`\gamma` in extrinsic ZYZ convention (degree).
    quadrupole_asymmetry_dist (:class:`Distribution`): Distribution to the asymmetry parameter of the electric field gradient, 0 to 1.
    isotropic (bool): If *True* the hyperfine site is set to 3D angular distribution.
      The site parameters (the angels between the magnetic field and the quadrupole) are fixed.

      .. versionchanged:: 1.2.0

         Prior to version 1.2.0, it overrides all other distributions on angular values applied to the Hyperfine object.
         Since version 1.2.0 the random distributions are kept.

    BareHyperfines (List of :class:`BareHyperfine`): Holds all hyperfine parameters combinations assigned to the site. So each explicit parameter combination due to distributions.
*/
class Hyperfine {
public:
  Hyperfine(
    Var* const weight,
    Var* const isomer,
    Var* const magnetic_field,
    Var* const magnetic_theta,
    Var* const magnetic_phi,
    Var* const quadrupole,
    Var* const quadrupole_alpha,
    Var* const quadrupole_beta,
    Var* const quadrupole_gamma,
    Var* const quadrupole_asymmetry,
    Var* const texture,
    Var* const lamb_moessbauer,
    Var* const broadening,
    Distribution* const isomer_dist = nullptr,
    Distribution* const magnetic_field_dist = nullptr,
    Distribution* const magnetic_theta_dist = nullptr,
    Distribution* const magnetic_phi_dist = nullptr,
    Distribution* const quadrupole_dist = nullptr,
    Distribution* const quadrupole_alpha_dist = nullptr,
    Distribution* const quadrupole_beta_dist = nullptr,
    Distribution* const quadrupole_gamma_dist = nullptr,
    Distribution* const quadrupole_asymmetry_dist = nullptr,
    const bool isotropic = false,
    const std::string id = ""
  );

  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  /**
  Set an isomer shift distribution.

  Args:
     dist (:class:`Distribution`): Distribution applied to the isomer shift.
  */
  void SetIsomerDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetIsomerDistribution() const;

  /**
  Set a magnetic field distribution.

  Args:
     dist (:class:`Distribution`): Distribution applied to the magnetic field.
  */
  void SetMagneticFieldDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetMagneticFieldDistribution() const;
  
  /**
  Set a magnetic field polar angle distribution.

  Args:
     dist (:class:`Distribution`): Distribution applied to the magnetic field polar angle.
  */
  void SetMagneticThetaDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetMagneticThetaDistribution() const;

  /**
  Set a magnetic field azimuthal angle distribution.

  Args:
     dist (:class:`Distribution`): Distribution applied to the magnetic field azimuthal angle.
  */
  void SetMagneticPhiDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetMagneticPhiDistribution() const;

  /**
  Set a quadrupole distribution.

  Args:
     dist (:class:`Distribution`): Distribution applied to the quadrupole amplitude.
  */
  void SetQuadrupoleDistribution(Distribution* const dist);
  
  std::pair<std::vector<double>, std::vector<double>> GetQuadrupoleDistribution() const;

  /**
  Set a quadrupole distribution in the :math:`\alpha` angle.

  Args:
     dist (:class:`Distribution`): Distribution applied to :math:`\alpha`.
  */
  void SetQuadrupoleAlphaDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetQuadrupoleAlphaDistribution() const;

  /**
  Set a quadrupole distribution in the :math:`\beta` angle.

  Args:
     dist (:class:`Distribution`): Distribution applied to :math:`\beta`.
  */
  void SetQuadrupoleBetaDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetQuadrupoleBetaDistribution() const;

  /**
  Set a quadrupole distribution in the :math:`\gamma` angle.

  Args:
     dist (:class:`Distribution`): Distribution applied to :math:`\gamma`.
  */
  void SetQuadrupoleGammaDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetQuadrupoleGammaDistribution() const;

  /**
  Set a quadrupole distribution in the quadrupole asymmetry.

  Args:
     dist (:class:`Distribution`): Distribution applied to the quadrupole asymmetry.
  */
  void SetQuadrupoleAsymmetryDistribution(Distribution* const dist);

  std::pair<std::vector<double>, std::vector<double>> GetQuadrupoleAsymmetryDistribution() const;

  /*
  Set a random distribution in the magnetic hyperfine field or the electric field gradient.
  For a hyperfine site random magnetic and random quadrupole magnitude distributions can be set in parallel.

  .. versionchanged:: 1.2.0

     In versions < 1.2.0 this function provided a real random distribution of vectors.
     However, this requires a large number of points and random sampling does not guarantee the correct spectrum.
     Since 1.2.0, complete random distributions are removed.
     Now two optimized versions are implemented.
     A new averaging method uses a group theoretical approach for optimized sampling of vectors.
     This method "model" should be used for most cases as ist reduces the number of points needed considerably.
     The random distribution method "random" is still available but now use an optimized random sampling of points based on permutations and mirroring of random vectors, which leads to much better results.
     The algorithm requires multiple of 48 points for 3D and multiple of 4 points for 2D distributions.
     The algorithm will choose a possible number of points closest to your point number input.

  .. note:: A random orientation of the asymmetry parameter is not included here. Only Vzz is randomly chosen and :math:`\alpha` is always zero. 

  Args:
     target (string): ``mag`` for magnetic distributions.
       ``efg`` for quadrupole distributions. 
       ``none`` to reset all angular distributions.
     type (string): *3D* for 3-dimensional distribution or for 2-dimensional distributions one of the following options:
                    *2Dks*, *2Dkp*, *2Dsp*, *2Dsk*, *2Dpk*, *2Dps*.
                    Convention for *2Dxy* is that xy give the plane of the distribution.
                    x and y can be *k* (k-vector direction), *s* (:math:`sigma` polarization direction) or *p* (:math:`pi` polarization direction).
     points (int): Number of points for the distribution. Only used for method *random*. Default is 401 points.

       .. versionchanged:: 1.2.0

     method (string): *random* for random selection of points, which are permuted and mirrored.
       *model* for selected points to average over the parameter after [Hasselbach]_.
       
       .. versionadded:: 1.2.0

     order (int): The order parameter determines the number of basis sets used in the spherical 3D approximation of the *average* method, not for the 2D one.
       For ``order=1`` 48 vectors are used, up to 1008 vectors for ``order=6``. Default is 1.
       
       .. versionadded:: 1.2.0

  */
  void SetRandomDistribution(const std::string target, const std::string type, const int points, const std::string method, const int order);


  /**
  Clears the selected random or spherical distribution.

  .. versionadded:: 1.2.0

  Args:
     target (string): ``mag`` for magnetic distributions.
       ``efg`` for quadrupole distributions.
       ``all`` for mag and efg.
  */
  void ClearRandomDistribution(const std::string target);

  mutable Var* weight = nullptr;

  mutable Var* isomer = nullptr;

  mutable Var* magnetic_field = nullptr;

  mutable Var* magnetic_theta = nullptr;

  mutable Var* magnetic_phi = nullptr;

  mutable Var* quadrupole = nullptr;

  mutable Var* quadrupole_alpha = nullptr;

  mutable Var* quadrupole_beta = nullptr;

  mutable Var* quadrupole_gamma = nullptr;

  mutable Var* quadrupole_asymmetry = nullptr;

  mutable Var* texture = nullptr;

  mutable Var* lamb_moessbauer = nullptr;

  mutable Var* broadening = nullptr;

  Distribution* isomer_dist_ptr = nullptr;
  
  Distribution* magnetic_field_dist_ptr = nullptr;
 
  Distribution* magnetic_theta_dist_ptr = nullptr;
  
  Distribution* magnetic_phi_dist_ptr = nullptr;
  
  Distribution* quadrupole_dist_ptr = nullptr;
  
  Distribution* quadrupole_alpha_dist_ptr = nullptr;
  
  Distribution* quadrupole_beta_dist_ptr = nullptr;
  
  Distribution* quadrupole_gamma_dist_ptr = nullptr;
  
  Distribution* quadrupole_asymmetry_dist_ptr = nullptr;

  bool isotropic = false;

  std::string id = "";

  std::vector<double> isomer_dist{};

  std::vector<double> magnetic_field_dist{};

  std::vector<double> magnetic_theta_dist{};

  std::vector<double> magnetic_phi_dist{};

  std::vector<double> quadrupole_dist{};

  std::vector<double> quadrupole_alpha_dist{};

  std::vector<double> quadrupole_beta_dist{};

  std::vector<double> quadrupole_gamma_dist{};

  std::vector<double> quadrupole_asymmetry_dist{};

  std::vector<double> isomer_dist_weight{};

  std::vector<double> magnetic_field_dist_weight{};

  std::vector<double> magnetic_theta_dist_weight{};

  std::vector<double> magnetic_phi_dist_weight{};

  std::vector<double> quadrupole_dist_weight{};

  std::vector<double> quadrupole_alpha_dist_weight{};

  std::vector<double> quadrupole_beta_dist_weight{};

  std::vector<double> quadrupole_gamma_dist_weight{};

  std::vector<double> quadrupole_asymmetry_dist_weight{};

  std::vector<BareHyperfine> BareHyperfines{};


  std::string distribution_target_magnetic = "none";
  
  std::string distribution_type_magnetic = "none";

  std::string distribution_method_magnetic = "model";
  
  int distribution_points_magnetic = 1;

  int distribution_order_magnetic = 1;
  
  int distribution_actual_points_magnetic = 1;


  std::string distribution_target_quadrupole = "none";
  
  std::string distribution_type_quadrupole = "none";

  std::string distribution_method_quadrupole = "model";
  
  int distribution_points_quadrupole = 1;

  int distribution_order_quadrupole = 1;

  int distribution_actual_points_quadrupole = 1;

private:

  void CallDistributionFunction(Distribution* const dist) const;

  void CalcDistribution(Distribution* const dist_ptr, const double value, std::vector<double>* dist_values,
    std::vector<double>* dist_weight);

  void CalcAllDistributions();

  std::pair<std::vector<double>, std::vector<double>> GetDistribution(const Var* const target, Distribution* const dist) const;

  std::vector<BareHyperfine> HyperfineCombinations(const std::string target, std::vector<double> &dist_values,
    std::vector<double> &dist_weights, const std::vector<BareHyperfine> inputhyper);

  std::vector<BareHyperfine> HyperfineRandomMagnetic(const std::string type, const int points,
    const std::string method, const int order, const std::vector<BareHyperfine> inputhyper);

  std::vector<BareHyperfine> HyperfineRandomQuadrupole(const std::string type, const int points,
    const std::string method, const int order, const std::vector<BareHyperfine> inputhyper);

  void SetBareHyperfines();

  void SetBareHyperfineRandom();
};

#endif // NEXUS_HYPERFINE_H_
