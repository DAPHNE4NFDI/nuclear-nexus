// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.



// header only file



// BareHyperfine holds hyperfine parameters as doubles.
// To be passed to nuclear calculations.
//   HyperfineTransitions()
//   HyperfineNuclearCurrents()
//   HamiltonianGroundState()
//   HamiltonianExcitedState()
//
// Each Hyperfine object set is converted to BareHyperfine objects before the actual calculations of the transitions.
// In BareHyperfine the angles are converted from deg to rad



#ifndef NEXUS_BARE_HYPERFINE_H_
#define NEXUS_BARE_HYPERFINE_H_

#include "../nexus_definitions.h"
#include "../math/constants.h"


/**
Constructor for :class:`BareHyperfine` class. This class is only used for calculations of the nuclear Hamiltonian.
Defines a set of hyperfine parameters acting on a Moessbauer isotope.
Angles are input in degree but internally stored in radian.

Args:
    weight (float): Relative weight of the site, > 0.
    isomer (float): Isomer shift (mm/s).
    magnetic_field (float): Magnetic hyperfine field amplitude (Tesla). 
    magnetic_theta (float): Polar angle of the magnetic field with respect to the beam propagation direction - photon :math:`k` vector (degree).
    magnetic_phi (float): Azimuthal angle of the magnetic field with respect to the :math:`sigma` direction of the beam (degree).
    quadrupole (float): Quadrupole splitting amplitude (mm/s).
    quadrupole_alpha (float): Euler angle of the electric field gradient :math:`\alpha` in extrinsic ZYZ convention (degree).
    quadrupole_beta (float): Euler angle of the electric field gradient :math:`\beta` in extrinsic ZYZ convention (degree).
    quadrupole_gamma (float): Euler angle of the electric field gradient :math:`\gamma` in extrinsic ZYZ convention (degree).
    quadrupole_asymmetry (float): Asymmetry parameter of the electric field gradient, 0 to 1.
    isotropic (bool): If *True* the hyperfine site is set to 3D random distribution in both magnetic field and quadrupole splitting.
    texture (float): Texture coefficient between 0 and 1.

      .. versionadded:: 1.2.0

    lamb_moessbauer (float): Lamb-Moessbauer factor of the hyperfine site.

      .. versionadded:: 1.2.0

    broadening (float): Broadening parameter of the site. It multiplies with the linewidth.

      .. versionadded:: 1.2.0


Attributes:
    weight (float): Relative weight of the site, > 0.
    isomer (float): Isomer shift (mm/s).
    magnetic_field (float): Magnetic hyperfine field amplitude (Tesla). 
    magnetic_theta (float): Polar angle of the magnetic field with respect to the beam propagation direction - photon :math:`k` vector (rad).
    magnetic_phi (float): Azimuthal angle of the magnetic field with respect to the :math:`sigma` direction of the beam (rad).
    quadrupole (float): Quadrupole splitting amplitude (mm/s).
    quadrupole_alpha (float): Euler angle of the electric field gradient :math:`\alpha` in extrinsic ZYZ convention (rad).
    quadrupole_beta (float): Euler angle of the electric field gradient :math:`\beta` in extrinsic ZYZ convention (rad).
    quadrupole_gamma (float): Euler angle of the electric field gradient :math:`\gamma` in extrinsic ZYZ convention (rad).
    quadrupole_asymmetry (float): Asymmetry parameter of the electric field gradient, 0 to 1.
    isotropic (bool): If *True* the hyperfine site is set to 3D random distribution in both magnetic field and quadrupole splitting.
    texture (float): Texture coefficient between 0 and 1.
      If isotropic is ``True`` it is not taken into account.

      .. versionadded:: 1.2.0

    lamb_moessbauer (float): Lamb-Moessbauer factor of the hyperfine site.
      Not used if the :attr:`lamb_moessbauer` of the material is ``None``.

      .. versionadded:: 1.2.0

    broadening (float): Broadening parameter of the site. It multiplies with the linewidth.

      .. versionadded:: 1.2.0
*/
struct BareHyperfine {
public:
  BareHyperfine(
    const double weight = 0.0,
    const double isomer = 0.0,
    const double magnetic_field = 0.0,
    const double magnetic_theta = 0.0,
    const double magnetic_phi = 0.0,
    const double quadrupole = 0.0,
    const double quadrupole_alpha = 0.0,
    const double quadrupole_beta = 0.0,
    const double quadrupole_gamma = 0.0,
    const double quadrupole_asymmetry = 0.0,
    const double texture = 1.0,
    const double lamb_moessbauer = 0.0,
    const double broadening = 1.0,
    const bool isotropic = false
  ) :
    weight(weight),
    isomer(isomer),
    magnetic_field(magnetic_field),
    magnetic_theta(magnetic_theta * constants::kDegToRad),
    magnetic_phi(magnetic_phi * constants::kDegToRad),
    quadrupole(quadrupole),
    quadrupole_alpha(quadrupole_alpha * constants::kDegToRad),
    quadrupole_beta(quadrupole_beta * constants::kDegToRad),
    quadrupole_gamma(quadrupole_gamma * constants::kDegToRad),
    quadrupole_asymmetry(quadrupole_asymmetry),
    texture(texture),
    lamb_moessbauer(lamb_moessbauer),
    broadening(broadening),
    isotropic(isotropic)    
  {};

  double weight = 0.0;   // relative weight
  
  double isomer = 0.0;   // mm/s
  
  double magnetic_field = 0.0;  // T
  
  double magnetic_theta = 0.0;  // rad
  
  double magnetic_phi = 0.0;  // rad
  
  double quadrupole = 0.0;   // mm/s
  
  double quadrupole_alpha = 0.0;  // rad
  
  double quadrupole_beta = 0.0;  // rad
  
  double quadrupole_gamma = 0.0;  // rad
  
  double quadrupole_asymmetry = 0.0;
  
  double texture = 1.0;

  double lamb_moessbauer = 0.0;

  double broadening = 1.0;
  
  bool isotropic = 0.0;
};

#endif // NEXUS_BARE_HYPERFINE_H_
