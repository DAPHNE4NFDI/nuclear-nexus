// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module material

%{
    #define SWIG_FILE_WITH_INIT
    #include "material.h"
%}


%feature("shadow") Material::Material %{
  def __init__(self,
               composition,
               density,
               isotope = None,
               abundance = 0.0,
               lamb_moessbauer = 0.0,
               hyperfine_sites = [],
               id = ""):

    if isinstance(density, (int, float)):
      density = Var(density, 0.0, 23.0, False, "")

    if isinstance(abundance, (int, float)):
      abundance = Var(abundance, 0.0, 1.0, False, "")
      
    if isinstance(lamb_moessbauer, (int, float)):
      lamb_moessbauer = Var(lamb_moessbauer, 0.0, 1.0, False, "")

    if isotope is None:
      isotope = MoessbauerIsotope()

    if isinstance(hyperfine_sites, (Hyperfine)):
      hyperfine_sites = [hyperfine_sites]

    self._composition = composition
    self._density = density
    self._isotope = isotope
    self._abundance = abundance
    self._lamb_moessbauer = lamb_moessbauer

    # the python class object must reference to the python list with objects pointers
    # in order to maintain securely in the python domain 
    self._hyperfine_sites = hyperfine_sites
   
    _cnexus.Material_swiginit(self, _cnexus.new_Material(composition, density, isotope, abundance, lamb_moessbauer, hyperfine_sites, id))
%}



%include "material.h"



%extend Material{
  %pythoncode{
    
  def __setattr__(self, attr, val):
    if attr not in ("this", "_composition", "_density", "_isotope", "_abundance", "_lamb_moessbauer", "_hyperfine_sites") and not hasattr(self, attr):
      raise Exception("Material does not have attribute {}".format(attr))
    
    if attr in ("density", "abundance", "lamb_moessbauer") and isinstance(val, (int, float)):
      setattr(getattr(self, attr), "value", val)
      return

    if attr == "density":
      self._density = val

    if attr == "isotope":
      self._isotope = val

    if attr == "abundance":
      self._abundance = val

    if attr == "lamb_moessbauer":
      self._lamb_moessbauer = val

    if attr == "composition":
      self._composition = val  # ref to python list
      val = Composition(val)
    
    if attr == "hyperfine_sites":
      if isinstance(val, (list, tuple)):
        self._hyperfine_sites = val  # ref to python list
        val = HyperfineSites(val)

    super().__setattr__(attr, val)

  def __repr__(self):
    self.Update()
    output = "Material\n"
    output += "  .id: {}\n".format(self.id)
    output += "  .composition:"
    for i in range(len(self.composition)):
      output += "  {} {}".format(self.composition[i][0], self.composition[i][1])
    output += "\n"
    output += "  .density (g/cm^3) {}\n".format(self.density)
    output += "  .isotope: {}\n".format(self.isotope.isotope)
    output += "  .abundance {}\n".format(self.abundance)
    output += "  .lamb_moessbauer {}\n".format(self.lamb_moessbauer)
    output += "    derived parameters:\n"
    output += "    .total_number_density (1/m^3) = {}\n".format(self.total_number_density)
    output += "    .average_mole_mass (g/mole) = {}\n".format(self.average_mole_mass)
    output += "    .isotope_number_density (1/m^3) = {}\n".format(self.isotope_number_density)
    output += "    number of hyperfine sites {}\n".format(len(self.hyperfine_sites))
    return output


  def Copy(self, ref_hyperfine = True):
    r"""
    Copy the material.

    Args:
       ref_hyperfine (bool): Determines whether the hyperfine references should be copied as well (True) or not (False).
          The hyperfine sites are not copied, just the reference is passed to the new object.
          Changing values in the hyperfine sites will affect both the original and the copied ``Material`` object.
      
    Returns:
       Material: Copy of the material.
    """
    new_material = Material(self.composition, density = self.density.Copy(), isotope = self.isotope, abundance = self.abundance.Copy(),
                            lamb_moessbauer = self.lamb_moessbauer.Copy(), id = self.id)
    if ref_hyperfine:
        new_material.hyperfine_sites = HyperfineSites(self.hyperfine_sites)
    new_material.Update()
    return new_material


  @staticmethod
  def Template(material):
    r"""
    Returns a copy of the material from a template of the ``lib.material``.

    Args:
       Material: Material from ``nexus.lib.material``.
    """
    return material.Copy()

  }
}
