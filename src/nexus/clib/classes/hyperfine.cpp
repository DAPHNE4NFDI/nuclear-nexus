// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "hyperfine.h"

#include <iostream>
#include <algorithm>
#include <random>
#include <array>

#include "../nexus_definitions.h"
#include "../math/coordinates.h"
#include "../utilities/errors.h"



Hyperfine::Hyperfine(
  Var* const weight_,
  Var* const isomer_,
  Var* const magnetic_field_,
  Var* const magnetic_theta_,
  Var* const magnetic_phi_,
  Var* const quadrupole_,
  Var* const quadrupole_alpha_,
  Var* const quadrupole_beta_,
  Var* const quadrupole_gamma_,
  Var* const quadrupole_asymmetry_,
  Var* const texture_,
  Var* const lamb_moessbauer_,
  Var* const broadening_,
  Distribution* const isomer_dist_,
  Distribution* const magnetic_field_dist_,
  Distribution* const magnetic_theta_dist_,
  Distribution* const magnetic_phi_dist_,
  Distribution* const quadrupole_dist_,
  Distribution* const quadrupole_alpha_dist_,
  Distribution* const quadrupole_beta_dist_,
  Distribution* const quadrupole_gamma_dist_,
  Distribution* const quadrupole_asymmetry_dist_,
  const bool isotropic_, 
  const std::string id_
) :
  weight(weight_),
  isomer(isomer_),
  magnetic_field(magnetic_field_),
  magnetic_theta(magnetic_theta_),
  magnetic_phi(magnetic_phi_),
  quadrupole(quadrupole_),
  quadrupole_alpha(quadrupole_alpha_),
  quadrupole_beta(quadrupole_beta_),
  quadrupole_gamma(quadrupole_gamma_),
  quadrupole_asymmetry(quadrupole_asymmetry_),
  texture(texture_),
  lamb_moessbauer(lamb_moessbauer_),
  broadening(broadening_),
  isomer_dist_ptr(isomer_dist_),
  magnetic_field_dist_ptr(magnetic_field_dist_),
  magnetic_theta_dist_ptr(magnetic_theta_dist_),
  magnetic_phi_dist_ptr(magnetic_phi_dist_),
  quadrupole_dist_ptr(quadrupole_dist_),
  quadrupole_alpha_dist_ptr(quadrupole_alpha_dist_),
  quadrupole_beta_dist_ptr(quadrupole_beta_dist_),
  quadrupole_gamma_dist_ptr(quadrupole_gamma_dist_),
  quadrupole_asymmetry_dist_ptr(quadrupole_asymmetry_dist_),
  isotropic(isotropic_),
  id(id_)
{
  if (weight_->value < 0)
  {
    errors::WarningMessage("Hyperfine "+id, "hyperfine weight must be >= 0. Parameter set to zero.");

    weight->value = 0;
  }
  
  if (quadrupole_asymmetry_->value < 0 &&
      quadrupole_asymmetry_->value > 1)
  {
    errors::WarningMessage("Hyperfine", "quadrupole asymmetry must be in range[0, 1]. Parameter set to zero.");

    quadrupole_asymmetry->value = 0;
  }

  if (texture_->value < 0 &&
      texture_->value > 1)
  {
    errors::WarningMessage("Hyperfine", "texture must be in range[0, 1]. Parameter set to zero.");

    texture->value = 0;
  }

  if (lamb_moessbauer_ != nullptr)
  {
    if (lamb_moessbauer_->value < 0 &&
        lamb_moessbauer_->value > 1)
    {
      errors::WarningMessage("Hyperfine", "lamb_moessbauer must be in range[0, 1]. Parameter set to zero.");

      lamb_moessbauer->value = 0;
    }
  }

  if (broadening_ != nullptr)
  {
    if (broadening_->value < 1)
    {
      errors::WarningMessage("Hyperfine", "broadening smaller 1, must be >=1 . Parameter set to 1.");

      broadening->value = 1.0;
    }
  }

  Update();
}


std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetDistribution(const Var* const target, Distribution* const dist) const
{
  dist->DistributionFunction();

  std::pair<std::vector<double>, std::vector<double>> pair(dist->delta, dist->weight);

  const double value = target->value;

  std::for_each(pair.first.begin(), pair.first.end(), [value](double& elem)
    {
      elem += value;
    }
  );

  return pair;
}


void Hyperfine::SetIsomerDistribution(Distribution* const dist_ptr)
{
  isomer_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetIsomerDistribution() const
{
  return GetDistribution(isomer, isomer_dist_ptr);
}


void Hyperfine::SetMagneticFieldDistribution(Distribution* const dist_ptr)
{
  magnetic_field_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetMagneticFieldDistribution() const
{
  return GetDistribution(magnetic_field, magnetic_field_dist_ptr);
}


void Hyperfine::SetMagneticThetaDistribution(Distribution* const dist_ptr)
{
  magnetic_theta_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetMagneticThetaDistribution() const
{
  return GetDistribution(magnetic_theta, magnetic_theta_dist_ptr);
}


void Hyperfine::SetMagneticPhiDistribution(Distribution* const dist_ptr)
{
  magnetic_phi_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetMagneticPhiDistribution() const
{
  return GetDistribution(magnetic_phi, magnetic_phi_dist_ptr);
}


void Hyperfine::SetQuadrupoleDistribution(Distribution* const dist_ptr)
{
  quadrupole_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetQuadrupoleDistribution() const
{
  return GetDistribution(quadrupole, quadrupole_dist_ptr);
}


void Hyperfine::SetQuadrupoleAlphaDistribution(Distribution* const dist_ptr)
{
  quadrupole_alpha_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetQuadrupoleAlphaDistribution() const
{
  return GetDistribution(quadrupole_alpha, quadrupole_alpha_dist_ptr);
}


void Hyperfine::SetQuadrupoleBetaDistribution(Distribution* const dist_ptr)
{
  quadrupole_beta_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetQuadrupoleBetaDistribution() const
{
  return GetDistribution(quadrupole_beta, quadrupole_beta_dist_ptr);
}


void Hyperfine::SetQuadrupoleGammaDistribution(Distribution* const dist_ptr)
{
  quadrupole_gamma_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetQuadrupoleGammaDistribution() const
{
  return GetDistribution(quadrupole_gamma, quadrupole_gamma_dist_ptr);
}


void Hyperfine::SetQuadrupoleAsymmetryDistribution(Distribution* const dist_ptr)
{
  quadrupole_asymmetry_dist_ptr = dist_ptr;
}

std::pair<std::vector<double>, std::vector<double>> Hyperfine::GetQuadrupoleAsymmetryDistribution() const
{
  return GetDistribution(quadrupole_asymmetry, quadrupole_asymmetry_dist_ptr);
}


void Hyperfine::SetRandomDistribution(const std::string target, const std::string type, const int points, const std::string method, const int order)
{
  if (points < 1)
  {
    errors::ErrorMessage("SetRandomDistribution", "distribution points must be greater than zero");
    
    return;
  }

  if (order < 1 ||
      order > 6)
  {
    errors::ErrorMessage("SetRandomDistribution", "order parameter must be 1 to 6");

    return;
  }

  if (!(method == "random"||
        method == "model"))
  {
    errors::ErrorMessage("SetRandomDistribution", "distribution method " + method + " not valid");

    return;
  }

  if (!(
    type == "none" ||
    type == "3D"   ||
    type == "2Dks" ||
    type == "2Dkp" ||
    type == "2Dsp" ||
    type == "2Dsk" ||
    type == "2Dpk" ||
    type == "2Dps"))
  {
    errors::ErrorMessage("SetRandomDistribution", "distribution type " + type + " not valid");

    return;
  }

  if (target == "none")
  {
    distribution_target_magnetic = "none";
    distribution_type_magnetic = "none";
    distribution_method_magnetic = "model";
    distribution_points_magnetic = 1;
    distribution_order_magnetic = 1;

    distribution_target_quadrupole = "none";
    distribution_type_quadrupole = "none";
    distribution_method_quadrupole = "model";
    distribution_points_quadrupole = 1;
    distribution_order_quadrupole = 1;
  }
  else if (target == "mag")
  {
    distribution_target_magnetic = target;
    distribution_type_magnetic = type;
    distribution_method_magnetic = method;
    distribution_points_magnetic = points;
    distribution_order_magnetic = order;
  }
  else if (target == "efg")
  {
    distribution_target_quadrupole = target;
    distribution_type_quadrupole = type;
    distribution_method_quadrupole = method;
    distribution_points_quadrupole = points;
    distribution_order_quadrupole = order;
  }
  else
  {
    errors::WarningMessage("SetRandomDistribution", "distribution target " + target + " not valid.");

    return;
  }  
}


void Hyperfine::ClearRandomDistribution(const std::string target)
{
  if (target == "mag")
  {
    distribution_target_magnetic = "none";
    distribution_type_magnetic = "none";
    distribution_method_magnetic = "model";
    distribution_points_magnetic = 1;
    distribution_order_magnetic = 1;
  }
  else if (target == "efg")
  {
    distribution_target_quadrupole = "none";
    distribution_type_quadrupole = "none";
    distribution_method_quadrupole = "model";
    distribution_points_quadrupole = 1;
    distribution_order_quadrupole = 1;
  }
  else if (target == "all")
  {
    distribution_target_magnetic = "none";
    distribution_type_magnetic = "none";
    distribution_method_magnetic = "model";
    distribution_points_magnetic = 1;
    distribution_order_magnetic = 1;

    distribution_target_quadrupole = "none";
    distribution_type_quadrupole = "none";
    distribution_method_quadrupole = "model";
    distribution_points_quadrupole = 1;
    distribution_order_quadrupole = 1;
  }
  else
  {
    errors::WarningMessage("ClearRandomDistribution", "target " + target + " not valid.");
  }
}

//this is the function call of the virtual function defined in Python, the range deltax is set priorly by Distribution class
void Hyperfine::CallDistributionFunction(Distribution* const dist) const
{
  if (dist != nullptr)
    dist->DistributionFunction();
}

// get and calculate the range and weights from the class Distribution and store in class Hyperfine instance
void Hyperfine::CalcDistribution(Distribution* const dist_ptr, const double value, std::vector<double>* dist_values,
  std::vector<double>* dist_weight)
{
  (*dist_values).clear();
  (*dist_weight).clear();

  // if no pointer is set ignore distribution and set only the Var::value
  if (dist_ptr == nullptr)
  {
    (*dist_values).push_back(value);
  
    (*dist_weight).push_back(1.0);
  }
  // calculate the distribution from Python and apply value offset to dist
  else
  {   
    CallDistributionFunction(dist_ptr);

    *dist_weight = dist_ptr->weight;
    *dist_values = dist_ptr->delta;

    std::for_each((*dist_values).begin(), (*dist_values).end(), [value](double& elem)
      {
        elem += value;
      }
    );

  }
}

void Hyperfine::CalcAllDistributions()
{
  /* prior to 
  * 
  // no distribution is calculated for random isotropic angles. Is set in SetBareHyperfineRandom
  if (isotropic == true)
  {
    magnetic_theta_dist_ptr = nullptr;
    magnetic_phi_dist_ptr = nullptr;

    quadrupole_alpha_dist_ptr = nullptr;
    quadrupole_beta_dist_ptr = nullptr;
    quadrupole_gamma_dist_ptr = nullptr;
    quadrupole_asymmetry_dist_ptr = nullptr;

    distribution_target_magnetic = "none";
    distribution_type_magnetic = "none";

    distribution_target_quadrupole = "none";
    distribution_type_quadrupole = "none";
  }
  */

  // distribution target validities are checked in SetRandomDistribution
  if (distribution_target_magnetic == "mag")
  {
    magnetic_theta_dist_ptr = nullptr;
    magnetic_phi_dist_ptr = nullptr;
  }

  if (distribution_target_quadrupole == "efg")
  {
    quadrupole_alpha_dist_ptr = nullptr;
    quadrupole_beta_dist_ptr = nullptr;
    quadrupole_gamma_dist_ptr = nullptr;
  }  

  // for each nullptr the value is simply set to the Var::value
  // for random distributions in mag or efg angles the angular values are set in SetBareHyperfineRandom
  CalcDistribution(isomer_dist_ptr, isomer->value, &isomer_dist, &isomer_dist_weight);

  CalcDistribution(magnetic_field_dist_ptr, magnetic_field->value, &magnetic_field_dist, &magnetic_field_dist_weight);

  CalcDistribution(magnetic_theta_dist_ptr, magnetic_theta->value, &magnetic_theta_dist, &magnetic_theta_dist_weight);

  CalcDistribution(magnetic_phi_dist_ptr, magnetic_phi->value, &magnetic_phi_dist, &magnetic_phi_dist_weight);

  CalcDistribution(quadrupole_dist_ptr, quadrupole->value, &quadrupole_dist, &quadrupole_dist_weight);

  CalcDistribution(quadrupole_alpha_dist_ptr, quadrupole_alpha->value, &quadrupole_alpha_dist, &quadrupole_alpha_dist_weight);

  CalcDistribution(quadrupole_beta_dist_ptr, quadrupole_beta->value, &quadrupole_beta_dist, &quadrupole_beta_dist_weight);

  CalcDistribution(quadrupole_gamma_dist_ptr, quadrupole_gamma->value, &quadrupole_gamma_dist, &quadrupole_gamma_dist_weight);

  CalcDistribution(quadrupole_asymmetry_dist_ptr, quadrupole_asymmetry->value, &quadrupole_asymmetry_dist, &quadrupole_asymmetry_dist_weight);
}


// the angles have to be converted here because the BareHyperfine constructor is not used
std::vector<BareHyperfine> Hyperfine::HyperfineCombinations(const std::string target, std::vector<double> &dist_values,
  std::vector<double> &dist_weights, const std::vector<BareHyperfine> inputhyper)
{
  const size_t inputhyper_size = inputhyper.size();
  const size_t dist_values_size = dist_values.size();

  std::vector<BareHyperfine> hyperfine_combinations{};

  BareHyperfine inplace_hyperfine;

//  for (size_t j = 0; j < inputhyper_size; ++j)
  for (const BareHyperfine& hyp : inputhyper)
  {
    for (size_t i = 0; i < dist_values_size; ++i)
    {
      //inplace_hyperfine = inputhyper[j];
      inplace_hyperfine = hyp;

      inplace_hyperfine.weight *= dist_weights[i];

      if (target == "isomer")
      {
        inplace_hyperfine.isomer = dist_values[i];
      }
      else if (target == "magnetic_field")
      {
        inplace_hyperfine.magnetic_field = dist_values[i];
      }
      else if (target == "magnetic_theta")
      {
        inplace_hyperfine.magnetic_theta = dist_values[i] * constants::kDegToRad;
      }
      else if (target == "magnetic_phi")
      {
        inplace_hyperfine.magnetic_phi = dist_values[i] * constants::kDegToRad;
      }
      else if (target == "quadrupole")
      {
        inplace_hyperfine.quadrupole = dist_values[i];
      }
      else if (target == "quadrupole_alpha")
      {
        inplace_hyperfine.quadrupole_alpha = dist_values[i] * constants::kDegToRad;
      }
      else if (target == "quadrupole_beta")
      {
        inplace_hyperfine.quadrupole_beta = dist_values[i] * constants::kDegToRad;
      }
      else if (target == "quadrupole_gamma")
      {
        inplace_hyperfine.quadrupole_gamma = dist_values[i] * constants::kDegToRad;
      }
      else if (target == "quadrupole_asymmetry")
      {
        inplace_hyperfine.quadrupole_asymmetry = dist_values[i];
      }

      hyperfine_combinations.push_back(inplace_hyperfine);
    }
  }

  return hyperfine_combinations;
}


void Hyperfine::SetBareHyperfines()
{
  // set weight to the value of the specific hyperfine site
  BareHyperfines.assign(1, BareHyperfine(this->weight->value,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    this->texture->value,
    this->lamb_moessbauer->value,
    this->broadening->value,
    this->isotropic)
  );

  BareHyperfines = HyperfineCombinations("isomer", isomer_dist, isomer_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("magnetic_field", magnetic_field_dist, magnetic_field_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("magnetic_theta", magnetic_theta_dist, magnetic_theta_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("magnetic_phi", magnetic_phi_dist, magnetic_phi_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("quadrupole", quadrupole_dist, quadrupole_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("quadrupole_alpha", quadrupole_alpha_dist, quadrupole_alpha_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("quadrupole_beta", quadrupole_beta_dist, quadrupole_beta_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("quadrupole_gamma", quadrupole_gamma_dist, quadrupole_gamma_dist_weight, BareHyperfines);

  BareHyperfines = HyperfineCombinations("quadrupole_asymmetry", quadrupole_asymmetry_dist, quadrupole_asymmetry_dist_weight, BareHyperfines);
}


std::vector<BareHyperfine> Hyperfine::HyperfineRandomMagnetic(const std::string type, const int points, const std::string method, const int order,
  const std::vector<BareHyperfine> inputhyper)
{ 
  int num_points = points;

  std::vector<coordinates::Polar> random_vector_polar{};

  std::vector<coordinates::Spherical> random_vector_spherical{};

  if (type == "3D")
  {
    if (method == "model")
    {
      random_vector_spherical = coordinates::AverageSphereSpherical(order);
    }
    else
    {
      random_vector_spherical = coordinates::RandomPMSpherical(static_cast<int>(round(points / 48.0)));
    }

    num_points = static_cast<int>(random_vector_spherical.size());

    distribution_actual_points_magnetic = num_points;
  }
  else
  {
    if (method == "model")
    {
      random_vector_polar = coordinates::AverageCirclePolar();
      //random_vector_polar = coordinates::FixedCirclePolar();
    }
    else
    {
      random_vector_polar = coordinates::RandomPMPolar(static_cast<int>(round(points / 8.0)));
    }

    num_points = static_cast<int>(random_vector_polar.size());
    
    distribution_actual_points_magnetic = num_points;
  }

  std::vector<BareHyperfine> hyperfine_combinations{};

  BareHyperfine inplace_hyperfine{};

  for (const BareHyperfine& hyper: inputhyper)
  {
    inplace_hyperfine = hyper;

    inplace_hyperfine.weight /= num_points;

    if (type == "3D")
    {
      for (const coordinates::Spherical& random_spherical : random_vector_spherical)
      {
        inplace_hyperfine.magnetic_theta = random_spherical.theta;
        inplace_hyperfine.magnetic_phi = random_spherical.phi;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
    else if (type == "2Dks" ||
             type == "2Dsk")
    {
      for (const coordinates::Polar& random_polar : random_vector_polar)
      {
        inplace_hyperfine.magnetic_theta = random_polar.phi;
        inplace_hyperfine.magnetic_phi = 0.0;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
    else if (type == "2Dkp" ||
             type == "2Dpk")
    {
      for (const coordinates::Polar& random_polar : random_vector_polar)
      {
        inplace_hyperfine.magnetic_theta = random_polar.phi;
        inplace_hyperfine.magnetic_phi = 90.0 * constants::kDegToRad;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
    else if (type == "2Dsp" ||
             type == "2Dps")
    {
      for (const coordinates::Polar& random_polar : random_vector_polar)
      {
        inplace_hyperfine.magnetic_theta = 90.0 * constants::kDegToRad;
        inplace_hyperfine.magnetic_phi = random_polar.phi;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
  }

  return hyperfine_combinations;
}


std::vector<BareHyperfine> Hyperfine::HyperfineRandomQuadrupole(const std::string type, const int points,
  const std::string method, const int order, const std::vector<BareHyperfine> inputhyper)
{
  int num_points = points;

  std::vector<coordinates::Cartesian2D> random_vector_cartesian2D{};

  std::vector<coordinates::Cartesian3D> random_vector_cartesian3D{};

  if (type == "3D")
  {
    if (method == "model")
    {
      random_vector_cartesian3D = coordinates::AverageSphereCartesian(order);
    }
    else
    {
      random_vector_cartesian3D = coordinates::RandomPMCartesian3D(static_cast<int>(round(points / 48.0)));
    }

    num_points = static_cast<int>(random_vector_cartesian3D.size());
  }
  else
  {
    if (method == "model")
    {
      random_vector_cartesian2D = coordinates::AverageCircleCartesian();
    }
    else
    {
      random_vector_cartesian2D = coordinates::RandomPMCartesian2D(static_cast<int>(round(points / 8.0)));
    }

    num_points = static_cast<int>(random_vector_cartesian2D.size());
  }

  distribution_actual_points_quadrupole = num_points;

  std::vector<BareHyperfine> hyperfine_combinations{};

  BareHyperfine inplace_hyperfine{};

  coordinates::Cartesian3D cartesian{};   // (sigma, pi, k) basis

  coordinates::Euler euler{};

  for (const BareHyperfine& hyper : inputhyper)
  {
    inplace_hyperfine = hyper;

    inplace_hyperfine.weight /= (num_points);

    // Extrinsic ZYZ convention. (for intrinsic change alpha and gamma values)
    // So, first rotation is around k, then pi, then k again

    if (type == "3D")
    {
      for (const coordinates::Cartesian3D& random_vector : random_vector_cartesian3D)
      {
        euler.FromVector(random_vector);

        inplace_hyperfine.quadrupole_alpha = euler.alpha;
        inplace_hyperfine.quadrupole_beta = euler.beta;
        inplace_hyperfine.quadrupole_gamma = euler.gamma;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
    else if (type == "2Dks" ||
             type == "2Dsk")
    {
      for (const coordinates::Cartesian2D& random_vector : random_vector_cartesian2D)
      {
        cartesian = {random_vector.x, 0.0, random_vector.y};

        euler.FromVector(cartesian);

        inplace_hyperfine.quadrupole_alpha = euler.alpha;
        inplace_hyperfine.quadrupole_beta = euler.beta;
        inplace_hyperfine.quadrupole_gamma = euler.gamma;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
    else if (type == "2Dkp" ||
             type == "2Dpk")
    {
      for (const coordinates::Cartesian2D& random_vector : random_vector_cartesian2D)
      {
        cartesian = { 0.0, random_vector.x, random_vector.y };

        euler.FromVector(cartesian);

        inplace_hyperfine.quadrupole_alpha = euler.alpha;
        inplace_hyperfine.quadrupole_beta = euler.beta;
        inplace_hyperfine.quadrupole_gamma = euler.gamma;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
    else if (type == "2Dsp" ||
             type == "2Dps")
    {
      for (const coordinates::Cartesian2D& random_vector : random_vector_cartesian2D)
      {
        cartesian = { random_vector.x, random_vector.y, 0.0 };

        euler.FromVector(cartesian);

        inplace_hyperfine.quadrupole_alpha = euler.alpha;
        inplace_hyperfine.quadrupole_beta = euler.beta;
        inplace_hyperfine.quadrupole_gamma = euler.gamma;

        hyperfine_combinations.push_back(inplace_hyperfine);
      }
    }
  }

  return hyperfine_combinations;
}


void Hyperfine::SetBareHyperfineRandom()
{
  if (distribution_target_magnetic == "mag" ||
      distribution_type_magnetic != "none")
  {
    magnetic_theta->fit = false;
    magnetic_phi->fit = false;
    
    BareHyperfines = HyperfineRandomMagnetic(distribution_type_magnetic, distribution_points_magnetic,
      distribution_method_magnetic, distribution_order_magnetic, BareHyperfines);
  }

  if (distribution_target_quadrupole == "efg" ||
      distribution_type_quadrupole != "none")
  {
    quadrupole_alpha->fit = false;
    quadrupole_beta->fit = false;
    quadrupole_gamma->fit = false;

    BareHyperfines = HyperfineRandomQuadrupole(distribution_type_quadrupole, distribution_points_quadrupole, 
      distribution_method_quadrupole, distribution_order_quadrupole, BareHyperfines);
  }
}

void Hyperfine::Update()
{
  // here the distributions are calculated from the python definition
  // for isotropic angular random distributions, the corresponding distribution pointers have be set to zero
  CalcAllDistributions(); 

  SetBareHyperfines();

  SetBareHyperfineRandom();
}


void Hyperfine::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(weight);

  fit_handler->PrepareFitVariable(isomer);

  fit_handler->PrepareFitVariable(magnetic_field);

  fit_handler->PrepareFitVariable(magnetic_theta);

  fit_handler->PrepareFitVariable(magnetic_phi);

  fit_handler->PrepareFitVariable(quadrupole);

  fit_handler->PrepareFitVariable(quadrupole_alpha);

  fit_handler->PrepareFitVariable(quadrupole_beta);

  fit_handler->PrepareFitVariable(quadrupole_gamma);

  fit_handler->PrepareFitVariable(quadrupole_asymmetry);

  fit_handler->PrepareFitVariable(texture);

  fit_handler->PrepareFitVariable(lamb_moessbauer);

  fit_handler->PrepareFitVariable(broadening);

  if (isomer_dist_ptr != nullptr)
  {
    for (Var* var : isomer_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (magnetic_field_dist_ptr != nullptr)
  {
    for (Var* var : magnetic_field_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (magnetic_theta_dist_ptr != nullptr)
  {
    for (Var* var : magnetic_theta_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (magnetic_phi_dist_ptr != nullptr)
  {
    for (Var* var : magnetic_phi_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (quadrupole_dist_ptr != nullptr)
  {
    for (Var* var : quadrupole_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (quadrupole_alpha_dist_ptr != nullptr)
  {
    for (Var* var : quadrupole_alpha_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (quadrupole_beta_dist_ptr != nullptr)
  {
    for (Var* var : quadrupole_beta_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (quadrupole_gamma_dist_ptr != nullptr)
  {
    for (Var* var : quadrupole_gamma_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  if (quadrupole_asymmetry_dist_ptr != nullptr)
  {
    for (Var* var : quadrupole_asymmetry_dist_ptr->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

}
