// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Maps relate the string identifier to the atomic number an the atomic mass of an element. 


#ifndef NEXUS_ELEMENTMAPS_H_
#define NEXUS_ELEMENTMAPS_H_

#include <string>
#include <map>


namespace element_maps {
  
  // ATOMIC NUMBER Z
  static const std::map<const std::string, const int> atomic_number_map = {
    {"", 0},
    // 1st period, Z = 1 to 2 
    {"H", 1} , {"He", 2},
    // 2nd period, Z = 3 to 10
    {"Li", 3}, {"Be", 4}, {"B", 5}, {"C", 6}, {"N", 7}, {"O", 8}, {"F", 9}, {"Ne", 10},
    // 3rd period, Z = 11 to 18
    {"Na", 11}, {"Mg", 12}, {"Al", 13}, {"Si", 14}, {"P", 15}, {"S", 16}, {"Cl", 17},
    {"Ar", 18},
    // 4th period, Z = 19 to 36
    {"K", 19}, {"Ca", 20}, {"Sc", 21}, {"Ti", 22}, {"V", 23}, {"Cr", 24}, {"Mn", 25},
    {"Fe", 26}, {"Co", 27}, {"Ni", 28}, {"Cu", 29}, {"Zn", 30}, {"Ga", 31}, {"Ge", 32},
    {"As", 33}, {"Se", 34}, {"Br", 35}, {"Kr", 36},
    // 5th period, Z = 37 to 54
    {"Rb", 37}, {"Sr", 38}, {"Y", 39}, {"Zr", 40}, {"Nb", 41}, {"Mo", 42}, {"Tc", 43},
    {"Ru", 44}, {"Rh", 45}, {"Rd", 46}, {"Ag", 47}, {"Cd", 48}, {"In", 49}, {"Sn", 50},
    {"Sb", 51}, {"Te", 52}, {"I", 53}, {"Xe", 54},
    // 6th period, Z = 55 to 86
    {"Cs", 55}, {"Ba", 56}, {"La", 57}, {"Ce", 58}, {"Pr", 59}, {"Nd", 60}, {"Pm", 61},
    {"Sm", 62}, {"Eu", 63}, {"Gd", 64}, {"Tb", 65}, {"Dy", 66}, {"Ho", 67}, {"Er", 68},
    {"Tm", 69}, {"Yb", 70}, {"Lu", 71}, {"Hf", 72}, {"Ta", 73}, {"W", 74}, {"Re", 75},
    {"Os", 76}, {"Ir", 77}, {"Pt", 78}, {"Au", 79}, {"Hg", 80}, {"Tl", 81}, {"Pb", 82},
    {"Bi", 83}, {"Po", 84}, {"At", 85}, {"Rn", 86},
    // 7th period, Z >= 87
    // not included
  };

  // ATOMIC MASSES (g/mole)
  static const std::map<const std::string, const double> atomic_mass_map = {
    {"", 0.0},
    // 1st period, Z = 1 to 2
    {"H", 1.008} , {"He", 4.002602},
    // 2nd period, Z = 3 to 10
    {"Li", 6.94}, {"Be", 9.0121831}, {"B", 10.81}, {"C", 12.011}, {"N", 14.007}, {"O", 15.999},
    {"F", 18.998403163}, {"Ne", 20.1797},
    // 3rd period, Z = 11 to 18
    {"Na", 22.98976928}, {"Mg", 24.305}, {"Al", 26.9815384}, {"Si", 28.085}, {"P", 30.973761998}, {"S", 32.06},
    {"Cl", 35.45}, {"Ar", 39.95},
    // 4th period, Z = 19 to 36
    {"K", 39.0983}, {"Ca", 40.078}, {"Sc", 44.955908}, {"Ti", 47.867}, {"V", 50.9415}, {"Cr", 51.9961}, {"Mn", 54.938043},
    {"Fe", 55.845}, {"Co", 58.933194}, {"Ni", 58.6934}, {"Cu", 63.546}, {"Zn", 65.38}, {"Ga", 69.723}, {"Ge", 72.630},
    {"As", 74.921595}, {"Se", 78.971}, {"Br", 79.904}, {"Kr", 83.798},
    // 5th period, Z = 37 to 54
    {"Rb", 85.4678}, {"Sr", 87.62}, {"Y", 88.90584}, {"Zr", 91.224}, {"Nb", 92.90637}, {"Mo", 95.95}, {"Tc", 98.9062},
    {"Ru", 101.07}, {"Rh", 102.90549}, {"Rd", 106.42}, {"Ag", 107.8682}, {"Cd", 112.414}, {"In", 114.818}, {"Sn", 118.710},
    {"Sb", 121.760}, {"Te", 127.60}, {"I", 126.90447}, {"Xe", 131.293},
    // 6th period, Z = 55 to 86
    {"Cs", 132.90545196}, {"Ba", 137.327}, {"La", 138.90547}, {"Ce", 140.116}, {"Pr", 140.90766}, {"Nd", 144.242}, {"Pm", 145.0},
    {"Sm", 150.36}, {"Eu", 151.964}, {"Gd", 157.25}, {"Tb", 158.925354}, {"Dy", 162.500}, {"Ho", 164.930328}, {"Er", 167.259},
    {"Tm", 168.934218}, {"Yb", 173.045}, {"Lu", 174.9668}, {"Hf", 178.486}, {"Ta", 180.94788}, {"W", 183.84}, {"Re", 186.207},
    {"Os", 190.23}, {"Ir", 192.217}, {"Pt", 195.084}, {"Au", 196.966570}, {"Hg", 200.592}, {"Tl", 204.38}, {"Pb", 207.2},
    {"Bi", 208.98040}, {"Po", 209.0}, {"At", 210.0}, {"Rn", 222.0},
    // 7th period, Z >= 87
    // not included
  };

}  // namespace element_maps

#endif //NEXUS_ELEMENTMAPS_H_
