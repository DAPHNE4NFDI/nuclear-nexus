// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Analyzer definition. 
//
// An analyzer describes a photon polarization selective element in the beam path.
// An analyzer is of Jones matrix type.
// 
// Taken from Sturhahn Hyperfine Interactions 125, 149 (2000)
// The given canting angle seems to be wrong in Eq (9)
// A canting of 90 deg should give pi polarization, which it doesn't
// 180 deg gives the polarization to pi
// corrected here

// Version 2
// To be consistent with the standard Jones vector calculus we use a new approach since verison 2.0
// We use only normalized vetors and matrices here.
// 
// The polarizer matrix J_u for unpolarized light is
//
//   J_u = (1 0)
//         (0 1)
//
// The rotation R(a)* Ju * R(-a) will give J_u as R(-a) = R(a)^-1 and Ju is the Identity, i.e. independent of rotation.
// a is the rotation angle from sigma.
// 
// For polarized light, the Jones vector is
// 
//   j = (Es, Ep)
//     = (|Es|, |Ep| exp(id))
//     = A (cos(x), sin(x) exp(id))
// 
// x is the ellipticity angle
// d is the phase differnece
// A is the amplitude
// 
// A general normalized Jones vector is also given by
// (see J.J. Gil and R. Ossikovski, "Polarized light and the Muller matrix approach")
// 
//    j = R(a) (cos(x), i sin(x)) with d = 90�
// 
// This vector is sufficient to describe the Jones vector via x and a.
// The polarizer matrix is given by the outer product of the Jones vector (which should be analyzed)
// 
//    J_p = j*j^H
//
//        = 1/2 (Jss   Jsp)
//              (Jsp*  Jpp)
// 
//    Jss = 1 + cos(2x) cos(2a)
//    Jsp = sin(2a)cos(2x) - i sin(2x)
//    Jpp = 1 - cos(2x) cos(2a)
// 
// The full coherency matrix with efficiency E is 
//  
//    J = (1-E) J_u + E J_p
//  
//      = (1-E) (1 0) + E (   1 + cos(2x)cos(2a)       sin(2a)cos(2x)-i*sin(2x))
//              (0 1)     (sin(2a)cos(2x)+i*sin(2x)       1 - cos(2x)cos(2a)   )
// 
//      = 1/2 ( 2 - E [1 - cos(2x)cos(2a)]      E [sin(2a)cos(2x)-i*sin(2x)])
//            (E [sin(2a)cos(2x)+i*sin(2x)]      2 - E [1 + cos(2x)cos(2a)] )
//
//  which is different to CONUSS in both angles by a factor of two.
//


#ifndef NEXUS_ANALYZER_H_
#define NEXUS_ANALYZER_H_

#include <string>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/nxobject.h"
#include "../classes/optimizer_handler.h"


/**
Constructor for the :class:`Analyzer` class.
Analyzer objects are only considered in polarization dependent nuclear calculations.
In pure electronic calculations the beam has no polarization information.
The default arguments set unpolarized detection.
See Eq.(9), [Sturhahn2000]_, however, the canting angle given in Nexus is multiplied by 2 to give a polarization of :math:`\pi` at 90 degree.

.. admonition:: Changed in version 2.0.0

      All parameters are now such that it corresponds to standard values of optical theory.
      The mixing angle now corresponds to the ellpticity angle of the ellipse of the coherent Jones vector.

Args:
   efficiency (float or :class:`Var`): Polarization efficiency of the detection process, 0 to 1.

     .. versionchanged:: 2.0.0
        fitable since version 2.0.0

   mixing_angle (float or :class:`Var`): Mixing angle (deg). Defines the contribution of independent polarization components of the Jones vector.

     * 0 degree linear polarization.
     * +90 degree circular left polarization. +45 deg since verison 2.0.0.
     * -90 degree circular right polarization. -45 deg since verison 2.0.0.
   
     .. versionchanged:: 2.0.0
        fitable since version 2.0.0

   canting_angle (float or :class:`Var`): Canting angle of the analyzer with respect to :math:`\sigma` direction (deg).

     .. versionchanged:: 2.0.0
        fitable since version 2.0.0

   id (string): User identifier.

Attributes:
   efficiency (:class:`Var`): Efficiency of the polarization detection, 0 to 1.
        
     .. versionadded:: 2.0.0

   mixing_angle (:class:`Var`): Mixing angle (deg). Defines the contribution of independent polarization components of the Jones vector.

     .. versionadded:: 2.0.0

     * 0 degree linear polarization.
     * +45 degree circular left polarization.
     * -45 degree circular right polarization.

   canting_angle (:class:`Var`): Canting angle of the beam with respect to sigma direction (deg).

     .. versionadded:: 2.0.0

   scattering_factor (complex): Electronic scattering factor of the object. The default value is ``1+0j``. This value should only be changed if you know what you are doing.
   matrix (ndarray): 2x2 complex Jones matrix of the analyzer.
   id (string): User identifier.
*/
class Analyzer : public NxObject {
public:
  Analyzer(
    Var* const efficiency,
    Var* const mixing_angle,
    Var* const canting_angle,
    const std::string id
  );

  // NxObject functions
  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);


  /**
  Set the analyzer to unpolarized detection.
  */
  void Unpolarized();

  /**
  Set the analyzer to polarized detection along :math:`\sigma` direction.
  */
  void LinearSigma();

  /**
  Set the analyzer to polarized detection along :math:`\pi` direction.
  */
  void LinearPi();

  /**
  Set the analyzer to detection of circular left polarized light.
  */
  void CircularLeft();

  /**
  Set the analyzer to detection of circular right polarized light.
  */
  void CircularRight();


  /**
  Rotate the analyzer by the given angle.

  .. versionremoved:: 2.0.0

  Args:
    angle (float): angle of rotation (degree).
  */
  void Rotate(const double angle);


  /**
  Set the Jones matrix directly by a complex 2x2 array.

  .. versionremoved:: 2.0.0

  Args:
     matrix (ndarray): a complex 2x2 Jones matrix.
  */
  void SetJonesMatrix(const Eigen::Matrix2cd& matrix);


  /** 
  Electronic scattering amplitude of the analyzer.
  This is always one, i.e. no absorption is considered in pure electronic calculations.
  In order to calculate polarization effect, use the fully polarization dependent nuclear calculations.

  Args:
     energy (float): Not used.

  Returns:
     complex: 1+0j.
  */
  Complex ElectronicAmplitude(const double energy)
  {
    return scattering_factor;
  };


  /**
  Electronic scattering matrix of the analyzer. Same as the :attr:`matrix`.

  Args:
     energy (float): Not used.

  Returns:
     list: 2x2 scattering matrix.
  */
  Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy)
  {
    return matrix;
  }


  /**
  Scattering matrix of the analyzer. Returns the constant :attr:`matrix` on the given detuning values.

  Args:
     isotope (:class:`MoessbauerIsotope`): Not used.
     detuning (list or ndarray): Detuning values.
     calc_transitions (bool): not used.

  Returns:
     list: List of the complex 2x2 analyzer matrix.
  */
  ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
    const bool calc_transitions);


  Var* efficiency = nullptr;
  
  Var* mixing_angle = nullptr;
  
  Var* canting_angle = nullptr;

  Complex scattering_factor = Complex(1.0, 0.0);

  Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Identity();

};

#endif // NEXUS_ANALYZER_H_
