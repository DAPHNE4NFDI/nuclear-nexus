// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "optimizer_options.h"

#include <iostream>
#include <algorithm>

#include "../utilities/errors.h"



bool OptimizerOptions::InitialCheck()
{
  bool success = true;

  global_fit = false;

  // method
  if (std::find(ceres_methods.begin(), ceres_methods.end(), method) != ceres_methods.end())
  {
    fit_module = OptimizerModule::Ceres;
  }
  else if (std::find(nlopt_local_methods.begin(), nlopt_local_methods.end(), method) != nlopt_local_methods.end())
  {
    fit_module = OptimizerModule::NLopt;
  }
  else if (std::find(pagmo_methods.begin(), pagmo_methods.end(), method) != pagmo_methods.end())
  {
    fit_module = OptimizerModule::Pagmo;

    if (method != "CompassSearch")
      global_fit = true;
  }
  else
  {
    fit_module = OptimizerModule::Invalid;

    errors::WarningMessage("FitOptions", method + " method not found. Check spelling.");

    success = false;
  }

  // local method
  if (std::find(ceres_methods.begin(), ceres_methods.end(), local) != ceres_methods.end())
  {
    fit_module_local = OptimizerModule::Ceres;
  }
  else if (std::find(nlopt_local_methods.begin(), nlopt_local_methods.end(), local) != nlopt_local_methods.end())
  {
    fit_module_local = OptimizerModule::NLopt;
  }
  else if (local == "CompassSearch")
  {
    fit_module_local = OptimizerModule::Pagmo;
  }
  else
  {
    fit_module_local = OptimizerModule::Invalid;
   
    errors::WarningMessage("FitOptions", local + " local method not found. Check spelling.");

    success = false;
  }

  if (std::find(no_boundary_methods.begin(), no_boundary_methods.end(), method) != no_boundary_methods.end())
  {
    errors::WarningMessage("FitOptions", method + " method does not support boundaries. Boundaries ignored.");
  }

  if (std::find(LineSearch->line_search_type_list.begin(), LineSearch->line_search_type_list.end(), LineSearch->line_search_type) == LineSearch->line_search_type_list.end())
  {
    errors::WarningMessage("FitOptions", LineSearch->line_search_type + " line_search type not found. Check spelling.");

    success = false;
  }

  if (std::find(report_list.begin(), report_list.end(), report) == report_list.end())
  {
    errors::WarningMessage("FitOptions", report + " ceres report type not found. Check spelling.");

    success = false;
  }

  // error method
  if (std::find(error_method_list.begin(), error_method_list.end(), error_method) == error_method_list.end())
  {
    errors::WarningMessage("FitOptions", "Error analysis method " + error_method + " not found. Check spelling.");

    success = false;
  }

  // bootstrap
  if (std::find(Bootstrap->bootstrap_methods.begin(), Bootstrap->bootstrap_methods.end(), Bootstrap->method) == Bootstrap->bootstrap_methods.end())
  {
    errors::WarningMessage("FitOptions", "Bootstrap " + Bootstrap->method + " method not found. Check spelling.");

    success = false;
  }

  return success;
}
