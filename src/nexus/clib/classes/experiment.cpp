// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "experiment.h" 

#include <cmath>
#include <complex>
#include <algorithm>
#include <numeric>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include <Eigen/Dense>

#include "../math/fourier.h"
#include "../math/intensity.h"
#include "../math/matrix.h"
#include "../classes/analyzer.h"
#include "../classes/forward_sample.h"
#include "../classes/sample.h"
#include "../classes/conuss_object.h"



using namespace std::complex_literals;


void Experiment::Update()
{
  beam->Update();

  for (NxObject* const obj : objects)
    obj->Update();
}


void Experiment::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  beam->PopulateFitHandler(fit_handler);

  for (NxObject* obj : objects)
    obj->PopulateFitHandler(fit_handler);
}


Complex Experiment::ElectronicAmplitude(const double energy)
{
  Complex amplitude(1.0, 0.0);

  for (NxObject* const obj : objects)
    amplitude *= obj->ElectronicAmplitude(energy);

  return amplitude;
};



ExperimentMatrix2 Experiment::Matrix(const std::vector<double>& detuning_grid, bool calc_transitions) const
{
  //create time_grid
  const size_t num_points = detuning_grid.size();

  // check that a detuning step can be calculated
  if (detuning_grid.size() > 1)
  {
    const double detuning_step = detuning_grid.at(1) - detuning_grid.at(0);

    const double time_step = fourier::Step(num_points, detuning_step / (2.0 * constants::kPi * isotope->lifetime));

    time_grid.resize(num_points);

    for (size_t i = 0; i < num_points; ++i)
      time_grid[i] = i * time_step;
  }

  // prepare input identity matrix
  ExperimentMatrix2 matrix(num_points, Eigen::Matrix2cd::Identity());

  for (NxObject* const obj : objects)
  {
    // determine sample matrix
    const ObjectMatrix2 obj_matrix = obj->ObjectMatrix(isotope, detuning_grid, calc_transitions);

    // check for moving sample
    if (BasicSample* const sample = dynamic_cast<BasicSample*>(obj))
    {
      // if sample is moving apply lab and sample frame transformations
      // see, for example, Pim van den Heuvel, master thesis
      // Arbitrary phase control of X-ray pulses with zeptosecond phase stability, 2021
      if (sample->function_time != nullptr && time_grid.size() > 1)
      {
        if (detuning_grid.size() % 2 != 0)
          errors::ErrorMessage("Experiment", "detuning_grid size is not even. Output might be wrong.");

        // input to time domain
        ObjectMatrix2 matrix_time = fourier::FourierTransform(matrix, fourier::FourierScaling::none, "none");

        // transform input into the sample rest frame
        std::vector<Complex> transform(time_grid.size());

        // Function(times)
        // times must be in ns
        // Function has to return phase factor
        for (size_t i = 0; i < time_grid.size(); ++i)
          transform[i] = exp(1.0i * sample->function_time->Function(time_grid[i] * 1.0e9));  // time_grid to ns

        matrix_time = matrix::DivideVector(matrix_time, transform);

        // input back to frequency domain
        matrix = fourier::FourierTransform(matrix_time, fourier::FourierScaling::N, "none", true);

        // multiply with moving sample matrix 
        matrix = matrix::Multiply(obj_matrix, matrix);

        // output to time domain
        matrix_time = fourier::FourierTransform(matrix, fourier::FourierScaling::none, "none");

        // transform output back to lab frame
        matrix_time = matrix::MultiplyVector(matrix_time, transform);

        // output back to frequency domain
        matrix = fourier::FourierTransform(matrix_time, fourier::FourierScaling::N, "none", true);
      }
      else
      {
        // sample without motion, multiply by object matrix
        matrix = matrix::Multiply(obj_matrix, matrix);
      }
    }
    else
    {
      // no sample object, multiply by object matrix
      matrix = matrix::Multiply(obj_matrix, matrix);
    }
  }

  return matrix;
};


// functions for arbitrary distribution in experiment

// returns distribution_points to the power of all layers that have a distribution
// before this function call ForwardPropagationMatrices
size_t Experiment::PopulateThicknessDistribution(const size_t distribution_points)
{
  size_t total_combinations = 1;

  for (NxObject* const obj : objects)
  {  
    if (ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
      total_combinations *= sample->PopulateThicknessDistribution(distribution_points);
  }

  return total_combinations;
}


void Experiment::ResetDistributionIndex()
{
  for (NxObject* const obj : objects)
  {
    if (ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
      sample->ResetDistributionIndex();

  }
}


// increase the distribution index of each layer by cycling through layer and reset index if at end
void Experiment::IncreaseDistributionIndex(const size_t distribution_points)
{
  bool break_flag = false;

  for (NxObject* const obj : objects)
  {
    if (const ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
    {
      for (Layer* lay : sample->layers)
      {
        lay->distribution_index += 1;

        if (lay->distribution_index < static_cast<unsigned int>(distribution_points))
        {
          break_flag = true;

          break;
        }
        else
        {
          lay->distribution_index = 0;
        }

      }
    }

    if (break_flag)
      break;
  }
}


void Experiment::StoreForwardThicknesses()
{
  std::vector<double> thickness_values;

  for (NxObject* const obj : objects)
  {
    if (const ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
    {
      for (Layer* const lay : sample->layers)
        lay->thickness_store = lay->thickness->value;
    }
  }
}


void Experiment::RecallForwardThicknesses()
{
  for (NxObject* const obj : objects)
  {
    if (const ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
    {
      for (Layer* const lay : sample->layers)
        lay->thickness->value = lay->thickness_store;
    }
  }
}


double Experiment::SetForwardThicknessAndGetWeight()
{
  double weight = 1.0;

  // set each layer to correct thickness and determine weight
  for (NxObject* const obj : objects)
  {
    if (const ForwardSample* const sample = dynamic_cast<ForwardSample*>(obj))
    {
      for (const Layer* lay : sample->layers)
      {
        lay->thickness->value = lay->thickness_distribution[lay->distribution_index];

        weight *= lay->thickness_distribution_weight[lay->distribution_index];
      }
    }
  }

  return weight;
}



// returns distribution_points to the power of all layers that have a distribution
// before this function call ForwardPropagationMatrices have to be calculated
size_t Experiment::PopulateDivergenceDistribution(const size_t distribution_points)
{
  size_t total_combinations = 1;

  for (NxObject* const obj : objects)
  {
    if (GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
    {
      sample->PopulateDivergence(distribution_points);

      total_combinations *= sample->GetDivergenceDistributionSize();
    }
    else if (ConussObject* const conuss_object = dynamic_cast<ConussObject*>(obj))
    {
      conuss_object->PopulateDivergence();

      total_combinations *= conuss_object->GetDivergenceDistributionSize();
    }
  }

  return total_combinations;
}


void Experiment::ResetDivergenceIndex()
{
  for (NxObject* const obj : objects)
  {
    if (GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
    {
      sample->ResetDivergenceIndex();
    }
    else if (ConussObject* const conuss_object = dynamic_cast<ConussObject*>(obj))
    {
      conuss_object->ResetDivergence();
    }
  }
}


// increase the divergence index of each sample by cycling through samples and reset index if at end
void Experiment::IncreaseDivergenceIndex(const size_t distribution_points)
{
  for (NxObject* const obj : objects)
  {
    if (GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
    {
      sample->divergence_index += 1;

      if (sample->divergence_index < static_cast<unsigned int>(distribution_points))
      {
        break;
      }
      else
      {
        sample->divergence_index = 0;
      }
    }
    else if (ConussObject* const conuss_object = dynamic_cast<ConussObject*>(obj))
    {
      conuss_object->divergence_index += 1;

      if (conuss_object->divergence_index < static_cast<unsigned int>(conuss_object->angles.size()))
      {
        break;
      }
      else
      {
        conuss_object->divergence_index = 0;
      }
    }

  }
}


void Experiment::StoreGrazingAngles()
{
  std::vector<double> angle_values;

  for (NxObject* const obj : objects)
  {
    if (GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
      sample->angle_store = sample->angle->value;
  }
}


void Experiment::RecallGrazingAngles()
{
  for (NxObject* const obj : objects)
  {
    if (GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
      sample->angle->value = sample->angle_store;
  }
}


double Experiment::SetAngleAndGetWeight()
{
  double weight = 1.0;

  // set each sample to correct angle and determine weight
  for (NxObject* const obj : objects)
  {
    if (const GrazingSample* const sample = dynamic_cast<GrazingSample*>(obj))
    {
      sample->angle->value = sample->divergence_distribution[sample->divergence_index];

      weight *= sample->divergence_distribution_weight[sample->divergence_index];
      
    }
    else if (ConussObject* const conuss_object = dynamic_cast<ConussObject*>(obj))
    {
      conuss_object->object_matrix = conuss_object->ObjectMatrixAtIndex(conuss_object->divergence_index);

      weight *= conuss_object->divergence_distribution_weight[conuss_object->divergence_index];
    }
  }

  return weight;
}
