// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of parameter distributions acting on hyperfine parameters.
// Used in Hyperfine objects.
// 
// This is a class with a virtual function Distribution() that is defined in Python (by the user).
// vector delta - delta values of the base parameter, e.g. magnetic hyperfine field: -deltax to deltax
// vector weight - weight values of the delta values, is automatically normalized to one in setygrid: 0-1
// The values here only give the changes around the base parameter, e.g. the magnetic field.
// The actual scaling is done in Hyperfine class, e.g. Bhf-deltax to Bhf+deltax
// The deltax values can be specified with SetDeltaX()


#ifndef NEXUS_DISTRIBUTION_H_
#define NEXUS_DISTRIBUTION_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"


// abstract class for distributions
/**
Abstract class for :class:`Distribution` acting on :class:`Hyperfine` parameters.
Do not use this class directly.
A Distribution must be derived from this class in Python the following way.

.. code-block::

    # definition of the derived class
    class DistributionDefinedInPython(nx.Distribution):
        def __init__(self, points, ...):
            super().__init__(points, "user defined distribution")
            ... followed by your code if needed ...

        # implementation of the actual distribution function
        def DistributionFunction(self):
            # here goes your implementation
            .....
            # set the deltax values to a reasonable range
            x = np.array(self.SetRange(...))
            ...
            # calculate the weight
            weight =  a function of x most probably
            # set the weight values
            self.SetWeight(weight)

A lot of distribution are predefined in the library :mod:`distribution`.

Args:
   points (int): Number of distribution points.
   id (string): User identifier.

Attributes:
   points (int): Number of distribution points.
   delta (list): The delta values of the distribution.
   weight (list): Relative weight of the delta values.
   id (string): User identifier.
   fit_variables (list): List of :class:`nx.Var` objects.
*/
class Distribution {
public:
  Distribution(const unsigned int points = 101, const std::string id = "");

  virtual ~Distribution() {};

  /**
  Call of the distribution function implementation from python.
  */
  virtual void DistributionFunction() = 0;

  /**
  Set :attr:`delta` values to a specific range around zero.
    
  Args:
     range (double): The total distribution range.
  */
  std::vector<double> SetRange(const double range);

  /**
  Set :attr:`delta`.

  Args:
     values (list or ndarray): List of delta values.
  */
  void SetDelta(const std::vector<double> values);

  /**
  Set :attr:`weight`. The sum of the weight values is normalized to one.

  Args:
     values (list or ndarray): List of weight values.
  */
  void SetWeight(std::vector<double> values);  // weight is normalized to sum of one

  /**
  Prints all :attr:`delta` values.
  */
  void PrintDelta() const;

  /**
  Prints all :attr:`weight` values.
  */
  void PrintWeight() const;
  
  unsigned int points = 101;

  std::string id = "";

  std::vector<double> delta{};
  
  std::vector<double> weight{};

  std::vector<Var*> fit_variables{};
};


// direct call from Python
void CallDistribution(Distribution* const dist);

#endif // NEXUS_DISTRIBUTION_H_
