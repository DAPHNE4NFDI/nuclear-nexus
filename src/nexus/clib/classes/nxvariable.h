// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Var class.
// This class holds the value of an variable and additional parameters used in fitting.
// Most input in Nexus is of Var type.
// The swig interface often converts integer or float inputs to Vars.


#ifndef NEXUS_NXVARIABLE_H_
#define NEXUS_NXVARIABLE_H_

#include <string>
#include <vector>
#include <limits>

#include "../nexus_definitions.h"
#include "../utilities/errors.h"
#include "../classes/equality.h"


/*
Constructor for the :class:`Var` class.

Args:
   value (float): Value of the :class:`Var`.
   min (float): Minimum value used in fits with boundary conditions.
   max (float): Maximum value used in fits with boundary conditions.
   fit (bool): Set to *True* if the variable is to be fitted.
   id (string): User identifier.
   equality (:class:`Equality`): An :class:`Equality` object to define an equality constraint for fitting.

Attributes:
   value (float): Value of the :class:`Var`.
   min (float): Minimum value used in fits with boundary conditions.
   max (float): Maximum value used in fits with boundary conditions.
   fit (bool): Set to *True* if the variable is to be fitted.
   id (string): User identifier.
   equality (:class:`Equality`): An :class:`Equality` object to define an equality constraint for fitting.
   operator + (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
   operator - (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
   operator * (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
   operator / (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
   operator >> (operator): Operator for :class:`Var` object. Reassign :attr:`Var.value` by setting :class:`Var` to new .value directly.
 */
class Var {
public:
  Var(const double value = 0,
    const double min = -INFINITY,
    const double max = INFINITY,
    const bool fit = false,
    const std::string id = "",
    Equality* const equality = nullptr
  ) : value(value), min(min), max(max), fit(fit), id(id), equality(equality)
  {
    if (min > value)
      errors::WarningMessage("Var", ".id: " + id + " .min=" + std::to_string(min) + " is larger than .value=" + std::to_string(value) + ".");
    if (max < value)
      errors::WarningMessage("Var", ".id: " + id + " .max=" + std::to_string(max) + " is smaller than .value=" + std::to_string(value) + ".");
  };

  Var* operator+(const double& a);
  Var* operator-(const double& a);
  Var* operator*(const double& a);
  Var* operator/(const double& a);
  Var* operator>>(const double& a);

  /**
  Sets the minimum and maximum values for fitting.

  Args:
     min (float): Minimum value.
     max (float): Maximum value.
  */
  void FitRange(const double min, const double max);
  
  /**
  Print the pointer of the ``Var`` object.
  */
  void PrintPointer() const;

  double value = 0.0;

  double min = 0.0;

  double max = 0.0;

  bool fit = false;

  std::string id = "";

  Equality* equality = nullptr;
};

#endif // NEXUS_NXVARIABLE_H_
