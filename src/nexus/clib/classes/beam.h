// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Beam definition.

// A beam describes the photon polarization state via the coherency matrix and the Jones vector.

// Beam definitions are taken from W. Sturhahn Hyperfine Interactions 125, 149 (2000).
// The canting angle describes canting of the beam with respect to the sigma plane.
// The given canting angle seems to be wrong in Eq. (9).
// A canting of 90 deg should give pi polarization, which it doesn't
// In Sturhahn equations a 180 deg gives the polarization to pi
// Here, the canting angle is therefore multiplied by 2, to get pi at 90 deg.

// Version 2
// To be consistent with the standard Jones vector calculus we use a new approach since verison 2.0
// We use only normalized vetors and matrices here.
// 
// The coherency matrix J is
//
//   J_u = 1/2 (1 0)
//             (0 1)
//
// for unpolarized light.
// The rotation R(a)* Ju * R(-a) will give J_u as R(-a) = R(a)^-1 and Ju is 1/2*Identity, i.e. independent of rotation.
// a is the rotation angle from sigma.
// 
// For polarized light, the Jones vector is
// 
//   j = (Es, Ep)
//     = (|Es|, |Ep| exp(id))
//     = A (cos(x), sin(x) exp(id))
// 
// x is the ellipticity angle
// d is the phase differnece
// A is the amplitude
// 
// A general normalized Jones vector is also given by
// (see J.J. Gil and R. Ossikovski, "Polarized light and the Muller matrix approach")
// 
//    j = R(a) (cos(x), i sin(x)) with d = 90�
// 
// This vector is sufficient to describe the Jones vector via x and a.
// The polarized Jones matrix is given by the outer product
// 
//    J_p = j*j^H
//
// This can be done with the rotation R(a) included or one can rotate the full coherncy after setting it up.
// Without rotation it is
// 
//    J_p = (   cos^2(x)      -i cos(X) sin(x))
//          (i cos(X) sin(x)       sin^2(x)   )
//        
//        = 1/2 (2cos^2(x)  -i sin(2x))
//              (i sin(2x)   2sin^2(x))
// 
// and with rotation matrix included
// 
//    J_p = 1/2 (Jss   Jsp)
//              (Jsp*  Jpp)
// 
//    Jss = 1 + cos(2x) cos(2a)
//    Jsp = sin(2a)cos(2x) - i sin(2x)
//    Jpp = 1 - cos(2x) cos(2a)
// 
// The full coherency matrix with polrization P is 
//  
//    J = (1-P) J_u + P J_p
// 
// We can either write the unrotated matrix J_unrot and rotate afterwards J = R(a) J_unrot R(-a) 
// or directly use the rotated J_pol
//
//   J = 1/2 (1-P) (1 0) + P (   1+cos(2x)cos(2a)        sin(2a)cos(2x)-i*sin(2x))
//                 (0 1)     (sin(2a)cos(2x)+i*sin(2x)        1-cos(2x)cos(2a)   )
// 
//     = 1/2 (    1 + P cos(2x)cos(2a)         P [sin(2a)cos(2x)-i*sin(2x)])
//           (P [sin(2a)cos(2x)+i*sin(2x)]         1 - P cos(2x)cos(2a)    )
//
//  which is different to CONUSS in both angles by a factor of two.
//


#ifndef NEXUS_BEAM_H_
#define NEXUS_BEAM_H_

#include <string>
#include <complex>
#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"
#include "../classes/optimizer_handler.h"


/**
Constructor for the :class:`Beam` class.
Default values set a linear polarization along :math:`\sigma`.

See Eq. (9), [Sturhahn2000]_, however, the canting angle given in Nexus is multiplied by 2 to give a polarization of :math:`\pi` at 90 degree.

.. admonition:: Changed in version 2.0.0

      The default method sets a unpolarized beam.

      All parameters are now such that it corresponds to standard values of optical theory.
      The mixing angle now corresponds to the ellpticity angle of the ellipse of the coherent Jones vector.

Args:
   polarization (float or :class:`Var`): Degree of polarization, 0 to 1.

     .. versionchanged:: 2.0.0
        fitable since version 2.0.0

   mixing_angle (float or :class:`Var`): Mixing angle (deg). Defines the contribution of independent polarization components of the coherent Jones vector. The definition is such that circular left polarized light carries a spin angular momentum of :math:`+\hbar`.

      * 0 degree linear polarization.
      * +90 degree circular left polarization. +45 deg since version 2.0.0.
      * -90 degree circular right polarization. -45 deg since version 2.0.0.
   
     .. versionchanged:: 2.0.0
        fitable since version 2.0.0

        value is halved compared to version 1 to obtain the same beam properties.
           
   canting_angle (float or :class:`Var`): Canting angle of the beam with respect to sigma direction (deg).

     .. versionchanged:: 2.0.0
        fitable since version 2.0.0

   profile (string): Profile of the beam for grazing incidence scattering.
      The beam profile is only considered in the methods :class:`Reflectivity`, :class:`Transmission`, :class:`NuclearReflectivity` and  :class:`NuclearReflectivityEnergy`.
      
      * *g* - Gaussian profile.
      * *r* - Rectangular profile.
      * *n* - No beam profile correction.
      
      .. versionchanged:: 1.0.2
         Rectangular beam profile correction wrong prior to version 1.0.2
      
   fwhm (float or :class:`Var`): Full width half maximum of the beam in the scattering plane of grazing scattering geometry (mm).
      Only needed for grazing incidence geometry.
   id (string): User identifier.

Attributes:
   polarization (:class:`Var`): Degree of polarization, 0 to 1.

     .. versionadded:: 2.0.0

   mixing_angle (:class:`Var`): Mixing angle (deg). Defines the contribution of independent polarization components of the coherent Jones vector. The definition is such that circular left polarized light carries a spin angular momentum of :math:`+\hbar`.
      
     .. versionadded:: 2.0.0

     * 0 degree linear polarization.
     * +45 degree circular left polarization.
     * -45 degree circular right polarization.
           
   canting_angle (:class:`Var`): Canting angle of the beam with respect to sigma direction (deg).

     .. versionadded:: 2.0.0

   matrix (ndarray): 2x2 complex Jones matrix of the beam.
   jones_vector (ndarray): Jones vector for a fully coherent beam, 2-element vector. Can only be set via :attr:` or special methods with fully polarized beam properties.
   profile (string): Profile of the beam for grazing incidence scattering.
      The beam profile is only considered in the methods :class:`Reflectivity`, :class:`Transmission`, :class:`NuclearReflectivity` and  :class:`NuclearReflectivityEnergy`.
   
      * *g* - Gaussian profile.
      * *r* - Rectangular profile.
      * *n* - No beam profile correction.

      .. versionchanged:: 1.0.2
         Rectangular beam profile correction wrong prior to version 1.0.2

   fwhm (:class:`Var`): Full width half maximum (FWHM) of the beam in the scattering plane of grazing scattering geometry (mm).
      Only needed for grazing incidence geometry.
   id (string): User identifier.
*/
class Beam {
public:
  Beam(
    Var* const polarization,  // in degree
    Var* const mixing_angle, // in degree
    Var* const canting_angle, // in degree
    const std::string profile,
    Var* const fwhm, // in mm
    const std::string id
  );

  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);


  /**
  Set the beam to unpolarized radiation.
  */
  void Unpolarized();


  /**
  Set the beam to :math:`\sigma` polarized light.
  */
  void LinearSigma();


  /**
  Set the beam to :math:`\pi` polarized light.
  */
  void LinearPi();


  /**
  Set the beam to circular left polarized light.
  */
  void CircularLeft();


  /**
  Set the beam to circular right polarized light.
  */
  void CircularRight();


  /**
  Set the Jones vector.

  .. versionchanged:: 2.0.0
     Only sets the Jones vector since version 2.0.0.

  Args:
     jones_vector (ndarray): Two element Jones vector.
     angle (float): Rotation angle of Jones vector with respect to :math:`\sigma` (deg).
  */
  void SetJonesVector(const Eigen::Vector2cd& jones_vector, const double angle = 0.0);


  /**
  Set the coherency matrix. The Jones vector is invalid in this case.
  To set a fully coherent beam use :attr:`SetJonesVector` or a specific method.

  .. versionremoved:: 2.0.0

  Args:
     matrix (ndarray): 2x2 complex coherency matrix.
  */
  void SetCoherencyMatrix(const Eigen::Matrix2cd& matrix);


  /**
  Rotate the beam by the given angle. Jones vector and matrix are rotated.

  ..versionremoved:: 2.0.0

  Args:
     angle (float): angle of rotation (degree).
  */
  void Rotate(const double angle);


  /**
  Calculates the degree of polarization [Born]_

  .. math:: P = \frac{I_{pol}}{I_{tot}} = \sqrt{1 - \frac{4|\mathbf{J}|}{(J_{\sigma \sigma} + J_{\pi \pi})^2}}
   
  Returns:
     float: Degree of polarization. 0 to 1.
  */
  double Polarization() const;


  /**
  Calculates the complex coherence (or correlation between :math:`E_{\sigma}` and :math:`E_{\pi}`) of the beam [Born]_

  .. math:: \mu = \frac{J_{\sigma \pi}}{\sqrt(J_{\sigma \sigma}) \sqrt(J_{\pi \pi})}

  It is a measure of the correlation between the :math:`E`-vector components.

  If ``nan`` is returned one :math:`E`-vector component is zero.

  Returns:
     complex: Complex coherence between :math:`E_{\sigma}` and :math:`E_{\pi}`.
  */
  Complex ComplexCoherence() const;


  /**
  Calculates the coherence of the beam [Born]_

  .. math:: |\mu| = | \frac{J_{\sigma \pi}}{\sqrt(J_{\sigma \sigma}) \sqrt(J_{\pi \pi})} |

  It is a measure of the correlation between the :math:`E`-vector components.

  If ``nan`` is returned one :math:`E`-vector component is zero.

  Returns:
     float: Degree of coherence between :math:`E_{\sigma}` and :math:`E_{\pi}`.
  */
  double Coherence() const;


  /**
  Calculates the phase difference between :math:`E_{\sigma}` and :math:`E_{\pi}` [Born]_

  If ``nan`` is returned one :math:`E`-vector component is zero.

  Returns:
     float: Phase difference between :math:`E_{\sigma}` and :math:`E_{\pi}`.
  */
  double PhaseDifference() const;


  /**
  Checks a valid Jones vector is set.

  Returns:
     bool: True or False.
  */
  bool ValidJonesVector() const;


  /**
  Get the coherency matrix after you have set a valid Jones vector.

  Returns:
    ndarray: coherency matrix.
  */
  Eigen::Matrix2cd MatrixFromJonesVector() const;


  Var* polarization = nullptr;

  Var* mixing_angle = nullptr;

  Var* canting_angle = nullptr;

  std::string profile = "g";

  Var* fwhm = nullptr;

  std::string id = "";

  Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Zero();

  Eigen::Vector2cd jones_vector = Eigen::Vector2cd::Zero();
};

#endif // NEXUS_BEAM_H_
