// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module(directors="1") residual

%{ 
    #define SWIG_FILE_WITH_INIT
    #include "residual.h"
%}


%feature("director") Residual;


/*
// pass data from C++ to Python
%typemap(directorin) std::vector<double> intensity_data %{
    npy_intp dim_1 = $1.size();
    $input = PyArray_SimpleNewFromData(1, &dim_1, NPY_DOUBLE, (void *)$1.data());
%}

%typemap(directorin) std::vector<double> intensity_theory %{
    npy_intp dim_2 = $1.size();
    $input = PyArray_SimpleNewFromData(1, &dim_2, NPY_DOUBLE, (void *)$1.data());
%}

// pass data from Python to C++
%typemap(directorout) void *getObject %{
  $result = $1;
%}
*/

%include "residual.h"


%pythoncode %{

class Sqrt(Residual):
    def __init__(self, id = "Sqrt", exponent = 2, plot_string = r"$\sqrt{y} - \sqrt{\hat{y}}$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        sqrt_data = [np.sqrt(x) if x>=0.0 else 0.0 for x in input_data]
        sqrt_theory = [np.sqrt(x) if x>=0.0 else 0.0 for x in input_theory]
        return np.subtract(sqrt_data, sqrt_theory)

class Log10(Residual):
    def __init__(self, id = "Log10", exponent = 2, plot_string = r"$\log_{10}(y) - \log_{10}(\hat{y})$"):
        super().__init__(id, exponent, plot_string)

    def ResidualFunction(self, input_data, input_theory):
        log_data = [np.log10(x) if x>0.0 else 1e-16 for x in input_data]
        log_theory = [np.log10(x) if x>0.0 else 1e-16 for x in input_theory]
        return np.subtract(log_data, log_theory)

%}
