// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Sample class.
// A sample consists of several Layer objects and holds information on its lateral dimension and its scattering geometry.


#ifndef NEXUS_GRAZING_SAMLPE_H_
#define NEXUS_GRAZING_SAMLPE_H_

#include <string>
#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/optimizer_handler.h"
#include "../classes/layer.h"
#include "../classes/eff_dens_model.h"
#include "../classes/function_time.h"
#include "../classes/basic_sample.h"


/**
Constructor for :class:`GrazingSample`.

.. versionadded:: 2.0.0

Args:
   layers (list): List of Layer in order of beam propagation.
   geometry (string): Scattering geometry of the sample.

      * *r* - reflection in grazing incidence scattering
      * *t* - transmission in grazing incidence scattering

   angle (float or :class:`Var`): Angle of incidence in grazing geometry (degree).
   length (float): length of the sample along beam direction. Only used for grazing geometry.
   roughness (string): Roughness model in grazing geometry.

      * *n* - no roughness
      * *a* - analytical model
      * *e* - effective density model

   effective_thickness (float): Layer thickness in the effective density model (nm).
   drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
   function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
     Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
   divergence (float or :class:`Var`): Divergence of the incidence angle given as the FWHM of the divergence (deg).
   id (string): User identifier.

Attributes:
   layers (list): List of Layer in order of beam propagation.
   geometry (string): Scattering geometry of the sample.

      * *r* - reflection in grazing incidence scattering
      * *t* - transmission in grazing incidence scattering

   angle (float or :class:`Var`): Angle of incidence in grazing geometry (degree).
   length (float): length of the sample along beam direction. Only used for grazing geometry.
   roughness (string): Roughness model in grazing geometry.

      * *n* - no roughness
      * *a* - analytical model
      * *e* - effective density model

   effective_thickness (float): Layer thickness in the effective density model (nm).
   drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
   function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
     Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
   divergence (float or :class:`Var`): Divergence of the incidence angle given as the FWHM of the divergence (deg).
   id (string): User identifier.
   effective_coordinates (list): Layer coordinates in the effective density model.
   effective_layers (list): List of :class:`EffectiveLayer` objects.
*/
class GrazingSample : public BasicSample {
public:
  GrazingSample(
    const std::vector<Layer*> layers,
    const std::string geometry,  // r(eflection) or t(ransmission)
    Var* const angle,  // in degree
    const double length, // in mm (or actually just the same dimension as beam.fwhm)
    const std::string roughness,  // sigma nm
    const double effective_thickness, // nm
    const std::vector<double> drive_detuning, // Gamma
    FunctionTime* const function_time,  // return phase factor
    Var* const divergence,
    const std::string id = ""
  );

  /* NxObject function definition */

  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning, const bool calc_transitions);
 
  /**
  Calculates the relative electronic amplitude behind the sample.
  The geometry (reflection, transmission) is determined from the :attr:`geometry`.

  Args:
     energy (float): X-ray energy (eV)

  Returns:
     complex: Electronic amplitude.
  */
  Complex ElectronicAmplitude(const double energy);

  /* amplitudes */

  /**
  Calculates the relative electronic amplitude matrix behind the sample.
  The geometry (reflection, transmission) is determined from the :attr:`geometry`.

  Args:
     energy (float): X-ray energy (eV)

  Returns:
     complex: Electronic amplitude.
  */
  Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy); 

  /**
  Calculates the electronic X-ray field amplitude in the sample at a certain thickness.

  Args:
     energy (float): X-ray energy (eV).
     thickness (float): Thickness value (nm).

  Returns:
     complex: Electronic field amplitude in the sample.
  */
  Complex ElectronicAmplitudeThickness(const double energy, const double thickness);

  /* intensities */

  /**
  Calculates the relative electronic field intensity behind the sample.
  The geometry (reflection, transmission) is determined from the :attr:`geometry`.

  Args:
     energy (float): X-ray energy (eV)

  Returns:
     complex: Electronic field intensity.
  */
  double ElectronicIntensity(const double energy);

  /**
  Calculates the electronic X-ray field intensity in the sample at a certain thickness.

  Args:
     energy (float): X-ray energy (eV).
     thickness (float): Thickness value (nm).

  Returns:
     float: Electronic field intensity.
  */
  double ElectronicIntensityThickness(const double energy, const double thickness);

  /**
  Calculates the electronic X-ray field amplitude in the sample.

  Args:
     energy (float): X-ray energy (eV).
     num_points (int): Number of points of the output.

  Returns:
     complex: Electronic field amplitude in the sample.
  */
  std::vector<Complex> ElectronicFieldAmplitude(const double energy, const size_t num_points);

  /**
  Calculates the electronic X-ray field intensity in the sample.

  Args:
     energy (float): X-ray energy (eV).
     num_points (int): Number of points of the output.

  Returns:
     float: Electronic field intensity.
  */
  std::vector<double> ElectronicFieldIntensity(const double energy, const size_t num_points);


  /**
  Calculates the electronic 2x2 sample matrix in grazing incidence geometry.
  It is the product of all layer matrices.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     ndarray: Complex 2x2 matrix.
  */
  Eigen::Matrix2cd ElectronicSampleMatrix(const double energy);

  /**
  Calculates the electronic 2x2 sample matrix up to a certain sample thickness.
  It is the product of all layer matrices up to this thickness.

  Args:
     energy (float): X-ray energy (eV).
     thickness (float): Thickness at which the sample matrix should be calculated (nm).

  Returns:
     ndarray: Complex 2x2 matrix.
  */
  Eigen::Matrix2cd ElectronicSampleMatrixThickness(const double energy, const double thickness);

  /**
  Calculates the complex reflectivity amplitude of the sample.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     complex: Complex reflectivity amplitude.
  */
  Complex ElectronicReflectivityAmplitude(const double energy);

  /**
  Calculates the complex transmission amplitude of the sample.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     complex: Complex reflectivity amplitude.
  */
  Complex ElectronicTransmissionAmplitude(const double energy);

  /**
  Calculates the reflectivity intensity of the sample.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     float: Reflectivity.
  */
  double ElectronicReflectivity(const double energy);

  /**
  Calculates the transmission intensity of the sample in grazing incidence geometry.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     float: Transmission.
  */
  double ElectronicTransmission(const double energy);


  // nuclear

  void ScatteringMatrices(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid, const bool calc_transitions = true);

  void PropagationMatrices(const MoessbauerIsotope* const isotope);

  void LayerMatrices(const MoessbauerIsotope* const isotope);

  LayerMatrix4 SampleMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
    const bool calc_transitions = true);

  // these are the full energy dependent scattering matrices
  ObjectMatrix2 ReflectivityMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid, 
    const bool calc_transitions = true);

  ObjectMatrix2 TransmissionMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
    const bool calc_transitions = true);

  /**
  Calculates the product :math:`k_z \sigma` for each layer in the sample.
  The product should be << 1 for the validity of the W matrix calculation.

  Args:
     energy (float): X-ray energy (eV).
     angle (float): Incidence angle (deg).

  Returns:
     list: List of :math:`k_z \sigma` values for each layer.
  */
  std::vector<double> KzSigma(const double energy, const double angle_kz) const;

  
  std::string geometry = "r";

  Var* angle = nullptr;

  double length = 0.0;

  std::string roughness = "a";

  double effective_thickness = 0.0;

  Var* divergence = nullptr;


  // effective density model
  // scattering matrix f by weight of the layer
  /**
  Create an effective density model from the sample. Electronic scattering factors and matrices are always calculated.

  Args:
     nuclear (bool): Calculate the matrices for nuclear scattering in addition to the electronic ones.
  */
  void EffectiveLayerSystem(const bool nuclear);

  /**
  Total thickness of the sample in the effective density model.

  Returns:
     float: Total thickness in the effective density model.
  */
  double TotalThicknessEffective() const;

  std::vector<double> effective_coordinates{};

  std::vector<EffectiveLayer> extended_layers{};

  // the effective layer system is not of class Layer!
  // only intended to hold the new layers with their thicknesses
  // and weighted class variables: grazing_scattering_factor or _matrix
  std::vector<EffectiveLayer> effective_layers{}; 

  double offset_top = 0.0;

  double offset_bottom = 0.0;

  // angular divergence 
  void PopulateDivergence(const size_t distribution_points);

  void ResetDivergenceIndex();

  size_t GetDivergenceDistributionSize();

  size_t divergence_index = 0;

  double angle_store = 0.0;  // used as storage for incoherent calculations of divergences

  std::vector<double> divergence_distribution{};

  std::vector<double> divergence_distribution_weight{};

private:

  /* electronic sample matrix - roughness models */

  Eigen::Matrix2cd ElectronicSampleMatrixNoRoughness(const double kvector_z);

  Eigen::Matrix2cd ElectronicSampleMatrixAnalyticRoughness(const double kvector_z, Layer* prelayer); // use pointer for prelayer, otherwise lay pointer has to be copied to reference all the time, slow

  Eigen::Matrix2cd ElectronicSampleMatrixEffectiveRoughness(const double kvector_z);

  /* electronic sample matrix - roughness models atn thickness */

  Eigen::Matrix2cd ElectronicSampleMatrixNoRoughnessThickness(const double kvector_z, const double thickness);
  
  Eigen::Matrix2cd ElectronicSampleMatrixAnalyticRoughnessThickness(const double kvector_z, Layer* prelayer, const double thickness);

  Eigen::Matrix2cd ElectronicSampleMatrixEffectiveRoughnessThickness(const double kvector_z, const double thickness);

  /* nuclear sample matrix - roughness models */

  LayerMatrix4 SampleMatrixNoRoughness(const MoessbauerIsotope* const isotope, const size_t size);

  LayerMatrix4 SampleMatrixAnalyticRoughness(const MoessbauerIsotope* const isotope, const size_t size, Layer* prelayer); // use prelayer pointer, otherwise lay pointer has to be copied to reference all the time, slow

  LayerMatrix4 SampleMatrixEffectiveRoughness(const MoessbauerIsotope* const isotope, const size_t size);
};

#endif // NEXUS_GRAZING_SAMLPE_H_
