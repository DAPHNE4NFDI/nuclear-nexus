// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of an experiment.
// It holds all information needed t calculate a certain measurement of the experiment.


#ifndef NEXUS_EXPERIMENT_H_
#define NEXUS_EXPERIMENT_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/optimizer_handler.h"
#include "../classes/moessbauer.h"
#include "../classes/beam.h"
#include "../classes/nxobject.h"

/**
Constructor for :class:`Experiment` class.

Args:
   beam (:class:`Beam`): Beam object.
   objects (list): List of objects in the beam path.

     * :class:`Sample`
     * :class:`Analyzer`
     * :class:`FixedObject`
     * :class:`ConussObject`

     .. versionchanged:: 2.0.0

        * :class:`ForwardSample`
        * :class:`GrazingSample`
        * :class:`Analyzer`
        * :class:`FixedObject`
        * :class:`ConussObject`
   
   isotope (:class:`MoessbauerIsotope`): Resonant isotope for measurements of nuclear scattering type.
   id (string): User identifier.
 
Attributes:
   beam (:class:`Beam`): Beam object.
   objects (list): List of objects in the beam path.

     * :class:`Sample`
     * :class:`Analyzer`
     * :class:`FixedObject`
     * :class:`ConussObject`

     .. versionchanged:: 2.0.0

        * :class:`ForwardSample`
        * :class:`GrazingSample`
        * :class:`Analyzer`
        * :class:`FixedObject`
        * :class:`ConussObject`
      

   isotope (:class:`MoessbauerIsotope`): Resonant isotope for measurements of nuclear scattering type.
   id (string): User identifier.
   time_grid (list): List of times used for the calculation of moving samples.
     
     .. versionadded:: 1.0.4

*/
class Experiment {
public:
  Experiment(
    Beam* const beam,
    const std::vector<NxObject*> objects,
    MoessbauerIsotope* const isotope,
    const std::string id
  ) :
    beam(beam),
    objects(objects),
    isotope(isotope),
    id(id)
  {}

  /**
  Updates all dependencies of the experiment.
  */
  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  // electronic
  /**
  Calculates the electronic scattering amplitude behind all objects.
  Analyzer objects have a default :attr:`scattering_factor` of ``1+0j``.
  So, an analyzer does not change the amplitude because no polarization information are used in electronic calculations.
  If you know the scattering factor of the analyzer you can set its :attr:`scattering_factor` manually.
  Alternatively, use the full polarization-dependent calculations in of the nuclear resonant mode (can also be used without resonant material).

  Args:
     energy (float): X-ray energy in eV.

  Returns:
     complex: electronic scattering amplitude.
  */
  Complex ElectronicAmplitude(const double energy);

  // nuclear
  /**
  Calculates the detuning-dependent product of all object matrices, i.e. the transmission matrix behind all objects.
 
  Args:
     detunings (list or ndarray): Detuning values for the calculation.
     update_and_calc_transitions (bool): *True* for calculation of the nuclear transitions.

  Returns:
     list: List of complex 2x2 arrays.
  */
  ExperimentMatrix2 Matrix(const std::vector<double>& detunings, bool calc_transitions) const;

  Beam* beam = nullptr;

  std::vector<NxObject*> objects{};
  
  MoessbauerIsotope* isotope = nullptr;

  std::string id = "";

  // time_grid used for moving samples
  mutable std::vector<double> time_grid{};

  // functions for thickness distributions
  size_t PopulateThicknessDistribution(const size_t distribution_points);
  
  void ResetDistributionIndex();

  void IncreaseDistributionIndex(const size_t distribution_points);
  
  void StoreForwardThicknesses();

  void RecallForwardThicknesses();

  double SetForwardThicknessAndGetWeight();

  //functions for angular divergence
  size_t PopulateDivergenceDistribution(const size_t distribution_points);

  void ResetDivergenceIndex();

  void IncreaseDivergenceIndex(const size_t distribution_points);

  void StoreGrazingAngles();

  void RecallGrazingAngles();

  double SetAngleAndGetWeight();
};

#endif // NEXUS_EXPERIMENT_H_
