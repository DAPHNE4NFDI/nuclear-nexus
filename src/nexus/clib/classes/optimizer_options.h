// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the OptimizerOptions class.
// Used by the Optimizer and Fit classes.
// Holds all options that can be spezified by the user to control the optimization.
// 
// Options not needed or wanted for the following algorithms:
// NLopt: Newuoa, Subplex
// Pagmo: BeeColony, AntColony, GreyWolf, ParticleSwarm, ParticleSwarmGen, SimpleEvol, Genetic, Annealing, NaturalEvol


#ifndef NEXUS_OPTIMIZER_OPTIONS_H_
#define NEXUS_OPTIMIZER_OPTIONS_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"


// Definition of the Optimizer modules identifier.
enum class OptimizerModule
{
  Invalid,
  Ceres,
  NLopt,
  Pagmo,
};


/**
:class:`LineSearchOptions` for the Line Search method. The ``LineSearch`` method does not support boundaries on the fit parameters.

.. seealso:: `<https://en.wikipedia.org/wiki/Line_search>`_

Attributes:
   line_search_type (string): Linear search type for the "LineSearch" method. Default is ``LBFGS``.

      * ``LBFGS``: Limited-memory Broyden Fletcher Goldfarb Shanno method.
      * ``BFGS``: Broyden Fletcher Goldfarb Shanno method.
      * ``SteepDes``: Steepest descent method.
      * ``NonConGrag``: Non-linear conjugate gradient method.

   max_num_line_search_step_size_iterations (int): Default is 20.
*/
struct LineSearchOptions {
  const std::vector<std::string> line_search_type_list
  {
    "LBFGS",
    "BFGS",
    "SteepDes",
    "NonConGrag"
  };

  std::string line_search_type = "LBFGS";

  int max_num_line_search_step_size_iterations = 20;
};


/**
:class:`CompassSearchOptions` for the Compass Search algorithm.

   * If ``options.iterations = 0``, the number of iterations is set to 500.

Attributes:
   start_range (double): In range (0,1]. Default is 0.1.
   stop_range (double): In range (0,start_range]. Default is 0.05.
   reduction_coeff (double): In range (0,1). Default is 0.5.
*/
struct CompassSearchOptions
{
  double start_range = 0.1;

  double stop_range = 0.005;

  double reduction_coeff = 0.5;
};


/**
:class:`DiffEvolOptions` for the Differential Evolution algorithms.

The `Adaptive` Differential Evolution takes the ``variant`` and the ``adaptive_variant`` into account.
For the `Pagmo` Differential Evolution only the ``adaptive_variant`` is used.
For the classical Differential Evolution take the following into consideration:

"The mutation factor F is a positive control parameter for scaling and controlling the amplification of the difference vector.
Small values of F will lead to smaller mutation step sizes and as a result it will take longer for the algorithm to converge.
Large values of F facilitate exploration, but can lead to the algorithm overshooting good optima.
Thus, the value has to be small enough to enhance local exploration but also large enough to maintain diversity.
The crossover probability CR has an influence on the diversity of DE, as it controls the number of elements that will change.
Larger values of CR will lead to introducing more variation in the new population, therefore increasing it increases exploration."
from Manolis S. Georgioudakis and Vagelis Plevris,
`A Comparative Study of Differential Evolution Variants in Constrained Structural Optimization`.

   * If ``options.population = 0``, the population is set to :math:`10 (n+1)`, where :math:`n` is the number of fit parameters.
     Default is ``0``. When you increase the population you should also decrease the weight F but typically not lower than 0.5.
   * If ``options.iterations = 0``, the number of iterations is set to 100.

.. seealso:: `<https://en.wikipedia.org/wiki/Differential_evolution>`_

Attributes:
   F (double): Differential weight F in the range [0,1]. Default is 0.8.
   CR (double): Crossover probability CR in the range [0,1]. Default is 0.9.
   variant (int): Variants of the mutation scheme, value is 1, 2, ..., 10. Default is 2.
      Labeling: vector decision / number difference vectors / crossover strategy (binominal or exponential)
      
      * 1 - best/1/exp
      * 2 - rand/1/exp
      * 3 - rand-to-best/1/exp
      * 4 - best/2/exp
      * 5 - rand/2/exp
      * 6 - best/1/bin
      * 7 - rand/1/bin
      * 8 - rand-to-best/1/bin
      * 9 - best/2/bin
      * 10 - rand/2/bin

   adaptive_variant (int): Self-adaptation variants, value is 1 or 2. Default is 1.

      * 1 - jDE (Brest et al.)
      * 2 - iDE (Elsayed at al.)
   
*/
struct DiffEvolOptions
{
  double F = 0.8;

  double CR = 0.9;

  unsigned int variant = 2;

  unsigned int adaptive_variant = 1;
};


/**
:class:`BasinHoppingOptions` for the Basin Hopping algorithm.
This is a meta algorithm that uses another inner algorithm.
``CompassSearch`` or ``Subplex`` method can be used as inner algorithm. 

.. seealso:: `<https://en.wikipedia.org/wiki/Basin-hopping>`_

Attributes:
   inner (string): Inner algorithm. Either ``Subplex`` or ``Compass``. Default is ``Compass``. 
   stop (int): Runs of the inner algorithm without improvement. Default is 5.
   perturbation (double): Perturbation to be applied to each component of the decision vector of the best population. Default is 0.01.
*/
struct BasinHoppingOptions
{
  std::string inner = "Compass";

  unsigned int stop = 5;

  double perturbation = 0.01;
};


/**
:class:`BootstrapOptions` for the Bootstrap error analysis.

.. seealso:: `<https://en.wikipedia.org/wiki/Bootstrapping_(statistics)>`_

.. versionadded:: 1.1.0

Attributes:
   method (string): Bootstrap method. Default is "Wild".

     * ``Poisson``: Parametric bootstrap with Poisson statistics.
       The data points are resampled at each point from a Poisson distribution with an expectation value of the measured intensity.
     * ``Gaussian``: Parametric bootstrap with Gaussian statistics.
       The data points are resampled at each point from a Gaussian distribution with a mean value of the measured intensity and a with of the square root of the intensity.
     * ``Wild``:  The residuals of the initial fit are reweighted by a Gaussian distribution with a variance of one to generate a new data set.
       This method should be applied for heteroscedastic data sets (varying variance of all data points, like Time spectra).
       Can also be used for homoscedastic data sets.
     * ``Residuals``: The residuals of the initial fit are randomly redistributed over the fit curve to generate a new data set.
       This method should only be applied for homoscedastic data sets (same variance of all data, like typical Moessbauer spectra).
     * ``Smoothed``: For each data point random noise is added from a Gaussian distribution kernel with variance of one scaled by ``h``.
       The smoothing parameter is the scaling factor of the data point, so ``h = smooting_parameter * data_point``.
     * ``Smoothed_Uniform``: For each data point random noise is added from a uniform distribution kernel [-h, h].
       The smoothing parameter is the scaling factor of the data point, so ``h = smooting_parameter * data_point``
        
     .. seealso:: `<https://en.wikipedia.org/wiki/Homoscedasticity_and_heteroscedasticity>`_
   
   num_bootstrap (int): Number of evaluations for bootstrap error method. Default is 50.
   smoothing_parameter (float): The smoothing parameter is the scaling factor of the data point.
     So, the distribution is scaled by ``h = smooting_parameter * data_point``.
     The value determines how much noise is added to the data.
     Default is 0.01.
*/
struct BootstrapOptions
{
  const std::vector<std::string> bootstrap_methods
  {
    "Poisson",    // parametric bootstrap with Poisson model
    "Gaussian",   // parametric bootstrap with Gaussian model
    "Wild",       // Residual re-weighting, for heteroscedastic residuals
    "Residuals",  // Resampling of the residual , for homoscedastic residuals
    "Smoothed",    // smoothed bootstrap with random noise
    "Smoothed_Uniform"   // smoothed bootstrap with uniform distribution of random values
    //"StdDev"     // data reasignment by std dev of residuals
  };

  std::string method = "Wild";

  unsigned int iterations = 50;

  double smoothing_parameter = 0.01;
};


/**
:class:`OptimizerOptions` for the used algorithms in the :class:`Optimizer` and :class:`Fit` classes.

For more information on the different solvers and their algorithms see:

   * ceres solver [Ceres]_ `<http://ceres-solver.org/index.html>`_
   * NLopt [NLopt]_ `<https://nlopt.readthedocs.io/en/latest/>`_
   * pagmo [Pagmo]_ `<https://esa.github.io/pagmo2/index.html>`_

Attributes:
   method (string): Method used by the :class:`Optimizer` or :class:`Fit` module.
      All methods take boundary constraints except the ``LineSearch`` method. Default is ``LevMar``. 
      
      Gradient-based LOCAL fitting - **Ceres solver**.

         * ``LevMar`` - Levenberg-Marquardt algorithm. 
         * ``Dogleg`` - Dogleg algorithm.
         * ``SubDogleg`` - Subspace Dogleg algorithm.
         * ``LineSearch`` - Linear search algorithm. No boundary constraints.
      
      Gradient-free LOCAL fitting - **NLopt** or **Pagmo** solver.

         * ``Subplex`` - Nelder-Mead algorithm on a sequence of subspaces (*NLopt*).
         * ``Newuoa`` - New Unconstrained Optimization with quadratic Approximation algorithm (*NLopt*).
         * ``CompassSearch`` - Compass Search algorithm (*Pagmo*).

      Gradient-free GLOBAL fitting - **Pagmo** solver.

         * ``PagmoDiffEvol`` - Self-adaptive Differential Evolution 1220 algorithm.
         * ``AdaptiveDiffEvol`` - Self-adaptive Differential Evolution algorithm.
         * ``DiffEvol`` - Differential Evolution algorithm.
         * ``BeeColony`` - Artificial Bee Colony algorithm.
         * ``CMA-ES`` - Covariance Matrix Adaptation Evolutionary Strategy algorithm.
         * ``AntColony`` - Extended Ant Colony Optimization algorithm.
         * ``GreyWolf`` - Grey Wolf Optimizer algorithm.
         * ``ParticleSwarm`` - Particle Swarm Optimization algorithm.
         * ``ParticleSwarmGen`` - Particle Swarm Optimization Generational algorithm.
         * ``SimpleEvol`` - (N+1)-ES Simple Evolutionary Algorithm.
         * ``Genetic`` - Simple Genetic Algorithm.
         * ``Annealing`` - Simulated Annealing algorithm.
         * ``NaturalEvol`` - Exponential Natural Evolution Strategies algorithm.
         * ``BasinHopping`` - Monotonic Basin Hopping with local algorithm.

   local (string): A subsequent local solver following the method. If it is the same as method no second fit is performed. Default is ``LevMar``.
   population (int): Initial population for global fit methods. When value is zero the population is set by the fit method. Default is 0.
   iterations (int): Maximum of iterations of a solver.
      If set to zero the solver method will set a reasonable value. Default is 0.
   max_time (float): Maximum run time of the ceres solver or the Nlopt solver (seconds). Default is 300.
   output (bool): Set output during fitting. Default is ``True``.
   report (string): Output of the ceres solver. ``None``, ``Brief`` or ``Full``. Default ``Brief``.
   function_tolerance (float): Tolerance of change of the cost value for termination of the solver :math:`|(\Delta cost) / cost|`.
                               Only for methods that support this option. Default is 1.0e-10.
   gradient_tolerance (float): Tolerance of change of the gradient for termination of the solver.
                               Only for methods that support this option. Default is 1.0e-10.
   parameter_tolerance (float): Tolerance of change of the parameter for termination of the solver.
                                Only for methods that support this option. Default is 1.0e-4.
   cost_relative_step_size (float): Relative step size for the numerical derivatives in the ceres solver. Default is 1e-6.

     .. versionchanged:: 1.1.0 - Default changed from 0.01 to 1e-6.
   
   file_output (bool): Determines if an output file with the fit results is generated.
     The file is named after the id and each time a fit is run the filename is automatically increased in numbers.

     .. versionadded:: 1.1.0

   error_method (string): Set the method of error calculations of the fit parameters.

     * ``Gradient``: Based on the gradient-based local algorithms from the ceres solver.
       The ceres solver will be called as last fit instance to determine the error estimates.
       Default.
     * ``Bootstrap``: Bootstrap method for error analysis.
       A resampling strategy to determine errors of unknown distributions.
       This method is computationally expensive and might take quite long.
       See :class:`BootstrapOptions` for options on this method.
     * ``None``: No error analysis.

     .. versionadded:: 1.1.0

   LineSearch (:class:`LineSearchOptions`): Options for the LineSearch method of the ceres solver.
   DiffEvol (:class:`DiffEvolOptions`): Options for all Differential Evolution algorithms.
   CompassSearch (:class:`CompassSearchOptions`): Options for the Compass Search algorithm.
   BasinHopping (:class:`BasinHoppingOptions`): Options for the Basin Hopping algorithm.
   Bootstrap (:class:`BootstrapOptions`): Options for the Bootstrap error analysis.
     
     .. versionadded:: 1.1.0
*/
struct OptimizerOptions {
  OptimizerOptions()
  {
    LineSearch = new LineSearchOptions();
    CompassSearch = new CompassSearchOptions();
    DiffEvol = new DiffEvolOptions();
    BasinHopping = new BasinHoppingOptions();
    Bootstrap = new BootstrapOptions();
  }

  ~OptimizerOptions()
  {
    delete LineSearch;
    delete CompassSearch;
    delete DiffEvol;
    delete BasinHopping;
    delete Bootstrap;
  }
 
  bool InitialCheck();

  // GENERAL OPTIONS
  OptimizerModule fit_module = OptimizerModule::Invalid;

  OptimizerModule fit_module_local = OptimizerModule::Invalid;

  bool global_fit = false;

  std::string method = "LevMar";

  std::string local = "LevMar";

  // gradient based methods by Ceres
  const std::vector<std::string> ceres_methods
  {
    "LevMar",     // LevMar - Ceres Levenberg-Marquardt, trust region method
    "Dogleg",     // Dogleg - Ceres traditional Dogleg, trust region method
    "SubDogleg",  // SubDogleg - Ceres subspace dogleg, trust region method
    "LineSearch"  // LineSearch - Ceres line search // no boundaries applied
  };

  const std::vector<std::string> no_boundary_methods
  {
    "LineSearch"
  };
  
  // local gradient free methods by NLopt, only the implemented ones worked reliable for fitting
  const std::vector<std::string> nlopt_local_methods
  {
    "Newuoa",
    "Subplex"
  };

  const std::vector<std::string> pagmo_methods
  {
    "BeeColony",
    "CMA-ES",
    "CompassSearch",
    "DiffEvol",
    "PagmoDiffEvol",
    "AntColony",
    "GreyWolf",
    "ParticleSwarm",
    "ParticleSwarmGen",
    "AdaptiveDiffEvol",
    "SimpleEvol",
    "Genetic",
    "Annealing",
    "NaturalEvol",
    "BasinHopping",
  };

  // These values drastically influence the fit as they determine the desired quality
  double function_tolerance = 1.0e-10;

  double gradient_tolerance = 1.0e-10;
  
  double parameter_tolerance = 1.0e-4;

  double max_time = 300;  // in seconds
  
  unsigned int population = 0;  // 0 = set method to default in Nlopt and in Differential Evolution
  
  unsigned int iterations = 0;  // 0 = methods decides, also sets the maximum for other stop parameters

  // Ceres cost function options
  // numeric differentiation step size. must be large enough such that the changes in the parameter are detectable.
  // especially for distributions the value should be ~1e-2, the default of 1e-6 is too low.
  double cost_relative_step_size = 1.0e-6; //0.01;

  bool file_output = true;

  const std::vector<std::string> error_method_list
  {
    "None",
    "Gradient",
    "Bootstrap"
  };

  std::string error_method = "Gradient";

  // determines if output is print during fitting.
  bool output = true;

  // ceres output summary options
  const std::vector<std::string> report_list
  {
    "None",
    "Brief",
    "Full"
  };

  std::string report = "Brief";

  LineSearchOptions* LineSearch = nullptr;

  CompassSearchOptions* CompassSearch = nullptr;
  
  DiffEvolOptions* DiffEvol = nullptr;

  BasinHoppingOptions* BasinHopping = nullptr;

  BootstrapOptions* Bootstrap = nullptr;
};

#endif // NEXUS_OPTIMIZER_OPTIONS_H_
