// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module nxvariable

%{
    #define SWIG_FILE_WITH_INIT
    #include "nxvariable.h"
%}


%feature("autodoc", "List of :class:`Var` objects.") std::vector<Var*>;

%template(FitVariables) std::vector<Var*>;


%feature("shadow") Var::Var %{
  r"""    
  Constructor for the :class:`Var` class.

  Args:
     value (float): Value of the :class:`Var`.
     min (float): Minimum value used in fits with boundary conditions.
     max (float): Maximum value used in fits with boundary conditions.
     fit (bool): Set to *True* if the variable is to be fitted.
     id (string): User identifier.
     equality (function): A function to define an equality constraint for fitting. Must return a float.

  Attributes:
     value (float): Value of the :class:`Var`.
     min (float): Minimum value used in fits with boundary conditions.
     max (float): Maximum value used in fits with boundary conditions.
     fit (bool): Set to *True* if the variable is to be fitted.
     id (string): User identifier.
     equality (function): A function to define an equality constraint for fitting. Must return a float.
     operator + (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
     operator - (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
     operator * (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
     operator / (operator): Operator for :class:`Var` object. It works on the :attr:`Var.value` only.
     operator >> (operator): Operator for :class:`Var` object. Reassign :attr:`Var.value` by setting :class:`Var` to new .value directly.
  """
  def __init__(self, value = 0.0, min = -np.inf, max = np.inf, fit = False, id = "", equality = None):
      
      if not callable(equality) and equality != None:
        raise Exception("equality must be a function. Use equality=function or equality=lambda: function(*args)")
      
      class Equality_Var(Equality):
        def __init__(self, id):
            super().__init__(id)

        def Function(self):
            return 0.0
      
      self._equality_function = equality

      self._equality = None
      
      if equality != None:
          self._equality = Equality_Var(id+" - equality")
          self._equality.Function = equality

      _cnexus.Var_swiginit(self, _cnexus.new_Var(value, min, max, fit, id, self._equality))
%}



%include "nxvariable.h"



%extend Var {
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this", "_equality", "_equality_function") and not hasattr(self, attr):
      raise Exception("Var does not have attribute {}".format(attr))

    super().__setattr__(attr, val)

  def __repr__(self):
    output = "Var.value = {}, ".format(self.value)
    output += ".min = {}, ".format(self.min)
    output += ".max = {}, ".format(self.max)
    output += ".fit: {}, ".format(self.fit)
    output += ".id: {}".format(self.id)
    return output

  def Copy(self):
    r"""
    Copy the :class:`Var` object.
    """
    new_var = Var(self.value, self.min, self.max, self.fit, self.id)
    return new_var
  }
}

