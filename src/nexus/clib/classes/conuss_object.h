// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Conuss Object class


#ifndef NEXUS_CONUSS_OBJECT_H_
#define NEXUS_CONUSS_OBJECT_H_

#include <string>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/nxobject.h"

/*
Constructor for a :class:`ConussObject`.

see conuss_object.i
*/
class ConussObject : public NxObject {
public:
  ConussObject(
    const std::string file_name,
    const std::vector<Complex> scattering_factors,
    std::complex<double>* data, int num_points_distribution, int num_points_detuning, int nx, int ny,
    const std::vector<double> angles,
    const std::vector<double> detuning,
    Var* const angle,
    Var* const divergence,
    const std::string id = ""
  );
 
  // NxObject functions
  void Update() {};

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  /**
  Set the :attr:`scattering_factor` and the :attr:`object_matrix` to the ones at the given index.
  The index corresponds to the index of the angles array.
  The set objects are used in the functions :func:`ElectronicAmplitude` and :func:`ElectronicAmplitudeMatrix`.
  These functions are also used by *Nexus* in for calculations of electronic properties and amplitude calculations.

  Args:
      index(int): Used index of the scattering matrices array.
  */
  void SetPropertiesAtIndex(size_t index);

  /**
  Returns the relative electronic amplitude behind the object. Corresponds to the electronic scattering factor.

  Args:
     energy (float): no dependence

  Returns:
     complex: electronic scattering factor set during initialization
  */
  Complex ElectronicAmplitude(const double energy);

  /**
  Electronic scattering matrix of the object.

  Args:
     energy (float): Not used.

  Returns:
     list: 2x2 scattering matrix.
  */
  Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy);

  /**
  Returns the scattering factor corresponding to the angles array.

  Args:
     index (int): Index of the scattering factor corresponding to the angles array.

  Returns:
     Complex: scattering factor.
  */
  Complex ScatteringFactorAtIndex(size_t index) const;

  /**
  Returns the detuning-independent object matrix corresponding to the angles array. Is of layer matrix type.
  
  Args:
     isotope (MoessbauerIsotope): Moessbauer isotope of the experiment
     detuning (ndarray or list): n-dimensional array with detuning values
  
  Returns:
     array: n-dimensional array of 2x2 complex arrays.
  */
  ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& new_detuning,
    const bool calc_transitions = true);

  /**
  Returns the detuning-dependent object matrix. Corresponds to a layer matrix type.

  Args:
     index (int): Index of the object matrix corresponding to the angles array.

  Returns:
     array: n-dimensional array of 2x2 complex arrays.
  */
  ObjectMatrix2 ObjectMatrixAtIndex(size_t index) const;

  /**
  Returns the detuning-dependent object matrix on a new detuning grid. Corresponds to a layer matrix type.

  Args:
     index (int): Index of the object matrix corresponding to the angles array.
     new_detuning (ndarray): new detuning grid on which the object matrix is interpolated.

  Returns:
     array: n-dimensional array of 2x2 complex arrays.
  */
  ObjectMatrix2 ObjectMatrixAtIndexDetuning(size_t index, const std::vector<double>& new_detuning) const;

  std::string file_name = "";

  Complex scattering_factor = Complex(0.0, 0.0);

  std::vector<Complex> scattering_factors{};

  ObjectMatrix2 object_matrix{};

  std::vector<ObjectMatrix2> object_matrices{};

  std::vector<double> angles{};

  std::vector<double> detuning{};

  Var* angle = nullptr;

  Var* divergence = nullptr;

  // Angular divergence 
  void PopulateDivergence();

  void ResetDivergence();

  size_t GetDivergenceDistributionSize();
  
  size_t divergence_index = 0;

  std::vector<double> divergence_distribution_weight{};

private:
  /*
  Converts the input numpy array of type (std::complex<double>* data, int num_distr_points, int num_detuning_points, int nx, int ny) to a vector of LayerMatrices.
  The function assumes an array in the form (N_distribution, N_detuning, 2, 2).
  The matrix entries in CONUSS are:
  [0,0] pi-pi
  [0,1] pi-sigma
  [1,0] sigma-pi
  [1,1] sigma-sigma

  Args:
     data (pointer): pointer to complex array.

  Returns:
     LayerMatrix2: n-dimensional array of 2x2 complex arrays.
  */
  std::vector<LayerMatrix2> ConussDataToLayerMatrices(const Complex* data, const int n_distribution, const int n_detuning) const;

  Complex* const data = nullptr;

  const int num_points_distribution;

  const int num_points_detuning;
  
  // matrix dimensions
  const int nx;
  
  const int ny;
};

#endif // NEXUS_CONUSS_OBJECT_H_
