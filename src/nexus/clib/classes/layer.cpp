// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "layer.h"

#include <iostream>
#include <complex>
#include <algorithm>
#include <numeric>

#include "../nexus_definitions.h"
#include "../math/conversions.h"
#include "../scattering/electronic_scattering.h"


using namespace std::complex_literals;


Layer::Layer(
  Material* const material_,
  Var* const thickness_,
  Var* const roughness_,
  Var* const thickness_fwhm_,
  const std::string id_
) :
  material(material_),
  thickness(thickness_),
  roughness(roughness_),
  thickness_fwhm(thickness_fwhm_),
  id(id_)
{
  if (thickness->value < 0.0)
    thickness->value = INFINITY;
  
  Update();
}


void Layer::Update()
{
  material->Update();
}


void Layer::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(thickness);

  fit_handler->PrepareFitVariable(roughness);

  fit_handler->PrepareFitVariable(thickness_fwhm);

  material->PopulateFitHandler(fit_handler);
}


// electronic
// forward

Complex Layer::ElectronicForwardScatteringFactor(const double energy)
{
  forward_scattering_factor = electronic::forward::ScatteringFactor(material, energy);
  
  return forward_scattering_factor;
}

Complex Layer::ElectronicRefractiveIndex(const double energy)
{
  forward_refractive_index = electronic::forward::RefractiveIndex(material, energy);
  
  return forward_refractive_index;
}

double Layer::CriticalAngle(const double energy)
{
  return electronic::grazing::CriticalAngle(material, energy);;
}

Complex Layer::ElectronicForwardTransmissionFactor(const double energy)
{
  const double kvector = conversions::EnergyToKvector(energy);

  forward_scattering_factor = electronic::forward::ScatteringFactor(material, energy);

  electronic_propagation_factor = electronic::forward::PropagationFactor(forward_scattering_factor, kvector);

  electronic_transmission_factor = electronic::forward::TransmissionFactor(electronic_propagation_factor, thickness->value * 1.0e-9);

  return electronic_transmission_factor;
}

Complex Layer::ElectronicForwardTransmissionFactor(const double energy, const double thickness_)
{
  const double kvector = conversions::EnergyToKvector(energy);

  forward_scattering_factor = electronic::forward::ScatteringFactor(material, energy);

  electronic_propagation_factor = electronic::forward::PropagationFactor(forward_scattering_factor, kvector);

  electronic_transmission_factor = electronic::forward::TransmissionFactor(electronic_propagation_factor, thickness_ * 1.0e-9);

  return electronic_transmission_factor;
}


double Layer::ElectronicForwardTransmission(const double energy)
{
  return std::norm(ElectronicForwardTransmissionFactor(energy));
}

double Layer::ElectronicForwardTransmission(const double energy, const double thickness_)
{
  return std::norm(ElectronicForwardTransmissionFactor(energy, thickness_));
}

// end forward
// 
// grazing

Complex Layer::ElectronicGrazingScatteringFactor(const double energy, const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(energy, angle);

  const Complex scattering_factor = electronic::grazing::ScatteringFactor(material, energy, kvector_z);

  return scattering_factor;
}

Eigen::Matrix2cd Layer::ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z)
{
  if (isinf(thickness->value) == true)
  {
    electronic_layer_matrix = electronic::grazing::LayerMatrixBottom(scattering_factor, kvector_z);
  }
  else
  {
    electronic_propagation_matrix = electronic::grazing::PropagationMatrix(scattering_factor, kvector_z);

    electronic_layer_matrix = electronic::grazing::LayerMatrix(electronic_propagation_matrix, thickness->value * 1.0e-9);
  }

  return electronic_layer_matrix;
}

Eigen::Matrix2cd Layer::ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z, const double thickness_)
{
  if (isinf(thickness_) == true)
  {
    electronic_layer_matrix = electronic::grazing::LayerMatrixBottom(scattering_factor, kvector_z);
  }
  else
  {
    electronic_propagation_matrix = electronic::grazing::PropagationMatrix(scattering_factor, kvector_z);

    electronic_layer_matrix = electronic::grazing::LayerMatrix(electronic_propagation_matrix, thickness_ * 1.0e-9);
  }

  return electronic_layer_matrix;
}

// end electronic - grazing


// nuclear - forward

ScatteringMatrix2 Layer::ForwardScatteringMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid, const bool calc_transitions)
{
  const ScatteringMatrix2 f_matrix = nuclear::forward::ScatteringMatrix(material, isotope, detuning_grid, calc_transitions);

  return f_matrix;
}

PropagationMatrix2 Layer::ForwardPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& f_matrix)
{
  const PropagationMatrix2 F_matrix = nuclear::forward::PropagationMatrix(f_matrix, isotope->kvector);
  
  return F_matrix;
}

LayerMatrix2 Layer::ForwardTransmissionMatrix(const PropagationMatrix2& F_matrix)
{
  const LayerMatrix2 layer_matrix = nuclear::forward::TransmissionMatrix(F_matrix, thickness->value * 1.0e-9);  // convert to meter
  
  return layer_matrix;
}

LayerMatrix2 Layer::ForwardTransmissionMatrix(const PropagationMatrix2& F_matrix, const double thickness_)
{
  const LayerMatrix2 layer_matrix = nuclear::forward::TransmissionMatrix(F_matrix, thickness_ * 1.0e-9);  // convert to meter
  
  return layer_matrix;
}

// for Python user
PropagationMatrix2 Layer::ForwardPropagationMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid)
{
  const ScatteringMatrix2 f_matrix = ForwardScatteringMatrix(isotope, detuning_grid, true);

  const PropagationMatrix2 F_matrix = nuclear::forward::PropagationMatrix(f_matrix, isotope->kvector);

  return F_matrix;
}

// for Python user
LayerMatrix2 Layer::ForwardTransmissionMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid)
{
  const ScatteringMatrix2 f_matrix = ForwardScatteringMatrix(isotope, detuning_grid, true);

  const PropagationMatrix2 F_matrix = nuclear::forward::PropagationMatrix(f_matrix, isotope->kvector);

  const LayerMatrix2 layer_matrix = nuclear::forward::TransmissionMatrix(F_matrix, thickness->value * 1.0e-9);  // convert to meter

  return layer_matrix;
}

// Thickness distribution functions
void Layer::PopulateDistribution(const size_t distribution_points)
{
  thickness_distribution.resize(1);

  thickness_distribution_weight.resize(1);

  const double sigma = thickness_fwhm->value / constants::kSigmaToFHWM;

  const double range = 6.0 * sigma;  // good range for Gaussian distribution

  const double border = thickness->value - range / 2.0;

  if (distribution_points > 1 &&
      range > 0.0)
  {
    thickness_distribution.resize(distribution_points);

    thickness_distribution_weight.resize(distribution_points);

    const double step = range / (distribution_points - 1.0);

    for (size_t i = 0; i < distribution_points; ++i)
    {
      const double thickness_tmp = border + step * i;

      thickness_distribution[i] = thickness_tmp;

      if (thickness_tmp > 0)
      {
        thickness_distribution_weight[i] = exp(-0.5 * pow((thickness_tmp - thickness->value) / sigma, 2.0));
      }
      else
      {
        thickness_distribution_weight[i] = 0.0;
      }
    }

    const double norm = std::accumulate(thickness_distribution_weight.begin(), thickness_distribution_weight.end(), 0.0);

    std::for_each(thickness_distribution_weight.begin(), thickness_distribution_weight.end(), [norm](double& dist)
      {
        dist /= norm;
      }
    );

  }
  else
  {
    thickness_distribution[0] = thickness->value;

    thickness_distribution_weight[0] = 1.0;
  }
}

void Layer::ResetDistributionIndex()
{
  distribution_index = 0;
}

// end nuclear - forward

// nuclear - grazing

ScatteringMatrix2 Layer::GrazingScatteringMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const double angle, const bool calc_transitions)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  const ScatteringMatrix2 grazing_f_matrix = nuclear::grazing::ScatteringMatrix(material, isotope, detuning_grid, kvector_z, calc_transitions);

  return grazing_f_matrix;
}

PropagationMatrix4 Layer::GrazingPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
  const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  const PropagationMatrix4 grazing_F_matrix = nuclear::grazing::PropagationMatrix(grazing_f_matrix, kvector_z);

  return grazing_F_matrix;
}

LayerMatrix4 Layer::GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
  const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  LayerMatrix4 grazing_L_matrix {};

  if (isinf(thickness->value) == true ||
      thickness->value < 0)
  {
    grazing_L_matrix = nuclear::grazing::LayerMatrixBottom(grazing_f_matrix, kvector_z);
  }
  else
  {
    const PropagationMatrix4 grazing_F_matrix = nuclear::grazing::PropagationMatrix(grazing_f_matrix, kvector_z);

    grazing_L_matrix = nuclear::grazing::LayerMatrix(grazing_F_matrix, thickness->value * 1.0e-9);  // convert to meter
  }

  return grazing_L_matrix;
}

// for Python access
PropagationMatrix4 Layer::GrazingPropagationMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  const ScatteringMatrix2 grazing_f_matrix  = GrazingScatteringMatrix(isotope, detuning_grid, angle, true);

  const PropagationMatrix4 grazing_F_matrix = nuclear::grazing::PropagationMatrix(grazing_f_matrix, kvector_z);

  return grazing_F_matrix;
}

LayerMatrix4 Layer::GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  const ScatteringMatrix2 grazing_f_matrix = GrazingScatteringMatrix(isotope, detuning_grid, angle, true);

  LayerMatrix4 grazing_L_matrix{};

  if (isinf(thickness->value) == true ||
      thickness->value < 0)
  {
    grazing_L_matrix = nuclear::grazing::LayerMatrixBottom(grazing_f_matrix, kvector_z);
  }
  else
  {
    const PropagationMatrix4 grazing_F_matrix = nuclear::grazing::PropagationMatrix(grazing_f_matrix, kvector_z);

    grazing_L_matrix = nuclear::grazing::LayerMatrix(grazing_F_matrix, thickness->value * 1.0e-9);  // convert to meter
  }

  return grazing_L_matrix;
}

// end nuclear - grazing


double Layer::KzSigma(const double energy, const double angle) const
{
  const double kvector_z = conversions::EnergyToKvectorZ(energy, angle);

  return  kvector_z * roughness->value * 1.0e-9;  // nm to m
}
