// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module(directors="1") function_time

%{ 
    #define SWIG_FILE_WITH_INIT
    #include "function_time.h"
%}


%feature("director") FunctionTime;


%feature("shadow") FunctionTime::CreateDetuning(const double max_detuning, int num_steps) const %{
def CreateDetuning(self, max_detuning, num_points):
    r"""
    Compute a detuning grid needed for energy based calculations.
    The detuning grid is created almost symmetrically in the range

    .. math:: -max\_detuning, -max\_detuning + step, ..., max\_detuning - step

    where :math:`step = 2 \frac{max\_detuning}{num\_points}`.

    Args:
       float: maximum detuning in units of :math:`\Gamma`.
       int: number of points, must be even.

    Returns:
       ndarray: The detuning grid in units of :math:`\Gamma`.
    """
    return np.array(_cnexus.FunctionTime_CreateDetuning(self, max_detuning, num_points))
%}



%include "function_time.h"



%extend FunctionTime{
  %pythoncode{

  def __setattr__(self, attr, val):
    if isinstance(val, (int, float)):
      try:
        super().__setattr__(attr, val)
        return
      except:
        setattr(getattr(self, attr), "value", val)
        return

    if isinstance(val, (list, tuple)):
      val = FitVariables(val)

    super().__setattr__(attr, val)

  }
}
