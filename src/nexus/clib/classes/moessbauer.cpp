// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "moessbauer.h"

#include "../nexus_definitions.h"
#include "../utilities/errors.h"
#include "../math/constants.h"
#include "../math/conversions.h"
#include "../classes/element_maps.h"


MoessbauerIsotope::MoessbauerIsotope(
  const std::string isotope_,
  const std::string element_,
  const double mass_,
  const double energy_,
  const double lifetime_,
  Var* const internal_conversion_,
  const Multipolarity multipolarity_,
  Var* const  mixing_ratio_E2M1_,
  const double spin_ground_,
  const double spin_excited_,
  Var* const gfactor_ground_,
  Var* const gfactor_excited_,
  Var* const quadrupole_ground_,
  Var* const quadrupole_excited_,
  Var* const interference_term_,
  const double natural_abundance_
) : 
  isotope(isotope_),
  element(element_),
  mass(mass_),
  energy(energy_), // eV
  lifetime(lifetime_),
  internal_conversion(internal_conversion_),
  multipolarity(multipolarity_),
  mixing_ratio_E2M1(mixing_ratio_E2M1_),
  spin_ground(spin_ground_),
  spin_excited(spin_excited_),
  gfactor_ground(gfactor_ground_),
  gfactor_excited(gfactor_excited_),
  quadrupole_ground(quadrupole_ground_),
  quadrupole_excited(quadrupole_excited_),
  interference_term(interference_term_),
  natural_abundance(natural_abundance_)
{
  Update();

  if (natural_abundance < 0.0)
  {
    errors::ErrorMessage("MoessbauerIsotope", "natural abundance smaller 0. Set to 0.");

    natural_abundance = 0.0;
  }
  else if (natural_abundance > 1.0)
  {
    errors::ErrorMessage("MoessbauerIsotope", "natural abundance larger 1. Set to 1.");

    natural_abundance = 1.0;
  }
}

MoessbauerIsotope MoessbauerIsotope::Copy() const
{
  return *this;
}


void MoessbauerIsotope::Update()
{
  wavelength = conversions::EnergyToWavelength(energy);  // m

  kvector = conversions::EnergyToKvector(energy);  // 1/m

  half_lifetime = log(2) * lifetime; // s

  gamma = constants::khbar / lifetime;  // eV

  gamma_photon = gamma / (1.0 + internal_conversion->value);

  gamma_electron = gamma / (1.0 + 1.0 / internal_conversion->value);

  quality_factor = energy / gamma;

  if (multipolarity == Multipolarity::E1)
  {
    L = 1;
    lambda1 = 1;
  }
  else if (multipolarity == Multipolarity::M1)
  {
    L = 1;
    lambda1 = 0;
  }
  else if (multipolarity == Multipolarity::E2)
  {
    L = 2;
    lambda1 = 1;
  }
  else if (multipolarity == Multipolarity::M1E2)
  {
    L = 1;
    lambda1 = 0;

    L2 = 2;
    lambda2 = 1;
  }
  else if (multipolarity == Multipolarity::M2)
  {
    L = 2;
    lambda1 = 0;
  }

  number_L = 2 * L + 1;
  number_L2 = 2 * L2 + 1;

  atomic_number = element_maps::atomic_number_map.find(element)->second;

  number_ground_states = static_cast<int>(round(2 * spin_ground + 1));
  number_excited_states = static_cast<int>(round(2 * spin_excited + 1));

  magnetic_moment_ground = constants::kNuclearMagneton * gfactor_ground->value * spin_ground;  // eV/T
  magnetic_moment_excited = constants::kNuclearMagneton * gfactor_excited->value * spin_excited;  // eV/T

  nuclear_cross_section = 2.0 * constants::kPi / pow(kvector, 2) * number_excited_states /
    number_ground_states / (1.0 + internal_conversion->value);

  clebsch_gordon_Ig_magnetic = quantum::ClebshGordon(spin_ground, 1, spin_ground, 0, spin_ground, spin_ground);
  clebsch_gordon_Ie_magnetic = quantum::ClebshGordon(spin_excited, 1, spin_excited, 0, spin_excited, spin_excited);

  clebsch_gordon_Ig_quadrupole = quantum::ClebshGordon(spin_ground, 2, spin_ground, 0, spin_ground, spin_ground);
  clebsch_gordon_Ie_quadrupole = quantum::ClebshGordon(spin_excited, 2, spin_excited, 0, spin_excited, spin_excited);

  YLM1 = quantum::VecSpherHarmPolarization(L, lambda1);

  // |weight_M1|^2 + |weight_E2|^2 = 1
  // actually: |weight_M1|^2 + |weight_E2|^2 = 1/(1 + internal_conversion), but this is accounted for in the nuclear cross section
  if (multipolarity == Multipolarity::M1E2)
  {
    mixing_coefficient_1 = 1.0 / sqrt(1.0 + pow(mixing_ratio_E2M1->value, 2));  // M1
    mixing_coefficient_2 = mixing_coefficient_1 * mixing_ratio_E2M1->value;  // E2

    YLM2 = quantum::VecSpherHarmPolarization(L2, lambda2);
  }
  else
  {
    mixing_coefficient_1 = 1.0;
    mixing_coefficient_2 = 0.0;

    YLM2 = {};
  }
}


void MoessbauerIsotope::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(internal_conversion);

  fit_handler->PrepareFitVariable(mixing_ratio_E2M1);

  fit_handler->PrepareFitVariable(gfactor_ground);

  fit_handler->PrepareFitVariable(gfactor_excited);

  fit_handler->PrepareFitVariable(quadrupole_ground);

  fit_handler->PrepareFitVariable(quadrupole_excited);

  fit_handler->PrepareFitVariable(interference_term);
}
