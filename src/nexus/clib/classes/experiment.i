// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module experiment

%{
    #define SWIG_FILE_WITH_INIT
    #include "experiment.h"
%}


%feature("shadow") Experiment::Experiment %{
  def __init__(self,
               beam,
               objects,
               isotope = None,
               id = ""):

    if isinstance(objects, (NxObject)):
      objects = [objects]

    if isotope is None:
      isotope = MoessbauerIsotope()

    self._beam = beam
    self._isotope = isotope

    # the python class object must reference to the python list with objects pointers
    # in order to maintain securely in the python domain 
    self._objects = objects
    
    _cnexus.Experiment_swiginit(self, _cnexus.new_Experiment(beam, objects, isotope, id))
%}


%include "experiment.h"



%extend Experiment{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this", "_beam", "_isotope", "_objects") and not hasattr(self, attr):
      raise Exception("Experiment does not have attribute {}".format(attr))

    if attr == "beam":
      self._beam = val

    if attr == "isotope":
      self._isotope = val

    if attr == "objects":
      if isinstance(val, (NxObject)):
        val = [val]
        self._objects = val  # ref to python list
        val = VectorNxObject(val)
      elif isinstance(val, (list, tuple)):
        self._objects = val  # ref to python list
        val = VectorNxObject(val)

    super().__setattr__(attr, val)

  def __repr__(self):
    self.Update()
    output = "Experiment:\n"
    output += "  .id: {}\n".format(self.id)
    output += "  .beam.id: {}\n".format(self.beam.id)
    output += "  .objects:\n"
    i = 0
    for obj in self.objects:
      type = ""
      if obj.object_id == NxObjectId_Sample:
        type = "Sample"
      elif  obj.object_id == NxObjectId_Analyzer:
        type = "Analyzer"
      elif obj.object_id == NxObjectId_FixedObject:
        type = "FixedObject"
      elif obj.object_id == NxObjectId_ConussObject:
        type = "ConussObject"
      output += "    index: {}, type: {}, .id: {}\n".format(i, type, obj.id)
      i += 1

    return output

  }
}