  // Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "sample.h"

#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include <Eigen/QR>

#include "../math/conversions.h"
#include "../math/intensity.h"
#include "../math/matrix.h"
#include "../classes/moessbauer.h"
#include "../classes/material.h"


// The effective layer system is automatically calculated in case the roughness is set to "e". The instances where this happens are
// constructor, to get the new layer system after construction
// AddLayers, to get the new layer system after construction
// 
// and for the actual calculations, especially fitting, where the constructors are not called again but values changes, in
// ElectronicGrazingSampleMatrixEffectiveRoughness
// GrazingSampleMatrixEffectiveRoughness

// TODO: check propagation matrix for bottom layer, see comment below
// TODO: check that the effective system calculates the correct steps sizes and depths


namespace sample_definitions
{
  static Composition kAirComposition = { std::make_pair("N", 1.562), std::make_pair("O", 0.42), std::make_pair("C", 0.0003), std::make_pair("Ar", 0.0094) };
  static Var* const kDensity = new Var(0.0012041, 0.0, 0.0012041, false);
  static Var* const kAbundance = new Var(0.0, 0.0, 0.0, false);
  static Var* const kMoessbauerDummy = new Var(0.0, 0.0, 0.0, false);
  static Var* const kLambMoessbauer = new Var(0.0, 0.0, 0.0, false);
  static MoessbauerIsotope* const kNone = new MoessbauerIsotope("none", "", 0.0, 0.0, 0.0, kMoessbauerDummy, Multipolarity::M1, kMoessbauerDummy, 0.0, 0.0, kMoessbauerDummy, kMoessbauerDummy, kMoessbauerDummy, kMoessbauerDummy, kMoessbauerDummy, 0.0);
  static Material* const kAirMaterial = new Material(kAirComposition, kDensity, kNone, kAbundance, kLambMoessbauer, {}, "air");
  static Var* const kThickness = new Var(0.0, 0.0, 0.0, false);
  static Var* const kRoughness = new Var(0.0, 0.0, 0.0, false);
  static Var* const kThicknessFwhm = new Var(0.0, 0.0, 0.0, false);
  static Layer* const kAirLayer = new Layer(kAirMaterial, kThickness, kRoughness, kThicknessFwhm, "air");
}


GrazingSample::GrazingSample(
  const std::vector<Layer*> layers_,
  const std::string geometry_,
  Var* const angle_,
  const double length_,
  const std::string roughness_,
  const double effective_thickness_,
  const std::vector<double> drive_detuning_,
  FunctionTime* const function_time_,
  Var* const divergence_,
  const std::string id_
) :
  BasicSample(layers_, drive_detuning_, function_time_, NxObjectId::GrazingSample, id_),
  geometry(geometry_),
  angle(angle_),
  length(length_),
  roughness(roughness_),
  effective_thickness(effective_thickness_),
  divergence(divergence_)
{
  if (!(geometry_ == "r" ||
        geometry_ == "t"))
  {
    errors::WarningMessage("GrazingSample", ".geometry not recognized. Set to reflectivity.");

    geometry = "r";
  }

  if (!(roughness_ == "a" ||
        roughness_ == "n" ||
        roughness_ == "e"))
  {
    errors::WarningMessage("GrazingSample", ".roughness not recognized. Set to analytical.");

    roughness = "a";
  }

  if (roughness == "e")
    EffectiveLayerSystem(false);
}

// cycle through layers in sample. Update only if not already Updated
void GrazingSample::Update()
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  for (Layer* lay : unique_layers)
    lay->Update();
}

void GrazingSample::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(angle);

  fit_handler->PrepareFitVariable(divergence);

  if (function_time != nullptr)
  {
    for (Var* var : function_time->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  for (Layer* lay : layers)
    lay->PopulateFitHandler(fit_handler);
}

/* electronic */

// roughness model dependent sample matrices

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixNoRoughness(const double kvector_z)
{
  Eigen::Matrix2cd sample_matrix = Eigen::Matrix2cd::Identity();

  for (Layer* lay : layers)
  {
    sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z) * sample_matrix;

    if (isinf(lay->thickness->value) == true)
      break;
  }

  return sample_matrix;
}

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixAnalyticRoughness(const double kvector_z, Layer* pre_layer)
{
  Eigen::Matrix2cd sample_matrix = Eigen::Matrix2cd::Identity();

  for (Layer* lay : layers)
  {
    if (lay->roughness->value > 0.0)
    {
      sample_matrix = electronic::grazing::RoughnessLayerMatrix(pre_layer->grazing_scattering_factor,
        lay->grazing_scattering_factor, kvector_z, lay->roughness->value * 1e-9) * sample_matrix;
    }

    sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z) * sample_matrix;

    if (isinf(lay->thickness->value) == true)
      break;

    pre_layer = lay;
  }

  return sample_matrix;
}

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixEffectiveRoughness(const double kvector_z)
{
  EffectiveLayerSystem(false);

  Eigen::Matrix2cd sample_matrix = Eigen::Matrix2cd::Identity();

  for (EffectiveLayer& eff_layer : effective_layers)
  {
    sample_matrix = eff_layer.ElectronicGrazingLayerMatrix(eff_layer.grazing_scattering_factor, kvector_z) * sample_matrix;

    if (isinf(eff_layer.thickness))
      break;
  }

  return sample_matrix;
}

// thickness dependent sample matrices 
// these thickness dependent functions are only needed for the field amplitudes in the layer system
Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixNoRoughnessThickness(const double kvector_z, const double thickness)
{
  double thickness_buffer = thickness;

  Eigen::Matrix2cd sample_matrix = Eigen::Matrix2cd::Identity();

  for (Layer* lay : layers)
  {
    if (thickness_buffer >= lay->thickness->value)
    {
      sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z) * sample_matrix;

      thickness_buffer -= lay->thickness->value;
    }
    else if (isinf(lay->thickness->value))
    {
      sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z) * sample_matrix;

      break;
    }
    else
    {
      sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z, thickness_buffer)
        * sample_matrix;

      break;
    }
  }

  return sample_matrix;
}

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixAnalyticRoughnessThickness(const double kvector_z, Layer* pre_layer, const double thickness)
{
  double thickness_buffer = thickness;

  Eigen::Matrix2cd sample_matrix = Eigen::Matrix2cd::Identity();

  for (Layer* lay : layers)
  {
    if (lay->roughness->value > 0.0)
    {
      sample_matrix = electronic::grazing::RoughnessLayerMatrix(pre_layer->grazing_scattering_factor,
        lay->grazing_scattering_factor, kvector_z, lay->roughness->value * 1.0e-9) * sample_matrix;
    }

    if (thickness_buffer >= lay->thickness->value)
    {
      sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z) * sample_matrix;

      thickness_buffer -= lay->thickness->value;
    }
    else if (isinf(lay->thickness->value))
    {
      sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z) * sample_matrix;

      break;
    }
    else
    {
      sample_matrix = lay->ElectronicGrazingLayerMatrix(lay->grazing_scattering_factor, kvector_z, thickness_buffer)
        * sample_matrix;
      break;
    }

    pre_layer = lay;
  }

  return sample_matrix;
}

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixEffectiveRoughnessThickness(const double kvector_z, const double thickness)
{
  EffectiveLayerSystem(false);

  double thickness_buffer = thickness;

  Eigen::Matrix2cd sample_matrix = Eigen::Matrix2cd::Identity();

  for (EffectiveLayer& eff_layer : effective_layers)
  {
    if (thickness_buffer >= eff_layer.thickness)
    {
      sample_matrix = eff_layer.ElectronicGrazingLayerMatrix(eff_layer.grazing_scattering_factor, kvector_z) * sample_matrix;

      thickness_buffer -= eff_layer.thickness;
    }
    else if (isinf(eff_layer.thickness))
    {
      sample_matrix = eff_layer.ElectronicGrazingLayerMatrix(eff_layer.grazing_scattering_factor, kvector_z) * sample_matrix;

      break;
    }
    else
    {
      sample_matrix = eff_layer.ElectronicGrazingLayerMatrix(eff_layer.grazing_scattering_factor, kvector_z, thickness_buffer)
        * sample_matrix;

      break;
    }

  }

  return sample_matrix;
}

// electronic sample matrices

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrix(const double energy)
{
  const double kvector_z = conversions::EnergyToKvectorZ(energy, angle->value);

  Layer* pre_layer = sample_definitions::kAirLayer;

  // precompute scattering factor for effective density model
  pre_layer->grazing_scattering_factor = pre_layer->ElectronicGrazingScatteringFactor(energy, angle->value);

  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  for (Layer* lay : unique_layers)
    lay->grazing_scattering_factor = lay->ElectronicGrazingScatteringFactor(energy, angle->value);

  Eigen::Matrix2cd sample_matrix;

  if (roughness.compare("a") == 0)
  {
    sample_matrix = ElectronicSampleMatrixAnalyticRoughness(kvector_z, pre_layer);
  }
  else if (roughness.compare("n") == 0)
  {
    sample_matrix = ElectronicSampleMatrixNoRoughness(kvector_z);
  }
  else if (roughness.compare("e") == 0)
  {
    sample_matrix = ElectronicSampleMatrixEffectiveRoughness(kvector_z);
  }

  return sample_matrix;
}

Eigen::Matrix2cd GrazingSample::ElectronicSampleMatrixThickness(const double energy, const double thickness)
{
  const double kvector_z = conversions::EnergyToKvectorZ(energy, angle->value);

  Layer* pre_layer = sample_definitions::kAirLayer;

  // precompute scattering factor for effective dens model
  pre_layer->grazing_scattering_factor = pre_layer->ElectronicGrazingScatteringFactor(energy, angle->value);

  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  for (Layer* lay : unique_layers)
    lay->grazing_scattering_factor = lay->ElectronicGrazingScatteringFactor(energy, angle->value);

  Eigen::Matrix2cd sample_matrix;

  if (roughness.compare("a") == 0)
  {
    sample_matrix = ElectronicSampleMatrixAnalyticRoughnessThickness(kvector_z, pre_layer, thickness);
  }
  else if (roughness.compare("n") == 0)
  {
    sample_matrix = ElectronicSampleMatrixNoRoughnessThickness(kvector_z, thickness);
  }
  else if (roughness.compare("e") == 0)
  {
    sample_matrix = ElectronicSampleMatrixEffectiveRoughnessThickness(kvector_z, thickness);
  }

  return sample_matrix;
}

// electronic amplitudes

Complex GrazingSample::ElectronicReflectivityAmplitude(const double energy)
{
  const Eigen::Matrix2cd sample_matrix = ElectronicSampleMatrix(energy);

  const Complex reflectivity = -sample_matrix(1, 0) / sample_matrix(1, 1);

  return reflectivity;
}

Complex GrazingSample::ElectronicTransmissionAmplitude(const double energy)
{
  const Eigen::Matrix2cd sample_matrix = ElectronicSampleMatrix(energy);

  const Complex transmission = sample_matrix(0, 0)
    - sample_matrix(0, 1) * sample_matrix(1, 0) / sample_matrix(1, 1);

  return transmission;
}

Complex GrazingSample::ElectronicAmplitude(const double energy)
{
  Complex amplitude = std::numeric_limits<double>::quiet_NaN();

  if (geometry == "t")
  {
    amplitude = ElectronicTransmissionAmplitude(energy);
  }
  else if (geometry == "r")
  {
    amplitude = ElectronicReflectivityAmplitude(energy);
  }

  return amplitude;
}

Eigen::Matrix2cd GrazingSample::ElectronicAmplitudeMatrix(const double energy)
{
  Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Zero();

  if (geometry == "t")
  {
    matrix(0, 0) = ElectronicTransmissionAmplitude(energy);
    matrix(1, 1) = matrix(0, 0);
  }
  else if (geometry == "r")
  {
    matrix(0, 0) = ElectronicReflectivityAmplitude(energy);
    matrix(1, 1) = matrix(0, 0);
  }

  return matrix;
}

Complex GrazingSample::ElectronicAmplitudeThickness(const double energy, const double thickness)
{
  const Eigen::Matrix2cd sample_matrix = ElectronicSampleMatrix(energy);

  const Eigen::Matrix2cd sample_matrix_t = ElectronicSampleMatrixThickness(energy, thickness);

  const Complex field = sample_matrix_t(0, 0) + sample_matrix_t(1, 0)
    - (sample_matrix_t(0, 1) + sample_matrix_t(1, 1)) * sample_matrix(1, 0) / sample_matrix(1, 1);

  return field;
}

// implement amplitude again to avoid the calculation of the sample matrix over and over again
// by calling ElectronicAmplitudeThickness
std::vector<Complex> GrazingSample::ElectronicFieldAmplitude(const double energy, const size_t num_points)
{
  const double thickness_step = TotalThickness() / (static_cast<double>(num_points) - 1.0);

  const Eigen::Matrix2cd sample_matrix = ElectronicSampleMatrix(energy);

  std::vector<Complex> field_values(num_points);

  for (size_t i = 0; i < num_points; ++i)
  {
    const Eigen::Matrix2cd sample_matrix_t = ElectronicSampleMatrixThickness(energy, thickness_step * i);

    const Complex field = sample_matrix_t(0, 0) + sample_matrix_t(1, 0)
      - (sample_matrix_t(0, 1) + sample_matrix_t(1, 1)) * sample_matrix(1, 0) / sample_matrix(1, 1);

    field_values[i] = field;
  }

  return field_values;
}

// intensities

double GrazingSample::ElectronicIntensity(const double energy)
{
  return std::norm(ElectronicAmplitude(energy));
}

double GrazingSample::ElectronicReflectivity(const double energy)
{
  return std::norm(ElectronicReflectivityAmplitude(energy));
}

double GrazingSample::ElectronicTransmission(const double energy)
{
  return std::norm(ElectronicTransmissionAmplitude(energy));
}

double GrazingSample::ElectronicIntensityThickness(const double energy, const double thickness)
{
  return std::norm(ElectronicAmplitudeThickness(energy, thickness));
}

std::vector<double> GrazingSample::ElectronicFieldIntensity(const double energy, const size_t num_points)
{
  return intensity::Intensity(ElectronicFieldAmplitude(energy, num_points));
}

/* nuclear */

void GrazingSample::ScatteringMatrices(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid, const bool calc_transitions)
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  std::for_each(unique_layers.begin(), unique_layers.end(), [&](Layer* lay)
    {
      lay->grazing_scattering_matrix = lay->GrazingScatteringMatrix(isotope, detuning_grid, angle->value, calc_transitions);
    }
  );

}

// assumes that nuclear scattering matrices of all layer are already calculated
void GrazingSample::PropagationMatrices(const MoessbauerIsotope* const isotope)
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  std::for_each(unique_layers.begin(), unique_layers.end(), [&](Layer* lay)
    {
      lay->grazing_propagation_matrix = lay->GrazingPropagationMatrix(isotope, lay->grazing_scattering_matrix, angle->value);
    }
  );

}

// assumes that the nuclear scattering matrices of all layer are already calculated
void GrazingSample::LayerMatrices(const MoessbauerIsotope* const isotope)
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  std::for_each(unique_layers.begin(), unique_layers.end(), [&](Layer* lay)
    {
      lay->grazing_layer_matrix = lay->GrazingLayerMatrix(isotope, lay->grazing_scattering_matrix, angle->value);
    }
  );

}

// nuclear roughness models
LayerMatrix4 GrazingSample::SampleMatrixNoRoughness(const MoessbauerIsotope* const isotope, const size_t size)
{
  PropagationMatrices(isotope);

  LayerMatrices(isotope);

  ObjectMatrix4 sample_matrix(size, Eigen::Matrix4cd::Identity());

  for (const Layer* const lay : layers)
  {
    sample_matrix = matrix::Multiply(lay->grazing_layer_matrix, sample_matrix);

    if (isinf(lay->thickness->value) == true)
      break;
  }

  return sample_matrix;
}

LayerMatrix4 GrazingSample::SampleMatrixAnalyticRoughness(const MoessbauerIsotope* const isotope, const size_t size, Layer* pre_layer)
{
  pre_layer->grazing_propagation_matrix = pre_layer->GrazingPropagationMatrix(isotope, pre_layer->grazing_scattering_matrix, angle->value);

  PropagationMatrices(isotope);

  LayerMatrices(isotope);

  ObjectMatrix4 sample_matrix(size, Eigen::Matrix4cd::Identity());

  for (Layer* const lay : layers)
  {
    if (lay->roughness->value >= 0.0)
    {
      const LayerMatrix4 roughness_matrix = nuclear::grazing::RoughnessLayerMatrix(lay->grazing_propagation_matrix,
        pre_layer->grazing_propagation_matrix, lay->roughness->value * 1.0e-9);

      sample_matrix = matrix::Multiply(roughness_matrix, sample_matrix);
    }

    sample_matrix = matrix::Multiply(lay->grazing_layer_matrix, sample_matrix);

    if (isinf(lay->thickness->value))
      break;

    pre_layer = lay;
  }

  return sample_matrix;
}


LayerMatrix4 GrazingSample::SampleMatrixEffectiveRoughness(const MoessbauerIsotope* const isotope, const size_t size)
{
  EffectiveLayerSystem(true);

  std::for_each(NEXUS_EXECUTION_POLICY effective_layers.begin(), effective_layers.end(), [&](EffectiveLayer& eff_layer)
    {
      eff_layer.grazing_propagation_matrix = eff_layer.GrazingPropagationMatrix(isotope, eff_layer.grazing_scattering_matrix, angle->value);
    }
  );

  std::for_each(NEXUS_EXECUTION_POLICY effective_layers.begin(), effective_layers.end(), [&](EffectiveLayer& eff_layer)
    {
      eff_layer.grazing_layer_matrix = eff_layer.GrazingLayerMatrix(isotope, eff_layer.grazing_scattering_matrix, angle->value);
    }
  );

  ObjectMatrix4 sample_matrix(size, Eigen::Matrix4cd::Identity());

  for (EffectiveLayer& eff_layer : effective_layers)
  {
    sample_matrix = matrix::Multiply(eff_layer.grazing_layer_matrix, sample_matrix);

    if (isinf(eff_layer.thickness))
      break;
  }

  return sample_matrix;
}

LayerMatrix4 GrazingSample::SampleMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const bool calc_transitions)
{
  Layer* pre_layer = sample_definitions::kAirLayer;

  // precompute scattering matrices
  pre_layer->grazing_scattering_matrix = pre_layer->GrazingScatteringMatrix(isotope, detuning_grid, angle->value, calc_transitions);

  ScatteringMatrices(isotope, detuning_grid, calc_transitions);

  ObjectMatrix4 sample_matrix{};

  if (roughness.compare("a") == 0)
  {
    sample_matrix = SampleMatrixAnalyticRoughness(isotope, detuning_grid.size(), pre_layer);
  }
  else if (roughness.compare("n") == 0)
  {
    sample_matrix = SampleMatrixNoRoughness(isotope, detuning_grid.size());
  }
  else if (roughness.compare("e") == 0)
  {
    sample_matrix = SampleMatrixEffectiveRoughness(isotope, detuning_grid.size());
  }

  return sample_matrix;
}

ObjectMatrix2 GrazingSample::ReflectivityMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const bool calc_transitions)
{
  const ObjectMatrix4 sample_matrix = SampleMatrix(isotope, detuning_grid, calc_transitions);

  ObjectMatrix2 reflectivity_matrix(sample_matrix.size());

  std::transform(NEXUS_EXECUTION_POLICY sample_matrix.begin(), sample_matrix.end(), reflectivity_matrix.begin(),
    [](const Eigen::Matrix4cd& matrix)
    {
      const Eigen::Matrix2cd reflectivity = -1.0 * matrix.bottomRightCorner<2, 2>().completeOrthogonalDecomposition().solve(matrix.bottomLeftCorner<2, 2>());

      return reflectivity;
    }
  );

  return reflectivity_matrix;
}

ObjectMatrix2 GrazingSample::TransmissionMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const bool calc_transitions)
{
  const ObjectMatrix4 sample_matrix = SampleMatrix(isotope, detuning_grid,  calc_transitions);

  ObjectMatrix2 transmission_matrix(sample_matrix.size());

  std::transform(NEXUS_EXECUTION_POLICY sample_matrix.begin(), sample_matrix.end(), transmission_matrix.begin(), [&](const Eigen::Matrix4cd& matrix)
    {
      const Eigen::Matrix2cd reflectivity = -1.0 * matrix.bottomRightCorner<2, 2>().completeOrthogonalDecomposition().solve(matrix.bottomLeftCorner<2, 2>());

      return matrix.topLeftCorner<2, 2>() + matrix.topRightCorner<2, 2>() * reflectivity;
    }
  );

  return transmission_matrix;
}


ObjectMatrix2 GrazingSample::ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const bool calc_transitions)
{
  ObjectMatrix2 result{};

  if (geometry == "r")
  {
    result = ReflectivityMatrix(isotope, detuning_grid, calc_transitions);
  }
  else if (geometry == "t")
  {
    result = TransmissionMatrix(isotope, detuning_grid, calc_transitions);
  }

  return result;
}

// validity check for W matrix

std::vector<double> GrazingSample::KzSigma(const double energy, const double angle_kz) const
{
  std::vector<double> layer_vector(layers.size());

  for (const Layer* const lay : layers)
  {
    const double kzSigma = lay->KzSigma(energy, angle_kz);

    layer_vector.push_back(kzSigma);
  }

  return layer_vector;
}

/* effective layer system */

double GrazingSample::TotalThicknessEffective() const
{
  double total_thickness = 0.0;

  for (const EffectiveLayer& ext_lay : extended_layers)
  {
    if (isinf(ext_lay.thickness))
      break;

    total_thickness += ext_lay.thickness;
  }

  return total_thickness;
}

// use negative z axis to use formulas from paper directly
// 6 sigma is good width, so offset should be at least 3.0, be conservative set to 5.0
void GrazingSample::EffectiveLayerSystem(const bool nuclear)
{
  extended_layers = effdensmodel::ExtendedLayerSystem(layers, TotalThickness(), effective_thickness, nuclear, offset_top, offset_bottom);

  effective_coordinates = effdensmodel::EffectiveLayerCoordinates(TotalThicknessEffective(), effective_thickness);

  effdensmodel::CalculateLayerWeights(extended_layers, effective_coordinates);

  // if last layer is vacuum no inf substrate was present, so remove
  if (extended_layers.back().grazing_scattering_factor == Complex(0.0, 0.0))
    extended_layers.pop_back();

  for (double& coordinate : effective_coordinates)  // create positive coordinate values
    coordinate = std::abs(coordinate);

  // create new layer corresponding to each effective_coordinates
  effective_layers = effdensmodel::EffectiveLayers(extended_layers, effective_thickness, effective_coordinates.size());
}

/* divergences distribution functions */

void GrazingSample::PopulateDivergence(const size_t distribution_points)
{
  divergence_distribution.resize(1);

  divergence_distribution_weight.resize(1);

  const double sigma = divergence->value / constants::kSigmaToFHWM;

  const double range = 6.0 * sigma;  // good range for Gaussian distribution

  const double border = angle->value - range / 2.0;

  if (distribution_points > 1 &&
    range > 0.0)
  {
    divergence_distribution.resize(distribution_points);

    divergence_distribution_weight.resize(distribution_points);

    const double step = range / (distribution_points - 1.0);

    for (size_t i = 0; i < distribution_points; ++i)
    {
      const double angle_tmp = border + step * i;

      divergence_distribution[i] = angle_tmp;

      if (angle_tmp > 0)
      {
        divergence_distribution_weight[i] = exp(-0.5 * pow((angle_tmp - angle->value) / sigma, 2.0));
      }
      else
      {
        divergence_distribution_weight[i] = 0.0;
      }

    }

    const double norm = std::accumulate(divergence_distribution_weight.begin(), divergence_distribution_weight.end(), 0.0);

    std::for_each(divergence_distribution_weight.begin(), divergence_distribution_weight.end(), [norm](double& dist)
      {
        dist /= norm;
      }
    );
  }
  else
  {
    divergence_distribution[0] = angle->value;

    divergence_distribution_weight[0] = 1.0;
  }
}

void GrazingSample::ResetDivergenceIndex()
{
  divergence_index = 0;
}

size_t GrazingSample::GetDivergenceDistributionSize()
{
  return divergence_distribution.size();
}
