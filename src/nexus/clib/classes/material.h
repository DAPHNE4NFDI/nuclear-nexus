// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Material class.
// A physical material consists of an element composition.
// It also holds all hyperfine interactions acting on the Moesssbauer isotope.
//
// The units of the inputs are:
// Composition - input elements Element name and amount (at% (mole fraction) or chemical composition)
// density (g/cm^3)
// abundance (dimensionless 0 to 1)
// total_number_density (1/m^3)
// average_mole_mass (g/mole)
// lamb_moessbauer (dimensionless 0 to 1)
// isotope_number_density (1/m^3)


#ifndef NEXUS_MATERIAL_H_
#define NEXUS_MATERIAL_H_

#include <string>
#include <vector>
#include <utility>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../math/transitions.h"
#include "../classes/nxvariable.h"
#include "../classes/optimizer_handler.h"
#include "../classes/moessbauer.h"
#include "../classes/element.h"
#include "../classes/hyperfine.h"


// pair can be assigned directly with a list or tuple in Python, so keep vector of pairs.
typedef std::pair<std::string, double> CompositionEntry;

typedef std::vector<std::pair<std::string, double>> Composition;


/**
Constructor for :class:`Material` class.

Args:
   composition (list): Composition of the material in the format *[["element symbol" (string), relative atomic fraction (float)] , ... ]*.
   density (float or :class:`Var`): Density (g/cm :sup:`3`).
   isotope (:class:`MoessbauerIsotope`): Moessbauer isotope.
   abundance (float or :class:`Var`): Isotope abundance (0 to 1).
   lamb_moessbauer (float or :class:`Var` or None): Lamb Moessbauer factor (0 to 1).
	   
     .. versionchanged:: 1.2.0
        Set to ``None`` in case you want to use hyperfine site specific Lamb-Moessbauer factors in this material.

   hyperfine_sites (list): List of :class:`Hyperfine` sites assigned to the material.
   id (string): User identifier.

Attributes:
   composition (list): Composition of the material in the format *[["element symbol" (string), relative atomic fraction (float)] , ... ]*.
   density (:class:`Var`): Density (g/cm :sup:`3`).
   isotope (:class:`MoessbauerIsotope`): Moessbauer isotope.
   abundance (:class:`Var`): Isotope abundance (0 to 1).
   lamb_moessbauer (:class:`Var` or None): Lamb Moessbauer factor (0 to 1).

     .. versionchanged:: 1.2.0
        Set to ``None`` in case you want to use hyperfine site specific Lamb-Moessbauer factors in this material

   id (string): User identifier.
   total_number_density (float): Total number density (m :sup:`-3`).
   average_mole_mass (float): Average mole mass (g/mole).
   isotope_number_density (float): Isotope number density (m :sup:`-3`).
   hyperfine_sites (list): List of :class:`Hyperfine` sites assigned to the material.
   hyperfine_transitions (list): List of all :class:`Transition` object in the material.
   elements (list): List with all :class:`Element` objects in the material.
*/
class Material {
public:
	Material(
		const Composition composition,
		Var* const density,
		MoessbauerIsotope* const isotope,
		Var* const abundance,
		Var* const lamb_moessbauer,
		const std::vector<Hyperfine*> hyperfine_sites,
		const std::string id = ""
	);

	void Update();

	void UpdateComposition();

	void UpdateHyperfineSites();

	/**
  Returns the relative and normalized weights of the sites present in the material.
  
  Returns:
     list: List of relative weights.
	*/
	std::vector<double> GetRelativeWeights();

	void PopulateFitHandler(OptimizerHandler* fit_handler);

	/**
  Calculates a list with all weights, transition energies and polarization weight matrices in the material.
  Properly weighted over all distributions and hyperfine sites.
  
  Returns:
     list: List of transitions.
	*/
	void CalcTransitions();

	// variables

	Composition composition{};

	Var* density = nullptr;

	MoessbauerIsotope* isotope = nullptr;

	Var* abundance = nullptr;

	Var* lamb_moessbauer = nullptr;

	std::vector<Hyperfine*> hyperfine_sites{};

	std::string id = "";

	std::vector<Element> elements{}; // list of all elements and additional infos, e.g. their composition amount in at percent ()

	double average_mole_mass = 0.0;

	double total_number_density = 0.0;

	double isotope_number_density = 0.0;

	// stores all nuclear transitions in the material
	transitions::Transitions hyperfine_transitions{};
};

#endif // NEXUS_MATERIAL_H_
