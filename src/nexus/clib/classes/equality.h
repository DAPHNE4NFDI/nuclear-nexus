// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of parameter distributions acting on hyperfine parameters.
// Used in Hyperfine objects.
// 
// This is a class with a virtual function Distribution() that is defined in Python (by the user).
// vector delta - delta values of the base parameter, e.g. magnetic hyperfine field: -deltax to deltax
// vector weight - weight values of the delta values, is automatically normalized to one in setygrid: 0-1
// The values here only give the changes around the base parameter, e.g. the magnetic field.
// The actual scaling is done in Hyperfine class, e.g. Bhf-deltax to Bhf+deltax
// The deltax values can be specified with SetDeltaX()


#ifndef NEXUS_EQUALITY_H_
#define NEXUS_EQUALITY_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"
#include "../utilities/errors.h"


// abstract class for equalities
/**
Abstract class for an :class:`Equality` constraint or dependency of :class:`Var` objects.
Do not use this class directly.
An Equality must be derived from this class in Python the following way.

.. code-block::

    # definition of the derived class
    class EqualityDefinedInPython(nx.Equality):
        def __init__(self, id="user isd"):
            super().__init__(id)

        # implementation of the actual equality function
        # must return a float value
        def Function(self):
            # here goes your implementation
            return ...

Args:
   id (string): User identifier.

Attributes:
   id (string): User identifier.
*/
class Equality {
public:
  Equality(const std::string id = "") :
    id(id)
  {};

  virtual ~Equality() {};

  /**
  Call of the Equality function implementation from python.
  */
  virtual double Function() = 0;

  std::string id = "";
};


#endif // NEXUS_DISTRIBUTION_H_
