// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of a chemical element.
// relative_amout (at%), same as mole fraction
// number_density (1/m^3)


#ifndef NEXUS_ELEMENT_H_
#define NEXUS_ELEMENT_H_

#include <string>

#include "../nexus_definitions.h"
#include "../classes/element_maps.h"


/**
Constructor for the :class:`Element`.

Args:
   element (string): Element symbol, e.g. "Fe" for iron.
   number_density (float): Number density (1/cm^3).
   relative_amount (float): Relative amount in the material.

Attributes:
   atomic_number (int): Atomic or proton number.
   element (string): Element symbol.
   number_density (float): Number density (1/cm^3).
   relative_amount (float): Relative amount in the material.
*/
struct Element {
	Element(
	  const std::string element = "", 
	  const double number_density = 0.0,
	  const double relative_amount = 0.0
	) :
		element(element),
		number_density(number_density),
		relative_amount(relative_amount)
	{
		atomic_number = element_maps::atomic_number_map.find(element)->second;
	};

	std::string element = "";

	int atomic_number = 0;
	
	double number_density = 0.0;
	
	double relative_amount = 0.0;
};

#endif // NEXUS_ELEMENT_H_
