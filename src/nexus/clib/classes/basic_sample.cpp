// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "basic_sample.h"

#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include <Eigen/QR>

#include "../nexus_definitions.h"
#include "../utilities/errors.h"
#include "../math/conversions.h"
#include "../math/intensity.h"
#include "../classes/moessbauer.h"
#include "../classes/material.h"
#include "../scattering/electronic_scattering.h"
#include "../math/matrix.h"



BasicSample::BasicSample(
    const std::vector<Layer*> layers_,
    const std::vector<double> drive_detuning_,
    FunctionTime* const function_time_,
    const NxObjectId object_id_,
    const std::string id_
  ) :
  NxObject(id_, object_id_),
  layers(layers_),
  drive_detuning(drive_detuning_),
  function_time(function_time_)
{
  if (drive_detuning.size() > 0 && function_time != nullptr)
    errors::ErrorMessage("Sample", "Sample cannot have drive_detuning and function_time.");
}

std::vector<Layer*> BasicSample::FindUniqueLayers() const
{
  std::vector<Layer*> unique_layer_pointers{};

  for (Layer* const lay : layers)
  {
    if (std::find(unique_layer_pointers.begin(), unique_layer_pointers.end(), lay) == unique_layer_pointers.end()) // if not found
      unique_layer_pointers.push_back(lay);
  }

  return unique_layer_pointers;
}

double BasicSample::TotalThickness() const
{
  double total_thickness = 0.0;

  for (const Layer* const lay : layers)
  {
    if (isinf(lay->thickness->value))
      break;

    total_thickness += lay->thickness->value;
  }

  return total_thickness;
}

std::vector<double> BasicSample::LayerCenters() const
{
  std::vector<double> layer_center_list{};

  double center_thickness = 0.0;

  double added_thickness = 0.0;

  for (const Layer* const lay : layers)
  {
    if (isinf(lay->thickness->value) == true)
    {
      center_thickness = added_thickness;
    }
    else
    {
      center_thickness = 0.5 * lay->thickness->value + added_thickness;
    }

    layer_center_list.push_back(center_thickness);

    added_thickness += lay->thickness->value;
  }

  return layer_center_list;
}

std::vector<double> BasicSample::Interfaces() const
{
  std::vector<double> interface_list{ 0 };

  double added_thickness = 0.0;

  for (const Layer* const lay : layers)
  {
    added_thickness += lay->thickness->value;

    interface_list.push_back(added_thickness);
  }

  return interface_list;
}

std::vector<std::string> BasicSample::Ids() const
{
  std::vector<std::string> interface_list{};

  for (const Layer* const lay : layers)
    interface_list.push_back(lay->id);

  return interface_list;
}
