// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "conuss_object.h"

#include <iostream>
#include <cmath>
#include <complex>
#include <numeric>
#include <algorithm>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif

#include "../nexus_definitions.h"
#include "../math/constants.h"
#include "../math/interpolate.h"


using namespace std::complex_literals;


ConussObject::ConussObject(
  const std::string file_name_,
  const std::vector<Complex> scattering_factors_,
  Complex* data_, int num_points_distribution_, int num_points_detuning_, int nx_, int ny_,
  const std::vector<double> angles_,
  const std::vector<double> detuning_,
  Var* const angle_,
  Var* const divergence_,
  const std::string id_
) :
  NxObject(id_, NxObjectId::ConussObject),
  file_name(file_name_),
  scattering_factors(scattering_factors_),
  data(data_),
  num_points_distribution(num_points_distribution_),
  num_points_detuning(num_points_detuning_),
  nx(nx_),
  ny(ny_),
  angles(angles_),
  detuning(detuning_),
  angle(angle_),
  divergence(divergence_)
{
  object_matrices = ConussDataToLayerMatrices(data, num_points_distribution, num_points_detuning);

  size_t i = static_cast<size_t>(floor(num_points_distribution / 2));
 
  scattering_factor = scattering_factors.at(i);
    
  object_matrix = object_matrices.at(i);

  divergence_distribution_weight.resize(angles.size());
}


void ConussObject::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(angle);

  fit_handler->PrepareFitVariable(divergence);
}


void ConussObject::SetPropertiesAtIndex(size_t index)
{
  scattering_factor = scattering_factors.at(index);

  object_matrix = object_matrices.at(index);
}


Complex ConussObject::ElectronicAmplitude(const double energy)
{
  return scattering_factor;
}


Eigen::Matrix2cd ConussObject::ElectronicAmplitudeMatrix(const double energy)
{
  return scattering_factor * Eigen::Matrix2cd::Identity();
}


Complex ConussObject::ScatteringFactorAtIndex(size_t index) const
{
  return scattering_factors.at(index);
}


// returns the Object Matrix of the Conuss Object priorly set to object_matrix via ObjectMatrixAtIndex interpolated at on the new detuning grid
ObjectMatrix2 ConussObject::ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& new_detuning,
  const bool calc_transitions)
{
  ObjectMatrix2 object_matrix_interpolated(new_detuning.size());

  std::transform(NEXUS_EXECUTION_POLICY new_detuning.begin(), new_detuning.end(), object_matrix_interpolated.begin(),
    [&](const double det)
    {
      return interpolate::InterpolateLinear<Eigen::Matrix2cd>(detuning, object_matrix, det, false);
    });

  return object_matrix_interpolated;
}


ObjectMatrix2 ConussObject::ObjectMatrixAtIndex(size_t index) const
{
  return object_matrices.at(index);
}


ObjectMatrix2 ConussObject::ObjectMatrixAtIndexDetuning(size_t index, const std::vector<double>& new_detuning) const
{
  ObjectMatrix2 object_matrix_interpolated(new_detuning.size());

  ObjectMatrix2 object_mat = object_matrices.at(index);

  std::transform(NEXUS_EXECUTION_POLICY new_detuning.begin(), new_detuning.end(), object_matrix_interpolated.begin(),
    [&](const double det)
    {
      return interpolate::InterpolateLinear<Eigen::Matrix2cd>(detuning, object_mat, det, false);
    });

  return object_matrix_interpolated;
}


std::vector<ObjectMatrix2> ConussObject::ConussDataToLayerMatrices(const Complex* data, const int n_distribution, const int n_detuning) const
{
  std::vector<ObjectMatrix2> matrices(static_cast<size_t>(n_distribution));

  ObjectMatrix2 matrix(static_cast<size_t>(n_detuning));

  Eigen::Matrix2cd mat;

  for (int j = 0; j < n_distribution; ++j)
  {
    // reorder CONUSS matrix to Nexus matrix
    for (int i = 0; i < n_detuning; ++i)
    {
      mat(1, 1) = *data++;
      mat(1, 0) = *data++;
      mat(0, 1) = *data++;
      mat(0, 0) = *data++;

      matrix.at(i) = mat;
    }

    matrices.at(j) = matrix;
  }

  return matrices;
}


// Angle FWHM distribution functions
void ConussObject::PopulateDivergence()
{
  const size_t distribution_points = angles.size();

  const double sigma = divergence->value / constants::kSigmaToFHWM;

  const double range = 6.0 * sigma;  // good range for Gaussian distribution

  //const double border = angle->value - range / 2.0;

  if (distribution_points > 1 &&
      range > 0.0)
  {
    const double step = range / (distribution_points - 1.0);

    for (size_t i = 0; i < distribution_points; ++i)
      divergence_distribution_weight.at(i) = exp(-0.5 * pow((angles.at(i) - angle->value) / sigma, 2.0));

    const double norm = std::accumulate(divergence_distribution_weight.begin(), divergence_distribution_weight.end(), 0.0);

    std::for_each(divergence_distribution_weight.begin(), divergence_distribution_weight.end(), [norm](double& dist)
      {
        dist /= norm;
      }
    );
  }
  else
  {
    divergence_distribution_weight.at(0) = 1.0;
  }
}


void ConussObject::ResetDivergence()
{
  divergence_index = 0;
}


size_t ConussObject::GetDivergenceDistributionSize()
{
  return angles.size();
}
