// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module grazing_sample

%{
    #define SWIG_FILE_WITH_INIT
    #include "sample.h"
%}


%feature("shadow") GrazingSample::GrazingSample %{
  def __init__(self, layers, geometry = "r", angle = 0.0, length = 10, roughness = "a", effective_thickness = 0.3, drive_detuning = [], function_time = None, divergence = 0.0, id = ""):
    
    if isinstance(layers, (Layer)):
      layers = [layers]

    if isinstance(angle, (int, float)):
      angle = Var(angle, 0, np.inf, False, "")

    if isinstance(divergence, (int, float)):
      divergence = Var(divergence, 0, np.inf, False, "")

    self._angle = angle

    self._divergence = divergence

    # the python class object must reference to the python list with objects pointers
    # in order to maintain securely in the python domain 
    self._layers = layers

    self._function_time = function_time
    
    _cnexus.GrazingSample_swiginit(self, _cnexus.new_GrazingSample(layers, geometry, angle, length, roughness, effective_thickness, drive_detuning, function_time, divergence, id))
%}



%include "sample.h"



%extend GrazingSample{
  %pythoncode{

  def __setattr__(self, attr, val):

    if attr == "angle" and isinstance(val, (int, float)):
      setattr(getattr(self, attr), "value", val)
      return

    if attr == "divergence" and isinstance(val, (int, float)):
      setattr(getattr(self, attr), "value", val)
      return

    if attr == "angle":
      self._angle = val

    if attr == "divergence":
      self._divergence = val

    if attr == "layers":
      if isinstance(val, (list, tuple)):
        self._layers = val  # ref to python list
        val = Layers(val)

    if attr == "drive_detuning":
      if isinstance(val, (list, tuple, np.ndarray)):
        val = DoubleVector(val)

    if attr == "roughness" and val == "e":
      self.EffectiveLayerSystem(self.effective_thickness, False)

    if attr == "function_time":
      self._function_time = val

    super().__setattr__(attr, val)

  def __repr__(self):
    self.Update()
    output = "GrazingSample\n"
    output += "  .id: {}\n".format(self.id)
    output += "  .geometry: {}\n".format(self.geometry)
    output += "  .angle (deg) = {}\n".format(self.angle.value)
    output += "  .divergence (deg) = {}\n".format(self.divergence.value)
    output += "  .length (mm) = {}\n".format(self.length)
    output += "  .roughness (model): {}\n".format(self.roughness)

    output += "-------|------------------------|---------------|-------------|-------------|--------|-----------|----------|-------------|\n"
    output += "{:>8}".format("index |")
    output += "{:>25}".format("Layer id |")
    output += "{:>16}".format("dens. (g/cm3) |")
    output += "{:>14}".format("thick. (nm) |")
    output += "{:>14}".format("rough. (nm) |")
    output += "{:>9}".format("abund. |")
    output += "{:>12}".format("LM factor |")
    output += "{:>11}".format("HI sites |")
    output += "{:>15}".format("dist points |\n")
    output += "-------|------------------------|---------------|-------------|-------------|--------|-----------|----------|-------------|\n"

    i = 0
    for lay in self.layers:
      output += "{:>6} |".format(i)
      output += "{:>23} |".format(lay.id)
      output += "{:>14} |".format(lay.material.density.value)
      output += "{:>12} |".format(lay.thickness.value)
      output += "{:>12} |".format(lay.roughness.value)
      output += "{:>7} |".format(lay.material.abundance.value)
      output += "{:>10} |".format(lay.material.lamb_moessbauer.value)
      if len(lay.material.hyperfine_sites) != 0:
        num_distributionpoints = 0
        for site in lay.material.hyperfine_sites:
          num_distributionpoints += len(site.BareHyperfines)
        output += "{:>9} |".format(len(lay.material.hyperfine_sites))
        output += "{:>12} |".format(num_distributionpoints)
      else:
        output += "{:>9} |".format("")
        output += "{:>12} |".format("")
      output += "\n"
      i +=1

    output += "-------|------------------------|---------------|-------------|-------------|--------|-----------|----------|-------------|\n"
    return output

  }
}


%pythoncode %{

# this is for documentation of Sample class in verison 1.X.X only

class Sample_v1:
  r"""
  Constructor for :class:`Sample` in versions 1.X.X.

  .. versionremoved:: 2.0.0

     Use the :class:`ForwardSample` or :class:`GrazingSample` instead.
     A sample constructor ``Sample`` still exists that generates either of the two from.

  Args:
     layers (list): List of Layer in order of beam propagation.
     geometry (string): Scattering geometry of the sample.

        * *f* - forward scattering
        * *r* - reflection in grazing incidence scattering
        * *t* - transmission in grazing incidence scattering

     angle (float or :class:`Var`): Angle of incidence for grazing geometry (degree).
     length (float): length of the sample along beam direction. Only used for grazing geometry.
     roughness (string): Roughness model in grazing geometry.

        * *n* - no roughness
        * *a* - analytical model
        * *e* - effective density model

     effective_thickness (float): Layer thickness in the effective density model (nm).
     drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
     function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
       Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
     divergence (float or :class:`Var`): Divergence of the incidence angle given as the FWHM of the divergence (deg).
     id (string): User identifier.

  Attributes:
     layers (list): List of Layer in order of beam propagation.
     geometry (string): Scattering geometry of the sample.
      
        * *f* - forward scattering
        * *r* - reflection in grazing incidence scattering
        * *t* - transmission in grazing incidence scattering
   
     angle (float or :class:`Var`): Angle of incidence for grazing geometry (degree).
     length (float): length of the sample along beam direction. Only used for grazing geometry.
     roughness (string): Roughness model in grazing geometry.
      
        * *n* - no roughness
        * *a* - analytical model
        * *e* - effective density model
      
     effective_thickness (float): Layer thickness in the effective density model (nm).
     drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
     function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
       Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
     divergence (float or :class:`Var`): Divergence of the incidence angle given as the FWHM of the divergence (deg).
     id (string): User identifier.
     effective_coordinates (list): Layer coordinates in the effective density model.
     effective_layers (list): List of :class:`EffectiveLayer` objects.
  """

  def LayerPointer():
    r"""
    Returns a list of all layer pointers. Useful in order to check if layers have the same reference.

    Returns:
        list: List of layer pointers
    """
    pass
  
  def PrintLayerPointers():
    r"""
    Print the pointer of each Layer. Useful in order to check if layers have the same reference.

    Returns: 
        Prints the layer pointers to the output
    """
    pass

  def ClearLayers():
    r"""
    Clear the :attr:`layers` list.
    """
    pass

  def TotalThickness():
    r"""
    Calculates the total thickness of the sample.

    Returns:
       float: Total thickness of the sample (nm).
    """
    pass

  def ForwardEffectiveThickness():
    r"""
    Returns the effective thickness :math:`t_{eff} = \sigma \sum_i \rho_{i} f^{LM}_i t_i` of the sample in forward geometry, where
    :math:`\sigma` is the nuclear cross section,
    :math:`\rho_{i}` is the number density of the resonant nuclei in the i-th layer,
    :math:`f^{LM}_i` is the Lamb Moessbauer factor of the i-th layer,
    :math:`t_i` is the thickness of the layer i-th layer.
  
    Returns:
       double: Effective thickness.
    """
    pass

  def LayerCenters():
    r"""
    A list with the center coordinates of all layers in the sample. 

    Returns:
       list: Position of the Layer centers.
    """
    pass

  def Interfaces():
    r"""
    A list with the positions of the layer interfaces.

    Returns:
       list: Position of the Layer interfaces.
    """
    pass

  def Ids():
    r"""
    List with all layer id of the sample.

    Returns:
       list: Layers ids.
    """
    pass

  def ElectronicAmplitude(energy):
    r"""
    Calculates the relative electronic amplitude behind the sample.
    It is given by the product of the complex transmission or layer matrices of the sample layers.
    The geometry is determined from the :attr:`geometry`.

    Args:
       energy (float): X-ray energy (eV)

    Returns:
       complex: Electronic amplitude.
    """
    pass

  def ElectronicTransmission(energy):
    r"""
    Calculates the relative electronic transmission behind the sample.
    It is given by the product of the complex transmission or layer matrices of the sample layers.
    The geometry is determined from the :attr:`geometry`.

    Args:
       energy (float): X-ray energy (eV)

    Returns:
       float: Electronic transmission.
    """
    pass

  def ElectronicAmplitudeMatrix(energy):
    r"""
    Calculates the relative electronic amplitude matrix behind the sample. It is given by the product of the complex transmission or layer matrices of the sample layers.
    The geometry is determined from the :attr:`geometry`.

    Args:
       energy (float): X-ray energy (eV)

    Returns:
       complex: Electronic amplitude.
    """
    pass

  def ElectronicFieldAmplitude(energy, num_points = 101, angle = 0.0):
    r"""
    Calculates the electronic X-ray field amplitude in the sample.

    Args:
       energy (float): X-ray energy (eV).
       num_points (int): Number of points of the output.
       angle (float): Incidence angle in grazing incidence geometry.

    Returns:
       complex: Electronic field amplitude in the sample.
    """
    pass

  def ElectronicFieldIntensity(energy, num_points = 101, angle = 0.0):
    r"""
    Calculates the electronic X-ray field intensity in the sample.

    Args:
       energy (float): X-ray energy (eV).
       num_points (int): Number of points of the output.
  
    Returns:
       float: Electronic field intensity.
    """
    pass

  def ElectronicForwardAmplitude(energy):
    r"""
    Calculates the electronic amplitude behind the sample in forward geometry .

    Args:
       energy (float): X-ray energy (eV).

    Returns:
       complex: Forward amplitude.
    """
    pass

  def ElectronicForwardTransmission(energy):
    r"""
    Calculates the electronic transmission behind the sample in forward geometry .

    Args:
       energy (float): X-ray energy (eV).

    Returns:
       float: Forward transmission.
    """
    pass

  def ElectronicGrazingSampleMatrix(energy, angle, calc_roughness, eff_layer_thickness):
    r"""
    Calculates the electronic 2x2 sample matrix in grazing incidence geometry.  It is the product of all layer matrices.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence-angle of the sample (degree).
       calc_roughness (string): Assumed roughness model, either *n*, *a*, *e*.
       eff_layer_thickness (float): thickness of the layers in the effective layer model (*e*).

    Returns:
       ndarray: Complex 2x2 matrix.
    """
    pass
  
  def ElectronicGrazingSampleMatrix(energy, angle, thickness, calc_roughness, eff_layer_thickness):
    r"""
    Calculates the electronic 2x2 sample matrix up to a certain sample thickness in grazing incidence geometry. It is the product of all layer matrices up to this thickness.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence-angle of the sample (degree).
       thickness (float): Thickness at which the sample matrix should be calculated (nm).
       calc_roughness (string): Assumed roughness model, either *n*, *a*, *e*.
       eff_layer_thickness (float): thickness of the layers in the effective layer model (*e*).

    Returns:
       ndarray: Complex 2x2 matrix.
    """
    pass

  def ElectronicGrazingReflectivityAmplitude(energy, angle, calc_roughness, eff_layer_thickness):
    r"""
    Calculates the complex reflectivity amplitude of the sample in grazing incidence geometry.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence-angle of the sample (degree).
       calc_roughness (string): Assumed roughness model, either *n*, *a*, *e*.
       eff_layer_thickness (float): thickness of the layers in the effective layer model (*e*).

    Returns:
       complex: Complex reflectivity amplitude.
    """
    pass
  
  def ElectronicGrazingTransmissionAmplitude(energy, angle, calc_roughness, eff_layer_thickness):
    r"""
    Calculates the complex transmission amplitude of the sample in grazing incidence geometry.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence-angle of the sample (degree).
       calc_roughness (string): Assumed roughness model, either *n*, *a*, *e*.
       eff_layer_thickness (float): thickness of the layers in the effective layer model (*e*).

    Returns:
       complex: Complex reflectivity amplitude.
    """
    pass
  
  def ElectronicGrazingReflectivity(energy, angle, calc_roughness, eff_layer_thickness):
    r"""
    Calculates the reflectivity intensity of the sample in grazing incidence geometry.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence-angle of the sample (degree).
       calc_roughness (string): Assumed roughness model, either *n*, *a*, *e*.
       eff_layer_thickness (float): Thickness of the layers in the effective layer model (*e*).

    Returns:
       float: Reflectivity.
    """
    pass
  
  def ElectronicGrazingTransmission(energy, angle, calc_roughness, eff_layer_thickness):
    r"""
    Calculates the transmission intensity of the sample in grazing incidence geometry.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence-angle of the sample (degree).
       calc_roughness (string): Assumed roughness model, either *n*, *a*, *e*.
       eff_layer_thickness (float): Thickness of the layers in the effective layer model (*e*).

    Returns:
       float: Transmission.
    """
    pass

  def SampleMatrix(isotope, detuning, angle = 0.0, calc_roughness = "a", eff_layer_thickness = 0.1, calc_transitions = True):
    r"""
    Calculates the sample matrix - the product of all layer matrices in the given sample geometry.
    Same as the ObjectMatrix.
   
    Args:
       isotope (:class:`MoessbauerIsotope`): Moessbauer isotope.
       detuning (list or ndarray): Detuning values of the calculation.
       angle (float): Angle in grazing geometry (degree).
       calc_roughness (string): Calculation mode for roughness. 

            *n* - no roughness
            *a* - analytical model
            *e* - effective density model

       eff_layer_thickness (float): Layer thickness for the effective density model (nm).
       calc_transitions (bool): Specifies if the nuclear transitions should be updated or not. Set this value to *True*.

    Returns:
       list: List of complex 2x2 matrices.
    """
    pass
  
  def KzSigma(energy, angle_kz):
    r"""
    Calculates the product :math:`k_z \sigma` for each layer in the sample. The product should be << 1 for the validity of the W matrix calculation.

    Args:
       energy (float): X-ray energy (eV).
       angle (float): Incidence angle (deg).

    Returns:
       list: List of :math:`k_z \sigma` values for each layer.
    """
    pass

  def TotalThicknessEffective():
    r"""
    Total thickness of the sample in the effective density model.

    Returns:
       float: Total thickness in the effective density model.
    """
    pass

  def EffectiveLayerSystem(eff_layer_thickness, nuclear):
    r"""
    Create an effective density model from the sample. Electronic scattering factors and matrices are always calculated.

    Args:
       eff_layer_thickness (float): Layer thickness in the effective density model.
       nuclear (bool): Calculate the matrices for nuclear scattering in addition to the electronic ones.
    """
    pass


# for backward compatibility with old Sample class constructor

def Sample(layers, geometry = "f", angle = 0.0, length = 10, roughness = "a", effective_thickness = 0.3, drive_detuning = [], function_time = None, divergence = 0.0, id = ""):
    r"""
    .. versionadded:: 2.0.0

    A sample constructor that generates either a :class:`ForwardSample` or :class:`GrazingSample`.
    """
    if geometry == "f":
        return ForwardSample(layers, drive_detuning, function_time, id)

    elif geometry in ("r", "t"):
        return GrazingSample(layers, geometry, angle, length, roughness, effective_thickness, drive_detuning, function_time, divergence, id)

%}
