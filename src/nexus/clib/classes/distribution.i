// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module(directors="1") distribution

%{ 
    #define SWIG_FILE_WITH_INIT
    #include "distribution.h"
%}


%feature("shadow") Distribution::Distribution %{
    def __init__(self, points = 101, id = ""):
        if self.__class__ == Distribution:
            _self = None
        else:
            _self = self
        _cnexus.Distribution_swiginit(self, _cnexus.new_Distribution(_self, points, id))
%}


%feature("director") Distribution;



%include "distribution.h"



%extend Distribution{
  %pythoncode{

  def __setattr__(self, attr, val):
    if isinstance(val, (int, float)):
      try:
        super().__setattr__(attr, val)
        return
      except:
        setattr(getattr(self, attr), "value", val)
        return

    if isinstance(val, (list, tuple)):
      val = FitVariables(val)

    super().__setattr__(attr, val)

  }
}