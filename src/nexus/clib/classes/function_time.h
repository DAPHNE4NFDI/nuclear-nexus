// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the abstract FunctionTime class.
// It is used to return a value depending on a time. 
// The actual implementation is done in a derived class in python.
//
// header only


#ifndef NEXUS_FUNCTION_TIME_H_
#define NEXUS_FUNCTION_TIME_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"
#include "../utilities/errors.h"


/**
Abstract class for :class:`FunctionTime`.
Do not use this class directly.
A :class:`FunctionTime` implementation must be derived from this class in Python the following way.

.. versionadded:: 1.0.4

.. code-block::

    # definition of the derived class
    class FunctionTimeDefinedInPython(nx.FunctionTime):
        def __init__(self, id = "my function of time implementation", ...):
            super().__init__(id)
            ...

        # implementation of the actual function of time
        def FunctionTimeFunction(self, time):
            ...
            dependent_variable = f(time)
            return dependent_variable

Args:
   id (string): User identifier.
        
Attributes:
   id (string): User identifier.
   fit_variables (list): List of :class:`nx.Var` objects.
*/
class FunctionTime {
public:
  FunctionTime(
    const std::string id 
  ) : 
    id(id)
  {};

  virtual ~FunctionTime() {};

  /**
  Call the function implementation from python.
  
  Args:
     float: time in nanoseconds.

  Returns:
     float: Dependent variable. 
  */
  virtual double Function(const double& time) = 0;

  /*
  Compute a detuning grid needed for energy based calculations.
  */
  std::vector<double> CreateDetuning(const double max_detuning, int num_steps) const
  {
    if (num_steps == 0)
    {
      errors::WarningMessage("FunctionTime", "num_steps must be larger zero and even.");
      
      return std::vector<double>{};
    }

    if (num_steps % 2 != 0)
    {
      errors::WarningMessage("FunctionTime", "num_steps must be even, num_steps increased by one");

      num_steps += 1;
    }
    
    const double detuning_step = 2.0 * max_detuning / num_steps;

    std::vector<double> detuning_grid(num_steps);

    for (int i = 0; i < num_steps; ++i)
      detuning_grid[i] = (-num_steps / 2 + i) * detuning_step;

    return detuning_grid;
  }

  std::string id = "";

  std::vector<Var*> fit_variables{};
};


#endif // NEXUS_FUNCTION_TIME_H_
