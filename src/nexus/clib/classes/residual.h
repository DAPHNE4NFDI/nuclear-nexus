// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the abstract Residual class.
// It is used by the Fit module to calculate the residuals between data and simulation.
// The actual implementation is done in a derived class in python.


#ifndef NEXUS_RESIDUAL_H_
#define NEXUS_RESIDUAL_H_

#include <string>
#include <vector>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"


/**
Abstract class for :class:`Residual`.
Do not use this class directly.
A Residual implementation must be derived from this class in Python the following way.

.. code-block::

    # definition of the derived class
    class ResidualDefinedInPython(nx.Residual):
        def __init__(self, id = "my residual implementation"):
        # since version 2.0.0 it must be 
        # def __init__(self, id = "my residual implementation", exponent=X):
            super().__init__(id, exponent)

        # implementation of the actual residual function
        def ResidualFunction(self, input_data, input_theory):
            residual = ...
            return residual

            def __init__(self, id = "my residual implementation"):

.. versionchanged:: 2.0.0
   
   Exponent must be specified since version 2.0.0

Some residual weight functions are predefined in the library :mod:`residual`.

Args:
   id (string): User identifier.
   exponent (double): Exponent of the residual function. For least square problems it is 2.

     .. versionadded:: 2.0.0

   plot_string (string): String used for .plot() functions in residual plot.
        
     .. versionadded:: 1.0.3

Attributes:
   id (string): User identifier.
   exponent (double): Exponent of the residual function. For least square problems it is 2.

     .. versionadded:: 2.0.0

   plot_string (string): String used for .plot() functions in residual plot.

     .. versionadded:: 1.0.3
*/
class Residual {
public:
  Residual(
    const std::string id,
    const double exponent,
    const std::string plot_string = ""
  ) : 
    id(id),
    exponent(exponent),
    plot_string(plot_string)
  {};

  virtual ~Residual() {};

  /**
  Call of the residual function implementation from python.
  
  Returns:
     list or ndarray: List of residual values of measured and theoretical intensities. 
  */
  virtual std::vector<double> ResidualFunction(const std::vector<double>& intensity_data,
    const std::vector<double>& intensity_theory) = 0;

  std::string id = "";

  double exponent = 2.0;

  std::string plot_string = "";
};

#endif // NEXUS_RESIDUAL_H_
