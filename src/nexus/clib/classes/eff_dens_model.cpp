// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "eff_dens_model.h"

#include <cmath>
#include <algorithm>
#include <iterator>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include "../math/conversions.h"
#include "../math/constants.h"
#include "../math/matrix.h"
#include "../scattering/electronic_scattering.h"
#include "../scattering/scattering_matrix.h"
#include "../scattering/layer_matrix.h"


Eigen::Matrix2cd EffectiveLayer::ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z)
{
  Eigen::Matrix2cd electronic_layer_matrix;

  if (isinf(thickness) == true)
  {
    electronic_layer_matrix = electronic::grazing::LayerMatrixBottom(scattering_factor, kvector_z);
  }
  else
  {
    const Eigen::Matrix2cd electronic_propagation_matrix = electronic::grazing::PropagationMatrix(scattering_factor, kvector_z);

    electronic_layer_matrix = electronic::grazing::LayerMatrix(electronic_propagation_matrix, thickness * 1.0e-9);
  }

  return electronic_layer_matrix;
}


Eigen::Matrix2cd EffectiveLayer::ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z,
  const double input_thickness)
{
  Eigen::Matrix2cd electronic_layer_matrix;
  
  if (isinf(input_thickness) == true)
  {
    electronic_layer_matrix = electronic::grazing::LayerMatrixBottom(scattering_factor, kvector_z);
  }
  else
  {
    const Eigen::Matrix2cd electronic_propagation_matrix = electronic::grazing::PropagationMatrix(scattering_factor, kvector_z);

    electronic_layer_matrix = electronic::grazing::LayerMatrix(electronic_propagation_matrix, input_thickness * 1.0e-9);
  }

  return electronic_layer_matrix;
}


PropagationMatrix4 EffectiveLayer::GrazingPropagationMatrix(const MoessbauerIsotope* const isotope,
  const ScatteringMatrix2& grazing_f_matrix, const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  const PropagationMatrix4 grazing_F_matrix = nuclear::grazing::PropagationMatrix(grazing_f_matrix, kvector_z);

  return grazing_F_matrix;
}


LayerMatrix4 EffectiveLayer::GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
  const double angle)
{
  const double kvector_z = conversions::EnergyToKvectorZ(isotope->energy, angle);

  LayerMatrix4 grazing_L_matrix{};

  if (isinf(thickness))
  {
    grazing_L_matrix = nuclear::grazing::LayerMatrixBottom(grazing_f_matrix, kvector_z);
  }
  else
  {
    const PropagationMatrix4 grazing_F_matrix = nuclear::grazing::PropagationMatrix(grazing_f_matrix, kvector_z);

    grazing_L_matrix = nuclear::grazing::LayerMatrix(grazing_F_matrix, thickness * 1.0e-9);  // convert to meter
  }

  return grazing_L_matrix;
}


namespace effdensmodel {

  double Yfunction(const double z, double sigma_j)
  {
    if (sigma_j == 0.0)
      sigma_j = 1.0e-299;

    return erf(z / (constants::kSqrt2 * sigma_j));
  }

  // This is the relative weight of the scattering factors (number densities)
  // when only two layers are mixed and interface is always at position 0
  double Wsimple(const double z, const double sigma)
  {
    return  0.5 * (1.0 + Yfunction(z, sigma));
  }

  // This is the relative weight of the layer's scattering_matrices
  double Wfunction(const double z, const double z_j, const double z_jm1, double sigma_j, double sigma_jm1)
  {
    if (sigma_j == 0.0)
      sigma_j = 1.0e-299;

    if (sigma_jm1 == 0.0)
      sigma_jm1 = 1.0e-299;
  
    const double eta = (sigma_j * z_jm1 + sigma_jm1 * z_j) / (sigma_j + sigma_jm1);

    double value = 0.0;
    
    if (z <= eta)
    {
      value = 0.5 * (1.0 + Yfunction(z - z_j, sigma_j));
    }
    else
    {
      value = 0.5 * (1.0 - Yfunction(z - z_jm1, sigma_jm1));
    }

    return value;
  }


  std::vector<EffectiveLayer> ExtendedLayerSystem(const std::vector<Layer*> layers, const double total_thickness,
    const double eff_layer_thickness, const bool nuclear, double& sample_offset_top, double& sample_offset_bottom)
  {
    const double kOffsetFactor = 5.0;

    // model: vacuum (N=1) is a above z = 0 to + inf, its roughness at inf is zero
    // here we need a physical vacuum layer to extend to real calculated data in the transition region, its roughness stays zero at inf
    double vacuum_thickness = layers[0]->roughness->value * kOffsetFactor;

    if (layers[1]->roughness->value > layers[0]->roughness->value + layers[0]->thickness->value)
      vacuum_thickness = (layers[1]->roughness->value - layers[0]->thickness->value) * kOffsetFactor;

    if (vacuum_thickness == 0.0)
      vacuum_thickness = 0.5 * eff_layer_thickness; // ensure that one does not start on exact eff_layer_thickness value
                                                    // to get mixtures where no should be

    EffectiveLayer vaccum_layer = EffectiveLayer(vacuum_thickness, 0.0, Complex(0.0, 0.0), {});

    const ScatteringMatrix2 vacuum_scattering_matrix = ScatteringMatrix2(layers.front()->grazing_scattering_matrix.size(), Eigen::Matrix2cd::Zero());

    if (nuclear)
      vaccum_layer.grazing_scattering_matrix = vacuum_scattering_matrix;

    std::vector<EffectiveLayer> extended_layer_system = { vaccum_layer };

    // compute needed thicknesses for effective density model
    // the offset bottom has be inserted at the end if no infinite layer is present or in front of the infinite layer
    // extend bottom offset such that the total thickness is a multiple of the effective_layer_thickness
    const Layer* last_layer = layers.back();

    const double offset_top = vacuum_thickness;

    double offset_bottom = last_layer->roughness->value * kOffsetFactor;

    const double tot_thick = offset_top + offset_bottom + total_thickness;

    const double fracpart = tot_thick - floor(tot_thick / eff_layer_thickness) * eff_layer_thickness;

    offset_bottom = offset_bottom + (eff_layer_thickness - fracpart);

    sample_offset_top = offset_top;
    sample_offset_bottom = offset_bottom;

    // model: the substrate(reflection)/vacuum(transmission) layer (N+1) extends to -inf, its roughness at -inf is zero
    // add an infinity layer, necessary as in formulas of paper the layer roughness is defined below the layer.
    // the additional layer only holds the last roughness value of the infinite substrate/vacuum at inifinite value, roughness is zero
    // now fill the layers - up to infinite layer and insert offset_bottom_layer
    bool infinite_layer_flag = false;

    for (const Layer* const lay : layers)
    {
      if (isinf(lay->thickness->value))
      {
        EffectiveLayer offset_bottom_layer = EffectiveLayer(offset_bottom, lay->roughness->value, lay->grazing_scattering_factor, {});

        if (nuclear)
          offset_bottom_layer.grazing_scattering_matrix = lay->grazing_scattering_matrix;

        extended_layer_system.push_back(offset_bottom_layer);

        EffectiveLayer eff_layer = EffectiveLayer(lay->thickness->value, 0.0, lay->grazing_scattering_factor, {});

        if (nuclear)
          eff_layer.grazing_scattering_matrix = lay->grazing_scattering_matrix;

        extended_layer_system.push_back(eff_layer);

        infinite_layer_flag = true;
      }
      else
      {
        EffectiveLayer eff_layer = EffectiveLayer(lay->thickness->value, lay->roughness->value, lay->grazing_scattering_factor, {});

        if (nuclear)
          eff_layer.grazing_scattering_matrix = lay->grazing_scattering_matrix;

        extended_layer_system.push_back(eff_layer);
      }
    }

    // if no infinite layer is present append vacuum offset to end of extended layers
    if (infinite_layer_flag == false)
    {
      EffectiveLayer offset_bottom_layer = EffectiveLayer(offset_bottom, last_layer->roughness->value, Complex(0.0, 0.0), {});

      if (nuclear)
        offset_bottom_layer.grazing_scattering_matrix = vacuum_scattering_matrix;

      extended_layer_system.push_back(offset_bottom_layer);

      EffectiveLayer infinite_bottom_layer = EffectiveLayer(INFINITY, 0.0, Complex(0.0, 0.0), {});

      if (nuclear)
        infinite_bottom_layer.grazing_scattering_matrix = vacuum_scattering_matrix;

      extended_layer_system.push_back(infinite_bottom_layer);
    }

    return extended_layer_system;
  }


  // use negative z axis to use formulas from paper ... directly
  // create one layer more, this one is used for the infinite substrate
  std::vector<double> EffectiveLayerCoordinates(const double total_thickness, const double eff_layer_thickness)
  {
    const size_t number_steps = static_cast<size_t>(round((total_thickness) / eff_layer_thickness)) + 1;

    std::vector<double> coordinates(number_steps);

    for (size_t i = 0; i < number_steps; ++i)
      coordinates[i] = -1.0 * static_cast<double>(i) * eff_layer_thickness;

    return coordinates;
  }


  void CalculateLayerWeights(std::vector<EffectiveLayer>& extended_layer_system, const std::vector<double>& effective_layer_coordinates)
  {
    // create vector of negative layer positions, those are the z_j values of the layers
    // zero entry not needed as no roughness anyway
    double added_thickness = 0.0;
    
    std::vector<double> layer_list(extended_layer_system.size());

    std::transform(extended_layer_system.begin(), extended_layer_system.end(), layer_list.begin(),
      [&](const EffectiveLayer& ext_layer)
      {
        added_thickness -= ext_layer.thickness;

        return added_thickness;
      }
    );

    // create vector with layer weights in each extended layers
    double weight = 0.0;

    size_t extended_layers_size = extended_layer_system.size();

    size_t effective_layer_coordinates_size = effective_layer_coordinates.size();

    for (size_t i = 0; i < extended_layers_size; ++i)
    {
      extended_layer_system[i].effective_layer_weight.resize(effective_layer_coordinates_size);

      for (size_t j = 0; j < effective_layer_coordinates_size; ++j)
      {
        if (i == 0)
        {
          weight = Wfunction(effective_layer_coordinates[j], layer_list[0], INFINITY, extended_layer_system[1].roughness, 0.0);
        }
        else if (i == extended_layers_size)
        {
          weight = Wfunction(effective_layer_coordinates[j], layer_list[i], layer_list[i - 1], 0.0, extended_layer_system[i].roughness);
        }
        else
        {
          weight = Wfunction(effective_layer_coordinates[j], layer_list[i], layer_list[i - 1], extended_layer_system[i + 1].roughness,
            extended_layer_system[i].roughness);
        }

        extended_layer_system[i].effective_layer_weight[j] = weight;
      }
    }

    if (isinf(extended_layer_system.back().thickness))
      extended_layer_system.back().effective_layer_weight = std::vector<double>(extended_layer_system.front().effective_layer_weight.size(), 0.0);

    // calc norm factor
    // do not take infinite layer into account as the infinite bottom layer matrix is used for this layer later and this should not be weighted at all.
    std::vector<double> norm_weight(effective_layer_coordinates.size(), 0.0);

    std::for_each(extended_layer_system.begin(), std::prev(extended_layer_system.end()),
      [&norm_weight](const EffectiveLayer& ext_layer)
      {
        std::transform(norm_weight.begin(), norm_weight.end(), ext_layer.effective_layer_weight.begin(),
          norm_weight.begin(), std::plus<double>());
      }
    );

    // normalize all layers
    std::for_each(extended_layer_system.begin(), std::prev(extended_layer_system.end()),
      [&norm_weight](EffectiveLayer& ext_layer)
      {
        std::transform(ext_layer.effective_layer_weight.begin(), ext_layer.effective_layer_weight.end(), norm_weight.begin(),
          ext_layer.effective_layer_weight.begin(), std::divides<double>());
      }
    );
  }


  std::vector<EffectiveLayer> EffectiveLayers(const std::vector<EffectiveLayer>& extended_layer_system,
    const double eff_layer_thickness, const size_t effective_coordinates_size)
  {
    std::vector<EffectiveLayer> effective_layers(effective_coordinates_size, EffectiveLayer(0.0, 0.0, Complex(0.0, 0.0),
      ScatteringMatrix2(extended_layer_system.front().grazing_scattering_matrix.size(), Eigen::Matrix2cd::Identity()))
    );
    
    size_t index = 0;

    for (EffectiveLayer& eff_layer : effective_layers)
    {
      eff_layer.thickness = eff_layer_thickness;

      // initialize zero values to which the extended_layers parameters are added
      eff_layer.grazing_scattering_factor = Complex(0.0, 0.0);

      eff_layer.grazing_scattering_matrix = matrix::Scale(extended_layer_system.front().grazing_scattering_matrix, 0.0);

      // cycle through each extended layer and add the weighted factors and matrices at the index of the effective layers
      for (const EffectiveLayer& ext_lay : extended_layer_system)
      {
        eff_layer.grazing_scattering_factor += ext_lay.grazing_scattering_factor * ext_lay.effective_layer_weight[index];

        const ScatteringMatrix2 scaled_scattering_matrix = matrix::Scale(ext_lay.grazing_scattering_matrix,
          ext_lay.effective_layer_weight[index]);

        eff_layer.grazing_scattering_matrix = matrix::Add(eff_layer.grazing_scattering_matrix, scaled_scattering_matrix);
      }

      ++index;
    }

    return  effective_layers;
  }

}   // namespace effdensmodel
