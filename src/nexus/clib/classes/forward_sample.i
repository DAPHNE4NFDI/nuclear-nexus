// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module forward_sample

%{
    #define SWIG_FILE_WITH_INIT
    #include "forward_sample.h"
%}


%feature("shadow") ForwardSample::ForwardSample %{
  def __init__(self, layers, drive_detuning = [], function_time = None, id = ""):
    
    if isinstance(layers, (Layer)):
      layers = [layers]

    self._layers = layers

    self._function_time = function_time
    
    _cnexus.ForwardSample_swiginit(self, _cnexus.new_ForwardSample(layers,
                                                                   drive_detuning,
                                                                   function_time,
                                                                   id))
%}



%include "forward_sample.h"



%extend ForwardSample{
  %pythoncode{

  def __setattr__(self, attr, val):

    if attr == "layers":
      if isinstance(val, (list, tuple)):
        self._layers = val  # ref to python list
        val = Layers(val)

    if attr == "drive_detuning":
      if isinstance(val, (list, tuple, np.ndarray)):
        val = DoubleVector(val)

    if attr == "function_time":
      self._function_time = val

    super().__setattr__(attr, val)

  def __repr__(self):
    self.Update()
    output = "ForwardSample\n"
    output += "  .id: {}\n".format(self.id)
    
    output += "-------|------------------------|---------------|-------------|-------------|--------|-----------|----------|-------------|\n"
    output += "{:>8}".format("index |")
    output += "{:>25}".format("Layer id |")
    output += "{:>16}".format("dens. (g/cm3) |")
    output += "{:>14}".format("thick. (nm) |")
    output += "{:>14}".format("rough. (nm) |")
    output += "{:>9}".format("abund. |")
    output += "{:>12}".format("LM factor |")
    output += "{:>11}".format("HI sites |")
    output += "{:>15}".format("dist points |\n")
    output += "-------|------------------------|---------------|-------------|-------------|--------|-----------|----------|-------------|\n"

    i = 0
    for lay in self.layers:
      output += "{:>6} |".format(i)
      output += "{:>23} |".format(lay.id)
      output += "{:>14} |".format(lay.material.density.value)
      output += "{:>12} |".format(lay.thickness.value)
      output += "{:>12} |".format(lay.roughness.value)
      output += "{:>7} |".format(lay.material.abundance.value)
      output += "{:>10} |".format(lay.material.lamb_moessbauer.value)
      if len(lay.material.hyperfine_sites) != 0:
        num_distributionpoints = 0
        for site in lay.material.hyperfine_sites:
          num_distributionpoints += len(site.BareHyperfines)
        output += "{:>9} |".format(len(lay.material.hyperfine_sites))
        output += "{:>12} |".format(num_distributionpoints)
      else:
        output += "{:>9} |".format("")
        output += "{:>12} |".format("")
      output += "\n"
      i +=1

    output += "-------|------------------------|---------------|-------------|-------------|--------|-----------|----------|-------------|\n"
    return output

  }
}


%pythoncode %{
def Air(length, id = ""):
    r"""
    Creates a :class:`ForwardSample` type of air.

    .. versionchanged:: 2.0.0

    Args:
      length (float): length of the air distance in meter.
      id (string): optional string identifier.

    Returns:
      :class:`ForwardSample`: Sample object with material air.
    """
    if id == "":
        id = "Air - {}m".format(length)

    material_sample_air = Material(id = "material_air",
                                   composition = [("N", 1.562), ("O", 0.42), ("C", 0.0003), ("Ar", 0.0094)],
                                   density = Var(0.0012041, 0, 0.0012041, False, "")
                                   )
        
    layer_sample_air = Layer(id = "layer air",
                material = material_sample_air,
                thickness = Var(length * 1.0e9, 0.0, length * 1.0e9, False, ""),
                roughness = Var(0.0, 0.0, 0.0, False, ""),
                thickness_fwhm = Var(0.0, 0.0, 0.0, False, "")
                )

    sample_air = ForwardSample(layers = [layer_sample_air],
                               id = id)
 
    return sample_air


class SimpleSample(ForwardSample):
    r"""
    Creates a :class:`ForwardSample` with only one layer and material.
    All needed instances are created automatically.
    The layer and material parameters can be assigned directly via the :class:`SimpleSample` attributes.

    .. versionchanged:: 2.0.0

    Args:
      thickness (float or :class:`Var`): Thickness of the layer (nm).
      composition (list): Composition of the material in the format *[["element symbol" (string), relative atomic fraction (float)] , ... ]*.
      density (float or :class:`Var`): Density (g/cm :sup:`3`).
      isotope (:class:`MoessbauerIsotope`): Moessbauer isotope.
      abundance (float or :class:`Var`): Isotope abundance (0 to 1).
      lamb_moessbauer (float or :class:`Var`): Lamb Moessbauer factor (0 to 1).
      hyperfine_sites (list): List of :class:`Hyperfine` sites assigned to the material.
      id (string): user identifier.

    Attributes:
      thickness (float or :class:`Var`): Thickness of the layer (nm).
      composition (list): Composition of the material in the format *[["element symbol" (string), relative atomic fraction (float)] , ... ]*.
      density (float or :class:`Var`): Density (g/cm :sup:`3`).
      isotope (:class:`MoessbauerIsotope`): Moessbauer isotope.
      abundance (float or :class:`Var`): Isotope abundance (0 to 1).
      lamb_moessbauer (float or :class:`Var`): Lamb Moessbauer factor (0 to 1).
      hyperfine_sites (list): List of :class:`Hyperfine` sites assigned to the material.
      id (string): user identifier.
      layer (:class:`Layer`): Layer instance of the :class:`SimpleSample`.
      material (:class:`Material`): Material instance of the :class:`SimpleSample`.

    Returns:
      :class:`ForwardSample`: Sample object.
    """

    def __init__(self,
                 thickness,
                 composition,
                 density,
                 isotope=None,
                 abundance=0.0,
                 lamb_moessbauer=0.0,
                 hyperfine_sites=[],
                 id=''):
        
        self.material = Material(composition=composition,
                                 density=density,
                                 isotope=isotope,
                                 abundance=abundance,
                                 lamb_moessbauer=lamb_moessbauer,
                                 hyperfine_sites=hyperfine_sites,
                                 id='')
        
        self.layer = Layer(thickness, self.material, id="")
        
        super().__init__([self.layer], [], None, id)
        
        self.thickness = self.layers[0].thickness
        self.composition = self.material.composition
        self.density = self.material.density
        self.isotope = self.material.isotope
        self.abundance = self.material.abundance
        self.hyperfine_sites = self.material.hyperfine_sites
    
    def __setattr__(self, attr, val):

        if attr == "thickness":
            self.layers[0].thickness = val
            val = self.layers[0].thickness

        if attr == "density":
            self.layers[0].material.density = val
            val = self.layers[0].material.density

        if attr == "abundance":
            self.layers[0].material.abundance = val
            val = self.layers[0].material.abundance

        if attr == "lamb_moessbauer":
            self.layers[0].material.lamb_moessbauer = val
            val = self.layers[0].material.lamb_moessbauer

        if attr == "composition":
            val = Composition(val)
            self.layers[0].material.composition = val
            val = self.layers[0].material.composition

        if attr == "hyperfine_sites":
            if isinstance(val, (list, tuple)):
                val = HyperfineSites(val)
                self.layers[0].material.hyperfine_sites = val
                val = self.layers[0].material.hyperfine_sites
        
        super().__setattr__(attr, val)

%}
