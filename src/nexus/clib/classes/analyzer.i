// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module analyzer

%{
    #define SWIG_FILE_WITH_INIT
    #include "analyzer.h"
%}


%feature("shadow") Analyzer::Analyzer %{
  def __init__(self, efficiency = 0, mixing_angle = 0, canting_angle = 0, id = ""):

    if isinstance(efficiency, (int, float)):
      efficiency = Var(efficiency, 0, 1, False, "")

    if isinstance(mixing_angle, (int, float)):
      mixing_angle = Var(mixing_angle, -45, 45, False, "")

    if isinstance(canting_angle, (int, float)):
      canting_angle = Var(canting_angle, -90, 90, False, "")

    self._efficiency = efficiency

    self._canting_angle = canting_angle

    self._mixing_angle = mixing_angle

    _cnexus.Analyzer_swiginit(self, _cnexus.new_Analyzer(efficiency, mixing_angle, canting_angle, id))
%}



%include "analyzer.h"



%extend Analyzer{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this", "_efficiency", "_mixing_angle", "_canting_angle") and not hasattr(self, attr):
      raise Exception("Analyzer has no attribute {}".format(attr))

    if attr in ("efficiency", "mixing_angle", "canting_angle", "fwhm"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if attr == "efficiency":
      self._efficiency = val

    if attr == "mixing_angle":
      self._mixing_angle = val

    if attr == "canting_angle":
      self._canting_angle = val

    super().__setattr__(attr, val)


  def __repr__(self):
    output = "Analyzer\n"
    output += "  .id: {}\n".format(self.id)
    output += "  .efficiency: {}\n".format(self.efficiency)
    output += "  .mixing_angle: {}\n".format(self.mixing_angle)
    output += "  .canting_angle: {}\n".format(self.canting_angle)
    output += "  .matrix:\n{}\n".format(self.matrix)
    return output

  }
}