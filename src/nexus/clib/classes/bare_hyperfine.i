// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module bare_hyperfine

%{
    #define SWIG_FILE_WITH_INIT
    #include "bare_hyperfine.h"
%}


%feature("kwargs") BareHyperfine;

%feature("autodoc", "List of :class:`BareHyperfine` objects.") std::vector<BareHyperfine>;

%template(VectorBareHyperfine) std::vector<BareHyperfine>;



%include "bare_hyperfine.h"



%extend BareHyperfine{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr != "this" and not hasattr(self, attr):
      raise Exception("Barehyperfine does not have attribute {}".format(attr))

    super().__setattr__(attr, val)

  def __repr__(self):
    output = "BareHyperfine Parameters: \n"
    output += "  .weight = {}\n".format(self.weight)
    output += "  .isomer_shift (mm/s) = {}\n".format(self.isomer)
    output += "  .magnetic_field (T) = {}\n".format(self.magnetic_field)
    output += "  .magnetic_theta (rad) = {}\n".format(self.magnetic_theta)
    output += "  .magnetic_phi (rad) = {}\n".format(self.magnetic_phi)
    output += "  .quadrupole (mm/s) = {}\n".format(self.quadrupole)
    output += "  .quadrupole_alpha (rad) = {}\n".format(self.quadrupole_alpha)
    output += "  .quadrupole_beta (rad) = {}\n".format(self.quadrupole_beta)
    output += "  .quadrupole_gamma (rad) = {}\n".format(self.quadrupole_gamma)
    output += "  .quadrupole_asymmetry = {}\n".format(self.quadrupole_asymmetry)
    output += "  .texture = {}\n".format(self.texture)
    output += "  .lamb_moessbauer = {}\n".format(self.lamb_moessbauer)
    output += "  .broadening = {}\n".format(self.broadening)
    output += "  .isotropic = {}\n".format(self.isotropic)
    return output

  }
}
