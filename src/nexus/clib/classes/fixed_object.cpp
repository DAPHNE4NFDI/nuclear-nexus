// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "fixed_object.h"

#include <iostream>
#include <cmath>
#include <complex>

#include "../nexus_definitions.h"
#include "../math/constants.h"


using namespace std::complex_literals;


FixedObject::FixedObject(
  const Complex scattering_factor_,
  const Eigen::Matrix2cd& matrix_,
  const std::string id_
) :
  NxObject(id_, NxObjectId::FixedObject),
  scattering_factor(scattering_factor_),
  matrix(matrix_)
{}

// NxObject function definitions

ObjectMatrix2 FixedObject::ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
  const bool calc_transitions)
{
  return ObjectMatrix2(detuning.size(), matrix);
}
