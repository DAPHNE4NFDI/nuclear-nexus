// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the OptimizerHandler class.
// The OptimizerHandler is called by the Optimizer and the Fit classes to setup the optimization.


#ifndef NEXUS_OPTIMIZER_HANDLER_H_
#define NEXUS_OPTIMIZER_HANDLER_H_

#include <string>
#include <vector>

#include <ceres/ceres.h>

#ifdef NEXUS_USE_NLOPT
#include <nlopt.h>
#endif

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"
#include "../classes/optimizer_options.h"
#include "../classes/inequality.h"


class OptimizerHandler {
public:
  OptimizerHandler(
    const std::string id,
    const std::vector<Var*> external_fit_variables,
    Inequality* const inequality
  ) :
    id(id),
    external_fit_variables(external_fit_variables),
    inequality(inequality)
  {
    options = new OptimizerOptions();
  }

  ~OptimizerHandler()
  {
    delete options;
  };

  /**
  Set all fitted vars to their initial values.
  */
  void SetInitialVars();

  void ClearFitVariables();

  void PrepareFitVariable(Var* const var);

  void AddFitVariable(Var* const var);
  
  void EraseFitVariable(Var* const var);
  
  std::vector<Var*> GetFitVariables() const;

  void AddEquality(Var* const var);

  std::vector<Var*> GetFitEqualities() const;

  bool Initialize(bool global_fit);
  
  std::vector<double*> GetFitParameterPointers() const;

  void SetFitParameters(const std::vector<double> input_vector);

  std::vector<double> GetFitParameters() const;

  std::vector<double> GetInitialParameters() const;

  std::vector<double> GetLowerBounds() const;

  std::vector<double> GetUpperBounds() const;

  std::vector<std::string> GetIds() const;

  std::string id = "";

  OptimizerOptions* options = nullptr;

  double cost = INFINITY;
 
  std::vector<Var*> fit_variables{};

  std::vector<Var*> external_fit_variables;

  Inequality* inequality = nullptr;

  std::vector<Var*> fit_equalities{};

  size_t num_fit_parameters = 0;

  std::vector<double> initial_parameters{};

  std::vector<double*> fit_parameter_pointers{};

  std::vector<double> lower_bounds{};

  std::vector<double> upper_bounds{};

  std::vector<std::string> ids{};

  // total sum of all user residuals of all data (measurements) provided by the user
  double total_sum_user_residuals = std::numeric_limits<double>::quiet_NaN();

  // total sum of user residuals / number of data points
  double total_cost = std::numeric_limits<double>::quiet_NaN();

  // indices of parameters on which is the fit model does not depend on.
  // Obtained from number of zero diagonal element of covariance matrix
  // in the code from the nan values of the correlation matrix
  std::vector<int> non_dependent_variables{};

  // inverseHessian
  // covariance from the ceres solver
  // C(x) = (J*(x)J(x))^(-1)
  // with J being the Jacobi matrix
  Eigen::MatrixXd inverse_hessian{};

  // scaled version of C
  // f = residual weight * SUM_i(residual_i^2) / (number of data points - number of fit parameters);
  // covariance = f * inverse Hessian
  Eigen::MatrixXd covariance_matrix{};

  // Correlation matrix
  // corr_ij = C_ij / sqrt(C_ii * C_jj)
  Eigen::MatrixXd correlation_matrix{};

  // standard estimated error for fit parameter
  Eigen::VectorXd fit_parameter_errors{};

  double* nlopt_fit_parameters = nullptr;

  #ifdef NEXUS_USE_NLOPT
  void SetNloptOptions(nlopt_opt opt);
  #endif

  void SetCeresOptions(
    const std::string method,
    ceres::Solver::Options& ceres_solver_options,
    ceres::NumericDiffOptions& ceres_numeric_cost_options
  );

};

#endif // NEXUS_OPTIMIZER_HANDLER_H_
