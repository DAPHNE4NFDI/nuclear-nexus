// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "optimizer_handler.h"

#include <iostream>

#include "../utilities/errors.h"
#include "../utilities/python_print.h"


void OptimizerHandler::SetInitialVars()
{
	for (size_t i = 0; i < fit_parameter_pointers.size(); ++i)
		*fit_parameter_pointers[i] = initial_parameters[i];
}

void OptimizerHandler::ClearFitVariables()
{
	fit_variables.clear();
}

void  OptimizerHandler::PrepareFitVariable(Var* const var)
{
	if (var != nullptr)
	{
		if (var->fit)
			AddFitVariable(var);
		else if (var->equality != nullptr)
			AddEquality(var);
	}
}

void OptimizerHandler::AddFitVariable(Var* const var)
{
  const auto itr = std::find(fit_variables.begin(), fit_variables.end(), var);

	if (itr == fit_variables.end()) // if not found add var pointer
	  fit_variables.push_back(var);
}

void OptimizerHandler::EraseFitVariable(Var * const var)
{
	const auto itr = std::find(fit_variables.begin(), fit_variables.end(), var);

	if (itr != fit_variables.end()) // if found erase var at position
    fit_variables.erase(itr);
}

void OptimizerHandler::AddEquality(Var* const var)
{
	const auto itr = std::find(fit_equalities.begin(), fit_equalities.end(), var);

	if (itr == fit_equalities.end()) // if not found add var pointer
		fit_equalities.push_back(var);
}

std::vector<Var*> OptimizerHandler::GetFitEqualities() const
{
	return fit_equalities;
}


bool OptimizerHandler::Initialize(bool global_fit)
{
	fit_parameter_pointers.clear();
	initial_parameters.clear();
	
	lower_bounds.clear();
	upper_bounds.clear();
	
	ids.clear();

	bool ret = true;

	for (Var* const var : fit_variables)
	{
		fit_parameter_pointers.push_back(&(var->value));
		initial_parameters.push_back(var->value);
	
		lower_bounds.push_back(var->min);
		upper_bounds.push_back(var->max);
		
		ids.push_back(var->id);

		if (var->min > var->value ||
			  var->max < var->value ||
			  var->min > var->max)
		{
			errors::WarningMessage("OptimizerHandler", "boundary warning in nx.Var id: " + var->id + " value: " + std::to_string(var->value)
				+ " min: " + std::to_string(var->min) + " max: " + std::to_string(var->max));
		}

		if (global_fit)
		{
			if (isinf(var->min))
			{
				errors::WarningMessage("OptimizerHandler - boundary error in a global fit method",
					"nx.Var id : " + var->id + " value : " + std::to_string(var->value) + "  - min boundary is infinite.\n"
				  + " Fit region too large. No fit performed.");

				ret = false;
			}

			if (isinf(var->max))
			{
				errors::WarningMessage("OptimizerHandler - boundary error in a global fit method",
					"nx.Var id : " + var->id + " value : " + std::to_string(var->value) + "  - max boundary is infinite.\n"
					+ " Fit region too large. No fit performed.");

				ret = false;
			}
		}
	}

	return ret;
}

std::vector<Var*> OptimizerHandler::GetFitVariables() const
{
	return fit_variables;
}

std::vector<double*> OptimizerHandler::GetFitParameterPointers() const
{
	return fit_parameter_pointers;
}

void OptimizerHandler::SetFitParameters(const std::vector<double> input_vector)
{
	for (size_t i = 0; i < fit_variables.size(); i++)
		fit_variables[i]->value = input_vector[i];
}

std::vector<double> OptimizerHandler::GetFitParameters() const
{
	std::vector<double> fit_parameters{};

	for (Var* const var : fit_variables)
		fit_parameters.push_back(var->value);

	return fit_parameters;
}

std::vector<double> OptimizerHandler::GetInitialParameters() const
{
	return initial_parameters;
}

std::vector<double> OptimizerHandler::GetLowerBounds() const
{
	return lower_bounds;
}

std::vector<double> OptimizerHandler::GetUpperBounds() const
{
	return upper_bounds;
}

std::vector<std::string> OptimizerHandler::GetIds() const
{
	return ids;
}


void OptimizerHandler::SetCeresOptions(const std::string method, ceres::Solver::Options& ceres_solver_options, ceres::NumericDiffOptions& ceres_numeric_cost_options)
{
	// NUMERIC COST OPTIONS
	ceres_numeric_cost_options.relative_step_size = options->cost_relative_step_size;

	// SOLVER OPTIONS
	// minimizer, trust region, dogleg and line search type
	if (method == "LevMar")
	{
		ceres_solver_options.minimizer_type = ceres::TRUST_REGION;
		ceres_solver_options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
	}
	else if (method == "Dogleg")
	{
		ceres_solver_options.minimizer_type = ceres::TRUST_REGION;
		ceres_solver_options.trust_region_strategy_type = ceres::DOGLEG;
		ceres_solver_options.dogleg_type = ceres::TRADITIONAL_DOGLEG;
	}
	else if (method == "SubDogleg")
	{
		ceres_solver_options.minimizer_type = ceres::TRUST_REGION;
		ceres_solver_options.trust_region_strategy_type = ceres::DOGLEG;
		ceres_solver_options.dogleg_type = ceres::SUBSPACE_DOGLEG;
	}
	else if (method == "LineSearch")
	{
		ceres_solver_options.minimizer_type = ceres::LINE_SEARCH;
	}

	// line search options
	if (options->LineSearch->line_search_type == "LBFGS")
	{
		ceres_solver_options.line_search_direction_type = ceres::LBFGS;
	}
	else if (options->LineSearch->line_search_type == "BFGS")
	{
		ceres_solver_options.line_search_direction_type = ceres::BFGS;
	}
	else if (options->LineSearch->line_search_type == "NonConGrad")
	{
		ceres_solver_options.line_search_direction_type = ceres::NONLINEAR_CONJUGATE_GRADIENT;
	}
	else if (options->LineSearch->line_search_type == "SteepDes")
	{
		ceres_solver_options.line_search_direction_type = ceres::STEEPEST_DESCENT;
	}

	//min_line_search_step_size = min_line_search_step_size
	ceres_solver_options.max_num_line_search_step_size_iterations = options->LineSearch->max_num_line_search_step_size_iterations;

	// stop conditions
	ceres_solver_options.max_num_iterations = static_cast<int>(options->iterations);

	if (options->iterations == 0)
		ceres_solver_options.max_num_iterations = 200;

	ceres_solver_options.max_solver_time_in_seconds = options->max_time;

	// threads MUST be 1, otherwise parallel calculations will conflict with nexus internal pointer assignment of fit values.
	ceres_solver_options.num_threads = 1;

	ceres_solver_options.minimizer_progress_to_stdout = options->output;

	// stop conditions
	ceres_solver_options.function_tolerance = options->function_tolerance;

	ceres_solver_options.gradient_tolerance = options->gradient_tolerance;

	ceres_solver_options.parameter_tolerance = options->parameter_tolerance;
}


#ifdef NEXUS_USE_NLOPT
void OptimizerHandler::SetNloptOptions(nlopt_opt opt)
{
	// no absolute minimum stop value on cost
	nlopt_set_stopval(opt, -HUGE_VAL);

	// no absolute function tolerance
	nlopt_set_ftol_abs(opt, -1);

	const double parameter_abs = -1;

	nlopt_set_xtol_abs(opt, &parameter_abs);

	// relative function tolerance
	nlopt_set_ftol_rel(opt, options->function_tolerance);

	// relative parameter tolerance
	// divided here because the tolerance of the whole parameter vector is used in NLopt
	nlopt_set_xtol_rel(opt, options->parameter_tolerance / initial_parameters.size());

	// max time, should be set for global optimization
	nlopt_set_maxtime(opt, options->max_time);  // in sec

	unsigned int iterations = options->iterations;

	if (options->iterations == 0)
		iterations = 2000;

	nlopt_set_maxeval(opt, static_cast<int>(iterations));
}
#endif
