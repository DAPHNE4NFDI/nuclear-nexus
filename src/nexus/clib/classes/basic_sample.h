// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Sample class.
// A sample consists of several Layer objects and holds information on its lateral dimension and its scattering geometry.


#ifndef NEXUS_BASIC_SAMLPE_H_
#define NEXUS_BASIC_SAMLPE_H_


#include <vector>

#include "../nexus_definitions.h"
#include "../classes/optimizer_handler.h"
#include "../classes/nxobject.h"
#include "../classes/layer.h"
#include "../classes/function_time.h"

/**
Constructor for :class:`BasicSample`.

.. versionadded:: 2.0.0

   This is a base class only, do not use it directly.
   Use the :class:`ForwardSample` or :class:`GrazingSample` instead.

Args:
   layers (list): List of Layer in order of beam propagation.
   drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
   function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
     Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
   id (string): User identifier.

Attributes:
   layers (list): List of Layer in order of beam propagation.
   drive_detuning (list or ndarray): Detuning values for a sample on a Moessbauer drive.
   function_time (:class:`FunctionTime`): A :class:`FunctionTime` implementation of the user to describe sample motion.
     Must return the phase factor :math:`\phi(t) = k x(t)`, where :math:`k` is the photon wave vector along beam direction and :math:`x(t)` the sample motion along the beam propagation direction.
   id (string): User identifier.
*/
class BasicSample : public NxObject {
public:
  BasicSample(
    const std::vector<Layer*> layers,
    const std::vector<double> drive_detuning, // Gamma
    FunctionTime* const function_time,  // return phase factor
    const NxObjectId object_id,
    const std::string id = ""
    );

  virtual ~BasicSample() {};

  /* virtual definitions of NxObject functions */

  virtual void Update() = 0;

  virtual void PopulateFitHandler(OptimizerHandler* fit_handler) = 0;

  virtual Complex ElectronicAmplitude(const double energy) = 0;

  virtual Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy) = 0;

  virtual ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
    const bool calc_transitions) = 0;

  /* virtual functions of BasicSample */

  virtual Complex ElectronicAmplitudeThickness(const double energy, const double thickness) = 0;

  virtual std::vector<Complex> ElectronicFieldAmplitude(const double energy, const size_t num_points) = 0;

  virtual double ElectronicIntensity(const double energy) = 0;

  virtual double ElectronicIntensityThickness(const double energy, const double thickness) = 0;

  virtual std::vector<double> ElectronicFieldIntensity(const double energy, const size_t num_points) = 0;

  /* BasicSample functions */

  std::vector<Layer*> FindUniqueLayers() const;

  /**
  Calculates the total thickness of the sample.

  Returns:
     float: Total thickness of the sample (nm).
  */
  double TotalThickness() const;

  /**
  A list with the center coordinates of all layers in the sample.

  Returns:
     list: Position of the Layer centers.
  */
  std::vector<double> LayerCenters() const;

  /**
  A list with the positions of the layer interfaces.

  Returns:
     list: Position of the Layer interfaces.
  */
  std::vector<double> Interfaces() const;

  /**
  List with all layer id of the sample.

  Returns:
     list: Layers ids.
  */
  std::vector<std::string> Ids() const;

  /* variables */

  std::vector<Layer*> layers{};

  std::vector<double> drive_detuning{};

  FunctionTime* function_time = nullptr;
};

#endif // NEXUS_BASIC_SAMLPE_H_
