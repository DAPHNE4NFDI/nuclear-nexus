// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "analyzer.h"

#include <cmath>

#include "../math/constants.h"
#include "../math/matrix.h"


using namespace std::complex_literals;


Analyzer::Analyzer(
  Var* const efficiency_,
  Var* const mixing_angle_,
  Var* const canting_angle_,
  const std::string id_
) :
  NxObject(id_, NxObjectId::Analyzer),
  efficiency(efficiency_),
  mixing_angle(mixing_angle_),
  canting_angle(canting_angle_)
{
  Update();
}

void Analyzer::Update()
{
  const Complex x = cos(mixing_angle->value * constants::kDegToRad);
  const Complex y = 1.0i * sin(mixing_angle->value * constants::kDegToRad);

  const Eigen::Vector2cd jones_vector = matrix::RotationMatrix(canting_angle->value) * Eigen::Vector2cd(x, y);

  matrix = (1.0 - efficiency->value) * Eigen::Matrix2cd::Identity()
           + efficiency->value * jones_vector * jones_vector.adjoint();
};


void Analyzer::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  fit_handler->PrepareFitVariable(efficiency);

  fit_handler->PrepareFitVariable(mixing_angle);

  fit_handler->PrepareFitVariable(canting_angle);
};


void Analyzer::Unpolarized()
{
  efficiency->value = 0.0;

  mixing_angle->value = 0.0;

  canting_angle->value = 0.0;

  Update();
}


void Analyzer::LinearSigma()
{
  efficiency->value = 1.0;

  mixing_angle->value = 0.0;

  canting_angle->value = 0.0;

  Update();
}


void Analyzer::LinearPi()
{
  efficiency->value = 1.0;

  mixing_angle->value = 0.0;

  canting_angle->value = 90.0;

  Update();
}


void Analyzer::CircularLeft()
{
  efficiency->value = 1.0;

  mixing_angle->value = 45.0;

  canting_angle->value = 0.0;

  Update();
}


void Analyzer::CircularRight()
{
  efficiency->value = 1.0;

  mixing_angle->value = -45.0;

  canting_angle->value = 0.0;

  Update();
}


void Analyzer::SetJonesMatrix(const Eigen::Matrix2cd& matrix_)
{
  // matrix = matrix_;

  errors::WarningMessage("Analyzer", "removed in version 2.0.0");
}


void Analyzer::Rotate(const double angle)
{
  //matrix = matrix::MatrixRotation(matrix, angle);

  errors::WarningMessage("Analyzer", "removed in version 2.0.0");
}


// NxObject function definitions

ObjectMatrix2 Analyzer::ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
  const bool calc_transitions)
{
  return ObjectMatrix2(detuning.size(), matrix);
}
