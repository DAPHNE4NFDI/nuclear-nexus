// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of NxObject class.
// Base class for all objects in the beam path.
// This class is passed to the Experiment class.
// The class identifier NxObjectId is used in functors for safe class down casting.


#ifndef NEXUS_NXOBJECT_H_
#define NEXUS_NXOBJECT_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../scattering/layer_matrix.h"
#include "../classes/optimizer_handler.h"


enum class NxObjectId { Analyzer, ForwardSample, GrazingSample, FixedObject, ConussObject };


/**
Abstract class for each object in the beam path. Base class for :class:`Analyzer`, :class:`Sample` and :class:`FixedObject`. Do not use an :class:`NxObject`, use the derived classes.

.. versionchanged:: 2.0.0

	 changed from old :class:`Sample` class to :class:`BasicSample`.

Attributes:
   object_id (NxObjectId): Internal object identifier.
	 object_matrix (list): n-dimensional array of 2x2 complex matrices, where n is the number of detuning points
	 id (string): User identifier.
 */
struct NxObject {
	NxObject(const std::string id, const NxObjectId object_id): 
		id(id), 
		object_id(object_id)
	{};

	virtual ~NxObject() {};

	virtual void Update() = 0;

	virtual void PopulateFitHandler(OptimizerHandler* fit_handler) = 0;

  /**
  Args:
   energy (float): Energy for the calculation of the scattering factor
	
	Returns:
	  complex: Electronic scattering factor
  */
	virtual Complex ElectronicAmplitude(const double energy) = 0;

  /**
  Calculates the detuning-dependent complex 2x2 object matrix.
	
	Args:
    energy (float): Energy for the calculation of the scattering factor
  
	Returns:
	 list: Complex 2x2 matrix.
  */
  virtual Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy) = 0;

	/**
	Calculates the detuning-dependent complex 2x2 object matrix.

	Args:
     isotope (:class:`MoessbauerIsotope`): Moessbauer isotope of the experiment.
     detuning (list or ndarray): n-dimensional array with detuning values.
     calc_transitions (bool): Set to ``True`` for calculations of the nuclear transitions in the object.

	Returns:
     list: List of complex 2x2 matrices.
	*/
	virtual ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
		const bool calc_transitions) = 0;

	std::string id = "";

	NxObjectId object_id;

	// used for storing the object matrix in EnergyTimeSpectrum calculations.
	LayerMatrix2 object_matrix{};

	std::string GetAddress()
	{
		std::stringstream ss;

		ss << this;

		return ss.str();
	}
};

#endif // NEXUS_NXOBJECT_H_
