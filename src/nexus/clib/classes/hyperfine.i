// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module hyperfine

%{
    #define SWIG_FILE_WITH_INIT
    #include "hyperfine.h"
%}


%template(HyperfineSites) std::vector<Hyperfine*>;


%feature("shadow") Hyperfine::Hyperfine %{
  def __init__(self,
               id = "",
               weight = 1.0,
               isomer = 0.0,
               magnetic_field = 0.0,
               magnetic_theta = 0.0,
               magnetic_phi = 0.0,
               quadrupole = 0.0,
               quadrupole_alpha = 0.0,
               quadrupole_beta = 0.0,
               quadrupole_gamma = 0.0,
               quadrupole_asymmetry = 0.0,
               texture = 1.0,
               lamb_moessbauer = 0.0,
               broadening = 1.0,
               isomer_dist = None,
               magnetic_field_dist = None,
               magnetic_theta_dist = None,
               magnetic_phi_dist = None,
               quadrupole_dist = None,
               quadrupole_alpha_dist = None,
               quadrupole_beta_dist = None,
               quadrupole_gamma_dist = None,
               quadrupole_asymmetry_dist = None,
               isotropic = False
               ):

    if isinstance(weight, (int, float)):
      weight = Var(weight, 0.0, np.inf, False, "")
    if isinstance(isomer, (int, float)):
      isomer = Var(isomer, -np.inf, np.inf, False, "")
    if isinstance(magnetic_field, (int, float)):
      magnetic_field = Var(magnetic_field, -np.inf, np.inf, False, "")
    if isinstance(magnetic_theta, (int, float)):
      magnetic_theta = Var(magnetic_theta, -np.inf, np.inf, False, "")
    if isinstance(magnetic_phi, (int, float)):
      magnetic_phi = Var(magnetic_phi, -np.inf, np.inf, False, "")
    if isinstance(quadrupole, (int,float)):
      quadrupole = Var(quadrupole, -np.inf, np.inf, False, "")
    if isinstance(quadrupole_alpha, (int, float)):
      quadrupole_alpha = Var(quadrupole_alpha, -np.inf, np.inf, False, "")
    if isinstance(quadrupole_beta, (int, float)):
      quadrupole_beta = Var(quadrupole_beta, -np.inf, np.inf, False, "")
    if isinstance(quadrupole_gamma, (int, float)):
      quadrupole_gamma = Var(quadrupole_gamma, -np.inf, np.inf, False, "")
    if isinstance(quadrupole_asymmetry, (int, float)):
      quadrupole_asymmetry = Var(quadrupole_asymmetry, 0.0, 1.0, False, "")
    if isinstance(texture, (int, float)):
      texture = Var(texture, 0.0, 1.0, False, "")
    if isinstance(lamb_moessbauer, (int, float)):
      lamb_moessbauer = Var(lamb_moessbauer, 0.0, 1.0, False, "")
    if isinstance(broadening, (int, float)):
      broadening = Var(broadening, 0.0, np.inf, False, "")

    self._weight = weight
    self._isomer = isomer
    self._magnetic_field = magnetic_field
    self._magnetic_theta = magnetic_theta
    self._magnetic_phi = magnetic_phi
    self._quadrupole = quadrupole
    self._quadrupole_alpha = quadrupole_alpha
    self._quadrupole_beta = quadrupole_beta
    self._quadrupole_gamma = quadrupole_gamma
    self._quadrupole_asymmetry = quadrupole_asymmetry
    self._texture = texture
    self._lamb_moessbauer = lamb_moessbauer
    self._broadening = broadening

    self._isomer_dist = isomer_dist
    self._magnetic_field_dist = magnetic_field_dist
    self._magnetic_theta_dist = magnetic_theta_dist
    self._magnetic_phi_dist = magnetic_phi_dist
    self._quadrupole_dist = quadrupole_dist
    self._quadrupole_alpha_dist = quadrupole_alpha_dist
    self._quadrupole_beta_dist = quadrupole_beta_dist
    self._quadrupole_gamma_dist = quadrupole_gamma_dist
    self._quadrupole_asymmetry_dist = quadrupole_asymmetry_dist

    _cnexus.Hyperfine_swiginit(self, _cnexus.new_Hyperfine(weight,
                                                           isomer,
                                                           magnetic_field,
                                                           magnetic_theta,
                                                           magnetic_phi,
                                                           quadrupole,
                                                           quadrupole_alpha,
                                                           quadrupole_beta,
                                                           quadrupole_gamma,
                                                           quadrupole_asymmetry,
                                                           texture,
                                                           lamb_moessbauer,
                                                           broadening,
                                                           isomer_dist,
                                                           magnetic_field_dist,
                                                           magnetic_theta_dist,
                                                           magnetic_phi_dist,
                                                           quadrupole_dist,
                                                           quadrupole_alpha_dist,
                                                           quadrupole_beta_dist,
                                                           quadrupole_gamma_dist,
                                                           quadrupole_asymmetry_dist,
                                                           isotropic,
                                                           id))
%}


%feature("pythonprepend") Hyperfine::SetIsomerDistribution %{
  self._isomer_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetIsomerDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetIsomerDistribution %{
  def GetIsomerDistribution(self):
    r"""
    Get the isomer distribution.

    Returns:
       ndarray, ndarray: isomer values, relative weight
    """
    if self.isomer_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .isomer.")

    dist = _cnexus.Hyperfine_GetIsomerDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetMagneticFieldDistribution %{
  self._magnetic_field_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetMagneticFieldDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetMagneticFieldDistribution %{
  def GetMagneticFieldDistribution(self):
    r"""
    Get the magnetic field distribution.

    Returns:
       ndarray, ndarray: magnetic field values, relative weight
    """
    if self.magnetic_field_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .magnetic_field.")

    dist = _cnexus.Hyperfine_GetMagneticFieldDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetMagneticThetaDistribution %{
  self._magnetic_theta_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetMagneticThetaDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetMagneticThetaDistribution %{
  def GetMagneticThetaDistribution(self):
    r"""
    Get the magnetic theta angle distribution.

    Returns:
       ndarray, ndarray: magnetic theta values, relative weight
    """
    if self.magnetic_theta_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .magnetic_theta.")

    dist = _cnexus.Hyperfine_GetMagneticThetaDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetMagneticPhiDistribution %{
  self._magnetic_phi_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetMagneticPhiDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetMagneticPhiDistribution %{
  def GetMagneticPhiDistribution(self):
    r"""
    Get the magnetic phi angle distribution.

    Returns:
       ndarray, ndarray: magnetic phi values, relative weight
    """
    if self.magnetic_phi_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .magnetic_phi.")

    dist = _cnexus.Hyperfine_GetMagneticPhiDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetQuadrupoleDistribution %{
  self._quadrupole_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetQuadrupoleDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetQuadrupoleDistribution %{
  def GetQuadrupoleDistribution(self):
    r"""
    Get the quadrupole distribution.

    Returns:
       ndarray, ndarray: quadrupole values, relative weight
    """
    if self.quadrupole_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .quarupole.")

    dist = _cnexus.Hyperfine_GetQuadrupoleDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetQuadrupoleAlphaDistribution %{
  self._quadrupole_alpha_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetQuadrupoleAlphaDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetQuadrupoleAlphaDistribution %{
  def GetQuadrupoleAlphaDistribution(self):
    r"""
    Get the quadrupole alpha angle distribution.

    Returns:
       ndarray, ndarray: quadrupole alpha angle values, relative weight
    """
    if self.quadrupole_alpha_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .quarupole_alpha.")

    dist = _cnexus.Hyperfine_GetQuadrupoleAlphaDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetQuadrupoleBetaDistribution %{
  self._quadrupole_beta_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetQuadrupoleBetaDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetQuadrupoleBetaDistribution %{
  def GetQuadrupoleBetaDistribution(self):
    r"""
    Get the quadrupole beta angle distribution.

    Returns:
       ndarray, ndarray: quadrupole beta angle values, relative weight
    """
    if self.quadrupole_beta_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .quarupole_beta.")

    dist = _cnexus.Hyperfine_GetQuadrupoleBetaDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetQuadrupoleGammaDistribution %{
  self._quadrupole_gamma_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetQuadrupoleGammaDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetQuadrupoleGammaDistribution %{
  def GetQuadrupoleGammaDistribution(self):
    r"""
    Get the quadrupole gamma angle distribution.

    Returns:
       ndarray, ndarray: quadrupole gamma angle values, relative weight
    """
    if self.quadrupole_gamma_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .quarupole_gamma.")

    dist = _cnexus.Hyperfine_GetQuadrupoleGammaDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}


%feature("pythonprepend") Hyperfine::SetQuadrupoleAsymmetryDistribution %{
  self._quadrupole_asymmetry_dist_ptr = dist
%}

%feature("pythonappend") Hyperfine::SetQuadrupoleAsymmetryDistribution %{
   self.Update()
%}

%feature("shadow") Hyperfine::GetQuadrupoleAsymmetryDistribution %{
  def GetQuadrupoleAsymmetryDistribution(self):
    r"""
    Get the quadrupole asymmetry distribution.

    Returns:
       ndarray, ndarray: quadrupole asymmetry values, relative weight
    """
    if self.quadrupole_asymmetry_dist_ptr == None:
        raise Exception("NEXUS error: No distribution set on .quarupole_asymmetry.")

    dist = _cnexus.Hyperfine_GetQuadrupoleAsymmetryDistribution(self)
    return np.array(dist[0]), np.array(dist[1])
%}

%feature("shadow") Hyperfine::SetRandomDistribution %{
  def SetRandomDistribution(self, target, type, points = 401, method = "model", order = 1):
    r"""
    Set a random distribution in the magnetic hyperfine field or the electric field gradient.
    For a hyperfine site random magnetic and random quadrupole magnitude distributions can be set in parallel.

    .. versionchanged:: 1.2.0

       In versions < 1.2.0 this function provided a real random distribution of vectors.
       However, this requires a large number of points and random sampling does not guarantee the correct spectrum.
       Since 1.2.0, complete random distributions are removed.
       Now two optimized versions are implemented.
       A new averaging method uses a group theoretical approach for optimized sampling of vectors.
       This method "model" should be used for most cases as it reduces the number of points needed considerably.
       The random distribution method "random" is still available but now use an optimized random sampling of points based on permutations and mirroring of random vectors, which leads to much better results.
       This algorithm requires multiple of 48 points for 3D and multiple of 8 points for 2D distributions and will choose a possible number of points closest to your point number input.

    .. note:: For a quadrupole distribution a random orientation of the asymmetry parameter is not included.
       Only Vzz is randomly chosen and :math:`\alpha` is always zero. 

    Args:
       target (string): ``mag`` for magnetic distributions.
         ``efg`` for quadrupole distributions. 
         ``none`` to reset all angular distributions.
       type (string): *3D* for 3-dimensional distribution or for 2-dimensional distributions one of the following options:
                      *2Dks*, *2Dkp*, *2Dsp*, *2Dsk*, *2Dpk*, *2Dps*.
                      Convention for *2Dxy* is that xy give the plane of the distribution.
                      x and y can be *k* (k-vector direction), *s* (:math:`sigma` polarization direction) or *p* (:math:`pi` polarization direction).
       points (int): Number of points for the distribution. Only used for method *random*. Default is 401 points.

         .. versionchanged:: 1.2.0

       method (string): *random* for random selection of points, which are permuted and mirrored.
         *model* for selected points to average over the parameter after [Hasselbach]_.
       
         .. versionadded:: 1.2.0

       order (int): The order parameter determines the number of basis sets used in the spherical 3D approximation of the *average* method, not for the 2D one.
         For ``order=1`` 48 vectors are used, up to 1008 vectors for ``order=6``. Default is 1.
       
         .. versionadded:: 1.2.0

    """
    return _cnexus.Hyperfine_SetRandomDistribution(self, target, type, points, method, order)
%}



%include "hyperfine.h"



%extend Hyperfine{
  %pythoncode{
  def __setattr__(self, attr, val):
    if attr not in ("this",
                    "_weight",
                    "_isomer",
                    "_magnetic_field",
                    "_magnetic_theta",
                    "_magnetic_phi", 
                    "_quadrupole",
                    "_quadrupole_alpha",
                    "_quadrupole_beta",
                    "_quadrupole_gamma",
                    "_quadrupole_asymmetry",
                    "_texture",
                    "_lamb_moessbauer",
                    "_broadening",
                    "_isomer_dist",
                    "_magnetic_field_dist",
                    "_magnetic_theta_dist",
                    "_magnetic_phi_dist",
                    "_quadrupole_dist",
                    "_quadrupole_alpha_dist",
                    "_quadrupole_beta_dist",
                    "_quadrupole_gamma_dist",
                    "_quadrupole_asymmetry_dist",
                    "_isomer_dist_ptr",
                    "_magnetic_field_dist_ptr",
                    "_magnetic_theta_dist_ptr",
                    "_magnetic_phi_dist_ptr",
                    "_quadrupole_dist_ptr",
                    "_quadrupole_alpha_dist_ptr",
                    "_quadrupole_beta_dist_ptr",
                    "_quadrupole_gamma_dist_ptr",
                    "_quadrupole_asymmetry_dist_ptr"
                    ) and not hasattr(self, attr):
      raise Exception("Hyperfine does not have attribute {}".format(attr))

    if attr != "isotropic" and isinstance(val, (int, float)):
      setattr(getattr(self, attr), "value", val)
      return

    if attr == "weight":
      self._weight = val

    if attr == "isomer":
      self._isomer = val

    if attr == "magnetic_field":
      self._magnetic_field = val

    if attr == "magnetic_theta":
      self._magnetic_theta = val

    if attr == "magnetic_phi":
      self._magnetic_phi = val

    if attr == "quadrupole":
      self._quadrupole = val

    if attr == "quadrupole_alpha":
      self._quadrupole_alpha = val

    if attr == "quadrupole_beta":
      self._quadrupole_beta = val

    if attr == "quadrupole_gamma":
      self._quadrupole_gamma = val

    if attr == "quadrupole_asymmetry":
       self._quadrupole_asymmetry = val

    if attr == "texture":
       self._texture = val

    if attr == "lamb_moessbauer":
       self._lamb_moessbauer = val

    if attr == "broadening":
       self._broadening = val

    super().__setattr__(attr, val)

  def __repr__(self):
    self.Update()
    output = "Hyperfine .id: {}\n".format(self.id)
    output += "  .weight =               {}\n".format(self.weight.value)
    output += "  .isomer_shift =         {}    \t dist points: {}\n".format(self.isomer.value, len(self.isomer_dist))
    output += "  .magnetic_field =       {}    \t dist points: {}\n".format(self.magnetic_field.value, len(self.magnetic_field_dist))
    output += "  .magnetic_theta =       {}    \t dist points: {}\n".format(self.magnetic_theta.value, len(self.magnetic_theta_dist))
    output += "  .magnetic_phi =         {}    \t dist points: {}\n".format(self.magnetic_phi.value, len(self.magnetic_phi_dist))
    output += "  .quadrupole =           {}    \t dist points: {}\n".format(self.quadrupole.value, len(self.quadrupole_dist))
    output += "  .quadrupole_alpha =     {}    \t dist points: {}\n".format(self.quadrupole_alpha.value, len(self.quadrupole_alpha_dist))
    output += "  .quadrupole_beta =      {}    \t dist points: {}\n".format(self.quadrupole_beta.value, len(self.quadrupole_beta_dist))
    output += "  .quadrupole_gamma =     {}    \t dist points: {}\n".format(self.quadrupole_gamma.value, len(self.quadrupole_gamma_dist))
    output += "  .quadrupole_asymmetry = {}    \t dist points: {}\n".format(self.quadrupole_asymmetry.value, len(self.quadrupole_asymmetry_dist))
    output += "  .lamb_moessbauer =      {}\n".format(self.lamb_moessbauer.value)
    output += "  .broadening =           {}\n".format(self.broadening.value)
    output += "  .texture =              {}\n".format(self.texture.value)
    output += "  .isotropic =            {}    \t 3D distribution of site in mag and efg.\n".format(self.isotropic)
    output += "  random magnetic distribution: {}\t dist points: {}\n".format(self.distribution_type_magnetic, self.distribution_actual_points_magnetic)
    output += "  random magnetic method: {}\n".format(self.distribution_method_magnetic)
    output += "  random quadrupole distribution: {}\t dist points: {}\n".format(self.distribution_type_quadrupole, self.distribution_actual_points_quadrupole)
    output += "  random quadrupole method: {}\n".format(self.distribution_method_quadrupole)
    output += "  total number of distribution points: {}\n".format(len(self.BareHyperfines))
    return output

  def Copy(self, ref_dist = True):
    r"""
    Copy the Hyperfine object.
      
    Args:
       ref_dist (bool): Determines whether the distribution references should be copied as well (*True*) or not (*False*).
          The distributions itself are not copied, just the reference is passed to the new object.
          Changing values in the distributions will affect both the original and the copied :class:`Hyperfine` object.
          Hyperfine site nx.Distributions are not copied but use same reference.
    """
    new_hyperfine = Hyperfine(id = self.id, weight = self.weight.Copy(), isomer = self.isomer.Copy(), magnetic_field = self.magnetic_field.Copy(),
                              magnetic_theta = self.magnetic_theta.Copy(), magnetic_phi = self.magnetic_phi.Copy(), 
                              quadrupole = self.quadrupole.Copy(), quadrupole_alpha = self.quadrupole_alpha.Copy(), quadrupole_beta = self.quadrupole_beta.Copy(),
                              quadrupole_gamma = self.quadrupole_gamma.Copy(), quadrupole_asymmetry = self.quadrupole_asymmetry.Copy(),
                              texture = self.texture.Copy(), isotropic = self.isotropic)
    if ref_dist:
      new_hyperfine.SetIsomerDistribution(self.isomer_dist_ptr)
      new_hyperfine.SetMagneticFieldDistribution(self.magnetic_field_dist_ptr)
      new_hyperfine.SetMagneticThetaDistribution(self.magnetic_theta_dist_ptr)
      new_hyperfine.SetMagneticPhiDistribution(self.magnetic_phi_dist_ptr)
      new_hyperfine.SetQuadrupoleDistribution(self.quadrupole_dist_ptr)
      new_hyperfine.SetQuadrupoleAlphaDistribution(self.quadrupole_alpha_dist_ptr)
      new_hyperfine.SetQuadrupoleBetaDistribution(self.quadrupole_beta_dist_ptr)
      new_hyperfine.SetQuadrupoleGammaDistribution(self.quadrupole_gamma_dist_ptr)
      new_hyperfine.SetQuadrupoleAsymmetryDistribution(self.quadrupole_asymmetry_dist_ptr)

      new_hyperfine.SetRandomDistribution(self.distribution_target_magnetic, self.distribution_type_magnetic, self.distribution_points_magnetic,self.distribution_method_magnetic, self.distribution_order_magnetic)
      new_hyperfine.SetRandomDistribution(self.distribution_target_quadrupole, self.distribution_type_quadrupole, self.distribution_points_quadrupole, self.distribution_method_quadrupole, self.distribution_order_quadrupole)

    new_hyperfine.Update()
    return new_hyperfine
  }
}
