// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "forward_sample.h"

#include "../math/intensity.h"
#include "../math/matrix.h"



ForwardSample::ForwardSample(
  const std::vector<Layer*> layers_,
  const std::vector<double> drive_detuning_,
  FunctionTime* const function_time_,
  const std::string id_
) :
BasicSample(layers_, drive_detuning_, function_time_, NxObjectId::ForwardSample, id_)
{}

void ForwardSample::Update()
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  for (Layer* const lay : unique_layers)
    lay->Update();
}

void ForwardSample::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  if (function_time != nullptr)
  {
    for (Var* const var : function_time->fit_variables)
      fit_handler->PrepareFitVariable(var);
  }

  for (Layer* const lay : layers)
    lay->PopulateFitHandler(fit_handler);
}

double ForwardSample::EffectiveThickness() const
{
  double effective_thickness = 0.0;

  for (const Layer* const lay : layers)
  {
    effective_thickness += lay->material->isotope_number_density * lay->material->lamb_moessbauer->value
      * lay->material->isotope->nuclear_cross_section * lay->thickness->value * 1.0e-9;
  }

  return effective_thickness;
}

/* electronic */

// amplitudes

Complex ForwardSample::ElectronicAmplitude(const double energy)
{
  Complex amplitude(1.0, 0.0);

  for (Layer* const lay : layers)
    amplitude *= lay->ElectronicForwardTransmissionFactor(energy);

  return amplitude;
}

Eigen::Matrix2cd ForwardSample::ElectronicAmplitudeMatrix(const double energy)
{
  Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Zero();

  matrix(0, 0) = ElectronicAmplitude(energy);
  matrix(1, 1) = matrix(0, 0);

  return matrix;
}

Complex ForwardSample::ElectronicAmplitudeThickness(const double energy, const double thickness)
{
  Complex amplitude(1.0, 0.0);

  double thickness_buffer = thickness;

  for (Layer* const lay : layers)
  {
    if (thickness_buffer > lay->thickness->value)
    {
      amplitude *= lay->ElectronicForwardTransmissionFactor(energy);

      thickness_buffer -= lay->thickness->value;
    }
    else
    {
      amplitude *= lay->ElectronicForwardTransmissionFactor(energy, thickness_buffer);

      break;
    }

  }

  return amplitude;
}

std::vector<Complex> ForwardSample::ElectronicFieldAmplitude(const double energy, const size_t num_points)
{
  const double thickness_step = TotalThickness() / (num_points - 1.0);

  std::vector<Complex> amplitudes(num_points);

  for (size_t i = 0; i < num_points; ++i)
    amplitudes[i] = ElectronicAmplitudeThickness(energy, thickness_step * i);

  return amplitudes;
}

// intensities

double ForwardSample::ElectronicIntensity(const double energy)
{
  return std::norm(ElectronicAmplitude(energy));
}

double ForwardSample::ElectronicIntensityThickness(const double energy, const double thickness)
{
  return std::norm(ElectronicAmplitudeThickness(energy, thickness));
}

std::vector<double> ForwardSample::ElectronicFieldIntensity(const double energy, const size_t num_points)
{
  return intensity::Intensity(ElectronicFieldAmplitude(energy, num_points));
}

/* nuclear */

void ForwardSample::ScatteringMatrices(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid, const bool calc_transitions)
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  std::for_each(unique_layers.begin(), unique_layers.end(), [&](Layer* lay)
    {
      lay->forward_scattering_matrix = lay->ForwardScatteringMatrix(isotope, detuning_grid, calc_transitions);
    }
  );

}

// assumes that nuclear scattering matrices of all layer are already calculated
// call ForwardSample::ScatteringMatrices(isotope, detuning_grid, calc_transitions) before calling this function
void ForwardSample::PropagationMatrices(const MoessbauerIsotope* const isotope)
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  std::for_each(unique_layers.begin(), unique_layers.end(), [&](Layer* lay)
    {
      lay->forward_propagation_matrix = lay->ForwardPropagationMatrix(isotope, lay->forward_scattering_matrix);
    }
  );

}

// assumes that nuclear propagation matrices of all layer are already calculated
// call ForwardSample::PropagationMatrices(isotope) before calling this function
void ForwardSample::LayerMatrices()
{
  const std::vector<Layer*> unique_layers = FindUniqueLayers();

  std::for_each(unique_layers.begin(), unique_layers.end(), [](Layer* lay)
    {
      lay->forward_layer_matrix = lay->ForwardTransmissionMatrix(lay->forward_propagation_matrix);
    }
  );

}

ObjectMatrix2 ForwardSample::ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning_grid,
  const bool calc_transitions)
{
  ScatteringMatrices(isotope, detuning_grid, calc_transitions);

  PropagationMatrices(isotope);

  LayerMatrices();

  ObjectMatrix2 sample_matrix(detuning_grid.size(), Eigen::Matrix2cd::Identity());

  for (const Layer* const lay : layers)
    sample_matrix = matrix::Multiply(lay->forward_layer_matrix, sample_matrix);

  return sample_matrix;
}

/* thickness distribution */

// returns distribution_points to the power of number of layers
// before this function call ForwardPropagationMatrices have to be calculated
size_t ForwardSample::PopulateThicknessDistribution(const size_t distribution_points)
{
  size_t total_combinations = 1;

  for (Layer* const lay : layers)
  {
    lay->PopulateDistribution(distribution_points);

    total_combinations *= lay->thickness_distribution.size();
  }

  return total_combinations;
}

// increase the distribution index of each layer by cycling through layer and reset index if at end
void ForwardSample::IncreaseDistributionIndex(const size_t distribution_points)
{
  for (Layer* const lay : layers)
  {
    lay->distribution_index += 1;

    if (lay->distribution_index < distribution_points)
    {
      break;
    }
    else
    {
      lay->distribution_index = 0;
    }
  }
}

void ForwardSample::ResetDistributionIndex()
{
  for (Layer* const lay : layers)
    lay->ResetDistributionIndex();
}
