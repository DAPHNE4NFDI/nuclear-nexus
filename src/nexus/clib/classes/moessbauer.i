// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module moessbauer

%{
    #define SWIG_FILE_WITH_INIT
    #include "moessbauer.h"
%}

%feature("shadow") MoessbauerIsotope::MoessbauerIsotope %{
  def __init__(self,
               isotope = "none",
               element = "",
               mass = 0.0,
               energy = 0.0,
               lifetime = 0.0,
               internal_conversion = 0.0,
               multipolarity = _cnexus.Multipolarity_E1,
               mixing_ratio_E2M1 = 0.0,
               spin_ground = 0.0,
               spin_excited = 0.0,
               gfactor_ground = 0.0,
               gfactor_excited = 0.0,
               quadrupole_ground = 0.0,
               quadrupole_excited = 0.0,
               interference_term = 0.0,
               natural_abundance = 0.0):

    if isinstance(internal_conversion, (int, float)):
      internal_conversion = Var(internal_conversion, 0.0, 1.0e9, False, "")
      
    if isinstance(mixing_ratio_E2M1, (int, float)):
      mixing_ratio_E2M1 = Var(mixing_ratio_E2M1, -10.0, 10.0, False, "")

    if isinstance(gfactor_ground, (int, float)):
      gfactor_ground = Var(gfactor_ground, -5.0, 5.0, False, "")

    if isinstance(gfactor_excited, (int, float)):
      gfactor_excited = Var(gfactor_excited, -5.0, 5.0, False, "")

    if isinstance(quadrupole_ground, (int, float)):
      quadrupole_ground = Var(quadrupole_ground, -10.0, 10.0, False, "")

    if isinstance(quadrupole_excited, (int, float)):
      quadrupole_excited = Var(quadrupole_excited, -10.0, 10.0, False, "")

    if isinstance(interference_term, (int, float)):
      interference_term = Var(interference_term, -10.0, 10.0, False, "")

    self._internal_conversion = internal_conversion
    self._mixing_ratio_E2M1 = mixing_ratio_E2M1
    self._gfactor_ground = gfactor_ground
    self._gfactor_excited = gfactor_excited
    self._quadrupole_ground = quadrupole_ground
    self._quadrupole_excited = quadrupole_excited
    self._interference_term = interference_term

    _cnexus.MoessbauerIsotope_swiginit(self, _cnexus.new_MoessbauerIsotope(isotope,
                                                                           element,
                                                                           mass,
                                                                           energy,
                                                                           lifetime,
                                                                           internal_conversion,
                                                                           multipolarity,
                                                                           mixing_ratio_E2M1,
                                                                           spin_ground,
                                                                           spin_excited,
                                                                           gfactor_ground,
                                                                           gfactor_excited,
                                                                           quadrupole_ground,
                                                                           quadrupole_excited,
                                                                           interference_term,
                                                                           natural_abundance)
        )
%}



%include "moessbauer.h"



%extend MoessbauerIsotope{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this", "_internal_conversion", "_mixing_ratio_E2M1", "_gfactor_ground", "_gfactor_excited", "_quadrupole_ground", "_quadrupole_excited", "_interference_term") and not hasattr(self, attr):
      raise Exception("MoessbauerIsotope has no attribute {}".format(attr))

    if attr in ("internal_conversion", "mixing_ratio_E2M1", "gfactor_ground", "gfactor_excited", "quadrupole_ground", "quadrupole_excited", "interference_term", "natural_abundance") and isinstance(val, (int, float)):
      setattr(getattr(self, attr), "value", val)
      return

    if attr == "internal_conversion":
      self._internal_conversion = val

    if attr == "mixing_ratio_E2M1":
      self._mixing_ratio_E2M1 = val

    if attr == "gfactor_ground":
      self._gfactor_ground = val

    if attr == "gfactor_excited":
      self._gfactor_excited = val

    if attr == "quadrupole_ground":
      self._quadrupole_ground = val

    if attr == "quadrupole_excited":
      self._quadrupole_excited = val

    if attr == "interference_term":
      self._interference_term = val
    
    super().__setattr__(attr, val)

  @staticmethod
  def Template(moessbauer):
    return moessbauer.Copy()

  def __repr__(self):
    self.Update()
    output = "MoessbauerIsotope: {}\n".format(self.isotope)
    output += "  .element = {}\n".format(self.element)
    output += "  .mass (u) = {}\n".format(self.mass)
    output += "  .spin_ground = {}\n".format(self.spin_ground)
    output += "  .spin_excited = {}\n".format(self.spin_excited)
    output += "  .energy (eV) = {}\n".format(self.energy)
    output += "  .wavelength (m) = {:e}\n".format(self.wavelength)
    output += "  .kvector (1/m) = {:e}\n".format(self.kvector)
    output += "  .lifetime (s) = {:e}\n".format(self.lifetime)
    output += "  .half_lifetime (s) = {:e}\n".format(self.half_lifetime)
    output += "  .gamma (eV) = {:e}\n".format(self.gamma)
    output += "  .gamma_photon (eV) = {:e}\n".format(self.gamma_photon)
    output += "  .gamma_electron (eV) = {:e}\n".format(self.gamma_electron)
    output += "  .quality_factor = {:e}\n".format(self.quality_factor)
    output += "  .internal_conversion = {}\n".format(self.internal_conversion.value)
    output += "  .interference_term (beta) = {}\n".format(self.interference_term.value)
    mult = ""
    if self.multipolarity == _cnexus.Multipolarity_E1:
      mult = "E1"
    elif self.multipolarity == _cnexus.Multipolarity_E2:
      mult = "E2"
    elif self.multipolarity == _cnexus.Multipolarity_M1:
      mult = "M1" 
    elif self.multipolarity == _cnexus.Multipolarity_M1E2:
      mult = "M1E2"
    elif self.multipolarity == _cnexus.Multipolarity_M2:
      mult = "M2"
    output += "  .multipolarity = {} (L = {}, lambda = {}".format(mult, self.L, self.lambda1)
    if (self.multipolarity == _cnexus.Multipolarity_M1E2):
        output += " / L = {}, lambda = {}".format(self.L2, self.lambda2)
    output += ")\n"
    output += "  .mixing_ratio_E2M1 = {}\n".format(self.mixing_ratio_E2M1.value)
    if (self.multipolarity == _cnexus.Multipolarity_M1E2):
        output += "  .mixing_coefficient_1 (M1) = {}\n".format(self.mixing_coefficient_1)
        output += "  .mixing_coefficient_2 (E2) = {}\n".format(self.mixing_coefficient_2)
    output += "  .gfactor_ground = {}\n".format(self.gfactor_ground.value)
    output += "  .gfactor_excited = {}\n".format(self.gfactor_excited.value)
    output += "  .quadrupole_ground (barn) = {}\n".format(self.quadrupole_ground.value)
    output += "  .quadrupole_excited (barn) = {}\n".format(self.quadrupole_excited.value)
    output += "  .magnetic_moment_ground (eV/T) = {}\n".format(self.magnetic_moment_ground)
    output += "  .magnetic_moment_excited (eV/T) = {}\n".format(self.magnetic_moment_excited)
    output += "  .nuclear_cross_section (converted to kbarn) = {}\n".format(self.nuclear_cross_section*1e25)
    output += "  .natural_abundance = {}\n".format(self.natural_abundance)
    return output
  }
}
