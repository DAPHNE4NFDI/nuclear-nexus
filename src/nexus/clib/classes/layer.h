// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Layer class.
// It represents an infinitely extended physical layer of a certain material with a specific thickness.
// N layers build up a sample.


#ifndef NEXUS_LAYER_H_
#define NEXUS_LAYER_H_

#include <string>
#include <vector>
#include <cmath>
#include <complex>
#include <limits>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/nxvariable.h"
#include "../classes/optimizer_handler.h"
#include "../classes/moessbauer.h"
#include "../classes/material.h"
#include "../scattering/scattering_matrix.h"
#include "../scattering/propagation_matrix.h"
#include "../scattering/layer_matrix.h"


/**
Constructor for :class:`Layer` class.
When a material is provided, the composition and density arguments must be *None*.
If the Layer constructor should initialize the material as well, set :attr:`material` to *None* and specify :attr:`composition` and :attr:`density`.

Args:
   thickness (float or :class:`Var`): Thickness of the layer (nm).
   material (:class:`Material`): Material of the layer.
   roughness (float or :class:`Var`): Roughness (:math:`\sigma`) of the layer in grazing incidence geometry (nm).
   thickness_fwhm (float or :class:`Var`): FWHM of a thickness distribution of the layer for samples in forward geometry (nm).
      If set to zero no thickness distribution is taken into account. The user must specify a number of points used for the distribution in the corresponding :class:`Measurement` in order to take the thickness distribution into account.
   id (string): User identifier.
   composition (list): List of elements of the material to be created by the :class:`layer` constructor. See :attr:`material`.
   density (float or :class:`Var`): Density (g/cm :sup:`3`) of the :class:`Material` to be created by the :class:`Layer` constructor.

Attributes:
   id (string): User identifier.
   thickness (:class:`Var`): Thickness of the layer (nm).
   material (:class:`Material`): Material of the layer.
   roughness (:class:`Var`): Roughness (:math:`\sigma`) of the layer (nm).
   thickness_fwhm (float or :class:`Var`): FWHM of a thickness distribution of the layer for samples in forward geometry (nm).
      If set to zero no thickness distribution is taken into account. The user must specify a number of points used for the distribution in the corresponding :class:`Measurement` in order to take the thickness distribution into account.
   composition (list): List of elements of the material to be created by the :class:`layer` constructor. See :attr:`material`.
   density (:class:`Var`): Density (g/cm :sup:`3`) of the :attr:`material`.
*/
class Layer {
public:
	Layer(
		Material* const material,
		Var* const thickness,
		Var* const roughness,
		Var* const thickness_fwhm,
		const std::string id
	);

	void Update();

	void PopulateFitHandler(OptimizerHandler* fit_handler);

	// electronic - forward
	/** 
  Pure electronic forward scattering factor of the layer.
  Scalar version of Eq. (4.4), [Roehlsberger]_.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     complex: Electronic scattering factor of the layer.
	*/
	Complex ElectronicForwardScatteringFactor(const double energy);

	/**
  Pure electronic refractive index of the layer.
  Scalar version of Eq. (4.5), [Roehlsberger]_.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     complex: Refractive index of the layer.
  */
	Complex ElectronicRefractiveIndex(const double energy);


  /**
  Critical angle of the layer, see Eq. (4.22), [Roehlsberger]_.

  .. versionadded:: 1.0.3

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     float: Critical angle (rad).
  */
  double CriticalAngle(const double energy);
	
	/**
  Pure electronic transmission factor T of the layer.
  Scalar version of Eq. (4.16), [Roehlsberger]_.

  Args:
     energy (float): X-ray energy (eV).

  Returns:
     complex: Transmission factor of the layer.
	*/
	Complex ElectronicForwardTransmissionFactor(const double energy);

	Complex ElectronicForwardTransmissionFactor(const double energy, const double thickness);

	/**
  Pure electronic transmission of the X-ray intensity through the layer.
  Absolute of the scalar version of Eq. (4.16), [Roehlsberger]_.
 
  Args:
     energy (float): X-ray energy (eV).

  Returns:
     float: Electronic transmission of the layer.
  */
	double ElectronicForwardTransmission(const double energy);

	double ElectronicForwardTransmission(const double energy, const double thickness);
	// end electronic - forward
	
	// electronic - grazing
  /**
  Pure electronic grazing incidence scattering factor in grazing incidence geometry.
  Scalar version of Eq. (4.25), [Roehlsberger]_.

  Args:
     energy (float): X-ray energy (eV).
     angle (float): Incidence angle (degree). 

  Returns:
     complex: Electronic scattering factor of the layer.
	*/
	Complex ElectronicGrazingScatteringFactor(const double energy, const double angle);

	/**
  Pure electronic layer matrix *L*.
  2x2 matrix, Eq. (4.23), [Roehlsberger]_.

  Args:
     scattering_factor (complex): Complex scattering factor of the layer.
     kvector_z (float): k vector along z direction.

  Returns:
     list: List of complex 2x2 electronic layer matrices.
  */
	Eigen::Matrix2cd ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z);

	Eigen::Matrix2cd ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z, double thickness);
	// end electronic - grazing

	// nuclear - forward
	/**
  Forward scattering matrix *f* of the layer, Eq. (4.4), [Roehlsberger]_.
	
  Args:
     isotope (:class:`MoessbauerIsotope`): Resonant isotope.
     detuning (list or ndarray): Detuning values, in units of resonant isotope linewidth.

  Returns:
     list: List of complex 2x2 scattering matrices.
	*/
	ScatteringMatrix2 ForwardScatteringMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning, const bool calc_transitions);

	PropagationMatrix2 ForwardPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& f_matrix);

	LayerMatrix2 ForwardTransmissionMatrix(const PropagationMatrix2& F_matrix);

	LayerMatrix2 ForwardTransmissionMatrix(const PropagationMatrix2& F_matrix, const double thickness);
	
	//for Python access
	/**
  Forward propagation matrix *F* of the layer, Eq. (4.6), [Roehlsberger]_.

  Args:
     isotope (:class:`MoessbauerIsotope`): Resonant isotope.
     detuning (list or ndarray): Detuning values, in units of resonant isotope linewidth.

  Returns:
     list: List of complex 2x2 propagation matrices.
	*/
	PropagationMatrix2 ForwardPropagationMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning);

	/**
  Forward transmission matrix *T* of the layer, Eq. (4.16), [Roehlsberger]_.
	
  Args:
     isotope (:class:`MoessbauerIsotope`): Resonant isotope.
     detuning (list or ndarray): Detuning values, in units of resonant isotope linewidth.

  Returns:
     list: List of complex 2x2 layer matrices.
  */
	LayerMatrix2 ForwardTransmissionMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning);

	// end nuclear - forward
	
	// nuclear - grazing
	/**
  Scattering matrix *f* in grazing incidence geometry, Eq. (4.25), [Roehlsberger]_.

  Args:
     isotope (:class:`MoessbauerIsotope`): Resonant isotope.
     detuning (list or ndarray): Detuning values, in units of resonant isotope linewidth.
     angle (float): Incidence angle (degree).

  Returns:
     list: List of complex 4x4 propagation matrices.
	*/
	ScatteringMatrix2 GrazingScatteringMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
    const double angle, const bool calc_transitions);

	PropagationMatrix4 GrazingPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
    const double angle);

	LayerMatrix4 GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
    const double angle);

	//for Python access
	/**
  Propagation matrix *F* in grazing incidence geometry, Eq. (4.24), [Roehlsberger]_.

  Args:
     isotope (:class:`MoessbauerIsotope`): Resonant isotope.
     detuning (list or ndarray): Detuning values, in units of resonant isotope linewidth.
     angle (float): Incidence angle (degree).

  Returns:
     list: List of complex 4x4 propagation matrices.
	*/
	PropagationMatrix4 GrazingPropagationMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
    const double angle);

	/**
  Layer matrix *L* in grazing incidence geometry, Eq. (4.23), [Roehlsberger]_.

  Args:
     isotope (:class:`MoessbauerIsotope`): Resonant isotope.
     detuning (list or ndarray): Detuning values, in units of resonant isotope linewidth.
     angle (float): Incidence angle (degree).

  Returns:
     list: List of complex 4x4 layer matrices.
  */
	LayerMatrix4 GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
    const double angle);

	/**
  Calculates the product :math:`k_z \sigma` which should be << 1 for a valid roughness W matrix calculation in grazing incidence geometry, see [Roehlsberger]_.

  Args:
     energy (float): X-ray energy (eV).
     angle (float): Incidence angle (deg).

  Returns:
     float: :math:`k_z \sigma`.
	*/
	double KzSigma(const double energy, const double angle) const;
	// end nuclear - grazing


	Material* material = nullptr;

	mutable Var* thickness = nullptr;

  double thickness_store = 0;  // used as storage for incoherent calculations of distributions

	mutable Var* roughness = nullptr;

	mutable Var* thickness_fwhm = nullptr;

  std::string id = "";

	// redirection to material
	Var* density = material->density;
	
	Composition* composition = &(material->composition);

	// electronic - forward
	Complex forward_scattering_factor = std::numeric_limits<double>::quiet_NaN();

	Complex forward_refractive_index = std::numeric_limits<double>::quiet_NaN();

	Complex electronic_propagation_factor = std::numeric_limits<double>::quiet_NaN();

	Complex electronic_transmission_factor = std::numeric_limits<double>::quiet_NaN();

	// electronic - grazing
	Complex grazing_scattering_factor = std::numeric_limits<double>::quiet_NaN();

	Complex grazing_refractive_index = std::numeric_limits<double>::quiet_NaN();

	Eigen::Matrix2cd electronic_propagation_matrix{};

	Eigen::Matrix2cd electronic_layer_matrix{};

	// nuclear - forward
	ScatteringMatrix2 forward_scattering_matrix{};

	PropagationMatrix2 forward_propagation_matrix{};

	LayerMatrix2 forward_layer_matrix{};

	// nuclear - grazing
	ScatteringMatrix2 grazing_scattering_matrix{};

	PropagationMatrix4 grazing_propagation_matrix{};

	LayerMatrix4 grazing_layer_matrix{};

	// thickness distribution instances for forward module via incoherent addition
	std::vector<double> thickness_distribution{};

	std::vector<double> thickness_distribution_weight{};

	size_t distribution_index = 0;

	void PopulateDistribution(const size_t distribution_points);

  void ResetDistributionIndex();
};

#endif // NEXUS_LAYER_H_
