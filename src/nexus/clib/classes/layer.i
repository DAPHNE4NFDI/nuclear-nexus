// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module layer

%{
    #define SWIG_FILE_WITH_INIT
    #include "layer.h"
%}

%template(Layers) std::vector<Layer*>;

%ignore Layer::ElectronicForwardLayerFactor(const double energy, const double thickness);

%ignore Layer::ElectronicForwardTransmission(const double energy, const double thickness);

%ignore Layer::ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z, double thickness);

%ignore Layer::ForwardPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& f_matrix);

%ignore Layer::ForwardTransmissionMatrix(const PropagationMatrix2& F_matrix);

%ignore Layer::ForwardTransmissionMatrix(const PropagationMatrix2& F_matrix, const double thickness);

%ignore Layer::ForwardLayerMatrix(const MoessbauerIsotope* const isotope, const PropagationMatrix2& F_matrix);

%ignore Layer::ForwardLayerMatrix(const MoessbauerIsotope* const isotope, const PropagationMatrix2& F_matrix, const double thickness);

%ignore Layer::GrazingPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
		                                const double angle);

%ignore Layer::GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
		                          const double angle);

%ignore Layer::GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
		                          const double angle, const double thickness);


%feature("shadow") Layer::Layer %{
  def __init__(self,
               thickness,
               material = None,
               roughness = 0.0,
               thickness_fwhm = 0,
               id = "",
               composition = None,
               density = None):

    if isinstance(thickness, (int, float)):
      thickness = Var(thickness, 0.0, np.inf, False, "")

    if isinstance(roughness, (int, float)):
      roughness = Var(roughness, 0.0, np.inf, False, "")
    
    if thickness_fwhm == 0.0:
      thickness_fwhm = int(thickness_fwhm)
    if isinstance(thickness_fwhm, (int, float)):
      thickness_fwhm = Var(thickness_fwhm, 0.0, np.inf, False, "")
    if thickness_fwhm.value == 0.0:
      thickness_fwhm.value = int(thickness_fwhm.value)

    self._material = material
    self._thickness = thickness
    self._roughness = roughness
    self._thickness_fwhm = thickness_fwhm

    # for material definition in layer init
    if material and (composition or density):
      output = "-------------------------------------------------------------------------------------\n"
      output += " NEXUS ERROR: Conflict in Layer definition. Set material or composition and density.\n"
      output += "-------------------------------------------------------------------------------------"
      raise Exception(output)
    
    if not material and (not composition or not density):
      output = "-------------------------------------------------------------------------------------\n"
      output += " NEXUS ERROR: Conflict in Layer definition. Set composition and density.\n"
      output += "-------------------------------------------------------------------------------------"
      raise Exception(output)
    
    if not material and composition and density:
      if isinstance(density, (int, float)):
        density = Var(density, 0.0, 23.0, False, "")

      self._composition = composition
      composition = Composition(composition)

      self._density = density

      material = Material(composition, density)
      self._material = material
    
    _cnexus.Layer_swiginit(self, _cnexus.new_Layer(material, thickness, roughness, thickness_fwhm, id))
%}



%include "layer.h"



%extend Layer{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this", "_material", "_thickness", "_roughness", "_thickness_fwhm", "_composition", "_density") and not hasattr(self, attr):
      raise Exception("Layer does not have attribute {}".format(attr))

    if attr in ("thickness", "roughness", "thickness_fwhm", "density") and isinstance(val, (int, float)):
      setattr(getattr(self, attr), "value", val)
      return
   
    if attr == "composition":
      val = Composition(val)
      self._composition = val
      setattr(getattr(self, "material"), "composition", val)
      return

    if attr  == "material":
       self._material = val

    if attr == "thickness":
      self._thickness = val

    if attr == "roughness":
      self._roughness = val

    if attr == "thickness_fwhm":
      self._thickness_fwhm = val

    if attr == "density":
      self._density = val

    super().__setattr__(attr, val)

  def __repr__(self):
    self.Update()
    output = "Layer\n"
    output += "  .id: {}\n".format(self.id)
    output += "  .material.id: {}\n".format(self.material.id)
    output += "  .material.composition: "
    for i in range(len(self.material.composition)):
      output += " {} {},".format(self.material.composition[i][0], self.material.composition[i][1])
    output += "\n"
    output += "  .material.density (g/cm^3) {}\n".format(self.material.density)
    output += "  .thickness (nm) {}\n".format(self.thickness)
    output += "  .roughness (nm, sigma) {}\n".format(self.roughness)
    output += "  .thickness_fwhm (nm) {}\n".format(self.thickness_fwhm)
    return output

  }
}

/*
  def Copy(self, cmaterial = False, rhyperfine = True):
    if cmaterial:
      new_layer = Layer(material = self.material.Copy(rhyperfine), thickness = self.thickness.Copy(), roughness = self.roughness.Copy(), id = self.id)
    else:
      new_layer = Layer(material = self.material, thickness = self.thickness.Copy(), roughness = self.roughness.Copy(), id = self.id)
    new_layer.Update()
    return new_layer
*/