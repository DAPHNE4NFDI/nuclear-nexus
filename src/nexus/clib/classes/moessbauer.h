﻿// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Implementation of the MoessbauerIsotope class.
// It holds all isotope properties.
// The hyperfine interactions are not specific to the isotope and are defined in the material.
// The MossbauerIsotope also calculates additional derived parameters on construction to avoid recalculations in the code.
// In the module materials.Moessbauer, definitions for most relevant isotopes are given.
// Transition types
// E1: E:lambda = 1, 1: L = 1
// M1: M:lambda = 0, 1: L = 1 
// E2: E:lambda = 1, 2: L = 2
// M2: M:lambda = 0, 2: L = 2
// Only the multipolarity M1+E2 mixing occurs for Moessbauer transitions.
// Quadrupole moments (barn = 1e-28 1/m^2)


#ifndef NEXUS_MOESSBAUER_H_
#define NEXUS_MOESSBAUER_H_

#include "../nexus_definitions.h"
#include "../math/quantum.h"
#include "../classes/nxvariable.h"
#include "../classes/optimizer_handler.h"


// multipolarity definition of the transition
enum class Multipolarity { E1, M1, E2, M1E2, M2 };


/** 
Constructor for the :class:`MoessbauerIsotope` class.

Args:
   isotope (string): User identifier. Should be given in the format "mass number-element", e.g. "57-Fe".
   element (string): Element symbol, e.g. "Fe" for iron.
   mass (float): Isotope mass in unified atomic mass unit / Daltons (u).
   energy (float): Transition energy (eV).
   lifetime (float): Life time (seconds).

     .. versionchanged:: 1.2.0
        version < 1.2.0 in nanoseconds, since 1.2.0 in seconds.

   internal_conversion (float or :class:`Var`): Internal conversion factor :math:`\alpha`.

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   multipolarity (Multipolarity): Multipolarity of the transition, one of the following options

      * *nexus.Multipolarity_E1* (0)
      * *nexus.Multipolarity_M1* (1)
      * *nexus.Multipolarity_E2* (2)
      * *nexus.Multipolarity_M1E2* (3)
      * *nexus.Multipolarity_M2* (4)
   
      ..  versionchanged:: 1.0.3
          Multipolarity *M2* added.
   
   mixing_ratio_E2M1 (float or :class:`Var`): The mixing ratio :math:`\delta` of the *E2* and *M1* transitions. Note, that the definition of the sign is not consistent in literature.
     The sign convention here is such that for 193-Ir, the mixing coefficient is positive, although it is often found negative in literature.
     See [Sturhahn]_.

     "The E2/M1 mixing parameter :math:`\delta` is defined as the ratio of the reduced E2 and M1 matrix elements.
     Thus :math:`\delta^2` gives the relative intensities of E2 and M1 type radiation.
     The sign of :math:`\delta`, which describes the relative phase of the E2 and M1 waves, is a matter of definition and therefore inevitably a source of confusion."
     [Wagner]_.

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   spin_ground (float): Spin of the ground state :math:`I_g`.
   spin_excited (float): Spin of the excited state :math:`I_e`.
   gfactor_ground (float :class:`Var`): g-factor of the ground state :math:`g_g`.

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   gfactor_excited (float or :class:`Var`): g-factor of the excited state :math:`g_e`.

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   quadrupole_ground (float or :class:`Var`): Quadrupole moment of the ground state (barn).

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   quadrupole_excited (float or :class:`Var`): Quadrupole moment of the excited state (barn).

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   interference_term (float or :class:`Var`): Interference term of the nuclear and electronic currents (:math:`\beta`).

     .. versionchanged:: 1.2.0
        :class:`Var` input possible now.

   natural_abundance (float): Natural abundance of the isotope.

     .. versionadded:: 2.0.0


Attributes:
   isotope (string): User identifier. Should be given in the format "mass number-element", e.g. "57-Fe".
   element (string): Element symbol, e.g. "Fe" for iron.
   mass (float): Isotope mass in unified atomic mass unit / Daltons (u).
   energy (float): Transition energy (eV).
   lifetime (float): Lifetime of the state (seconds).
   internal_conversion (:class:`Var`): Internal conversion factor :math:`\alpha`.

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type, now :class:`Var`.

   multipolarity (Multipolarity): Multipolarity of the transition, one of the following options

      * *nexus.Multipolarity_E1* (0)
      * *nexus.Multipolarity_M1* (1)
      * *nexus.Multipolarity_E2* (2)
      * *nexus.Multipolarity_M1E2* (3)
      * *nexus.Multipolarity_M2* (4)

      ..  versionchanged:: 1.0.3
          Multipolarity *M2* added.
   
   L (int): Photon angular momentum. For mixed multipolarity *M1E2*, photon angular momentum of *M1*.
      
      * L=1 for *E1* and *M1* transitions
      * L=2 for *E2* and "M2" transitions

   lambda1 (int): Scattering type (electronic or magnetic). For mixed multipolarity *M1E2*, scattering type of *M1*.
   
      * :math:`\lambda = 1` for electronic *E1* and *E2* transition.
      * :math:`\lambda = 0` for magnetic *M1* and *M2* transition.

   L2 (int): For mixed multipolarity *M1E2*, photon angular momentum of *E2*, L=2.
   lambda2 (int): For mixed multipolarity *M1E2*, scattering type of *E2*, :math:`\lambda = 0`.
   mixing_ratio_E2M1 (:class:`Var`): The mixing ratio :math:`\delta` of the *E2* and *M1* transitions. Note, that the definition of the sign is not consistent in literature.
     The sign convention here is such that for 193-Ir, the mixing coefficient is positive, although it is often found negative in literature.
     See [Sturhahn]_.
     
     "The E2/M1 mixing parameter :math:`\delta` is defined as the ratio of the reduced E2 and M1 matrix elements.
     Thus :math:`\delta^2` gives the relative intensities of E2 and M1 type radiation.
     The sign of :math:`\delta`, which describes the relative phase of the E2 and M1 waves, is a matter of definition and therefore inevitably a source of confusion."
     [Wagner]_.

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type, now :class:`Var`.

   spin_ground (float): Spin of the ground state :math:`I_g`.
   spin_excited (float): Spin of the excited state :math:`I_e`.
   gfactor_ground (:class:`Var`): g-factor of the ground state :math:`g_g`.

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type, now :class:`Var`.

   gfactor_excited (:class:`Var`): g-factor of the excited state :math:`g_e`.

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type, now :class:`Var`.

   quadrupole_ground (:class:`Var`): Quadrupole moment of the ground state (kbarn).

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type, now :class:`Var`.

   quadrupole_excited (:class:`Var`): Quadrupole moment of the excited state (kbarn).

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type , now :class:`Var`.

   interference_term (:class:`Var`): Interference term of the nuclear and electronic currents (:math:`\beta`).

     .. versionchanged:: 1.2.0
        version < 1.2.0 float type , now :class:`Var`.

   atomic_number (int): Atomic (proton) number.
   wavelength (float): Wavelength of the transition energy (meter).
   kvector (float): k-vector of the transition energy (1/meter).
   half_lifetime (float): Half lifetime of the state (seconds).
   
     .. versionadded:: 1.0.3
   
   gamma (float): Transition linewidth (eV), :math:`\Gamma = \Gamma_{\gamma} + \Gamma_e`.
   gamma_photon (float): Partial transition linewidth by gamma emission (eV).
   
     .. versionadded:: 1.0.3
   
   gamma_electron (float): Partial transition linewidth by internal conversion (eV).

     .. versionadded:: 1.0.3

   quality_factor (float): Quality factor :math:`E/\Gamma` of the transition.

     .. versionadded:: 1.0.3

   nuclear_cross_section (float): Nuclear cross section (kbarn).
   magnetic_moment_ground (float): Magnetic moment of the ground state (eV/T).
     It is given by :math:`\mu_g = g_g \mu_N I_g`, where :math:`I_g` is given in units of :math:`\hbar`.
   magnetic_moment_excited (float): Magnetic moment of the excited state (eV/T).
     It is given by :math:`\mu_e = g_e \mu_N I_e`, where :math:`I_e` is given in units of :math:`\hbar`.
   natural_abundance (float): Natural abundance of the isotope.

     .. versionadded:: 2.0.0
*/
struct MoessbauerIsotope {
public:
  MoessbauerIsotope(
    const std::string isotope,
    const std::string element,
    const double mass,
    const double energy,
    const double lifetime,
    Var* const internal_conversion,
    const Multipolarity,
    Var* const mixing_ratio_E2M1,
    const double spin_ground,
    const double spin_excited,
    Var* const gfactor_ground,
    Var* const gfactor_excited,
    Var* const quadrupole_ground,
    Var* const quadrupole_excited,
    Var* const interference_term,
    const double natural_abundance
  );
  
  /**
  Copies the :class:`MoessbauerIsotope`.
  */
  MoessbauerIsotope Copy() const;

  void Update();

  void PopulateFitHandler(OptimizerHandler* fit_handler);

  std::string isotope = "";

  std::string element = "";
  
  double mass = 0.0;

  double energy = 0.0;

  double lifetime = 0.0;

  mutable Var* internal_conversion = nullptr;

  Multipolarity multipolarity = Multipolarity::E1;

  int L = 0 ;  // photon angular momentum
  int lambda1 = 0;  // scattering type (electronic or magnetic) of first multipolarity

  // E2 for mixed M1E2 multipolarity
  int L2 = 0;  // for mixed polarities (M1E2), photon angular momentum
  int lambda2 = 0;  // for mixed polarities (M1E2), scattering type

  mutable Var* mixing_ratio_E2M1 = nullptr;

  double spin_ground = 0.0;
  double spin_excited = 0.0;
  
  mutable Var* gfactor_ground = nullptr;
  mutable Var* gfactor_excited = nullptr;
  
  mutable Var* quadrupole_ground = nullptr;
  mutable Var* quadrupole_excited = nullptr;

  mutable Var* interference_term = nullptr;

  double natural_abundance = 0.0;

  // derived parameters
  int atomic_number = 0;
  
  double wavelength = 0.0;

  double kvector = 0.0 ;
  
  double half_lifetime = 0.0;

  double gamma = 0.0;
  double gamma_photon = 0.0;
  double gamma_electron = 0.0;

  double quality_factor = 0.0;
  
  double nuclear_cross_section = 0.0;
  
  double magnetic_moment_ground = 0.0;
  double magnetic_moment_excited = 0.0;
  
  int number_ground_states = 0;
  int number_excited_states = 0;
  
  int number_L = 0;
  int number_L2 = 0;
  
  double mixing_coefficient_1 = 1.0;
  double mixing_coefficient_2 = 0.0;

  double clebsch_gordon_Ig_magnetic = 0.0;
  double clebsch_gordon_Ie_magnetic = 0.0;
  
  double clebsch_gordon_Ig_quadrupole = 0.0;
  double clebsch_gordon_Ie_quadrupole = 0.0;

  quantum::VecSpherHarm YLM1 {};
  quantum::VecSpherHarm YLM2 {};
};

#endif // NEXUS_MOESSBAUER_H_
