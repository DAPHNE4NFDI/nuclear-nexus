// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module beam

%{
    #define SWIG_FILE_WITH_INIT
    #include "beam.h"
%}


%feature("shadow") Beam::Beam %{
def __init__(self, polarization = 0, mixing_angle = 0, canting_angle = 0, profile = "g", fwhm = 0, id = ""):

    if isinstance(polarization, (int, float)):
      polarization = Var(polarization, 0, 1, False, "")

    if isinstance(mixing_angle, (int, float)):
      mixing_angle = Var(mixing_angle, -45, 45, False, "")

    if isinstance(canting_angle, (int, float)):
      canting_angle = Var(canting_angle, -90, 90, False, "")

    if isinstance(fwhm, (int, float)):
      fwhm = Var(fwhm, 0.0, np.inf, False, "FWHM")

    self._polarization = polarization

    self._canting_angle = canting_angle

    self._mixing_angle = mixing_angle

    self._fwhm = fwhm

    _cnexus.Beam_swiginit(self, _cnexus.new_Beam(polarization, mixing_angle, canting_angle, profile, fwhm, id))
%}



%include "beam.h"



%extend Beam{
  %pythoncode{

  def __setattr__(self, attr, val):
    if attr not in ("this", "_fwhm", "_polarization", "_mixing_angle", "_canting_angle") and not hasattr(self, attr):
      raise Exception("Beam has no attribute {}".format(attr))

    if attr in ("polarization", "mixing_angle", "canting_angle", "fwhm"):
      if isinstance(val, (int, float)):
        setattr(getattr(self, attr), "value", val)
        return

    if attr == "polarization":
      self._polarization = val

    if attr == "mixing_angle":
      self._mixing_angle = val

    if attr == "canting_angle":
      self._canting_angle = val

    if attr == "fwhm":
      self._fwhm = val

    super().__setattr__(attr, val)


  def __repr__(self):
    self.Update()
    output = "Beam \n"
    output += "  .id: {}\n".format(self.id)
    output += "  .polarization (mm): {}\n".format(self.polarization)
    output += "  .mixing_angle (mm): {}\n".format(self.mixing_angle)
    output += "  .canting_angle (mm): {}\n".format(self.canting_angle)
    output += "  .matrix:\n{}\n".format(self.matrix)
    output += "  .jones_vector:\n{}\n".format(self.jones_vector)
    output += "  .profile: {}\n".format(self.profile)
    output += "  .fwhm (mm): {}\n".format(self.fwhm)
    output += "  .Polarization(): {}\n".format(self.Polarization())
    output += "  .Coherence(): {}\n".format(self.Coherence())
    output += "  .PhaseDifference() [sigma - pi](rad): {}".format(self.PhaseDifference())
    return output

  }
}
