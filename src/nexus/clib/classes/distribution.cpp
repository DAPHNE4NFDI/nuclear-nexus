// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "distribution.h"

#include <numeric>
#include <algorithm>

#include "../utilities/python_print.h"


Distribution::Distribution(
  const unsigned int points_,
  const std::string id_
) :
  points(points_),
  id(id_)
{
  if (points_ < 1)
    points = 1;
}


std::vector<double> Distribution::SetRange(const double range_)
{
  const double range = abs(range_);

  if (points > 1 &&
      range > 0.0)
  {
    delta.resize(points);

    const double border = -range / 2.0;

    const double step = range / (points - 1);

    for (size_t i = 0; i < points; ++i)
       delta[i] = (border + step * i);
  }
  else
  {
    delta.assign(0, 0.0);
  }

  return delta;
}


void Distribution::SetDelta(const std::vector<double> values)
{
  delta = values;
}


void Distribution::SetWeight(std::vector<double> values)
{
  const double norm = std::accumulate(values.begin(), values.end(), 0.0);

  std::for_each(values.begin(), values.end(),
    [norm](double& elem)
    {
      elem /= norm;
    }
  );

  weight = values;
}


void Distribution::PrintDelta() const
{
  python_print::output("delta values: ");

  for (const double& item : delta)
    python_print::output(std::to_string(item) + ", ", false);

  python_print::output("");
}


void Distribution::PrintWeight() const
{
  python_print::output("weight values: ");

  for (const double& item : weight)
    python_print::output(std::to_string(item) + ", ", false);

  python_print::output("");
}


void CallDistribution(Distribution* const dist)
{
  dist->DistributionFunction();
}
