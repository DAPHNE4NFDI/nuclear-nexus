// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// The class holds an effective layer instance for grazing incidence measurements.
// The class is used by the sample class to discretize the given layers into effective layers.

#ifndef NEXUS_EFF_DENS_MODEL_H_
#define NEXUS_EFF_DENS_MODEL_H_

#include <vector>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/moessbauer.h"
#include "../classes/layer.h"


/**
Constructor for the :class:`EffectiveLayer`. This class is automatically initialized by the :class:`Sample` class.

.. versionchanged:: 2.0.0

   initialized by :class:`GrazingSample`.

Args:
   thickness (float): Thickness of the effective layer
   roughness (float): Roughness of the effective layer.
   grazing_scattering_factor (complex): Electronic scattering factor.
   grazing_scattering_matrix (list): List of 2x2 matrices.

Attributes:
   thickness (float): Thickness of the effective layer
   roughness (float): Roughness of the effective layer.
   grazing_scattering_factor (complex): Electronic scattering factor.
   grazing_scattering_matrix (list): List of 2x2 matrices.
   grazing_propagation_matrix (list): List of 4x4 matrices.
   grazing_layer_matrix (list): List of 4x4 matrices.
   effective_layer_weight (list): Weight of the layer in th effective coordinate system.
*/
class EffectiveLayer {
public:
	EffectiveLayer(
		const double thickness = 0.0,
		const double roughness = 0.0,
		const Complex grazing_scattering_factor = Complex(0.0, 0.0),
		const ScatteringMatrix2 grazing_scattering_matrix = ScatteringMatrix2(0)
	) :
		thickness(thickness),
		roughness(roughness),
		grazing_scattering_factor(grazing_scattering_factor),
		grazing_scattering_matrix(grazing_scattering_matrix)
	{};

	// electronic
	Eigen::Matrix2cd ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z);

	Eigen::Matrix2cd ElectronicGrazingLayerMatrix(const Complex scattering_factor, const double kvector_z, const double input_thickness);

	// nuclear
	PropagationMatrix4 GrazingPropagationMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
		const double angle);
	
	LayerMatrix4 GrazingLayerMatrix(const MoessbauerIsotope* const isotope, const ScatteringMatrix2& grazing_f_matrix,
		const double angle);

	double thickness = 0.0;

	double roughness = 0.0;

	Complex grazing_scattering_factor = Complex(0.0, 0.0);

	ScatteringMatrix2 grazing_scattering_matrix{};
	
	PropagationMatrix4 grazing_propagation_matrix{};
	
	LayerMatrix4 grazing_layer_matrix{};

	std::vector<double> effective_layer_weight{};
};


namespace effdensmodel {

	std::vector<EffectiveLayer> ExtendedLayerSystem(const std::vector<Layer*> layers, const double total_thickness,
		const double eff_layer_thickness, const bool nuclear, double& sample_offset_top, double& sample_offset_bottom);

	std::vector<double> EffectiveLayerCoordinates(const double total_thickness, const double eff_layer_thickness);

	void CalculateLayerWeights(std::vector<EffectiveLayer>& extended_layer_system, const std::vector<double>& effective_layer_coordinates);

	std::vector<EffectiveLayer> EffectiveLayers(const std::vector<EffectiveLayer>& extended_layer_system,
		const double eff_layer_thickness, const size_t layer_coordinates_size);
}

#endif // NEXUS_EFF_DENS_MODEL_H_
