// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#include "material.h"

#include <map>
#include <algorithm>
#include <mutex>

#ifndef NEXUS_WITH_CLANG
#include <execution>
#endif


#include "../nexus_definitions.h"
#include "../math/constants.h"
#include "../classes/element_maps.h"

#include "../utilities/python_print.h"


Material::Material(
  const Composition composition_,
  Var* const density_,
  MoessbauerIsotope* const isotope_,
  Var* const abundance_,
  Var* const lamb_moessbauer_,
  const std::vector<Hyperfine*> hyperfine_sites_,
  const std::string id_
) :
  composition(composition_),
  density(density_),
  isotope(isotope_),
  abundance(abundance_),
  lamb_moessbauer(lamb_moessbauer_),
  hyperfine_sites(hyperfine_sites_),
  id(id_)
{
  Update();
};


void Material::Update()
{
  isotope->Update();

  UpdateComposition();

  UpdateHyperfineSites();
}

void Material::UpdateComposition()
{
  elements.clear();

  isotope_number_density = 0.0;

  // get sum composition amounts
  double sum = 0.0;

  for (const CompositionEntry& element : composition)
    sum += element.second;

  // average_mole_mass or atomic mass (u or g/mole)
  average_mole_mass = 0.0;

  for (const CompositionEntry& element : composition)
  {
    double mass = element_maps::atomic_mass_map.find(element.first)->second;  // averaged atomic mass over natural abundance

    // correct mass for given abundance of resonant isotope
    // X * M_resonant_isotope + Y M_other_isotopes = A (averaged atomic mass)
    // X + Y = 1
    // M_other_isotopes = (A - X * M_resonant_isotope) / (1 - X)
    if (element.first == isotope->element)
    {
      const double mass_non_res_isotopes = (mass - isotope->natural_abundance * isotope->mass) / (1.0 - isotope->natural_abundance);
      
      mass = abundance->value * isotope->mass + (1.0 - abundance->value) * mass_non_res_isotopes;
    }

    average_mole_mass += element.second / sum * mass;
  }

  // density in g/cm^3, convert to g/m^3 (1e6)
  // average_mole_mass is in g/mole
  total_number_density = constants::kNa * (density->value * 1.0e6) / average_mole_mass;  // 1/m^3

  for (const CompositionEntry& element : composition)
  {
    const double relative_amount = element.second / sum;

    const double number_density = relative_amount * total_number_density;

    if (element.first == isotope->element)
       isotope_number_density = abundance->value * number_density;

    elements.push_back(Element(element.first, number_density, relative_amount));
  }
}

void Material::UpdateHyperfineSites()
{
  // set lamb_moessbauer factor of material in not None 
  if (lamb_moessbauer != nullptr)
    for (const Hyperfine* hyperfine_site : hyperfine_sites)
      hyperfine_site->lamb_moessbauer = lamb_moessbauer;

  // calculate norm factor of all sites
  double norm = 0.0;

  for (const Hyperfine* hyperfine_site : hyperfine_sites)
    norm += hyperfine_site->weight->value;

  std::for_each(hyperfine_sites.begin(), hyperfine_sites.end(), [norm](Hyperfine* hyperfine_site)
    {
      hyperfine_site->weight->value /= norm;  // normalize all sites to sum of one

      hyperfine_site->Update();
    }
  );

}


std::vector<double> Material::GetRelativeWeights()
{
  std::vector<double> relative_weights(hyperfine_sites.size());

  double norm = 0.0;

  for (const Hyperfine* const hyperfine_site : hyperfine_sites)
    norm += hyperfine_site->weight->value;

  std::transform(hyperfine_sites.begin(), hyperfine_sites.end(), relative_weights.begin(), [norm](Hyperfine* hyperfine_site)
    {
      return hyperfine_site->weight->value /= norm;
    }
  );

  return relative_weights;
}


void Material::PopulateFitHandler(OptimizerHandler* fit_handler)
{
  isotope->PopulateFitHandler(fit_handler);

  fit_handler->PrepareFitVariable(density);

  fit_handler->PrepareFitVariable(abundance);

  fit_handler->PrepareFitVariable(lamb_moessbauer);

  for (Hyperfine* hyper : hyperfine_sites)
    hyper->PopulateFitHandler(fit_handler);
}


// parallelization of second most time consuming calculation in Nexus
void Material::CalcTransitions ()
{
  hyperfine_transitions.clear();
  
  std::mutex transitions_mutex;

  for (const Hyperfine* site : hyperfine_sites)
  {
    std::for_each(NEXUS_EXECUTION_POLICY site->BareHyperfines.begin(), site->BareHyperfines.end(), [&](const BareHyperfine& barehyperfine)
      {
        const transitions::Transitions site_transitions = transitions::HyperfineTransitions(barehyperfine, isotope);  // must be defined in the loop
      
        const std::lock_guard<std::mutex> writeLock(transitions_mutex);
      
        hyperfine_transitions.insert(hyperfine_transitions.end(), site_transitions.begin(), site_transitions.end());
      }
    );
  }
}
