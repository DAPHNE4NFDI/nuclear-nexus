// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the Fixed Object class
// A fixed object has a user-defined scattering factor and scattering matrix.


#ifndef NEXUS_FIXED_OBJECT_H_
#define NEXUS_FIXED_OBJECT_H_

#include <string>

#include <Eigen/Dense>

#include "../nexus_definitions.h"
#include "../classes/nxobject.h"

/**
Constructor for a :class:`FixedObject`. A :class:`FixedObject` is an object in the beam path without any energy dependence.
 
Args:
   scattering_factor (complex): Electronic scattering factor of the object.
   matrix (ndarray): Fixed (detuning-independent) 2x2 complex object matrix. Corresponds to a layer/sample/object matrix type.
   id (string): User identifier.

Attributes:
   scattering_factor (complex): Electronic scattering factor of the object.
   matrix (ndarray): Fixed (detuning-independent) 2x2 complex object matrix. Corresponds to a layer/sample/object matrix type.
   id (string): User identifier.
*/
class FixedObject : public NxObject {
public:
  FixedObject(
    const Complex scattering_factor,
    const Eigen::Matrix2cd& matrix,
    const std::string id = ""
  );
 
  // NxObject functions
  void Update() {};

  void PopulateFitHandler(OptimizerHandler* fit_handler) {};

  /**
  Returns the relative electronic amplitude behind the fixed object. Corresponds to the electronic scattering factor.

  Args:
     energy (float): no dependence

  Returns:
     complex: electronic scattering factor set during initialization
  */
  Complex ElectronicAmplitude(const double energy)
  {
     return scattering_factor;
  };

  /**
  Electronic scattering matrix of the fixed object.

  Args:
     energy (float): Not used.

  Returns:
     list: 2x2 scattering matrix.
  */
  Eigen::Matrix2cd ElectronicAmplitudeMatrix(const double energy)
  {
    return matrix;
  }

  /**
  Returns the detuning-independent object matrix. Corresponds to a layer matrix type.
  
  Args:
     isotope (MoessbauerIsotope): Moessbauer isotope of the experiment
     detuning (ndarray or list): n-dimensional array with detuning values
  
  Returns:
     LayerMatrix2: n-dimensional array of 2x2 complex arrays. All entries are the same for a fixed object.
  */
  ObjectMatrix2 ObjectMatrix(const MoessbauerIsotope* const isotope, const std::vector<double>& detuning,
    const bool calc_transitions = true);

  Complex scattering_factor = Complex(1.0, 0.0);

  Eigen::Matrix2cd matrix = Eigen::Matrix2cd::Identity();
};

#endif // NEXUS_FIXED_OBJECT_H_
