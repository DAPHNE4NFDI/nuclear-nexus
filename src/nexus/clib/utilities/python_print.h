// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of the output print function to python.
// header only file.


#ifndef NEXUS_PYTHONPRINT_H_
#define NEXUS_PYTHONPRINT_H_

#include <iostream>

#include "Python.h"

#include "../nexus_definitions.h"


namespace python_print {

  inline void output(std::string output_string, const bool endl = true)
  {
    if (endl)
      output_string = output_string + "\n";

    const char* nullterminated = output_string.c_str();

    PyObject* const sysmod = PyImport_ImportModuleNoBlock("sys");

    PyObject* const pystdout = PyObject_GetAttrString(sysmod, "stdout");

    PyObject* const result = PyObject_CallMethod(pystdout, "write", "s", nullterminated);

    Py_XDECREF(result);

    Py_XDECREF(pystdout);

    Py_XDECREF(sysmod);
  }

} //end namespace python_print

#endif // NEXUS_PYTHONPRINT_H_
