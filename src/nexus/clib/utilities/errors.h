// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Functions to handle errors and warnings
// Output to python


#ifndef NEXUS_NXERRORS_H_
#define NEXUS_NXERRORS_H_

#include "../nexus_definitions.h"
#include "../utilities/python_print.h"


namespace errors {

  inline void WarningMessage(std::string module_name, std::string warning_message)
  {
    python_print::output("-------------------------------------------------------------------------------------------");
    python_print::output(" NEXUS WARNING in " + module_name);
    python_print::output(" warning: " + warning_message);
    python_print::output("-------------------------------------------------------------------------------------------");
  }

  inline void ErrorMessage(std::string module_name, std::string warning_message)
  {
    python_print::output("-------------------------------------------------------------------------------------------");
    python_print::output(" NEXUS ERROR in " + module_name);
    python_print::output(" warning: " + warning_message);
    python_print::output("-------------------------------------------------------------------------------------------");
  }

} //end namespace error

#endif // NEXUS_NXERRORS_H_
