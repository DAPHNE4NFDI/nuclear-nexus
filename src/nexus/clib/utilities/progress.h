// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


#ifndef NEXUS_PROGRESS_H_
#define NEXUS_PROGRESS_H_

#include <iostream>

#include "../nexus_definitions.h"
#include "../utilities/python_print.h"


// Implementation of a progress bar in the cmd line output
// used for nuclear reflectivities
// header only file


namespace progress {

  constexpr const char* kProgressBarString = "............................................................";

  constexpr const int kProgressBarWidth = 60;

  inline void PrintProgress(const double ratio)
  {
    const int percentage = static_cast<int>(round(ratio * 100));

    const int lpad = static_cast<int>(ratio * kProgressBarWidth);

    const int rpad = kProgressBarWidth - lpad;

    printf("\r%3d%% [%.*s%*s]", percentage, lpad, kProgressBarString, rpad, "");

    fflush(stdout);
  }

} //end namespace error

#endif // NEXUS_PROGRESS_H_
