// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// File output functions.
// header only file.


#ifndef NEXUS_FILE_OUTPUT_H_
#define NEXUS_FILE_OUTPUT_H_

#include <filesystem>
#include <fstream>

#include "../nexus_definitions.h"


namespace file_output {

	inline std::filesystem::path increase_file_number(const std::filesystem::path file_path)
	{
		std::filesystem::path new_file_path{ file_path };

		// file_name.stem() + file_name.extension() == file_name.filename()
		// strip file_name of extensions
		std::string stem = new_file_path.stem().string();

		// file_name extension
		std::string file_name_extension = new_file_path.extension().string();

		for (int i = 1; std::filesystem::exists(new_file_path); ++i)
		{
			std::ostringstream file_stringstream;

			file_stringstream << stem << "_" << std::setw(3) << std::setfill('0') << i << file_name_extension;

			new_file_path.replace_filename(file_stringstream.str());
		}

		return new_file_path;
	}


	inline void write_file(const std::string file_name, const std::string file_string, const bool increase_number)
	{
		std::filesystem::path file_path = std::filesystem::current_path() / file_name;

		if (increase_number)
			file_path = increase_file_number(file_path);

		std::ofstream output_file(file_path);

	  output_file << file_string;

	  output_file.close();
	}

} //end namespace file_output

#endif // NEXUS_FILE_OUTPUT_H_
