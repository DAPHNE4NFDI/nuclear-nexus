// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module nexus_definitions

%{
    #define SWIG_FILE_WITH_INIT
    #include "nexus_definitions.h"
%}



%include "nexus_definitions.h"



%pythoncode %{
  InitGoogleLogging()  # needs to be called once for the ceres solver to log errors

  internal_angular_resolution = 0.001  # FWHM in deg
  r"""Definition of nexus standard angular resolution. Default is 0.001 degree."""

  internal_energy_resolution = 1.0  # FWHM in Gamma, for Moessbauer spectra, SMS. Do not take for EnergySpectra or EnergyTimeSpectra via NRS.
  r"""Definition of nexus standard energy resolution. Default is 1 Gamma."""

  internal_time_resolution = 0.7  # FWHM in ns
  r"""Definition of nexus standard time resolution. Default is 0.7 ns."""
%}