// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


// Definition of preprocessor directives, typdefs, etc. used in the whole code.


#ifndef NEXUS_DEFINITIONS_H_
#define NEXUS_DEFINITIONS_H_

// For the typical matrix sizes the direct implementation of Eigen is faster than MKL.
// Only for large matrices the increase in speed is considerable.
// #define EIGEN_USE_MKL_ALL // has to be included before Eigen headers! makes no sense here because EigenSolver does not use MKL.
// #define MKL_DIRECT_CALL  // this is also not faster
// Eigen is setup not to use MKL at all.
#define EIGEN_NO_DEBUG

// this is now shifted to the arguments of the compiler
// necessary in order to be compatible with Apple clang, which is not supporting std::execution 
//#define NEXUS_EXECUTION_POLICY std::execution::par, // multi-thread execution policy

// these definitions are now passed to the compiler
//#define NEXUS_USE_NLOPT;
//#define NEXUS_USE_PAGMO;

#include <complex>

#include <glog/logging.h> // not necessarily needed for Ceres, only for error tracking
#include <Eigen/Dense>

typedef std::complex<double> Complex;

// here the same definitions are repeated quite often
// this is done to keep better track of the object types used

// scattering matrix f
typedef std::vector<Eigen::Matrix2cd> ScatteringMatrix2;

// propagation matrix F
typedef std::vector<Eigen::Matrix2cd> PropagationMatrix2;
typedef std::vector<Eigen::Matrix4cd> PropagationMatrix4;

// Layer matrix L (also transmission matrix T in forward scattering)
typedef std::vector<Eigen::Matrix2cd> LayerMatrix2;
typedef std::vector<Eigen::Matrix4cd> LayerMatrix4;

// ObjectMatrix
// for a Sample it is the product of all layer matrices
typedef std::vector<Eigen::Matrix2cd> ObjectMatrix2;
typedef std::vector<Eigen::Matrix4cd> ObjectMatrix4;

// ExperimentMatrix
// product of all Object matrices
typedef std::vector<Eigen::Matrix2cd> ExperimentMatrix2;


// must be initialized once to track errors in the Ceres solver 
static void InitGoogleLogging()
{
  google::InitGoogleLogging("");
}


#endif // NEXUS_DEFINITIONS_H_
