// Nexus - the Nuclear Elastic X-ray scattering Universal Software package
// 
// Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
// A research centre of the Helmholtz Association.
// All rights reserved.
//
// Author: Lars Bocklage - lars.bocklage@desy.de
// 
// This file is part of Nexus.
// 
// Nexus is free software; you can redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
// 
// Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with Nexus.
// If not, see <https://www.gnu.org/licenses/>.


%module cnexus

%pythoncode %{
import numpy as np
import struct
import matplotlib.pyplot as plt
import matplotlib.colors as clr
%}


%include "typemaps.i"
%include "std_complex.i"
%include "std_vector.i"
%include "std_string.i"
%include "std_pair.i"
%include "std_map.i"

%include "swig_typemaps/eigen.i"
%include "swig_typemaps/numpy.i"

%init %{
  import_array();
%}


// numpy conversions
%numpy_typemaps(std::complex<double>, NPY_CDOUBLE, int)


// templates, define before eigen typemaps

%template(StringVector) std::vector<std::string>;

%template(DoubleVector) std::vector<double>;
%template(DoubleVectorVector) std::vector<std::vector<double>>;

%template(Complex) std::complex<double>;
%template(ComplexVector) std::vector<std::complex<double>>;

%template(CompositionEntry) std::pair<std::string, double>;

%feature("autodoc", "List of lists of *[[\"element symbol\" (string), relative amount (float)], ...]*.") std::vector<std::pair<std::string, double>>;

%template(Composition) std::vector<std::pair<std::string, double>>;

%feature("autodoc", "List of complex two element vectors.") std::vector<Eigen::Vector2cd>;

%template(DistributionReturn) std::pair<std::vector<double>, std::vector<double>>;

%template(AmplitudeVector) std::vector<Eigen::Vector2cd>;
//%template(AmplitudeVector2D) std::vector<std::vector<Eigen::Vector2cd>>; // leads to memory leaks, code rewritten to std::vector<Eigen::MatrixXcd>


%template(Matrix2) std::vector<Eigen::Matrix2cd>;
%template(Matrix4) std::vector<Eigen::Matrix4cd>;
%template(MatrixXd) std::vector<Eigen::MatrixXd>;
%template(MatrixXcd) std::vector<Eigen::MatrixXcd>;


// eigen typemaps
%eigen_typemaps(Eigen::VectorXd)
%eigen_typemaps(Eigen::Matrix<double, Eigen::Dynamic, 1>)
%eigen_typemaps(Eigen::Vector2cd)
%eigen_typemaps(Eigen::VectorXcd)
%eigen_typemaps(Eigen::Matrix<std::complex<double>, 2, 1>)

%eigen_typemaps(Eigen::Matrix3d)
%eigen_typemaps(Eigen::Matrix<double, 3, 3>)
%eigen_typemaps(Eigen::MatrixXd)
%eigen_typemaps(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>)

%eigen_typemaps(Eigen::Matrix2cd)
%eigen_typemaps(Eigen::Matrix4cd)
%eigen_typemaps(Eigen::MatrixXcd)
%eigen_typemaps(Eigen::Matrix<std::complex<double>, 2, 2>)
%eigen_typemaps(Eigen::Matrix<std::complex<double>, 4, 4>)
%eigen_typemaps(Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic>)

%feature("doxygen:notranslate");


// nexus definitions
%include nexus_definitions.i      # not documented for python

// math modules
//%include math/coordinates.i     # not included nor documented for python
//%include math/conversions.i     # not included nor documented for python
%include math/quantum.i
%include math/diagonalization.i
%include math/hamiltonian.i
%include math/transitions.i
%include math/fourier.i           # not documented for python
%include math/convolution.i       # not documented for python


// classes
%include classes/nxvariable.i
%include classes/equality.i
%include classes/function_time.i
%include classes/moessbauer.i
%include classes/distribution.i
%include classes/bare_hyperfine.i
%include classes/hyperfine.i
%include classes/element.i
%include classes/material.i
%include classes/layer.i
%include classes/eff_dens_model.i
%include classes/beam.i
%include classes/nxobject.i
%include classes/basic_sample.i
%include classes/forward_sample.i
%include classes/sample.i
%include classes/analyzer.i
%include classes/fixed_object.i
%include classes/conuss_object.i
%include classes/experiment.i        
%include classes/residual.i
%include classes/inequality.i

// modules with class dependencies
%include math/intensity.i                     # not documented for python
%include scattering/electronic_scattering.i
%include scattering/scattering_length.i
%include scattering/scattering_matrix.i
//%include scattering/propagation_matrix.i    # not included nor documented for python
//%include scattering/layer_matrix.i          # not included nor documented for python

// functors
%include functors/measurement.i               
%include functors/fit_measurement.i
%include functors/electronic.i
%include functors/electronic_transmission.i
%include functors/electronic_reflectivity.i
%include functors/energy_spectrum.i           
%include functors/time_spectrum.i
%include functors/energy_time_spectrum.i
%include functors/nuclear_reflectivity.i

// optimizer functors
%include classes/optimizer_options.i
%include classes/optimizer_handler.i           # not documented for python
%include functors/fit.i
%include functors/optimizer.i
