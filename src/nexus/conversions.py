# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Module for conversions of physical properties and units.
"""

import numpy as np

from nexus import clib
from nexus import constants
from nexus.lib import moessbauer


def SigmaToFwhm(sigma):
    r"""
    Returns the Full Width Half Maximum (FWHM) from Gaussian sigma value.
    """
    return 2.0 * np.sqrt(2.0 * np.log(2.0)) * sigma
    

def FwhmToSigma(fwhm):
    r"""
    Returns sigma of a Gaussian from its Full Width Half Maximum (FWHM).
    """
    return fwhm / 2.0 / np.sqrt(2.0 * np.log(2.0))


def DegToRad(degree):
    r"""
    Conversion from degree to radians.

    Args:
       degree (double): angle in degree.

    Returns:
       double: angle in rad.
    """
    return degree * np.pi / 180.0


def RadToDeg(rad):
    r"""
    Conversion from radians to degree.

    Args:
       rad (double): angle in rad.
       
    Returns:
       double: angle in degree.
    """
    return rad * 180.0 / np.pi


def DegToMrad(degree):
    r"""
    Conversion from degree to milliradians.
    
    Args:
       degree (double): angle in degree.

    Returns:
       double: angle in millirad.
    """
    return degree * np.pi / 180.0 * 1.0e3


def MradToDeg(mrad):
    r"""
    Conversion from milliradians to degree.

    Args:
       mrad (double): angle in millirad.
       
    Returns:
       double: angle in degree.
    """
    return mrad / 1.0e3 * 180.0 / np.pi


def DegToUrad(degree):
    r"""
    Conversion from degree to microradians.
    
    Args:
       degree (double): angle in degree.

    Returns:
       double: angle in microrad.
    """
    return degree * np.pi / 180.0 * 1.0e6


def UradToDeg(urad):
    r"""
    Conversion from microradians to degree.

    Args:
       urad (double): angle in microrad.
       
    Returns:
       double: angle in degree.
    """
    return urad / 1.0e6 * 180.0 / np.pi


def EnergyToKvector(energy): # in eV
    r"""
    Conversion from energy to photon k-vector.
    
    Args:
       energy (double): energy in eV.
       
    Returns:
       double: k-vector in 1/m.
    """
    kvector = energy / constants.HBar * constants.SpeedOfLight

    return kvector # in 1/m


def KvectorToEnergy(kvector): # in 1/m
    r"""
    Conversion from photon k-vector to energy.
    
    Args:
       kvector (double): k-vector in 1/m.
       
    Returns:
       double: energy in eV.
    """
    energy = constants.HBar * constants.SpeedOfLight * kvector

    return energy # in eV


def EnergyToWavelength(energy): # in eV
    r"""
    Conversion from energy to photon wavelength.
    
    Args:
       energy (double): energy in eV.
       
    Returns:
       double: wavelength in meter.
    """
    wavelength = constants.PlanckConstant * constants.SpeedOfLight / energy

    return wavelength # in meter


def WavelengthToEnergy(wavelength): # in m
    r"""
    Conversion from photon wavelength to energy.
    
    Args:
       wavelength (double): wavelength in meter.
       
    Returns:
       double:energy in eV.
    """
    energy = constants.PlanckConstant * constants.SpeedOfLight / wavelength
    
    return energy # in eV


def EnergyToKvectorZ(energy, angle): # in eV, deg
    r"""
    Conversion from energy to photon k-vector along z direction in grazing incidence direction.
    
    Args:
       energy (double): energy in eV.
       angle (double): angle in degree.
       
    Returns:
       double: k-vector along z direction in 1/m.
    """
    kvector_z = energy / (constants.HBar * constants.SpeedOfLight) * np.sin(angle * DegToRad(angle))
    
    return kvector_z # in 1/m


def VelocityToEnergy(velocity, isotope):
    r"""
    Convert a Doppler detuning from velocity to energy.

    Args:
       velocity (float or list or ndarray): velocity in mm/s.
       isotope (:class:`MoessbauerIsotope`): Moessbauer isotope of the experiment.

    Returns:
       float or list or ndarray: Energy detuning in eV.
    """
    delta_energy = velocity * 1.0e-3 / constants.SpeedOfLight * isotope.energy
    
    return delta_energy


def VelocityToGamma(velocity, isotope):
    r"""
    Convert a Doppler detuning from velocity to energy in units of the linewidth.

    Args:
       velocity (float or list or ndarray): Velocity in mm/s.
       isotope (:class:`MoessbauerIsotope`): Moessbauer isotope of the experiment.

    Returns:
       float or list or ndarray: Energy detuning in units of the linewidth (:math:`\Gamma`).
    """
    delta_energy = velocity * 1.0e-3 / constants.SpeedOfLight * isotope.energy / isotope.gamma
    
    return delta_energy


def EnergyToVelocity(energy, isotope): # mm/s
    r"""
    Convert a Doppler detuning from energy to velocity.

    .. versionchanged:: 1.2.0
       Conversion error corrected.

    Args:
       energy (float or list or ndarray): Energy in eV.
       isotope (:class:`MoessbauerIsotope`): Moessbauer isotope of the experiment.

    Returns:
       float or list or ndarray: Velocity detuning in mm/s.
    """
    velocity = energy / isotope.energy * constants.SpeedOfLight / 1.0e3
    
    return velocity


def GammaToVelocity(energy, isotope): # mm/s
    r"""
    Convert a Doppler detuning from energy in units of the linewidth to velocity.

    Args:
       energy (float or list or ndarray): Energy in units of the linewidth (:math:`\Gamma`).
       isotope (:class:`MoessbauerIsotope`): Moessbauer isotope of the experiment.

    Returns:
       float or list or ndarray: Velocity detuning in mm/s.
    """
    velocity = energy / isotope.energy * isotope.gamma * constants.SpeedOfLight / 1.0e-3
    
    return velocity


# Composition is a tuple of lists (or list of lists) with two entries.
# First entry is the element in string format. the second entry is its fraction in either at% or wt%.

atomicmasses = {
    # 1st period
    'H': 1.008, 'He': 4.002602,
    # 2nd period
    'Li': 6.94, 'Be': 9.0121831, 'B': 10.81, 'C': 12.011, 'N': 14.007, 'O': 15.999, 'F': 18.998403163,
    'Ne': 20.1797,
    # 3rd period
    'Na': 22.98976928, 'Mg': 24.305, 'Al': 26.9815384, 'Si': 28.085, 'P': 30.973761998, 'S':  32.06, 'Cl': 35.45,
    'Ar': 39.95,
    # 4th period
    'K': 39.0983, 'Ca': 40.078, 'Sc': 44.955908, 'Ti': 47.867, 'V': 50.9415, 'Cr': 51.9961, 'Mn': 54.938043,
    'Fe': 55.845, 'Co': 58.933194, 'Ni': 58.6934, 'Cu': 63.546, 'Zn': 65.38, 'Ga': 69.723, 'Ge': 72.630,
    'As': 74.921595, 'Se': 78.971, 'Br': 79.904, 'Kr': 83.798,
    # 5th period
    'Rb': 85.4678, 'Sr': 87.62, 'Y': 88.90584, 'Zr': 91.224, 'Nb': 92.90637, 'Mo': 95.95, 'Tc': 98.9062,
    'Ru': 101.07, 'Rh': 102.90549, 'Rd': 106.42, 'Ag': 107.8682, 'Cd': 112.414, 'In': 114.818, 'Sn': 118.710,
    'Sb': 121.760, 'Te': 127.60, 'I': 126.90447, 'Xe': 131.293,
    # 6th period
    'Cs': 132.90545196, 'Ba': 137.327, 'La': 138.90547, 'Ce': 140.116, 'Pr': 140.90766, 'Nd': 144.242, 'Pm': 145,
    'Sm': 150.36, 'Eu': 151.964, 'Gd': 157.25, 'Tb': 158.925354, 'Dy': 162.500, 'Ho': 164.930328, 'Er': 167.259,
    'Tm': 168.934218, 'Yb': 173.045, 'Lu': 174.9668, 'Hf': 178.486, 'Ta': 180.94788, 'W': 183.84, 'Re': 186.207,
    'Os': 190.23, 'Ir': 192.217, 'Pt': 195.084, 'Au': 196.966570, 'Hg': 200.592, 'Tl': 204.38, 'Pb': 207.2,
    'Bi': 208.98040, 'Po': 209, 'At': 210, 'Rn': 222,
    # 7th period
    # not included
}


def normalize_composition(composition):
    fraction_sum = np.sum([elem[1] for elem in composition])

    for elem in composition:
        elem[1] /= fraction_sum

    return composition


def AtToWt(composition):
    r"""
    Convert material composition given in at% to wt%.
    
    in atomic composition: composition = (['Fe', 2], ['O', 3]) for Fe :sub:`2` O :sub:`3`
    
    or in at%: composition = (['Fe', 0.4], ['O', 0.6])
    
    Args:
        composition (list): (['A', x], ['B', y], ...) where A, B, ... are element strings and x, y, ... is the fraction in atomic composition or at%.

    Returns:
        list: composition with x, y, ... in wt%
    """
    composition = normalize_composition(composition)

    div = np.sum([elem[1] * atomicmasses[elem[0]] for elem in composition])

    # calculate wt from at
    for elem in composition:
        elem[1] = elem[1] * atomicmasses[elem[0]] / div

    return composition


def WtToAt(composition):
    r"""
    Convert material composition given in wt% to at%.
    
    in wt%: composition = (['Fe', 0.699], ['O', 0.301]) for Fe :sub:`2` O :sub:`3`
    
    Args:
        composition (list): (['A', x], ['B', y], ...) where A, B, ... are element strings and x, y, ... is the fraction in wt%.

    Returns:
        list: composition with x, y, ... in at%
    """
    composition = normalize_composition(composition)

    div = np.sum([elem[1] / atomicmasses[elem[0]] for elem in composition])

    # calculate at from wt
    for elem in composition:
        elem[1] = elem[1] / atomicmasses[elem[0]] / div

    return composition


def MagVectorToSpherical(sigma, pi, k, angle = "deg"):
    r"""
    Calculates the magnetic field in spherical coordinates from an input vector in the *Nexus* frame (:math:`\sigma`, :math:`\pi`, k).

    .. versionadded:: 1.0.2

    Args:
        sigma (float): :math:`\sigma` component.
        pi (float): :math:`\pi` component.
        k (float): k component.
        angle (string): Units of the returned angles, "rad" or "deg".

    Returns:
        float, float, float: magnitude, polar angle, azimuthal angle.
    """
    mag = np.sqrt(sigma**2 + pi**2 + k**2)
    theta = np.arccos(k/mag)
    phi = np.arctan2(pi, sigma)

    if angle == "deg":
        theta *= 180.0 / np.pi
        phi *= 180.0 / np.pi

    return mag, theta, phi


def MagSphericalToVector(mag, theta, phi, angle = "deg"):
    r"""
    Calculates the magnetic field in *Nexus* frame (:math:`\sigma`, :math:`\pi`, k) to spherical coordinates.

    .. versionadded:: 1.0.2

    Args:
        mag (float): magnetic field :math:`\sigma` component.
        theta (float): polar angle.
        phi (float): azimuthal angle.
        angle (string): Units of the input angles, "rad" or "deg".

    Returns:
        float, float, float: :math:`\sigma` component, :math:`\pi` component, k component.
    """
    if angle == "deg":
        theta *= np.pi / 180.0
        phi *= np.pi / 180.0

    sigma = mag * np.sin(theta) * np.cos(phi)
    pi = mag * np.sin(theta) * np.sin(phi)
    k = mag * np.cos(theta)

    return sigma, pi, k


def AngleToQ(angle, energy):
    r"""
    Converts the grazing scattering angle to absolute of the :math:`q`-vector.

    .. versionadded:: 1.0.3

    Args:
        angle (float or list): angle(s) to be converted (rad).
        energy (float): X-ray energy (eV).
        
    Returns:
        float: absolute of the :math:`q`-vector (1/m).
    """
    return 4.0 * np.pi / EnergyToWavelength(energy) * np.sin(angle)


def QToAngle(q, energy):
    r"""
    Converts the absolute of the :math:`q`-vector to the grazing scattering angle.

    .. versionadded:: 1.0.3

    Args:
        q (float or list): absolute of the :math:`q`-vector(s) (1/m).
        energy (float): X-ray energy (eV).
        
    Returns:
        float: grazing scattering angle (rad).
    """
    return np.arcsin(q * EnergyToWavelength(energy) / (4.0 * np.pi))
