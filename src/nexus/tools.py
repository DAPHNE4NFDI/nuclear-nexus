# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Module for tools.
"""

from nexus import clib
import numpy as np


def Mask(x, y, start, end):
    r"""
    Mask two arrays to a certain range.
    Both arrays are reduced to the start/end range, which is in units of the x array.

    .. versionadded:: 1.2.0
    
    Args:
       x (array): x array to be masked.
       y (array): y array to be masked.
       start (float): Start of the mask in units of x.
       end (float): End of the mask in units of x.
       
    Returns:
       array, array: Masked x array, masked y array.
    """
    mask = (x >= start) & (x <= end)
    
    x_masked = np.array(x)[mask]
    
    y_masked = np.array(y)[mask]

    return x_masked, y_masked


def CountRateEstimator(photon_flux, time, intensity, isotope, time_start=0, time_end=np.inf, detector_efficiency=1, attenuation=1):
    r"""
    Estimate the photon count rate of a time spectrum.

    .. versionadded:: 1.2.0
    
    Args:
       photon_flux (float): Number of photons per energy bandwidth and time. In photons/meV/s.
       time (array): Array of time from a :class:`TimeSpectrum`. In ns.
       intensity (array): Array of intensities from a :class:`TimeSpectrum`. In :math:`\Gamma`/ns.
       isotope (:class:`MoessbauerIsotope`): Moessbauer isotope for the calculation.
       time_start (float): Time where the integration over the radiant flux starts in the time spectrum. In ns.
         Default is ``0``.
         Should be set to the veto time of a time spectrum.
       time_stop (float): Time where the integration over the radiant flux stops in the time spectrum. In ns.
         Default in ``inf`` meaning that time spectrum up to the last time provided.
         Should be set to the end of the measurement time of a histogram.
       detector_efficiency (float): Efficiency of the detector. Default is ``1``.
       attenuation (float): Attenuation of the beam. Default is ``1``.
         This includes attenuator, air, etc. Can also be applied in the TimeSpectrum calculation otherwise.
       
    Returns:
       float: Estimated photon count rate (in photons per second).
    """
    time_masked, intensity_masked = Mask(time, intensity, time_start, time_end)
    
    integral = np.trapz(intensity_masked, time_masked)
    
    return photon_flux * isotope.gamma * 1.0e3 * integral * detector_efficiency * attenuation  # 1.0e3 from eV to meV


def GetSiteSpectra(spectrum, sample):
    r"""
    Returns the spectra of single Hyperfine sites of a sample.

    .. versionadded:: 1.2.0
    
    Args:
        spectrum (:class:`EnergySpectrum` or :class:`EmissionSpectrum`): The EnergySpectrum, EmissionSpectrum or MoessbauerSpectrum of the calculation.
        sample (:class:`Sample`): The sample for which the hyperfine site spectra should be calculated.

    Returns:
        array: Returns a 2D list of intensity values, where the first index represents the layer and the second index the site in this layer.
    """
    lay_list = []
    
    for lay in sample.layers:
        mat = clib.Material(id = "plot material",
                            composition = lay.material.composition,
                            density = lay.material.density,
                            isotope = lay.material.isotope,
                            abundance = lay.material.abundance.Copy(),
                            lamb_moessbauer = lay.material.lamb_moessbauer,
                            hyperfine_sites = [clib.Hyperfine()]
                            )

        lay = clib.Layer(id = "plot layer",
                                material = mat,
                                thickness = lay.thickness,
                                roughness = lay.roughness,
                                thickness_fwhm = lay.thickness_fwhm
                            )
        
        lay_list.append(lay)

    plot_sample = clib.Sample(id = "plot sample",
                              layers = lay_list,
                              geometry = sample.geometry,
                              angle = sample.angle,
                              length = sample.length,
                              roughness = sample.roughness,
                              divergence = sample.divergence,
                              effective_thickness = sample.effective_thickness,
                              drive_detuning = sample.drive_detuning,
                              function_time = sample.function_time
                              )

    obj_list = []

    for obj in spectrum.experiment.objects:
        if obj.GetAddress() == sample.GetAddress():
            obj_list.append(plot_sample)
        else:
            obj_list.append(obj)
        
    plot_exp = clib.Experiment(beam = spectrum.experiment.beam,
                               objects = obj_list,
                               isotope = spectrum.experiment.isotope,
                               id = "plot experiment")

    plot_spectrum = clib.EnergySpectrum(experiment = plot_exp,
                                        detuning = spectrum.detuning,
                                        electronic = spectrum.electronic,
                                        scaling = spectrum.scaling,
                                        background = spectrum.background,
                                        resolution = spectrum.resolution,
                                        distribution_points = spectrum.distribution_points,
                                        fit_weight = spectrum.fit_weight,
                                        kernel_type = spectrum.kernel_type,
                                        residual = spectrum.residual,
                                        id = "plot spectrum")

    intensity_list = []

    for plot_lay in plot_sample.layers:
        plot_lay.material.hyperfine_sites = []
    
    for lay, plot_lay in zip(sample.layers, plot_sample.layers):
        layer_int = []
        
        for hyp_site in lay.material.hyperfine_sites:
            h = hyp_site.Copy(True)
            
            plot_lay.material.hyperfine_sites = [h]
            plot_lay.material.abundance.value *= h.weight.value
            
            layer_int.append(plot_spectrum())

            plot_lay.material.hyperfine_sites = []
            plot_lay.material.abundance.value /= h.weight.value

        intensity_list.append(layer_int)
    
    return intensity_list



def AreaSites(spectrum, sample, norm=False):
    r"""
    Returns the area of the single Hyperfine sites of a sample in units of linewidth (Gamma).

    .. versionadded:: 1.2.0
    
    Args:
        spectrum (:class:`EnergySpectrum` or :class:`EmissionSpectrum`): The EnergySpectrum, EmissionSpectrum or MoessbauerSpectrum of the calculation.
        sample (:class:`Sample`): The sample for which the hyperfine site spectra should be calculated.
        norm (bool): Normalize to relative contribution per site area. Default is `False`.

    Returns:
        array: Returns a 2D list of relative areas, where the first index represents the layer and the second index the site in this layer.
    """

    intensity_sites = GetSiteSpectra(spectrum, sample)

    area_list = []

    for lay in intensity_sites:
        areas_lay = []
        
        for site_int in lay:
            if isinstance(spectrum, clib.EmissionSpectrum):
                base_value = np.min(site_int)
            else:
                base_value = np.max(site_int)
            
            base = np.full_like(site_int, base_value)
            
            area = np.abs(np.trapz(base, spectrum.detuning) - np.trapz(site_int, spectrum.detuning))

            areas_lay.append(area)

        area_list.append(areas_lay)

    area_list = np.array(area_list)

    if norm:
        area_sum = np.sum(area_list)

        area_list_norm = []

        for lay_area in area_list:
            areas_lay = []

            for site_area in lay_area:
                areas_lay.append(site_area / area_sum)
            
            area_list_norm.append(areas_lay)

        area_list = np.array(area_list_norm)
                
    return area_list