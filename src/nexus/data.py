# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Module for data handling.
"""

from nexus import clib
from nexus.lib import distribution
from nexus.lib import material
from nexus.lib import moessbauer

import numpy as np

from scipy.optimize import least_squares
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy.signal import find_peaks
from scipy.interpolate import CubicSpline, PchipInterpolator, Akima1DInterpolator

from pybaselines.polynomial import poly
from pybaselines import Baseline as pyBase


def Save(file_name, data_list):
    r"""
    Save data arrays as columns to file.
    
    Args:
       file_name (string): File to load (with relative path).
       data_list (list): List containing the data to save column wise.
    """
    np.savetxt(file_name, np.column_stack(data_list))


def Load(file_name, x_index, intensity_index, x_start = None, x_stop = None, intensity_threshold = None):
    r"""
    Load data arrays from columns in file.
    
    Args:
       file_name (string): File to load (with relative path).
       x_index (int): Column index in file for x axis data.
       intensity_index (int): Column index in file for intensity data.
       x_start (float): x data smaller than x_start are not considered.
       x_stop (float): x data larger than x_stop are not considered.
       intensity_threshold (float): intensity data <= intensity_threshold are not considered.

    Returns:
       ndarray, ndarray: x_data, intensity_data.
    """
    data = np.loadtxt(file_name)
    
    x = data[:,x_index]
    intensity = data[:,intensity_index]

    mask_x_start = np.ones(len(x))
    if x_start != None:
        mask_x_start = np.where(x < x_start, False, True)
    
    mask_x_stop = np.ones(len(x))
    if x_stop != None:
        mask_x_stop = np.where(x > x_stop, False, True)
    
    mask_int = np.ones(len(intensity))
    if intensity_threshold != None:
        mask_int = np.where(intensity <= intensity_threshold, False, True)

    mask = mask_x_start * mask_x_stop * mask_int
    
    x = x[np.nonzero(mask)]

    intensity = intensity[np.nonzero(mask)]
    
    return x, intensity


def LoadFile(file_name, skip_rows=2):
    r"""
    Load a file with data saved in a 1D or 2D format and outputs to a 1D data set.

    .. versionadded:: 1.2.0
    
    Args:
       file_name (string): mos file to load (with relative path).
       skip_rows (int): number of rows to skip. Default is 2.

    Returns:
       ndarray: 1D data
    """
    data = np.loadtxt(file_name, skiprows=skip_rows)
    
    data = np.reshape(data, -1)
    
    return data


def HistogramStdDev(data):
    r"""
    Determines the standard deviation of histogram data, which is given by the square root of the measured histogram value.
    
    For Poisson statistics the expectation value (here the one single measured value in a data set) and the variance are the same.
    Thus, the variance is :math:`Var = y = \sigma_y^2`, with the measured value :math:`y`.
    The standard deviation is :math:`\sigma_y = \sqrt{y}`.
    """
    return np.sqrt(data)


def Binning(data, bin=2, method="sum" , endpoint=False):
    r"""
    Performs a binning/bucketing procedure on the data array with the given bin size.
    Either the sum or the mean of the binned data points can be calculated.
    If data array cannot be divided into equal sizes the endpoint can be discarded or binned with smaller size.

    Args:
      data (list): Data to be folded.
      bin (int): Bin size, the number of points to be processed.
      method (string): Either ``sum`` or ``mean``.
      endpoint (bool): Determines the endpoint treatment in case the array length is not integer dividable by bin.
        If ``False`` the last points are discarded.
        If ``True`` the bin of the last points equals the number of points left.
        It will result in non-equidistant points on the x-axis.

    Returns:
      ndarray: Binned data.
    """
    if bin == 1:
        return data
    
    if method not in ["sum", "mean"]:
        raise Exception("method not recognized")
    
    if endpoint:
        points = int(np.ceil(len(data) / bin))
    else:
        points = int(np.floor(len(data) / bin))
        
    bin_data = np.zeros(points)
    
    for i in range(len(bin_data)):
        step = i * bin
        
        elem = data[step:step+bin]
        
        if method == "sum":
            bin_data[i] = np.sum(elem)
        elif method == "mean":
            bin_data[i] = np.mean(elem)
    
    return bin_data


def Fold(data, folding_point, flip='left'):
    r"""
    Folds a one dimensional data array at the given folding point.
    The given array is cut in half.
    Data of the second half are rolled and re-introduced on the other side to ensure equal intensities.
    Give an even number of data points.

    The central shift depends on the folding direction (left or right).
    The folding direction depends on the experimental setup.
    Measure the second order Doppler shift to determine the correct folding.

    .. versionchanged:: 1.0.2

    Args:
      data (list): Data to be folded. Number of data points has to be even.
      folding_point (int): Folding point of the array.
      flip (string): determines whether the left or right part of the spectrum is flipped.
        
        .. versionadded:: 1.0.2

    Returns:
      ndarray: Folded data.
    """
    num_points = len(data) / 2

    split_data = np.hsplit(np.array(data), 2)

    if flip == 'right':
        left_data = split_data[0]

        right_data = np.flip(split_data[1])
    elif flip == 'left':
        left_data = np.flip(split_data[0])

        right_data = split_data[1]
    
    shift_value = int(folding_point - num_points)

    right_data = np.roll(right_data, shift_value)
    
    return left_data + right_data


def Fold_Interpolate(data, flip='left', factor=1, method="linear", lag=0.0):
    r"""
    Folds a one dimensional data array.
    The given data array is cut in half.
    Data of the flipped half are rolled (and so re-introduced on the other side) to ensure equal intensities.
    Give an even number of data points.
    The lag between the two points defines how far the two arrays are shifted in terms of channels.
    The precission must match the factor. So X.X1 must have a factor of 100.

    .. versionadded:: 2.0.0

    The central shift depends on the folding direction (left or right).
    The folding direction depends on the experimental setup.
    Measure the second order Doppler shift to determine the correct folding.

    Args:
      data (list): Data to be folded. Number of data points has to be even.
      flip (string): determines whether the left or right part of the spectrum is flipped.
      factor (int): the interpolation factor. For 1 the original data are used. For higher values the data are interpolated by a times factor finer grid than the input data.
      method (string): interpolation method.
      
        * ``linear``: Linear interpolation.
        * ``cubic``: Cubic spline interpolation.
        * ``PChip``: PCHIP 1-D monotonic cubic interpolation.
        * ``Akima``: Akima interpolation.
    
    lag (float): Number of channels to be shifted.
    
    Returns:
      ndarray: Folded data.
    """
    split_data = np.hsplit(np.array(data), 2)

    if flip == 'right':
        left_data = split_data[0]

        right_data = np.flip(split_data[1])
    elif flip == 'left':
        left_data = np.flip(split_data[0])

        right_data = split_data[1]

    # interpolation
    xp = np.arange(0, left_data.size)
    x = np.arange(0, left_data.size, 1/factor)
    
    if method == "linear":
        left_data_interpol = np.interp(x, xp, left_data)
        right_data_interpol = np.interp(x, xp, right_data)
    elif method == "cubic":
        interpolator_left = CubicSpline(xp, left_data, bc_type='clamped')
        interpolator_right = CubicSpline(xp, right_data, bc_type='clamped')

        left_data_interpol = interpolator_left(x)
        right_data_interpol = interpolator_right(x)
    elif method == "PChip":
        interpolator_left = PchipInterpolator(xp, left_data)
        interpolator_right = PchipInterpolator(xp, right_data)

        left_data_interpol = interpolator_left(x)
        right_data_interpol = interpolator_right(x)
    elif method == "Akima":
        interpolator_left = Akima1DInterpolator(xp, left_data, extrapolate=True)
        interpolator_right = Akima1DInterpolator(xp, right_data, extrapolate=True)

        left_data_interpol = interpolator_left(x)
        right_data_interpol = interpolator_right(x)
    else:
        raise Exception("method not recognized.")

    right_data_interpol = np.roll(right_data_interpol, lag * factor)
    
    folded_data_interpol = left_data_interpol + right_data_interpol

    # calculate intensity on original grid
    folded_data = folded_data_interpol.reshape(-1, factor).mean(axis=1)

    return folded_data


def AutoFold(data, flip='left', factor=1, method="linear", extra_data=[]):
    r"""
    Folds a one dimensional data array.
    Uses autocorrelation to determine the correct shift between the spectra.
    The given data array is cut in half.
    Data of the flipped half are rolled (and so re-introduced on the other side) to ensure equal intensities.
    Give an even number of data points.
    Returns the folded data and the lag, which describes how much the two parts are shifted.

    The central shift depends on the folding direction (left or right).
    The folding direction depends on the experimental setup.
    Measure the second order Doppler shift to determine the correct folding.

    .. versionchanged:: 1.2.0

       now returns the lag instead of the folding point.

    Args:
      data (list): Data to be folded. Number of data points has to be even.
      flip (string): determines whether the left or right part of the spectrum is flipped.
      
        .. versionadded:: 1.0.2

      factor (int): the interpolation factor. For 1 the original data are used. For higher values the data are interpolated by a times factor finer grid than the input data.
      
        .. versionadded:: 1.2.0

      method (string): interpolation method.
      
        * ``linear``: Linear interpolation.
        * ``cubic``: Cubic spline interpolation.
        * ``PChip``: PCHIP 1-D monotonic cubic interpolation.
        * ``Akima``: Akima interpolation.
        
        .. versionadded:: 1.2.0

      extra_data (list): List of data sets that are folded in the same way as the provided :attr:`data` for which the folding is performed.

        .. versionadded:: 2.0.0

    Returns:
      ndarray, int, list of folded extra_data: folded data, lag, list
    """
    split_data = np.hsplit(np.array(data), 2)

    if flip == 'right':
        left_data = split_data[0]

        right_data = np.flip(split_data[1])
    elif flip == 'left':
        left_data = np.flip(split_data[0])

        right_data = split_data[1]

    # interpolation
    xp = np.arange(0, left_data.size)
    x = np.arange(0, left_data.size, 1/factor)
    
    if method == "linear":
        left_data_interpol = np.interp(x, xp, left_data)
        right_data_interpol = np.interp(x, xp, right_data)
    elif method == "cubic":
        interpolator_left = CubicSpline(xp, left_data, bc_type='clamped')
        interpolator_right = CubicSpline(xp, right_data, bc_type='clamped')

        left_data_interpol = interpolator_left(x)
        right_data_interpol = interpolator_right(x)
    elif method == "PChip":
        interpolator_left = PchipInterpolator(xp, left_data)
        interpolator_right = PchipInterpolator(xp, right_data)

        left_data_interpol = interpolator_left(x)
        right_data_interpol = interpolator_right(x)
    elif method == "Akima":
        interpolator_left = Akima1DInterpolator(xp, left_data, extrapolate=True)
        interpolator_right = Akima1DInterpolator(xp, right_data, extrapolate=True)

        left_data_interpol = interpolator_left(x)
        right_data_interpol = interpolator_right(x)
    else:
        raise Exception("method not recognized.")

    left_data_interpol_corr = left_data_interpol - np.mean(left_data_interpol)
    right_data_interpol_corr = right_data_interpol - np.mean(right_data_interpol)
    
    correlation = np.correlate(left_data_interpol_corr, right_data_interpol_corr, mode = 'same')

    correlation_lag = int(correlation.argmax() - left_data_interpol_corr.size / 2.0)

    right_data_interpol = np.roll(right_data_interpol, correlation_lag)
    
    folded_data_interpol = left_data_interpol + right_data_interpol

    # calculate intensity on original grid
    folded_data = folded_data_interpol.reshape(-1, factor).mean(axis=1)

    extra_data_folded = []
    
    for data_set in extra_data:
        data_set_folded = Fold_Interpolate(data_set, flip, factor, method, correlation_lag / factor)
        
        extra_data_folded.append(data_set_folded)
    
    return folded_data, correlation_lag / factor, extra_data_folded


def Fold_2D_hor(data, flip='left'):
    split_data = np.hsplit(np.array(data), 2)

    if flip == 'right':
        left_data = split_data[0]

        right_data = np.flip(split_data[1], axis=1)
    elif flip == 'left':
        left_data = np.flip(split_data[0], axis=1)

        right_data = split_data[1]
    
    correlation = np.correlate(np.sum(left_data, axis=0), np.sum(right_data, axis=0), mode = 'same')

    correlation_maximum = np.argmax(correlation)
    
    shift_value = int(correlation_maximum - (left_data.shape[1]) / 2)

    right_data = np.roll(right_data, shift_value, axis=1)

    return left_data + right_data, int(data.shape[1] / 2 + shift_value)


def Fold_2D_vert(data, flip='left'):
    split_data = np.vsplit(np.array(data), 2)

    if flip == 'right':
        left_data = split_data[0]

        right_data = np.flip(split_data[1], axis=0)
    elif flip == 'left':
        left_data = np.flip(split_data[0], axis=0)

        right_data = split_data[1]
    
    correlation = np.correlate(np.sum(left_data, axis=1), np.sum(right_data, axis=1), mode = 'same')

    correlation_maximum = np.argmax(correlation)
    
    shift_value = int(correlation_maximum - (left_data.shape[0]) / 2)

    right_data = np.roll(right_data, shift_value, axis=0)

    return left_data + right_data, int(data.shape[0] / 2 + shift_value)


def AutoFold_2D(data, axis='hor', flip='left'):
    r"""
    Folds a two dimensional data array along the given axis.
    Uses autocorrelation on the data, which are integrated over the second axis, to determine the correct folding point.
    The given data array is cut in half.
    Data of the second half are rolled and re-introduced on the other side to ensure equal intensities.
    Give an even number of data points along the fold direction.
    Returns the folded data and the fold index.

    The central shift depends on the folding direction (left or right).
    The folding direction depends on the experimental setup.
    Measure the second order Doppler shift to determine the correct folding.
    
    .. versionchanged:: 1.0.2

    Args:
      data (list): Data to be folded. Number of data points has to be even along fold direction.
      axis (int): Either *hor* (horizontal) or *vert* (vertical) folding.
      flip (string): determines whether the left or right (upper/lower) part of the spectrum is flipped.
        
        .. versionadded:: 1.0.2

    Returns:
      ndarray, int: Folded data, fold index
    """
    result = data

    if axis == 'hor':
      result, shift_value = Fold_2D_hor(data, flip)
    elif axis == 'vert':
      result, shift_value = Fold_2D_vert(data, flip)
      
    return result, shift_value


def ChannelsToVelocity(num_channels, velocity, offset=0.0, mode="constant"):
    r"""
    Converts a given number of channels to a velocity grid with possible offset for either sinusoidal mode or constant acceleration mode of a MOessbauer drive unit.

    Args:
      num_channels (int): Number of channels.
      velocity (float): maximum velocity in mm/s.
        The velocity is set symmetrically around zero.
      offset (float): Offset to the symmetric velocity in mm/s. default is 0.
      mode (string): Either constant acceleration mode (``constant``) or sinusoidal mode (``sinus``) of the Moessbauer drive unit.
        Default is ``constant``.

    Returns:
      ndarray: Velocity of the channels in mm/s.
    """
    if mode == "constant":
        velocities = np.linspace(-velocity, velocity, num_channels)
    elif mode == "sinus":
        channels = np.arange(num_channels)
        velocities = -velocity * np.cos(channels / len(channels) * np.pi)
    else:
        raise Exception("mode not recognized.")

    velocities = velocities + offset

    return velocities


def CalibrateChannels(intensity,
                      velocity,
                      intensity_reference,
                      velocity_reference,
                      mode="constant",
                      shift=0.0,
                      center_shift=0.0):
    r"""
    Calibrates the velocity of the intensity data set to a given reference data set with known velocity.
    The intensity array is fit against the reference spectrum.

    .. versionchanged:: 1.0.3
       The velocity offset is now applied to the theory not to the experiment anymore.

    .. versionchanged:: 1.0.4
       Reverted change of version 1.0.3. Offset is applied to velocity again to properly account for drive offset.

    Args:
      intensity (list): Data for which the velocity should be calibrated.
      velocity (float or list): If a float is given, the velocity is the assumed maximum velocity of the data set.
        If a list is provided, it is the assumed experimental velocity grid.
      intensity_reference (list): Array of intensity data used as reference. 
      velocity_reference (list): Array of velocities used as reference.
      mode (string): Either constant acceleration mode (``constant``) or sinusoidal mode (``sinus``) of the Moessbauer drive unit. 
        Default is ``constant``.
      shift (float): Initial guess for shift of the spectrum in mm/s. Optional, default is zero.
        
        .. versionadded:: 1.0.3

      center_shift (float): Offset to the velocity calibration in mm/s. Optional, default is zero.
        For example, when a center shift is known between source and velocity reference sample.

        .. versionadded:: 1.0.4

    Returns:
      ndarray, float, float, ndarray, ndarray:
         calibrated experimental velocity,
         velocity offset,
         velocity scaling factor,
         velocity used for the theoretical spectrum,
         theoretical spectrum
    """
 
    if isinstance(velocity, (int, float)):
        if mode == "constant":
            velocities = np.linspace(-velocity, velocity, len(intensity))
        elif mode == "sinus":
            channel = np.arange(len(intensity))
            velocities = -velocity * np.cos(channel / len(channel) * np.pi)
        else:
            raise Exception("mode not recognized.")
    elif isinstance(velocity, (list, tuple, np.ndarray)):
        velocities = velocity
    else:
        raise Exception("velocity has wrong format.")
    
    # this is a pointer to an interpolation function for the theory data
    theory_interpolation_function = interp1d(velocity_reference, intensity_reference, fill_value = 'extrapolate')
    
    def fit_func(x, intensity, velocities, theory_interpolation_func):
        velocity_exp = x[1] * velocities - x[0]
        
        return intensity - (x[3] * theory_interpolation_func(velocity_exp) + x[2])

    result = least_squares(fit_func,
                           x0 = [shift, 1, np.mean(intensity), max(intensity)-min(intensity)], # velocity_offset, velocity_scaling, int_offset, int_scaling
                           args = (intensity, velocities, theory_interpolation_function),
                           method = 'trf')
    
    velocity_exp =  result.x[1] * velocities - result.x[0] - center_shift

    intensity_reference_scaled = result.x[3] * intensity_reference + result.x[2]

    return velocity_exp, result.x[0], result.x[1], velocity_reference, intensity_reference_scaled



def CalibrateChannelsExperiment(intensity,
                                velocity,
                                experiment,
                                emission=False,
                                mode="constant",
                                shift=0.0,
                                center_shift=0.0):
    r"""
    Calibrates the velocity of a reference experiment.
    The theoretical model of the reference :class:`Experiment` must be given.
    The intensity array is fit against the theoretical spectrum.

    .. versionchanged:: 1.0.3
       The velocity offset is now applied to the theory not to the experiment anymore.

    .. versionchanged:: 1.0.4
       Reverted change of version 1.0.3. Offset is applied to velocity again to properly account for drive offset.

    Args:
      intensity (list): Data for which the velocity should be calibrated.
      velocity (float or list): If a float is given, the velocity is the assumed maximum velocity of the data set.
        If a list is provided, it is the assumed experimental velocity grid.
      experiment (nx.Experiment): An :class:`Experiment` against which the experimental velocity is fit.
      emission (bool): If ``False`` (default), an absorption spectrum is calculated. If ``True`` an emission spectrum is calculated.
      mode (string): Either constant acceleration mode (``constant``) or sinusoidal mode (``sinus``) of the Moessbauer drive unit. 
        Default is ``constant``.
      shift (float): initial guess for shift of the spectrum in mm/s.

        .. versionadded:: 1.0.3

      center_shift (float): Offset to the velocity calibration in mm/s. Optional, default is zero.
        For example, when a center shift is known between source and velocity reference sample.

        .. versionadded:: 1.0.4

    Returns:
      ndarray, float, float, ndarray, ndarray:
         calibrated experimental velocity,
         velocity offset,
         velocity scaling factor,
         velocity used for the theoretical spectrum,
         theoretical spectrum
    """
    if isinstance(velocity, (int, float)):
        v_max = velocity
    elif isinstance(velocity, (list, tuple, np.ndarray)):
        v_max = max(velocity, key=abs)
    else:
        raise Exception("velocity has wrong format.")

    # theory calculations are performed on linear grid
    velocities_theory = np.linspace(-v_max, v_max, len(intensity))

    if (emission == True):
        spectrum = clib.EmissionSpectrum(experiment = experiment,
                                         velocity = velocities_theory)
    else:
        spectrum = clib.MoessbauerSpectrum(experiment = experiment,
                                           velocity = velocities_theory)
        
    spectrum_theory = spectrum()
    
    return CalibrateChannels(intensity, velocity, spectrum_theory, velocities_theory, mode, shift = shift, center_shift = center_shift)


def CalibrateChannelsAlphaFe(intensity,
                             velocity,
                             thickness,
                             Bhf=33.0,
                             B_fwhm=0.3,
                             emission=False,
                             mode="constant",
                             shift=0.0,
                             center_shift=0.0):
    r"""
    Calibrates the velocity of an isotropically distributed :math:`\\alpha`-Fe spectrum.
    No beam polarization.
    The intensity array is fit against a theoretical sextet of :math:`\\alpha`-Fe.

    .. versionchanged:: 1.0.3
       The velocity offset is now applied to the theory not to the experiment anymore.

    .. versionchanged:: 1.0.4
       Reverted change of version 1.0.3. Offset is applied to velocity again to properly account for drive offset.

    Args:
      intensity (list): Data from an :math:`\\alpha`-Fe sample.
      velocity (float or list): If a float is given, the velocity is the assumed maximum velocity of the data set.
        If a list is provided, it is the assumed experimental velocity grid.
      thickness (float): Thickness of the :math:`\\alpha`-Fe sample (nm).
        Please note, that for sample thicknesses exceeding the typical escape length of the emitted probe (X-ray fluorescence or electrons) the escape length should be given.
      Bhf (float): The magnitude of the magnetic hyperfine field.
        Default is 33 T.
      B_fwhm (float): The FWHM of the magnetic hyperfine field distribution.
        Default is 0.3 T.
      emission (bool): If ``False`` (default), an absorption spectrum is calculated. If ``True`` an emission spectrum is calculated.
      mode (string): Either constant acceleration mode (``constant``) or sinusoidal mode (``sinus``) of the Moessbauer drive unit. 
        Default is ``constant``.
      shift (float): initial guess for shift of the spectrum in mm/s.

        .. versionadded:: 1.0.3

      center_shift (float): Offset to the velocity calibration in mm/s. Optional, default is zero.
        For example, when a center shift is known between source and velocity reference sample.

        .. versionadded:: 1.0.4

    Returns:
      ndarray, float, float, ndarray, ndarray:
          calibrated experimental velocity,
          velocity offset,
          velocity scaling factor,
          velocity used for the theoretical spectrum,
          theoretical spectrum
    """
    dist = distribution.Gaussian(points = 31, fwhm = B_fwhm)
    
    site = clib.Hyperfine(magnetic_field = Bhf,
                          magnetic_field_dist = dist,
                          isotropic = True)
    
    mat = clib.Material.Template(material.Fe)
    
    mat.hyperfine_sites = [site]
    
    layer = clib.Layer(thickness = thickness,
                     material = mat)
    
    sample = clib.Sample(layers = [layer])
    
    beam = clib.Beam()
    beam.Unpolarized()
    
    exp = clib.Experiment(beam = beam,
                          objects = [sample],
                          isotope = moessbauer.Fe57)

    return CalibrateChannelsExperiment(intensity,
                                       velocity,
                                       experiment=exp,
                                       emission=emission,
                                       mode=mode,
                                       shift=shift,
                                       center_shift=center_shift)


def FindMinima(x_data, y_data, n=None):
    r"""
    Returns the x and y values of minima or of a single minimum found on the y data set.

    .. versionadded:: 1.0.2

    Args:
      x_data (list): x data.
      y_data (list): y data on which the minima are searched for.
      n (int): returns only the n-th minimum, optional parameter.

    Returns:
      ndarray or float, ndarray or float, ndarray or int: minima position on the x axis, minima values on the y axis, indices of the minima
    """
    indices, _ = find_peaks(-y_data)

    if n is not None:
        indices = indices[n-1]

    return x_data[indices], y_data[indices], indices


def FindMaxima(x_data, y_data, n=None):
    r"""
    Returns the x and y values of maxima or of a single maximum found on the y data set.

    .. versionadded:: 1.0.2

    Args:
      x_data (list): x data.
      y_data (list): y data on which the maxima are searched for.
      n (int): returns only the n-th maximum, optional parameter.

    Returns:
      ndarray or float, ndarray or float, ndarray or int: maxima position on the x axis, maxima values of the y axis, indices of the maxima
    """

    indices, _ = find_peaks(y_data)

    if n is not None:
        indices = indices[n-1]

    return x_data[indices], y_data[indices], indices


def FindPeaks(x, y, n=6, neg=True):
    r"""
    Returns the x position and y values of a number of expected extrema on the y data.

    .. versionadded:: 1.2.0

    Args:
      x_data (list): x data.
      y_data (list): y data on which the minima are searched for.
      n (int): number of peaks to find.
      neg (bool): If "True", negative peaks are searched for. Default is "True".

    Returns:
      ndarray or float, ndarray or float, ndarray or int, int: extrema position on the x axis, extrema on the y axis, indices of the extrema, number of extrema found
    """
    val = y
    
    if neg:
        val = -y

    widths = 1

    indices, _ = find_peaks(val, width=widths)
    
    while (len(indices) != n):
      widths += 1
      indices, _ = find_peaks(val, width=widths)

    return x[indices], y[indices], indices, len(indices)


def BaselinePoly(data, left_point, right_point, poly_order=2):
    r"""
    Apply a polynomial baseline correction to the data.
    In order to correct the baseline, only parts of the spectrum should be used where no peaks are visible.

    Be aware, that baseline correction changes the intensity of the lines.

    For more options and other algorithms use the `pybaselines` package.

    .. versionadded:: 1.0.4

    Args:
      data (list): Data to be corrected.
      left_point (int): Index of the data array. Data from zero to this index are used.
      right_point (int): Index of the data array. Data from this point to the end of the array are used.
      poly_order (int): Polynominal order for fitting the baseline. Default is 2.

    Returns:
      ndarray, ndarray: Returns the data corrected by the baseline and the baseline.
    """
    data = np.array(data)

    x = np.arange(len(data))

    non_peaks = ((x < left_point) | (x > right_point))
    x_masked = x[non_peaks]
    d_masked = data[non_peaks]

    _, params = poly(d_masked, x_masked, poly_order=poly_order, return_coef=True)
    
    baseline = np.polynomial.Polynomial(params['coef'])(x)

    return data - baseline + np.mean(baseline), baseline


def Baseline(data, lam, algorithm='aspls', max_iter=100, tol=0.001):
    r"""
    Apply various algorithms for baseline correction of the data.
    The correct ``lam`` value has to be specified by the user.
    A good procedure is to start with a low value (100) and then increase in an exponential manner (1e3, 1e4, ...).
    At some point the curvature of the baseline changes.
    Then, reduce the values again and adjust.

    Be aware, that baseline correction changes the intensity of the lines.

    For more options and other algorithms use the `pybaselines` package.

    .. versionadded:: 1.0.4

    Args:
      data (list): Data to be corrected.
      lam (float): The smoothing parameter. Larger values will create smoother baselines.
      algorithm (string): Defines the used algorithm.

        * ``aspls`` Adaptive Smoothness Penalized Least Squares.
        * ``mpls`` Morphological Penalized Least Squares.
        * ``asls`` Asymmetric Least Squares.
        * ``drlps`` Doubly Reweighted Penalized Least Squares.
        * ``arpls`` Asymmetrically Reweighted Penalized Least Squares.
        * ``iarpls`` Improved Asymmetrically Reweighted Penalized Least Squares.
        * ``amormol`` Iteratively averaging morphological and mollified baseline fit.
      
        See `pybaselines` package documentaiton for more information.

      max_iter (int): Maximum number of iterations. Default is 100.
      tol (float): Tolerance value, defines the exit criteria. Default is 0.001.

    Returns:
      ndarray, ndarray: Returns the data corrected by the baseline and the baseline.
    """
    data = np.array(data)

    x = np.arange(len(data))

    baseline_fitter = pyBase(x_data=x)

    if algorithm == 'aspls':
        baseline, _ = baseline_fitter.aspls(data, lam=lam, diff_order=2, max_iter=max_iter, tol=tol, weights=None)
    elif algorithm == 'mpls':
        baseline, _ = baseline_fitter.mpls(data, half_window=None, lam=lam, p=0.0, diff_order=2, max_iter=max_iter, tol=tol, weights=None)
    elif algorithm == 'asls':
        baseline, _ = baseline_fitter.asls(data, lam=lam, p=0.01, diff_order=2, max_iter=max_iter, tol=tol, weights=None)
    elif algorithm == 'drpls':
        baseline, _ = baseline_fitter.drpls(data, lam=lam, eta=0.5, diff_order=2, max_iter=max_iter, tol=tol, weights=None)
    elif algorithm == 'arpls':
        baseline, _ = baseline_fitter.arpls(data, lam=lam, diff_order=2, max_iter=max_iter, tol=tol, weights=None)
    elif algorithm == 'iarpls':
        baseline, _ = baseline_fitter.iarpls(data, lam=lam, diff_order=2, max_iter=max_iter, tol=tol, weights=None)
    elif algorithm == 'amormol':
        baseline, _ = baseline_fitter.amormol(data, half_window=None, max_iter=max_iter, tol=tol, pad_kwargs=None)
    else:
        raise Exception('Nexus warning - algorithm in baseline_Wittaker not found.')
        
    return data - baseline + np.mean(baseline), baseline


def Normalize(data, method = "baseline", value = 0, left_point = 0, right_point = 0, poly_order = 0):
    r"""
    Returns the normalized data.

    .. versionadded:: 1.2.0

    Args:
      data (list): Data to be normalized.
      method (string): Can be 
      
        * "value": Normalize by given value.
        * "max": Normalize by maximum of data.
        * "min": Normalize by minimum of data.
        * "sum": Normalize by sum of all data points.
        * "mean": Normalize by mean of all data points.
        * "baseline": Normalize by a polynominal baseline fit.

      value (float): Initial guess for shift of the spectrum in mm/s. Optional, default is zero.
      left_point (int): Index of the data array. Data from zero to this index are used for the baseline fit. If zero it is automatically selected.
      right_point (int): Index of the data array. Data from this point to the end of the array are used for the baseline fit. If zero it is automatically selected.
      poly_order (int): Polynominal order for fitting the baseline. ``0`` for flat baseline and ``1`` for tilted line correction. Default is ``0``.

    Returns:
      ndarray, float: normalized intensity, normalization factor
    """
    norm = 1.0

    if method is "value":
        norm =  value
    elif method is "max":
        norm = np.max(data)
    elif method is "min":
        norm = np.min(data)
    elif method is "sum":
        norm = np.sum(data)
    elif method is "mean":
        norm = np.mean(data)
    elif method is "baseline":
        x = len(data)

        if left_point is 0:
            lp = 10
        else:
            lp = left_point
        
        if right_point is 0:
            rp = x-11
        else:
            rp = right_point

        _, baseline = BaselinePoly(data, lp, rp, poly_order)

        norm = np.average(baseline)
    else:
        raise Exception("method not recognized")

    normalized = np.array(data) / norm

    return normalized, norm


def GetLorentzian(velocity, intensity, n=6, neg=True, baseline=None):
    r"""
    Returns the widths in units of velocity values of the peaks found.
    The number of peaks needs to be specified.

    .. versionadded:: 1.2.0

    Args:
      velocity (list): velocity data.
      intensity (list): intensity data.
      n (int): number of peaks to find.
      neg (bool): If "True", negative peaks are searched for. Default is "True".

    Returns:
      int, ndarry, ndarray, ndarray,ndarray, ndarray, float, ndarray:
        number of peaks,
        indices of peaks,
        velocity values of peaks,
        intensity values of peaks,
        width (FWHM) values in units of the velocity,
        area values,
        baseline of fit,
        fitted spectrum.
    """
    def Lorentzian(x, amplitude, offset, center, width):
        return amplitude * width**2 / ((x - center)**2 + width**2) + offset

    def Total_Lorentzian(x, *par):
        r"""
        adds up Lorentzian curves depending on the number of parameter parsed
        par[0] is the baseline value
        then it is followed by 3 values specifying each Lorentzian [amplitude, velocity, width] (mm/s)
        """

        tot_lor = np.full(len(x), par[0])
    
        for i in range(len(par)//3):
            tot_lor += Lorentzian(x, par[3*i+1], 0, par[3*i+2], par[3*i+3])

        return tot_lor

    vel, int, indices, n = FindPeaks(velocity, intensity, n, neg)

    baseline_start = np.mean(intensity)

    bounds_lower = []
    bounds_upper = []

    if baseline != None:
        baseline = float(baseline)
        params = [baseline]
        bounds_lower.append(baseline - 1.0e-16)
        bounds_upper.append(baseline + 1.0e-16)
    else:
        params = [baseline_start]
        bounds_lower.append(-np.inf)
        bounds_upper.append(np.inf)

    for i in range(n):
        params.extend([int[i] - baseline_start, vel[i], 0.3])
        bounds_lower.extend([-np.inf, -np.inf, -np.inf])
        bounds_upper.extend([np.inf, np.inf, np.inf])

    tot = Total_Lorentzian(velocity, *params)

    bounds = (bounds_lower, bounds_upper)

    popt_lor, _ = curve_fit(Total_Lorentzian, velocity, intensity, p0=params, bounds=bounds, method='trf')

    velocities = []
    widths = []
    intensities = []
    areas = []

    for i in range(len(popt_lor)//3):
        int = popt_lor[3*i+1]
        intensities.append(int)

        velocities.append(popt_lor[3*i+2])
        
        w_fwhm = 2.0 * popt_lor[3*i+3]
        widths.append(w_fwhm)
        
        if neg== True:
            int *= -1.0
        areas.append(np.pi * w_fwhm * int)
        
    return n, indices, np.squeeze(velocities), np.squeeze(intensities), np.squeeze(widths), np.squeeze(areas), popt_lor[0], Total_Lorentzian(velocity, *popt_lor)


def GetLinewidths(velocity, intensity, n=6, neg=True):
    r"""
    Returns the widths in units of velocity values of the peaks found.
    The number of peaks needs to be specified.
    Helpful to get the instrumental function from a reference spectrum.

    .. versionadded:: 1.2.0

    Args:
      velocity (list): velocity data.
      intensity (list): intensity data.
      n (int): number of peaks to find.
      neg (bool): If "True", negative peaks are searched for. Default is "True".

    Returns:
      ndarray: width (FWHM) values in units of the velocity
    """
    _, _, _, _, widths, _, _, _ = GetLorentzian(velocity, intensity, n, neg)

    return widths


def GetAreas(velocity, intensity, n=6, neg=True, norm = None):
    r"""
    Returns the sum of the peak areas found in the fit from smallest to highest detuning values and their ratios on the total area.
    
    .. versionadded:: 1.2.0

    Args:
      velocity (list): velocity data.
      intensity (list): intensity data.
      n (int): number of peaks to find.
      neg (bool): If "True", negative peaks are searched for. Default is "True".
      norm (string): Determines the normalization factor. None, "sum", "mean", "min" or "max".

    Returns:
      ndarray, ndarray: areas values, area ratio
    """
    _, _, _, _, _, areas, _, _ = GetLorentzian(velocity, intensity, n, neg)

    areas_folded = []

    for i in range(len(areas)//2):
        areas_folded.append(areas[i] + areas[-(i+1)])

    norm_factor = 1

    if norm == "sum":
      norm_factor = np.sum(areas_folded)
    if norm == "mean":
      norm_factor = np.mean(areas_folded)
    elif norm == "min":
      norm_factor = np.min(areas_folded)
    elif norm == "max":
      norm_factor = np.max(areas_folded)

    areas_ratio = np.array(areas_folded) / norm_factor

    return np.array(areas_folded), areas_ratio
