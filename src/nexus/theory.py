# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https:www.gnu.org/licenses/>.

 
"""
Module for theoretical calculations.
"""

import numpy as np

from nexus import clib
from nexus import constants
from nexus import conversions
from nexus.lib import moessbauer
import scipy.integrate


# nuclear properties

def HalfLifetime(lifetime):
    r"""
    Calculates the half lifetime :math:`t_{\frac{1}{2}}` [Gonser]_

    .. math:: t_{\frac{1}{2}} = \ln(2) \tau

    Args:
        lifetime (float): lifetime :math:`\tau` (arb. units).

    Returns:
        float: half lifetime (in units of lifetime).
    """
    return lifetime * np.log(2)


# Doppler shift functions

def DopplerShift(value, velocity):
    r"""
    Calculates the Doppler shift of energy or frequency [Gonser]_

    .. math:: \Delta E = \frac{v}{c} E

    The velocity is positive when source and receiver are moving toward each other.

    Args:
        value (float): energy or frequency in units of choice.
        velocity (float): velocity (m/s).

    Returns:
        float: Doppler shift of energy or frequency.
    """

    return velocity / constants.SpeedOfLight * value


def DopplerEnergy(value, velocity):
    r"""
    Calculates the Doppler shifted energy or frequency [Gonser]_

    .. math:: E_{\gamma} = \left(1 + \frac{v}{c}\right) E_0

    The velocity is positive when source and receiver are moving toward each other.

    Args:
        value (float): energy or frequency in units of choice.
        velocity (float): velocity (m/s).

    Returns:
        float: shifted energy or frequency.
    """

    return (1.0 + velocity / constants.SpeedOfLight) * value


def SecondOrderDopplerShift(velocity_mean, energy):
    r"""
    Calculates the second order Doppler shift for a given mean squared velocity [Josephson]_
     
    .. math:: \Delta  E = -E \frac{\langle  v^2 \rangle}{2 c^2}.
    
    .. versionadded:: 1.0.3
    
    Args:
       velocity_mean (float): Mean squared velocity ((m/s) :sup:`2` ).
       energy (float): Energy of the transition (eV).

    Returns:
       float: Change in energy due to second order Doppler shift (eV).
    """
    return -energy * velocity_mean / (2.0 * constants.SpeedOfLight**2)


def SecondOrderDopplerVelocityShift(velocity_mean):
    r"""
    Calculates the second order Doppler shift for a given mean squared velocity [Nasu]_
     
    .. math:: \Delta  v = -\frac{\langle v^2 \rangle }{2 c}.

    .. versionadded:: 1.0.3

    Args:
       velocity_mean (float): Mean squared velocity ((m/s) :sup:`2` ).

    Returns:
       float: Change in velocity due to second order Doppler shift (mm/s).
    """
    return -velocity_mean / (2.0 * constants.SpeedOfLight) * 1.0e3


def SecondOrderDopplerDebye(temperature, Debye_temperature, energy, mass):
    r"""
    Calculates the second order Doppler shift for a given temperature in the Debye model [Tanaka]_
    
    .. math:: \Delta E = -E \frac{9 k_B \Theta_D}{16 M c^2} \left( 1 + 8 \left(\frac{T}{\Theta_D}\right)^4 \int_0^{\frac{\Theta_D}{T}} \frac{x^3}{e^x-1}dx \right)

    .. versionadded:: 1.0.3

    Args:
        temperature (float): real temperature (K).
        Debye_temperature (float): Debye temperature (K).
        energy (float): Transition energy (eV).
        mass (float): atomic mass in atomic units (amu).

    Returns:
        float: Energy shift (eV).
    """
    temperature_ratio = temperature / Debye_temperature

    if temperature_ratio == 0.0:  # Integral from 0 to inf.
        int = np.pi**4 / 15.0
    elif 1.0/temperature_ratio < 1:
        int = scipy.integrate.quad(lambda x: x**3 / (np.exp(x) - 1.0), 0.0, 1.0 / temperature_ratio)[0]
    else:  # Constant is integral from 0 to 1 to avoid singularity.
        int = 0.224805 + scipy.integrate.quad(lambda x: x**3 / (np.exp(x) - 1.0), 1.0, 1.0 / temperature_ratio)[0]
    
    Boltzmann = 1.380649e-23  # in J/K

    mass_kg = mass * 1.0/constants.Na * 1.0e-3  # amu to g to kg

    deltaE = -energy * (9.0 * Boltzmann * Debye_temperature) / (16.0 * mass_kg * constants.SpeedOfLight**2) * (1.0 + 8.0 * temperature_ratio**4 * int)

    return deltaE


def SecondOrderDopplerDebyeIsotope(temperature, Debye_temperature, isotope):
    r"""
    Calculates the second order Doppler shift for a given temperature in the Debye model [Tanaka]_
    
    .. math:: \Delta E = -E \frac{9 k_B \Theta_D}{16 M c^2} \left( 1 + 8 \left(\frac{T}{\Theta_D}\right)^4 \int_0^{\frac{\Theta_D}{T}} \frac{x^3}{e^x-1}dx \right)

    .. versionadded:: 1.0.3

    Args:
        temperature (float): real temperature (K).
        Debye_temperature (float): Debye temperature (K).
        isotope (:class:`MoessbauerIsotope`): isotope for the calculation.

    Returns:
        float: Energy shift (eV).
    """
    return SecondOrderDopplerDebye(temperature, Debye_temperature, isotope.energy, isotope.mass)


def SecondOrderDopplerDebyeVelocity(temperature, Debye_temperature, mass):
    r"""
    Calculates the second order Doppler shift in units of velocity for a given temperature in the Debye model.
    
    .. math:: SOD = -\frac{9 k_B \Theta_D}{16 M c} \left( 1 + 8 \left(\frac{T}{\Theta_D}\right)^4 \int_0^{\frac{\Theta_D}{T}} \frac{x^3}{e^x-1}dx \right)

    .. versionadded:: 1.0.4

    Args:
        temperature (float): real temperature (K).
        Debye_temperature (float): Debye temperature (K).
        mass (float): atomic mass in atomic units (amu).

    Returns:
        float: SOD (mm/s).
    """
    temperature_ratio = temperature / Debye_temperature

    if temperature_ratio == 0.0:  # Integral from 0 to inf.
        int = np.pi**4 / 15.0
    elif 1.0/temperature_ratio < 1:
        int = scipy.integrate.quad(lambda x: x**3 / (np.exp(x) - 1.0), 0.0, 1.0 / temperature_ratio)[0]
    else:  # Constant is integral from 0 to 1 to avoid singularity.
        int = 0.224805 + scipy.integrate.quad(lambda x: x**3 / (np.exp(x) - 1.0), 1.0, 1.0 / temperature_ratio)[0]
    
    Boltzmann = 1.380649e-23  # in J/K

    mass_kg = mass * 1.0/constants.Na * 1.0e-3  # amu to g to kg

    sod = -9.0 * Boltzmann * Debye_temperature / (16.0 * mass_kg * constants.SpeedOfLight) * (1.0 + 8.0 * temperature_ratio**4 * int) # m/s

    return sod * 1000  # m/s


def SecondOrderDopplerDebyeVelocityIsotope(temperature, Debye_temperature, isotope):
    r"""
    Calculates the second order Doppler shift in units of velocity for a given temperature in the Debye model.
    
    .. math:: SOD = -\frac{9 k_B \Theta_D}{16 M c} \left( 1 + 8 \left(\frac{T}{\Theta_D}\right)^4 \int_0^{\frac{\Theta_D}{T}} \frac{x^3}{e^x-1}dx \right)

    .. versionadded:: 1.0.4

    Args:
        temperature (float): real temperature (K).
        Debye_temperature (float): Debye temperature (K).
        isotope (:class:`MoessbauerIsotope`): isotope for the calculation.

    Returns:
        float: SOD (mm/s).
    """
    return SecondOrderDopplerDebyeVelocity(temperature, Debye_temperature, isotope.mass)



# Recoil functions

def RecoilEnergy(energy, mass):
    r"""
    Calculates the recoil energy of a photon at mass :math:`m` [Gonser]_

    .. math:: R = \frac{E^2}{2mc^2}

    Args:
        energy (float): Photon energy (eV).
        mass (float): mass in atomic mass units (amu).

    Returns:
        float: recoil energy (eV).
    """
    mass_kg = mass * 1.0/constants.Na * 1.0e-3  # amu to g to kg
    
    recoil_energy = energy**2 / (2.0 * mass_kg * constants.SpeedOfLight**2) * constants.ElementaryCharge  # from joule to eV

    return recoil_energy


def RecoilEnergyIsotope(isotope):
    r"""
    Calculates the recoil energy of a photon at a specific isotope [Gonser]_

    .. math:: R = \frac{E^2}{2m_{I}c^2}

    .. versionadded:: 1.0.3

    Args:
        energy (:class:`MoessbuaerIsotope`): Isotope for the calculation.

    Returns:
        float: recoil energy (eV).
    """
    return RecoilEnergy(isotope.energy, isotope.mass)


def LambMoessbauerDebye(temperature, Debye_temperature, energy, mass):
    r"""
    Calculates the Lamb Moessbauer factor in the Debye model [Gonser]_

    .. math:: f_{LM} = \exp\left( -\frac{3 E_R}{2k_B\Theta_D} \left( 1+ 4 \left(\frac{T}{\Theta_D}\right)^2 \int_0^{\frac{\Theta_D}{T}} \frac{x}{e^x-1}dx \right) \right)

    .. versionadded:: 1.0.3

    Args:
        temperature (float): real temperature (K).
        Debye_temperature (float): Debye temperature (K).
        energy (float): Photon energy (eV).
        mass (float): mass in atomic mass units (amu).

    Returns:
        float: Lamb Moessbauer factor.
    """
    temperature_ratio = temperature / Debye_temperature
       
    #recoil_energy = RecoilEnergy(energy, mass)
 
    if temperature_ratio == 0.0:  # Integral from 0 to inf.
        int = np.pi**2 / 6.0
    elif 1.0/temperature_ratio < 1:
        int = scipy.integrate.quad(lambda x: x / (np.exp(x) - 1.0), 0.0, 1.0 / temperature_ratio)[0]
    else:  # Constant is integral from 0 to 1 to avoid singularity.
        int = 0.777505 + scipy.integrate.quad(lambda x: x / (np.exp(x) - 1.0), 1.0, 1.0 / temperature_ratio)[0]
    
    mass_kg = mass * 1.0/constants.Na * 1.0e-3  # amu to g to kg

    factor = -3.0 * energy**2 / (mass_kg * constants.SpeedOfLight**2 * 1.0/constants.ElementaryCharge * constants.Boltzmann * Debye_temperature) *  (1/4 + temperature_ratio**2 * int)

    return np.exp(factor)


def DebyeTemperatureImpurity(Debye_temperature, mass_host, mass_impurity):
    r"""
    Calculates the Debye temperature for an impurity atom in a host matrix [Gonser]_

    .. math:: \Theta^I_D = \sqrt{\frac{M_{host}}{M_{impurity}}}\Theta_D

    .. versionadded:: 1.0.3

    Args:
        Debye_temperature (float): Debye temperature of the host material (K).
        mass_host (float): mass of the host atom.
        mass_impurity (float): mass of the impurity atom.

    Returns:
        float: Debye temperature of the impurity (K).
    """
    return np.sqrt(mass_host/mass_impurity) * Debye_temperature


def LambMoessbauerDebyeIsotope(temperature, Debye_temperature, isotope):
    r"""
    Calculates the Lamb Moessbauer factor in the Debye model [Gonser]_

    .. math:: f_{LM} = \exp\left( -\frac{3 E_R}{2k_B\Theta_D} \left( 1+ 4 \left(\frac{T}{\Theta_D}\right)^2 \int_0^{\frac{\Theta_D}{T}} \frac{x}{e^x-1}dx \right) \right)

    .. versionadded:: 1.0.3

    Args:
        temperature (float): real temperature (K).
        Debye_temperature (float): Debye temperature (K).
        isotope (:class:`MoessbauerIsotope`): isotope for the calculation.

    Returns:
        float: Lamb Moessbauer factor.
    """
    return LambMoessbauerDebye(temperature, Debye_temperature, isotope.energy, isotope.mass)


# Energy functions

def QuadrupoleEnergy(I, m, Q, Vzz, asymmetry):
    r"""
    Calculates the quadrupole energy [Gonser]_ and [Drago]_

    .. math:: E = \frac{eQV_{zz}}{4I(2I+1)} \left(3m^2-I(I+1))\right) \sqrt{1 + \frac{\eta^2}{3}},

    .. versionadded:: 1.0.3

    Args:
        I (float): Spin of the (ground or excited) state :math:`I`.
        m (float): magnetic quantum number :math:`m = -I, ..., I`.
        Q (float): Quadrupole moment (barn).
        Vzz (float): Electric field gradient along main axis (V/m\ :sup:`2`).
        asymmetry: Asymmetry parameter :math:`\eta` (dimensionless 0 to 1).

    Returns:
        float: Quadrupole energy (eV).
    """
    E = Q * 1e-28  *Vzz / (4.0 * I * (2.0 * I - 1)) * (3.0 * m**2 - I * (I + 1.0)) * np.sqrt(1.0 + asymmetry**2 / 3.0)  # J to eV is divided by e

    return E


def QuadrupoleSplittingDoublet(Q, Vzz, asymmetry):
    r"""
    Calculates the quadrupole splitting for a doublet (I = 3/2) [Gonser]_ and [Drago]_

    .. math:: \Delta E = \frac{eQV_{zz}}{2} \sqrt{1 + \frac{\eta^2}{3}},

    .. versionadded:: 1.0.3

    Args:
        Q (float): Quadrupole moment (barn).
        Vzz (float): Electric field gradient along main axis (V/m\ :sup:`2`).
          Note, :math:`V_{zz} = eq_{zz}`, where :math:`q_{zz}` is the direction of the z-axis of the field gradient.
        asymmetry: Asymmetry parameter :math:`\eta` (dimensionless 0 to 1).

    Returns:
        float: Quadrupole energy (eV).
    """
    splitting = 1/2 * Q * 1e-28 * Vzz * np.sqrt(1.0 + asymmetry**2 / 3.0)  # J to eV is divided by e

    return splitting


def QuadrupoleSplittingDoubletVelocity(isotope, Q, Vzz, asymmetry):
    r"""
    Calculates the quadrupole splitting for a doublet (I = 3/2) in units of the velocity [Gonser]_ and [Drago]_ 

    .. versionadded:: 1.0.3

    Args:
        isotope (:class:`MoessbauerIsotope`): Isotope for the calculation.
        Q (float): Quadrupole moment (barn).
        Vzz (float): Electric field gradient along main axis (V/m\ :sup:`2`).
          Note, :math:`V_{zz} = eq_{zz}`, where :math:`q_{zz}` is the direction of the z-axis of the field gradient.
        asymmetry: Asymmetry parameter :math:`\eta` (dimensionless 0 to 1).

    Returns:
        float: velocity spacing (mm/s).
    """
    velocity = constants.SpeedOfLight * QuadrupoleSplittingDoublet(Q, Vzz, asymmetry) / isotope.energy
    
    return velocity * 1.0e3  # to mm/s


def NuclearMagneticMoment(g, I):
    r"""
    Calculates the nuclear magnetic moment [Gonser]_ and [Drago]_

    .. math:: \mu = g \mu_N I

    .. versionadded:: 1.0.3

    Args:
        g (float): g-factor of the (ground or excited) state.
        I (float): Spin of the (ground or excited) state.

    Returns:
        float: nuclear magnetic moment (eV/T).
    """
    return g * constants.NuclearMagneton * I


def MagneticEnergy(m, g, Bhf):
    r"""
    Calculates the magnetic energy due to a hyperfine field [Gonser]_

    .. math:: E = -mg \mu_N B_{hf}

    .. versionadded:: 1.0.3

    Args:
         isotope (:class:`MoessbauerIsotope`): Isotope for the calculation.
         Bhf (float): Magnetic hyperfine field (T).

    Returns:
        float: energy (eV).
    """
    return -m * g * constants.NuclearMagneton * Bhf


def MagneticEnergyDifference(mg, gg, me, ge, Bhf):
    r"""
    Calculates the magnetic energy difference of magnetically split lines [Gonser]_

    .. math:: \Delta E =\left(m_e g_e - m_g g_g \right)\mu_N B_{hf} 

    .. versionadded:: 1.0.3

    Args:
         mg (float): magnetic quantum number of the ground state.
         gg (float): g factor of the ground state.
         me (float): magnetic quantum number of the excited state.
         ge (float): g factor of the excited state.
         Bhf (float): Magnetic hyperfine field (T).

    Returns:
        float: energy (eV).
    """
    return -(me * ge - mg * gg) * constants.NuclearMagneton * Bhf


def MagneticSplittingSextet(isotope, Bhf):
    r"""
    Calculates the outer line splitting of a magnetic sextet, like for Fe57,

    .. math:: \Delta E = B_{hf} \mu_N \left(3|g_e| + |g_g| \right). 

    .. versionadded:: 1.0.3

    Args:
        isotope (:class:`MoessbauerIsotope`): Isotope for the calculation.
        Bhf (float): Magnetic hyperfine field (T)

    Returns:
        float: energy (eV).
    """
    splitting = Bhf * constants.NuclearMagneton * (3 * np.abs(isotope.gfactor_excited) + np.abs(isotope.gfactor_ground))
    
    return splitting


def MagneticSplittingSextetVelocity(isotope, Bhf):
    r"""
    Calculates the outer line splitting velocity of a magnetic sextet, like for Fe57, in units of velocity

    .. versionadded:: 1.0.3

    Args:
        isotope (:class:`MoessbauerIsotope`): Isotope for the calculation.
        Bhf (float): Magnetic hyperfine field (T).

    Returns:
        float: velocity spacing (mm/s).
    """
    velocity = constants.SpeedOfLight * MagneticSplittingSextet(isotope, Bhf) / isotope.energy
    
    return velocity * 1.0e3


# cross section

def CrossSection(energy, isotope):
    r"""
    Calculates the absorption cross section [Gonser]_

    .. math:: I = \sigma_0 \frac{\Gamma^2}{4(E-E_0)^2+\Gamma^2}

    .. versionadded:: 1.0.3

    Args:
        energy (float or list or ndarray): energy value(s) (eV).
        isotope (:class:`MoessbauerIsotope`): Isotope for the calculation.

    Returns:
        float or ndarray: absorption.
    """
    energy_np = np.array(energy)

    absorption = isotope.nuclear_cross_section * isotope.gamma**2 /(4.0 * (energy_np - isotope.energy)**2 + isotope.gamma**2)
    
    return absorption


# coherence functions

def LongitudinalCoherence(wavelength, bandwidth):
    r"""
    Calculates the longitudinal coherence length for a given wavelength and its bandwidth via
    
    .. math:: \xi_{\parallel} = \left(\frac{\lambda}{2}\right) \left(\frac{\lambda}{\Delta\lambda}\right)

    .. versionadded:: 1.0.3

    Args:
       wavelength (float): Photon wavelength :math:`\lambda` (m).
       bandwidth (float): Photon wavelength bandwidth :math:`\Delta\lambda` in (m).

    Returns:
       float: longitudinal coherence length (m).
    """
    return wavelength**2 /(2.0 * bandwidth)


def LongitudinalCoherenceEnergy(energy, bandwidth):
    r"""
    Calculates the longitudinal coherence length for a given energy and its bandwidth.

    .. math:: \xi_{\parallel} = \hbar c \frac{1 + \frac{\Delta E}{E}}{2 \Delta E}

    .. versionadded:: 1.0.3

    Args:
       energy (float): Photon energy (eV).
       bandwidth (float): Photon energy bandwidth :math:`\Delta E` in (eV).

    Returns:
       float: longitudinal coherence length (m).
    """
    return constants.PlanckConstant * constants.SpeedOfLight * (1 + bandwidth / energy) / (2.0 * bandwidth)


def CoherenceTime(coherence_length):
    r"""
    Calculates the coherence time of a given longitudinal coherence length :math:`\xi_{\parallel}`

    .. math:: \tau = \frac{\xi_{\parallel}}{c}.

    .. versionadded:: 1.0.3

    Args:
       coherence_length (float): longitudinal coherence length (m).

    Returns:
       float: coherence time (s).
    """
    return coherence_length / constants.SpeedOfLight


def LongitudinalCoherenceFromTime(coherence_time):
    r"""
    Calculates the longitudinal coherence length from a given coherence time :math:`\tau`

    .. math:: \xi_{\parallel} = \tau c

    .. versionadded:: 1.0.3

    Args:
       coherence_time (float): coherence time (s).
       
    Returns:
       float: longitudinal coherence length (m).
    """
    return coherence_time * constants.SpeedOfLight


def TransverseCoherence(wavelength, source_size, source_distance):
    r"""
    Calculates the transverse coherence
    
    .. math:: \xi_{\perp} = \left(\frac{\lambda}{2}\right) \left(\frac{d}{s}\right)

    This equation should be used for vertical and horizontal directions independently.

    .. versionadded:: 1.0.3

    Args:
        wavelength (float): wavelength :math:`\lambda` (m).
        source_size (float): Source size :math:`s` in (m).
        source_distance (float): Source distance :math:`d` in (m).

    Returns:
        float: transverse coherence length (m).
    """
    return wavelength/2 * source_distance/source_size


def TransverseCoherenceEnergy(energy, source_size, source_distance):
    r"""
    Calculates the transverse coherence

    .. math:: \xi_{\perp} = \left(\frac{hc}{2E}\right) \left(\frac{d}{s}\right)

    This equation should be used for vertical and horizontal directions independently.

    .. versionadded:: 1.0.3

    Args:
        energy (float): energy (eV).
        source_size (float): Source size :math:`s` in (m).
        source_distance (float): Source distance :math:`d` in (m).

    Returns:
        float: transverse coherence length (m).
    """
    wavelength = conversions.EnergyToWavelength(energy)

    return TransverseCoherence(wavelength, source_size, source_distance)


def TransverseCoherenceNRS(energy, source_size, source_distance, detector_size, detector_distance, sample_size = None):
    r"""
    Calculates the transverse coherence via 
    
    .. math:: \xi_{\perp} = \left(\frac{hc}{2 E}\right) \left(\frac{1}{\pi\sigma}\right) \textrm{    with    } \sigma^2 = \left( \frac{\sigma_0}{S} \right)^2 + \left( \frac{\sigma_d}{D} \right)^2 + \left( \frac{\sigma_s}{D}+\frac{\sigma_s}{S} \right)^2 + \frac{1}{4k^2\sigma_s^2}
    
    see  [Baron96]_, [Baron99]_.

    Here, especially the detector size typically determines the transverse coherence.
    This equation should be used for vertical and horizontal directions independently.

    .. versionadded:: 1.0.3

    Args:
        energy (float): Photon energy (eV).
        source_size (float): Source size (Gaussian :math:`\sigma_0`) (m).
        source_distance (float): Source-sample distance :math:`S` (m).
        detector_size (float): Detector size (Gaussian :math:`\sigma_d`) (m).
        detector_distance (float): Detector-sample distance :math:`D` (m).
        sample_size (float): Sample size (Gaussian :math:`\sigma_s`) (m), illuminated part, optional.

    Returns:
        float: transverse coherence length (m).
    """
    s = 0

    if sample_size != None:
        s = (sample_size / source_distance + sample_size / detector_distance)**2 + 1 / (4.0 * conversions.EnergyToKvector(energy)**2 * sample_size**2)

    a = np.sqrt(source_size**2 / source_distance**2 + detector_size**2 / detector_distance**2 + s)
    
    coherence = conversions.EnergyToWavelength(energy) / (2.0 * np.pi * a)
    
    return coherence


def BeamDivergence(beam_size1, beam_size2, distance):
    r"""
    Calculates the beam divergence

    .. math:: divergence = 2 \arctan \left( \frac{|s1-s2|}{2 d} \right)

    Args:
       beam_size1 (float): beam size at point 1 (m).
       beam_size2 (float): beam size at point 2 (m).
       distance (float): distance of the two points (m).

    Returns:
       float: beam divergence (degree).
    """

    return 2.0 * np.arctan2(np.abs(beam_size1 - beam_size2), 2.0 * distance) * 180.0 / np.pi


def AreaRatios(input, norm=False, max=False):
    r"""
    Returns the area ratios for A1, A2, A3 and the ratio A23 for the magentic sextet of a magnetic dipole transition given by

    .. math:: A1 : A2 : A3 = 3(1+\cos^2\vartheta) :  4 \sin^2\vartheta : 1 + \cos^2\vartheta

    Note that theoretically A13 is always 3.

    Args:
       input (float or :class:`Hyperfine`): angle (deg) or a :class:`Hyperfine` object.
       norm (bool): If True, A1, A2 and A3 are normalized to a sum of 1.
       max (bool): If True, A1, A2 and A3 are normalized such that the largest area is 1.

    Returns:
       float, float, float, float, float: A1, A2, A3, A23
    """
    if isinstance(input, clib.Hyperfine):
        angle = input.magnetic_theta.value
    else:
        angle = input

    angle_rad = angle * np.pi / 180.0

    a1 = 3.0 * (1.0 + np.cos(angle_rad)**2)
    a2 = 4.0 * np.sin(angle_rad)**2
    a3 = 1.0 + np.cos(angle_rad)**2
    
    ratio = np.array([a1, a2, a3])

    if norm:
        sum = np.sum(ratio)
        ratio = ratio / sum
        
    if max:
        maxi = np.max(ratio)
        ratio = ratio / maxi
    
    return ratio[0], ratio[1], ratio[2], a2/a3


def AngleA23(A23):
    r"""
    Returns the polar angle from a given A23 ratio for a magnetic dipole transition.

    Args:
       A23 (float): A23 ratio between 0 to 4.

    Returns:
       float: angle (deg).
    """
    a = A23 / 4.0
    
    if A23 == 0:
        angle = 0.0
    else:
        angle = 2.0 * np.arctan(np.sqrt( (1.0 - np.sqrt(1.0 - a**2)) / a ))
        
    return angle * 180.0 / np.pi

