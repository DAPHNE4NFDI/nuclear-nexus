# Nexus - Nuclear Elastic X-ray scattering Universal Software

The *Nuclear Elastic X-ray scattering Universal Software* (*NEXUS*) is a *Python* package for simulating and fitting of

* Moessbauer spectra
* nuclear resonant scattering (NRS) data
* pure electronic X-ray reflectivities (XRR)
* nuclear X-ray reflectivities (nXRR)
* polarization dependent electronic scattering.

## Documentation

[comment]: http://lars.bocklage.pages.hzdr.de/nuclearnexus

https://fs-mcp.pages.desy.de/nuclear-nexus/

## Support
In case you find bugs, need support in using the software, have questions on how to setup an evaluation, or you need additional features, please contact:

Lars Bocklage - lars.bocklage@desy.de

Many thanks go to Leon Merten Lohse for setting up the GitLab environment and all users for feedback and bug reports.
