# Nexus - the Nuclear Elastic X-ray scattering Universal Software package
# 
# Copyright 2020 - 2025 Deutsches Elektronen-Synchrotron DESY
# A research centre of the Helmholtz Association.
# All rights reserved.
#
# Author: Lars Bocklage - lars.bocklage@desy.de
# 
# This file is part of Nexus.
# 
# Nexus is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Nexus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with Nexus.
# If not, see <https://www.gnu.org/licenses/>.


# Setup of the manual build process

import os
import sys
import subprocess

do_update = True
do_sphinx_update = False
do_docs = True
run_test = False

nexus_release = '2.0.0'
nexus_version = nexus_release[:-2]

print("\nStarting build process for Nexus {}\n".format(nexus_release))

py_version_major = sys.version_info.major
py_version_minor = sys.version_info.minor
py_version = '{}{}'.format(py_version_major, py_version_minor)
py_version_dot = '{}.{}'.format(py_version_major, py_version_minor)

print("Building Nexus {} for Python version {}\n".format(nexus_release, py_version_dot))

if do_update:

  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pip', '--upgrade'])

  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'setuptools', '--upgrade'])

  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'wheel', '--upgrade'])

  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'numpy', '--upgrade'])


from setuptools import setup, Extension, find_namespace_packages
from setuptools.command.build_py import build_py
import numpy
import fileinput
 
# set correct nexus version in build files
with fileinput.FileInput("pyproject.toml", inplace = True) as f:
    for line in f:
        if "version = '" in line:
            print("version = '{}'".format(nexus_release), end ='\n')
        else:
            print(line, end ='')

with fileinput.FileInput("meson.build", inplace = True) as f:
    for line in f:
        if "        version : '" in line:
            print("        version : '{}'".format(nexus_release), end ='\n')
        else:
            print(line, end ='')

#include directories
pythondir = sys.exec_prefix
python_include_dir = pythondir + '\\include'

nexus_master = os.getcwd()

numpy_include_dir = numpy.get_include()

#### Include MKL dirs ####
#oneAPI_includedir = 'C:\\Program Files (x86)\\Intel\\oneAkPi\\mkl\\2021.2.0\\include'
#oneAPI_mpidir = 'C:\\Program Files (x86)\\Intel\\oneAkPi\\mkl\\2021.2.0\\redist\\intel64'
#oneAPI_libdir = 'C:\\Program Files (x86)\\Intel\\oneAkPi\\mkl\\2021.2.0\\lib\\intel64'

# Eigen 3.4.0
eigendir = '..\\mathlibs\\eigen-3.4.0'

# glog 0.5.0
glogdir = '..\\mathlibs\\glog\\include'
glog_libdir = '..\\mathlibs\\glog\\lib'

# ceres solver 2.0.0
ceresdir = '..\\mathlibs\\ceres_solver\\include'
ceres_libdir = '..\\mathlibs\\ceres_solver\\lib'
# for ceres 2.1.0, changes in Local Parameterization warning - do not use ceres 2.2.0 only manifold supported

# nLopt 2.7.1
nloptdir = '..\\mathlibs\\nlopt\\include'
nlopt_libdir = '..\\mathlibs\\nlopt\\lib'

# boost 1.78 (for Pagmo)
# with boost 1.86 Python 3.7 & 3.8 do not complie
boostdir = '..\\mathlibs\\boost_1_78_0'
boost_libdir = '..\\mathlibs\\boost_1_78_0\\stage\\lib'

# pagmo 2.18.0
pagmodir = '..\\mathlibs\\pagmo\\include'
pagmo_libdir = '..\\mathlibs\\pagmo\\lib'

# nexus c++ and swig files
clibfolder = 'src\\nexus\\clib'
sourcefiles = [os.path.join(clibfolder,'cnexus.i')]

#for file in os.listdir(os.path.join(nexus_master+"\\"+clibfolder)):
#    if file.endswith(".cpp"):
#        sourcefiles.append(os.path.join(clibfolder, file))

all_inlcude_dirs = [nexus_master,
                    python_include_dir,
                    numpy_include_dir,
                    eigendir,
                    glogdir,
                    ceresdir,
                    nloptdir,
                    boostdir,
                    boost_libdir,
                    pagmodir,
                    clibfolder,
                    #oneAPI_includedir,
                    #oneAPI_mpidir,
                    '../include']

utilitiesdir = 'src\\nexus\\clib\\utilities'
all_inlcude_dirs.append(utilitiesdir)
for file in os.listdir(os.path.join(nexus_master+"\\"+utilitiesdir)):
    if file.endswith(".cpp") and not file.endswith("_wrap.cpp"):
        sourcefiles.append(os.path.join(utilitiesdir, file))


mathdir = 'src\\nexus\\clib\\math'
all_inlcude_dirs.append(mathdir)
for file in os.listdir(os.path.join(nexus_master+"\\"+mathdir)):
    if file.endswith(".cpp"):
        sourcefiles.append(os.path.join(mathdir, file))


classesdir = 'src\\nexus\\clib\\classes'
all_inlcude_dirs.append(classesdir)
for file in os.listdir(os.path.join(nexus_master+"\\"+classesdir)):
    if file.endswith(".cpp"):
        sourcefiles.append(os.path.join(classesdir, file))


scatteringdir = 'src\\nexus\\clib\\scattering'
all_inlcude_dirs.append(scatteringdir)
for file in os.listdir(os.path.join(nexus_master+"\\"+scatteringdir)):
    if file.endswith(".cpp"):
        sourcefiles.append(os.path.join(scatteringdir, file))


functorsdir = 'src\\nexus\\clib\\functors'
all_inlcude_dirs.append(functorsdir)
for file in os.listdir(os.path.join(nexus_master+"\\"+functorsdir)):
    if file.endswith(".cpp"):
        sourcefiles.append(os.path.join(functorsdir, file))


module = Extension('_cnexus',
                   sources = sourcefiles,
                   include_dirs = all_inlcude_dirs,
                   swig_opts = [
                       '-c++',
                       '-I../include',
                       '-doxygen',
                       '-Wall',
                       '-DNEXUS_USE_NLOPT',
                       '-DNEXUS_USE_PAGMO',
                       '-DNEXUS_EXECUTION_POLICY=std::execution::par,',
#                       '-DNEXUS_EXECUTION_POLICY=std::execution::seq,',
### test Apple Clang compatibility
#                       '-DNEXUS_EXECUTION_POLICY= ',
#                       '-DNEXUS_WITH_CLANG',
###
                   ], # '-modern',
                   extra_compile_args = [
                       '/std:c++17',
                       '/MP',
                       '/MD',
                       '/O2',
                       '/arch:AVX2',
                       '/fp:precise',
                       '/W3',
                       '/DNEXUS_USE_NLOPT',
                       '/DNEXUS_USE_PAGMO',
                       '/DNEXUS_EXECUTION_POLICY=std::execution::par,',
#                       '/DNEXUS_EXECUTION_POLICY=std::execution::seq,',
### test Apple Clang compatibility
#                       '/DNEXUS_EXECUTION_POLICY= ',
#                       '/DNEXUS_WITH_CLANG',
###
                       #'/w15038',
                       #'/w14388',
                       #'/w14365',
                       #'/favor:blend'
                   ],
                   # conda auto flags for msvc
#                   extra_compile_args = [
#                        "/nologo",
#                        "/showIncludes",
#                        "/utf-8",
#                   #     "/Zc:__cplusplus",     #
#                        "/W3",
#                        "/EHsc",
#                        "/std:c++17",
#                   #     "/permissive-",     #
#                        "/O2",
#                        "-DBOOST_SERIALIZATION_DYN_LINK",
#                        "-DBOOST_SERIALIZATION_NO_LIB",
#                        "-DNLOPT_DLL",
#                        "-DGFLAGS_IS_A_DLL=1",
#                        "-DGLOG_CUSTOM_PREFIX_SUPPORT",
#                        "-DGLOG_NO_ABBREVIATED_SEVERITIES",
#                        "-DNEXUS_USE_NLOPT",
#                        "-DNEXUS_USE_PAGMO"
#                   ],

                   library_dirs = [glog_libdir,
                                   ceres_libdir,
                                   nlopt_libdir,
                                   boost_libdir,
                                   pagmo_libdir,
                                   # oneAPI_libdir, 
                                   ],
                   libraries = ['glog', 'ceres', 'nlopt', 'pagmo']
                   ##### MKL library does not increase speed for smaller matrices
                   #library_dirs = [oneAPI_libdir, oneAPI_mpidir],
                   #libraries = ['mkl_intel_lp64', 'mkl_sequential', 'mkl_core', 'mkl_blacs_intelmpi_lp64', 'mkl_lapack95_lp64'] # EigenSolver slower with MKL for small matrices
                   #libraries = ['mkl_rt'],  # not working with mkl_rt
				   )


# Build extensions before python modules,
# or the generated SWIG python files will be missing.
class buildpy(build_py):
    def run(self):
        self.run_command('build_ext')
        super(build_py, self).run()

setup(name = 'nexus',
      version = nexus_release,
      description = 'Nexus - Nuclear Elastic X-ray scattering Universal Software',
      url = 'https://gitlab.hzdr.de/lars.bocklage/nuclearnexus',
      author = 'Lars Bocklage',
      author_email = 'lars.bocklage@desy.de',
      license = 'GNU GPLv3 or later',

      setup_requires = ['numpy',
                        'setuptools',
                        'wheel'
                        ],

      install_requires = ['numpy',
                          'scipy',
                          'matplotlib',
                          'pybaselines'
                          ],

      cmdclass = {'build_py': buildpy,},
      zip_safe = False,
      
      package_dir = {'': 'src'},
      packages = find_namespace_packages(where='src'),
      package_data = { '': ['*.dll']}, # add all .dll files and .nff files
    
      ext_package = 'nexus.clib',
      ext_modules = [module],
      
      script_args = ["build_py", "--force", "bdist_wheel"] # build external C++ and creates wheel for distribution

      # test_suite = "tests"  # setup correct way
      )

print("\nBuild finished\n")

print("\nUninstalling prior Nexus version\n")

subprocess.check_call([sys.executable, '-m', 'pip', 'uninstall', '-y', 'nexus'])

print("\nInstalling Nexus wheel\n")

wheel_string = 'dist\\nexus-{}-cp{}-cp{}-win_amd64.whl'.format(nexus_release, py_version, py_version)
try:
    subprocess.run([sys.executable, '-m', 'pip', 'install', wheel_string], stderr=subprocess.DEVNULL)
except subprocess.CalledProcessError:
    print("error during installation, trying different name")

wheel_string = 'dist\\nexus-{}-cp{}-cp{}m-win_amd64.whl'.format(nexus_release, py_version, py_version)
try:
    subprocess.run([sys.executable, '-m', 'pip', 'install', wheel_string], stderr=subprocess.DEVNULL)
except subprocess.CalledProcessError:
    print("error during installation, check setup")





if do_sphinx_update:
  print("\nUpdating sphinx\n")

  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'sphinx', '--upgrade'])
  
  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'sphinx_rtd_theme', '--upgrade'])
  
  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pandoc', '--upgrade'])
  
  subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'nbsphinx', '--upgrade'])



if do_docs:
  print("\nCreating Nexus documentation\n")

  os.system(r'docs\make clean')

  os.system(r'docs\make html')




if run_test:
  
  print("\nRunning tests\n")

  subprocess.run(['py', '-{}'.format(py_version_dot), 'tests/test_quantum.py'])



print("\nFinished Build Process")
